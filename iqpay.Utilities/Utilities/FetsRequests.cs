namespace iqpay.Core.Utilities
{
    public class FetsBalanceResponse
    {
        public int ResponseCode { get; set; } 
        public bool Success { get; set; } 
        public string Message { get; set; } 
        public decimal Balance { get; set; } 
    }

    public class FetsVendResponse
    {
        public int ResponseCode { get; set; } 
        public bool Success { get; set; } 
        public string Message { get; set; } 
        public decimal Balance { get; set; } 
    }

    public class FetsBalanceRequest
    {
        public string MerchantId{get;set;}
        public string EnKey{get;set;}
    }

    public class FetsVendRequest
    {
        public string MerchantId{get;set;}
        public string EnKey{get;set;}
        public decimal Amount{get;set;}
        public long BillerId{get;set;}
        public long ProductId{get;set;}
        public string CustomerRefNum{get;set;}
        public string CustomerPhone{get;set;}
        public string OrderId{get;set;}
        public string CustomerId {get;set;}
    }

}