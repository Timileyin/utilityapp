﻿using System;

public class ServiceAttribute : Attribute
{
    public string Name { get; private set; }
    public string CapriconCode { get; private set; }
    public string FetsCode { get; private set; }
    public string KadickCode { get; private set; }
    public string NomiCode { get; private set; }
    public string PagaCode { get; private set; }
    public string VatebraCode { get; private set; }
    public string Processors { get; private set; }

    public ServiceAttribute(string name, string capriconCode, string fetsCode, string kadickCode, string nomiCode, string pagaCode, string vatebraCode, string processor)
    {
        Name = name;
        CapriconCode = capriconCode;
        FetsCode = fetsCode;
        KadickCode = kadickCode;
        NomiCode = nomiCode;
        PagaCode = pagaCode;
        VatebraCode = vatebraCode;
        Processors = processor;

    }
}

