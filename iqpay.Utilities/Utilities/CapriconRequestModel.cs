﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace iqpay.Core.Utilities
{
    public class CapriconResponse<T> where T:class
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public string Code { get; set; }
        public CapriconError Error{get;set;}
        public List<CapriconError> Errors { get; set; }
        public T Data { get; set; }
    }



    public class AccountDetails
    {
        public AccountUser User { get; set; }
    }

    public class AccountUser
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string OutstandingBalance { get; set; }
        public string AccountNumber { get; set; }
        public decimal? MinimumAmount { get; set; }
        public dynamic RawOutput { get; set; }
    }

    public class CapriconError
    {
        public string Message { get; set; }
    }

    public class TransactionSuccessful
    {
        public string StatusCode { get; set; }
        public string TransactionMessage { get; set; }
        public string TransactionStatus { get; set; }
        public string TransactionReference { get; set; }
        public List<EPin> Pins { get; set; }
        public string TokenCode { get; set; }
        public string AmountOfPower{get;set;}
    }

    public class EPin
    {
        public string Instructions{get;set;}
        public string SerialNumber{get;set;}
        public string Pin{get;set;}
        public string ExpiresOn{get;set;}

    }

    public class CapricornBalance
    {
        public decimal Balance { get; set; }
        public DateTime LastDeposit { get; set; }
    }

    public class VendElectricity
    {
        public string Account_number { get; set; }
        public int Amount { get; set; }
        public string Phone { get; set; }
        public string Service_type { get; set; }
        public int AgentId { get; set; }
        public string AgentReference { get; set; }
    }

    public class VendEPins
    {
        public string Service_type{get;set;}
        public int Amount{get;set;}
        public int PinValue{get;set;}
        public int NumberOfPins{get;set;}
        public string AgentReference{get;set;}
        public int AgentId{get;set;} 
    }

    public enum CapricornRequests
    {
        [Description("superagent/account/balance")]
        FetchBalance,
        [Description("services/multichoice/request")]
        VendMultiChoice,
        [Description("services/multichoice/request")]
        VendStarTimes,
        [Description("services/namefinder/query")]
        VerifyAccountName,
        [Description("services/electricity/request")]
        VendElectricity,
        [Description("services/epin/request")]
        VendEPins
    }
}
