using System;
using System.Collections.Generic;
using System.ComponentModel;
using Newtonsoft.Json;

namespace iqpay.Core.Utilities
{
    public class ShagoRequest
    {
        public string ServiceCode { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Disco { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string MeterNo { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? Amount { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Phonenumber { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Address { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Request_id { get; set; }
    }

    public class ShagoWallet
    {
        public long WalletId { get; set; }
        public string CommissionBalance { get; set; }
        public decimal PrimaryBalance { get; set; }
        public CreatedAt CreatedAt { get; set; }    
    }

    public class CreatedAt
    {        
        public DateTimeOffset Date { get; set; }        
        public long TimezoneType { get; set; }
        public string Timezone { get; set; }
    }

    public class ShagoResponse
    {
        public string Status{get;set;}
        public string Message{get;set;}
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public ShagoWallet Wallet{get;set;} 

    }

    public class ShageVerificationResponse
    {
        public string MeterNo { get; set; } 
        public string CustomerName { get; set; } 
        public string CustomerAddress { get; set; } 
        public string CustomerDistrict { get; set; } 
        public string PhoneNumber { get; set; } 
        public string Type { get; set; } 
        public string Disco { get; set; } 
        public string Status { get; set; } 
    }

    public class ShagoVendResponse
    {     
        public string Token { get; set; }
        public string Unit { get; set; }
        public string TaxAmount { get; set; }
        public decimal? BonusUnit { get; set; }
        public string BonusToken { get; set; }
        public decimal Amount { get; set; }
        public string Message { get; set; }
        public long Status { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public DateTimeOffset Date { get; set; }
        public string TransId { get; set; }
        public string Disco { get; set; }
    }

    public class ShagoVendRequest
    {
        public string ServiceCode { get; set; }
        public string Disco { get; set; }
        public string MeterNo { get; set; }
        public string Type { get; set; }
        public long Amount { get; set; }
        public string Phonenumber { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string RequestId { get; set; }
    }
}