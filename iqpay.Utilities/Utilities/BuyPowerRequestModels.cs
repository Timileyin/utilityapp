using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace iqpay.Core.Utilities
{
    public class BuyPowerGetBalanceResponse
    {
        public bool Error { get; set; } 
        public string DiscoCode { get; set; } 
        public string VendType { get; set; } 
        public string MeterNo { get; set; } 
        public int MinVendAmount { get; set; } 
        public int MaxVendAmount { get; set; } 
        public int ResponseCode { get; set; } 
        public int Outstanding { get; set; } 
        public int DebtRepayment { get; set; } 
        public string Name { get; set; } 
        public string Address { get; set; } 
    }

    public class BuyPowerVendRequest
    {
        public string Meter { get; set; } 
        public string Disco { get; set; } 
        public string VendType { get; set; } 
        public string OrderId { get; set; } 
        public string PaymentType { get; set; } 
        public string Phone { get; set; } 
        public string Email { get; set; } 
        public string Name { get; set; } 
        public decimal Amount { get; set; } 
    }

    public class BuyPowerVerify
    {
        public bool Error { get; set; } 
        public string DiscoCode { get; set; } 
        public string VendType { get; set; } 
        public string MeterNo { get; set; } 
        public decimal MinVendAmount { get; set; } 
        public string Name { get; set; } 
        public string Address { get; set; } 
        public decimal MaxVendAmount { get; set; }     
    }

    public class BuyPowerVerifyRequest
    {
        public string Meter { get; set; } 
        public string Disco { get; set; } 
        public string VendType { get; set; } 
        public bool OrderId { get; set; } 
    }


    public class BuyPowerBalance
    {
        public decimal Balance { get; set; }
    }

    public class BuyPowerVendData    {
        public int Id { get; set; } 
        public string AmountGenerated { get; set; } 
        public string DebtAmount { get; set; } 
        public string Disco { get; set; } 
        public string FreeUnits { get; set; } 
        public string OrderId { get; set; } 
        public string ReceiptNo { get; set; } 
        public string Tax { get; set; } 
        public string VendTime { get; set; } 
        public string Token { get; set; } 
        public int TotalAmountPaid { get; set; } 
        public string Units { get; set; } 
        public string VendAmount { get; set; } 
        public string VendRef { get; set; } 
        public string ResponseCode { get; set; } 
        public string ResponseMessage { get; set; } 
    }

    public class BuyPowerVendResponse    
    {
        public bool Status { get; set; } 
        public BuyPowerVendData Data { get; set; } 
    }


}