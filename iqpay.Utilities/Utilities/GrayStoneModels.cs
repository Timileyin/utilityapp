using Newtonsoft.Json;
namespace iqpay.Core.Utilities
{
    public class GreyStoneSessionResponse
    {
        public int RespCode { get; set; } 
        public string RespDescription { get; set; } 
        public string SessionId { get; set; } 
        public string Hash { get; set; } 
    }

    public class GreyStoneBalance
    {
        public string ResponseCode { get; set; } 
        public string ResponseDesrciption { get; set; } 
        public decimal Balance { get; set; } 
        public string EmailAddress { get; set; } 
        public string AgentID { get; set; } 
        public string AgentKey { get; set; } 
    }

    public class GreyStoneVendResponse
    {
        public string RespCode { get; set; } 
        public string RespDescription { get; set; } 
        public decimal? Amount { get; set; } 
        public string TokenAmount { get; set; } 
        public string Description { get; set; } 
        public string ReceiptNumber { get; set; } 
        public string Tariff { get; set; } 
        public string Tax { get; set; } 
        public string Units { get; set; } 
        public string UnitsType { get; set; } 
        public string Value { get; set; } 
        public string BsstToken { get; set; } 
        public string BsstTokenUnits { get; set; } 
        public string OrderNumber { get; set; } 
        public string BillerStatus { get; set; } 
        public string CustomerName { get; set; } 
        public string CustomerAdddress { get; set; } 
  
    }

    public class VerifyUserResponse{
        public long Status { get; set; }
        public string RespCode { get; set; }
        public string ResponseDesrciption { get; set; }
        public string CustomerMessage { get; set; }
        public string CustomerId { get; set; }
        public string ThirdPartyCode { get; set; }
        public string CustReference { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string Othername { get; set; }
        public string PaymentType { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string BusniessDistrict { get; set; }
        public string Receiver { get; set; }
        public string MinimumAmount { get; set; }
        public string CreditLimit { get; set; }
        public string OutStandingAmount { get; set; }
        public string AccountType { get; set; }
        public string AccountNumber { get; set; }
        public string AgentId { get; set; }
        public string AgentKey { get; set; }
        public string Hash { get; set; }
    }

    public class GreyStoneVendRequest
    {
        public string CustomerId { get; set; } 
        public string AccountType { get; set; } 
        public string CustomerName { get; set; } 
        public int Amount { get; set; } 
        public string RefNumber { get; set; } 
        [JsonProperty("MerchantFK")]
        public string MerchantFK { get; set; } 
        public string HashValue { get; set; }
        public string PhoneNumber{get;set;}
    }

}