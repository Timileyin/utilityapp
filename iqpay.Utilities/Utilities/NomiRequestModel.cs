﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace iqpay.Core.Utilities
{    
    public class NomiRequestModel
    {
        public NomiAuth Auth { get; set; }
        public int Version { get; set; }
        public string Command { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public object ExtraParams { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? Operator { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public decimal? AmountOperator { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public long? AccountId { get; set; }
        public string Msisdn { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public int? ProductId{get;set;}
    }

    public class NomiAuth
    {
        public string Username { get; set; } 
        public string Password { get;  set; }
        public string Salt { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Signature { get; set; }
    }

    public enum NomiWorldCommand
    {
        [Description("getBalance")]
        GetBalance = 1,
        [Description("execTransaction")]
        ExecTransaction
    }

    public class NomiResponseStatus
    {
        public int Id { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public string TypeName { get; set; }
    } 

    public class NomiResponse<T> where T:class
    {
        public NomiResponseStatus Status { get; set; }
        public string Command { get; set; }
        public long TimeStamp { get; set; }
        public string Reference { get; set; }
        public T Result { get;  set; }    
    }

    public class NomiVendProductParams
    {
        public string AccountType { get; set; }
    }

    public class GetBalanceResult
    {
        public decimal Value { get; set; }
        public string Currency { get; set; }
    }


    public class VendResult
    {

    }
}
