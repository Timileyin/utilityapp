using System.Threading.Tasks;

namespace iqpay.Repository.AutoReversals
{
    public interface IVaterbraReversal
    {
        Task ReversalTransaction();
    }
}