using System.Threading.Tasks;
using iqpay.Repository.Interface;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.Processors.ProcessorImplementations;
using Microsoft.Extensions.Logging;
using System;

namespace iqpay.Repository.AutoReversals
{
    public class VatebraReversal:IVaterbraReversal
    {
        private readonly IRepository<VendingLogs> _repository;
        private readonly IVatebraRequests _vatebraRequests;
        private readonly ILogger<VatebraRequests> _logger;
        public VatebraReversal(IRepository<VendingLogs> repository, IVatebraRequests vatebraRequests, ILogger<VatebraRequests> _logger)
        {
            _repository = repository;
            _vatebraRequests = vatebraRequests;
        }
        public async Task ReversalTransaction()
        {
            var yesterday = DateTime.Now.Date.AddDays(-1);
            var transactions = await _repository.GetAllListAsync(v=>v.Status == VendingStatusEnum.Completed && v.DateCreated >= yesterday);
            foreach(var transaction in transactions)
            {
                _logger.LogInformation(null, $"Checking Transaction reference status for transaction {transaction.VendingCode}");                
                var response  = await _vatebraRequests.FetchTransactionByRef(transaction.VendingCode);
                _logger.LogInformation(null, $"Transaction log status {response.Message}");

                if(response.Message == "Failed")
                {
                    transaction.Status = VendingStatusEnum.TransactionReversed;
                    await _repository.UpdateAsync(transaction);
                    await _repository.SaveChangesAsync();
                }
            }
        }
    }
}