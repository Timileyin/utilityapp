using System;
using System.Threading.Tasks;
using iqpay.Data.Entities.Settings;
using iqpay.Repository.Interface;

namespace iqpay.Repository.SettingsRepository
{
    public interface ISettingsRepository:IAutoDependencyRegister
    {
        void UpdateSettings(Settings settings);

        Settings FetchSettings();
    }
}