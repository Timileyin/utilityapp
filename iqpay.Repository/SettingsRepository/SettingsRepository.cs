using System;
using System.Threading.Tasks;
using iqpay.Data.Entities.Settings;
using iqpay.Repository.Interface;

namespace iqpay.Repository.SettingsRepository
{
    public class SettingsRepository:IAutoDependencyRegister
    {
        private readonly IRepository<Settings> _settingsRepo;

        public SettingsRepository(IRepository<Settings> settingsRepo)
        {
            _settingsRepo = settingsRepo;
        }
        public void UpdateSettings(Settings settings)
        {            
            if(settings.IsTransient())
            {
                _settingsRepo.Insert(settings);
                _settingsRepo.SaveChanges();
            }else
            {
                var existingSettings = _settingsRepo.FirstOrDefault(x=>x.Id == settings.Id);
                if(existingSettings != null)
                {
                    existingSettings.HouseHoldPowerBonus = settings.HouseHoldPowerBonus;
                    existingSettings.HouseHoldBonusDates = settings.HouseHoldBonusDates;
                    existingSettings.EnableHouseHoldBonus = settings.EnableHouseHoldBonus;

                    _settingsRepo.Update(existingSettings);
                    _settingsRepo.SaveChanges();                    
                }
            }
        }

        public Settings FetchSettings()
        {
            var settings = _settingsRepo.FirstOrDefault(x=>true);
            return settings;
        }
    }
}