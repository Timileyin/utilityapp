﻿using Autofac;
using iqpay.Data;
using iqpay.Repository.CoreRepository;
using iqpay.Repository.Interface;

namespace iqpay.Repository.AutoFacModule
{
    public class RepositoryModule:Module
    {
        protected override void Load(ContainerBuilder builder)
        {


            builder.RegisterType<IQPAYContext>().InstancePerLifetimeScope();
           

            builder.RegisterGeneric(typeof(Repository<>))
                .As(typeof(IRepository<>))
                .InstancePerLifetimeScope();
            //builder.RegisterType<NACCPaymentJobRepository>().As<INACCPaymentJobRepository>().InstancePerMatchingLifetimeScope();
            //builder.RegisterType<EmailJobRepositorycs>().As<IEmailJobRepository>().InstancePerMatchingLifetimeScope();
            builder.RegisterAssemblyTypes(typeof(IAutoDependencyRegister).Assembly)
                .AssignableTo<IAutoDependencyRegister>()
                .As<IAutoDependencyRegister>()
                .AsImplementedInterfaces().InstancePerLifetimeScope();
            base.Load(builder);
        }
    }
}