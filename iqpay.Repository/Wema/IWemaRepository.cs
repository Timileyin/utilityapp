using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;
using iqpay.Data.UserManagement.Model;
using iqpay.Repository.Wema.Models;

namespace iqpay.Repository.Wema
{
    public interface IWemaRepository:IAutoDependencyRegister
    {
        string GenerateNUBAN(string prefix, string fiCode);
        ApplicationUser FindUserByWema(string accountNumber);
        List<ApplicationUser> FetchWemaAccounts(); 
        Task<Transaction> FetchTransaction(string sessionId, string crAccount, DateTime txnDate, decimal amount);       
    }
}
