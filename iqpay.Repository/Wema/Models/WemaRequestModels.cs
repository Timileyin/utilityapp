using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace iqpay.Repository.Wema.Models
{

    public class WemaAccount
    {
        public long Id{get;set;}
        public string DealerCode{get;set;}
        public string AccountNumber{get;set;}        
        public DateTime DateCreated{get;set;}
    }

    public class TransactionRequest    
    {
        public string Sessionid { get; set; } 
        public string Craccount { get; set; } 
        public decimal Amount { get; set; } 
        public string Txndate { get; set; } 
    }

    public class WemaLookUp
    {
        public string Accountnumber{get;set;}
        public string Bankcode{get;set;}
    }

    public class WemaPayload
    {
        public string Originatoraccountnumber { get; set; }
        public decimal Amount { get; set; }
        public string Originatorname { get; set; }
        public string Narration { get; set; }
        public string Craccountname { get; set; }
        public string Paymentreference { get; set; }
        public string Bankname { get; set; }
        public string Sessionid { get; set; }
        public string Craccount { get; set; }
        public string Bankcode { get; set; }
    }

    public class Transaction
    {
        public string Originatoraccountnumber { get; set; } 
        public string Amount { get; set; } 
        public string Originatorname { get; set; } 
        public string Narration { get; set; } 
        public string Craccountname { get; set; } 
        public string Paymentreference { get; set; } 
        public string Bankname { get; set; } 
        public string Sessionid { get; set; } 
        public string Craccount { get; set; } 
        public string Bankcode { get; set; } 
        public string Requestdate { get; set; } 
        public string Nibssresponse { get; set; } 
        public string Sendstatus { get; set; } 
        public string Sendresponse { get; set; } 
    }

    public class TransactionResponse {
        public string Status { get; set; } 
        public string Status_desc { get; set; } 
        public List<Transaction> Transactions { get; set; } 
    }

}