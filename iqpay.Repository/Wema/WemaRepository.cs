using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;
using iqpay.Core.Utilities;
using iqpay.Data.UserManagement.Model;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using Microsoft.Extensions.Logging;
using iqpay.Repository.Wema.Models;
using System.Linq;

namespace iqpay.Repository.Wema
{    
    public class WemaRepository:IWemaRepository
    {

        private readonly IRepository<ApplicationUser> _userRepo;
        private readonly IConfiguration _configuration;
        private readonly ILogger<WemaRepository> _logger;
        private readonly HttpClient _httpClient;
        private readonly string _url;

        public WemaRepository(ILogger<WemaRepository> logger, IConfiguration config, IRepository<ApplicationUser> userRepo)
        {            
            _configuration = config;
            //_apiKey = _configuration["Monnify:APIKEY"];
            //secret = _configuration["Wema:Secret"];
            _url = _configuration["Wema:URL"];
            _userRepo = userRepo;
            //_callBackURL = _configuration["Rubies:CallBackURL"];
            //_contractCode = _configuration["Monnify:ContractCode"];
            _logger = logger;
            //_base64Token = _configuration["Monnify:Base64Token"];

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(_url),                                
            };            
            //_httpClient.DefaultRequestHeaders.Add("Authorization", _secret);
        }   

        private async Task<T> Request<T>(HttpMethod method,  string endpoint)where T:class
        {
            return await Request<T, Object>(method, endpoint);
        }  

        
        private async Task<T> Request<T, U>(HttpMethod method,  string endpoint, U request = null  ) where T:class where U:class
        {
            T response;
            var responseBody = new HttpResponseMessage();

            if(method == HttpMethod.Post)
            {
                var postBody = JsonConvert.SerializeObject(request);
                var jsondata = new StringContent(postBody, Encoding.UTF8, "application/json");
                responseBody = await _httpClient.PostAsync(endpoint, jsondata);
                _logger.Log(LogLevel.Warning,$"Wema Response({endpoint}=>: {jsondata})");
            }else if(method == HttpMethod.Get){
                responseBody = await _httpClient.GetAsync(endpoint);
            }                      
            
            if(responseBody.IsSuccessStatusCode)
            {
                var body = await responseBody.Content.ReadAsStringAsync();
                _logger.Log(LogLevel.Warning,$"Wema Response({endpoint}=>: {body})");
                response = JsonConvert.DeserializeObject<T>(body);
                return response;
            }else
            {
                var body = await responseBody.Content.ReadAsStringAsync();
                throw new Exception($"Wema request failed with this response {body}");
            }        
        }

        public ApplicationUser FindUserByWema(string accountNumber)
        {
            var userInfo = _userRepo.FirstOrDefault(x=>x.VirtualAccountNumber == accountNumber);
            return userInfo;
        }

        public async Task<Transaction> FetchTransaction(string sessionId, string crAccount, DateTime txnDate, decimal amount)
        {
            var reqBody = new TransactionRequest{
                Amount = amount,
                Craccount = crAccount,
                Sessionid = sessionId,
                Txndate = txnDate.ToString("dd-mm-yyyy")
            };
        
            var resp = await Request<TransactionResponse, TransactionRequest>(HttpMethod.Post,"Trans/TransQuery", reqBody);
            if(resp.Status == "00")
            {
                return resp.Transactions.FirstOrDefault();
            }

            throw new Exception("Error occured while fetching transaction");            
        }

        public List<ApplicationUser> FetchWemaAccounts()
        {
            var wemaAccount = _userRepo.GetAllList(x=>!string.IsNullOrWhiteSpace(x.VirtualAccountNumber));
            return wemaAccount;
        } 

        public string GenerateNUBAN(string prefix, string fiCode)
        {
            var serialNuban = Utilities.GenerateRandomDigits(6);

            var user = _userRepo.FirstOrDefault(x=>x.VirtualAccountNumber.Contains(prefix+serialNuban));
            if(user != null)
            {
                serialNuban = Utilities.GenerateRandomDigits(6);
            }

            var nubanNumber = fiCode+prefix+serialNuban;

            var nubanCheckDigit = 10 - (((3*(int)nubanNumber[0]) + (7*(int)nubanNumber[1]) + (3*(int)nubanNumber[2]) + (3*(int)nubanNumber[3]) + (7*(int)nubanNumber[4])
                + (3*(int)nubanNumber[5]) + (3*(int)nubanNumber[6]) + (7*(int)nubanNumber[7]) + (3*(int)nubanNumber[8]) + (3*(int)nubanNumber[9]) + (7*(int)nubanNumber[10])
                + (3*(int)nubanNumber[11]) + (3*(int)nubanNumber[12]) + (7*(int)nubanNumber[13]) + (3*(int)nubanNumber[14]))%10);

            return prefix+serialNuban+nubanCheckDigit;
        }
    }
}
