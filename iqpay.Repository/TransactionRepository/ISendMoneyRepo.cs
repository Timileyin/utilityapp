using System;
using System.Threading.Tasks;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.Interface;
using System.Collections.Generic;

namespace iqpay.Repository.TransactionRepository
{
    public interface ISendMoneyRepo:IAutoDependencyRegister
    {
        SendMoneyLog GetSendMoneyLog(string txnRef);
        Task<List<SendMoneyLog>> FetchLogs(long? userId = null, DateTime? startTime = null, DateTime? endDate = null);
        Task UpdateLog(SendMoneyLog log);
        Task<SendMoneyLog> CreateLog(string txnref, decimal amount, string accountNumber, string accountName, string bankName, string narration, long userId);        
    }
}
