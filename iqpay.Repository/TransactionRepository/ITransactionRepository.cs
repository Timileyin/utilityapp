﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.Interface;

namespace iqpay.Repository.TransactionRepository
{
    public interface ITransactionRepository:IAutoDependencyRegister
    {
        string GenerateTransactionReference(string prefix);
        BillLog FetchTransaction(string txnRef);
        Task<BillLog> GenerateBillLog(decimal amount, string txnRef, BillType billType, long userId);
        Task<List<BillLog>> FetchOrders(DateTime startDate, DateTime endDate);
        void UpdateTransation(BillLog bill);     
                         
    }
}
