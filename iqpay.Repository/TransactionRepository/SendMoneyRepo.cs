using System;
using System.Threading.Tasks;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.Interface;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace iqpay.Repository.TransactionRepository
{
    public class SendMoneyRepo:ISendMoneyRepo
    {
        private IRepository<SendMoneyLog> _repo;

        public SendMoneyRepo(IRepository<SendMoneyLog> repo)
        {
            _repo = repo;
        }
        public SendMoneyLog GetSendMoneyLog(string txnRef)
        {
            var log = _repo.GetAll().Include(x=>x.SentBy).FirstOrDefault(x=>x.TxnRef == txnRef);
            return log;
        }
        public async Task<List<SendMoneyLog>> FetchLogs(long? userId = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            if(startDate == null || endDate == null)
            {
                endDate = DateTime.Now;
                startDate = DateTime.Today;
            }
            var logs = await _repo.GetAll().Include(x=>x.SentBy).Where(x=>(x.SentById == userId || userId == null) && x.DateCreated >= startDate && x.DateCreated <= endDate).OrderByDescending(x=>x.DateCreated).ToListAsync();;
            return logs;
        }

        public async Task<SendMoneyLog> CreateLog(string txnref, decimal amount, string accountNumber, string accountName, string bankName, string narration, long userId)
        {
            var log = new SendMoneyLog{                
                AccountNumber = accountNumber,
                Amount = amount,
                Bank = bankName,                
                SentById = userId,                
                Status = SendMoneyEnum.Initiated,
                TxnRef = txnref,
                AccountName = accountName,
                Narration = narration                            
            };

            await _repo.InsertAsync(log);
            await _repo.SaveChangesAsync();
            return log;
        }

        public async Task UpdateLog(SendMoneyLog log)
        {
            var existingLog = await _repo.FirstOrDefaultAsync(x=>x.Id == log.Id);
            if(existingLog != null)
            {
                existingLog.Fee = log.Fee;
                existingLog.Status = log.Status;
                existingLog.TransferResponse = log.TransferResponse;
                existingLog.DateCompleted = log.DateCompleted;
                existingLog.Narration = log.Narration;
                existingLog.AccountName = log.AccountName;
                existingLog.Bank = log.Bank;
                
                await _repo.UpdateAsync(existingLog);
                await _repo.SaveChangesAsync();
            }
        }
        
    }
}