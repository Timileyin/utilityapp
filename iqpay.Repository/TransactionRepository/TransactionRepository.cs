﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace iqpay.Repository.TransactionRepository
{
    public class TransactionRepository:ITransactionRepository
    {
        private IRepository<BillLog> _repo;
        public TransactionRepository(IRepository<BillLog> repo)
        {
            _repo = repo;
        }

        public BillLog FetchTransaction(string txnRef)
        {
            var transaction = _repo.GetAll().Include(x=>x.User).FirstOrDefault(x => x.BillRefenceNo == txnRef);
            return transaction;
        }
       
        public async Task<BillLog> GenerateBillLog(decimal amount, string txnRef, BillType billType, long userId)
        {
            try
            {
                var billLog = new BillLog
                {
                    Amount = amount,
                    BillRefenceNo = txnRef,
                    BillType = (int)billType,
                    BillStatus = (int)BillStatus.Initiated,
                    UserId = userId
                };

                await _repo.InsertAsync(billLog);
                await _repo.SaveChangesAsync();

                return billLog;
            }catch(Exception ex)
            {
                return null;
            }
            
        }

        public void UpdateTransation(BillLog bill)
        {
            _repo.Update(bill);
            _repo.SaveChanges();
        }

        public string GenerateTransactionReference(string prefix)
        {
            var randomNumber = new Random().Next(1000000, 9999999);
            var txnRef = prefix + randomNumber.ToString();
            while(FetchTransaction(txnRef) != null)
            {
                randomNumber = new Random().Next(1000000, 9999999);
                txnRef = prefix + randomNumber.ToString();
            }

            return txnRef;
        }

        public async Task<List<BillLog>> FetchOrders(DateTime startDate, DateTime endDate){
            return await _repo.GetAll().ToListAsync(); //.Where(x=>x.BillStatus == BillStatus.)
        }
    }
}
