﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Processors
{
    public class ProcessorRepository:IProcessorRepository
    {
        private readonly IRepository<Processor> _repository;

        public ProcessorRepository(IRepository<Processor> repository)
        {
            _repository = repository;
        }

        public async Task<Processor> GetProcessorByCode(string code)
        {
            var processor = await _repository.FirstOrDefaultAsync(x=>x.Code == code);
            return processor;
        }

        public async Task CreateProcessor(Processor processor)
        {
            await _repository.InsertAsync(new Processor
            {
                Code = processor.Code,
                Name = processor.Name,
                Status = processor.Status
            });
            await _repository.SaveChangesAsync();
        }

        public async Task<List<Processor>> FetchProcessors()
        {
            var processors = await _repository.GetAllListAsync();

            return processors;
        }

        public async Task<Processor> GetProcessor(long id)
        {
            var processor = await _repository.FirstOrDefaultAsync(x => x.Id == id);
            return processor;
        }

        public async Task UpdateProcessor(Processor processor)
        {
            var updateProcessor = _repository.GetById(processor.Id);
            if (updateProcessor != null)
            {
                updateProcessor.Name = processor.Name;
                updateProcessor.Code = processor.Code;
                updateProcessor.Status = processor.Status;
                await _repository.UpdateAsync(updateProcessor);
                await _repository.SaveChangesAsync();
            }
            
        }
    }
}
