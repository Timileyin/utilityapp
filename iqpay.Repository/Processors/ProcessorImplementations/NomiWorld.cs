﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public class NomiWorldRequests : INomiWorldRequests 
    {
        private readonly string _password;
        private readonly string _username;
        private readonly int _version;
        private readonly string _url;
        private readonly IRepository<VendingLogs> _vendingRepo; 
        private readonly IConfiguration _configuration;
        private readonly HttpClient _httpClient;

        public NomiWorldRequests(IConfiguration configuration, IRepository<VendingLogs> vendingRepo)
        {
            _configuration = configuration;
            _vendingRepo = vendingRepo;
            _password = _configuration["Processors:NomiWorld:Password"];//GetSection("Processors.NomiWorld.Password").Value;
            _username = _configuration["Processors:NomiWorld:Username"];
            _version = Convert.ToInt32(_configuration["Processors:NomiWorld:Version"]);
            _url = _configuration["Processors:NomiWorld:URL"];

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(_url),
            };
        }


        public async Task<GetBalanceResult> FetchBalance()
        {
            var balanceResponse = await Request<GetBalanceResult>(NomiWorldCommand.GetBalance);
            if (balanceResponse.Status.Type == 0)
            {
                return balanceResponse.Result;
            }

            return null;
        }

        public NomiResponse<Object> VendProduct(string vendCode, string serviceCode, decimal amount, string accountNumber, string accountType, out string message)
        {
            message = string.Empty;
            var balanceTasks = Task.Run(()=>FetchBalance()).Result;
            if (balanceTasks != null)
            {
                if (balanceTasks.Value > amount)
                {
                    var extraParams = new NomiVendProductParams
                    {
                        AccountType = accountType
                    };

                    var vendResponse = Request<Object>(NomiWorldCommand.ExecTransaction, extraParams, serviceCode, accountNumber, amount).Result;
                    var vendDetails = _vendingRepo.FirstOrDefault(x=>x.VendingCode == vendCode);
                    vendDetails.ProcessorsPayLoad = JsonConvert.SerializeObject(vendResponse);
                    _vendingRepo.Update(vendDetails);
                    _vendingRepo.SaveChanges();
                    return vendResponse;
                }

                message = "Transaction has failed. Please try again in 15 minutes";
                return null;
            }

            message = "Service vending Process failed. Please try again later - F2";
            return null;
        }

        private string LocalizeNumber(string number)
        {
            if(number[0] == '0')
            {
                return $"234{number.Substring(1, 10)}";
            }
            return $"234{number}";
        }

        private async Task<NomiResponse<T>> Request<T>(NomiWorldCommand command, object extraParams = null, string vendorCode = null, string customerNumber = null, decimal? amount = null) where T:class
        {
            string productCode = null;
            int productId = 0;
            var passwordHash = Utilities.GenerateNomiPasswordHash(_password, out string salt);
            if(!string.IsNullOrEmpty(vendorCode)){
                if(vendorCode.Contains(','))            
                {
                    var codeSplit = vendorCode.Split(',');
                    vendorCode = codeSplit[0];
                    productCode = codeSplit[1];
                    int.TryParse(productCode, out productId);
                }   
            }                     
            
            var requestImp = new NomiRequestModel
            {
                Auth = new NomiAuth
                {
                    Password = passwordHash,
                    Salt = salt,
                    Username = _username
                },
                Version = _version,
                Command = command.GetDescription(),
                ExtraParams = extraParams,
                Msisdn = !string.IsNullOrWhiteSpace(customerNumber) ? LocalizeNumber(customerNumber) : customerNumber,
                AmountOperator = amount,
                Operator = Convert.ToInt32(vendorCode)                                
            };

            if(productId == 0){
                requestImp.ProductId = null;
            }else{
                requestImp.ProductId = productId;
            }

            var requestContent = JsonConvert.SerializeObject(requestImp);
            var data = new StringContent(requestContent, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(string.Empty,data);
            if (response.IsSuccessStatusCode)
            {
                var body = await response.Content.ReadAsStringAsync();
                var requestResponse = JsonConvert.DeserializeObject<NomiResponse<T>>(body);
                return requestResponse;
            }

            return null;
        }

    }
}
