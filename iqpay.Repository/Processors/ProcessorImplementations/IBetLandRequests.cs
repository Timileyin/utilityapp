using System;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public interface IBetLandRequest
    {
        bool Bet(string vendCode, int gameId, int[] panel ); 
    }
}