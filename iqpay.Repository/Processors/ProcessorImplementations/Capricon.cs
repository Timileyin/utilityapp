﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using iqpay.Data.Entities.Services;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.Interface;
using Microsoft.Extensions.Logging;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public class Capricon:ICapriconRequests
    {
        private readonly string _apikey;
        private readonly string _url;

        private readonly IConfiguration _configuration;
        private readonly HttpClient _httpClient;
        private readonly string _agentId;
        private readonly IRepository<VendingLogs> _vendingRepos;
        private readonly ILogger<Capricon> _logger;
        public Capricon(IConfiguration configuration, IRepository<VendingLogs> vendingRepos, ILogger<Capricon> logger)
        {
            _configuration = configuration;

            _apikey = _configuration["Processors:Capricorn:APIKEY"];
            _url = _configuration["Processors:Capricorn:URL"];
            _agentId = _configuration["Processors:Capricorn:AgentId"];
            _logger = logger;

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(_url),                
            };

            _vendingRepos = vendingRepos;
            
            _httpClient.DefaultRequestHeaders.Add("x-api-key", _apikey);
        }

        public async Task<CapricornBalance> FetchBalance()
        {
            var balanceResponse = await Request<CapricornBalance>(CapricornRequests.FetchBalance);
            if(balanceResponse != null)
            {
                if(balanceResponse.Status == "success")
                {
                    return balanceResponse.Data;
                }
                
            }
            return null;
        }

        private async Task<CapriconResponse<T>> Request<T>(CapricornRequests request, string vendRef =  null, string vendorCode = null, string serviceType = null,string customerNumber = null, decimal? amount = null, int? numberOfPins = null) where T : class
        {
            var data = new Dictionary<string, string>();
            var requestURL = request.GetDescription();

            if (request != CapricornRequests.VendElectricity)
            {
                if (!string.IsNullOrEmpty(customerNumber))
                {
                    data.Add("smartcard_number", customerNumber);
                }

                if (amount.HasValue)
                {
                    data.Add("total_amount", amount.ToString());
                }

                if (!string.IsNullOrEmpty(vendorCode))
                {
                    data.Add("product_code", vendorCode);
                }
                data.Add("product_monthsPaidFor", "1");
                if (!string.IsNullOrEmpty(serviceType))
                {
                    data.Add("service_type", serviceType.ToLower());
                }

                if (!string.IsNullOrEmpty(_agentId))
                {
                    data.Add("agentId", _agentId);
                }

                if (!string.IsNullOrEmpty(vendRef))
                {
                    data.Add("agentReference", vendRef);
                }

                if (!string.IsNullOrEmpty(customerNumber))
                {
                    data.Add("account_number", customerNumber);
                }
            }
            var requestUri = QueryHelpers.AddQueryString(requestURL, data);
            _logger.LogWarning($"Capricorn Verify Account URL{requestUri}");

            HttpResponseMessage response;
            if (request == CapricornRequests.FetchBalance)
            {
                response = await _httpClient.GetAsync(requestUri);
            }
            else if(request == CapricornRequests.VendElectricity)
            {
                var electricityDetails = new VendElectricity
                {
                    Account_number = customerNumber,
                    AgentId = Convert.ToInt32(_agentId),
                    AgentReference = vendRef,
                    Amount = Convert.ToInt32(amount),
                    Phone = "08096279121",
                    Service_type = vendorCode
                };
                var requestContent = JsonConvert.SerializeObject(electricityDetails);
                var jsondata = new StringContent(requestContent, Encoding.UTF8, "application/json");
                response = await _httpClient.PostAsync(requestURL, jsondata);
            }
            else if(request == CapricornRequests.VendEPins)
            {
                var purchasePin = new VendEPins
                {
                    NumberOfPins = numberOfPins.Value,
                    AgentId = Convert.ToInt32(_agentId),
                    AgentReference = vendRef,
                    Amount =  Convert.ToInt32(amount),
                    PinValue = Convert.ToInt32(amount/numberOfPins.Value),
                    Service_type = vendorCode                    
                };
                var requestContent = JsonConvert.SerializeObject(purchasePin);
                _logger.LogWarning(requestContent);
                var jsondata = new StringContent(requestContent, Encoding.UTF8, "application/json");
                response = await _httpClient.PostAsync(requestURL, jsondata);
                
            }
            else
            {
                response = await _httpClient.PostAsync(requestUri, null);
            }
            
            
            var body = await response.Content.ReadAsStringAsync();
            _logger.Log(LogLevel.Warning,body);
            var requestResponse = JsonConvert.DeserializeObject<CapriconResponse<T>>(body);
        
            return requestResponse;
        }

        public CapriconResponse<TransactionSuccessful> VendProduct(string vendCode, ServiceType type, string serviceCode, string vendRef, string serviceType, decimal amount, string accountNumber, string accountType, out string message)
        {
            message = string.Empty;
            var balance = Task.Run(()=>FetchBalance()).Result;
            if (balance != null)
            {
                if (balance.Balance > amount)
                {
                     
                    if (type == ServiceType.CableTV)
                    {
                        var vendResponse = Task.Run(()=>Request<TransactionSuccessful>(CapricornRequests.VendMultiChoice, vendRef, serviceCode, serviceType, accountNumber, Convert.ToInt32(amount))).Result;
                        var vendingLog = _vendingRepos.FirstOrDefault(x=>x.VendingCode == vendCode);
                        vendingLog.ProcessorsPayLoad = JsonConvert.SerializeObject(vendResponse);
                        _vendingRepos.Update(vendingLog);
                        _vendingRepos.SaveChanges();
                        return vendResponse;
                    }
                    else if(type == ServiceType.EPins)
                    {
                        var vendingLog = _vendingRepos.FirstOrDefault(x=>x.VendingCode == vendCode);
                        var vendResponse = Task.Run(()=>Request<TransactionSuccessful>(CapricornRequests.VendEPins, vendRef, serviceCode, serviceType, accountNumber, Convert.ToInt32(amount), vendingLog.NumberOfPins)).Result;                        
                        vendingLog.ProcessorsPayLoad = JsonConvert.SerializeObject(vendResponse);
                        _vendingRepos.Update(vendingLog);
                        _vendingRepos.SaveChanges();
                        return vendResponse;
                    }
                    else
                    {                        
                        var vendResponse = Task.Run(()=>Request<TransactionSuccessful>(CapricornRequests.VendElectricity, vendRef, serviceCode, serviceType, accountNumber, Convert.ToInt32(amount))).Result;
                        var vendingLog = _vendingRepos.FirstOrDefault(x=>x.VendingCode == vendCode);
                        vendingLog.ProcessorsPayLoad = JsonConvert.SerializeObject(vendResponse);
                        _vendingRepos.Update(vendingLog);
                        _vendingRepos.SaveChanges();
                        return vendResponse;
                    }                    
                }
                
                message = "Transaction has failed. Please try again in 15 minutes";
                return null;
            }
            message = "Vending Failed";
            return null;
        }

        public async Task<AccountDetails> VerifyAccount(string customerNumber, string serviceType)
        {
            var accountDetails = await Request<AccountDetails>(CapricornRequests.VerifyAccountName, customerNumber: customerNumber,  serviceType: serviceType );
            if (accountDetails != null)
            {
                return accountDetails.Data;
            }
            return null;
        }
    }
}
