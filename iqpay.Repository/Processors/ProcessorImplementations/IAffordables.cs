using System;
using iqpay.Data.Entities.Payment;
using System.Collections.Generic;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public interface IAffordables:IAutoDependencyRegister
    {
        List<VendingLogs> FetchOrders(DateTime startDate, DateTime endDate, long? userId);
        VendingLogs FetchOrder(string code);         
    }
}