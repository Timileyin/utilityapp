using System;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public interface IFetsRequests:IAutoDependencyRegister
    {
        Task<FetsBalanceResponse> FetchBalance();
        Task<FetsVendResponse> VendProduct(string vendCode, string phoneNumber, string serviceCode, decimal amount, string accountNumber);        
    }
}
