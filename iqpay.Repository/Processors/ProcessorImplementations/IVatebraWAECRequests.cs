using System;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public interface IVatebraWAECRequests:IAutoDependencyRegister
    {
        Task<decimal>  FetchBalance();
        Task<VatebraResponse> VendProduct(string vendCode, decimal amount, string accountNumber);
    }
}