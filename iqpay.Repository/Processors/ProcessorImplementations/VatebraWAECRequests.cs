using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Xml.Serialization;
using ServiceReference4;
using iqpay.Data.Entities.Payment;
using System.ServiceModel;
using iqpay.Repository.Interface;
using Microsoft.Extensions.Logging;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public class VatebraWAECRequests:IVatebraWAECRequests,IDisposable
    {
        private readonly IConfiguration _configuration;
        private readonly string _dealerCode;
        private readonly string _apiKey;
        private readonly string _url; 
        private readonly ContentServiceSoapClient _contentServiceSoapClient;
        private readonly IRepository<VendingLogs> _vendingRepo;
        private readonly ILogger<VatebraEKORequests> _logger;
        private readonly string _wasp;
        private readonly string _waspEmail; 
        private readonly string _requestServiceCode;
        private readonly string _netWorkOperator;
        private readonly string _ussdCode;
        private readonly string _sessionId;


        public VatebraWAECRequests(ILogger<VatebraEKORequests> logger, IConfiguration configuration, IRepository<VendingLogs> vendingRepo)
        {
            _configuration = configuration;            
            _dealerCode = _configuration["Processors:VatebraWAEC:DealerCode"];
            _apiKey = _configuration["Processors:VatebraWAEC:APIKEY"]; 
            _url = _configuration["Processors:VatebraWAEC:URL"];
            _wasp = _configuration["Processors:VatebraWAEC:WASP"];
            _waspEmail = _configuration["Processors:VatebraWAEC:WASPEmail"];
            _requestServiceCode = _configuration["Processors:VatebraWAEC:RequestServiceCode"];
            _netWorkOperator = _configuration["Processors:VatebraWAEC:NetworkOperator"];
            _ussdCode = _configuration["Processors:VatebraWAEC:USSDCode"];
            _sessionId = _configuration["Processors:VatebraWAEC:SessionId"];
            _logger = logger;

            _contentServiceSoapClient  = new ContentServiceSoapClient(ContentServiceSoapClient.EndpointConfiguration.ContentServiceSoap12);  
            _contentServiceSoapClient.Endpoint.Binding.SendTimeout = new TimeSpan(0, 5, 0);
            _vendingRepo = vendingRepo;         
        }

        public void Dispose() 
        {
            _contentServiceSoapClient.Close();
        }

        private string GenerateHashString(string value)
        {
            return Utilities.GenerateSHA512String($"{value}{_dealerCode}");            
        }

        public async Task<decimal>  FetchBalance()
        {            
            var dealerBalanceHashString  = GenerateHashString(_dealerCode);
            var dealerBalance = await _contentServiceSoapClient.GetWaecContentServiceBalanceAsync(_wasp, _waspEmail, _dealerCode, _apiKey, _requestServiceCode);
            var balanceStr = dealerBalance.Body.GetWaecContentServiceBalanceResult.ResultItem;
            _logger.LogWarning($"Vatebra WAEC Balance => {dealerBalance.Body.GetWaecContentServiceBalanceResult.ResultItem}");
            var balanceIsDecimal = decimal.TryParse(balanceStr, out decimal balance);            
            if(!balanceIsDecimal)
            {
                _logger.LogWarning($"Vatebra WAEC Balance => {dealerBalanceHashString}{balanceStr}, {dealerBalance.Body.GetWaecContentServiceBalanceResult}");
                _logger.LogWarning("Fetch Balance has failed");
                throw new Exception($"{balanceStr} Fetch Balance has failed");
            }
            else
            {
                return balance;
            }
        }
        
        public async Task<VatebraResponse> VendProduct(string vendCode, decimal amount, string accountNumber)
        {
            var hashString = GenerateHashString(accountNumber);
            var balance = await FetchBalance();
            
            if(balance > amount)
            {
                var vendResult = await _contentServiceSoapClient.GetWaecPINServiceContentAsync(accountNumber, _netWorkOperator, _wasp, _waspEmail, _ussdCode, _dealerCode, _apiKey, _sessionId, _requestServiceCode);   

                var vendingLog = _vendingRepo.FirstOrDefault(x=>x.VendingCode == vendCode);
                
                vendingLog.ProcessorsPayLoad = JsonConvert.SerializeObject(vendResult.Body.GetWaecPINServiceContentResult.ResultItem);
                _vendingRepo.Update(vendingLog);
                _vendingRepo.SaveChanges();
                _logger.LogWarning(vendResult.Body.GetWaecPINServiceContentResult.ResultItem);
                if(!vendResult.Body.GetWaecPINServiceContentResult.ResultItem.StartsWith("00"))
                {
                    return new VatebraResponse
                    {
                        Status = false,
                        Message = vendResult.Body.GetWaecPINServiceContentResult.ResultItem
                    };
                }else
                {
                    return new VatebraResponse
                    {
                        Status = true,
                        Message = vendResult.Body.GetWaecPINServiceContentResult.ResultItem
                    };
                }                                 
            }
            else
            {
                return new VatebraResponse{
                    Status = false,
                    Message = "Transaction has failed. Please try again in 15 minutes"
                };
            }
            
        }

        private string GenerateSoapXML<T>(T request) where T: class
        {
            var requestXml = "";
            using(var stringwriter = new System.IO.StringWriter())
            { 
                var serializer = new XmlSerializer(this.GetType());
                serializer.Serialize(stringwriter, this);
                requestXml = serializer.ToString();

                var xmlString = String.Format(@"<?xml version=""1.0"" encoding=""utf-8""?>
                <soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">
                    <soap12:Body>
                        {0}
                    </soap12:Body>
                </soap12:Envelope>", requestXml);

                return xmlString;
            }
        }
    }
    
}