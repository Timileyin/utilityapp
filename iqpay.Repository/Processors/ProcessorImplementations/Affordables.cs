using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.Interface;
using Microsoft.Extensions.Logging;
using System.Linq;
using iqpay.Data.Entities.Services;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public class Affordables:IAffordables
    {
        private readonly IRepository<VendingLogs> _repo;
        public Affordables(IRepository<VendingLogs> repo)
        {
            _repo = repo;
        }

        public List<VendingLogs> FetchOrders(DateTime startDate, DateTime endDate, long? userId = null)
        {            
            var orders = _repo.GetAll().Include(x=>x.Processor).Include(x=>x.Service.Vendor).Where(x=>x.Processor.Code == ProcessorsEnum.IQIPAYSHIPPING.GetDescription() && (x.UserId == userId || userId == null) && x.DateCreated >= startDate && x.DateCreated <= endDate);
            return orders.ToList();
        }

        public VendingLogs FetchOrder(string code)
        {
            var order = _repo.GetAll().Include(x=>x.Processor).Include(x=>x.Service.Vendor).FirstOrDefault(x=> x.VendingCode == code);
            return order;
        }    
    }
}