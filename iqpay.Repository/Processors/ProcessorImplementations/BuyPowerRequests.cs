using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.Interface;
using Microsoft.Extensions.Logging;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public class BuyPowerRequests:IBuyPowerRequests
    {
        public IConfiguration _config;
        public string _apiKey;
        public string _url;
        public HttpClient _httpClient;
        public ILogger<BuyPowerRequests> _logger;
        public IRepository<VendingLogs> _vendingRepos;

        public BuyPowerRequests(IConfiguration config, ILogger<BuyPowerRequests> logger, IRepository<VendingLogs> vendingRepos)
        {
            _config = config;
            _apiKey = _config["Processors:BuyPower:APIKEY"];
            _url = _config["Processors:BuyPower:URL"];
            _logger = logger;
            _vendingRepos = vendingRepos;

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(_url),  
            };
            _httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {_apiKey}");
        }

        public async Task<BuyPowerVerify> VerifyAccount(string meter, string disco, string vendType, bool orderId)
        {
            try{
                var account = await Request<BuyPowerVerify>(HttpMethod.Get, $"check/meter?meter={meter}&disco={disco}&vendType={vendType}");
                return account;
            }catch(Exception ex){
                _logger.LogError(ex, ex.Message);
                return null;
            }            
        }

        public async Task<BuyPowerBalance> FetchBalance()
        {
            var balance = await Request<BuyPowerBalance>(HttpMethod.Get, "wallet/balance");
            return balance;
        }

        private async Task<T> Request<T>(HttpMethod method,  string endpoint)where T:class
        {
            return await Request<T, Object>(method, endpoint);
        }

        private async Task<T> Request<T, U>(HttpMethod method, string endpoint, U request = null ) where T : class where U:class
        {
            var responseBody = new HttpResponseMessage();
            if(method == HttpMethod.Post)
            {
                var postBody = JsonConvert.SerializeObject(request);
                var jsondata = new StringContent(postBody, Encoding.UTF8, "application/json");
                 _logger.Log(LogLevel.Warning,$"BuyPower Response({endpoint}=>: {responseBody})"); 
                responseBody = await _httpClient.PostAsync(endpoint, jsondata);
                
            }
            else
            {
                responseBody = await _httpClient.GetAsync(endpoint);
                 _logger.Log(LogLevel.Warning,$"BuyPower Response({endpoint}=>: {responseBody})");
            }
            var responseContent = await responseBody.Content.ReadAsStringAsync();
            if(responseBody.IsSuccessStatusCode)
            {           
                _logger.Log(LogLevel.Warning,$"BuyPower Response({endpoint}=>: {responseBody})");     
                T response = JsonConvert.DeserializeObject<T>(responseContent); 
                return response;
            }
            else
            {
                throw new Exception($"BuyPower Request has failed with this response {responseContent}");
            }
            
        }

        public async Task<BuyPowerVendResponse> VendProduct(string email, string username, string phoneNumber, string vendCode, string serviceCode, decimal amount, string accountNumber)
        {
            var balance = await FetchBalance();
            if(balance.Balance >= amount)
            {
                var vendResult = new VendResult();
                var serviceCodeSplit = serviceCode.Split('-');
                var requestBody = new BuyPowerVendRequest
                {
                    Meter = accountNumber,
                    Disco = serviceCodeSplit[0],
                    VendType = serviceCodeSplit[1],
                    Amount = amount,
                    Email = email,
                    OrderId = vendCode,
                    PaymentType = "ONLINE",
                    Phone = phoneNumber,
                    Name = username
                };
                var vend = await Request<BuyPowerVendResponse, BuyPowerVendRequest>(HttpMethod.Post, "vend", requestBody );
                if(vend.Status)
                {
                    var vendingLog = _vendingRepos.FirstOrDefault(x=>x.VendingCode == vendCode);
                    vendingLog.ProcessorsPayLoad = JsonConvert.SerializeObject(vend);
                    _vendingRepos.Update(vendingLog);
                    _vendingRepos.SaveChanges();
                    return vend;
                }
            }

            throw new Exception($"BuyPower Balance too low: {balance}" );
        }       

    }
}

