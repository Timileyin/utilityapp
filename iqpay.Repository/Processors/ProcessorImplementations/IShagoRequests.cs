using System;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public interface IShagoRequests:IAutoDependencyRegister
    {
        Task<ShagoWallet> FetchBalance();
        Task<ShageVerificationResponse> VerifyAccount(string accountNo, string serviceCode);
        Task<ShagoVendResponse> VendProduct(string email, string username, string phoneNumber, string vendCode, string serviceCode, decimal amount, string accountNumber);
    }
}
