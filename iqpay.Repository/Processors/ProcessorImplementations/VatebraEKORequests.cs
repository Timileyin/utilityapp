using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Xml.Serialization;
using ServiceReference2;
using iqpay.Data.Entities.Payment;
using System.ServiceModel;
using iqpay.Repository.Interface;
using Microsoft.Extensions.Logging;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public class VatebraEKORequests:IVatebraEKORequests,IDisposable
    {
        private readonly IConfiguration _configuration;
        private readonly string _dealerCode;
        private readonly string _apiKey;
        private readonly string _url; 
        private readonly vproxySoapClient _vproxySoap;
        private readonly IRepository<VendingLogs> _vendingRepo;
        private readonly ILogger<VatebraEKORequests> _logger;

        public VatebraEKORequests(ILogger<VatebraEKORequests> logger, IConfiguration configuration, IRepository<VendingLogs> vendingRepo)
        {
            _configuration = configuration;            
            _dealerCode = _configuration["Processors:VatebraEKO:DealerCode"];
            _apiKey = _configuration["Processors:VatebraEKO:APIKEY"]; 
            _url = _configuration["Processors:VatebraEKO:URL"];
            _logger = logger;

            _vproxySoap  = new vproxySoapClient(vproxySoapClient.EndpointConfiguration.vproxySoap12);  
            _vendingRepo = vendingRepo;         
        }

        private string GenerateHashString(string value)
        {
            return Utilities.GenerateSHA512String($"{value}{_dealerCode}");            
        }

        public async Task<VatebraResponse> FetchUser(string meterNo)
        {
            var hashString = GenerateHashString(meterNo);
            var verifyResult = await _vproxySoap.FetchCustAsync(meterNo, hashString, _apiKey);
            if(verifyResult.Body.FetchCustResult.StartsWith("00"))
            {
                if(verifyResult.Body.FetchCustResult.Contains("Account/Meter No. not found"))
                {
                    return new VatebraResponse
                    {
                        Status = false,
                        Message = verifyResult.Body.FetchCustResult
                    };
                }else
                {
                    var resultSplit = verifyResult.Body.FetchCustResult.Split(',');
                    var message = $"{resultSplit[2]} && {resultSplit[7]}";

                    return new VatebraResponse
                    {
                        Status = true,
                        Message = message
                    };
                }
            }else
            {
                return new VatebraResponse
                {
                    Status = false,
                    Message = verifyResult.Body.FetchCustResult
                };
            }
            
        }

        public void Dispose() 
        {
            _vproxySoap.Close();
        }
        

        public async Task<decimal>  FetchBalance()
        {            
            var dealerBalanceHashString  = GenerateHashString(_dealerCode);
            var dealerBalance = await _vproxySoap.FetchDealerBalanceAsync(_dealerCode, dealerBalanceHashString, _apiKey);
            var balanceStr = dealerBalance.Body.FetchDealerBalanceResult.Substring(3);
            _logger.LogWarning($"Vatebra EKO Balance => {dealerBalanceHashString}{balanceStr}, {dealerBalance.Body.FetchDealerBalanceResult}");
            var balanceIsDecimal = decimal.TryParse(balanceStr, out decimal balance);            
            if(!balanceIsDecimal)
            {
                 _logger.LogWarning($"Vatebra EKO Balance => {dealerBalanceHashString}{balanceStr}, {dealerBalance.Body.FetchDealerBalanceResult}");
                _logger.LogWarning("Fetch Balance has failed");
                throw new Exception($"{balanceStr} Fetch Balance has failed");
            }else
            {
                return balance;
            }
        }
        
        public async Task<VatebraResponse> VendProduct(string vendCode, decimal amount, string accountNumber)
        {
            var hashString = GenerateHashString(accountNumber);
            var balance = await FetchBalance();
            
            if(balance > amount)
            {
                var vendResult = await _vproxySoap.PostTransactionAsync(accountNumber, amount, hashString, _apiKey);   

                var vendingLog = _vendingRepo.FirstOrDefault(x=>x.VendingCode == vendCode);
                
                vendingLog.ProcessorsPayLoad = JsonConvert.SerializeObject(vendResult.Body.PostTransactionResult);
                _vendingRepo.Update(vendingLog);
                _vendingRepo.SaveChanges();
                _logger.LogWarning(vendResult.Body.PostTransactionResult);
                if(!vendResult.Body.PostTransactionResult.StartsWith("00_Successful"))
                {
                    return new VatebraResponse
                    {
                        Status = false,
                        Message = vendResult.Body.PostTransactionResult
                    };
                }else
                {
                    return new VatebraResponse
                    {
                        Status = true,
                        Message = vendResult.Body.PostTransactionResult
                    };
                }                                 
            }
            else
            {
                return new VatebraResponse{
                    Status = false,
                    Message = "Transaction has failed. Please try again in 15 minutes"
                };
            }
            
        }

        private string GenerateSoapXML<T>(T request) where T: class
        {
            var requestXml = "";
            using(var stringwriter = new System.IO.StringWriter())
            { 
                var serializer = new XmlSerializer(this.GetType());
                serializer.Serialize(stringwriter, this);
                requestXml = serializer.ToString();

                var xmlString = String.Format(@"<?xml version=""1.0"" encoding=""utf-8""?>
                <soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">
                    <soap12:Body>
                        {0}
                    </soap12:Body>
                </soap12:Envelope>", requestXml);

                return xmlString;
            }
        }
    }
    
}