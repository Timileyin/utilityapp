using System;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using iqpay.Data.Entities.Payment;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public class GreyStoneRequests:IGreyStoneRequests
    {
        private readonly IConfiguration _config;
        private readonly string _agentId;
        private readonly string _agentKey;
        private readonly string _url;
        private readonly string _email;
        private readonly HttpClient _httpClient;
        private readonly ILogger<GreyStoneRequests> _logger;
        private readonly IRepository<VendingLogs> _vendingRepos;
        private readonly DateTime _lastSessionTime;
        private readonly string _signature;

        public GreyStoneRequests(IConfiguration config, ILogger<GreyStoneRequests> logger, IRepository<VendingLogs> vendingRepos)
        {
            _config = config;
            _agentKey = _config["Processors:GREYSTONE:AgentKey"];
            _url = _config["Processors:GREYSTONE:URL"];
            _agentId = _config["Processors:GREYSTONE:AgentId"];
            _email = _config["Processors:GREYSTONE:Email"];
            _logger = logger;
            _vendingRepos = vendingRepos;
            _signature = Utilities.GenerateSHA512String($"{_agentId}{_agentKey}{_email}");

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(_url),  
            };
           // _httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {_apiKey}");
        }

        private async Task<T> Request<T, U>(HttpMethod method, string endpoint, U request = null ) where T : class where U:class
        {
            var responseBody = new HttpResponseMessage();
            if(method == HttpMethod.Post)
            {
                var postBody = JsonConvert.SerializeObject(request);
                postBody = postBody.Replace("merchantFK", "MerchantFK");
                _logger.Log(LogLevel.Warning, postBody);
                var jsondata = new StringContent(postBody, Encoding.UTF8, "application/json");
                responseBody = await _httpClient.PostAsync(endpoint, jsondata);                
            }
            else
            {
                responseBody = await _httpClient.GetAsync(endpoint);
            }
            var responseContent = await responseBody.Content.ReadAsStringAsync();
            if(responseBody.IsSuccessStatusCode)
            {           
                _logger.Log(LogLevel.Warning,$"GreyStone Response({endpoint}=>: {responseContent})");     
                T response = JsonConvert.DeserializeObject<T>(responseContent); 
                return response;
            }
            else
            {
                _logger.Log(LogLevel.Warning,$"GreyStone Response({endpoint}=>: {responseBody})");
                throw new Exception($"GreyStone Request has failed with this response {responseContent}");
            }
        }

        private async Task<T> Request<T>(HttpMethod method,  string endpoint)where T:class
        {
            return await Request<T, Object>(method, endpoint);
        }

        public async Task<GreyStoneSessionResponse> FetchSession()
        {            
            var sessionReq = await Request<GreyStoneSessionResponse>(HttpMethod.Get, $"AccountTransact/GetSessionId?agentId={_agentId}&agentKey={_agentKey}&signature={_signature}");
            return sessionReq;            
        }

        public async Task<VerifyUserResponse> FetchUser(string merchantFK, string serviceType, string customerNo )
        {
            var session = await FetchSession();
            if(!string.IsNullOrEmpty(session.SessionId))
            {
                _httpClient.DefaultRequestHeaders.Clear();
                _httpClient.DefaultRequestHeaders.Add("agentID", _agentId);
                _httpClient.DefaultRequestHeaders.Add("agentKey", _agentKey);
                _httpClient.DefaultRequestHeaders.Add("signature", _signature);
                _httpClient.DefaultRequestHeaders.Add("sessionid", session.SessionId);
            
                var reqBody = new GreyStoneVendRequest{
                    MerchantFK = merchantFK,
                    CustomerId = customerNo,
                    AccountType  = serviceType,
                    HashValue =  Utilities.GenerateSHA512String($"{_agentId}{_agentKey}{customerNo}")
                };

                var request  = await Request<VerifyUserResponse, GreyStoneVendRequest>(HttpMethod.Post, "PowerTransact/ValidateCustomerID", reqBody);
                if(request.RespCode == "00")
                {
                    return request;
                }

            }
            return null;         
        }

        public async Task<GreyStoneBalance> FetchBalance()
        {
            var balanceReq = await Request<GreyStoneBalance>(HttpMethod.Get, $"AccountTransact/GetBalance?agentId={_agentId}&agentKey={_agentKey}&emailAddress={_email}");
            if(balanceReq.ResponseCode == "00")
            {
                return balanceReq;
            }

            throw new Exception("Failed to fetch balance");
            
        }

        public async Task<GreyStoneVendResponse> VendProduct(string vendCode,string customerName, string serviceCode, decimal amount, string accountNumber, string phoneNumber)
        {
            var balance = await FetchBalance();
            if(balance.Balance > amount)
            {
                var session = await FetchSession();
                if(!string.IsNullOrEmpty(session.SessionId))
                {
                    _httpClient.DefaultRequestHeaders.Clear();
                    _httpClient.DefaultRequestHeaders.Add("agentID", _agentId);
                    _httpClient.DefaultRequestHeaders.Add("agentKey", _agentKey);
                    _httpClient.DefaultRequestHeaders.Add("signature", _signature);
                    _httpClient.DefaultRequestHeaders.Add("sessionid", session.SessionId);

                    var splitCode = serviceCode.Split("-");
                    var req = new GreyStoneVendRequest
                    {                    
                        RefNumber = vendCode,
                        MerchantFK = splitCode[0],
                        HashValue = Utilities.GenerateSHA512String($"{accountNumber}{Convert.ToInt32(amount)}{_agentId}{_agentKey}"),
                        CustomerName = customerName,                    
                        CustomerId = accountNumber,
                        Amount = Convert.ToInt32(amount),
                        AccountType = splitCode[1],
                        PhoneNumber = phoneNumber                                       
                    };

                    _logger.Log(LogLevel.Warning, JsonConvert.SerializeObject(req));
                    var vend = await Request<GreyStoneVendResponse, GreyStoneVendRequest>(HttpMethod.Post, "PowerTransact/BuyPower", req);
                    if(vend.RespDescription == "success")
                    {
                        var vendingLog = _vendingRepos.FirstOrDefault(x=>x.VendingCode == vendCode);
                        vendingLog.ProcessorsPayLoad = JsonConvert.SerializeObject(vend);
                        _vendingRepos.Update(vendingLog);
                        _vendingRepos.SaveChanges();
                        return vend;
                    }
                    throw new Exception($"Grey stone vending failed");
                }else{
                    throw new Exception("Grey stone failed to fetch session Id");
                }                
            }

            throw new Exception($"Grey stone Balance too low {balance.Balance}");                        
        }        
    }
}
