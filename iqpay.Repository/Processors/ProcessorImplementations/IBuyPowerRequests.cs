using System.Threading.Tasks;
using iqpay.Core.Utilities;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public interface IBuyPowerRequests:IAutoDependencyRegister
    {
        Task<BuyPowerBalance> FetchBalance();
        Task<BuyPowerVerify> VerifyAccount(string meter, string disco, string vendType, bool orderId);
        Task<BuyPowerVendResponse> VendProduct(string email, string username, string phoneNumber, string vendCode, string serviceCode, decimal amount, string accountNumber);

    }
}

