﻿using System;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public interface ICapriconRequests:IAutoDependencyRegister
    {
        Task<CapricornBalance> FetchBalance();
        CapriconResponse<TransactionSuccessful> VendProduct(string vendCode,ServiceType type, string serviceCode, string vendRef, string VendProduct, decimal amount, string accountNumber, string accountType, out string message);
        Task<AccountDetails> VerifyAccount(string customerNumber, string serviceType);
    }
}
