using System;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using Microsoft.Extensions.Logging;
using iqpay.Data.Entities.Payment;
using System.Text;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public class FetsRequests:IFetsRequests
    {
        private readonly IConfiguration _config;
        private readonly string _apiKey;
        private readonly string _url;
        private readonly string _merchantId;
        private readonly string _password;
        private readonly HttpClient _httpClient;
        private readonly string _username;
        private readonly ILogger<BuyPowerRequests> _logger;
        public IRepository<VendingLogs> _vendingRepos; 

        public FetsRequests(IConfiguration config)
        {
            _config = config;
            _url = _config["Processors:Fets:URL"];
            _apiKey = _config["Processors:Fets:APIKEY"];
            _password = _config["Processors:Fets:Password"];
            _merchantId = _config["Processors:Fets:MerchantId"];
            _username = _config["Processors:Fets:Username"];

            _httpClient = new HttpClient{
                BaseAddress = new Uri(_url),
            };

            var sha512Key = Utilities.GenerateSHA512String($"username:{_username}:password:{_password}"); 
            _httpClient.DefaultRequestHeaders.Add("Authorization",sha512Key);
        }

        private async Task<T> Request<T, U>(HttpMethod method, string endpoint, U request = null ) where T : class where U:class
        {
            var responseBody = new HttpResponseMessage();
            if(method == HttpMethod.Post)
            {
                var postBody = JsonConvert.SerializeObject(request);
                var jsondata = new StringContent(postBody, Encoding.UTF8, "application/json");
                responseBody = await _httpClient.PostAsync(endpoint, jsondata);                
            }
            else
            {
                responseBody = await _httpClient.GetAsync(endpoint);
            }
            var responseContent = await responseBody.Content.ReadAsStringAsync();
            if(responseBody.IsSuccessStatusCode)
            {           
                _logger.Log(LogLevel.Warning,$"Fets Response({endpoint}=>: {responseBody})");     
                T response = JsonConvert.DeserializeObject<T>(responseContent); 
                return response;
            }
            else
            {
                throw new Exception($"Fets Request has failed with this response {responseContent}");
            }
            
        }

        private string GenerateHashString(string value)
        {
            return Utilities.GenerateSHA512String($"{value}{_apiKey}");            
        }

        public async Task<FetsBalanceResponse> FetchBalance()
        {
            var req = new FetsBalanceRequest{
                MerchantId = _merchantId,
                EnKey = GenerateHashString(_merchantId)
            }; 

            var balReq = await Request<FetsBalanceResponse, FetsBalanceRequest>(HttpMethod.Post, "fetsCheckBalance", req);
            return balReq;
        }
        
        public async Task<FetsVendResponse> VendProduct(string vendCode, string phoneNumber, string serviceCode, decimal amount, string accountNumber)
        {
            var balance = await FetchBalance();
            if(balance.Balance >= amount)
            {
                var splitCode = serviceCode.Split("-");
                var req = new FetsVendRequest
                {
                    MerchantId = _merchantId,
                    EnKey = GenerateHashString($"{_merchantId}{amount}{accountNumber}{splitCode[1]}{vendCode}"),
                    Amount = amount,
                    BillerId = Convert.ToInt64(splitCode[0]),
                    ProductId = Convert.ToInt64(splitCode[1]),
                    CustomerRefNum = accountNumber,
                    CustomerPhone = phoneNumber,
                    OrderId = vendCode,
                    CustomerId = ""
                };

                var vend = await Request<FetsVendResponse, FetsVendRequest>(HttpMethod.Post, "fetsBillsPayment", req);
                if(vend.Success)
                {
                    var vendingLog = _vendingRepos.FirstOrDefault(x=>x.VendingCode == vendCode);
                    vendingLog.ProcessorsPayLoad = JsonConvert.SerializeObject(vend);
                    _vendingRepos.Update(vendingLog);
                    _vendingRepos.SaveChanges();
                    return vend;
                }    
            }
            throw new Exception("Fets balance too low");
        }
        
    }
}
