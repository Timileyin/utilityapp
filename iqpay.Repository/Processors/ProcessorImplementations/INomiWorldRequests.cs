﻿using System;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public interface INomiWorldRequests:IAutoDependencyRegister
    {
        Task<GetBalanceResult> FetchBalance();
        NomiResponse<Object> VendProduct(string vendCode, string serviceCode, decimal amount, string accountNumber, string accountType, out string message);
    }
}
