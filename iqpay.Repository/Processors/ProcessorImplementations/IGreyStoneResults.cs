using System;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public interface IGreyStoneRequests:IAutoDependencyRegister
    {
        Task<GreyStoneBalance> FetchBalance();
        Task<VerifyUserResponse> FetchUser(string merchantFK, string serviceType, string customerNo );
        Task<GreyStoneVendResponse> VendProduct(string vendCode,string customerName, string serviceCode, decimal amount, string accountNumber,string phoneNumber);      
    }
}
