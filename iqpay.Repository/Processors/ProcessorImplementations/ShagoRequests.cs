using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.Interface;
using Microsoft.Extensions.Logging;

namespace iqpay.Repository.Processors.ProcessorImplementations
{
    public class ShagoRequests : IShagoRequests 
    {
        private readonly string _password;
        private readonly string _username;
        private readonly string _hashKey;
        private readonly string _url;
        private readonly IRepository<VendingLogs> _vendingRepo; 
        private readonly IConfiguration _configuration;
        private readonly HttpClient _httpClient;
        private readonly ILogger<ShagoRequests> _logger;

        public ShagoRequests(IConfiguration configuration, IRepository<VendingLogs> vendingRepo, ILogger<ShagoRequests> logger)
        {
            _logger = logger;
            _configuration = configuration;
            _vendingRepo = vendingRepo;
            _password = _configuration["Processors:Shago:Password"];//GetSection("Processors.NomiWorld.Password").Value;
            _username = _configuration["Processors:Shago:Email"];
            _hashKey = _configuration["Processors:Shago:HashKey"];
            _url = _configuration["Processors:Shago:URL"];

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(_url),               
            };
            _httpClient.DefaultRequestHeaders.Add("email", _username);
            _httpClient.DefaultRequestHeaders.Add("password", _password);
            _httpClient.DefaultRequestHeaders.Add("hashKey", _hashKey);
        }
        public async Task<ShagoWallet> FetchBalance()        
        {
            var serviceCodeReq = new ShagoRequest{
                ServiceCode = "BAL"
            };
            var response = await Request<ShagoResponse,ShagoRequest>(HttpMethod.Post, "", serviceCodeReq);
            return response.Wallet;
        }

        public async Task<ShageVerificationResponse> VerifyAccount(string accountNo, string serviceCode)
        {
            var vendorDetails = serviceCode.Split("-");
            var verifyAccount = new ShagoRequest{
                ServiceCode = "AOV",
                Disco = vendorDetails[0],
                MeterNo = accountNo,
                Type = vendorDetails[1]
            };
            var response = await Request<ShageVerificationResponse, ShagoRequest>(HttpMethod.Post, "", verifyAccount);
            if(response.Status == "200")
            {
                return response;
            }
            throw new Exception("Shago verification has failed");
        }

        public async Task<ShagoVendResponse> VendProduct(string email, string username, string phoneNumber, string vendCode, string serviceCode, decimal amount, string accountNumber)
        {
            var balance = await FetchBalance();
            if(balance.PrimaryBalance >= amount)
            {
                var vendorDetails = serviceCode.Split("-");

                var vendReq = new ShagoRequest{
                    ServiceCode = "AOB",
                    Amount = amount,
                    Phonenumber = phoneNumber,
                    Address = "",
                    Disco =  vendorDetails[0],
                    Type = vendorDetails[1],
                    MeterNo = accountNumber,
                    Name = username,
                    Request_id = vendCode                    
                };

                var response = await Request<ShagoVendResponse, ShagoRequest>(HttpMethod.Post, "", vendReq);
                var vendingLog = _vendingRepo.FirstOrDefault(x=>x.VendingCode == vendCode);
                vendingLog.ProcessorsPayLoad = JsonConvert.SerializeObject(response);
                _vendingRepo.Update(vendingLog);
                _vendingRepo.SaveChanges();
                return response;
            }
            throw new Exception($"Balance too low: {balance}" );            
        }

        private async Task<T> Request<T>(HttpMethod method,  string endpoint)where T:class
        {
            return await Request<T, Object>(method, endpoint);
        }

        private async Task<T> Request<T, U>(HttpMethod method, string endpoint, U request = null ) where T : class where U:class
        {
            var responseBody = new HttpResponseMessage();
            if(method == HttpMethod.Post)
            {
                var postBody = JsonConvert.SerializeObject(request);
                _logger.LogWarning($"Shago Request: {postBody}");
                var jsondata = new StringContent(postBody, Encoding.UTF8, "application/json");
                responseBody = await _httpClient.PostAsync(endpoint, jsondata);                
            }
            else
            {
                responseBody = await _httpClient.GetAsync(endpoint);
            }
            var responseContent = await responseBody.Content.ReadAsStringAsync();
            if(responseBody.IsSuccessStatusCode)
            {                           
                _logger.Log(LogLevel.Warning,$"Shago Response({endpoint}=>: {responseContent})");  
                T response = JsonConvert.DeserializeObject<T>(responseContent);                                    
                return response;
            }
            else
            {
                _logger.Log(LogLevel.Warning,$"Shago Response({endpoint}=>: {responseBody})");     
                T response = JsonConvert.DeserializeObject<T>(responseContent); 
                throw new Exception($"Shago Request has failed with this response {responseContent}");
            }          
        }
        public NomiResponse<Object> VendProduct(string vendCode, string serviceCode, decimal amount, string accountNumber, string accountType, out string message)
        {
            message = "";
            return null;
        }
    }
}