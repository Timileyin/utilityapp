﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Processors
{
    public interface IProcessorRepository:IAutoDependencyRegister
    {
        Task<List<Processor>> FetchProcessors();
        Task<Processor> GetProcessor(long id);
        Task<Processor> GetProcessorByCode(string code);
        Task CreateProcessor(Processor processor);
        Task UpdateProcessor(Processor processor);
    }
}
