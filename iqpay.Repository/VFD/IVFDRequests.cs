using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;
using iqpay.Data.UserManagement.Model;
using iqpay.Repository.VFD.Models;

namespace iqpay.Repository.VFD
{
    public interface IVFDRequests:IAutoDependencyRegister
    {
        Task<TokenModel> Authenticate();
        Task<TransactionStatusResponse> FetchTransaction(string reference);
        Task<VerifyBVN> VerifyBVN(string bvn);
        Task<BankLists> FetchBanks();
        Task<NameEnquiryResponse> NameEnquiry(string accountNo, string bankCode);
        Task<TransferResponse> CompleteOutwardTransfer(string txnRef, decimal amount, string account,string bankCode,string toBvn, string toClient, string toClientId, string toSession, string toSavingsId, string narration);
        Task<GenerateAccount> ReserveAccount(string firstName, string lastName, DateTime? dob, string address, string mobileNum, string bvn);       
    }
}
