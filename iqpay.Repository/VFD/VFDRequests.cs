using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Repository.VFD.Models;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System;
using System.Text;
using Microsoft.Extensions.Logging;
using iqpay.Core.Utilities;

namespace iqpay.Repository.VFD
{    
    public class VFDRequests : IVFDRequests
    {
        private readonly string _secret;
        private readonly string _consumerKey;
        private readonly string _url;
        private readonly IConfiguration _configuration;
        private readonly HttpClient _httpClient;
        private readonly ILogger<VFDRequests> _logger;
        private readonly string _base64Token;
        private readonly string _username;
        private readonly string _password;
        private readonly string _clientId;
        private readonly string _refPrefix;
        private readonly string _accountNumber;
        private readonly string _clientName;
        private readonly string _accessToken;
        private readonly string _walletCredentials;
        private readonly string _bvn;
        private readonly string _accountId;
        public VFDRequests(ILogger<VFDRequests> logger, IConfiguration config)
        {
            _configuration = config;
            _consumerKey = _configuration["VFD:ConsumerKey"];
            _secret = _configuration["VFD:Secret"];
            _url = _configuration["VFD:URL"];
            _username = _configuration["VFD:Username"];
            _password = _configuration["VFD:Password"];
            _accessToken = _configuration["VFD:AccessToken"];
            _walletCredentials = _configuration["VFD:WalletCredentials"];
            _clientId = _configuration["VFD:ClientId"];
            _clientName = _configuration["VFD:ClientName"];
            _bvn = _configuration["VFD:BVN"];
            _refPrefix = _configuration["VFD:RefPrefix"];
            _accountNumber = _configuration["VFD:AccountNumber"];
            _accountId = _configuration["VFD:FromSavingsId"];
            _logger = logger;

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(_url),
            };
        }

        public async Task<TokenModel> Authenticate()
        {
            var reqBody = new Dictionary<string, string>();
            reqBody.Add("username", _username);
            reqBody.Add("password", _password);
            var authResponse = await Request<TokenModel>(false, HttpMethod.Post, "token", reqBody);
            return authResponse;            
        }

        public async Task<VerifyBVN> VerifyBVN(string bvn)
        {
            var req = await Request<VFDResponse<VerifyBVN>>(true, HttpMethod.Get, $"wallet2/client?bvn={bvn}&wallet-credentials={_walletCredentials}");
            if(req.Status == "00")
            {
                return req.Data;
            }
            throw new Exception("Error occured while fetch transaction");
        }

        public async Task<BankLists> FetchBanks()
        {
            var req = await Request<VFDResponse<BankLists>>(true, HttpMethod.Get, "wallet2/bank");
            if(req.Status == "00"){
                return req.Data;
            }

            throw new Exception("Error occured while fetching bank list");
        }
        public async Task<TransactionStatusResponse> FetchTransaction(string txnRef)
        {
            var req = await Request<VFDResponse<TransactionStatusResponse>>(true, HttpMethod.Get, $"wallet2/transactions?reference={txnRef}&wallet-credentials={_walletCredentials}");
            _logger.LogInformation($"VFD Responnse: {JsonConvert.SerializeObject(req)}");
            if(req.Status == "00")
            {
                return req.Data;
            }
            throw new Exception("Error occured while fetch transaction ");
        }

        public async Task<NameEnquiryResponse> NameEnquiry(string accountNo, string bankCode)
        {
            var transferType = "inter";
            if(bankCode == _configuration["VFD:VFDCode"]) 
            {
                transferType = "intra";
            }

            var nameEnquiry = await Request<VFDResponse<NameEnquiryResponse>>(true, HttpMethod.Get, $"wallet2/transfer/recipient?accountNo={accountNo}&transfer_type={transferType}&bank={bankCode}&wallet-credentials={_walletCredentials}");
            if(nameEnquiry.Status == "00")
            {
                return nameEnquiry.Data;
            }

            throw new Exception("Name Enquiry has failed");
        }

        public async Task<TransferResponse> CompleteOutwardTransfer(string txnRef, decimal amount, string account,string bankCode,string toBvn, string toClient, string toClientId, string toSession, string toSavingsId, string narration)
        {
            var reqBody = new VFDTransfer
            {
                Amount = amount,
                FromAccount = _accountNumber,
                FromClientId = _clientId,
                FromClient = _clientName,
                FromBvn = _bvn,
                FromSavingsId = _accountId,
                Reference = txnRef,
                Remark = $"IQPAY Transfer - {narration}",
                Signature = Utilities.GenerateSHA512String($"{_accountNumber}{account}").ToLower(),
                ToAccount = account,
                ToBank = bankCode,
                ToBvn = toBvn,
                ToClient = toClient,
                ToClientId = toClientId,
                ToKyc = "99",
                ToSession = toSession,
                ToSavingsId = toSavingsId,
                TransferType = bankCode == _configuration["VFD:VFDCode"] ? "intra" : "inter"                
            };

            var req = await Request<VFDResponse<TransferResponse>>(true, HttpMethod.Post, $"wallet2/transfer?wallet-credentials={_walletCredentials}", reqBody);
            if(req.Status == "00")
            {
                return req.Data;
                
            }

            throw new Exception("Transfer to bank has failed");
        }

        public async Task<GenerateAccount> ReserveAccount(string firstName, string lastName, DateTime? dob, string address, string mobileNum, string bvn)
        {
            var reqBody = new GenerateAccountModel{
                Address = address,
                Bvn = bvn,
                Dob = dob.ToString(),
                Firstname = firstName,
                Lastname = lastName,
                Phone = mobileNum,
            };
            var body = JsonConvert.SerializeObject(reqBody);
            var req = await Request<VFDResponse<GenerateAccount>>(true, HttpMethod.Post,$"wallet2/clientdetails/create?wallet-credentials={_walletCredentials}",reqBody);
            _logger.LogInformation($"VFD Response: {JsonConvert.SerializeObject(req)}");
            if(req.Status == "00")
            {
                return req.Data;
            }
            throw new Exception("Error occured while fetching VFD Account number");        
        }
        

        private async Task<T> Request<T>(bool requiresAuth, HttpMethod method, string endpoint, object request = null) where T : new()
        {
            var response = new T();
            var responseBody = new HttpResponseMessage();
            if (requiresAuth)
            {
                _httpClient.DefaultRequestHeaders.Clear();
                _httpClient.DefaultRequestHeaders.Add("Authorization", $"Bearer {_accessToken}");
            }

            if (method == HttpMethod.Post)
            {
                var postBody = JsonConvert.SerializeObject(request);
                var jsondata = new StringContent(postBody, Encoding.UTF8, "application/json");
                _logger.Log(LogLevel.Warning, $"Request Body => {postBody}");
                responseBody = await _httpClient.PostAsync(endpoint, jsondata);
            }
            else if (method == HttpMethod.Get)
            {
                responseBody = await _httpClient.GetAsync(endpoint);
            }
            
            if (responseBody.IsSuccessStatusCode)
            {
                var body = await responseBody.Content.ReadAsStringAsync();
                _logger.Log(LogLevel.Warning, $"VFD Response({endpoint}=>: {body})");
                response = JsonConvert.DeserializeObject<T>(body);
                return response;
            }
            else
            {
                var body = await responseBody.Content.ReadAsStringAsync();
                throw new Exception($"VFD request failed with this response {body}");
            }

        }


    }
}
