using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace iqpay.Repository.VFD.Models
{

    public class VerifyBVN
    {
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string PhoneNo { get; set; }
        public string PixBase64{get;set;}
    }

    public class VFDBank
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public class TokenModel
    {
        public string Access_token { get; set; }
        public string Refresh_token { get; set; }
        public string Scope { get; set; }
        public string Token_type { get; set; }
        public int Expires_in { get; set; }
    }

    public class GenerateAccount
    {
        
        public string Firstname { get; set; }
        public string Middlename { get; set; }
        public string Lastname { get; set; }
        public string Bvn { get; set; }
        public string Phone { get; set; }
        public string Dob { get; set; }
        public string AccountNo { get; set; }
    
    }

    public class VFDHookModel
    {        
        public string Reference { get; set; }
        public string Amount { get; set; }
        public string Account_number { get; set; }
        public string Originator_account_number { get; set; }
        public string Originator_account_name { get; set; }
        public string Originator_bank { get; set; }
        public DateTime Timestamp { get; set; }    
    }

    public class TransactionStatusResponse
    {
        public string TxnId { get; set; }
        public decimal Amount { get; set; }
        public string AccountNo { get; set; }
        public string TransactionStatus { get; set; }
        public string TransactionDate { get; set; }
        public string ToBank { get; set; }
        public string FromBank { get; set; }
        public string SessionId { get; set; }
    }

    public class TransactionResponse
    {
        public string Originatoraccountnumber { get; set; }
        public decimal Amount { get; set; }
        public string Originatorname { get; set; }
        public string Narration { get; set; }
        public string Craccountname { get; set; }
        public string Paymentreference { get; set; }
        public string Bankname { get; set; }
        public string Sessionid { get; set; }
        public string Craccount { get; set; }
        public string Bankcode { get; set; }
        public string Status{get;set;}
    }

    public class BankLists
    {
        public List<VFDBank> Bank { get; set; }
    }

    public class Account
    {
        public string Id { get; set; }
        public string Number { get; set; }
    }

    public class NameEnquiryResponse
    {
        public string Name { get; set; }
        public string ClientId { get; set; }
        public string Bvn { get; set; }
        public Account Account { get; set; }
        public string Status { get; set; }
        public string Currency { get; set; }
        public string Bank { get; set; }
    }

    public class VFDNameEnquiryModel
    {
        [Required]
        public string AccountNo{get;set;}
        public string Transfer_type{get;set;}
        public string AccountName{get;set;}
        [Required]
        public string Bank{get;set;}
        public string ClientId{get;set;}
        public string BVN{get;set;}
        public string BankName{get;set;}
        public string SessionId{get;set;}
    }

    public class TransferResponse
    {
        public string TxnId { get; set; }
        public string SessionId { get; set; }
        public string Reference { get; set; }
    }

    public class VFDTransfer{
        public string FromSavingsId { get; set; }
        public decimal Amount { get; set; }
        public string ToAccount { get; set; }
        public string FromBvn { get; set; }
        public string Signature { get; set; }
        public string FromAccount { get; set; }
        public string ToBvn { get; set; }
        public string Remark { get; set; }
        public string FromClientId { get; set; }
        public string FromClient { get; set; }
        public string ToKyc { get; set; }
        public string Reference { get; set; }
        public string ToClientId { get; set; }
        public string ToClient { get; set; }
        public string ToSession { get; set; }
        public string TransferType { get; set; }
        public string ToBank { get; set; }
        public string ToSavingsId { get; set; }
    }
    public class GenerateAccountModel
    {
        public string Firstname { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }
        public string Dob { get; set; }
        public string Middlename { get; set; }
        public string Bvn { get; set; }
        public string Lastname { get; set; }
    }

    public class VFDResponse<T>
    {
        public string Status{get;set;}
        public string Message{get;set;}
        public T Data{get;set;}
    }
}