using System;
using System.Threading.Tasks;
using iqpay.Data.Entities.Notification;
using iqpay.Repository.Interface;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Text;
using Microsoft.Extensions.Logging;
using iqpay.Core.Utilities;

namespace iqpay.Repository.SMSRepository
{
    public class SMSManager:ISMSManager
    {
        private readonly IRepository<SMSLog> _smsRepo;
        private readonly HttpClient _httpClient;
        private readonly IConfiguration _config;
        private readonly string _username;
        private readonly string _subAccountPassword;
        private readonly string _subAccount;
        private readonly string _url;
        private readonly string _sender;
        private readonly ILogger<SMSManager> _logger;
        public SMSManager(ILogger<SMSManager> logger, IRepository<SMSLog> smsRepo, IConfiguration config)
        {
            _smsRepo = smsRepo;
            _config = config;
            _username = _config["SMS:VANSOUsername"];
            _url = _config["SMS:VANSOURL"];            
            _subAccount = _config["SMS:SubAccount"];
            _subAccountPassword = _config["SMS:VANSOPassword"];
            _sender = _config["SMS:VANSOSender"];
            _logger = logger;
            _httpClient = new HttpClient{
                BaseAddress = new Uri(_url),                
            };
        }
        public async Task CreateSMSLog(SMSLog sms)
        {
            await _smsRepo.InsertAsync(sms);
            await _smsRepo.SaveChangesAsync();
        }

        private async Task<string> FetchSessionId()
        {
            string loginBody = string.Empty;
            var loginResponse = await _httpClient.GetAsync($"?cmd=login&owneremail={_username}&subacct={_subAccount}&subacctpwd={_subAccountPassword}");
            if(loginResponse.IsSuccessStatusCode)
            {
                loginBody = await loginResponse.Content.ReadAsStringAsync();
                if(loginBody.StartsWith("OK"))
                {
                    var split = loginBody.Split(":");
                    return split[1].Trim();
                } 
            }

            throw new Exception($"SMS Login CMd has failed with response {loginBody}");
        }

        private string AddCountryCode(string number)
        {
            if(!number.StartsWith("234") && !number.StartsWith("+234"))
            {
                var telNo = number.TrimStart('0');
                number = $"234{telNo}";
            }

            return number.TrimStart('+');
        }

        private async Task<SMSStatusEnum> SendVansoEnum(SMSLog sms)
        {
            var smsBody = string.Empty;
            var receiver = AddCountryCode(sms.Receiver);
            var smsContent = new VANSOSMSModel{
                SMS = new VANSOSMS{
                    Dest = receiver,
                    Src = _sender,
                    Text = sms.Content
                }
            };

            var postBody = JsonConvert.SerializeObject(smsContent);
            var jsondata = new StringContent(postBody, Encoding.UTF8, "application/json");
            var authkey = Utilities.EncodeToBase64($"{_username}:{_subAccountPassword}");
            _httpClient.DefaultRequestHeaders.Add("Authorization",$"Basic {authkey}");
            var sendSMSResponse = await _httpClient.PostAsync("rest/sms/submit",jsondata);
            if(sendSMSResponse.IsSuccessStatusCode)
            {
                var body = await sendSMSResponse.Content.ReadAsStringAsync();
                _logger.Log(LogLevel.Warning,$"Vanso Response: {body}");
                var response = JsonConvert.DeserializeObject<VANSOResponse>(body);
                smsBody = body;
                if(response.Status == "ACCEPTED")
                {
                    return SMSStatusEnum.Sent;
                }
            }

            throw new Exception($"Send SMS has failed with response {smsBody}");
        }

        private async Task<SMSStatusEnum> SendSMS(SMSLog sms)
        {
            var smsBody = string.Empty;
            var sessionId = await FetchSessionId();
            if(sessionId != null)
            {
                var receiver = AddCountryCode(sms.Receiver);
                var sendSMSResponse = await _httpClient.GetAsync($"?cmd=sendmsg&sessionid={sessionId}&message={sms.Content}&sender={_sender}&sendto={receiver}&msgtype=0");
                if(sendSMSResponse.IsSuccessStatusCode)
                {
                    smsBody = await sendSMSResponse.Content.ReadAsStringAsync();
                    if(smsBody.StartsWith("OK"))
                    {
                        var split = smsBody.Split(":");
                        sms.MessageId = split[1].Trim();
                        return SMSStatusEnum.Sent;
                    } 
                }
            }

            throw new Exception($"Send SMS has failed with response {smsBody}");
        }

        public async Task SendBatchSMSAsync()
        {
            var unsentSMS = await _smsRepo.GetAllListAsync(x => x.Status != SMSStatusEnum.Sent && x.DateToSend.Date == DateTime.Now.Date && x.IsSendImmediately);
            foreach(var sms in unsentSMS)
            {
                sms.Status = await SendVansoEnum(sms);
                _smsRepo.Update(sms);
                _smsRepo.SaveChanges();
            }
        }
    }
}