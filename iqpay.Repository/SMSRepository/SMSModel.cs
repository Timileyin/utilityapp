using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace iqpay.Repository.SMSRepository
{
     public class VANSOSMS 
     {
        public string Dest { get; set; } 
        public string Src { get; set; } 
        public string Text { get; set; } 
    }

    public class VANSOResponse
    {
        public int ErrorCode { get; set; } 
        public string ErrorMessage { get; set; } 
        public string TicketId { get; set; } 
        public string Status { get; set; } 
    }

    public class VANSOSMSModel    
    {
        public VANSOSMS SMS { get; set; } 
    }

}