using System;
using System.Threading.Tasks;
using iqpay.Data.Entities.Notification;
using iqpay.Repository.Interface;

namespace iqpay.Repository.SMSRepository
{
    public interface ISMSManager:IAutoDependencyRegister
    {
        Task CreateSMSLog(SMSLog sms);

        Task SendBatchSMSAsync();
    }
}