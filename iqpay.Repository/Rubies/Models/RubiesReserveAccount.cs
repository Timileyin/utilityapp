using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace iqpay.Repository.Rubies.Models
{

    public class ReserveAccountRequest
    {
         public string Virtualaccountname { get; set; } 
        public decimal Amount { get; set; } 
        public string Amountcontrol { get; set; } 
        public int Daysactive { get; set; } 
        public int Minutesactive { get; set; } 
        public string Callbackurl { get; set; } 
        public string Singleuse { get; set; } 
    }

    public class FetchBanksRequest
    {
        public string Request{get;set;}   
    }

    public class NameEnquiryRequest
    {
        public string Accountnumber { get; set; } 
        public string Bankcode { get; set; }
    }

    public class FundTransferResp
    {
        public string Transactionstatus { get; set; } 
        public string Responsecode { get; set; } 
        public decimal Amount { get; set; } 
        public string Nibssresponsemessage { get; set; } 
        public string Responsemessage { get; set; } 
        public DateTime Requestdatetime { get; set; } 
        public string Draccount { get; set; } 
        public string Sessionid { get; set; } 
        public string Craccount { get; set; } 
        public string Reference { get; set; } 
        public string Tcode { get; set; } 
        public DateTime Responsedatetime { get; set; } 
        public string Nibsscode { get; set; } 
        public string Narration { get; set; } 
        public string Customerid { get; set; } 
        public string Craccountname { get; set; } 
        public string Bankname { get; set; } 
        public string Draccountname { get; set; } 
        public string Bankcode { get; set; } 
        public string Username { get; set; } 
    }

    public class FundTransferReq
    {
        public string Reference { get; set; } 
        public decimal Amount { get; set; } 
        public string Narration { get; set; } 
        public string Craccountname { get; set; } 
        public string Bankname { get; set; } 
        public string Draccountname { get; set; } 
        public string Craccount { get; set; } 
        public string Bankcode { get; set; } 
    }  

    public class NameEnquiryResponse
    {
        public string Responsedatetime { get; set; } 
        public string Responsecode { get; set; } 
        public string Accountnumber { get; set; } 
        public string Accountname { get; set; } 
        public string Kyc { get; set; } 
        public string Responsemessage { get; set; } 
        public string Sessionid { get; set; } 
        public string Bvn { get; set; } 
        public string Bankcode { get; set; }
    }

    public class FetchTransactionRequest
    {       
        public string Paymentreference { get; set; } 
    }

    public class TransactionDetailsResponse
    {
        public string Responsecode { get; set; } 
        public string Amount { get; set; } 
        public string Virtualaccount { get; set; } 
        public string Settlementaccount { get; set; } 
        public string Virtualaccountname { get; set; } 
        public string Channel { get; set; } 
        public string Paymentreference { get; set; } 
        public string Sessionid { get; set; } 
        public string Originatoraccountnumber { get; set; } 
        public string Originatorname { get; set; } 
        public string Partner { get; set; } 
        public string Narration { get; set; } 
        public string Bankname { get; set; } 
        public string Bankcode { get; set; } 
    }

    public enum RubiesAmountControl
    {
        VARIABLEAMOUNT,
        FIXEDAMOUNT,
        UNDERPAYMENT,
        OVERPAYMENT,        
    }

    public class BankListResponse
    {
        public string Responsecode { get; set; } 
        public string ResponseMessage{get;set;}
        public List<BankData> Banklist{get;set;}
    }

    public class BankData
    {
        public string Bankname{get;set;}
        public string Bankcode{get;set;}
    }

    public class ReserveAccountResponse
    {
        public string Expirydate { get; set; } 
        public string Responsecode { get; set; } 
        public string Autoexpirydate { get; set; } 
        public string Virtualaccount { get; set; } 
        public string Virtualaccountname { get; set; } 
        public string Walletstatus { get; set; } 
        public string Responsemessage { get; set; } 
        public string Uniqueref { get; set; } 
        public string Amountcontroltype { get; set; } 
        public string Opendate { get; set; } 
        public string Merchantaccountno { get; set; } 
        public string Forcedisable { get; set; } 
        public string Requestdata { get; set; } 
        public string Totalamountpaid { get; set; } 
        public string Customerid { get; set; } 
        public string Merchantaccountname { get; set; } 
        public string Callbackurl { get; set; } 
        public string ExactAmount { get; set; } 
    }

}