using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Repository.Rubies.Models;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Rubies
{
    public interface IRubiesRequests:IAutoDependencyRegister
    {
        Task<TransactionDetailsResponse> FetchTransaction(string reference);
        Task<BankListResponse> BankLookup();
        Task<NameEnquiryResponse> NameEnquiry(string accountNumber, string bankCode);
        Task<FundTransferResp> FundTransfer(string vendCode,decimal amount, string narration, string craccountname, string bankName, string draccount, string craccount, string bankCode);
        Task<ReserveAccountResponse> ReserveAccount(string dealerCode, string dealerName, string dealerEmail);                 
    }
    
}