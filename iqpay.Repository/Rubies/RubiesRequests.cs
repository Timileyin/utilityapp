using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Repository.Rubies.Models;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System;
using System.Text;
using Microsoft.Extensions.Logging;

namespace iqpay.Repository.Rubies
{
    public class RubiesRequests:IRubiesRequests
    {
        private readonly string _secret;
        private readonly string _url;
        private readonly IConfiguration _configuration;
        private readonly HttpClient _httpClient;
        private readonly ILogger<RubiesRequests> _logger;
        private readonly string _callBackURL;         
        public RubiesRequests(ILogger<RubiesRequests> logger, IConfiguration config)
        {            
            _configuration = config;
            //_apiKey = _configuration["Monnify:APIKEY"];
            _secret = _configuration["Rubies:Secret"];
            _url = _configuration["Rubies:URL"];
            _callBackURL = _configuration["Rubies:CallBackURL"];
            //_contractCode = _configuration["Monnify:ContractCode"];
            _logger = logger;
            //_base64Token = _configuration["Monnify:Base64Token"];

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(_url),                                
            };            
            _httpClient.DefaultRequestHeaders.Add("Authorization", _secret);
        }   

        private async Task<T> Request<T>(HttpMethod method,  string endpoint)where T:class
        {
            return await Request<T, Object>(method, endpoint);
        }        

        public async Task<TransactionDetailsResponse> FetchTransaction(string txnRef)
        {
            var request = new FetchTransactionRequest
            {
                Paymentreference = txnRef
            };
            var transactionResponse = await Request<TransactionDetailsResponse, FetchTransactionRequest>(HttpMethod.Post, "confirmcallbacktransaction",request);
            if(transactionResponse.Responsecode == "00")
            {
                return transactionResponse;
            }

            throw new Exception("Rubies fetch transaction has failed");
        }

        private async Task<T> Request<T, U>(HttpMethod method,  string endpoint, U request = null  ) where T:class where U:class
        {
            T response;
            var responseBody = new HttpResponseMessage();

            if(method == HttpMethod.Post)
            {
                var postBody = JsonConvert.SerializeObject(request);
                var jsondata = new StringContent(postBody, Encoding.UTF8, "application/json");
                responseBody = await _httpClient.PostAsync(endpoint, jsondata);
                _logger.Log(LogLevel.Warning,$"Rubies Response({endpoint}=>: {jsondata})");
            }else if(method == HttpMethod.Get){
                responseBody = await _httpClient.GetAsync(endpoint);
            }                      
            
            if(responseBody.IsSuccessStatusCode)
            {
                var body = await responseBody.Content.ReadAsStringAsync();
                _logger.Log(LogLevel.Warning,$"Rubies Response({endpoint}=>: {body})");
                response = JsonConvert.DeserializeObject<T>(body);
                return response;
            }else
            {
                var body = await responseBody.Content.ReadAsStringAsync();
                throw new Exception($"Rubies request failed with this response {body}");
            }        
        }

        public async Task<BankListResponse> BankLookup()
        {
            var req = new FetchBanksRequest
            {
                Request = "banklist"
            };

            var resp = await Request<BankListResponse, FetchBanksRequest>(HttpMethod.Post,"banklist", req);
            if(resp.Responsecode == "00"){
                return resp;
            }

            throw new Exception("Rubies Fetch Banks has failed");
        }

        public async Task<NameEnquiryResponse> NameEnquiry(string accountNumber, string bankCode)
        {
            var req = new NameEnquiryRequest
            {
                Accountnumber = accountNumber,
                Bankcode = bankCode
            };

            var resp = await Request<NameEnquiryResponse, NameEnquiryRequest>(HttpMethod.Post, "nameenquiry", req);
            if(resp.Responsecode == "00")
            {
                return resp;
            }
            
            throw new Exception("Name enquiry has failed");
        }

        public async Task<FundTransferResp> FundTransfer(string refnum,decimal amount, string narration, string craccountname, string bankName, string draccount, string craccount, string bankCode)
        {
            var req = new FundTransferReq
            {
                Amount = amount,
                Bankcode = bankCode,
                Bankname = bankName,
                Craccount = craccount,
                Craccountname = craccountname,
                Draccountname = draccount,
                Narration = narration,
                Reference = refnum
            };

            var response = await Request<FundTransferResp, FundTransferReq>(HttpMethod.Post,"fundtransfer",req);
            if(response.Responsecode == "00")
            {
                return response;
            }
            
            throw new Exception("Fund transfer has failed");
        }

        public async Task<ReserveAccountResponse> ReserveAccount(string dealerCode, string dealerName, string dealerEmail)
        {
            var request = new ReserveAccountRequest
            {                
                Virtualaccountname   = $"IQPAY/{dealerCode}",
                Amountcontrol = RubiesAmountControl.VARIABLEAMOUNT.ToString(),
                Amount = 0.00M,
                Singleuse = "N",
                Minutesactive = 30,
                Daysactive = 20000,
                Callbackurl = $"{_callBackURL}{dealerCode}",                
            };

            var response = await Request<ReserveAccountResponse, ReserveAccountRequest>(HttpMethod.Post, "createvirtualaccount", request);
            if(response.Responsecode == "00")
            {
                return response;
            }

            _logger.LogError("Rubies account reservation has failed");
            throw new Exception("Rubies account reservation has failed");
        }
    }
 }