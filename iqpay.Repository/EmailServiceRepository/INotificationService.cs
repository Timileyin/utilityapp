﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using iqpay.Repository.Interface;

namespace iqpay.Repository.EmailServiceRepository
{
    public interface INotificationService:IAutoDependencyRegister
    {
        Task SendEmails();
    }
}
