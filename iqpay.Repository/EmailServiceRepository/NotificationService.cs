﻿using iqpay.Data;
using iqpay.Data.Entities.Email;
using iqpay.Engine.Base.Entities;
using iqpay.Engine.Base.Repository.EmailRepository;
using iqpay.Engine.Repository.EmailRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using iqpay.Repository.SMSRepository;

namespace iqpay.Repository.EmailServiceRepository
{
    public class NotificationService : INotificationService
    {
       // private IQPAYContext _context;
        private IEmailManager<EmailLog, EmailTemplate> _emailManager;
        private ISMSManager _smsManager;

        public NotificationService(IEmailManager<EmailLog, EmailTemplate> emailManager, ISMSManager smsManager)
        {
            _emailManager = emailManager;
            _smsManager = smsManager;
        }

        public async Task SendEmails()
        {
            await _emailManager.SendBatchMailAsync();               
        }

        public async Task SendSMS()
        {
            await _smsManager.SendBatchSMSAsync();
        }        
    }
}
