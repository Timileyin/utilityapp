﻿using iqpay.Repository.Interface;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.TransactionRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using iqpay.Data.UserManagement.Model;
using iqpay.Repository.Users;
using iqpay.Core.Utilities;
using iqpay.Repository.ServicesRepository;
using iqpay.Repository.Wallet.Models;

namespace iqpay.Repository.Wallet
{
    public class WalletRepository:IWalletRepository
    {
        private readonly IRepository<Data.Entities.Payment.Wallet> _repo;
        //private IRepository<Data.Entities.Payment.Wallet> _walletRepo;
        private readonly ITransactionRepository _transactionRepoisitory;
        private readonly IRepository<FundWalletRequest> _fundWalletRequestRepo;   
        private readonly IRepository<DealerCommission> _dealerCommission; 
        private readonly IRepository<WalletTransaction> _walletTransaction;
         private readonly UserManager<ApplicationUser> _userManager;  
        private readonly IDealerRepository _dealerRepo;  
        private readonly IRepository<BankTransfer> _bankTransferLog; 
        private readonly IRepository<DealersServices> _dealerService;
        private readonly IVendorRepository _vendorRepo;
        
        public WalletRepository(IVendorRepository vendorRepo,IRepository<WalletTransaction> walletTransaction, IRepository<DealersServices> dealerService, IRepository<BankTransfer> bankTransferLog, UserManager<ApplicationUser> userManager,IDealerRepository dealerRepo, IRepository<Data.Entities.Payment.Wallet> repo, IRepository<DealerCommission> dealerCommission,  IRepository<FundWalletRequest> fundWalletRequestRepo, ITransactionRepository transactionRepossitory)
        {
            _repo = repo;
            _fundWalletRequestRepo = fundWalletRequestRepo;
            _transactionRepoisitory = transactionRepossitory;
            _dealerCommission = dealerCommission;
            _userManager = userManager;
            _dealerRepo = dealerRepo;
            _bankTransferLog = bankTransferLog;
            _walletTransaction = walletTransaction;
            _dealerService = dealerService;
            _vendorRepo = vendorRepo;
            //_walletRepo = walletRepo;
        }
        public async Task<List<Data.Entities.Payment.Wallet>> FetchWallets(long userId)
        {
            var whitelistedvendors = (await _dealerService.GetAllListAsync(x=>x.UserId == userId)).Select(x=>x.VendorId);  
            var wallets = await _repo.GetAll().Include(x=>x.Vendor).Include(x=>x.Service.Vendor).Where(x => x.UserId == userId && (whitelistedvendors.Contains(x.VendorId) || x.VendorId == null ) && (whitelistedvendors.Contains(x.Service.VendorId)  || x.ServiceId == null) ).ToListAsync();// GetAllListAsync(x => x.UserId == userId);
            return wallets;
        }

        public async Task<List<WalletBalance>> FetchWalletBalances()
        {
            var walletBalances = await _repo.GetAll().Include(x=>x.User).Select(x=>new WalletBalance
            {
                DealerId = x.Id,
                DealerName = x.User.Name,
                DealerCode = x.User.UserName,
                TotalBalance = x.Balance,
                WalletType = x.WalletName
            }).ToListAsync();
            return walletBalances;
        } 

        public async Task LogTransfer(PaymentProcessor processor, decimal amountPaid, string paymentRef, DateTime timePaid, string status, string transactionRef, bool isProcessed, string accountNumber, string accountName, string bank, string monnifyResponse, string customerEmail, string creditAccount, string dealerCode, TransferType type)
        {
            await _bankTransferLog.InsertAsync(new BankTransfer{
                AccountNumber = accountNumber,
                AmountPaid =  amountPaid,
                Bank = bank,
                DatePaid = timePaid,
                IsProcessed = isProcessed,
                DateProcessed = DateTime.Now,
                MonnifyResponse = monnifyResponse,
                PaymentRef = paymentRef,
                AccountName = accountName,
                TransactionRef = transactionRef,
                PaymentStatus = status,
                CustomerEmail =  customerEmail,
                CustomerName = creditAccount,
                Processor = processor, 
                CreditAccount = creditAccount,
                DealerCode = dealerCode,
                Type = type                                      
            });

            await _bankTransferLog.SaveChangesAsync();            
        }

        public async Task<Data.Entities.Payment.Wallet> FetchVendorWallet(long userId, long vendorId)
        {
            var wallets = await FetchWallets(userId);
            var vendorWallet = wallets.FirstOrDefault(x=>x.VendorId == vendorId && x.CommissionMode == CommissionMode.CommissionAhead);
            return vendorWallet;
        }

        public async Task<BankTransfer> FetchMonnifyTransfer(string paymentRef)
        {
            var transfer = await _bankTransferLog.FirstOrDefaultAsync(x=>x.PaymentRef == paymentRef && (x.Processor == null || x.Processor == PaymentProcessor.Monnify));
            return transfer;
        }

        public async Task<BankTransfer> FetchRubiesTransfer(string paymentRef)
        {
            var transfer = await _bankTransferLog.FirstOrDefaultAsync(x=>x.PaymentRef == paymentRef && x.Processor == PaymentProcessor.Rubies);
            return transfer;
        }

        public async Task<BankTransfer> FetchWemaTransfer(string paymentRef)
        {
            var transfer = await _bankTransferLog.FirstOrDefaultAsync(x=>x.PaymentRef == paymentRef && x.Processor == PaymentProcessor.Wema);
            return transfer;
        }

        public async Task<BankTransfer> FetchVFDTransfer(string paymentRef)
        {
            var transfer = await _bankTransferLog.FirstOrDefaultAsync(x=>x.PaymentRef == paymentRef && x.Processor == PaymentProcessor.VFD);
            return transfer;
        }

        public async Task<List<BankTransfer>> FetchWemaTransfers()
        {
            var transfer = await _bankTransferLog.GetAllListAsync(x=>x.Processor == PaymentProcessor.Wema);
            return transfer;
        }

        public async Task<List<BankTransfer>> FetchTransfers()
        {
            var transfer = await _bankTransferLog.GetAllListAsync();
            return transfer;
        }

        public async Task<List<BankTransfer>> FetchTransfers(string dealerCode)
        {
            var transfer = await _bankTransferLog.GetAllListAsync(x=>x.DealerCode == dealerCode);
            return transfer;
        }    

        public async Task<List<VendorCummWalletBalance>> FetchVendorCummWalletBalance()
        {
            var vendors = await _vendorRepo.FetchActiveVendors();
            var walletBalance = new List<VendorCummWalletBalance>();
            foreach(var vendor in vendors)
            {
                if(vendor.IsCommissionAhead)
                {
                    var wallets = await _repo.GetAll().Include(x=>x.User).Where(x=>x.Type == WalletType.Commission && x.VendorId == vendor.Id).ToListAsync();
                    walletBalance.Add(
                        new VendorCummWalletBalance{
                            CummBalance = wallets.Sum(x=>x.Balance),
                            VendorName = vendor.VendorName,
                            WalletCount = wallets.Count()
                        }
                    );
                     
                }else{
                    var userIds = await _dealerService.GetAll().Include(x=>x.Service).Include(x=>x.User).Where(x=>(x.VendorId == vendor.Id || x.Service.VendorId == vendor.Id)).Select(x=>x.UserId).ToListAsync();
                    var wallets = await _repo.GetAll().Include(x=>x.User).Where(x=>x.Type == WalletType.Transaction && userIds.Contains(x.UserId)).ToListAsync();
                    walletBalance.Add(
                        new VendorCummWalletBalance{
                            CummBalance = wallets.Sum(x=>x.Balance),
                            VendorName = vendor.VendorName,
                            WalletCount = wallets.Count()
                        }
                    ); 
                }
            }
            
            return walletBalance;
        }
        
        public async Task<Data.Entities.Payment.Wallet> FetchServiceWallet(long userId, long serviceId)
        {
            var wallets = await FetchWallets(userId);
            var serviceWallet = wallets.FirstOrDefault(x=>x.ServiceId == serviceId && x.CommissionMode == CommissionMode.CommissionAhead);
            return serviceWallet;
        }
        public async Task<Data.Entities.Payment.Wallet> FetchBaseWallet(long userId)
        {
            var wallets = await FetchWallets(userId);
            var baseWallet = wallets.FirstOrDefault(x => x.CommissionMode == CommissionMode.Base);
            return baseWallet;
        }

        public async Task<bool> CreateVendorWallet(long userId, decimal balance, WalletType type, CommissionMode commissionMode, long vendorId, string vendorName)
        {
            var newWallet =  await _repo.InsertAsync(new Data.Entities.Payment.Wallet {
                Balance = balance,
                UserId = userId,
                Type = type,
                CommissionMode = commissionMode,
                WalletName = vendorName,
                VendorId = vendorId,
                Description = $"{vendorName} wallet"
            });

            await _repo.SaveChangesAsync();
            return true;
        }

        public async Task<Data.Entities.Payment.Wallet> FetchCommissionWallet(long userId)
        {
            var wallets = await FetchWallets(userId);
            return wallets.FirstOrDefault(x=>x.Type == WalletType.Commission);
        }

        public bool FulfillRequest(FundWalletRequest request, long userId)
        {
            var walletRequest = _fundWalletRequestRepo.GetById(request.Id);            
            var wallet = _repo.GetById(request.WalletId);
            walletRequest.CreditedById = userId;

            var userBaseWallet = Task.Run(()=>FetchBaseWallet(userId)).Result;          
            if(userBaseWallet.Balance >= walletRequest.AmmountCreditted)
            { 
                userBaseWallet.Balance -= walletRequest.AmmountCreditted;
                _repo.Update(userBaseWallet);
                _repo.SaveChanges();

                walletRequest.DebittedWalletBalance = userBaseWallet.Balance;
                walletRequest.CredittedWalletBalance = wallet.Balance + walletRequest.AmmountCreditted;
                walletRequest.AmmountCreditted = request.AmmountCreditted;
                walletRequest.Status = FundWalletRequestStatus.Credited;
                walletRequest.TransactionDate = DateTime.Now;
                _fundWalletRequestRepo.Update(walletRequest);
                _fundWalletRequestRepo.SaveChanges();

                wallet.Balance += walletRequest.AmmountCreditted;
                _repo.Update(wallet);
                _repo.SaveChanges();
                return true;
            }
            return false;
        }

        public FundWalletRequest GetFundWalletRequest(long Id)
        {
            var request = _fundWalletRequestRepo.GetAll().Include(x => x.Wallet.User).FirstOrDefault(x => x.Id == Id);
            return request;
        }

        public List<FundWalletRequest> FundingRequestedByUser(long initiatedBy, DateTime? startDate = null, DateTime? endDate = null)
        {
            IEnumerable<FundWalletRequest> requests;
            if(startDate == null || endDate == null)
            {
                endDate = DateTime.Now;
                startDate = DateTime.Today;
            }
            requests = _fundWalletRequestRepo.GetAll().Include(x=>x.Wallet.User).Where(x=>x.InitiatedById == initiatedBy).OrderByDescending(x=>x.DateCreated);
            requests = requests.Where(x=>x.DateCreated >= startDate && x.DateCreated <= endDate);
            return requests.ToList();
        }

        public bool FundBaseWalletFromWallet(long walletId, long baseWalletId, out string message)
        {
            message = "";
            var wallet = _repo.GetById(walletId);
            var baseWallet = _repo.GetById(baseWalletId);
            if(wallet.Balance > 0){

                baseWallet.Balance += wallet.Balance;
                _repo.Update(baseWallet);
                _repo.SaveChanges();

                wallet.Balance = 0;
                _repo.Update(wallet);
                _repo.SaveChanges(); 

                

                return true;
            }
            message = "Insufficient funds";
            return false;
        }

        public async Task<List<WalletTransaction>> FetchWalletTransaction(long? userId = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            IQueryable<WalletTransaction> walletTransactions;
            if(startDate == null || endDate == null)
            {
                endDate = DateTime.Now;
                startDate = DateTime.Today;
            }
            if(userId != null){
                walletTransactions =  _walletTransaction.GetAll().Include(x=>x.Wallet.Vendor).Include(x=>x.Wallet.Service.Vendor).Include(x=>x.User).Where(x=>x.UserId == userId).OrderByDescending(x=>x.DateCreated);                
            }else{
                walletTransactions =  _walletTransaction.GetAll().Include(x=>x.Wallet.Vendor).Include(x=>x.Wallet.Service.Vendor).Include(x=>x.User).OrderByDescending(x=>x.DateCreated);                
            }      

            walletTransactions = walletTransactions.Where(x=>x.DateCreated >= startDate && x.DateCreated <= endDate);
            return await walletTransactions.ToListAsync();      

        }

        public async Task<bool> FundWalletFromBaseWallet(long walletId, long baseWalletId, decimal amount, long userId)
        {
            string message = "";
            var baseWallet = _repo.GetById(baseWalletId);
            var wallet = _repo.GetAll().Include(x=>x.Service).Include(x=>x.Vendor).FirstOrDefault(x=>x.Id == walletId);
            
            if(baseWallet.Balance < amount || baseWallet.Balance <= 0)
            {
                message = "You do not have sufficient balance in base wallet to complete funding";
                return false;
            }
            else
            {
                baseWallet.Balance -= amount;
                _repo.Update(baseWallet);
                _repo.SaveChanges();
                
                if(baseWallet.CommissionMode == CommissionMode.Base && wallet.CommissionMode == CommissionMode.CommissionAhead)
                {
                    
                    decimal commission = 0;
                
                    var user =  await _userManager.FindByIdAsync(userId.ToString());
                    if(user != null)
                    {
                        if(user.AccountType == AccountType.SUPER_DEALER)
                        {
                            if(wallet.ServiceId == null)
                            {
                                var dealerCommissionInfo = await _dealerRepo.FetchVendorDealerCommission(user.Id, user.AccountType, wallet.VendorId.Value);
                                commission = (dealerCommissionInfo.Commission * amount)/100;
                                if(dealerCommissionInfo.CommissionCap.HasValue && commission > dealerCommissionInfo.CommissionCap){
                                    commission = dealerCommissionInfo.CommissionCap.Value;
                                }

                            }else if(wallet.VendorId == null)
                            {
                                var dealerCommissionInfo = await _dealerRepo.FetchServiceCommission(user.Id, user.AccountType, wallet.ServiceId.Value);
                                commission = ((dealerCommissionInfo.Commission) * amount)/100;
                                if(dealerCommissionInfo.CommissionCap.HasValue && commission > dealerCommissionInfo.CommissionCap){
                                    commission = dealerCommissionInfo.CommissionCap.Value;
                                }
                            }

                            var profit = commission;

                            wallet.Balance += amount + profit;
                            _repo.Update(wallet);
                            _repo.SaveChanges();

                            await _walletTransaction.InsertAsync(new WalletTransaction{
                                Amount = amount,                                
                                FromWalletId = baseWalletId,
                                Profit = profit,
                                WalletId = walletId,
                                Status = TransactionStatus.Completed,
                                UserId = userId                                                                
                            });

                            await _walletTransaction.SaveChangesAsync();
                        }
                        else if(user.AccountType == AccountType.DEALER)
                        {
                            if(wallet.ServiceId == null)
                            {
                                var dealerCommissionInfo = await _dealerRepo.FetchVendorDealerCommission(user.Id, user.AccountType, wallet.VendorId.Value);
                                commission = ((dealerCommissionInfo.Commission) * amount)/100;                                    
                                
                                if(commission > dealerCommissionInfo.CommissionCap){
                                    commission = dealerCommissionInfo.CommissionCap.Value;
                                }
                            }
                            else if(wallet.VendorId == null)
                            {
                                var dealerCommissionInfo = await _dealerRepo.FetchServiceCommission(user.Id, user.AccountType, wallet.ServiceId.Value);
                                commission = ((dealerCommissionInfo.Commission) * amount)/100;
                                if(commission > dealerCommissionInfo.CommissionCap){
                                    commission = dealerCommissionInfo.CommissionCap.Value;
                                }                                            
                            }

                            var profit = commission;

                                wallet.Balance += amount + profit;
                                _repo.Update(wallet);
                                _repo.SaveChanges();

                            await _walletTransaction.InsertAsync(new WalletTransaction{
                                Amount = amount,                                
                                FromWalletId = baseWalletId,
                                Profit = profit,
                                WalletId = walletId,
                                Status = TransactionStatus.Completed,
                                UserId = userId                              
                            });
                            
                            await _walletTransaction.SaveChangesAsync();
                        }
                        else if(user.AccountType == AccountType.AGENT)                        
                        {
                            if(wallet.VendorId != null)
                            {
                                var agentCommissionInfo = await _dealerRepo.FetchVendorDealerCommission(user.Id, user.AccountType, wallet.VendorId.Value);
                                var dealerCommInfo = await _dealerRepo.FetchVendorDealerCommission(user.SuperDealerId.Value, user.AccountType, wallet.VendorId.Value); 
                                var dealer = await _userManager.FindByIdAsync(user.SuperDealerId.ToString());
                                
                                
                                commission = ((agentCommissionInfo.Commission) * amount)/100;
                                if(agentCommissionInfo.CommissionCap.HasValue && commission > agentCommissionInfo.CommissionCap){
                                    commission = agentCommissionInfo.CommissionCap.Value;
                                }
                                
                            }
                            else if(wallet.ServiceId != null)
                            {
                                var agentCommissionInfo = await _dealerRepo.FetchServiceCommission(user.Id, user.AccountType, wallet.ServiceId.Value);
                                var dealerCommInfo = await _dealerRepo.FetchServiceCommission(user.SuperDealerId.Value, user.AccountType, wallet.ServiceId.Value); 
                                var dealer = await _userManager.FindByIdAsync(user.SuperDealerId.ToString());
                                
                                
                                commission = ((agentCommissionInfo.Commission) * amount)/100;
                                if(agentCommissionInfo.CommissionCap.HasValue && commission > agentCommissionInfo.CommissionCap){
                                    commission = agentCommissionInfo.CommissionCap.Value;
                                }

                                
                            }

                            wallet.Balance += amount + commission;
                                _repo.Update(wallet);
                                _repo.SaveChanges();

                            await _walletTransaction.InsertAsync(new WalletTransaction{
                                Amount = amount,                                
                                FromWalletId = baseWalletId,
                                Profit = commission,
                                WalletId = walletId,
                                Status = TransactionStatus.Completed,
                                UserId = userId                                  
                            });
                            
                            await _walletTransaction.SaveChangesAsync();
                        
                        }        
                        return true;                 
                    }
                                                                                                
                }
                 return false;


            }
        }

        public async Task<bool> UpdateWallet(Data.Entities.Payment.Wallet wallet)
        {
            var _wallet =  _repo.GetById(wallet.Id);
            if(_wallet != null)
            {
                _wallet.WalletName = wallet.WalletName;
                _wallet.Description = wallet.Description;
                _wallet.CommissionMode = wallet.CommissionMode;
                await _repo.UpdateAsync(_wallet);
                await _repo.SaveChangesAsync();

                return true;

            }

            return false;
        }

        public async Task<List<Data.Entities.Payment.Wallet>> FetchWallets()
        {
            var wallets = await _repo.GetAll().Include(x=>x.Vendor).Include(x=>x.Service.Vendor).ToListAsync();
            return wallets;
        }

        public async Task<Data.Entities.Payment.Wallet> CreateWallet(Data.Entities.Payment.Wallet wallet)
        {
            
            var newWallet = await _repo.InsertAsync(wallet);
            await _repo.SaveChangesAsync();

            return newWallet.Entity;
        }

        public async Task<iqpay.Data.Entities.Payment.Wallet> CreateWallet(long userId, decimal initialBalance, WalletType walletType, CommissionMode commissionMode)
        {
            var newWallet = await  _repo.InsertAsync(new Data.Entities.Payment.Wallet {
                Balance = initialBalance,
                UserId = userId,
                Type = walletType,
                CommissionMode = commissionMode,
                WalletName = walletType.GetDescription()
            });

            await _repo.SaveChangesAsync();

            return newWallet.Entity;
        }

        public async Task<iqpay.Data.Entities.Payment.Wallet> CreateWallet(long userId, decimal initialBalance, WalletType walletType, CommissionMode commissionMode, long serviceId, string walletName)
        {
            var newWallet = await _repo.InsertAsync(new Data.Entities.Payment.Wallet
            {
                Balance = initialBalance,
                UserId = userId,
                Type = walletType,
                CommissionMode = commissionMode,
                ServiceId = serviceId,
                WalletName = walletName
            });

            await _repo.SaveChangesAsync();

            return newWallet.Entity;
        }
        public bool CreditWallet(long walletId, decimal amount)
        {
            var walletToCredit = _repo.Get(walletId);
            if(walletToCredit != null)
            {
                walletToCredit.Balance += amount;
                 _repo.Update(walletToCredit);
                 _repo.SaveChanges();                 
                return true;
            }

            return false;
        }

        public async Task<iqpay.Data.Entities.Payment.Wallet> FetchWemaWallet()
        {
            var wemaWallet = await _repo.FirstOrDefaultAsync(x=>x.Type == WalletType.WemaInternalWallet);
            return wemaWallet;
        }
        public async Task<bool> DebitWallet(long walletId, decimal amount)
        {
            var walletToCredit = _repo.Get(walletId);
            if (walletToCredit != null)
            {
                walletToCredit.Balance -= amount;
                await _repo.InsertOrUpdateAsync(walletToCredit);
                await _repo.SaveChangesAsync();

                return true;
            }

            return false;
        }
        public async Task<iqpay.Data.Entities.Payment.Wallet> FetchWallet(long walletId)
        {
            var wallet = await _repo.GetAsync(walletId);
            return wallet;
        }

        public Task<bool> FundingRequest(FundWalletRequest request)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> InitiateFundingRequest(FundWalletRequest request)
        {
            try
            {
                await _fundWalletRequestRepo.InsertAsync(request);
                await _fundWalletRequestRepo.SaveChangesAsync();

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public bool FulfillAutoFundingRequest(string txnRef)
        {
            var fulfilled = false;
            try
            {
               
                var fundingRequest = _fundWalletRequestRepo.FirstOrDefault(x => x.TransactionRef == txnRef && x.IsAuto);

                if (fundingRequest != null)
                {
                    fundingRequest.CreditedById = fundingRequest.InitiatedById;
                    fundingRequest.Status = FundWalletRequestStatus.Credited;
                    

                    var wallet = _repo.FirstOrDefault(x => x.Id == fundingRequest.WalletId);
                    wallet.Balance += fundingRequest.Amount;
                    _fundWalletRequestRepo.Update(fundingRequest);
                    _fundWalletRequestRepo.SaveChanges();
                    fulfilled = true;
               }

                return fulfilled;
            }catch(Exception ex)
            {
                return fulfilled;
            }
        }

        public async Task<string> InitiateAutoFundingRequest(FundWalletRequest request)
        {
            try
            {
                var txnRef = _transactionRepoisitory.GenerateTransactionReference("IQP");
                              
                var billLog = await _transactionRepoisitory.GenerateBillLog(request.Amount, txnRef, BillType.WalletFunding, request.InitiatedById.Value);
                if (billLog != null)
                {
                    request.IsAuto = true;
                    request.TransactionRef = txnRef;
                    await _fundWalletRequestRepo.InsertAsync(request);
                    await _fundWalletRequestRepo.SaveChangesAsync();

                    return txnRef;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public async Task<iqpay.Data.Entities.Payment.Wallet> FetchWalletInTransaction(string txnRef)
        {
            var fundRequest = await _fundWalletRequestRepo.GetAll().Include(x => x.Wallet).FirstOrDefaultAsync(x => x.TransactionRef == txnRef);
            return fundRequest.Wallet;
        }

        public async Task<List<FundWalletRequest>> FetchFundingRequests(FundWalletRequestStatus? status)
        {
            var requests = await _fundWalletRequestRepo.GetAllListAsync(x => x.Status == status || status == null);
            return requests;
        }

        public async Task<List<FundWalletRequest>> FetchFundingRequestsOnAWallet(long id, FundWalletRequestStatus? status)
        {
            var requests = await _fundWalletRequestRepo.GetAllListAsync(x => x.Id == id &&( x.Status == status || status == null));
            return requests;
        }

        public List<FundWalletRequest> FetchFundWalletRequests(long? userId, DateTime? startDate = null, DateTime? endDate = null)
        {            
            if(startDate == null || endDate == null)
            {
                startDate = DateTime.Today;
                endDate = DateTime.Now;
            }

            if(userId == null)
            {
                IQueryable<FundWalletRequest> requests = _fundWalletRequestRepo.GetAll().Include(x=>x.Wallet.User).Where(x=>x.Wallet.User.AccountType == AccountType.SUPER_DEALER).OrderByDescending(x=>x.DateCreated);
                requests = requests.Where(x=>x.DateCreated >= startDate && x.DateCreated <= endDate);
                return requests.ToList();
            }else
            {
                IQueryable<FundWalletRequest> requests = _fundWalletRequestRepo.GetAll().Include(x=>x.Wallet.User).Where(x=>x.Wallet.User.SuperDealerId == userId).OrderByDescending(x=>x.DateCreated);
                requests = requests.Where(x=>x.DateCreated >= startDate && x.DateCreated <= endDate);
                return requests.ToList();
            }            
        }
    }
}
