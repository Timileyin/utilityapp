using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace iqpay.Repository.Wallet.Models{

    public class VendorCummWalletBalance
    {
        public string VendorName{get;set;}
        public long WalletCount{get;set;}
        public decimal CummBalance{get;set;} 
    }

}
