﻿using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.Interface;
using iqpay.Repository.Wallet.Models;
using System;

namespace iqpay.Repository.Wallet
{
    public interface IWalletRepository: IAutoDependencyRegister
    {
        Task<List<WalletBalance>> FetchWalletBalances();
        Task<BankTransfer> FetchMonnifyTransfer(string paymentRef);
        Task<BankTransfer> FetchRubiesTransfer(string paymentRef);
        Task<Data.Entities.Payment.Wallet> FetchServiceWallet(long userId, long serviceId);
        Task<Data.Entities.Payment.Wallet> FetchVendorWallet(long userId, long vendorId);
        Task<Data.Entities.Payment.Wallet> FetchBaseWallet(long userId);
        Task<Data.Entities.Payment.Wallet> FetchWemaWallet();
        Task<BankTransfer> FetchVFDTransfer(string paymentRef);
        Task<BankTransfer> FetchWemaTransfer(string paymentRef);
        Task<List<BankTransfer>> FetchTransfers();
        Task<List<WalletTransaction>> FetchWalletTransaction(long? userId = null, DateTime? startDate = null, DateTime? endDate = null);
        Task<Data.Entities.Payment.Wallet> CreateWallet(Data.Entities.Payment.Wallet wallet);
        Task<Data.Entities.Payment.Wallet> CreateWallet(long userId, decimal initialBalance, WalletType walletType, CommissionMode commissionMode);
        Task<Data.Entities.Payment.Wallet> CreateWallet(long userId, decimal initialBalance, WalletType walletType, CommissionMode commissionMode, long vendorId, string walletName);
        bool CreditWallet(long walletId, decimal amount);
        Task<bool> DebitWallet(long walletId, decimal amount);
        Task<Data.Entities.Payment.Wallet> FetchWallet(long walletId);
        Task<List<Data.Entities.Payment.Wallet>> FetchWallets(long userId);
        Task<List<Data.Entities.Payment.Wallet>> FetchWallets();
        Task<bool> InitiateFundingRequest(FundWalletRequest request);
        Task<List<FundWalletRequest>> FetchFundingRequests(FundWalletRequestStatus? status);
        Task<List<FundWalletRequest>> FetchFundingRequestsOnAWallet(long id, FundWalletRequestStatus? status);
        Task<bool> UpdateWallet(Data.Entities.Payment.Wallet wallet);
        Task<string> InitiateAutoFundingRequest(FundWalletRequest request);
        Task<bool> CreateVendorWallet(long userId,decimal defaultBalance, WalletType walletType, CommissionMode commissionMode, long VendorId, string vendorName);
        Task<bool> FundWalletFromBaseWallet(long walletId, long baseWalletId, decimal amount,long userId);
        List<FundWalletRequest> FetchFundWalletRequests(long? userId, DateTime? startDate = null, DateTime? endDate = null);
        FundWalletRequest GetFundWalletRequest(long Id);
        bool FulfillRequest(FundWalletRequest request, long userId);
        Task<Data.Entities.Payment.Wallet> FetchWalletInTransaction(string txnRef);
        List<FundWalletRequest> FundingRequestedByUser(long initiatedBy, DateTime? startDate = null, DateTime? endDate = null);
        bool FulfillAutoFundingRequest(string txnRef);
        Task<Data.Entities.Payment.Wallet> FetchCommissionWallet(long userId);
        bool FundBaseWalletFromWallet(long walletId, long baseWalletId, out string message);
        Task<List<BankTransfer>> FetchTransfers(string virtualAccount);
        Task<List<VendorCummWalletBalance>> FetchVendorCummWalletBalance();
        Task LogTransfer(PaymentProcessor processor, decimal amountPaid, string paymentRef, DateTime timePaid, string status, string transactionRef, bool isProcessed, string accountNumber, string accountName, string bank, string monnifyResponse, string customerEmail, string creditAccount, string dealerCode, TransferType type);
    }
}
