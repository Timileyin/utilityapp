using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Repository.Monnify.Models;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Monnify
{
    public interface IMonnifyRequests:IAutoDependencyRegister
    {
        Task<MonnifyLogin> Authenticate();
        Task<TransactionResponse> FetchTransaction(string reference);
        Task<MonnifyReservedAccount> ReserveAccount(string dealerCode, string dealerName, string dealerEmail);                 
    }
    
}