using System.Collections.Generic;
using System.Threading.Tasks;

namespace iqpay.Repository.Monnify.Models
{
    public class MonnifyResponse<T> where T:class
    {
        public bool RequestSuccessful{get;set;}
        public string ResponseMessage{get;set;}
        public string ResponseCode{get;set;}
        public T ResponseBody{get;set;}
    }
}