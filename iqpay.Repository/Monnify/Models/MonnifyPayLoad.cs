using System.Collections.Generic;
using System.Threading.Tasks;
using System;

namespace iqpay.Repository.Monnify.Models
{
    public class Product    {
        public string Type { get; set; } 
        public string Reference { get; set; } 
    }

    public class CardDetails    {
        public string CardType { get; set; } 
        public string AuthorizationCode { get; set; } 
        public string Last4 { get; set; } 
        public string ExpMonth { get; set; } 
        public string ExpYear { get; set; } 
        public string Bin { get; set; } 
        public bool reusable { get; set; } 
    }

    public class AccountDetails    {
        public string AccountName { get; set; } 
        public string AccountNumber { get; set; } 
        public string BankCode { get; set; } 
        public string AmountPaid { get; set; } 
    }

    public class AccountPayment    {
        public string AccountName { get; set; } 
        public string AccountNumber { get; set; } 
        public string BankCode { get; set; } 
        public string AmountPaid { get; set; } 
    }

    public class Customer    {
        public string Email { get; set; } 
        public string Name { get; set; } 
    }

    public class MetaData    {
    }

    public class TransactionResponse    {
        public string paymentMethod { get; set; } 
        public DateTime CreatedOn { get; set; } 
        public decimal Amount { get; set; } 
        public string CurrencyCode { get; set; } 
        public string CustomerName { get; set; } 
        public string CustomerEmail { get; set; } 
        public string PaymentDescription { get; set; } 
        public string PaymentStatus { get; set; } 
        public string TransactionReference { get; set; } 
        public string PaymentReference { get; set; } 
    }

    public class RubiesPayLoad
    {
        public string Originatoraccountnumber { get; set; } 
        public decimal Amount { get; set; } 
        public string Originatorname { get; set; } 
        public string Service { get; set; } 
        public string Narration { get; set; } 
        public string Craccountname { get; set; } 
        public string Paymentreference { get; set; } 
        public string Sessionid { get; set; } 
        public string Bankname { get; set; } 
        public string Craccount { get; set; } 
        public string Bankcode { get; set; } 
    }

    public class MonnifyPayLoad    {
        public string TransactionReference { get; set; } 
        public string PaymentReference { get; set; } 
        public decimal AmountPaid { get; set; } 
        public string TotalPayable { get; set; } 
        public string SettlementAmount { get; set; } 
        public string PaidOn { get; set; } 
        public string PaymentStatus { get; set; } 
        public string PaymentDescription { get; set; } 
        public string TransactionHash { get; set; } 
        public string Currency { get; set; } 
        public string PaymentMethod { get; set; } 
        public Product Product { get; set; } 
        public CardDetails CardDetails { get; set; } 
        public AccountDetails AccountDetails { get; set; } 
        public List<AccountPayment> AccountPayments { get; set; } 
        public Customer Customer { get; set; } 
        public MetaData MetaData { get; set; } 
    }
}