using System.Collections.Generic;
using System.Threading.Tasks;

namespace iqpay.Repository.Monnify.Models
{
    public class MonnifyLogin
    {
        public string AccessToken{get;set;}
        public int ExpiresIn{get;set;}
    }
}