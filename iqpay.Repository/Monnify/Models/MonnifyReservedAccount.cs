using System.Collections.Generic;
using System.Threading.Tasks;

namespace iqpay.Repository.Monnify.Models
{
    public class MonnifyReservedAccount
    {
        public string ContractCode { get; set; } 
        public string AccountReference { get; set; } 
        public string AccountName { get; set; } 
        public string CurrencyCode { get; set; } 
        public string CustomerEmail { get; set; } 
        public string AccountNumber { get; set; } 
        public string BankName { get; set; } 
        public string BankCode { get; set; } 
        public string ReservationReference { get; set; } 
        public string Status { get; set; } 
        public string CreatedOn { get; set; } 
        public List<IncomeSplitConfig> IncomeSplitConfig { get; set; } 
    }

     public class IncomeSplitConfig 
     {
        public string SubAccountCode { get; set; } 
        public double FeePercentage { get; set; } 
        public bool FeeBearer { get; set; } 
        public int SplitPercentage { get; set; } 
    }


    public class MonnifyReservedAccountRequest
    {
        public string AccountReference{get;set;}
        public string AccountName{get;set;}
        public string CurrencyCode{get;set;} = "NGN";
        public string ContractCode{get;set;}
        public string CustomerEmail{get;set;}
        public string CustomerName{get;set;}

    }
}