using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Repository.Monnify.Models;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System;
using System.Text;
using Microsoft.Extensions.Logging;

namespace iqpay.Repository.Monnify
{
    public class MonnifyRequests:IMonnifyRequests
    {
        private readonly string _secret;
        private readonly string _apiKey;
        private readonly string _url;
        private readonly IConfiguration _configuration;
        private readonly HttpClient _httpClient;
        private readonly ILogger<MonnifyRequests> _logger;
        private readonly string _contractCode;
        private readonly string _base64Token;
        public MonnifyRequests(ILogger<MonnifyRequests> logger, IConfiguration config)
        {

            _configuration = config;
            _apiKey = _configuration["Monnify:APIKEY"];
            _secret = _configuration["Monnify:Secret"];
            _url = _configuration["Monnify:URL"];
            _contractCode = _configuration["Monnify:ContractCode"];
            _logger = logger;
            _base64Token = _configuration["Monnify:Base64Token"];

            _httpClient = new HttpClient
            {
                BaseAddress = new Uri(_url),                
            };            
        }   

        private async Task<MonnifyResponse<T>> Request<T>(bool requiresAuth, HttpMethod method,  string endpoint)where T:class
        {
            return await Request<T, Object>(requiresAuth, method, endpoint);
        }

        public async Task<TransactionResponse> FetchTransaction(string reference)
        {
            _httpClient.DefaultRequestHeaders.Clear();
            _httpClient.DefaultRequestHeaders.Add("Authorization",$"Basic {_base64Token}");
            var transactionResponse = await Request<TransactionResponse>(false, HttpMethod.Get, $"merchant/transactions/query?paymentReference={reference}");
            if(transactionResponse.RequestSuccessful && transactionResponse.ResponseMessage == "success")
            {
                return transactionResponse.ResponseBody;
            }
            throw new Exception("Monnify fetch transaction has failed");
        }

        private async Task<MonnifyResponse<T>> Request<T, U>(bool requiresAuth, HttpMethod method,  string endpoint, U request = null  ) where T:class where U:class
        {
            var response = new MonnifyResponse<T>();
            var responseBody = new HttpResponseMessage();
            if(requiresAuth)
            {
                var authResponse = await Authenticate();   
                _httpClient.DefaultRequestHeaders.Clear();                            
                _httpClient.DefaultRequestHeaders.Add("Authorization",$"Bearer {authResponse.AccessToken}");
            }

            if(method == HttpMethod.Post)
            {
                var postBody = JsonConvert.SerializeObject(request);
                var jsondata = new StringContent(postBody, Encoding.UTF8, "application/json");
                responseBody = await _httpClient.PostAsync(endpoint, jsondata);
            }else if(method == HttpMethod.Get){
                responseBody = await _httpClient.GetAsync(endpoint);
            }                      
            
            if(responseBody.IsSuccessStatusCode)
            {
                var body = await responseBody.Content.ReadAsStringAsync();
                _logger.Log(LogLevel.Warning,$"Monnify Response({endpoint}=>: {body})");
                response = JsonConvert.DeserializeObject<MonnifyResponse<T>>(body);
                return response;
            }else
            {
                var body = await responseBody.Content.ReadAsStringAsync();
                throw new Exception($"Monnify request failed with this response {body}");
            }
        
        }
        public async Task<MonnifyLogin> Authenticate()
        {
            var keyWithSecret = _apiKey+":"+_secret;            
            var base64Token = Convert.ToBase64String(Encoding.Unicode.GetBytes(keyWithSecret));            
            _httpClient.DefaultRequestHeaders.Add("Authorization",$"Basic {_base64Token}");
            var response = await Request<MonnifyLogin>(false, HttpMethod.Post, "auth/login");
            if(response.RequestSuccessful && response.ResponseMessage == "success")
            {
                return response.ResponseBody;
            }
            _logger.LogError("Monnify authentication has failed");
            throw new Exception("Monnify authentication has failed");
        }

        public async Task<MonnifyReservedAccount> ReserveAccount(string dealerCode, string dealerName, string dealerEmail)
        {
            var request = new MonnifyReservedAccountRequest
            {
                AccountReference = dealerCode,
                AccountName = $"IQPAY/{dealerName}",
                ContractCode = _contractCode,
                CustomerEmail = dealerEmail,
                CustomerName = dealerName
            };
            var response = await Request<MonnifyReservedAccount, MonnifyReservedAccountRequest>(true, HttpMethod.Post, "bank-transfer/reserved-accounts", request);
            if(response.RequestSuccessful && response.ResponseMessage == "success")
            {
                return response.ResponseBody;
            }

            _logger.LogError("Monnify account reservation has failed");
            throw new Exception("Monnify account reservation has failed");
        }
    }
 }