﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VATEEP.Repository.Users
{
    public class AdminUserModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }        
        public string LastName { get; set; }        
        public string Email { get; set; }
        public string Role { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
