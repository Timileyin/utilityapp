﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using iqpay.Data.UserManagement.Model;
using iqpay.Repository.Interface;
using Microsoft.AspNetCore.Identity;
using iqpay.Data.Entities.Payment;
using iqpay.Data.Entities.Services;

namespace iqpay.Repository.Users
{
    public class DealerRepository : IDealerRepository
    {
        private readonly IRepository<ApplicationUser> _repository;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IRepository<Services> _servicesRepository;
        private readonly IRepository<Vendor> _vendorRepo;
        private readonly IRepository<DealerCommission> _dealerCommissionRate;

        public DealerRepository(IRepository<DealerCommission> dealerCommissionRate, IRepository<ApplicationUser> repository, UserManager<ApplicationUser> userManager, IRepository<Services> servicesRepository, IRepository<Vendor> vendorRepo)
        {
            _repository = repository;
            _userManager = userManager;
            _dealerCommissionRate = dealerCommissionRate;
            _servicesRepository = servicesRepository;
            _vendorRepo = vendorRepo;
        }
        public async Task<ApplicationUser> FetchDealerDetails(long dealerId)
        {
            var dealer = await _repository.GetAsync(dealerId);
            return dealer;
        }

        public async Task<List<Services>> FetchCommission(long dealerId)            
        {            
            var services =  await _servicesRepository.GetAll().Include(x=>x.Vendor).Include(x=>x.DealerCommissions).Where(x=>!x.Vendor.IsCommissionAhead).ToListAsync();                     
            return services.ToList();
        }

        public async Task<List<Vendor>> FetchVendorCommission(long dealer)
        {            
            var vendors =  await _vendorRepo.GetAll().Include(x=>x.DealerCommissions).Where(x=>x.IsCommissionAhead).ToListAsync();                     
            return vendors.ToList();
        }

        public async Task<DealerCommission> FetchVendorDealerCommission(long dealerId, AccountType dealerType, long vendorId)
        {
            var vendorCommissions = await FetchVendorCommission(dealerId);
            DealerCommission dealerCommission = vendorCommissions.FirstOrDefault(x=>x.Id == vendorId)?.DealerCommissions?.FirstOrDefault(x=>x.VendorId == vendorId && x.UserId == dealerId);

            if(dealerCommission == null){
                var vendor = vendorCommissions.FirstOrDefault(x=>x.Id == vendorId);
                if(dealerType == AccountType.AGENT)
                {
                    
                    dealerCommission = new DealerCommission{
                        Commission = vendor.AgentCommission,
                        CommissionCap = vendor.AgentCommissionCap
                    }; 
                }

                else if(dealerType == AccountType.DEALER)
                {
                    dealerCommission = new DealerCommission{
                        Commission = vendor.DealerCommission,
                        CommissionCap = vendor.DealerCommissionCap
                    }; 
                }

                else if(dealerType == AccountType.SUPER_DEALER)
                {
                    dealerCommission = new DealerCommission{
                        Commission = vendor.SuperDealerCommission,
                        CommissionCap = vendor.SuperDealerCommissionCap
                    }; 
                }
            }

            return  dealerCommission;
        }

        public async Task<DealerCommission> FetchServiceCommission(long dealerId, AccountType dealerType, long serviceId)
        {
            var serviceCommissions = await FetchCommission(dealerId);
            DealerCommission dealerCommission = serviceCommissions.FirstOrDefault(x=>x.Id == serviceId)?.DealerCommissions?.FirstOrDefault(x=>x.ServiceId == serviceId && x.UserId == dealerId);

            if(dealerCommission == null)
            {
                var service = await _servicesRepository.GetAsync(serviceId); //serviceCommissions.FirstOrDefault(x=>x.Id == serviceId);
                if(dealerType == AccountType.AGENT)
                {                    
                    dealerCommission = new DealerCommission{
                        Commission = service.DefaultAgentCommission,
                        CommissionCap = service.DefaultAgentCommissionCap
                    }; 
                }

                else if(dealerType == AccountType.DEALER)
                {
                    dealerCommission = new DealerCommission{
                        Commission = service.DefaultDealerCommission,
                        CommissionCap = service.DefaultDealerCommissionCap
                    }; 
                }

                else if(dealerType == AccountType.SUPER_DEALER)
                {
                    dealerCommission = new DealerCommission{
                        Commission = service.DefaultSuperDealerCommission,
                        CommissionCap = service.DefaultSuperDealerCommissionCap
                    }; 
                }
            }

            return  dealerCommission;
        }

        public bool UpdateCommission(long? serviceId, long? vendorId, long dealerId, decimal commission, out string message)
        {
            message = string.Empty;            
            var dealerInfo = _userManager.Users.FirstOrDefault(x=>x.Id == dealerId);
            if(dealerInfo != null)
            {
                var superDealerId = dealerInfo.SuperDealerId;                
                if(superDealerId != null)
                {
                    var superDealerCommission = _dealerCommissionRate.FirstOrDefault(x=>x.ServiceId == serviceId && x.VendorId == vendorId && x.UserId == superDealerId);
                    if(superDealerCommission != null)
                    {
                        if(superDealerCommission.Commission <= commission)
                        {
                            message = "You can not set a commission more than your commission for agents/dealers registered under you";
                            return false;
                        }
                    }
                    else
                    {
                        message = "You can not set a commission for agents/dealers registered under you while yours has not be set";
                        return false;
                    }
                }
                else
                {
                    if(serviceId != null)
                    {
                        var service = _servicesRepository.FirstOrDefault(x=>x.Id == serviceId);
                        if(service.Commission <= commission)
                        {
                            message = "You can not set a commission above the maximum commission for super dealers";
                            return false;
                        }
                    }else if(vendorId != null)
                    {
                        var vendor = _vendorRepo.FirstOrDefault(x=>x.Id == vendorId);
                        if(vendor.Commission <= commission)
                        {
                            message = "You can not set a commission above the maximum commission for super dealers";
                            return false;
                        }
                    }                    

                }

                var dealerCommission = _dealerCommissionRate.GetAll().Include(x=>x.User).FirstOrDefault(x=>x.UserId == dealerId && x.ServiceId == serviceId && x.VendorId == vendorId);
            
                if(dealerCommission != null)
                {
                    dealerCommission.Commission = commission;
                    _dealerCommissionRate.Update(dealerCommission);
                }
                else
                {
                    dealerCommission = new DealerCommission
                    {
                        Commission = commission,
                        ServiceId = serviceId,
                        VendorId = vendorId,
                        UserId = dealerId                        
                    };

                    _dealerCommissionRate.Insert(dealerCommission);
                }
                _dealerCommissionRate.SaveChanges();
                return true;
            }else
            {
                message = "Dealer info could not be fetched";
                return false;
            }
            
        }

        public bool UpdateCommissionCap(long? serviceId, long? vendorId, long dealerId, decimal cap, out string message)
        {
            message = string.Empty;            
            var dealerInfo = _userManager.Users.FirstOrDefault(x=>x.Id == dealerId);
            if(dealerInfo != null)
            {
                var superDealerId = dealerInfo.SuperDealerId;                
                if(superDealerId != null)
                {
                    var superDealerCommission = _dealerCommissionRate.FirstOrDefault(x=>x.ServiceId == serviceId && x.VendorId == vendorId && x.UserId == superDealerId);
                    if(superDealerCommission != null)
                    {
                        if(superDealerCommission.CommissionCap <= cap)
                        {
                            message = "You can not set a commission more than your commission for agents/dealers registered under you";
                            return false;
                        }
                    }
                    else
                    {
                        message = "You can not set a commission for agents/dealers registered under you while yours has not be set";
                        return false;
                    }
                }
                else
                {
                    if(serviceId != null)
                    {
                        var service = _servicesRepository.FirstOrDefault(x=>x.Id == serviceId);
                        if(service.CommissionCap <= cap)
                        {
                            message = "You can not set a commission above the maximum commission for super dealers";
                            return false;
                        }
                    }

                    if(vendorId != null)
                    {
                        var vendor = _vendorRepo.FirstOrDefault(x=>x.Id == vendorId);
                        if(vendor.CommissionCap <= cap)
                        {
                            message = "You can not set a commission above the maximum commission for super dealers";
                            return false;
                        }
                    }                    
                }

                var dealerCommission = _dealerCommissionRate.GetAll().Include(x=>x.User).FirstOrDefault(x=>x.UserId == dealerId && x.ServiceId == serviceId && x.VendorId == vendorId);
            
                if(dealerCommission != null)
                {
                    dealerCommission.CommissionCap = cap;
                    _dealerCommissionRate.Update(dealerCommission);
                }
                else
                {
                    dealerCommission = new DealerCommission
                    {
                        Commission = cap,
                        ServiceId = serviceId,
                        VendorId = vendorId,
                        UserId = dealerId                        
                    };

                    _dealerCommissionRate.Insert(dealerCommission);
                }
                _dealerCommissionRate.SaveChanges();
                return true;
            }else
            {
                message = "Dealer info could not be fetched";
                return false;
            }
            
        }

        public async Task<List<ApplicationUser>> FetchDealers(long? superDealerId)
        {
            var dealers = await _repository.GetAllListAsync(x => (x.SuperDealerId == superDealerId || superDealerId == null) && x.AccountType == AccountType.DEALER);
            return dealers;
        }

        public async Task<bool> RemoveDealer(long dealerId)
        {            
            var dealer = await FetchDealerDetails(dealerId);
            await _repository.DeleteAsync(dealer);
            return true;
        }

        public async Task<bool> UpdateDealer(ApplicationUser user)
        {
            var updated = false;
            try
            {
                var dealer = await _repository.GetAsync(user.Id);
                if (dealer != null)
                {
                    dealer.Name = user.Name;
                    dealer.Email = user.Email;
                    dealer.PhoneNumber = user.PhoneNumber;
                    dealer.Address = user.Address;
                    dealer.LockoutEnabled = user.LockoutEnabled;
                    dealer.LockoutEnd = user.LockoutEnd;
                    await _userManager.SetLockoutEndDateAsync(dealer, user.LockoutEnd); 
                    await _userManager.SetLockoutEnabledAsync(dealer, true); 
                    await _userManager.UpdateAsync(dealer);
                    updated = true;
                }
            }
            catch(Exception ex)
            {
                updated = false;
            }

            return updated;
        }
    }
}
