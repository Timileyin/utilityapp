﻿using iqpay.Data.UserManagement.Model;
using iqpay.Repository.Interface;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iqpay.Repository.Users
{
    public class UserRepository : IUserRepository
    {
        private IRepository<ApplicationUser> _userRepository;
        private IRepository<DeviceToken> _deviceTokenRepository;
        private IRepository<ApplicationUserRole> _userRoleRepository;
        private IRepository<ApplicationRole> _roleRepository;
        private UserManager<ApplicationUser> _userManager;
        private IRepository<ThridPartyAPIKeys> _apiKeys;

        public UserRepository(IRepository<ApplicationUser> userRepository, IRepository<DeviceToken> deviceTokenRepository, IRepository<ApplicationUserRole> userRoleRepository, IRepository<ApplicationRole> roleRepository, UserManager<ApplicationUser> userManager, IRepository<ThridPartyAPIKeys> apiKeys)
        {
            _userRepository = userRepository;
            _deviceTokenRepository = deviceTokenRepository;
            _roleRepository = roleRepository;
            _userManager = userManager;
            _userRoleRepository = userRoleRepository;
            _apiKeys = apiKeys;
        }


        public async Task RemoveDeviceToken(long userId, string token)
        {
            var deviceToken = _deviceTokenRepository.FirstOrDefault(x => x.UserId == userId || x.Token == token);
            if(deviceToken != null)
            {
                _deviceTokenRepository.Remove(deviceToken);
                await _deviceTokenRepository.SaveChangesAsync();
            }            
        }        


        public async Task<ThridPartyAPIKeys> FetchThirdPartyAPIKEY(int userId)
        {
            var apikey = await _apiKeys.GetAll().FirstOrDefaultAsync(x => x.UserId == userId);
            return apikey;
        }       

        public async Task<ThridPartyAPIKeys> GenerateThridAPIKEY(int userId)
        {

            var apiKey = new ThridPartyAPIKeys
            {
                PublicAPIKEY = "SECRETAPIKEY",
                SecretAPIKEY = "PUBLICAPIKEY",
                UserId = userId
            };

            var key = await _apiKeys.InsertOrUpdateAsync(apiKey);
            await _apiKeys.SaveChangesAsync();

            return key.Entity;
        }
       
        public async Task SaveDeviceToken(DeviceType deviceType, string deviceName, string deviceToken, long userId)
        {
            var tokenExists = await _deviceTokenRepository.GetAllListAsync(x => x.UserId == userId && x.Token == deviceToken);
            if (!tokenExists.Any())
            {
                await _deviceTokenRepository.InsertOrUpdateAsync(new DeviceToken
                {
                    DeviceName = deviceName,
                    DeviceType = deviceType,
                    Token = deviceToken,
                    UserId = userId
                });

                await _deviceTokenRepository.SaveChangesAsync();
            }            
        }

        public List<ApplicationUser> FetchAdminUser()
        {
            var adminUsers = _roleRepository.GetAllList(x=>x.Name == "SuperAdmin" || x.Name == "Admin")
                .Join(_userRoleRepository.GetAllList(), x=>x.Id, y=>y.RoleId, (x,y)=> new { RoleId = x.Id , UserId = y.UserId, Role = x.Name })
                .Join(_userRepository.GetAllList(), x=>x.UserId, y=>y.Id , (x,y)=>y).ToList();

            return adminUsers;
        }

        public ApplicationUser FetchAdminUser(long id)
        {
            var adminUsers = _roleRepository.GetAllList(x => x.Name == "SuperAdmin" || x.Name == "Admin")
               .Join(_userRoleRepository.GetAllList(), x => x.Id, y => y.RoleId, (x, y) => new { RoleId = x.Id, UserId = y.UserId })
               .Join(_userRepository.GetAllList(x=>x.Id == id), x => x.UserId, y => y.Id, (x, y) => y).ToList();

            return adminUsers.FirstOrDefault(x=>x.Id == id);
        }

        public async Task<List<ApplicationUser>> FetchDealers(long userId)
        {
            var dealers = await _userRepository.GetAllListAsync(x => x.SuperDealerId == userId && x.AccountType == AccountType.DEALER);
            return dealers;
        }

        public async Task<List<ApplicationUser>> FetchSuperDealers()
        {
            var superDealers = await _userRepository.GetAllListAsync(x => x.AccountType == AccountType.SUPER_DEALER);
            return superDealers;
        }

        public async Task<List<ApplicationUser>> FetchAgents(long userId)
        {
            var agents = await _userRepository.GetAllListAsync(x => x.SuperDealerId == userId && x.AccountType == AccountType.AGENT);
            return agents;
        }
    }
}
