﻿using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.UserManagement.Model;
using iqpay.Repository.Interface;


namespace iqpay.Repository.Users
{
    public interface IUserRepository: IAutoDependencyRegister
    {
        Task SaveDeviceToken(DeviceType deviceType, string deviceName, string deviceToken, long userId);
        Task RemoveDeviceToken(long userId, string deviceToken);        
        List<ApplicationUser> FetchAdminUser();
        ApplicationUser FetchAdminUser(long id);
        Task<List<ApplicationUser>> FetchDealers(long userId);
        Task<List<ApplicationUser>> FetchAgents(long userId);
        Task<List<ApplicationUser>> FetchSuperDealers();
    }
}
