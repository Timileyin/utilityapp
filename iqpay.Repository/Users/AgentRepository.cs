﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.UserManagement.Model;
using iqpay.Repository.Interface;
using Microsoft.AspNetCore.Identity;

namespace iqpay.Repository.Users
{
    public class AgentRepository:IAgentRepository
    {
        private readonly IRepository<ApplicationUser> _repository;
        private readonly UserManager<ApplicationUser> _userManager;

        public AgentRepository(IRepository<ApplicationUser> repository, UserManager<ApplicationUser> userManager)
        {
            _repository = repository;
            _userManager = userManager;
        }

        public async Task<ApplicationUser> FetchAgent(long dealerId)
        {
            var dealer = await _repository.GetAsync(dealerId);
            return dealer;
        }

        public async Task<ApplicationUser> FetchAgentDetails(long dealerId)
        {
            var agent = await _repository.GetAsync(dealerId);
            return agent;
        }

        public async Task<List<ApplicationUser>> FetchAgents(long? superDealerId)
        {
            var dealers = await _repository.GetAllListAsync(x => (x.SuperDealerId == superDealerId || superDealerId == null) && x.AccountType == AccountType.AGENT);
            return dealers;
        }

        public async Task<bool> RemoveAgent(long dealerId)
        {

            var dealer = await FetchAgent(dealerId);
            await _repository.DeleteAsync(dealer);
            return true;
        }

        public async Task<bool> UpdateAgent(ApplicationUser user)
        {
            var updated = false;
            try
            {
                var dealer = await _repository.GetAsync(user.Id);
                if (dealer != null)
                {
                    dealer.Name = user.Name;
                    dealer.Email = user.Email;
                    dealer.PhoneNumber = user.PhoneNumber;  
                    await _userManager.SetLockoutEndDateAsync(dealer, user.LockoutEnd); 
                    await _userManager.SetLockoutEnabledAsync(dealer, true);          
                    await _userManager.UpdateAsync(dealer);
                                        
                    updated = true;
                }
            }
            catch (Exception ex)
            {
                updated = false;
            }

            return updated;
        }
    }
}
