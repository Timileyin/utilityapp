using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.UserManagement.Model;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Users
{
    public class HouseholdRepository:IHouseholdRepository
    {
        private readonly IRepository<ApplicationUser> _userRepo;
        private readonly UserManager<ApplicationUser> _userManager;
        public HouseholdRepository(UserManager<ApplicationUser> userManager, IRepository<ApplicationUser> userRepo)    
        {
            _userManager = userManager;
            _userRepo = userRepo;
        }
        public List<ApplicationUser> FetchUsers()
        {
            var users = _userManager.Users.Where(x=>x.AccountType == AccountType.REGULAR).ToList();
            return users;
        }
        public ApplicationUser FetchUsersDetails(long dealerId)
        {
            var user = _userManager.Users.FirstOrDefault(x=>x.AccountType == AccountType.REGULAR && x.Id == dealerId);
            return user; 
        }    

        public async Task UpdateUser(ApplicationUser user)
        {
            var updated = false;
            try
            {
                var dealer =  _userManager.Users.FirstOrDefault(x=>x.Id == user.Id);
                if (dealer != null)
                {
                    dealer.Name = user.Name;
                    dealer.Email = user.Email;
                    dealer.PhoneNumber = user.PhoneNumber;  
                    await _userManager.SetLockoutEndDateAsync(dealer, user.LockoutEnd); 
                    await _userManager.SetLockoutEnabledAsync(dealer, true);          
                    await _userManager.UpdateAsync(dealer);
                     
                }
            }
            catch (Exception ex)
            {
                updated = false;
            }

        }
        public void RemoveUser( long dealerId)
        {
            var dealer =  FetchUsersDetails(dealerId);
            _userRepo.DeleteAsync(dealer);
            _userRepo.SaveChangesAsync();
        }
        
    }
}