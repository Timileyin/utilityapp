﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using iqpay.Data.UserManagement.Model;
using iqpay.Repository.Interface;
using Microsoft.AspNetCore.Identity;

namespace iqpay.Repository.Users
{
    public class SuperAgentRepository:ISuperDealerRepository
    {
        private readonly IRepository<ApplicationUser> _repository;
        private readonly UserManager<ApplicationUser> _userManager;

        public SuperAgentRepository(IRepository<ApplicationUser> repository, UserManager<ApplicationUser> userManager)
        {
            _repository = repository;
            _userManager = userManager;
        }

        public async Task<ApplicationUser> FetchSuperDealer(long dealerId)
        {
            var dealer = await _repository.GetAsync(dealerId);
            return dealer;
        }

        public async Task<ApplicationUser> FetchSuperDealerDetails(long dealerId)
        {
            var agent = await _repository.GetAsync(dealerId);
            return agent;
        }

        public async Task<List<ApplicationUser>> FetchSuperDealer()
        {
            var users = _userManager.Users.Where(x=>x.AccountType == AccountType.SUPER_DEALER).ToList(); //GetUsersInRoleAsync(RoleTypes.IQISUPERDEALER.GetDescription().ToUpper());
            return users.ToList();
        }

        public async Task<bool> RemoveSuperDealer(long dealerId)
        {
            var dealer = await FetchSuperDealerDetails(dealerId);
            await _repository.DeleteAsync(dealer);
            return true;
        }

        public async Task<bool> UpdateSuperDealer(ApplicationUser user)
        {
            var updated = false;
            try
            {
                var dealer = await _repository.GetAsync(user.Id);
                if (dealer != null)
                {
                    dealer.Name = user.Name;
                    dealer.Email = user.Email;
                    dealer.PhoneNumber = user.PhoneNumber;
                    await _userManager.SetLockoutEnabledAsync(dealer, true);
                    await _userManager.SetLockoutEndDateAsync(dealer, user.LockoutEnd);
                    await _userManager.UpdateAsync(dealer);
                    updated = true;
                }
            }
            catch (Exception ex)
            {
                updated = false;
            }

            return updated;
        }
    }
}
