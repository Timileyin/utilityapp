using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.UserManagement.Model;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Users
{
    public interface IPartnershipRegRepo:IAutoDependencyRegister
    {
        Task<PartnershipRegistration> GetPartnershipRegistration(long Id);
        Task<List<PartnershipRegistration>> FetchPartnershipRegistration();        
        Task<List<PartnershipRegistration>> FetchPartnershipRegistration(long? superDealerId);
        void CreateNewPartnershipRequest(PartnershipRegistration reg);
        Task<List<PartnershipRegistration>> GetPartnershipRegistrationByBVN(string bvn, string email, string nin);
        
    }
}