﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.UserManagement.Model;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Users
{
    public interface ISuperDealerRepository:IAutoDependencyRegister
    {
        Task<ApplicationUser> FetchSuperDealer(long dealerId);
        Task<ApplicationUser> FetchSuperDealerDetails(long dealerId);
        Task<List<ApplicationUser>> FetchSuperDealer();
        Task<bool> RemoveSuperDealer(long dealerId);
        Task<bool> UpdateSuperDealer(ApplicationUser user);

    }
}
