using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.UserManagement.Model;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Users
{
    public interface IHouseholdRepository:IAutoDependencyRegister
    {
        List<ApplicationUser> FetchUsers();
        ApplicationUser FetchUsersDetails(long dealerId);       
        Task UpdateUser(ApplicationUser user);
        void RemoveUser(long dealerId);
    }
}
