using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.UserManagement.Model;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Users
{
    public class PartnershipRegRepo:IPartnershipRegRepo
    {
        private readonly IRepository<PartnershipRegistration> _partnerRegRepo;
        public PartnershipRegRepo(IRepository<PartnershipRegistration> partnerRegRepo)
        {
            _partnerRegRepo = partnerRegRepo;
        }
        
        public async Task<PartnershipRegistration> GetPartnershipRegistration(long Id)
        {
            var partnerReg = await _partnerRegRepo.GetAsync(Id);
            return partnerReg;
        }

        

        public async Task<List<PartnershipRegistration>> FetchPartnershipRegistration()
        {
            var partnerRegRepo = await _partnerRegRepo.GetAllListAsync();
            return partnerRegRepo;
        }

        public async Task<List<PartnershipRegistration>> FetchPartnershipRegistration(long? superDealerId)
        {
            var partnerRegRepo = await _partnerRegRepo.GetAllListAsync(x=>x.SuperDealerId == superDealerId);
            return partnerRegRepo;
        }
                
        public void CreateNewPartnershipRequest(PartnershipRegistration reg)
        {
            _partnerRegRepo.Insert(reg);
            _partnerRegRepo.SaveChanges();
        }

        public Task<List<PartnershipRegistration>> GetPartnershipRegistrationByBVN(string bvn, string email, string nin)
        {
            var partnerReg = _partnerRegRepo.GetAllListAsync(x=>x.BVN == bvn || x.Email == email || x.NIN == nin);
            return partnerReg;
        }
    }
}