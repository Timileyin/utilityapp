﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.UserManagement.Model;
using iqpay.Repository.Interface;

namespace iqpay.Repository.Users
{
    public interface IAgentRepository:IAutoDependencyRegister
    {
        Task<List<ApplicationUser>> FetchAgents(long? superDealerId);
        Task<ApplicationUser> FetchAgentDetails(long dealerId);
        Task<bool> UpdateAgent(ApplicationUser user);
        Task<bool> RemoveAgent(long dealerId);
    }
}
