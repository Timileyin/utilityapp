﻿using iqpay.Data.UserManagement.Model;
using iqpay.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using iqpay.Data.Entities.Payment;
using iqpay.Data.Entities.Services;

namespace iqpay.Repository.Users
{
    public interface IDealerRepository:IAutoDependencyRegister
    {
        Task<List<ApplicationUser>> FetchDealers(long? superDealerId);
        Task<ApplicationUser> FetchDealerDetails(long dealerId);
        Task<bool> UpdateDealer(ApplicationUser user);
        Task<bool> RemoveDealer(long dealerId);
        Task<List<Services>> FetchCommission(long dealerId);
        Task<List<Vendor>> FetchVendorCommission(long dealerId);
        Task<DealerCommission> FetchVendorDealerCommission(long dealerId, AccountType dealerType,long vendorId);
        Task<DealerCommission> FetchServiceCommission(long dealerId, AccountType dealerType ,long serviceId);
        bool UpdateCommission(long? serviceId,long? vendorId, long dealerId, decimal commission, out string message);
        bool  UpdateCommissionCap(long? serviceId, long? vendorId, long dealerId, decimal cap, out string message);
    }
}
