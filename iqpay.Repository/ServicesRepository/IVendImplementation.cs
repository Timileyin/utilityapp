﻿using System;
using System.Threading.Tasks;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;
using System.Collections.Generic;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.ServicesRepository;

namespace iqpay.Repository.ServicesRepository
{
    public interface IVendImplementation:IAutoDependencyRegister
    {
        VendResult VendNomiWorld(string vendCode,string serviceCode, decimal amount, string customerNumber, string serviceExtra);
        decimal FetchProcessorBalance(ProcessorsEnum processorEnum, out string message);
        VendResult VendProduct(string vendCode);
        List<VendingLogs> FetchVendingLog(long? userId = null, DateTime? startDate = null, DateTime? endDate = null);
        Task<List<VendingLogs>> FetchVendingLogsProcessedBy(long processorId, DateTime? startDate = null, DateTime? endDate = null);
    }
}
