﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iqpay.Data.Entities.Payment;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;
using Microsoft.EntityFrameworkCore;

namespace iqpay.Repository.ServicesRepository
{
    public class ServicesRepository:IServicesRepository
    {
        private IRepository<Services> _repo;
        private IRepository<Processor> _processorRepository;
        private IRepository<ProcessorsServiceCodes> _processorServiceCodeRepository;
        private IRepository<VendingLogs> _vendingLogRepository;
        private IRepository<DealersServices> _dealerServicesRepo; 
        
        public ServicesRepository(IRepository<Services> repo, IRepository<DealersServices> dealerServicesRepo, IRepository<VendingLogs> vendingLogRepository, IRepository<ProcessorsServiceCodes> processorServiceCodeRepository, IRepository<Processor> processorRepository)
        {
            _repo = repo;
            _vendingLogRepository = vendingLogRepository;
            _processorRepository = processorRepository;
            _processorServiceCodeRepository = processorServiceCodeRepository;
            _dealerServicesRepo = dealerServicesRepo;
        }

        public async Task<List<DealersServices>> FetchDealersServices(long id)
        {
            var dealersServices = await _dealerServicesRepo.GetAllListAsync(x=>x.UserId == id);
            return dealersServices;
        }        

        public void UpdateDealerServiceChecklist(long id, List<long> vendorIds)
        {
            var currentCheckList = _dealerServicesRepo.GetAllList(x=>x.UserId == id);
            foreach(var vendor in currentCheckList)
            {
                if(!vendorIds.Contains(vendor.Id))
                {
                    _dealerServicesRepo.Remove(vendor);  
                }                              
            }

            foreach(var vendorId in vendorIds)
            {
                if(!currentCheckList.Any(x=>x.Id == vendorId))
                {
                    _dealerServicesRepo.Insert(new DealersServices{
                        VendorId = vendorId,
                        UserId = id
                    });                    
                }
            }

            _dealerServicesRepo.SaveChanges();            
        }

        public async Task UpdateVendDetails(VendingLogs log)
        {
            var vendingLog = _vendingLogRepository.GetById(log.Id);
            if(vendingLog != null)
            {
                vendingLog.Status = log.Status;
                vendingLog.ProcessorId = log.ProcessorId;
                vendingLog.AgentCommission = vendingLog.AgentCommission;
                vendingLog.SuperDealerCommission = vendingLog.SuperDealerCommission;
                vendingLog.DealerCommission = vendingLog.DealerCommission;

                await _vendingLogRepository.InsertOrUpdateAsync(vendingLog);
                await _vendingLogRepository.SaveChangesAsync();
            }
        }

        public async Task<int> FetchOrderCount(long serviceId){
            var count = await _vendingLogRepository.GetAll().Where(x=>x.ServiceId == serviceId && (x.Status == VendingStatusEnum.OrderPlaced ||  x.Status == VendingStatusEnum.OrderShipped || x.Status == VendingStatusEnum.OrderProcessed || x.Status == VendingStatusEnum.OrderReceived)).CountAsync();
            return count;
        }

        public bool UpdateServiceCodes(long id, List<ProcessorsServiceCodes> serviceCodes, out string message)
        {
            message = string.Empty;
            var service = _repo.GetById(id);
            if(service == null)
            {
                message = "Service not found";
                return false;
            }
            
            var processorCodes = _processorServiceCodeRepository.GetAllList(x => x.ServiceId == id);
            foreach(var code in processorCodes)
            {
                _processorServiceCodeRepository.Remove(code);
                _processorServiceCodeRepository.SaveChanges();
            }

            foreach(var code in serviceCodes)
            {
                if(!string.IsNullOrWhiteSpace(code.Code))
                {
                    code.Processor = null;
                    _processorServiceCodeRepository.Insert(code);
                    _processorServiceCodeRepository.SaveChanges();
                }
                
            }

            return true;

        }

        
        public List<ProcessorsServiceCodes> FetchServiceProcessorCode(long serviceId)
        {
            var processorCodes = from s in _processorRepository.GetAllList(s=>s.Status == ServiceStatusEnum.Activated)
                                 join c in _processorServiceCodeRepository.GetAllList(x=>x.ServiceId == serviceId)
                                 on s.Id equals c.ProcessorId into sc
                                 from c in sc.DefaultIfEmpty()
                                 select new ProcessorsServiceCodes { Code = c?.Code, ProcessorId = s.Id, Processor = s, ServiceId = serviceId};

            return processorCodes.ToList();
        }

        public async Task CreateService(Services service)
        {
            //service.Status = ServiceStatusEnum.Deactivated;
            await _repo.InsertAsync(service);
            await _repo.SaveChangesAsync();
        }

        public List<Services> FetchSerivceTypes()
        {
            throw new NotImplementedException();
        }

        public async Task<Services> FetchService(long id)
        {            
            var service = await _repo.GetAll().Include(x=>x.Vendor).Include("ProcessorCodes.Processor").Include(x=>x.DealerCommissions).FirstOrDefaultAsync(x=>x.Id == id);
            return service;            
        }

        public async Task<List<Services>> FetchServicesInType(int serviceType)
        {
            var services = await _repo.GetAll().Include(x=>x.Vendor).Where(x => x.Type == (ServiceType)serviceType).ToListAsync();
            return services;
        }

        public List<Services> ServicesBasedOnCommissionMode(CommissionMode mode)
        {
            var services =  _repo.GetAll().Include(x=>x.Vendor).Where(x => x.CommissionMode == mode).ToList();
            return services;
        }


        public async Task<List<Services>> FetchServices()
        {
            var services = await _repo.GetAll().Include(x => x.Vendor).Include("ProcessorCodes.Processor").ToListAsync();
            return services;
        }

        public async Task<List<Services>> FetchVendorServices(long vendorId)
        {
            var services = await _repo.GetAllListAsync(x => x.VendorId == vendorId);
            return services;
        }

        public async Task RemoveService(long id)
        {
            _repo.Remove(id);
            await _repo.SaveChangesAsync();

        }

        public async Task UpdateService(Services service)
        {
            await _repo.UpdateAsync(service);
            await _repo.SaveChangesAsync();

        }

        public VendingLogs FetchVending(string code)
        {
            var vendingLog = _vendingLogRepository.GetAll().Include(x=>x.Service.Vendor).Include("Service.ProcessorCodes.Processor").Include(x=>x.User).FirstOrDefault(x => x.VendingCode == code);
            return vendingLog;
        }

        public string GenerateVendingCode(string prefix)
        {
            var randomNumber = new Random().Next(1000000, 9999999);
            var vendCode = prefix + randomNumber.ToString();
            while (FetchVending(vendCode) != null)
            {
                randomNumber = new Random().Next(1000000, 9999999);
                vendCode = prefix + randomNumber.ToString();
            }

            return vendCode;
        }

        public VendingLogs InitiateVendService(long serviceId, decimal amount, string customerNumber, long userId, string accountName, string accountAddress, string accountAddress2, string collectionPoint, int? numberOfPins, DeliveryOption? deliveryOption, decimal? deliveryFee, string state = null, string lga = null)
        {
            try
            {
                var vendingCode = GenerateVendingCode("VEN");
                var addedEntity = _vendingLogRepository.Insert(new VendingLogs
                {
                    Amount = amount,
                    CustomerNumber = customerNumber,
                    ServiceId = serviceId,
                    Status = VendingStatusEnum.Initiated,
                    UserId = userId,
                    VendingCode = vendingCode,
                    AccountName = accountName,
                    AccountAddress = accountAddress,
                    AccountAddressLine2 = accountAddress2,
                    CollectionPoint = collectionPoint,
                    NumberOfPins = numberOfPins,
                    DeliveryOption = deliveryOption,
                    DeliveryFee = deliveryFee,
                    State = state,
                    LGA = lga
                });

                _vendingLogRepository.SaveChanges();
                return addedEntity.Entity;
            }
            catch(Exception)
            {
                return null;
            }
            
        }

        public async Task<bool> VendService(int serviceId, decimal amount)
        {
            var service = _repo.GetAll().Include(x => x.ProcessorCodes).FirstOrDefault(x=>x.Id == serviceId);
            if(service != null)
            {
                var serviceProcessors = service.ProcessorCodes.Where(x => string.IsNullOrWhiteSpace(x.Code));
                foreach(var processor in serviceProcessors)
                {
                    if(processor.Processor.Code == "")
                    {

                    }
                }

                return true;
            }

            return false;
        }
    }
}
