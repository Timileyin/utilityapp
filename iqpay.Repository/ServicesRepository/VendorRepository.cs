﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using iqpay.Data.Entities.Payment;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;

using Microsoft.EntityFrameworkCore;

namespace iqpay.Repository.ServicesRepository
{
    public class VendorRepository:IVendorRepository
    {
        private IRepository<Vendor> _repo;
        private IRepository<DealersServices> _dealerServicesRepo; 
        public VendorRepository(IRepository<Vendor> repo, IRepository<DealersServices> dealerServicesRepo)
        {
            _dealerServicesRepo = dealerServicesRepo;
            _repo = repo;
        }

        public async Task ActivateVendor(long id)
        {
            var vendor = _repo.GetById(id);
            vendor.Status = VendorStatusEnum.Activated;

            await _repo.UpdateAsync(vendor);
            await _repo.SaveChangesAsync();
        }

        public async Task CreateVendor(Vendor vendor)
        {
            //vendor.Status = VendorStatusEnum.Deactivated;
            await _repo.InsertAsync(vendor);
            await _repo.SaveChangesAsync();
        }

        public async Task<List<Services>> FetchVendorServices(int vendorId)
        {
            var vendor = await _repo.GetAll().Include(x => x.Serivces).FirstOrDefaultAsync(x => x.Id == vendorId);
            return vendor.Serivces;
        }

        public List<Vendor> VendorsBasedOnCommissionMode(CommissionMode mode)
        {
            var vendors = _repo.GetAll().Include(x => x.Serivces).Where(x => x.Serivces.Any(y => y.CommissionMode == mode &&  y.Status == ServiceStatusEnum.Activated) && x.Status == VendorStatusEnum.Activated).ToList();
            return vendors;
        }        

        public async Task<List<Vendor>> FetchVendorsInType(int serviceType, long? id = null)
        {

            var vendors = await _dealerServicesRepo.GetAll().Include(x=>x.Vendor.Serivces).Where(x=>x.UserId == id || id==null)
            .Select(x=>x.Vendor).Include(x=>x.Serivces)
            .Where(x=>x.Status ==VendorStatusEnum.Activated && x.Serivces.Any(y => y.Type == (ServiceType)serviceType && y.Status == ServiceStatusEnum.Activated)).ToListAsync();            
            foreach(var vendor in vendors)
            {
                vendor.Serivces = vendor.Serivces.Where(x => x.Status == ServiceStatusEnum.Activated && x.Type == (ServiceType)serviceType).ToList();
            }

            return vendors;
        }

        public async Task DeactivateVendor(long id)
        {
            var vendor = _repo.GetById(id);
            vendor.Status = VendorStatusEnum.Deactivated;

            await _repo.UpdateAsync(vendor);
            await _repo.SaveChangesAsync();
        }

        public async Task<Vendor> FetchVendorInfo(long id)
        {
            var vendor = await _repo.GetAsync(id);
            return vendor;
        }

        public async Task<List<Vendor>> FetchVendors()
        {
            var vendors = await _repo.GetAllListAsync();
            return vendors;
        }

        public async Task<List<Vendor>> FetchActiveVendors()
        {
            var vendors  = await _repo.GetAllListAsync(x=>x.Status == VendorStatusEnum.Activated);
            return vendors;
        }

        public async Task RemoveVendor(long id)
        {            
            _repo.Remove(id);
            await _repo.SaveChangesAsync();
        }

        public async Task<List<Vendor>> FetchCommissionAheadVendors()
        {
            return await _repo.GetAllListAsync(x=>x.IsCommissionAhead);            
        }

        public async Task UpdateVendor(long id, Vendor vendor)
        {

            var _vendor = _repo.GetById(id);
            if (_vendor != null)
            {
                _vendor.VendorName = vendor.VendorName;
                _vendor.Description = vendor.Description;
                _vendor.Status = vendor.Status;
                _vendor.VendorWalletBalance = vendor.VendorWalletBalance;
                if(!string.IsNullOrEmpty(vendor.LogoSrc))
                {
                    _vendor.LogoSrc = vendor.LogoSrc;
                }                
                _vendor.IsCommissionAhead = vendor.IsCommissionAhead;
                _vendor.SuperDealerCommission = vendor.SuperDealerCommission;
                _vendor.SuperDealerCommissionCap = vendor.SuperDealerCommissionCap;
                _vendor.DealerCommissionCap = vendor.DealerCommissionCap;
                _vendor.DealerCommission = vendor.DealerCommission;
                _vendor.AgentCommission = vendor.AgentCommission;
                _vendor.AgentCommissionCap = vendor.AgentCommissionCap;
                _vendor.CustomerCommissionCap = vendor.CustomerCommissionCap;
                _vendor.CustomerCommission = vendor.CustomerCommission;
                _vendor.Commission = vendor.Commission;
                _vendor.CommissionCap = vendor.CommissionCap;
                _vendor.EnableHouseHoldBonus = vendor.EnableHouseHoldBonus;
                _vendor.HouseHoldBonusCap = vendor.HouseHoldBonusCap;
                _vendor.HouseHoldBonusDates = vendor.HouseHoldBonusDates;
                _vendor.HouseHoldBonusPercentage = vendor.HouseHoldBonusPercentage;

                await _repo.UpdateAsync(_vendor);
                await _repo.SaveChangesAsync();
            }
        }
    }
}
