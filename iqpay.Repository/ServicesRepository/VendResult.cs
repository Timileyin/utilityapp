namespace iqpay.Repository.ServicesRepository
{
    public class VendResult
    {
        public bool IsSuccessful{get;set;}
        public string Message{get;set;}
        public long? ProcessorId{get;set;}    
    }
}
