﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.Entities.Payment;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;

namespace iqpay.Repository.ServicesRepository
{
    public interface IServicesRepository:IAutoDependencyRegister
    {
        Task<List<Services>> FetchServices();
        Task<Services> FetchService(long id);
        Task RemoveService(long id);
        Task UpdateService(Services service);
        Task CreateService(Services service);
        Task<List<Services>> FetchVendorServices(long vendorId);
        List<Services> FetchSerivceTypes();
        Task<List<Services>> FetchServicesInType(int serviceType);
        List<Services> ServicesBasedOnCommissionMode(CommissionMode mode);
        List<ProcessorsServiceCodes> FetchServiceProcessorCode(long serviceId);
        bool UpdateServiceCodes(long id, List<ProcessorsServiceCodes> serviceCodes, out string message);
        VendingLogs InitiateVendService(long serviceId, decimal amount, string customerNumber, long userId, string accoutnName, string accountAddress, string accountAddress2, string collectionPoint, int? numberOfPins, DeliveryOption? delOption, decimal? deliveryFee, string state, string lga);
        Task<bool> VendService(int serviceId, decimal amount);
        VendingLogs FetchVending(string vendingCode);
        Task UpdateVendDetails(VendingLogs log);
        Task<List<DealersServices>> FetchDealersServices(long id);
        void UpdateDealerServiceChecklist(long id, List<long> serviceId);
        Task<int> FetchOrderCount(long serviceId);
    }
}
