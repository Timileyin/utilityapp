﻿using System;
using System.Linq;
using System.Threading.Tasks;
using iqpay.Core.Utilities;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Processors.ProcessorImplementations;
using Microsoft.Extensions.Configuration;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.Interface;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using iqpay.Data.UserManagement.Model;


namespace iqpay.Repository.ServicesRepository
{
    public class VendImplentation : IVendImplementation
    {
        private readonly IConfiguration _configuration;
        private readonly INomiWorldRequests _nomiWorldRequests;
        private readonly ICapriconRequests _capriconRequests;
        private readonly IVatebraRequests _vatebraRequests;
        private readonly IVatebraEKORequests _vatebraEKORequests;
        private readonly IServicesRepository _servicesRepository;
        private readonly IRepository<VendingLogs> _vendingRepository;
        private readonly IVatebraIbadanRequests _vatebraIbadanRequests;
        private readonly IBuyPowerRequests _buyPower;
        private readonly IShagoRequests _shagoRequests;
        private readonly IFetsRequests _fetRequests;
        private readonly IGreyStoneRequests _greyStoneRquest;
       

        public VendImplentation(IConfiguration configuration,IGreyStoneRequests greyStoneRquest, IFetsRequests fetRequests, IBuyPowerRequests buyPower,IVatebraIbadanRequests vatebraIbadanRequests, IVatebraEKORequests vatebraEKORequests, ICapriconRequests capriconRequests, INomiWorldRequests nomiWorldRequests, IServicesRepository servicesRepository, IVatebraRequests vatebraRequests, IShagoRequests shago, IRepository<VendingLogs> vendingRepository)
        {
            _configuration = configuration;
            _nomiWorldRequests = nomiWorldRequests;
            _servicesRepository = servicesRepository;
            _capriconRequests = capriconRequests;
            _vatebraRequests = vatebraRequests;
            _vendingRepository = vendingRepository;
            _vatebraEKORequests = vatebraEKORequests;
            _vatebraIbadanRequests = vatebraIbadanRequests;
            _buyPower = buyPower;
            _shagoRequests = shago;
            _fetRequests = fetRequests;
            _greyStoneRquest = greyStoneRquest;
        }        

        public decimal FetchProcessorBalance(ProcessorsEnum processorEnum, out string message)
        {
            message = string.Empty;
            try{
                    switch (processorEnum) {
                        case ProcessorsEnum.NomiWorld:

                            var balanceResponse = Task.Run(()=> _nomiWorldRequests.FetchBalance()).Result;
                            if (balanceResponse == null)
                            {
                                message = "Error occurred while fetching processor balance";
                                return 0.00M;
                            }
                            return balanceResponse.Value;

                        case ProcessorsEnum.Capricorn:

                            var capbalanceResponse = Task.Run(() => _capriconRequests.FetchBalance()).Result;
                            if(capbalanceResponse == null)
                            {
                                message = "Error occurred while fetching processor balance";
                                return 0.00M;
                            }
                            return capbalanceResponse.Balance;

                        case ProcessorsEnum.GiftEdge:
                            var giftEgdeBalance = Task.Run(()=>_vatebraRequests.FetchBalance()).Result;                    
                            return giftEgdeBalance;

                        case ProcessorsEnum.VATEBRAEKO:
                            var vatEkoBalance = Task.Run(()=>_vatebraEKORequests.FetchBalance()).Result;                    
                            return vatEkoBalance;

                        case ProcessorsEnum.VATEBRAIBADAN:
                            var vatebraIbadanBalance = Task.Run(()=>_vatebraIbadanRequests.FetchBalance()).Result;                    
                            return vatebraIbadanBalance;               

                        case ProcessorsEnum.BUYPOWER:
                            var buyPowerBalance = Task.Run(() => _buyPower.FetchBalance()).Result;
                            return buyPowerBalance.Balance;
                        
                        case ProcessorsEnum.VATEBRAWAEC:
                            return 0.00M;
                        
                        case ProcessorsEnum.SHAGO:
                            var shageBalance = Task.Run(()=> _shagoRequests.FetchBalance()).Result;
                            return shageBalance.PrimaryBalance;
                        
                        case ProcessorsEnum.FETS:
                            var fetsBalance = Task.Run(()=>_fetRequests.FetchBalance()).Result;
                            return fetsBalance.Balance;

                        case ProcessorsEnum.GRAYSTONE:
                            var grayStoneBalance = Task.Run(()=>_greyStoneRquest.FetchBalance()).Result;
                            return grayStoneBalance.Balance;

                        default:
                            message = "Please supply a valid processor";
                            return 0.00M;
                }
            }catch(Exception ex){

                message = "Error occurred while fetching processor balance";
                return 0.00M;
            }
        }

        public List<VendingLogs> FetchVendingLog(long? userId = null, DateTime? startDate = null, DateTime? endDate = null)
        {
            IEnumerable<VendingLogs> vendingLogs;
            if(startDate == null || endDate == null)                
            {
                endDate = DateTime.Now;
                startDate = DateTime.Today;
            }

            if(userId == null)
            {
                vendingLogs = _vendingRepository.GetAll().Include(x=>x.User).Include(x=>x.Service.Vendor).OrderByDescending(x=>x.DateCreated);
            }else
            {
                vendingLogs = _vendingRepository.GetAll().Include(x=>x.User).Include(x=>x.Service.Vendor).Where(x=>x.UserId == userId).OrderByDescending(x=>x.DateCreated);                
            } 

            vendingLogs = vendingLogs.Where(x=>x.DateCreated >= startDate && x.DateCreated < endDate && x.Status != VendingStatusEnum.Initiated);  
            return vendingLogs.ToList();                             
        }
        
        public VendResult VendProduct(string vendCode)
        {
            var vendingResult = new VendResult();
            var message = "Vending Failed";
            var vendingLog = _servicesRepository.FetchVending(vendCode);
            if(vendingLog != null)
            {
                var processorCodes = vendingLog.Service.ProcessorCodes;
                var vendCompleted = false;
                var codeCounter = 0;
                while (!vendCompleted && codeCounter < processorCodes.Count())
                {
                    var serviceCode = processorCodes[codeCounter];
                    
                    if(serviceCode.Processor.Code == ProcessorsEnum.NomiWorld.GetDescription())
                    {
                        vendingResult = VendNomiWorld(vendCode, serviceCode.Code, vendingLog.Amount, vendingLog.CustomerNumber,vendingLog.Service.ServiceName);                                               
                    }

                    if(serviceCode.Processor.Code == ProcessorsEnum.GiftEdge.GetDescription())
                    {
                        vendingResult = VendGiftEdge(vendCode, vendingLog.Amount, vendingLog.CustomerNumber);                        
                    }

                    if(serviceCode.Processor.Code == ProcessorsEnum.VATEBRAEKO.GetDescription()){
                        vendingResult = VendVatebraEKO(vendCode, vendingLog.Amount, vendingLog.CustomerNumber);
                    }

                    if(serviceCode.Processor.Code == ProcessorsEnum.VATEBRAIBADAN.GetDescription()){
                        vendingResult = VendVatebraIbadan(vendCode, vendingLog.Amount, vendingLog.CustomerNumber);
                    }

                    if(serviceCode.Processor.Code == ProcessorsEnum.BUYPOWER.GetDescription())
                    {
                        vendingResult = VendBuyPower(vendingLog.User.Email, vendingLog.User.UserName, vendingLog.User.PhoneNumber, vendCode, serviceCode.Code, vendingLog.Amount, vendingLog.CustomerNumber);                   
                    }

                    if(serviceCode.Processor.Code == ProcessorsEnum.Capricorn.GetDescription())
                    {
                        vendingResult = VendCapricorn(vendCode, vendingLog.Service.Type, serviceCode.Code, vendingLog.VendingCode, vendingLog.Service.Vendor.VendorName, vendingLog.Amount, vendingLog.CustomerNumber, string.Empty);                        
                    }

                    if(serviceCode.Processor.Code == ProcessorsEnum.SHAGO.GetDescription())
                    {
                        vendingResult = VendShago(vendingLog.User.Email, vendingLog.User.UserName, vendingLog.User.PhoneNumber, vendCode, serviceCode.Code, vendingLog.Amount, vendingLog.CustomerNumber);
                    }

                    if(serviceCode.Processor.Code == ProcessorsEnum.FETS.GetDescription())
                    {
                        vendingResult = VendFets(vendingLog.User.PhoneNumber, vendingLog.VendingCode, serviceCode.Code, vendingLog.Amount, vendingLog.CustomerNumber);
                    }

                    if(serviceCode.Processor.Code == ProcessorsEnum.GRAYSTONE.GetDescription())
                    {
                        vendingResult = VendGrayStone(vendingLog.User.PhoneNumber, vendingLog.AccountName, vendingLog.VendingCode, serviceCode.Code, vendingLog.Amount, vendingLog.CustomerNumber);
                    }

                    if(serviceCode.Processor.Code == ProcessorsEnum.IQIPAYSHIPPING.GetDescription()){
                        vendingResult = VendIQPAYSHIPING(vendingLog.User.PhoneNumber, vendingLog.AccountName, vendingLog.VendingCode, serviceCode.Code, vendingLog.Amount, vendingLog.CustomerNumber);
                    }
                    
                    vendingResult.ProcessorId = serviceCode.Processor.Id;
                    vendCompleted = vendingResult.IsSuccessful;
                    message = vendingResult.Message;                    
                    codeCounter++;
                }
                
                return vendingResult;                                
            }
            vendingResult.Message = message;
            vendingResult.IsSuccessful = false;
            return vendingResult;
        }

        public VendResult VendGrayStone(string phoneNumber, string customerName, string vendCode, string serviceCode, decimal amount, string accountNumber)
        {
            var vendingResult = new VendResult();      
            var vendResponse = Task.Run(()=> _greyStoneRquest.VendProduct(vendCode, customerName, serviceCode,amount, accountNumber, phoneNumber)).Result;
            if (vendResponse != null)
            {
                vendingResult.IsSuccessful = vendResponse.RespCode == "00";
                vendingResult.Message = vendResponse.RespDescription;
                return vendingResult;                
            }
            
            vendingResult.IsSuccessful = false;
            vendingResult.Message = "Processor Failed to process vending";
            return vendingResult;
        }

        public VendResult VendIQPAYSHIPING(string phoneNumber, string customerName, string vendCode, string serviceCode, decimal amount, string accountNumber)
        {
            var vendingResult = new VendResult();
            var vendingDetails = _vendingRepository.GetAll().Include(x=>x.Service).FirstOrDefault(x=>x.VendingCode == vendCode);
            if(vendingDetails != null)
            {                                
                vendingDetails.Status = VendingStatusEnum.OrderPlaced;
                vendingResult.IsSuccessful = true;
                vendingResult.Message = "Order for product has been placed successfully";
                return vendingResult;                
            }            
            vendingResult.IsSuccessful = false;
            vendingResult.Message = "Processor Failed to process vending";
            return vendingResult;
        }

        public VendResult VendFets(string phoneNumber, string vendCode, string serviceCode, decimal amount, string accountNumber)
        {
            var vendingResult = new VendResult();      
            var vendResponse = Task.Run(()=> _fetRequests.VendProduct(vendCode, phoneNumber, serviceCode,amount, accountNumber)).Result;
            if (vendResponse != null)
            {
                vendingResult.IsSuccessful = vendResponse.ResponseCode == 0;
                vendingResult.Message = vendResponse.Message;
                return vendingResult;                
            }
            
            vendingResult.IsSuccessful = false;
            vendingResult.Message = "Processor Failed to process vending";
            return vendingResult;
        }

        public  VendResult VendNomiWorld(string vendCode,string serviceCode, decimal amount, string customerNumber, string serviceExtra)
        {      
            var vendingResult = new VendResult();      
            var vendResponse = _nomiWorldRequests.VendProduct(vendCode, serviceCode, amount, customerNumber, serviceExtra, out string message);
            if (vendResponse != null)
            {
                vendingResult.IsSuccessful = vendResponse.Status.Name == "Successful";
                vendingResult.Message = message;
                return vendingResult;                
            }
            
            vendingResult.IsSuccessful = false;
            vendingResult.Message = "Processor Failed to process vending";
            return vendingResult;
        }

        public async Task<List<VendingLogs>> FetchVendingLogsProcessedBy(long processorId, DateTime? startDate = null, DateTime? endDate = null)
        {
            if(startDate == null || endDate == null)                
            {
                endDate = DateTime.Now;
                startDate = DateTime.Today;
            }
            IQueryable<VendingLogs> vendingLogs = _vendingRepository.GetAll().Include(x=>x.User).Include(x=>x.Service.Vendor).Where(x=>x.ProcessorId == processorId && x.Status != VendingStatusEnum.Initiated);
            vendingLogs =  vendingLogs.Where(x=>x.DateCreated >= startDate && x.DateCreated < endDate).OrderByDescending(x=>x.DateCreated);
            return await vendingLogs.ToListAsync(); 
        }

        public VendResult VendShago(string email, string username, string phoneNumber, string vendCode, string serviceCode, decimal amount, string accountNumber)
        {
            var vendResult = new VendResult();
            var vendResponse = Task.Run(()=> _shagoRequests.VendProduct(email, username, phoneNumber, vendCode, serviceCode, amount, accountNumber)).Result;
            vendResult.Message = vendResponse.Message;
            vendResult.IsSuccessful = vendResponse.Status == 200;
            return vendResult;
        }        

        public VendResult VendVatebraEKO(string vendCode, decimal amount, string customerNumber)
        {  
            var vendResult = new VendResult();          
            var vendResponse = Task.Run(()=>_vatebraEKORequests.VendProduct(vendCode, amount, customerNumber)).Result;
            vendResult.Message = vendResponse.Message;
            vendResult.IsSuccessful = vendResponse.Status;
            return vendResult;
        }

        public VendResult VendVatebraIbadan(string vendCode, decimal amount, string customerNumber)
        {  
            var vendResult = new VendResult();          
            var vendResponse = Task.Run(()=>_vatebraIbadanRequests.VendProduct(vendCode, amount, customerNumber)).Result;
            vendResult.Message = vendResponse.Message;
            vendResult.IsSuccessful = vendResponse.Status;
            return vendResult;
        }

        public VendResult VendGiftEdge(string vendCode, decimal amount, string customerNumber)
        {  
            var vendResult = new VendResult();          
            var vendResponse = Task.Run(()=>_vatebraRequests.VendProduct(vendCode, amount, customerNumber)).Result;
            vendResult.Message = vendResponse.Message;
            vendResult.IsSuccessful = vendResponse.Status;
            return vendResult;
        }

        public VendResult VendBuyPower(string email, string username, string phoneNumber, string vendCode, string serviceCode, decimal amount, string accountNumber)
        {
            var vendResult = new VendResult();          
            var vendResponse = Task.Run(()=>_buyPower.VendProduct(email, username, phoneNumber, vendCode, serviceCode, amount, accountNumber)).Result;
            vendResult.Message = vendResponse.Data.ResponseMessage;
            vendResult.IsSuccessful = vendResponse.Status;
            return vendResult;
        }

        public VendResult VendCapricorn(string vendCode,ServiceType type, string serviceCode, string vendRef, string serviceType, decimal amount, string customerNumber, string serviceExtra)
        {
            var vendResult = new VendResult();
            var vendResponse = _capriconRequests.VendProduct(vendCode, type, serviceCode, vendRef, serviceType ,amount, customerNumber, serviceExtra, out string message);
            if(vendResponse != null)
            {
                vendResult.IsSuccessful = vendResponse.Status == "success";
                vendResult.Message = message;                
                return vendResult; 
            }

            vendResult.IsSuccessful = false;
            vendResult.Message = "Processor Failed to process vending";
            return vendResult;
        }
        
    }
}
