﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using iqpay.Data.Entities.Payment;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Interface;

namespace iqpay.Repository.ServicesRepository
{
    public interface IVendorRepository:IAutoDependencyRegister
    {
        Task<List<Vendor>> FetchVendors();
        Task<Vendor> FetchVendorInfo(long id);
        Task CreateVendor(Vendor vendor);
        Task RemoveVendor(long id);
        Task<List<Vendor>> FetchActiveVendors();
        Task DeactivateVendor(long id);
        Task ActivateVendor(long id);
        Task UpdateVendor(long id, Vendor vendor);
        Task<List<Vendor>> FetchCommissionAheadVendors();
        Task<List<Services>> FetchVendorServices(int vendorId);
        Task<List<Vendor>> FetchVendorsInType(int serviceType, long? id = null);
        List<Vendor> VendorsBasedOnCommissionMode(CommissionMode mode);

    }
}
