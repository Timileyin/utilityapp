﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using iqpay.Data;
using iqpay.Data.Entity.Interface;
using iqpay.Repository.Base;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace iqpay.Repository.CoreRepository
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public class Repository<TEntity>: RepositoryBase<TEntity> where TEntity : class, IEntity
    {
        private readonly IQPAYContext _dbContextProvider;

        public Repository(IQPAYContext dbContextProvider)
        {
            _dbContextProvider = dbContextProvider;
        }


        /// <summary>
        /// get the current Entity
        /// <returns><see cref="DbSet{TEntity}"/></returns>
        /// </summary>
        public virtual DbSet<TEntity> Table => _dbContextProvider.Set<TEntity>();

        /// <summary>
        /// get the entity <see cref="IQueryable{TEntity}"/>
        /// </summary>
        /// <returns></returns>
        public override IQueryable<TEntity> GetAll()
        {
            return Table.Where(x => !x.IsDeleted && x.IsActive);
        }

        public override TEntity GetById(long id)
        {
            return Table.Find(id);
        }


        
        public override void ExecuteStoreprocedure(string storeprocedureName, params object[] parameters)
        {
            _dbContextProvider.Database.ExecuteSqlCommand("EXEC " + storeprocedureName, parameters);
        }

        

         
        
        


       
        public override async Task<EntityEntry<TEntity>> InsertOrUpdateAsync(Expression<Func<TEntity, bool>> predicate, TEntity entity)
        {
            return !Table.Any(predicate) && entity.Id == 0
                ? await InsertAsync(entity)
                : await UpdateAsync(entity);
        }
        public override long SaveChanges()
        {
            return _dbContextProvider.SaveChanges();
        }

        public override async Task<long> SaveChangesAsync()
        {
            return await _dbContextProvider.SaveChangesAsync();
        }

        public override EntityEntry<TEntity> Insert(TEntity entity)
        {
            return Table.Add(entity);
        }

        public override Task<EntityEntry<TEntity>> InsertAsync(TEntity entity)
        {
            return Task.FromResult(Table.Add(entity));
        }

        public override long InsertAndGetId(TEntity entity)
        {
            entity = Insert(entity).Entity;
            //entity.IsTransient()
            if (entity.IsTransient())
            {
                _dbContextProvider.SaveChanges();
            }

            return entity.Id;
        }

        public override async Task<long> InsertAndGetIdAsync(TEntity entity)
        {
           var ent = await InsertAsync(entity);
            entity = ent.Entity;
            if (entity.IsTransient())
            {
                await _dbContextProvider.SaveChangesAsync();
            }

            return entity.Id;
        }

        public override long InsertOrUpdateAndGetId(TEntity entity)
        {
            entity = InsertOrUpdate(entity).Entity;
            //entity.IsTransient()
            if (entity.IsTransient())
            {
                _dbContextProvider.SaveChanges();
            }

            return entity.Id;
        }

        public override async Task<long> InsertOrUpdateAndGetIdAsync(TEntity entity)
        {
           var ent = await InsertOrUpdateAsync(entity);
            entity = ent.Entity;
            if (entity.IsTransient())
            {
                await _dbContextProvider.SaveChangesAsync();
            }

            return entity.Id;
        }

        public override EntityEntry<TEntity> Update(TEntity entity)
        {
            AttachIfNot(entity);
            var ent = _dbContextProvider.Entry(entity);
            ent.State = EntityState.Modified;
            return ent;
        }

        public override Task<EntityEntry<TEntity>> UpdateAsync(TEntity entity)
        {
            AttachIfNot(entity);
            var ent= _dbContextProvider.Entry(entity);
            ent.State = EntityState.Modified;
            return Task.FromResult(ent);
        }

        public override void Delete(TEntity entity)
        {
            Update(entity.Id, x => x.IsDeleted = true);
        }


        public override TEntity Update(long id, Action<TEntity> updateAction)
        {
            var entity = Table.Find(id);
            updateAction(entity);
            return entity;
        }

        public override void Delete(long id)
        {
            Update(id, x => x.IsDeleted = true);
        }

        public override Task DeleteAsync(long id)
        {
            return Task.Run(() => Delete(id));

        }
        

        public override void ExecuteRawSqlQuery(string storeprocedureName, object[] param)
        {
            _dbContextProvider.Database.ExecuteSqlCommand("EXEC " + storeprocedureName, param);
        }


        public override async Task<TEntity> UpdateAsync(long id, Func<TEntity, Task> updateAction)
        {
            var entity = await Table.FindAsync(id);
            await updateAction(entity);
            return entity;
        }

        public override async Task<TEntity> UpdateAsync(Expression<Func<TEntity, bool>> predicate, Func<TEntity, Task> updateAction)
        {
            var entity = await Table.FirstOrDefaultAsync(predicate);
            await updateAction(entity);
            return entity;
        }

        public override void DetachItem(TEntity entity)
        {
            _dbContextProvider.Entry(entity).State = EntityState.Detached;
        }


        protected virtual void AttachIfNot(TEntity entity)
        {
            if (!Table.Local.Contains(entity))
            {
                Table.Attach(entity);
            }
        }

        public override void Remove(long id)
        {
            var entity = Table.FirstOrDefault(x => x.Id == id);
            _dbContextProvider.Remove(entity);
        }

        public override void Remove(TEntity entity)
        {
            _dbContextProvider.Remove(entity);
        }

        public override IQueryable<TEntity> GetAllWithoutActive()
        {
            return Table.Where(x => !x.IsDeleted);
        }
    }
}