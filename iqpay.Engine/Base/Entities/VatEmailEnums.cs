﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace iqpay.Engine.Base.Entities
{
    public enum VatEmailTemplateType
    {
        [Description("Account Creation")]
        AccountCreation = 1,
        [Description("Password Reset")]
        PasswordReset,
        [Description("Quote Request Auto Response")]
        QuoteRequestAutoResponse,
        [Description("Attendee Registration")]
        AttendeeRegistration,
        [Description("Resend Confirmation Link")]
        ResendCOnfirmationLink,
        [Description("Payment Completed")]
        PaymentCompleted,
        [Description("Create Support Ticket")]
        CreateSupportTicket,
        [Description("Support Ticket Response")]
        SupportTicketResponse,
        [Description("Admin  Creation")]
        AdminCreation,
        [Description("Reminder Notification")]
        ReminderNotification,
        [Description("Support Ticket Response Admin")]
        SupportTicketResponseAdmin
    }

    public enum VatEmailStatus
    {
        Fresh = 1,
        Sent,
        Failed
    }
}

