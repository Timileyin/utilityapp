﻿using Autofac;
using iqpay.Engine.Base.Entities;
using iqpay.Engine.Repository.Base;
using iqpay.Engine.Repository.EmailRepository;
using iqpay.Engine.Repository.Interface;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;

namespace iqpay.Repository.AutoFacModule
{
    public class GenericRepositoryModule<TEntity, TDbContext> : Module where TEntity : class,IGenericEntity where TDbContext : DbContext
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<GenericRepository<TEntity, TDbContext>>()
                .As<IGenericRepository<TEntity>>()
                .InstancePerLifetimeScope();                       
            
            builder.RegisterAssemblyTypes(typeof(IAutoDependencyRegister).Assembly)
                .AssignableTo<IAutoDependencyRegister>()
                .As<IAutoDependencyRegister>()
                .AsImplementedInterfaces().InstancePerLifetimeScope();
            base.Load(builder);
        }
    }


    public class BillRepositoryModule<TBill, TPayment, TUser, TUserKey, TDbContext> : Module where TBill : VatBill<TUser, TUserKey, TPayment> where TPayment : VatPaymentLog<TUser, TUserKey> where TUser: IdentityUser<TUserKey> where TUserKey: IEquatable<TUserKey> where TDbContext:DbContext
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<GenericRepository<TBill, TDbContext>>()
                .As<IGenericRepository<TBill>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<GenericRepository<TPayment, TDbContext>>()
                .As<IGenericRepository<TPayment>>()
                .InstancePerLifetimeScope();

            builder.RegisterType<Billing<TBill,TPayment,TUser, TUserKey>>()
                .As<IBilling<TBill, TPayment, TUser, TUserKey>>()
                .InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(typeof(IAutoDependencyRegister).Assembly)
                .AssignableTo<IAutoDependencyRegister>()
                .As<IAutoDependencyRegister>()
                .AsImplementedInterfaces().InstancePerLifetimeScope();
            base.Load(builder);
        }
    } 

    
}
