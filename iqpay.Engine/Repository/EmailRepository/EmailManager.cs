﻿using iqpay.Engine.Base.Entities;
using iqpay.Engine.Base.Repository.EmailRepository;
using iqpay.Engine.Repository.Interface;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using SendGrid;
using SendGrid.Helpers.Mail;

namespace iqpay.Engine.Repository.EmailRepository
{
    public class EmailManager<TEMailLog, TEmailTemplate> : IEmailManager<TEMailLog, TEmailTemplate> where TEMailLog : VatEmailLog where TEmailTemplate : VatEmailTemplate
    {
        private readonly IRepository<TEMailLog, TEmailTemplate> _emailLogRepo;
        private readonly ILogger<EmailManager<TEMailLog, TEmailTemplate>> _logger;
        private readonly string _smtpPort;
        private readonly string _username;
        private readonly string _password;
        private readonly string _smtpHost;
        private readonly string _sendGridApi;

        public static bool _mailSent;
        private readonly string _emailFrom;// = "info@nacc.org";
       


        public EmailManager(IRepository<TEMailLog, TEmailTemplate> emailLogRepo, ILogger<EmailManager<TEMailLog, TEmailTemplate>> logger, IConfiguration configuration)
        {
            _emailLogRepo = emailLogRepo;
            _logger = logger;
            _smtpPort = configuration.GetSection("EmailSettings:Port").Value;
            _username = configuration.GetSection("EmailSettings:Username").Value;
            _password = configuration.GetSection("EmailSettings:Password").Value;
            _smtpHost = configuration.GetSection("EmailSettings:Server").Value;
            _emailFrom = configuration.GetSection("EmailSettings:EmailFrom").Value;
            _sendGridApi = configuration.GetSection("SendGrid:APIKEY").Value;
        }

        public async Task SaveEmailTemplate(TEmailTemplate emailTemplate)
        {
            await _emailLogRepo.InsertOrUpdateAsync(emailTemplate);
            await _emailLogRepo.SaveChangesAsync();           
        }

        public async Task DeleteEmailTemplate(TEmailTemplate emailTemplate)
        {
            await _emailLogRepo.DeleteAsync(emailTemplate);
            await _emailLogRepo.SaveChangesAsync();
        }

        public async Task LogEmail(TEMailLog emailLog)
        {
            var email =  await _emailLogRepo.InsertAsync(emailLog);
            await _emailLogRepo.SaveChangesAsync();            
        }

        public VatEmailStatus SendMail(TEMailLog emailLog)
        {
            throw new NotImplementedException();
        }

        public async Task<VatEmailStatus> SendMailAsync(TEMailLog emailLog)
        {
            try
            {
                var sendGridClient = new SendGridClient(_sendGridApi);
                var from = new EmailAddress(_emailFrom, "IQPAY");
                var subject = emailLog.Subject;
                var htmlContent = emailLog.MailBody;
                var to = new EmailAddress(emailLog.Receiver);
                var message = MailHelper.CreateSingleEmail(from, to, subject,null,htmlContent);
                if (emailLog.HasAttachements)
                {
                    var attachments = emailLog.AttachmentLoc.Split(',');
                    foreach (var attt in attachments)
                    {
                        var theAttachment = Path.Combine(Directory.GetCurrentDirectory(), attt);
                        using(var filestram = new FileStream(theAttachment, FileMode.Open, FileAccess.Read))
                        {
                            await message.AddAttachmentAsync(attt, filestram);
                        }                        
                    }
                }

                var response = await sendGridClient.SendEmailAsync(message);
                if(response.StatusCode == HttpStatusCode.Accepted || response.StatusCode == HttpStatusCode.OK)
                {
                    
                    return VatEmailStatus.Sent;
                    
                }
                else
                {
                    return VatEmailStatus.Failed;
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return VatEmailStatus.Failed;
            }
            
        }

        public async Task<TEmailTemplate> CreateEmailTempalteAsync(TEmailTemplate emailTemplate)
        {
            var template = await _emailLogRepo.InsertOrUpdateAsync(emailTemplate);
            await _emailLogRepo.SaveChangesAsync();
            return template.Entity;
        }

        public IQueryable<TEmailTemplate> GetEmailTemplate(VatEmailTemplateType type)
        {
            return _emailLogRepo.GetEMailTemplate(x => x.EmailTemplateType == type);
        }

        public IQueryable<TEmailTemplate> GetEmailTemplates(Expression<Func<TEmailTemplate, bool>> predicate)
        {
            return _emailLogRepo.GetEMailTemplate(predicate);
        }
        
        public IQueryable<TEmailTemplate> GetEmailTemplates()
        {
            return _emailLogRepo.GetAllEmailTemplates();
        }

        public async Task SendBatchMailAsync()
        {
            var unsentEmails = await _emailLogRepo.GetAllListAsync(x => x.Status != VatEmailStatus.Sent && x.DateToSend.Date == DateTime.Now.Date && x.SendImmediately);
            foreach(var email in unsentEmails)
            {
                email.Status = await SendMailAsync(email);
                _emailLogRepo.Update(email);
                _emailLogRepo.SaveChanges();
            }
        }

        public async Task LogBatchEmail(List<TEMailLog> eMailLogs)
        {
            await _emailLogRepo.InsertRangeAsync(eMailLogs);
            await _emailLogRepo.SaveChangesAsync();
        }
    }
}
