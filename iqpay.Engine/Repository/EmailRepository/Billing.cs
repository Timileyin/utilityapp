﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using EmailEngine.Base.Entities.Models;
using iqpay.Core.Utilities;
using iqpay.Engine.Base.Entities;
using iqpay.Engine.Repository.Base;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace iqpay.Engine.Repository.EmailRepository
{
    public class Billing<TBill, TPayment, TUser, TUserKey> : IBilling<TBill, TPayment, TUser, TUserKey> where TBill:VatBill<TUser,TUserKey,TPayment> where TPayment: VatPaymentLog<TUser, TUserKey> where TUser:IdentityUser<TUserKey> where TUserKey: IEquatable<TUserKey>
    {

        private readonly IGenericRepository<TBill> _repository;
        private readonly IGenericRepository<TPayment> _paymentRepository;        

        public Billing(IGenericRepository<TBill> repository, IGenericRepository<TPayment> paymentRepository)
        {
            _repository = repository;
            _paymentRepository = paymentRepository;
        }
        
        public async Task<TBill> GenerateBillLog(TBill billInfo, string billPrefix = "VATBL")
        {

            billInfo.BillRefenceNo = GenerateBillRefNo(billPrefix);
            if (!string.IsNullOrEmpty(billInfo.BillRefenceNo.Trim()))
            {
                await _repository.InsertOrUpdateAsync(billInfo);
                await _repository.SaveChangesAsync();

                return billInfo;
            }

            throw new Exception("Bill reference number can not be emty, whitespace or null");            
        }

        public async Task<TPayment> FetchPayment(string paymentRef)
        {
            var paymentHistory = await _paymentRepository.GetAll().Include(x=>x.User).FirstOrDefaultAsync(x => x.PaymentReferenceNo == paymentRef);
            return paymentHistory;
        }

        public async Task<string> UpdateBillLog(TBill billInfo)
        {
            if (!string.IsNullOrEmpty(billInfo.BillRefenceNo.Trim()))
            {
                await _repository.UpdateAsync(billInfo);
                await _repository.SaveChangesAsync();

                return billInfo.BillRefenceNo;
            }

            throw new Exception("Bill reference number can not be emty, whitespace or null");
        }

        public TBill GetBill(int billId)
        {
            try
            {
                var bill = _repository.GetById(billId);
                return bill;
            }catch(Exception ex)
            {
                return null;
            }
            
        }

        private string GenerateBillRefNo(string prefix)
        {
            var suffix = Utilities.GenerateRandomString(7);
            var randomString = prefix + suffix;
            
            while (_repository.GetAllList(x => x.BillRefenceNo == randomString).Any())
            {
                suffix = Utilities.GenerateRandomString();
                randomString = prefix + suffix;
            }

            return randomString;
        }                

        private string GeneratePaymentRefNo(string prefix)
        {
            var suffix = Utilities.GenerateRandomString(7);
            var randomString = prefix + suffix;
            while (_paymentRepository.GetAllList(x => x.PaymentReferenceNo == randomString).Any())
            {
                suffix = Utilities.GenerateRandomString();
                randomString = prefix + suffix;
            }

            return randomString;
        }

        public async Task<string> GeneratePayment(int billId, TPayment payment, string prefix = "VATPY")
        {
            payment.PaymentReferenceNo = GeneratePaymentRefNo(prefix);
            if(!string.IsNullOrEmpty(payment.PaymentReferenceNo))
            {
                var bill = _repository.GetAll().Include(c=>c.PaymentDetails).FirstOrDefault(x=>x.Id == billId);
                if (bill != null)
                {
                    bill.PaymentDetails.Add(payment);
                    await _repository.SaveChangesAsync();

                    return payment.PaymentReferenceNo;
                }
            }

            throw new Exception("Payment reference could not be generated");
        }

        public async Task<TBill> FetchBill(string billRef)
        {
            var bill = _repository.FirstOrDefault(x => x.BillRefenceNo == billRef);
            return bill;
        }

        public async Task<PaymentDetails> GetPaymentReceiptAsync(string transactionRef)
        {
            return await _paymentRepository.GetAll()
                   .Include(x => x.User)
                   .Where(x => x.PaymentReferenceNo == transactionRef)
                   .Select(x => new PaymentDetails
                   {
                       Id = x.Id,
                       Amount = x.Amount,
                       //FullName = x.User.First,
                       Email = x.User.Email,
                       Phone = x.User.PhoneNumber,
                       TechFee = x.TechFee,
                       Total = x.Amount + x.TechFee,
                       TransactionRef = x.PaymentReferenceNo,
                       BillId = x.BillId                       
                       //billRefenceNo = x.Bill.BillRefenceNo,
                       //BillStatus = x.Bill.BillStatus
                   }).FirstOrDefaultAsync();
        }

        public async Task<bool> UpdatePayment(TPayment payment)
        {
            await _paymentRepository.UpdateAsync(payment);
            await _paymentRepository.SaveChangesAsync();
            return true;
        }

        public async Task<List<TBill>> FetchUserBills(TUserKey userId)
        {
            var userBills = await _repository.GetAllListAsync(x => x.UserId.Equals(userId));
            return userBills;
        }

        public async Task<List<TPayment>> FetchUserPayments(TUserKey userId)
        {
            var userPayments = await _paymentRepository.GetAllListAsync(x => x.UserId.Equals(userId));
            return userPayments;
        }
    }
}
