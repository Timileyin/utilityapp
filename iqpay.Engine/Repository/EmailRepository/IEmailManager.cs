﻿using iqpay.Engine.Base.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace iqpay.Engine.Base.Repository.EmailRepository
{
    public interface IEmailManager<TEMailLog, TEmailTemplate> where TEMailLog : VatEmailLog where TEmailTemplate : VatEmailTemplate
    {
        Task<VatEmailStatus> SendMailAsync(TEMailLog emailLog);
        VatEmailStatus SendMail(TEMailLog emailLog);
        Task LogEmail(TEMailLog emailLog);
        Task LogBatchEmail(List<TEMailLog> eMailLogs);
        Task<TEmailTemplate> CreateEmailTempalteAsync(TEmailTemplate emailTemplate);
        IQueryable<TEmailTemplate> GetEmailTemplate(VatEmailTemplateType type);
        Task SendBatchMailAsync();
        IQueryable<TEmailTemplate> GetEmailTemplates();
        IQueryable<TEmailTemplate> GetEmailTemplates(Expression<Func<TEmailTemplate, bool>> predicate);
        Task DeleteEmailTemplate(TEmailTemplate emailTemplate);




    }
}
