﻿using EmailEngine.Base.Entities;
using iqpay.Engine.Base.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iqpay.Engine.Repository.EmailRepository
{
    public interface IAuditTrailManager<TActivity> where TActivity: VatActivityLog
    {
        Task<long?> AddAuditTrail(TActivity log);
        Task<List<TActivity>> GetAuditTrails();
        Task<List<TActivity>> FilterAuditTrail();
        TActivity GetAuditTrail(int id);   
    }
}
