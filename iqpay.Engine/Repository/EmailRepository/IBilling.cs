﻿using EmailEngine.Base.Entities;
using EmailEngine.Base.Entities.Models;
using iqpay.Engine.Base.Entities;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace iqpay.Engine.Repository.EmailRepository
{
    public interface IBilling<TBill, TPayment, TUser, TUserKey> where TBill:VatBill<TUser, TUserKey, TPayment> where TUser: IdentityUser<TUserKey> where TUserKey: IEquatable<TUserKey> where TPayment:VatPaymentLog<TUser, TUserKey>
    {

        Task<TBill> GenerateBillLog(TBill billInfo, string billPrefix = "VAT");
        Task<string> GeneratePayment(int billId, TPayment payment, string prefix = "VATPY");
        Task<PaymentDetails> GetPaymentReceiptAsync(string transactionRef);
        Task<TBill> FetchBill(string billRef);
        Task<TPayment> FetchPayment(string paymentRef);
        Task<bool> UpdatePayment(TPayment payment);
        Task<List<TBill>> FetchUserBills(TUserKey userId);
        Task<List<TPayment>> FetchUserPayments(TUserKey userId);
        TBill GetBill(int billId);
        Task<string> UpdateBillLog(TBill billInfo);
    }
}
