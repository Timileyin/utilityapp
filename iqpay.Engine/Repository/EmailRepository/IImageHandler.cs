﻿using Microsoft.AspNetCore.Http;
using System;
using System.Threading.Tasks;
using System.Drawing;
using iqpay.Engine.Repository.Interface;

namespace iqpay.Engine.Repository
{
    public interface IFileHandler:IAutoDependencyRegister
    {
        Task<string> UploadFile(IFormFile file, FileType fileType);
        Image FetchImage(int? width, int? height, string fileName);
        Task<byte[]> FetchFile(string file);
    }

    public enum FileType
    {
        PICTURE = 1,
        VIDEOS,
        PDF,
        SUPPORT,
        OTHERS
    }
}
