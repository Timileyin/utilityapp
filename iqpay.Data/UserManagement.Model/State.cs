﻿using iqpay.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace iqpay.Data.UserManagement.Model
{
    public class State:EntityBase
    {
        public string Name { get; set; }
        public long CountryId { get; set; }
        [ForeignKey("CountryId")]
        public Country Country { get; set; }
        

    }
}
