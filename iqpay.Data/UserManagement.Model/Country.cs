﻿using iqpay.Data.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace iqpay.Data.UserManagement.Model
{
    public class Country:EntityBase
    {
        public string Name { get; set; }
        public string DialCode { get; set; }
        public List<State> States { get; set; }
        public string CountryCode { get; set; }       
    }
}
