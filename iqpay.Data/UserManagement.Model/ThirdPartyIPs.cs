﻿using iqpay.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace iqpay.Data.UserManagement.Model
{
    public class ThridPartyAPIKeys:EntityBase
    {
        public string PublicAPIKEY { get; set; }
        public string SecretAPIKEY { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
        public long UserId { get; set; }
    }
}
