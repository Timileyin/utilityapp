﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;
using System;
using System.Collections.Generic;
using iqpay.Data.Entity.Interface;

namespace iqpay.Data.UserManagement.Model
{
    public class ApplicationUserRole : IdentityUserRole<long>, IAudit
    {
        public string CreatedBy { get ; set; }
        public string DeletedBy { get ; set ; }
        public string UpdatedBy { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public byte[] RowVersion { get; set; }
        public long Id { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }

        public bool IsTransient()
        {
            return EqualityComparer<long>.Default.Equals(Id, default(long));
        }
    }
}
