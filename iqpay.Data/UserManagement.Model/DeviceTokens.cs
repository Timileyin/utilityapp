﻿using iqpay.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace iqpay.Data.UserManagement.Model
{
    public class DeviceToken:EntityBase
    {
        public string Token { get; set; }
        public string DeviceName { get; set; }
        public DeviceType DeviceType { get; set; }
        public long UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
    }

    public enum DeviceType
    {
        AndroidMobile = 1,
        IOSMobile,
        Web,
        Others
    } 
}
