﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace iqpay.Data.UserManagement.Model
{
    public class ApplicationRoleStore: RoleStore<ApplicationRole,IQPAYContext, long, ApplicationUserRole, IdentityRoleClaim<long>>
    {
        public ApplicationRoleStore(IQPAYContext context) : base(context)
        {
        }
    }
}
