using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using iqpay.Data.Entities.Menus;
using iqpay.Data.Entity.Interface;
using Microsoft.AspNetCore.Identity;

namespace iqpay.Data.UserManagement.Model
{
    public enum PermissionEnum
    {
        [Description("Download Dealers")]
        DownloadDealers = 1,
        [Description("View Master Wallets Report")]
        ViewMasterWalletsReport,
        [Description("View Wallet Balance Report")]
        ViewWalletBalanceReport,
        [Description("View Commission Ahead Report")]
        ViewCommissionAheadReport,
        [Description("View Processor Vending Report")]
        ViewProcessorVendingReport,
        [Description("Manage Vendors")]
        ManageVendors,
        [Description("Manage Services")]
        ManageServices,
        [Description("View Transaction Logs")]
        ViewTransactionsLogs
    }
}