﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace iqpay.Data.UserManagement.Model
{
    public class ApplicationUserStore: UserStore<ApplicationUser, ApplicationRole,IQPAYContext,long,ApplicationUserClaim,ApplicationUserRole,ApplicationUserLogin,IdentityUserToken<long>,IdentityRoleClaim<long>>
    {
        public ApplicationUserStore(IQPAYContext context) : base(context)
        {
        }
    }
}
