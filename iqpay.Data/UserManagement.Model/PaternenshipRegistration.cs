using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;
using iqpay.Data.Entities;
namespace iqpay.Data.UserManagement.Model
{
    public class PartnershipRegistration:EntityBase
    {
        public string Email{ get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Phonenumber { get; set; }
        public string CompanyName{get;set;}
        public string BVN { get; set; }
        public string NIN { get; set; }
        public string AddressLine1 {get;set;}
        public int Status{get;set;}
        public long? ApprovedBy{get;set;}
        [ForeignKey("ApprovedBy")]
        public ApplicationUser User{get;set;}
        public int State{get;set;}
        public int LGA {get;set;}
        public string AddressLine2{get;set;}
        public string PhotoIdLoc{get;set;}
        public long? SuperDealerId{get;set;}
    }
}