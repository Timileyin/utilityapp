﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using iqpay.Data.Entity.Interface;
using Microsoft.AspNetCore.Identity;
using iqpay.Data.Entities.Payment;

namespace iqpay.Data.UserManagement.Model
{
    public class ApplicationUser : IdentityUser<long>, IAudit
    {
        //public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        //{
        //    //// Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
        //    //var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
        //    //// Add custom user claims here
        //    //userIdentity.AddClaim(new Claim("FullName", FirstName + " " + MiddleName + " " + LastName));
        //    //return userIdentity;
        //}
        public ApplicationUser()
        {
            
        }
        public ApplicationUser(string username)
        {
            UserName = username;
        }

        public ProfileTitle Title { get; set; }
        
        public string Address { get; set; }
        public string AddressLine2 {get;set;}
        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }                
        public bool IsDeleted { get; set; } = false;
        public bool IsActive { get; set; }= true;
        public string CreatedBy { get; set; }
        public string DeletedBy { get; set; }
        public string UpdatedBy { get; set; }
        public long? SuperDealerId { get; set; }
        [ForeignKey("SuperDealerId")]
        public ApplicationUser Dealer { get; set; }
        public string Permissions{get;set;}

        public DateTime DateCreated { get; set; } = DateTime.Now;

        [Timestamp]
        public byte[] RowVersion { get; set; }
        public DateTime? LastUpdateDate { get ; set; }
        public string VirtualAccountName{get;set;}
        public string VirtualAccountNumber{get;set;}
        public string VFDAccountNumber{get;set;}
        public string VFDAccountName {get;set;}
        public string ReservedAccountNumber{get;set;}
        public string ReservedAccountName{get;set;}
        public string RubiesAccountNumber{get;set;}
        public string RubiesAccountName{get;set;}
        public string ReservedAccountBankName{get;set;}
        public DateTime? DateAccountCreated{get;set;}
        public DateTime DateOfBirth{get;set;}
        public string ProfileImageLoc{get;set;}        
        public bool IsTransient()
        {
            return EqualityComparer<long>.Default.Equals(Id, default(long));
        }                
        public List<DeviceToken> DeviceTokens { get; set; }
        public AccountType AccountType { get; set; }
        public List<DealerCommission> Commissions{get;set;}
        public string BVN{get;set;}
        public string NIN{get;set;}
    }

    public enum AccountType
    {
        SUPER_DEALER = 1,
        DEALER,
        AGENT,
        REGULAR
    }

    public enum Gender
    {
        Male,
        Female
    }

    public enum ProfileTitle
    {
        Mr = 1,
        Mrs,
        Miss,
        Dr,
        Prof,
        Engr,
        Barr
    }

}
