﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using iqpay.Data.Entities.Menus;
using iqpay.Data.Entity.Interface;
using Microsoft.AspNetCore.Identity;

namespace iqpay.Data.UserManagement.Model
{

    public class ApplicationRole : IdentityRole<long>, IEntity
    {
        public ApplicationRole()
        {
            DateCreated = DateTime.Now;
            Menus= new List<RoleMenu>();
        }

        public bool IsDefault { get; set; }
        public DateTime DateCreated { get; set; }

        public bool IsTransient()
        {
            return EqualityComparer<long>.Default.Equals(Id, default(long));
        }

        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        
        public virtual ICollection<RoleMenu> Menus { get; set; }
        [NotMapped]
        public virtual ICollection<ApplicationUserRole> Users { get; } = new List<ApplicationUserRole>();
        public string Description { get; set; }
       
    }

    public enum RoleTypes
    {
        [Description("SUPER DEALER")]
        IQISUPERDEALER,
        [Description("DEALER")]
        DEALER,
        [Description("AGENT")]
        AGENT,
        [Description("REGULAR CUSTOMER")]
        REGULAR_CUSTOMER,
        [Description("SYSTEM ADMIN")]
        SYSADMIN,
        
    }
}
