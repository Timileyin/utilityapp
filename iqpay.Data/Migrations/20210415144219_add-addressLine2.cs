﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class addaddressLine2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AddressLine2",
                table: "AspNetUsers",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddressLine2",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "54f0268a-a431-4709-b7ee-446f1c246f19", new DateTime(2021, 4, 9, 16, 59, 46, 861, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "19f7ae30-dccd-420a-ad84-b86a115238e9", new DateTime(2021, 4, 9, 16, 59, 46, 874, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "f8bf0d1e-e336-4369-93b9-91338b070745", new DateTime(2021, 4, 9, 16, 59, 46, 874, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "014a1850-dfa4-4a7e-b71a-a86c30e24ba6", new DateTime(2021, 4, 9, 16, 59, 46, 874, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "53b1276a-5a7a-409b-8739-62905aaf4eaa", new DateTime(2021, 4, 9, 16, 59, 46, 874, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "01ea6e89-53b8-4be2-9d05-24fc1839d70b", new DateTime(2021, 4, 9, 16, 59, 46, 874, DateTimeKind.Local), "AQAAAAEAACcQAAAAEM/wXNXPuy3ppoYjYptl+etbSuco2ErS0fhP9nVROE9bF5FQcMQcDPV0o8Anb6uTmA==", null, "f3605e4c-56d4-4d1d-ac81-639e8aebb32a" });
        }
    }
}
