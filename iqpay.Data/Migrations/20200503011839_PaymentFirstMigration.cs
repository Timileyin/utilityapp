﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class PaymentFirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BillLogs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    DeletedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    LastUpdateDate = table.Column<DateTime>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    UserId = table.Column<long>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    BillStatus = table.Column<int>(nullable: false),
                    BillRefenceNo = table.Column<string>(nullable: true),
                    DatePaid = table.Column<DateTime>(nullable: true),
                    BillType = table.Column<int>(nullable: false),
                    Year = table.Column<int>(nullable: false),
                    PaymentMethod = table.Column<int>(nullable: false),
                    EntityId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BillLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BillLogs_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.CreateTable(
                name: "PaymentLog",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    DeletedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    LastUpdateDate = table.Column<DateTime>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    MembershipNo = table.Column<string>(nullable: true),
                    UserId = table.Column<long>(nullable: false),
                    PaymenType = table.Column<int>(nullable: false),
                    PaymentReferenceNo = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    FinalAmount = table.Column<decimal>(nullable: false),
                    PaymentDate = table.Column<DateTime>(nullable: true),
                    PaymentCharges = table.Column<decimal>(nullable: false),
                    ResponseCode = table.Column<string>(nullable: true),
                    ResponseDescription = table.Column<string>(nullable: true),
                    GateWayAmount = table.Column<decimal>(nullable: false),
                    TechFee = table.Column<decimal>(nullable: false),
                    PaymentStatus = table.Column<int>(nullable: false),
                    ResponsePayload = table.Column<string>(nullable: true),
                    CardNumber = table.Column<string>(nullable: true),
                    EntityId = table.Column<int>(nullable: true),
                    BillId = table.Column<long>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaymentLog", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PaymentLog_BillLogs_BillId",
                        column: x => x.BillId,
                        principalTable: "BillLogs",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PaymentLog_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "20f7327b-87e8-4487-9d78-0c24ee15e1e4", new DateTime(2020, 5, 3, 2, 18, 38, 869, DateTimeKind.Local).AddTicks(8100) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "69c17b3b-f782-4dec-ad4d-4f4155790e90", new DateTime(2020, 5, 3, 2, 18, 38, 884, DateTimeKind.Local).AddTicks(2520) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "4dc2d53f-7e29-4bcf-b0de-f73b438c86f8", new DateTime(2020, 5, 3, 2, 18, 38, 884, DateTimeKind.Local).AddTicks(2830) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "2ddf3e7c-4132-4d3c-ba82-a0e17f3b5409", new DateTime(2020, 5, 3, 2, 18, 38, 884, DateTimeKind.Local).AddTicks(2980) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "21f38b1a-159d-4b00-bd4d-256c6cc453ad", new DateTime(2020, 5, 3, 2, 18, 38, 884, DateTimeKind.Local).AddTicks(3120) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "93eb5370-5686-4f48-ad6d-67f2dbcd9720", new DateTime(2020, 5, 3, 2, 18, 38, 884, DateTimeKind.Local).AddTicks(4420), "AQAAAAEAACcQAAAAEJLi12Dc1UUDR/enI83CvfofJQir7SELnmuXwAcqJXmnYgDRGvqFXhG1DlN4nWwIxA==", "27609158-bfbe-4fef-960b-7152f3123109" });

            migrationBuilder.CreateIndex(
                name: "IX_BillLogs_UserId",
                table: "BillLogs",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentLog_BillId",
                table: "PaymentLog",
                column: "BillId");

            migrationBuilder.CreateIndex(
                name: "IX_PaymentLog_UserId",
                table: "PaymentLog",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PaymentLog");

            migrationBuilder.DropTable(
                name: "BillLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "82af9aca-42c9-4ec1-969a-30b118d33d55", new DateTime(2020, 5, 2, 6, 31, 4, 367, DateTimeKind.Local).AddTicks(2710) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "4c13d5cf-3936-45ba-b1ee-b5c7e6b9ab29", new DateTime(2020, 5, 2, 6, 31, 4, 372, DateTimeKind.Local).AddTicks(5640) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "491f4599-9cd7-4ffd-a347-c0dfea8c64cf", new DateTime(2020, 5, 2, 6, 31, 4, 372, DateTimeKind.Local).AddTicks(5880) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "2d102600-abb6-43ba-8164-1f51aeccb5a3", new DateTime(2020, 5, 2, 6, 31, 4, 372, DateTimeKind.Local).AddTicks(5980) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6d9fceb1-7bdd-4c81-ab00-c3f07b2cb717", new DateTime(2020, 5, 2, 6, 31, 4, 372, DateTimeKind.Local).AddTicks(6080) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "8e47cbaf-927b-475a-90a5-6e0768b374d9", new DateTime(2020, 5, 2, 6, 31, 4, 372, DateTimeKind.Local).AddTicks(7290), "AQAAAAEAACcQAAAAECouuVJqmkZnMAamjWuMqbOuNj//TMF8SuDdZQ4k/AvOH3OGCgv7l5doTND2HQGZow==", "b1b86e4a-3d36-4882-9a73-76ccded3ced2" });
        }
    }
}
