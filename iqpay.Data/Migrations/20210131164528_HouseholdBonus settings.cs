﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class HouseholdBonussettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "EnableHouseHoldBonus",
                table: "Vendors",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "HouseHoldBonusCap",
                table: "Vendors",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "HouseHoldBonusDates",
                table: "Vendors",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "HouseHoldBonusPercentage",
                table: "Vendors",
                nullable: false,
                defaultValue: 0m);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "EnableHouseHoldBonus",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "HouseHoldBonusCap",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "HouseHoldBonusDates",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "HouseHoldBonusPercentage",
                table: "Vendors");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "d412ee99-856c-4f03-b351-39e1a8a0c97a", new DateTime(2021, 1, 9, 8, 46, 44, 458, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6f5852ae-6c8d-49cf-8135-7eb2ac44721b", new DateTime(2021, 1, 9, 8, 46, 44, 467, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "7bed6bfc-7045-4f3b-bdb3-4389612f6365", new DateTime(2021, 1, 9, 8, 46, 44, 467, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "07cd6a4d-9850-4f4f-8861-759a890c27da", new DateTime(2021, 1, 9, 8, 46, 44, 467, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e8d2f4a8-48bb-4c9a-975d-3ce4a6704002", new DateTime(2021, 1, 9, 8, 46, 44, 467, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "c039e42e-0f01-4bd1-a555-ddcc98d7f26c", new DateTime(2021, 1, 9, 8, 46, 44, 467, DateTimeKind.Local), "AQAAAAEAACcQAAAAENznmAXjM+Vq/+WPAm2hxKlAV4DF+ZcgKnIEjD2Zkq+rAJp04Z1z2Jnox1ZrX4+0Ug==", null, "54cdc0e4-1798-4ee6-b944-b3395a730a1b" });
        }
    }
}
