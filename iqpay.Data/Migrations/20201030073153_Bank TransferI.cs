﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class BankTransferI : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AccountName",
                table: "BankTransfers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerEmail",
                table: "BankTransfers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CustomerName",
                table: "BankTransfers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TransactionRef",
                table: "BankTransfers",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountName",
                table: "BankTransfers");

            migrationBuilder.DropColumn(
                name: "CustomerEmail",
                table: "BankTransfers");

            migrationBuilder.DropColumn(
                name: "CustomerName",
                table: "BankTransfers");

            migrationBuilder.DropColumn(
                name: "TransactionRef",
                table: "BankTransfers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "68db6dd0-c1d0-40c1-8328-057e5cfeb2cd", new DateTime(2020, 10, 30, 7, 28, 40, 380, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "d4bbd8da-2ad7-4594-a01d-c8dedfb36e67", new DateTime(2020, 10, 30, 7, 28, 40, 413, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "83daaff1-cfc7-4072-8187-a758305cc7ad", new DateTime(2020, 10, 30, 7, 28, 40, 413, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "5f42116d-3095-4259-b58c-d8aeee10a95f", new DateTime(2020, 10, 30, 7, 28, 40, 413, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "cef57fd8-ffc6-4d79-b4f9-48d1305fdb1d", new DateTime(2020, 10, 30, 7, 28, 40, 413, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "9a0ea452-c492-498b-be33-aaba56139cd7", new DateTime(2020, 10, 30, 7, 28, 40, 414, DateTimeKind.Local), "AQAAAAEAACcQAAAAED+TD8+HmdNWpah6rGO6K0G8Dd6MGAcSKNnsL6Bk6xMzm8cbf3lpCVAqqu8BKKGz8Q==", null, "2b62bf37-2e09-4713-8b52-c47a1b326390" });
        }
    }
}
