﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class AddVendingLogDeliveryOptions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DeliveryOption",
                table: "VendingLogs",
                nullable: true);

            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeliveryOption",
                table: "VendingLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "7b2ed1de-64a2-42a3-8517-d6c668275d36", new DateTime(2022, 7, 3, 12, 10, 20, 943, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8a3a570b-ad86-4252-b309-16c14ce95b87", new DateTime(2022, 7, 3, 12, 10, 20, 947, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "49010618-2353-4cae-b8f9-3d6e73f86e74", new DateTime(2022, 7, 3, 12, 10, 20, 947, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "2c1f2189-c364-48c5-96ff-fcee5b5ad696", new DateTime(2022, 7, 3, 12, 10, 20, 947, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c45bcf20-5b11-4c02-a543-eaec74bbfcad", new DateTime(2022, 7, 3, 12, 10, 20, 947, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "c871afa6-c827-4c3b-bf05-3be0a2d77734", new DateTime(2022, 7, 3, 12, 10, 20, 947, DateTimeKind.Local), "AQAAAAEAACcQAAAAEFtj+l3+q3w8+jwD1FIIvIpMyTv73ywe73b/W+0CSmjbeeQ6F9vR+WrsPJJfTjNW0w==", null, "8921d87a-934f-45fc-a46b-a667da85ed42" });
        }
    }
}
