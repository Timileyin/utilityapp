﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class AddedServiceUnits : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsMeasuredService",
                table: "Services",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UnitType",
                table: "Services",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsMeasuredService",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "UnitType",
                table: "Services");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6fc10982-12d3-4a51-89cf-5bbdce0e452f", new DateTime(2021, 4, 25, 13, 40, 20, 981, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "9b7b0283-de15-445a-92a7-a1f267ae3df0", new DateTime(2021, 4, 25, 13, 40, 20, 992, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "9d10376a-e8d5-4304-b15a-ca1deaceeeb1", new DateTime(2021, 4, 25, 13, 40, 20, 992, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8f5bd892-3af6-4bb0-80f0-4fc74882dc9a", new DateTime(2021, 4, 25, 13, 40, 20, 992, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "da94ade4-b092-418d-a8da-1ac59101075f", new DateTime(2021, 4, 25, 13, 40, 20, 992, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "383bfcae-8354-4719-a615-7f0f2894267d", new DateTime(2021, 4, 25, 13, 40, 20, 992, DateTimeKind.Local), "AQAAAAEAACcQAAAAEMZg7/5QsNPGr0QCLnqvvQwQh6mwwBjI3DJGofJV01D9p/dJh81i1qh9XAi1wwIp0w==", null, "9d405b64-a304-4e83-a24b-869e9696675c" });
        }
    }
}
