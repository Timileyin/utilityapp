﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class IQPAYProfit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "IQPAYProfit",
                table: "VendingLogs",
                nullable: false,
                defaultValue: 0m);
        } 
    

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IQPAYProfit",
                table: "VendingLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "60cd2acb-7e68-426e-bf30-68535518d637", new DateTime(2020, 10, 11, 13, 51, 46, 415, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "1c03bb32-f206-4a29-a9a2-efa0766715a9", new DateTime(2020, 10, 11, 13, 51, 46, 448, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "3e1e3538-e936-403e-a54f-0bcecac70947", new DateTime(2020, 10, 11, 13, 51, 46, 448, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "7f7206b0-3361-4883-a627-8f694aa6e6ca", new DateTime(2020, 10, 11, 13, 51, 46, 448, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b0091db4-62c6-431e-bc7d-fc0b9eb3c10f", new DateTime(2020, 10, 11, 13, 51, 46, 448, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "bd191476-6b29-4b3b-8a8c-5fa05eac4ab0", new DateTime(2020, 10, 11, 13, 51, 46, 448, DateTimeKind.Local), "AQAAAAEAACcQAAAAEBHRERHDIzmXfigtI5aNLi5W6lrAXryr+7TqIdBN3AP6wz3FbL2f1XfDZixbpT7KLA==", null, "e2d0523c-049c-4924-876c-984818dffd3f" });
        }
    }
}
