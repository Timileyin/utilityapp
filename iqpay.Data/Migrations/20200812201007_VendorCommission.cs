﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class VendorCommission : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8993ec52-34b1-40aa-be02-6f949a9ffb76", new DateTime(2020, 8, 12, 21, 10, 6, 543, DateTimeKind.Local).AddTicks(3840) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8dae8f1d-0622-4e81-a9a2-54018ef31aed", new DateTime(2020, 8, 12, 21, 10, 6, 548, DateTimeKind.Local).AddTicks(2440) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c37f88b1-045d-4139-b044-feb2176ca020", new DateTime(2020, 8, 12, 21, 10, 6, 548, DateTimeKind.Local).AddTicks(2680) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a9c61ad3-4642-4886-a6a4-f7d7c5371987", new DateTime(2020, 8, 12, 21, 10, 6, 548, DateTimeKind.Local).AddTicks(2790) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "01e98af6-2d0d-4df5-92d3-28efad7aeb0f", new DateTime(2020, 8, 12, 21, 10, 6, 548, DateTimeKind.Local).AddTicks(2890) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "95af21da-3042-427d-a2b0-91841865c8a9", new DateTime(2020, 8, 12, 21, 10, 6, 548, DateTimeKind.Local).AddTicks(3800), "AQAAAAEAACcQAAAAEAEZ4iBDepAi1Kl/wi4jovbzVc8rbKJLyAojWOGb0VcIN7nrZ/lO3dARqpQpvTLInQ==", "019a7855-3356-4e2c-b9be-2025efade97a" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "9909ba21-18db-4866-bfaf-92c7944bd2db", new DateTime(2020, 8, 12, 7, 47, 9, 336, DateTimeKind.Local).AddTicks(9620) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e4cbdf66-8f72-4913-86f0-a2eed35b1077", new DateTime(2020, 8, 12, 7, 47, 9, 343, DateTimeKind.Local).AddTicks(910) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b6a83452-f211-47ed-9ab5-96af32035e0b", new DateTime(2020, 8, 12, 7, 47, 9, 343, DateTimeKind.Local).AddTicks(1210) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ec2c63ae-524b-4450-a901-176d51c51a10", new DateTime(2020, 8, 12, 7, 47, 9, 343, DateTimeKind.Local).AddTicks(1370) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "cd66e0aa-5ead-4f99-907d-a79c778d7492", new DateTime(2020, 8, 12, 7, 47, 9, 343, DateTimeKind.Local).AddTicks(1520) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e100d01f-01d5-4d7b-b0a9-aa07d3724faa", new DateTime(2020, 8, 12, 7, 47, 9, 343, DateTimeKind.Local).AddTicks(2840), "AQAAAAEAACcQAAAAEKIXlqN56CIGZX6ctPqqmbBgiPw3sf447xLT45zKC6hX/B3Ee5GTYM84kQeta5rvKA==", "959bc3ee-71f0-4efa-a495-606c728ff392" });
        }
    }
}
