﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class ProcessorBalance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "ProcessorBalance",
                table: "VendingLogs",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProcessorBalance",
                table: "VendingLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "cc6dc3c0-f529-4a5f-ba87-b180f6ddd38a", new DateTime(2020, 11, 11, 7, 28, 21, 842, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a367ec04-94ff-4759-a30c-c7f0cb86d24c", new DateTime(2020, 11, 11, 7, 28, 21, 849, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "4e73616f-340b-40f6-8c50-5692c5d11877", new DateTime(2020, 11, 11, 7, 28, 21, 849, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8573264c-884a-4599-90ec-640315e4ed02", new DateTime(2020, 11, 11, 7, 28, 21, 849, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a243c7b6-315e-4a9e-ae30-e368514d70e4", new DateTime(2020, 11, 11, 7, 28, 21, 849, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "fe7c99dc-3bbf-4d93-890f-a1ff94b90ac7", new DateTime(2020, 11, 11, 7, 28, 21, 849, DateTimeKind.Local), "AQAAAAEAACcQAAAAEO++gc0RyXAZrtbWL5z65leJm4QTyCPoU3GvIyPMR1x2fae9ru8PD1RwkZ0n4zKN+Q==", null, "7e8e4b2f-22ca-453b-9326-d6d509cde32d" });
        }
    }
}
