﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class VendingLogsProcessorIdNUllable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VendingLogs_Processors_ProcessorId",
                table: "VendingLogs");

            migrationBuilder.AlterColumn<long>(
                name: "ProcessorId",
                table: "VendingLogs",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6593e890-2bea-4d22-845f-3a08f55e06c1", new DateTime(2020, 6, 10, 22, 59, 49, 976, DateTimeKind.Local).AddTicks(8130) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e5a04c41-eb6b-41ef-a342-f3909a48f8a7", new DateTime(2020, 6, 10, 22, 59, 49, 983, DateTimeKind.Local).AddTicks(2710) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a4cd98bd-9ace-40ca-bdd1-d6136108277c", new DateTime(2020, 6, 10, 22, 59, 49, 983, DateTimeKind.Local).AddTicks(2920) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e9af08ae-af1f-46d2-a211-292e4a1c02d5", new DateTime(2020, 6, 10, 22, 59, 49, 983, DateTimeKind.Local).AddTicks(3020) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "bf47ea76-2bb7-49cb-8c96-a243626517d7", new DateTime(2020, 6, 10, 22, 59, 49, 983, DateTimeKind.Local).AddTicks(3120) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b26a811a-16aa-4783-af69-565582d20ba9", new DateTime(2020, 6, 10, 22, 59, 49, 983, DateTimeKind.Local).AddTicks(4010), "AQAAAAEAACcQAAAAEJWg6dNH/0LE6A9wXuI6nAZ0gI/K5BUeiMIXKbiW0gvb0+Y5apUcDbkcwRyFoshTLw==", "177f4d50-3f7c-47f0-abae-a8f35ecf5ccb" });

            migrationBuilder.AddForeignKey(
                name: "FK_VendingLogs_Processors_ProcessorId",
                table: "VendingLogs",
                column: "ProcessorId",
                principalTable: "Processors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VendingLogs_Processors_ProcessorId",
                table: "VendingLogs");

            migrationBuilder.AlterColumn<long>(
                name: "ProcessorId",
                table: "VendingLogs",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "95e88d86-3747-48fb-94aa-b532ca13d99f", new DateTime(2020, 6, 10, 17, 26, 8, 882, DateTimeKind.Local).AddTicks(7790) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "13d2a0ec-f1b4-4105-b4cc-245b33b11fc3", new DateTime(2020, 6, 10, 17, 26, 8, 892, DateTimeKind.Local).AddTicks(8870) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "81199c25-e59b-4230-acf5-f19898a47614", new DateTime(2020, 6, 10, 17, 26, 8, 892, DateTimeKind.Local).AddTicks(9090) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "642b9f56-62c9-447b-8920-6f08f28b55f8", new DateTime(2020, 6, 10, 17, 26, 8, 892, DateTimeKind.Local).AddTicks(9190) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ae53a2f6-41a1-4bbc-a5ce-9ed443e44e98", new DateTime(2020, 6, 10, 17, 26, 8, 892, DateTimeKind.Local).AddTicks(9630) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "7b3129bb-efea-4643-b5c4-4e6d63478391", new DateTime(2020, 6, 10, 17, 26, 8, 893, DateTimeKind.Local).AddTicks(1550), "AQAAAAEAACcQAAAAEJhH8OU983R2vWAGwI2WB/3YMriA3sPLYLatqDQ1CduLlaTVZBCSupfw0ZgTfkx2hg==", "7fcb2fd6-64b8-4c6d-8cd6-28f0c92ece1a" });

            migrationBuilder.AddForeignKey(
                name: "FK_VendingLogs_Processors_ProcessorId",
                table: "VendingLogs",
                column: "ProcessorId",
                principalTable: "Processors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
