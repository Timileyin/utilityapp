﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class permission : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Permissions",
                table: "AspNetUsers",
                nullable: true);

                 }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Permissions",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "895b4efa-ae94-4126-a941-5d9bc6d8144a", new DateTime(2021, 4, 15, 15, 42, 17, 193, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "416cf118-b340-4a57-8d35-ceb94af9796e", new DateTime(2021, 4, 15, 15, 42, 17, 202, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "f724b83b-c5f6-47af-b0da-57ceade813d9", new DateTime(2021, 4, 15, 15, 42, 17, 203, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "d984bc35-68ee-4746-975e-a815925a6803", new DateTime(2021, 4, 15, 15, 42, 17, 203, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "1da81bc8-b173-4503-865d-c7e2071fc5db", new DateTime(2021, 4, 15, 15, 42, 17, 203, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "7fd56014-1d05-4cb0-b3c3-1447ae321651", new DateTime(2021, 4, 15, 15, 42, 17, 203, DateTimeKind.Local), "AQAAAAEAACcQAAAAEDi+KHHfKN3DjwEwE3KDo+H3gtsl/Tha+dnuDdTS+fVOgssPz7h6gpdPRXuPpbDOAQ==", null, "5a596928-d407-43c7-915b-7e2983d00252" });
        }
    }
}
