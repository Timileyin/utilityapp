﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class ProviderStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "Processors",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6669d6ed-576f-4c98-a960-b30ee2fbe491", new DateTime(2020, 6, 3, 7, 32, 34, 768, DateTimeKind.Local).AddTicks(7170) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "2d232750-eb1b-48df-b91d-c17011d26e70", new DateTime(2020, 6, 3, 7, 32, 34, 776, DateTimeKind.Local).AddTicks(2990) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "201a79ba-c4f5-4fb7-8540-d1a0800f9cc5", new DateTime(2020, 6, 3, 7, 32, 34, 776, DateTimeKind.Local).AddTicks(3230) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "411daa7b-60cc-40ca-8be8-b807f007f59a", new DateTime(2020, 6, 3, 7, 32, 34, 776, DateTimeKind.Local).AddTicks(3340) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "001093fa-54af-4099-9407-46c8e4684b64", new DateTime(2020, 6, 3, 7, 32, 34, 776, DateTimeKind.Local).AddTicks(3450) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0e16353a-57a9-4286-8cfd-c40ad235d389", new DateTime(2020, 6, 3, 7, 32, 34, 776, DateTimeKind.Local).AddTicks(4440), "AQAAAAEAACcQAAAAEPk++GGSzEgGotdZU2fecgCjnw5hV1yz21EiL29TFj7nTj7yk+u2nyKWlYtOV7JB5w==", "8f06c7ab-bfe4-4348-b0a2-78c718fa06f7" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Status",
                table: "Processors");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "65817495-3c39-41ba-8a63-5ae8d6dbc11c", new DateTime(2020, 6, 1, 18, 35, 22, 563, DateTimeKind.Local).AddTicks(4750) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "5883a620-43fc-480a-99ed-6837dbf7cfac", new DateTime(2020, 6, 1, 18, 35, 22, 570, DateTimeKind.Local).AddTicks(8460) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ad60b011-9348-4b48-a20d-e5bbfff30f4e", new DateTime(2020, 6, 1, 18, 35, 22, 570, DateTimeKind.Local).AddTicks(8670) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "f0fb6216-5c37-4f10-af9a-c235c85cd80b", new DateTime(2020, 6, 1, 18, 35, 22, 570, DateTimeKind.Local).AddTicks(8800) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b232303b-410e-4d62-bd20-513d5d8bbfe4", new DateTime(2020, 6, 1, 18, 35, 22, 570, DateTimeKind.Local).AddTicks(8900) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e116b6e7-113b-48e8-8411-69bb1bf22bfb", new DateTime(2020, 6, 1, 18, 35, 22, 571, DateTimeKind.Local), "AQAAAAEAACcQAAAAENlul778ONUjy1vSuUvjeoHRTleit2w4cwhr/gpI0lwveb3De7duriJRYDuiRpkEtw==", "b381abcd-bb39-49d8-a701-a0568708595f" });
        }
    }
}
