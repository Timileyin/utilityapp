﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class VendorCommissionForUsers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DealerCommission_Services_ServiceId",
                table: "DealerCommission");

            migrationBuilder.AlterColumn<long>(
                name: "ServiceId",
                table: "DealerCommission",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AddColumn<long>(
                name: "VendorId",
                table: "DealerCommission",
                nullable: true);

            
            migrationBuilder.CreateIndex(
                name: "IX_DealerCommission_VendorId",
                table: "DealerCommission",
                column: "VendorId");

            migrationBuilder.AddForeignKey(
                name: "FK_DealerCommission_Services_ServiceId",
                table: "DealerCommission",
                column: "ServiceId",
                principalTable: "Services",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DealerCommission_Vendors_VendorId",
                table: "DealerCommission",
                column: "VendorId",
                principalTable: "Vendors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DealerCommission_Services_ServiceId",
                table: "DealerCommission");

            migrationBuilder.DropForeignKey(
                name: "FK_DealerCommission_Vendors_VendorId",
                table: "DealerCommission");

            migrationBuilder.DropIndex(
                name: "IX_DealerCommission_VendorId",
                table: "DealerCommission");

            migrationBuilder.DropColumn(
                name: "VendorId",
                table: "DealerCommission");

            migrationBuilder.AlterColumn<long>(
                name: "ServiceId",
                table: "DealerCommission",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "5701d6d7-4741-4988-930f-96b637149cdf", new DateTime(2020, 9, 11, 8, 3, 11, 351, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "0e91e48f-5230-4173-baee-5a5c92a6b63f", new DateTime(2020, 9, 11, 8, 3, 11, 365, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "d447db79-bb8a-40c9-9126-f86d18f0693c", new DateTime(2020, 9, 11, 8, 3, 11, 365, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a955f45f-529f-4dbc-96dc-c5328e21e7cb", new DateTime(2020, 9, 11, 8, 3, 11, 365, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "4e7f567e-8bee-4990-a96e-cba5150e4bfd", new DateTime(2020, 9, 11, 8, 3, 11, 365, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "46debc72-e95f-4d10-8bca-061eef7fe2e8", new DateTime(2020, 9, 11, 8, 3, 11, 366, DateTimeKind.Local), "AQAAAAEAACcQAAAAEBJtJg8XX2BfVSsGy6Y1xm6oHRUO1v6PAzsL3sQ+cGBWWNB9zjTgy5TGG4nGoL/2Iw==", null, "a18ca93e-682c-4801-8038-7170cb971566" });

            migrationBuilder.AddForeignKey(
                name: "FK_DealerCommission_Services_ServiceId",
                table: "DealerCommission",
                column: "ServiceId",
                principalTable: "Services",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
