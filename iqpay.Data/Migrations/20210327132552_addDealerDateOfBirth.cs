﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class addDealerDateOfBirth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateOfBirth",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateOfBirth",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ae438a9c-eadf-4ab4-947f-f90bde44c4ec", new DateTime(2021, 3, 27, 12, 11, 12, 60, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "5ce45dbf-12aa-499c-91c3-ea2e342d1922", new DateTime(2021, 3, 27, 12, 11, 12, 80, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e1e91041-0935-4d70-b611-a020aa84ad10", new DateTime(2021, 3, 27, 12, 11, 12, 80, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c56616d1-3530-47df-888e-149842226863", new DateTime(2021, 3, 27, 12, 11, 12, 80, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "3bc32b90-932d-48ce-af69-939d56a8a864", new DateTime(2021, 3, 27, 12, 11, 12, 80, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "b10ed0d4-a6da-463f-b608-48fb7b20575d", new DateTime(2021, 3, 27, 12, 11, 12, 80, DateTimeKind.Local), "AQAAAAEAACcQAAAAEGALpOBiLq+WMWqLZ1oG+VQiQFLjaBDySl1LcFqTaNKEiCl8tDp+JkIq1tcPGdKVbw==", null, "535dbe7b-1403-4781-bf5b-0750065edc38" });
        }
    }
}
