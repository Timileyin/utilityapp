﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class TrnasactionDatetoWalletRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "TransactionDate",
                table: "FundWallet",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "0c9a395c-c058-4ca3-b216-8deae2d1bc66", new DateTime(2020, 4, 3, 14, 17, 39, 653, DateTimeKind.Local).AddTicks(7890) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6bdb3807-df13-49af-90aa-103a64657548", new DateTime(2020, 4, 3, 14, 17, 39, 668, DateTimeKind.Local).AddTicks(6490) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "01a7272e-311f-4489-b6ee-b198a3f58ae6", new DateTime(2020, 4, 3, 14, 17, 39, 668, DateTimeKind.Local).AddTicks(6740) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "9acfa036-a875-4ffe-b456-d29fb29f2cb6", new DateTime(2020, 4, 3, 14, 17, 39, 668, DateTimeKind.Local).AddTicks(6850) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "56523e53-917a-4cf1-a600-182804bb235d", new DateTime(2020, 4, 3, 14, 17, 39, 668, DateTimeKind.Local).AddTicks(6960) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "39d34758-5f53-450b-a749-ca0f06f1975a", new DateTime(2020, 4, 3, 14, 17, 39, 668, DateTimeKind.Local).AddTicks(8140), "AQAAAAEAACcQAAAAEMCdCKFnYy6wHOoSgVvrpzl7NDLZuXlf5s/O/8HvjRzhHT41ganz67wYMT4nZGAkGA==", "ac5a5b6f-28a7-43b7-b85a-47d9f6321861" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TransactionDate",
                table: "FundWallet");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "79b04e26-e2bf-4677-8d1b-fb6b6ff0bb5c", new DateTime(2020, 4, 3, 11, 12, 38, 381, DateTimeKind.Local).AddTicks(4650) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "02f396b5-140b-4b16-87e2-c8ac2c80d008", new DateTime(2020, 4, 3, 11, 12, 38, 392, DateTimeKind.Local).AddTicks(7340) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b7fbed2b-4d24-4f98-a256-9d003abfc7a5", new DateTime(2020, 4, 3, 11, 12, 38, 392, DateTimeKind.Local).AddTicks(7770) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "644c1103-13df-454b-b92c-3fc0aab12273", new DateTime(2020, 4, 3, 11, 12, 38, 392, DateTimeKind.Local).AddTicks(7900) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "14d50c85-31cd-457b-a98a-7e2fe985dfa6", new DateTime(2020, 4, 3, 11, 12, 38, 392, DateTimeKind.Local).AddTicks(8020) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "736ff83b-d78e-49b9-be2d-e5a3b7376460", new DateTime(2020, 4, 3, 11, 12, 38, 392, DateTimeKind.Local).AddTicks(9520), "AQAAAAEAACcQAAAAENMKvqchEy3relCntLFsNcseQ4p2q0VZNjR7dTI/OxNOcNLIcSapCfPlIFKT7PAoLA==", "204e20a4-db93-4f19-97a1-5645272bcbfa" });
        }
    }
}
