﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class VendorLogoSrc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LogoSrc",
                table: "Vendors",
                nullable: true);

               }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LogoSrc",
                table: "Vendors");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "fd79017b-4039-4891-9282-c023cbbcb8a8", new DateTime(2020, 8, 24, 21, 38, 21, 225, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "3d03a8d3-f95c-4f71-ae4c-1b27ce18792c", new DateTime(2020, 8, 24, 21, 38, 21, 232, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "783342e7-a442-4765-839b-10dcbcbb2715", new DateTime(2020, 8, 24, 21, 38, 21, 232, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "d5eacd7c-c2bf-429f-b172-5ef3730367a9", new DateTime(2020, 8, 24, 21, 38, 21, 232, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "bf569399-dfd4-4532-8127-a96fce2dc28f", new DateTime(2020, 8, 24, 21, 38, 21, 232, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "6b848290-739c-473f-b86c-a060d89e6eaa", new DateTime(2020, 8, 24, 21, 38, 21, 232, DateTimeKind.Local), "AQAAAAEAACcQAAAAEBURf/wHRJkpDrhxjESkFPij+wvOH/sqOsZfUo5mZzKCrsyHH/ZGxHSvXHt3D00NYA==", null, "1b99cf17-3160-4679-b5fd-ad518e384588" });
        }
    }
}
