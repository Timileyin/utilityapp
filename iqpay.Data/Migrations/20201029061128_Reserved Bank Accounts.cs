﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class ReservedBankAccounts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateAccountCreated",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReservedAccountBankName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReservedAccountName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ReservedAccountNumber",
                table: "AspNetUsers",
                nullable: true);

        }
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateAccountCreated",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ReservedAccountBankName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ReservedAccountName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "ReservedAccountNumber",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "30ea760d-c9ec-47bb-a1ae-188bddae5791", new DateTime(2020, 10, 11, 16, 30, 46, 134, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "16079bef-3c94-42b5-ad74-02944caf2d7e", new DateTime(2020, 10, 11, 16, 30, 46, 306, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b906d6e2-8247-481c-bfa4-483647aad76c", new DateTime(2020, 10, 11, 16, 30, 46, 306, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "55759546-542a-4d40-8a99-7442bad8a819", new DateTime(2020, 10, 11, 16, 30, 46, 306, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "348a4247-f14f-4294-af3c-1a6f44779678", new DateTime(2020, 10, 11, 16, 30, 46, 306, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "30ae5721-8681-44a0-b5f5-c8f39556786e", new DateTime(2020, 10, 11, 16, 30, 46, 306, DateTimeKind.Local), "AQAAAAEAACcQAAAAEN+kqtxHWfOApkTv+hx7Vym53n4tJBr25aRZANB5OJ/2oDiIddxT39k5FQrTqu75bQ==", null, "5e05f102-f1ed-433a-928e-43d6e2234b2c" });
        }
    }
}
