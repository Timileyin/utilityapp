﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class VendorCommissionProperties : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "AgentCommission",
                table: "Vendors",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "AgentCommissionCap",
                table: "Vendors",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "CommissionCap",
                table: "Vendors",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "CustomerCommission",
                table: "Vendors",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "CustomerCommissionCap",
                table: "Vendors",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DealerCommission",
                table: "Vendors",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DealerCommissionCap",
                table: "Vendors",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SuperDealerCommission",
                table: "Vendors",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "SuperDealerCommissionCap",
                table: "Vendors",
                nullable: false,
                defaultValue: 0m);

      }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AgentCommission",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "AgentCommissionCap",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "CommissionCap",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "CustomerCommission",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "CustomerCommissionCap",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "DealerCommission",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "DealerCommissionCap",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "SuperDealerCommission",
                table: "Vendors");

            migrationBuilder.DropColumn(
                name: "SuperDealerCommissionCap",
                table: "Vendors");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "02a8ec3b-5eb3-4d14-9f8b-1bd446dbc092", new DateTime(2020, 9, 10, 14, 1, 26, 825, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6aeb3cc4-7ad0-4580-b668-2c72d81473d9", new DateTime(2020, 9, 10, 14, 1, 26, 851, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8fe8af23-0d49-4844-8a1a-f85b919b1054", new DateTime(2020, 9, 10, 14, 1, 26, 852, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "203443dc-8621-4bd6-b310-a75031bc97f5", new DateTime(2020, 9, 10, 14, 1, 26, 852, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "f4ddb199-00d6-4dfc-8377-6be3405d97fb", new DateTime(2020, 9, 10, 14, 1, 26, 852, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "dc6d376c-02e0-4eed-a463-15e36a180258", new DateTime(2020, 9, 10, 14, 1, 26, 852, DateTimeKind.Local), "AQAAAAEAACcQAAAAEBfldCWJPAengUtixOjZiP2wYUzVD+U6pB6mY3ItLcX+7yF/XU9m+fx+IdnSrFYj6g==", null, "6699fb42-aa9e-4767-9896-16cc3e41c92a" });
        }
    }
}
