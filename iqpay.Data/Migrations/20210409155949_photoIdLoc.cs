﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class photoIdLoc : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Address",
                table: "PaternshipRegistration",
                newName: "PhotoIdLoc");

            migrationBuilder.AddColumn<string>(
                name: "AddressLine1",
                table: "PaternshipRegistration",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AddressLine2",
                table: "PaternshipRegistration",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LGA",
                table: "PaternshipRegistration",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "State",
                table: "PaternshipRegistration",
                nullable: false,
                defaultValue: 0);

          
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AddressLine1",
                table: "PaternshipRegistration");

            migrationBuilder.DropColumn(
                name: "AddressLine2",
                table: "PaternshipRegistration");

            migrationBuilder.DropColumn(
                name: "LGA",
                table: "PaternshipRegistration");

            migrationBuilder.DropColumn(
                name: "State",
                table: "PaternshipRegistration");

            migrationBuilder.RenameColumn(
                name: "PhotoIdLoc",
                table: "PaternshipRegistration",
                newName: "Address");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "5878fd28-870d-448c-b531-e1e5dc73a431", new DateTime(2021, 3, 30, 7, 40, 0, 938, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "f806fb52-ca3b-4dbf-a0be-fdf3db3bbe0b", new DateTime(2021, 3, 30, 7, 40, 0, 977, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b26bf202-11bb-4e3c-a781-9861a8d24b18", new DateTime(2021, 3, 30, 7, 40, 0, 977, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "5ba8f6b6-45e4-4550-b321-44255954a9fe", new DateTime(2021, 3, 30, 7, 40, 0, 978, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "0a018dee-f318-4618-b162-f39c73c3a2a6", new DateTime(2021, 3, 30, 7, 40, 0, 978, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "cbea4204-bda4-4ee5-9ad4-f3eab01a0b45", new DateTime(2021, 3, 30, 7, 40, 0, 978, DateTimeKind.Local), "AQAAAAEAACcQAAAAEO+AI8ImIgVAbUZSZXXjRVEW+Nv9wwxpwc9N/W3uECFkJ1AYWyGsHAb8pBUl7EtkMg==", null, "ac9bfc8b-4a55-4ebe-983f-cb6965bdc79c" });
        }
    }
}
