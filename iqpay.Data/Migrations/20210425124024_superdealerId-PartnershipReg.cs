﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class superdealerIdPartnershipReg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "SuperDealerId",
                table: "PaternshipRegistration",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SuperDealerId",
                table: "PaternshipRegistration");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "64dd545a-6185-4c77-afb3-5bcda3c91fd8", new DateTime(2021, 4, 22, 21, 7, 34, 44, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c921cad0-b74a-4016-8c5a-e1c56820a96e", new DateTime(2021, 4, 22, 21, 7, 34, 63, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "7c4d0fa0-df16-4f36-a555-fe32b677d582", new DateTime(2021, 4, 22, 21, 7, 34, 64, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b936be09-5338-4b1f-9a32-55e4f4dd1bf6", new DateTime(2021, 4, 22, 21, 7, 34, 64, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a06bd2fa-f7b1-4fca-b63f-b0431e2496d9", new DateTime(2021, 4, 22, 21, 7, 34, 64, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "d8037e9b-81c6-441e-a838-5099d79138e0", new DateTime(2021, 4, 22, 21, 7, 34, 65, DateTimeKind.Local), "AQAAAAEAACcQAAAAEN7vYzS5GREl1KcRGygKK39uUkcXVhORvotV1sS8HwMjpNrrF+NCYTMsZtHD0wc6/g==", null, "aa7179ee-9fa8-48e4-b495-9a0ce3eac05e" });
        }
    }
}
