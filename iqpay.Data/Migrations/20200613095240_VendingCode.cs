﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class VendingCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "VendingCode",
                table: "VendingLogs",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "628c20a2-e7cf-4bde-8719-dff6ea440342", new DateTime(2020, 6, 13, 10, 52, 39, 848, DateTimeKind.Local).AddTicks(5100) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "3a5965fb-e901-4ce9-9e0e-61d135f72cca", new DateTime(2020, 6, 13, 10, 52, 39, 855, DateTimeKind.Local).AddTicks(5360) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "1536d79b-6a15-4a4f-b371-b393b40246c6", new DateTime(2020, 6, 13, 10, 52, 39, 855, DateTimeKind.Local).AddTicks(5820) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b64f87e6-40a3-4ad9-aef8-ccd3178ec6d5", new DateTime(2020, 6, 13, 10, 52, 39, 855, DateTimeKind.Local).AddTicks(6050) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "0cde7644-7e36-4e5e-99ee-b8c3d60a4808", new DateTime(2020, 6, 13, 10, 52, 39, 855, DateTimeKind.Local).AddTicks(6280) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "fc998551-356b-44e9-8501-45b61f7cdf0a", new DateTime(2020, 6, 13, 10, 52, 39, 855, DateTimeKind.Local).AddTicks(8580), "AQAAAAEAACcQAAAAEK+Uv37HJuH6S5GS5lisLZqZfQUe8VXOskeycuiYYWzZmHU7w1/mlEAVLrdO/SFH4g==", "ae449cb6-c548-4934-829e-ef7d920e9ee2" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VendingCode",
                table: "VendingLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6593e890-2bea-4d22-845f-3a08f55e06c1", new DateTime(2020, 6, 10, 22, 59, 49, 976, DateTimeKind.Local).AddTicks(8130) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e5a04c41-eb6b-41ef-a342-f3909a48f8a7", new DateTime(2020, 6, 10, 22, 59, 49, 983, DateTimeKind.Local).AddTicks(2710) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a4cd98bd-9ace-40ca-bdd1-d6136108277c", new DateTime(2020, 6, 10, 22, 59, 49, 983, DateTimeKind.Local).AddTicks(2920) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e9af08ae-af1f-46d2-a211-292e4a1c02d5", new DateTime(2020, 6, 10, 22, 59, 49, 983, DateTimeKind.Local).AddTicks(3020) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "bf47ea76-2bb7-49cb-8c96-a243626517d7", new DateTime(2020, 6, 10, 22, 59, 49, 983, DateTimeKind.Local).AddTicks(3120) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "b26a811a-16aa-4783-af69-565582d20ba9", new DateTime(2020, 6, 10, 22, 59, 49, 983, DateTimeKind.Local).AddTicks(4010), "AQAAAAEAACcQAAAAEJWg6dNH/0LE6A9wXuI6nAZ0gI/K5BUeiMIXKbiW0gvb0+Y5apUcDbkcwRyFoshTLw==", "177f4d50-3f7c-47f0-abae-a8f35ecf5ccb" });
        }
    }
}
