﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class AddVendingLogsStateLGA : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "LGA",
                table: "VendingLogs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "State",
                table: "VendingLogs",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LGA",
                table: "VendingLogs");

            migrationBuilder.DropColumn(
                name: "State",
                table: "VendingLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "3d0e2839-f87a-48a3-8b38-497994374054", new DateTime(2022, 7, 11, 21, 42, 22, 419, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "1f24135e-2afe-4279-994a-74dacdfe2fb0", new DateTime(2022, 7, 11, 21, 42, 22, 422, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "3256f6ee-258e-4478-938a-81cc0960a292", new DateTime(2022, 7, 11, 21, 42, 22, 422, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "17dbaa16-f2d0-46a9-abb2-fe17b4a8f29c", new DateTime(2022, 7, 11, 21, 42, 22, 422, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "018228f5-7bf9-41a9-97da-d6bbae97bd84", new DateTime(2022, 7, 11, 21, 42, 22, 422, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "aeb8bf33-fd91-4375-93b4-2a7af87f2231", new DateTime(2022, 7, 11, 21, 42, 22, 422, DateTimeKind.Local), "AQAAAAEAACcQAAAAEPrTEmZZELhz3KVFN+XBvwb6pPVvmTf51DYUia+Paq+UgyBtGcckCDwePY0OIMYoPg==", null, "22cdb43f-d02c-42dd-91ca-ed9176fa9f1f" });
        }
    }
}
