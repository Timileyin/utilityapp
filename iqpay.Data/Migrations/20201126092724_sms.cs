﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class sms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "SMSContent",
                table: "EmailTemplates",
                nullable: true);

           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SMSContent",
                table: "EmailTemplates");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "de4131a9-02f9-461d-b4a4-5d27b160ede1", new DateTime(2020, 11, 23, 23, 47, 55, 195, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "f1f7a1ba-9687-4ba3-9719-d07f50dc7319", new DateTime(2020, 11, 23, 23, 47, 55, 214, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "3c8e8aa3-ca13-4717-9d66-7431fb39521c", new DateTime(2020, 11, 23, 23, 47, 55, 214, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e6230dcc-2f64-4446-93cf-a720b16d5089", new DateTime(2020, 11, 23, 23, 47, 55, 214, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c311e9fd-7ec4-40f5-a5be-8d4fe8bbe543", new DateTime(2020, 11, 23, 23, 47, 55, 214, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "1c16efba-15db-4aa7-b46e-6f19d4451249", new DateTime(2020, 11, 23, 23, 47, 55, 214, DateTimeKind.Local), "AQAAAAEAACcQAAAAEHHR/mzSqXl91TmfzZjoxPPLHJ6QzJYs1l8G1Mc0za56eqa7KuCF2XlxJnEX05Z1WA==", null, "d418939f-1340-4362-9978-2dd7f6db3aa5" });
        }
    }
}
