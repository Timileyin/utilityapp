﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class BankTransfer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BankTransfers",
                columns: table => new
                {
                    DateCreated = table.Column<DateTime>(nullable: false),
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastUpdateDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    DeletedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    PaymentRef = table.Column<string>(nullable: true),
                    AmountPaid = table.Column<decimal>(nullable: false),
                    PaymentStatus = table.Column<string>(nullable: true),
                    IsProcessed = table.Column<bool>(nullable: false),
                    AccountNumber = table.Column<string>(nullable: true),
                    Bank = table.Column<string>(nullable: true),
                    DatePaid = table.Column<DateTime>(nullable: false),
                    MonnifyResponse = table.Column<string>(nullable: true),
                    DateProcessed = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankTransfers", x => x.Id);
                });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BankTransfers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "208c02d4-f0f6-47c6-ba01-01a7571782fa", new DateTime(2020, 10, 29, 7, 11, 26, 673, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "1bef3bc3-5db1-491c-89fe-585b7dfbd38b", new DateTime(2020, 10, 29, 7, 11, 26, 681, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8109a570-014e-43e7-98d7-fec3f6dfaa3e", new DateTime(2020, 10, 29, 7, 11, 26, 681, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "15c9ecc6-c26d-4b2f-9061-97baa8a4e8d9", new DateTime(2020, 10, 29, 7, 11, 26, 681, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "2ffedcc5-3084-4646-a11d-a79d3101b72e", new DateTime(2020, 10, 29, 7, 11, 26, 681, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "166257ef-9300-4342-b0c0-2ee0380ad45e", new DateTime(2020, 10, 29, 7, 11, 26, 681, DateTimeKind.Local), "AQAAAAEAACcQAAAAEAeZYF8MFasZ4B07GkFhaxSVHCO5Jvmhg95mfE4FtcimgbXm1nnpK/B6+mSxEwYdvA==", null, "602e7ba9-a7c6-4bfe-926c-f5ac82e16f7e" });
        }
    }
}
