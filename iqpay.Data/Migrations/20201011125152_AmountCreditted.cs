﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class AmountCreditted : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "AmmountCreditted",
                table: "FundWallet",
                nullable: false,
                defaultValue: 0m);

            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AmmountCreditted",
                table: "FundWallet");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "42258a01-68d6-405a-9b6a-2f471f935f5e", new DateTime(2020, 10, 11, 9, 27, 44, 715, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "18e1eb89-565e-4d7f-83fd-45be195dbeeb", new DateTime(2020, 10, 11, 9, 27, 44, 877, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "1e023f62-9cc7-4bed-a8b4-5a82dd10c0d3", new DateTime(2020, 10, 11, 9, 27, 44, 877, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e4db078f-782f-49be-9da9-c8582904b108", new DateTime(2020, 10, 11, 9, 27, 44, 877, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "00e0db80-25d9-415b-bed0-46344b79b3cf", new DateTime(2020, 10, 11, 9, 27, 44, 877, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "ae8df547-d1b7-4f6c-ac10-42972227a312", new DateTime(2020, 10, 11, 9, 27, 44, 877, DateTimeKind.Local), "AQAAAAEAACcQAAAAEObocorNny8ZFwo9FFRQsc7uTcAtqZda3s+MrBA7TTuG3tbYJAGjbdmmtFhqJMDkZQ==", null, "ae2dd48b-5857-4dad-a40f-90b9c12e9531" });
        }
    }
}
