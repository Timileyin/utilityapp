﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class ServicesCOmmissionMOde : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsEnabled",
                table: "Wallets",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "CommissionMode",
                table: "Services",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "36b16711-7d60-45bb-9e66-27f073b852d7", new DateTime(2020, 4, 10, 13, 23, 53, 693, DateTimeKind.Local).AddTicks(8780) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "dd445723-db03-4c22-85c3-be20e6bea99d", new DateTime(2020, 4, 10, 13, 23, 53, 699, DateTimeKind.Local).AddTicks(4700) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "32da576b-adb7-406d-be7d-c3fa50ba1a06", new DateTime(2020, 4, 10, 13, 23, 53, 699, DateTimeKind.Local).AddTicks(5090) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "10312685-1e26-435b-98de-50e2295cebac", new DateTime(2020, 4, 10, 13, 23, 53, 699, DateTimeKind.Local).AddTicks(5240) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "99240795-a869-41d4-b0bf-37b59814e32b", new DateTime(2020, 4, 10, 13, 23, 53, 699, DateTimeKind.Local).AddTicks(5380) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "545d72ab-b2b0-44e4-b6f8-d1787a6783d7", new DateTime(2020, 4, 10, 13, 23, 53, 699, DateTimeKind.Local).AddTicks(6880), "AQAAAAEAACcQAAAAEMS/ZHOAXE8q6QDqIcm/k8U3jE817oRGHpGWD4pXDBERVDzZspkN95RV9tQo2pTRFQ==", "06ca077b-6ee6-442d-ab4e-d513d7891fe2" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsEnabled",
                table: "Wallets");

            migrationBuilder.DropColumn(
                name: "CommissionMode",
                table: "Services");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "0c9a395c-c058-4ca3-b216-8deae2d1bc66", new DateTime(2020, 4, 3, 14, 17, 39, 653, DateTimeKind.Local).AddTicks(7890) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6bdb3807-df13-49af-90aa-103a64657548", new DateTime(2020, 4, 3, 14, 17, 39, 668, DateTimeKind.Local).AddTicks(6490) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "01a7272e-311f-4489-b6ee-b198a3f58ae6", new DateTime(2020, 4, 3, 14, 17, 39, 668, DateTimeKind.Local).AddTicks(6740) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "9acfa036-a875-4ffe-b456-d29fb29f2cb6", new DateTime(2020, 4, 3, 14, 17, 39, 668, DateTimeKind.Local).AddTicks(6850) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "56523e53-917a-4cf1-a600-182804bb235d", new DateTime(2020, 4, 3, 14, 17, 39, 668, DateTimeKind.Local).AddTicks(6960) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "39d34758-5f53-450b-a749-ca0f06f1975a", new DateTime(2020, 4, 3, 14, 17, 39, 668, DateTimeKind.Local).AddTicks(8140), "AQAAAAEAACcQAAAAEMCdCKFnYy6wHOoSgVvrpzl7NDLZuXlf5s/O/8HvjRzhHT41ganz67wYMT4nZGAkGA==", "ac5a5b6f-28a7-43b7-b85a-47d9f6321861" });
        }
    }
}
