﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class RubiesAccountNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "RubiesAccountName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RubiesAccountNumber",
                table: "AspNetUsers",
                nullable: true);
           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RubiesAccountName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "RubiesAccountNumber",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "9ac43055-1b50-46cf-b57b-fff91aa9f721", new DateTime(2020, 11, 17, 11, 54, 1, 259, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "2fd3e31c-b9d8-4668-b0e6-d5701c937494", new DateTime(2020, 11, 17, 11, 54, 1, 279, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ea2753e1-093c-4aa0-830b-b48c54bd997f", new DateTime(2020, 11, 17, 11, 54, 1, 279, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "37494f0d-154c-4554-9899-ce673567f8c4", new DateTime(2020, 11, 17, 11, 54, 1, 279, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ee8e0230-de0e-4a01-8de0-8ae59c0966ce", new DateTime(2020, 11, 17, 11, 54, 1, 279, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "7d6fbf99-eb79-4a69-be85-b65dc1b76ee7", new DateTime(2020, 11, 17, 11, 54, 1, 279, DateTimeKind.Local), "AQAAAAEAACcQAAAAEOSingMs36HCy8NcyI7GvgjcKeqS79HKUdfXrQ6zRKy3fLlW9LU4g1FN8vH4fG8nMQ==", null, "dc7a105a-41da-4d6c-8d78-4a245a9f27b0" });
        }
    }
}
