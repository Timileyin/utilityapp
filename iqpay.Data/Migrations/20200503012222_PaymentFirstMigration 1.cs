﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class PaymentFirstMigration1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "00de116b-ad22-41c1-89cf-c61e57f8af11", new DateTime(2020, 5, 3, 2, 22, 20, 860, DateTimeKind.Local).AddTicks(7200) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c258467f-24d1-491d-84bd-330cf73fb49b", new DateTime(2020, 5, 3, 2, 22, 20, 868, DateTimeKind.Local).AddTicks(5420) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "545ee04b-1741-44f2-8e5d-a3ef3afe6426", new DateTime(2020, 5, 3, 2, 22, 20, 868, DateTimeKind.Local).AddTicks(5670) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ac9c6698-4534-4919-9851-79c8cef1d45e", new DateTime(2020, 5, 3, 2, 22, 20, 868, DateTimeKind.Local).AddTicks(5780) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "9f3b8a0b-a596-44de-a60d-aa754346b526", new DateTime(2020, 5, 3, 2, 22, 20, 868, DateTimeKind.Local).AddTicks(5900) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "964ad817-cbaa-4d92-971d-425116ef6a8b", new DateTime(2020, 5, 3, 2, 22, 20, 868, DateTimeKind.Local).AddTicks(7090), "AQAAAAEAACcQAAAAEJbXrKEIbgKoIfULPBJ7i2/cHRYDWBZlspilY0zgrAoDBDmjW19vLD40idRHEzAHSg==", "b2515a06-574f-454c-859b-bcca8d862238" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "20f7327b-87e8-4487-9d78-0c24ee15e1e4", new DateTime(2020, 5, 3, 2, 18, 38, 869, DateTimeKind.Local).AddTicks(8100) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "69c17b3b-f782-4dec-ad4d-4f4155790e90", new DateTime(2020, 5, 3, 2, 18, 38, 884, DateTimeKind.Local).AddTicks(2520) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "4dc2d53f-7e29-4bcf-b0de-f73b438c86f8", new DateTime(2020, 5, 3, 2, 18, 38, 884, DateTimeKind.Local).AddTicks(2830) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "2ddf3e7c-4132-4d3c-ba82-a0e17f3b5409", new DateTime(2020, 5, 3, 2, 18, 38, 884, DateTimeKind.Local).AddTicks(2980) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "21f38b1a-159d-4b00-bd4d-256c6cc453ad", new DateTime(2020, 5, 3, 2, 18, 38, 884, DateTimeKind.Local).AddTicks(3120) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "93eb5370-5686-4f48-ad6d-67f2dbcd9720", new DateTime(2020, 5, 3, 2, 18, 38, 884, DateTimeKind.Local).AddTicks(4420), "AQAAAAEAACcQAAAAEJLi12Dc1UUDR/enI83CvfofJQir7SELnmuXwAcqJXmnYgDRGvqFXhG1DlN4nWwIxA==", "27609158-bfbe-4fef-960b-7152f3123109" });
        }
    }
}
