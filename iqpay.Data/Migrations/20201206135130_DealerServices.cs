﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class DealerServices : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DealersServices",
                columns: table => new
                {
                    DateCreated = table.Column<DateTime>(nullable: false),
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastUpdateDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    DeletedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    UserId = table.Column<long>(nullable: false),
                    ServiceId = table.Column<long>(nullable: true),
                    VendorId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DealersServices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DealersServices_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DealersServices_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DealersServices_Vendors_VendorId",
                        column: x => x.VendorId,
                        principalTable: "Vendors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            
            migrationBuilder.CreateIndex(
                name: "IX_DealersServices_ServiceId",
                table: "DealersServices",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_DealersServices_UserId",
                table: "DealersServices",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_DealersServices_VendorId",
                table: "DealersServices",
                column: "VendorId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DealersServices");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "948475ce-6e16-4a74-8f0a-37d404fb6842", new DateTime(2020, 12, 4, 6, 10, 22, 642, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "945b7ba7-4c15-410b-ae75-daf8854181f7", new DateTime(2020, 12, 4, 6, 10, 22, 650, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a952a6e6-3eab-4c84-ad4c-1f1725f23023", new DateTime(2020, 12, 4, 6, 10, 22, 650, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "cf89c0a1-35ca-4510-a139-e6a580524977", new DateTime(2020, 12, 4, 6, 10, 22, 650, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ebe872cc-27f7-42a2-9469-13f969d22832", new DateTime(2020, 12, 4, 6, 10, 22, 650, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "7531ccf2-fe8f-479b-9dd1-f8b31039d7d0", new DateTime(2020, 12, 4, 6, 10, 22, 650, DateTimeKind.Local), "AQAAAAEAACcQAAAAEK1Y1LuV6hcZNPjUNaF2n9M4WhFjZtv4v98Ca+RG+Rmm6WRhZR2R+J2I1TcZQklw3w==", null, "c7b7d32f-681e-4704-820f-fa50a0b3e8bd" });
        }
    }
}
