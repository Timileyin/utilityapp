﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class servicesCommissionII : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "CommissionCap",
                table: "Services",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "CommissionCap",
                table: "DealerCommission",
                nullable: false,
                defaultValue: 0m);
           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CommissionCap",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "CommissionCap",
                table: "DealerCommission");

            
        }
    }
}
