﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class smsLogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SMSLogs",
                columns: table => new
                {
                    DateCreated = table.Column<DateTime>(nullable: false),
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastUpdateDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    DeletedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    Receiver = table.Column<string>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    DateToSend = table.Column<DateTime>(nullable: false),
                    IsSendImmediately = table.Column<bool>(nullable: false),
                    MessageId = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SMSLogs", x => x.Id);
                });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SMSLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6a740003-e78d-4c2f-baa0-c13859473527", new DateTime(2020, 11, 26, 10, 27, 21, 742, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b8a3253a-0cf5-4613-8f08-37e7a1e68410", new DateTime(2020, 11, 26, 10, 27, 21, 788, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a2d1ea40-00d1-47b8-b379-52024f91a31c", new DateTime(2020, 11, 26, 10, 27, 21, 788, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "f56c0c98-f9f0-4668-8ff1-afd5a255cd63", new DateTime(2020, 11, 26, 10, 27, 21, 788, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "2fb3f495-381e-45cb-b16a-fcdb222a764e", new DateTime(2020, 11, 26, 10, 27, 21, 788, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "72b3f42b-f22a-4c46-8878-671e3dd3438d", new DateTime(2020, 11, 26, 10, 27, 21, 789, DateTimeKind.Local), "AQAAAAEAACcQAAAAENce4sstKd+bUG5GYnNqUs7Das/142uOHUK3r3rMFIq8BKynbkq0VSi2Z5uFFEew7w==", null, "9b2ee844-6e25-42c4-8ff3-8a730e59be4d" });
        }
    }
}
