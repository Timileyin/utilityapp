﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class addvendRespone : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Commission",
                table: "VendingLogs",
                newName: "SuperDealerCommission");

            migrationBuilder.AlterColumn<decimal>(
                name: "SuperDealerCommissionCap",
                table: "Vendors",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "DealerCommissionCap",
                table: "Vendors",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "CustomerCommissionCap",
                table: "Vendors",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "CommissionCap",
                table: "Vendors",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "AgentCommissionCap",
                table: "Vendors",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AddColumn<decimal>(
                name: "AgentCommission",
                table: "VendingLogs",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DealerCommission",
                table: "VendingLogs",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "ProcessorsPayLoad",
                table: "VendingLogs",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AgentCommission",
                table: "VendingLogs");

            migrationBuilder.DropColumn(
                name: "DealerCommission",
                table: "VendingLogs");

            migrationBuilder.DropColumn(
                name: "ProcessorsPayLoad",
                table: "VendingLogs");

            migrationBuilder.RenameColumn(
                name: "SuperDealerCommission",
                table: "VendingLogs",
                newName: "Commission");

            migrationBuilder.AlterColumn<decimal>(
                name: "SuperDealerCommissionCap",
                table: "Vendors",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DealerCommissionCap",
                table: "Vendors",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CustomerCommissionCap",
                table: "Vendors",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CommissionCap",
                table: "Vendors",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "AgentCommissionCap",
                table: "Vendors",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "527b737b-5d09-4889-b7bd-15af7f2c2935", new DateTime(2020, 9, 11, 22, 17, 40, 570, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "3d001e19-4186-4c64-b14f-995b954487de", new DateTime(2020, 9, 11, 22, 17, 40, 582, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "63957a36-638d-47df-b3f9-ad702a312161", new DateTime(2020, 9, 11, 22, 17, 40, 582, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8d955abd-6c55-415c-94ec-868ad49aeb38", new DateTime(2020, 9, 11, 22, 17, 40, 582, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "f3c687b1-c1cb-4a2e-a271-2a37e65a83df", new DateTime(2020, 9, 11, 22, 17, 40, 582, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "2a718126-2255-4a22-b7bd-f53df58feb74", new DateTime(2020, 9, 11, 22, 17, 40, 582, DateTimeKind.Local), "AQAAAAEAACcQAAAAEAge/75YQDNQbo2prl5rSzmwxS0zEDVwqxim3RwLcGyn2TS/+hPDKpLgflilR6bBhQ==", null, "985be953-44d4-4c59-a5e2-958d38dd5c40" });
        }
    }
}
