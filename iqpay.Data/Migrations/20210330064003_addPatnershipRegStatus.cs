﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class addPatnershipRegStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            

            migrationBuilder.AlterColumn<string>(
                name: "CreatedBy",
                table: "PaternshipRegistration",
                nullable: true,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ApprovedBy",
                table: "PaternshipRegistration",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Status",
                table: "PaternshipRegistration",
                nullable: true);

           
            migrationBuilder.CreateIndex(
                name: "IX_PaternshipRegistration_ApprovedBy",
                table: "PaternshipRegistration",
                column: "ApprovedBy");

            migrationBuilder.AddForeignKey(
                name: "FK_PaternshipRegistration_AspNetUsers_ApprovedBy",
                table: "PaternshipRegistration",
                column: "ApprovedBy",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PaternshipRegistration_AspNetUsers_ApprovedBy",
                table: "PaternshipRegistration");

            migrationBuilder.DropIndex(
                name: "IX_PaternshipRegistration_ApprovedBy",
                table: "PaternshipRegistration");

            migrationBuilder.DropColumn(
                name: "ApprovedBy",
                table: "PaternshipRegistration");
            
            migrationBuilder.DropColumn(
                name: "Status",
                table: "PaternshipRegistration");

            migrationBuilder.AlterColumn<long>(
                name: "CreatedBy",
                table: "PaternshipRegistration",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "bd3fed2e-bbc1-4598-8d69-0aa46095cf82", new DateTime(2021, 3, 30, 7, 26, 28, 843, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "04437226-d534-4079-bc65-896560ef2149", new DateTime(2021, 3, 30, 7, 26, 28, 856, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "44e9f9c5-451b-4613-9bab-be7c05401792", new DateTime(2021, 3, 30, 7, 26, 28, 856, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "bb87fea4-ce89-4244-83e9-e62c103bba95", new DateTime(2021, 3, 30, 7, 26, 28, 856, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "3d995a16-b965-40e8-9dcd-163d2efb9386", new DateTime(2021, 3, 30, 7, 26, 28, 856, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "84c57009-446c-4ce5-86ed-0047f0712718", new DateTime(2021, 3, 30, 7, 26, 28, 856, DateTimeKind.Local), "AQAAAAEAACcQAAAAEGH4CgjFgQoAZ7saEz36iMaG7TfrXIBttCi7w35vneEm7exzhG+BvhBdkGDjfrjPkQ==", null, "a4695f72-3256-48cf-a9d1-9fd548950ab7" });

            migrationBuilder.CreateIndex(
                name: "IX_PaternshipRegistration_CreatedBy",
                table: "PaternshipRegistration",
                column: "CreatedBy");

            migrationBuilder.AddForeignKey(
                name: "FK_PaternshipRegistration_AspNetUsers_CreatedBy",
                table: "PaternshipRegistration",
                column: "CreatedBy",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
