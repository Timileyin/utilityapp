﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class AccountIdVerification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AccountName",
                table: "VendingLogs",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "RequiresVerification",
                table: "Services",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c6dd668e-f502-4591-b273-97f304143615", new DateTime(2020, 7, 5, 8, 6, 23, 635, DateTimeKind.Local).AddTicks(3930) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "116566ac-de53-4551-8707-0d501f0ed06e", new DateTime(2020, 7, 5, 8, 6, 23, 642, DateTimeKind.Local).AddTicks(8210) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "67d02e87-777a-4084-b33d-d4ffc4827090", new DateTime(2020, 7, 5, 8, 6, 23, 642, DateTimeKind.Local).AddTicks(8430) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "250dda41-2c13-4050-96eb-23e89e19a5b8", new DateTime(2020, 7, 5, 8, 6, 23, 642, DateTimeKind.Local).AddTicks(8530) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "d21f0500-d52a-48c8-b5a0-a54b5c05862a", new DateTime(2020, 7, 5, 8, 6, 23, 642, DateTimeKind.Local).AddTicks(8630) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "dacc21e6-e61b-4df0-87af-47d5b037578e", new DateTime(2020, 7, 5, 8, 6, 23, 643, DateTimeKind.Local).AddTicks(250), "AQAAAAEAACcQAAAAEB3n2bvwoXAAGj9skheOe5xRE3QcU7wCumVBE/tO8QQmT7k6Vy0uALXzFiiV37+OSg==", "a19c7560-9476-4086-b59c-5f74d0f7be2c" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountName",
                table: "VendingLogs");

            migrationBuilder.DropColumn(
                name: "RequiresVerification",
                table: "Services");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "628c20a2-e7cf-4bde-8719-dff6ea440342", new DateTime(2020, 6, 13, 10, 52, 39, 848, DateTimeKind.Local).AddTicks(5100) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "3a5965fb-e901-4ce9-9e0e-61d135f72cca", new DateTime(2020, 6, 13, 10, 52, 39, 855, DateTimeKind.Local).AddTicks(5360) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "1536d79b-6a15-4a4f-b371-b393b40246c6", new DateTime(2020, 6, 13, 10, 52, 39, 855, DateTimeKind.Local).AddTicks(5820) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b64f87e6-40a3-4ad9-aef8-ccd3178ec6d5", new DateTime(2020, 6, 13, 10, 52, 39, 855, DateTimeKind.Local).AddTicks(6050) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "0cde7644-7e36-4e5e-99ee-b8c3d60a4808", new DateTime(2020, 6, 13, 10, 52, 39, 855, DateTimeKind.Local).AddTicks(6280) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "fc998551-356b-44e9-8501-45b61f7cdf0a", new DateTime(2020, 6, 13, 10, 52, 39, 855, DateTimeKind.Local).AddTicks(8580), "AQAAAAEAACcQAAAAEK+Uv37HJuH6S5GS5lisLZqZfQUe8VXOskeycuiYYWzZmHU7w1/mlEAVLrdO/SFH4g==", "ae449cb6-c548-4934-829e-ef7d920e9ee2" });
        }
    }
}
