﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class AccountAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AccountAddress",
                table: "VendingLogs",
                nullable: true);
            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountAddress",
                table: "VendingLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "15856dd2-0c3e-4560-860f-bef3aabe1d40", new DateTime(2020, 11, 4, 23, 57, 48, 46, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "66ea1364-1d8e-4a9e-a508-e1a98b17ede6", new DateTime(2020, 11, 4, 23, 57, 48, 54, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8b381ee3-b65d-44bc-a377-c3fe4bee8e79", new DateTime(2020, 11, 4, 23, 57, 48, 54, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "50fed177-3c14-4922-a380-3b374149a10e", new DateTime(2020, 11, 4, 23, 57, 48, 54, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "0788c679-d9fb-45cf-ab91-e952d9abd937", new DateTime(2020, 11, 4, 23, 57, 48, 54, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "4115e29c-1fd5-40ad-b511-f243e7ac7f3b", new DateTime(2020, 11, 4, 23, 57, 48, 54, DateTimeKind.Local), "AQAAAAEAACcQAAAAEMEgT5j6p5U90eg1m6oysBcUy7XH7cYLuwWaZ/c9GO0GiJ7DJFvFmGD5TDnFcyLAOw==", null, "958828b9-ccc7-41c3-a209-cce221899c98" });
        }
    }
}
