﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class TransactionDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "TransactionDate",
                table: "FundWallet",
                nullable: true,
                oldClrType: typeof(DateTime));

            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<DateTime>(
                name: "TransactionDate",
                table: "FundWallet",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ac52b87c-b9f7-4b57-85eb-43cdef32ab88", new DateTime(2020, 9, 27, 15, 54, 51, 195, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "0de78637-5540-4e14-816d-e406a477e4b8", new DateTime(2020, 9, 27, 15, 54, 51, 210, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "cfdfcc72-fe8b-4d7f-8fa8-a293e29f1f1e", new DateTime(2020, 9, 27, 15, 54, 51, 210, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "69ffb422-9478-4b1e-8c3e-5f46b50769e4", new DateTime(2020, 9, 27, 15, 54, 51, 210, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "d247e4ff-67b7-4576-81ee-560ebce262f6", new DateTime(2020, 9, 27, 15, 54, 51, 210, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "b8c63b82-c0ba-4e35-81e5-3b8dd801c0ce", new DateTime(2020, 9, 27, 15, 54, 51, 210, DateTimeKind.Local), "AQAAAAEAACcQAAAAEBEXZElVeO38+h0ne2rf04ku6H6nIGgFOTust/HdQdyoyyLPX0fpcAYW/j5NFZuZPQ==", null, "7da132b1-4d8f-4ffe-8fb0-4d9be66865ae" });
        }
    }
}
