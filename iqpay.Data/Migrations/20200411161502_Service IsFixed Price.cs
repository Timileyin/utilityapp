﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class ServiceIsFixedPrice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "isFixedPrice",
                table: "Services",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "cac7a5fe-11bf-49ef-9959-099f919ddafe", new DateTime(2020, 4, 11, 17, 15, 1, 88, DateTimeKind.Local).AddTicks(8060) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "bae27ab5-cb67-411e-b082-faa6e9dbb522", new DateTime(2020, 4, 11, 17, 15, 1, 110, DateTimeKind.Local).AddTicks(5720) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a386d99f-146f-4985-b032-ea9d18751d8a", new DateTime(2020, 4, 11, 17, 15, 1, 110, DateTimeKind.Local).AddTicks(6370) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "455f856e-7218-4f8b-911a-327a8276ed13", new DateTime(2020, 4, 11, 17, 15, 1, 110, DateTimeKind.Local).AddTicks(6540) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "cdb341a3-ab81-4808-8ca5-47886265751a", new DateTime(2020, 4, 11, 17, 15, 1, 110, DateTimeKind.Local).AddTicks(6690) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9aa62623-03bd-4dc9-ac8c-3e7d9c05d566", new DateTime(2020, 4, 11, 17, 15, 1, 110, DateTimeKind.Local).AddTicks(8150), "AQAAAAEAACcQAAAAEERghhpEzpXyn9N21D5k0N5yej3Lslab38SffqmO/8ek78WCo/jcgYkNMRKCaS3dng==", "4cf2e492-9c00-4667-ad90-d15a62900072" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isFixedPrice",
                table: "Services");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "36b16711-7d60-45bb-9e66-27f073b852d7", new DateTime(2020, 4, 10, 13, 23, 53, 693, DateTimeKind.Local).AddTicks(8780) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "dd445723-db03-4c22-85c3-be20e6bea99d", new DateTime(2020, 4, 10, 13, 23, 53, 699, DateTimeKind.Local).AddTicks(4700) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "32da576b-adb7-406d-be7d-c3fa50ba1a06", new DateTime(2020, 4, 10, 13, 23, 53, 699, DateTimeKind.Local).AddTicks(5090) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "10312685-1e26-435b-98de-50e2295cebac", new DateTime(2020, 4, 10, 13, 23, 53, 699, DateTimeKind.Local).AddTicks(5240) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "99240795-a869-41d4-b0bf-37b59814e32b", new DateTime(2020, 4, 10, 13, 23, 53, 699, DateTimeKind.Local).AddTicks(5380) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "545d72ab-b2b0-44e4-b6f8-d1787a6783d7", new DateTime(2020, 4, 10, 13, 23, 53, 699, DateTimeKind.Local).AddTicks(6880), "AQAAAAEAACcQAAAAEMS/ZHOAXE8q6QDqIcm/k8U3jE817oRGHpGWD4pXDBERVDzZspkN95RV9tQo2pTRFQ==", "06ca077b-6ee6-442d-ab4e-d513d7891fe2" });
        }
    }
}
