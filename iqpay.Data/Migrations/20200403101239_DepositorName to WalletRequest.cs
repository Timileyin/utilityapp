﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class DepositorNametoWalletRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DepositorName",
                table: "FundWallet",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "79b04e26-e2bf-4677-8d1b-fb6b6ff0bb5c", new DateTime(2020, 4, 3, 11, 12, 38, 381, DateTimeKind.Local).AddTicks(4650) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "02f396b5-140b-4b16-87e2-c8ac2c80d008", new DateTime(2020, 4, 3, 11, 12, 38, 392, DateTimeKind.Local).AddTicks(7340) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b7fbed2b-4d24-4f98-a256-9d003abfc7a5", new DateTime(2020, 4, 3, 11, 12, 38, 392, DateTimeKind.Local).AddTicks(7770) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "644c1103-13df-454b-b92c-3fc0aab12273", new DateTime(2020, 4, 3, 11, 12, 38, 392, DateTimeKind.Local).AddTicks(7900) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "14d50c85-31cd-457b-a98a-7e2fe985dfa6", new DateTime(2020, 4, 3, 11, 12, 38, 392, DateTimeKind.Local).AddTicks(8020) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "736ff83b-d78e-49b9-be2d-e5a3b7376460", new DateTime(2020, 4, 3, 11, 12, 38, 392, DateTimeKind.Local).AddTicks(9520), "AQAAAAEAACcQAAAAENMKvqchEy3relCntLFsNcseQ4p2q0VZNjR7dTI/OxNOcNLIcSapCfPlIFKT7PAoLA==", "204e20a4-db93-4f19-97a1-5645272bcbfa" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DepositorName",
                table: "FundWallet");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c6ca5e4c-4ef7-4eae-b3fd-83d0133a5aba", new DateTime(2020, 4, 1, 14, 20, 48, 123, DateTimeKind.Local).AddTicks(6300) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "76089cd6-8fda-4802-85d7-ab378e60e03e", new DateTime(2020, 4, 1, 14, 20, 48, 134, DateTimeKind.Local).AddTicks(1910) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "550dbcc6-a54b-4b61-be65-9dfc2b60c84e", new DateTime(2020, 4, 1, 14, 20, 48, 134, DateTimeKind.Local).AddTicks(2540) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "af001c63-7401-4c9c-bfd6-fc3128a4366b", new DateTime(2020, 4, 1, 14, 20, 48, 134, DateTimeKind.Local).AddTicks(3050) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e3b29911-6487-4df5-8761-cf5edebfe134", new DateTime(2020, 4, 1, 14, 20, 48, 134, DateTimeKind.Local).AddTicks(3300) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "c176de54-d958-4e32-8ed5-94b1d4833e28", new DateTime(2020, 4, 1, 14, 20, 48, 134, DateTimeKind.Local).AddTicks(4810), "AQAAAAEAACcQAAAAED8bnNaCkH1dWHP5igWlULtUV0BePmAWFTUaUhqsZDU+SP6DyLYzV0f2sLGRySPJJg==", "c156a9a0-2ab6-458f-aaa6-e0b9a8e7f640" });
        }
    }
}
