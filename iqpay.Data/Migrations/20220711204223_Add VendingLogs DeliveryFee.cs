﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class AddVendingLogsDeliveryFee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "DeliveryFee",
                table: "VendingLogs",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeliveryFee",
                table: "VendingLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "2189cd14-a1ce-40fa-9bfd-b236c6bb06aa", new DateTime(2022, 7, 11, 12, 19, 17, 653, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e3f9b65a-46bb-495f-a98a-852563ff74a3", new DateTime(2022, 7, 11, 12, 19, 17, 655, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8bd55475-1bf9-4391-a0f3-0d2fcce1c290", new DateTime(2022, 7, 11, 12, 19, 17, 655, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e63709d4-77cc-4eff-9c74-78bd7ad85443", new DateTime(2022, 7, 11, 12, 19, 17, 655, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "63b30a69-2d1f-41d7-8027-69903a3f26b0", new DateTime(2022, 7, 11, 12, 19, 17, 655, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "7f70d3ab-4da1-4ea9-a1e9-35cb355719b4", new DateTime(2022, 7, 11, 12, 19, 17, 655, DateTimeKind.Local), "AQAAAAEAACcQAAAAENBFwR9xczAjO+ZBGw0oHrua7oDA0AiglrktkzS1Yn53Oj9gYSkribiIqJasCZaoBg==", null, "582689a9-584f-4a1e-b14e-1115af4fb0ff" });
        }
    }
}
