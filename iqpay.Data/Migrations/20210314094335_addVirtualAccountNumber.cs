﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class addVirtualAccountNumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CreditAccount",
                table: "BankTransfers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VirtualAccountNumber",
                table: "AspNetUsers",
                nullable: true);            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CreditAccount",
                table: "BankTransfers");

            migrationBuilder.DropColumn(
                name: "VirtualAccountNumber",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b48f26a0-2a32-4715-8cec-02d15a75b115", new DateTime(2021, 1, 31, 17, 45, 27, 36, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "92fb4a53-68e6-494e-8102-d06bc198af05", new DateTime(2021, 1, 31, 17, 45, 27, 43, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "09a71059-de49-44ee-bf16-6ce4e70c27ff", new DateTime(2021, 1, 31, 17, 45, 27, 43, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6ff041ab-e84a-4b2a-98ad-8be4edb20fff", new DateTime(2021, 1, 31, 17, 45, 27, 43, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "88c28a44-52f1-4edf-b6cb-d0a934fe25a7", new DateTime(2021, 1, 31, 17, 45, 27, 43, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "bf7f63da-7bc8-4b2a-ad0b-cf8975783500", new DateTime(2021, 1, 31, 17, 45, 27, 43, DateTimeKind.Local), "AQAAAAEAACcQAAAAEDTyDifbKImdttB9pnX940LWre/Z3AVbvh2en4Y5q6oMUOHjneAUjoL6b9twpKtUIg==", null, "0702fbed-7d13-4e9e-a79a-0213e5cfc255" });
        }
    }
}
