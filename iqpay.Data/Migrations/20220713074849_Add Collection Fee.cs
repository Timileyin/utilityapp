﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class AddCollectionFee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "FlatCollectionFee",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "FlatCollectionFeeBeyondCap",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "FlatCollectionFeeCap",
                table: "Services",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FlatCollectionFee",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "FlatCollectionFeeBeyondCap",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "FlatCollectionFeeCap",
                table: "Services");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a8283ffb-40a0-4640-961e-15472a10f21b", new DateTime(2022, 7, 12, 12, 28, 29, 104, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c8e0d968-f2fb-4822-8e44-6a959dedc188", new DateTime(2022, 7, 12, 12, 28, 29, 106, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "d777b55b-500a-4215-9366-3c6d1db93025", new DateTime(2022, 7, 12, 12, 28, 29, 107, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "bc82f328-09d7-4dbc-856a-1408dd07a856", new DateTime(2022, 7, 12, 12, 28, 29, 107, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "1368959a-2d79-473c-afea-b2bbf461ee30", new DateTime(2022, 7, 12, 12, 28, 29, 107, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "58f0c340-32bf-42ef-87e1-5063513b2595", new DateTime(2022, 7, 12, 12, 28, 29, 107, DateTimeKind.Local), "AQAAAAEAACcQAAAAEI5s7ahAaCW0zv8UctQAuSOgW9DtZMCPCsJn77VUCrbhbjE+7ezjnTtiMu4B9rX6Wg==", null, "78ca062e-7a3a-4969-a13a-d9808ae4f818" });
        }
    }
}
