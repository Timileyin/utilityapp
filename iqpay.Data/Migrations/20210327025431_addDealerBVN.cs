﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class addDealerBVN : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BVN",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NIN",
                table: "AspNetUsers",
                nullable: true);

                }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BVN",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "NIN",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a9451c37-4752-4881-9386-0077e136b285", new DateTime(2021, 3, 17, 14, 45, 11, 837, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "5a271670-4b79-47d6-b266-1a37a69f107a", new DateTime(2021, 3, 17, 14, 45, 11, 929, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "1e5fb35a-2bce-479c-8c11-97ad7e6347b3", new DateTime(2021, 3, 17, 14, 45, 11, 929, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "2358163f-db92-4de5-b143-c3f2413b7f97", new DateTime(2021, 3, 17, 14, 45, 11, 929, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a9f7d0b7-598b-4123-af5c-80479c5d1e4d", new DateTime(2021, 3, 17, 14, 45, 11, 929, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "efb005e1-7697-47e3-802b-b0fc8f51ba8f", new DateTime(2021, 3, 17, 14, 45, 11, 929, DateTimeKind.Local), "AQAAAAEAACcQAAAAENblM//Mkl8xsS8NwLLJqHr3uGpuaGy8WKb9xhjedUVUwsTx9NFeDWtg+Jw2f7bBqA==", null, "2bf73da7-6acb-4f9c-9528-23f70ddfa81d" });
        }
    }
}
