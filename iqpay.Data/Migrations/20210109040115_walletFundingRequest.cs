﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class walletFundingRequest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "CredittedWalletBalance",
                table: "FundWallet",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DebittedWalletBalance",
                table: "FundWallet",
                nullable: false,
                defaultValue: 0m);

            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CredittedWalletBalance",
                table: "FundWallet");

            migrationBuilder.DropColumn(
                name: "DebittedWalletBalance",
                table: "FundWallet");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "2a9e700d-7911-433e-bf69-542623dcfae2", new DateTime(2021, 1, 4, 16, 26, 24, 728, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "9426bad4-7be5-4c4a-b072-bbde5c48692b", new DateTime(2021, 1, 4, 16, 26, 24, 735, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "d22d4b82-18ca-4300-b21d-631f6f51db38", new DateTime(2021, 1, 4, 16, 26, 24, 735, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "7f10ded3-26e1-42e9-a1fd-1c8bb032b3bf", new DateTime(2021, 1, 4, 16, 26, 24, 735, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "92d57950-4a5c-49a3-9b6d-6f92403461d2", new DateTime(2021, 1, 4, 16, 26, 24, 735, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "6c580218-7077-41c6-8aae-6c202197b64d", new DateTime(2021, 1, 4, 16, 26, 24, 735, DateTimeKind.Local), "AQAAAAEAACcQAAAAEPAb6ZS4EezZE0vZkc8O7rYUR5RUvcb/15uqPc7+zyJFvCqZLSFGHtfnVJ0Q/Im5nA==", null, "fe8d97c1-f693-4996-a9af-e8ea7a1d740d" });
        }
    }
}
