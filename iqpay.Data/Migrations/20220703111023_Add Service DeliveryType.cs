﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class AddServiceDeliveryType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DeliveryType",
                table: "Services",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DeliveryType",
                table: "Services");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "9c50a666-7111-490b-bf4d-a5a8a09dd448", new DateTime(2022, 7, 2, 19, 0, 2, 140, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "98dd82c9-3af0-47aa-baf6-3233b696d874", new DateTime(2022, 7, 2, 19, 0, 2, 142, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "839bf3d8-8332-41f8-aed5-6bd632a91fe5", new DateTime(2022, 7, 2, 19, 0, 2, 142, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b29fd2fc-fb29-43e2-be28-180f66b3f8dc", new DateTime(2022, 7, 2, 19, 0, 2, 142, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "241c9312-6703-4eaa-ac37-cc00f388c46b", new DateTime(2022, 7, 2, 19, 0, 2, 142, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "60241bdd-572c-4673-abf7-45975d2c29ca", new DateTime(2022, 7, 2, 19, 0, 2, 142, DateTimeKind.Local), "AQAAAAEAACcQAAAAEEyUy+9lgxAvNJj08ZX4y95B+KbZZAVrkqDLNqH1nwj6MAH6R9uQpxkiJkkIXgY+HQ==", null, "842ac467-4ced-491f-9837-a32fc83224bb" });
        }
    }
}
