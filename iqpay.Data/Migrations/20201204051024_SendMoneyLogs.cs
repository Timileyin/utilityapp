﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class SendMoneyLogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SendMoneyLogs",
                columns: table => new
                {
                    DateCreated = table.Column<DateTime>(nullable: false),
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastUpdateDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    DeletedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    TxnRef = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(nullable: false),
                    Fee = table.Column<decimal>(nullable: true),
                    Status = table.Column<int>(nullable: false),
                    AccountNumber = table.Column<string>(nullable: true),
                    AccountName = table.Column<string>(nullable: true),
                    Bank = table.Column<string>(nullable: true),
                    SentById = table.Column<long>(nullable: false),
                    DateCompleted = table.Column<DateTime>(nullable: false),
                    TransferResponse = table.Column<string>(nullable: true),
                    Narration = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SendMoneyLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SendMoneyLogs_AspNetUsers_SentById",
                        column: x => x.SentById,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

           
            migrationBuilder.CreateIndex(
                name: "IX_SendMoneyLogs_SentById",
                table: "SendMoneyLogs",
                column: "SentById");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SendMoneyLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "f556aea3-93a4-4207-8a73-58bc7e086812", new DateTime(2020, 11, 28, 20, 42, 14, 478, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "751ce457-43d0-4ec4-95df-6baecb5bdb48", new DateTime(2020, 11, 28, 20, 42, 14, 497, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c7c8fd90-4581-4be0-bd06-8d56984d4e74", new DateTime(2020, 11, 28, 20, 42, 14, 497, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "fe322ea0-28dc-4384-9792-b4b89c2155b3", new DateTime(2020, 11, 28, 20, 42, 14, 497, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "13d3bf39-d71f-4fa7-a051-4ba8ef3b5351", new DateTime(2020, 11, 28, 20, 42, 14, 497, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "f43b361f-34db-4340-b749-c84c0fe64df1", new DateTime(2020, 11, 28, 20, 42, 14, 497, DateTimeKind.Local), "AQAAAAEAACcQAAAAEHF/9w2ZTTRrpChuSB2zck0b3BCmXjSZR85x/B9OOj5HnzGqdG8FCKnLQgLSimwzMA==", null, "723a402a-ea86-4a97-b4d6-b28969aa86b2" });
        }
    }
}
