﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class servicesCommissionIII : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "CustomerCommission",
                table: "Services",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "CustomerCommissionCap",
                table: "Services",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DefaultAgentCommission",
                table: "Services",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DefaultAgentCommissionCap",
                table: "Services",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DefaultDealerCommission",
                table: "Services",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DefaultDealerCommissionCap",
                table: "Services",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DefaultSuperDealerCommission",
                table: "Services",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "DefaultSuperDealerCommissionCap",
                table: "Services",
                nullable: false,
                defaultValue: 0m);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CustomerCommission",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "CustomerCommissionCap",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "DefaultAgentCommission",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "DefaultAgentCommissionCap",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "DefaultDealerCommission",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "DefaultDealerCommissionCap",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "DefaultSuperDealerCommission",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "DefaultSuperDealerCommissionCap",
                table: "Services");

        }
    }
}
