﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class VendingLogs : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "VendingLogs",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    DeletedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    LastUpdateDate = table.Column<DateTime>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    CustomerNumber = table.Column<string>(nullable: true),
                    ServiceId = table.Column<long>(nullable: false),
                    Amount = table.Column<decimal>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    UserId = table.Column<long>(nullable: false),
                    ProcessorId = table.Column<long>(nullable: false),
                    Commission = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VendingLogs", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VendingLogs_Processors_ProcessorId",
                        column: x => x.ProcessorId,
                        principalTable: "Processors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VendingLogs_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VendingLogs_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.NoAction);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "95e88d86-3747-48fb-94aa-b532ca13d99f", new DateTime(2020, 6, 10, 17, 26, 8, 882, DateTimeKind.Local).AddTicks(7790) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "13d2a0ec-f1b4-4105-b4cc-245b33b11fc3", new DateTime(2020, 6, 10, 17, 26, 8, 892, DateTimeKind.Local).AddTicks(8870) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "81199c25-e59b-4230-acf5-f19898a47614", new DateTime(2020, 6, 10, 17, 26, 8, 892, DateTimeKind.Local).AddTicks(9090) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "642b9f56-62c9-447b-8920-6f08f28b55f8", new DateTime(2020, 6, 10, 17, 26, 8, 892, DateTimeKind.Local).AddTicks(9190) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ae53a2f6-41a1-4bbc-a5ce-9ed443e44e98", new DateTime(2020, 6, 10, 17, 26, 8, 892, DateTimeKind.Local).AddTicks(9630) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "7b3129bb-efea-4643-b5c4-4e6d63478391", new DateTime(2020, 6, 10, 17, 26, 8, 893, DateTimeKind.Local).AddTicks(1550), "AQAAAAEAACcQAAAAEJhH8OU983R2vWAGwI2WB/3YMriA3sPLYLatqDQ1CduLlaTVZBCSupfw0ZgTfkx2hg==", "7fcb2fd6-64b8-4c6d-8cd6-28f0c92ece1a" });

            migrationBuilder.CreateIndex(
                name: "IX_VendingLogs_ProcessorId",
                table: "VendingLogs",
                column: "ProcessorId");

            migrationBuilder.CreateIndex(
                name: "IX_VendingLogs_ServiceId",
                table: "VendingLogs",
                column: "ServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_VendingLogs_UserId",
                table: "VendingLogs",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VendingLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6669d6ed-576f-4c98-a960-b30ee2fbe491", new DateTime(2020, 6, 3, 7, 32, 34, 768, DateTimeKind.Local).AddTicks(7170) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "2d232750-eb1b-48df-b91d-c17011d26e70", new DateTime(2020, 6, 3, 7, 32, 34, 776, DateTimeKind.Local).AddTicks(2990) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "201a79ba-c4f5-4fb7-8540-d1a0800f9cc5", new DateTime(2020, 6, 3, 7, 32, 34, 776, DateTimeKind.Local).AddTicks(3230) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "411daa7b-60cc-40ca-8be8-b807f007f59a", new DateTime(2020, 6, 3, 7, 32, 34, 776, DateTimeKind.Local).AddTicks(3340) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "001093fa-54af-4099-9407-46c8e4684b64", new DateTime(2020, 6, 3, 7, 32, 34, 776, DateTimeKind.Local).AddTicks(3450) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "0e16353a-57a9-4286-8cfd-c40ad235d389", new DateTime(2020, 6, 3, 7, 32, 34, 776, DateTimeKind.Local).AddTicks(4440), "AQAAAAEAACcQAAAAEPk++GGSzEgGotdZU2fecgCjnw5hV1yz21EiL29TFj7nTj7yk+u2nyKWlYtOV7JB5w==", "8f06c7ab-bfe4-4348-b0a2-78c718fa06f7" });
        }
    }
}
