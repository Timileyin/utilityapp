﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class addDealerCodeToBankTransfer : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DealerCode",
                table: "BankTransfers",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "BankTransfers",
                nullable: false,
                defaultValue: 0);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DealerCode",
                table: "BankTransfers");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "BankTransfers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "3613e0b9-8b4d-4919-825c-2d2a7bb3bb7e", new DateTime(2021, 3, 14, 10, 43, 34, 59, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "3934a061-0e4a-4b36-a3bf-c67bd88c4dcc", new DateTime(2021, 3, 14, 10, 43, 34, 67, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "bddf3294-c62f-4fac-8781-bdfd735d55e5", new DateTime(2021, 3, 14, 10, 43, 34, 67, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b0679606-b716-4f62-a807-de7da943cdbf", new DateTime(2021, 3, 14, 10, 43, 34, 67, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c876063c-2e77-44cd-893c-6aef08157ab6", new DateTime(2021, 3, 14, 10, 43, 34, 67, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "ed6d3955-d84b-4e48-8075-46770998f75e", new DateTime(2021, 3, 14, 10, 43, 34, 67, DateTimeKind.Local), "AQAAAAEAACcQAAAAEN3v2232/DUw8EAV0iYjpsjf7B8em2NBHO3njpoiuXP5dhLXid9Tkp75wCmD/+HJZw==", null, "4cd3a2bf-1402-466c-a079-2b40b6fa65a4" });
        }
    }
}
