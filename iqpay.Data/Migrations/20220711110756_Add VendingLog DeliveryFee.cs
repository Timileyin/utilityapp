﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class AddVendingLogDeliveryFee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "FlatDeliveryFee",
                table: "VendingLogs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "FlatDeliveryFeeBeyondCap",
                table: "VendingLogs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "FlatDeliveryFeeCap",
                table: "VendingLogs",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FlatDeliveryFee",
                table: "VendingLogs");

            migrationBuilder.DropColumn(
                name: "FlatDeliveryFeeBeyondCap",
                table: "VendingLogs");

            migrationBuilder.DropColumn(
                name: "FlatDeliveryFeeCap",
                table: "VendingLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a2709c00-bac0-4c32-ba1e-a0d2eb201284", new DateTime(2022, 7, 3, 14, 33, 17, 541, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "d2675479-7eab-418f-8bc1-841d92c881ac", new DateTime(2022, 7, 3, 14, 33, 17, 544, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "86217894-a3c9-4383-a719-412a2336ae10", new DateTime(2022, 7, 3, 14, 33, 17, 544, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ca085f8d-5a95-4aac-a34c-8bd5f544c8d9", new DateTime(2022, 7, 3, 14, 33, 17, 544, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "aec6f67b-4b11-4998-8a94-34ac96da7d15", new DateTime(2022, 7, 3, 14, 33, 17, 544, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "bdfe5abd-f567-4283-8e8e-34181d06cd23", new DateTime(2022, 7, 3, 14, 33, 17, 544, DateTimeKind.Local), "AQAAAAEAACcQAAAAEBTJRfEsJ0Mje9JAj6Yn1X2DQFf3MMIXYLa9eVA7E1J3cMzonpG51XwVHGroVgf2Nw==", null, "55a6831c-bdce-47bf-ad04-05a32e6a4a46" });
        }
    }
}
