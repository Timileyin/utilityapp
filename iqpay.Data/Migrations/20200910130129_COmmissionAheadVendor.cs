﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class COmmissionAheadVendor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsCommissionAhead",
                table: "Vendors",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "02a8ec3b-5eb3-4d14-9f8b-1bd446dbc092", new DateTime(2020, 9, 10, 14, 1, 26, 825, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6aeb3cc4-7ad0-4580-b668-2c72d81473d9", new DateTime(2020, 9, 10, 14, 1, 26, 851, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8fe8af23-0d49-4844-8a1a-f85b919b1054", new DateTime(2020, 9, 10, 14, 1, 26, 852, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "203443dc-8621-4bd6-b310-a75031bc97f5", new DateTime(2020, 9, 10, 14, 1, 26, 852, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "f4ddb199-00d6-4dfc-8377-6be3405d97fb", new DateTime(2020, 9, 10, 14, 1, 26, 852, DateTimeKind.Local) });

       }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsCommissionAhead",
                table: "Vendors");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "65e23ae1-907f-4b02-9c4a-0ff039153b47", new DateTime(2020, 8, 25, 18, 42, 50, 844, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "5bd96aa0-7e4e-4029-8cb9-f00472c0aa0d", new DateTime(2020, 8, 25, 18, 42, 50, 857, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "92d851fd-7263-4381-957b-f9f59ae530d2", new DateTime(2020, 8, 25, 18, 42, 50, 857, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "0a27616a-a681-4028-844a-1f55b8a9df84", new DateTime(2020, 8, 25, 18, 42, 50, 857, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "61deab4a-03f0-4b85-9f0d-bc8c8d4b277d", new DateTime(2020, 8, 25, 18, 42, 50, 857, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "6ecaa5e9-a519-4bb3-9672-2027cf5aa48a", new DateTime(2020, 8, 25, 18, 42, 50, 857, DateTimeKind.Local), "AQAAAAEAACcQAAAAEGlNpM6KVC2S0F05i5Tio1ixR77SkTtGSbMPfUa+YCcKg30z5ADLQArutd7O0PMkdw==", null, "aff5c8b3-f326-4cd3-8691-80cec6e9e00b" });
        }
    }
}
