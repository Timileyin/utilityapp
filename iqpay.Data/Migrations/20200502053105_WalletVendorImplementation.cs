﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class WalletVendorImplementation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "VendorId",
                table: "Wallets",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "82af9aca-42c9-4ec1-969a-30b118d33d55", new DateTime(2020, 5, 2, 6, 31, 4, 367, DateTimeKind.Local).AddTicks(2710) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "4c13d5cf-3936-45ba-b1ee-b5c7e6b9ab29", new DateTime(2020, 5, 2, 6, 31, 4, 372, DateTimeKind.Local).AddTicks(5640) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "491f4599-9cd7-4ffd-a347-c0dfea8c64cf", new DateTime(2020, 5, 2, 6, 31, 4, 372, DateTimeKind.Local).AddTicks(5880) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "2d102600-abb6-43ba-8164-1f51aeccb5a3", new DateTime(2020, 5, 2, 6, 31, 4, 372, DateTimeKind.Local).AddTicks(5980) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6d9fceb1-7bdd-4c81-ab00-c3f07b2cb717", new DateTime(2020, 5, 2, 6, 31, 4, 372, DateTimeKind.Local).AddTicks(6080) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "8e47cbaf-927b-475a-90a5-6e0768b374d9", new DateTime(2020, 5, 2, 6, 31, 4, 372, DateTimeKind.Local).AddTicks(7290), "AQAAAAEAACcQAAAAECouuVJqmkZnMAamjWuMqbOuNj//TMF8SuDdZQ4k/AvOH3OGCgv7l5doTND2HQGZow==", "b1b86e4a-3d36-4882-9a73-76ccded3ced2" });

            migrationBuilder.CreateIndex(
                name: "IX_Wallets_VendorId",
                table: "Wallets",
                column: "VendorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Wallets_Vendors_VendorId",
                table: "Wallets",
                column: "VendorId",
                principalTable: "Vendors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Wallets_Vendors_VendorId",
                table: "Wallets");

            migrationBuilder.DropIndex(
                name: "IX_Wallets_VendorId",
                table: "Wallets");

            migrationBuilder.DropColumn(
                name: "VendorId",
                table: "Wallets");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "cac7a5fe-11bf-49ef-9959-099f919ddafe", new DateTime(2020, 4, 11, 17, 15, 1, 88, DateTimeKind.Local).AddTicks(8060) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "bae27ab5-cb67-411e-b082-faa6e9dbb522", new DateTime(2020, 4, 11, 17, 15, 1, 110, DateTimeKind.Local).AddTicks(5720) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a386d99f-146f-4985-b032-ea9d18751d8a", new DateTime(2020, 4, 11, 17, 15, 1, 110, DateTimeKind.Local).AddTicks(6370) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "455f856e-7218-4f8b-911a-327a8276ed13", new DateTime(2020, 4, 11, 17, 15, 1, 110, DateTimeKind.Local).AddTicks(6540) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "cdb341a3-ab81-4808-8ca5-47886265751a", new DateTime(2020, 4, 11, 17, 15, 1, 110, DateTimeKind.Local).AddTicks(6690) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "9aa62623-03bd-4dc9-ac8c-3e7d9c05d566", new DateTime(2020, 4, 11, 17, 15, 1, 110, DateTimeKind.Local).AddTicks(8150), "AQAAAAEAACcQAAAAEERghhpEzpXyn9N21D5k0N5yej3Lslab38SffqmO/8ek78WCo/jcgYkNMRKCaS3dng==", "4cf2e492-9c00-4667-ad90-d15a62900072" });
        }
    }
}
