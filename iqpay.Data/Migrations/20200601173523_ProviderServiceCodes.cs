﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class ProviderServiceCodes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Processors",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    DeletedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    LastUpdateDate = table.Column<DateTime>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Code = table.Column<string>(nullable: true),
                    Balance = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Processors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ProcessorServiceCode",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    CreatedBy = table.Column<string>(nullable: true),
                    DeletedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    LastUpdateDate = table.Column<DateTime>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    ServiceId = table.Column<long>(nullable: false),
                    ProcessorId = table.Column<long>(nullable: false),
                    Code = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProcessorServiceCode", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProcessorServiceCode_Processors_ProcessorId",
                        column: x => x.ProcessorId,
                        principalTable: "Processors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProcessorServiceCode_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "65817495-3c39-41ba-8a63-5ae8d6dbc11c", new DateTime(2020, 6, 1, 18, 35, 22, 563, DateTimeKind.Local).AddTicks(4750) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "5883a620-43fc-480a-99ed-6837dbf7cfac", new DateTime(2020, 6, 1, 18, 35, 22, 570, DateTimeKind.Local).AddTicks(8460) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ad60b011-9348-4b48-a20d-e5bbfff30f4e", new DateTime(2020, 6, 1, 18, 35, 22, 570, DateTimeKind.Local).AddTicks(8670) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "f0fb6216-5c37-4f10-af9a-c235c85cd80b", new DateTime(2020, 6, 1, 18, 35, 22, 570, DateTimeKind.Local).AddTicks(8800) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b232303b-410e-4d62-bd20-513d5d8bbfe4", new DateTime(2020, 6, 1, 18, 35, 22, 570, DateTimeKind.Local).AddTicks(8900) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "e116b6e7-113b-48e8-8411-69bb1bf22bfb", new DateTime(2020, 6, 1, 18, 35, 22, 571, DateTimeKind.Local), "AQAAAAEAACcQAAAAENlul778ONUjy1vSuUvjeoHRTleit2w4cwhr/gpI0lwveb3De7duriJRYDuiRpkEtw==", "b381abcd-bb39-49d8-a701-a0568708595f" });

            migrationBuilder.CreateIndex(
                name: "IX_ProcessorServiceCode_ProcessorId",
                table: "ProcessorServiceCode",
                column: "ProcessorId");

            migrationBuilder.CreateIndex(
                name: "IX_ProcessorServiceCode_ServiceId",
                table: "ProcessorServiceCode",
                column: "ServiceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProcessorServiceCode");

            migrationBuilder.DropTable(
                name: "Processors");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a6237b60-ad38-4a48-898c-b5bf15886d16", new DateTime(2020, 5, 3, 12, 26, 47, 238, DateTimeKind.Local).AddTicks(2840) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "081dbdad-b4ae-460e-ba3d-5bcf8f24ff9d", new DateTime(2020, 5, 3, 12, 26, 47, 247, DateTimeKind.Local).AddTicks(6080) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "5352f304-6e90-43c1-8fa7-05cf75107f6d", new DateTime(2020, 5, 3, 12, 26, 47, 247, DateTimeKind.Local).AddTicks(6390) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "1320815f-8ae1-4a45-99dc-44330ad1116d", new DateTime(2020, 5, 3, 12, 26, 47, 247, DateTimeKind.Local).AddTicks(6550) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "4707dfd4-9c2a-413f-b648-3c23a8424fc1", new DateTime(2020, 5, 3, 12, 26, 47, 247, DateTimeKind.Local).AddTicks(6690) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "fe20a893-1498-4c58-acb8-492e0c2aae56", new DateTime(2020, 5, 3, 12, 26, 47, 247, DateTimeKind.Local).AddTicks(7960), "AQAAAAEAACcQAAAAEGBbyL3fAoS0dwZYIwCOANuSAhcK3Cd3183qrytz23xmf4IbmW08YTJg4O99wdWpww==", "dec3fd1d-0a18-45b9-afa4-37393b5aa190" });
        }
    }
}
