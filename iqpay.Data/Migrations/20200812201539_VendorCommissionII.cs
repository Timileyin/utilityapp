﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class VendorCommissionII : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Commission",
                table: "Vendors",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "212a13ae-0450-4e2a-880f-581d68031a26", new DateTime(2020, 8, 12, 21, 15, 38, 597, DateTimeKind.Local).AddTicks(7030) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "bdda4ea5-9aed-4037-8bd2-d90b2e3ece19", new DateTime(2020, 8, 12, 21, 15, 38, 610, DateTimeKind.Local).AddTicks(1340) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e535170f-3b40-4f34-a855-58959c29921b", new DateTime(2020, 8, 12, 21, 15, 38, 610, DateTimeKind.Local).AddTicks(1590) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e472584d-5f98-4501-add8-1cc250dcb63c", new DateTime(2020, 8, 12, 21, 15, 38, 610, DateTimeKind.Local).AddTicks(1700) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "5937bee4-3e50-47ba-bdd7-21d7e84dc8bf", new DateTime(2020, 8, 12, 21, 15, 38, 610, DateTimeKind.Local).AddTicks(1810) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "cf33890d-004f-45b8-89f6-6bf452bc282d", new DateTime(2020, 8, 12, 21, 15, 38, 610, DateTimeKind.Local).AddTicks(2830), "AQAAAAEAACcQAAAAEF6JSzZVk3iN9WGGdzopB7CnuYn6uqym/UQJbAfVujePiC/FStobMuQRuveLVMDk4g==", "355e4f86-0869-4660-8511-a36f440f6feb" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Commission",
                table: "Vendors");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8993ec52-34b1-40aa-be02-6f949a9ffb76", new DateTime(2020, 8, 12, 21, 10, 6, 543, DateTimeKind.Local).AddTicks(3840) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8dae8f1d-0622-4e81-a9a2-54018ef31aed", new DateTime(2020, 8, 12, 21, 10, 6, 548, DateTimeKind.Local).AddTicks(2440) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c37f88b1-045d-4139-b044-feb2176ca020", new DateTime(2020, 8, 12, 21, 10, 6, 548, DateTimeKind.Local).AddTicks(2680) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a9c61ad3-4642-4886-a6a4-f7d7c5371987", new DateTime(2020, 8, 12, 21, 10, 6, 548, DateTimeKind.Local).AddTicks(2790) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "01e98af6-2d0d-4df5-92d3-28efad7aeb0f", new DateTime(2020, 8, 12, 21, 10, 6, 548, DateTimeKind.Local).AddTicks(2890) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "95af21da-3042-427d-a2b0-91841865c8a9", new DateTime(2020, 8, 12, 21, 10, 6, 548, DateTimeKind.Local).AddTicks(3800), "AQAAAAEAACcQAAAAEAEZ4iBDepAi1Kl/wi4jovbzVc8rbKJLyAojWOGb0VcIN7nrZ/lO3dARqpQpvTLInQ==", "019a7855-3356-4e2c-b9be-2025efade97a" });
        }
    }
}
