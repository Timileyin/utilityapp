﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class WalletBalanceTransfermoney : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "WalletBalance",
                table: "SendMoneyLogs",
                nullable: false,
                defaultValue: 0m);

            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WalletBalance",
                table: "SendMoneyLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e88d6f05-022b-47a3-8925-fdfb163b2eb6", new DateTime(2020, 12, 28, 11, 43, 3, 925, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "1b1a861c-7d42-4623-9959-c61819420715", new DateTime(2020, 12, 28, 11, 43, 3, 932, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "18b3203b-03dd-4575-9d72-632f8949df63", new DateTime(2020, 12, 28, 11, 43, 3, 932, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "19741625-1b50-4986-9e72-8328e261f949", new DateTime(2020, 12, 28, 11, 43, 3, 932, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "f3c0d21c-140d-4ad7-9c76-f502c99d9874", new DateTime(2020, 12, 28, 11, 43, 3, 932, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "053570af-682c-4aef-8f67-df7d9cf6792a", new DateTime(2020, 12, 28, 11, 43, 3, 932, DateTimeKind.Local), "AQAAAAEAACcQAAAAEFGP4oDJk5OKStVsS0op1VXCY2LVFzqyyFHAwmfC3Ky3m140FS9GdgIEpEQeq+gqNg==", null, "13dee209-6c4e-48a8-b2f0-08c42918b171" });
        }
    }
}
