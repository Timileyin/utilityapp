﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class walletFundingRequestWalletBalance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "DebittedWalletBalance",
                table: "FundWallet",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "CredittedWalletBalance",
                table: "FundWallet",
                nullable: true,
                oldClrType: typeof(decimal));

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "DebittedWalletBalance",
                table: "FundWallet",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CredittedWalletBalance",
                table: "FundWallet",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "0367faf5-73af-422b-82ba-afd369c1b5be", new DateTime(2021, 1, 9, 5, 1, 12, 880, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "6f3308ab-fd1d-4f96-a110-196d4fcc87cd", new DateTime(2021, 1, 9, 5, 1, 12, 927, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ad3cc4d3-17cb-4790-8455-128093c661a2", new DateTime(2021, 1, 9, 5, 1, 12, 927, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "4a859f56-107d-45c8-baaf-f663f046a81a", new DateTime(2021, 1, 9, 5, 1, 12, 927, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "962a1a23-45e7-4b26-aa4f-8e0ee7a8226d", new DateTime(2021, 1, 9, 5, 1, 12, 927, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "f5581ad3-482f-4ffe-9a3c-195f080f100e", new DateTime(2021, 1, 9, 5, 1, 12, 928, DateTimeKind.Local), "AQAAAAEAACcQAAAAEAJPYHO8XIfXGc4fYOHNdzW7kxOcuy/LWl+09PI5sZdDmtLZ3afDz8xnLBG+cmWgkg==", null, "49589fb8-db25-4313-802e-33f3ba5d9b8b" });
        }
    }
}
