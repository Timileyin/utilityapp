﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class NumberOfPins : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "NumberOfPins",
                table: "VendingLogs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NumberOfPins",
                table: "VendingLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "9c3dfa5b-5a42-48d0-a4a2-62147ea5b31c", new DateTime(2020, 11, 13, 17, 23, 54, 113, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ee3c5460-cc63-4262-94ba-efc905ee5aa7", new DateTime(2020, 11, 13, 17, 23, 54, 121, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "82c5ae05-f119-494c-a514-a10533fb6991", new DateTime(2020, 11, 13, 17, 23, 54, 121, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "bd99fc21-e856-4cc5-9a06-50bb337768bf", new DateTime(2020, 11, 13, 17, 23, 54, 121, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "9534b27d-a197-4636-94f1-ab92e2cfe217", new DateTime(2020, 11, 13, 17, 23, 54, 121, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "86c848cc-6d5d-4204-b09d-090e3903eef5", new DateTime(2020, 11, 13, 17, 23, 54, 121, DateTimeKind.Local), "AQAAAAEAACcQAAAAEJ07iy0Gncu62kNZ08fyWHqgX3Dqd7LDjr5Wag6s29sLtIk5pu8FxGhsd81y0039kQ==", null, "3828fbe0-e6d2-426c-b3c1-9a408a7f46b3" });
        }
    }
}
