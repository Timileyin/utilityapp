﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class requiredphonenumber : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "PhoneNumber",
                table: "AspNetUsers",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            
            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_PhoneNumber",
                table: "AspNetUsers",
                column: "PhoneNumber",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_PhoneNumber",
                table: "AspNetUsers");

            migrationBuilder.AlterColumn<string>(
                name: "PhoneNumber",
                table: "AspNetUsers",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c2ce2439-4e41-4885-a90f-ffda7604ab8f", new DateTime(2020, 12, 6, 14, 51, 27, 550, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "64f8232b-a12f-4848-a9cb-6c7d6b48fb95", new DateTime(2020, 12, 6, 14, 51, 27, 564, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "483540d2-e56e-4fe1-b768-a70d11dec6b9", new DateTime(2020, 12, 6, 14, 51, 27, 564, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "aeb2c978-268b-4439-9fb0-960a39738870", new DateTime(2020, 12, 6, 14, 51, 27, 564, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c2cd6b5c-4059-46e2-8d90-bc5310232c54", new DateTime(2020, 12, 6, 14, 51, 27, 564, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "93b6871f-2959-4bba-b2d8-99cb85dd8a94", new DateTime(2020, 12, 6, 14, 51, 27, 564, DateTimeKind.Local), "AQAAAAEAACcQAAAAEJJAr66pjEBrwZrsXh20GbTo9R7zYjS3QwQQjWe0BzQL800n8Vv/fDj3CoZp3F9x2Q==", null, "71df9c63-7964-4c8c-8272-68411c4f821c" });
        }
    }
}
