﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class AddAcountAddressLine1andLine2V2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "AccountAddressLine2",
                table: "VendingLogs",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CollectionPoint",
                table: "VendingLogs",
                nullable: true);

            }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AccountAddressLine2",
                table: "VendingLogs");

            migrationBuilder.DropColumn(
                name: "CollectionPoint",
                table: "VendingLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b1554e53-587a-4274-92b6-761e0142469a", new DateTime(2022, 7, 2, 18, 49, 4, 805, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "60d2fe95-ac9d-4df9-bf60-8c2fe9bbcce3", new DateTime(2022, 7, 2, 18, 49, 4, 807, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "15750752-b19d-45ac-8d95-b00720262b46", new DateTime(2022, 7, 2, 18, 49, 4, 807, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "f9b5c2fc-1c55-4303-83ec-7cc4fda47501", new DateTime(2022, 7, 2, 18, 49, 4, 807, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e3b5a2da-ca37-4f27-a664-3c764f952e09", new DateTime(2022, 7, 2, 18, 49, 4, 807, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "231eaea3-5718-4e0a-b1ac-081877ab0027", new DateTime(2022, 7, 2, 18, 49, 4, 807, DateTimeKind.Local), "AQAAAAEAACcQAAAAELr09rPJ1s1dFtD4Ta+qeY0xcVirosXiF7Yfdj9+UTpXQR2AxrLT4h1lm5IFO6bCeg==", null, "9476e238-5625-4335-8dcb-ca7ca83d8065" });
        }
    }
}
