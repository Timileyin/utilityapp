﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class BillLogTechFee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "TechFee",
                table: "BillLogs",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a6237b60-ad38-4a48-898c-b5bf15886d16", new DateTime(2020, 5, 3, 12, 26, 47, 238, DateTimeKind.Local).AddTicks(2840) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "081dbdad-b4ae-460e-ba3d-5bcf8f24ff9d", new DateTime(2020, 5, 3, 12, 26, 47, 247, DateTimeKind.Local).AddTicks(6080) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "5352f304-6e90-43c1-8fa7-05cf75107f6d", new DateTime(2020, 5, 3, 12, 26, 47, 247, DateTimeKind.Local).AddTicks(6390) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "1320815f-8ae1-4a45-99dc-44330ad1116d", new DateTime(2020, 5, 3, 12, 26, 47, 247, DateTimeKind.Local).AddTicks(6550) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "4707dfd4-9c2a-413f-b648-3c23a8424fc1", new DateTime(2020, 5, 3, 12, 26, 47, 247, DateTimeKind.Local).AddTicks(6690) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "fe20a893-1498-4c58-acb8-492e0c2aae56", new DateTime(2020, 5, 3, 12, 26, 47, 247, DateTimeKind.Local).AddTicks(7960), "AQAAAAEAACcQAAAAEGBbyL3fAoS0dwZYIwCOANuSAhcK3Cd3183qrytz23xmf4IbmW08YTJg4O99wdWpww==", "dec3fd1d-0a18-45b9-afa4-37393b5aa190" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TechFee",
                table: "BillLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "00de116b-ad22-41c1-89cf-c61e57f8af11", new DateTime(2020, 5, 3, 2, 22, 20, 860, DateTimeKind.Local).AddTicks(7200) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c258467f-24d1-491d-84bd-330cf73fb49b", new DateTime(2020, 5, 3, 2, 22, 20, 868, DateTimeKind.Local).AddTicks(5420) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "545ee04b-1741-44f2-8e5d-a3ef3afe6426", new DateTime(2020, 5, 3, 2, 22, 20, 868, DateTimeKind.Local).AddTicks(5670) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ac9c6698-4534-4919-9851-79c8cef1d45e", new DateTime(2020, 5, 3, 2, 22, 20, 868, DateTimeKind.Local).AddTicks(5780) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "9f3b8a0b-a596-44de-a60d-aa754346b526", new DateTime(2020, 5, 3, 2, 22, 20, 868, DateTimeKind.Local).AddTicks(5900) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "SecurityStamp" },
                values: new object[] { "964ad817-cbaa-4d92-971d-425116ef6a8b", new DateTime(2020, 5, 3, 2, 22, 20, 868, DateTimeKind.Local).AddTicks(7090), "AQAAAAEAACcQAAAAEJbXrKEIbgKoIfULPBJ7i2/cHRYDWBZlspilY0zgrAoDBDmjW19vLD40idRHEzAHSg==", "b2515a06-574f-454c-859b-bcca8d862238" });
        }
    }
}
