﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class walletBalance : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "WalletBalance",
                table: "VendingLogs",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "WalletBalance",
                table: "VendingLogs");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "3267c08c-da79-408d-b777-eefd09e38122", new DateTime(2020, 12, 6, 21, 9, 14, 466, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "312d2db0-128f-4da1-97e0-bd76e6a941cf", new DateTime(2020, 12, 6, 21, 9, 14, 475, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "650ed8bb-d12a-43ac-adee-f4bd7f3954d7", new DateTime(2020, 12, 6, 21, 9, 14, 475, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "005c167c-bfec-491c-908c-36995896a620", new DateTime(2020, 12, 6, 21, 9, 14, 475, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c7708fe1-9c4d-4c35-8e23-484be6d61991", new DateTime(2020, 12, 6, 21, 9, 14, 475, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "64a756e1-a338-4b44-8ee8-a1c6403fb477", new DateTime(2020, 12, 6, 21, 9, 14, 475, DateTimeKind.Local), "AQAAAAEAACcQAAAAEFJW3Fix7T6Wzmqze9GVeTEnDgeel0zLNz2Za43au5i4dBio5jKVDsZBzkBZBJr+7A==", null, "b6ad0489-7478-4577-b65d-f5e00207d707" });
        }
    }
}
