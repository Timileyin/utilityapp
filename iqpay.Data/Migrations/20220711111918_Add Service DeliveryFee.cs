﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class AddServiceDeliveryFee : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FlatDeliveryFee",
                table: "VendingLogs");

            migrationBuilder.DropColumn(
                name: "FlatDeliveryFeeBeyondCap",
                table: "VendingLogs");

            migrationBuilder.DropColumn(
                name: "FlatDeliveryFeeCap",
                table: "VendingLogs");

            migrationBuilder.AddColumn<decimal>(
                name: "FlatDeliveryFee",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "FlatDeliveryFeeBeyondCap",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "FlatDeliveryFeeCap",
                table: "Services",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FlatDeliveryFee",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "FlatDeliveryFeeBeyondCap",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "FlatDeliveryFeeCap",
                table: "Services");

            migrationBuilder.AddColumn<decimal>(
                name: "FlatDeliveryFee",
                table: "VendingLogs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "FlatDeliveryFeeBeyondCap",
                table: "VendingLogs",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "FlatDeliveryFeeCap",
                table: "VendingLogs",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "ce1eed2d-9ad6-4dd1-9544-75504d9f225f", new DateTime(2022, 7, 11, 12, 7, 55, 418, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "63ac30b1-16bb-4b89-b749-12ffb6ecf0b6", new DateTime(2022, 7, 11, 12, 7, 55, 420, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "349d0713-f364-4a0b-9b58-9db6fd5842a0", new DateTime(2022, 7, 11, 12, 7, 55, 420, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b5aac7e4-36b7-455d-8910-3e6babb9832d", new DateTime(2022, 7, 11, 12, 7, 55, 420, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "d5f8da2c-2740-48be-80fa-e9b306de2fc8", new DateTime(2022, 7, 11, 12, 7, 55, 420, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "8e69bf78-4fab-4a43-8acb-4b1238e78473", new DateTime(2022, 7, 11, 12, 7, 55, 420, DateTimeKind.Local), "AQAAAAEAACcQAAAAEEy8TNoTaLISATVM+ZoLiL/t+DHtSi+kSKNp6zHFMKtYHZQ/zaQNeUHYgq9VCGHzOQ==", null, "3625457e-63e5-4ab4-9e37-14935513d08b" });
        }
    }
}
