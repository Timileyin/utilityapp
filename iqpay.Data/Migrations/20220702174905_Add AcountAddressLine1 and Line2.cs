﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class AddAcountAddressLine1andLine2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
                   }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "c78a4c28-6d5d-4972-b747-8f2f614a3627", new DateTime(2021, 12, 13, 12, 50, 46, 11, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8b8f59c1-604e-4594-b600-212b364a8ebd", new DateTime(2021, 12, 13, 12, 50, 46, 12, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "bfadc1e8-3922-4999-9586-36135738d302", new DateTime(2021, 12, 13, 12, 50, 46, 12, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "bc714997-103f-4cef-8919-feaa1e399aad", new DateTime(2021, 12, 13, 12, 50, 46, 12, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "16bc492e-962e-4741-b109-c2f1861253e9", new DateTime(2021, 12, 13, 12, 50, 46, 12, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "9b0b75f2-a560-4526-a64f-7121ea2ad484", new DateTime(2021, 12, 13, 12, 50, 46, 12, DateTimeKind.Local), "AQAAAAEAACcQAAAAEC6KLAz6+IvamUHD5UJZpHlYLQAugP0NXx/lXmF05O676TsDKjMzvK+3kxTNB7U1Pw==", null, "6ac41d55-3bf4-4ea7-8f98-ed15e9a84d32" });
        }
    }
}
