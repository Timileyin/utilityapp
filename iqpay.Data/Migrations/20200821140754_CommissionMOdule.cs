﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class CommissionMOdule : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Wallets_Vendors_VendorId",
                table: "Wallets");

            migrationBuilder.RenameColumn(
                name: "VendorId",
                table: "Wallets",
                newName: "ServiceId");

            migrationBuilder.RenameIndex(
                name: "IX_Wallets_VendorId",
                table: "Wallets",
                newName: "IX_Wallets_ServiceId");

            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultSuperDealerCommissionCap",
                table: "Services",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultDealerCommissionCap",
                table: "Services",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultAgentCommissionCap",
                table: "Services",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "CustomerCommissionCap",
                table: "Services",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "CommissionCap",
                table: "Services",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AlterColumn<decimal>(
                name: "CommissionCap",
                table: "DealerCommission",
                nullable: true,
                oldClrType: typeof(decimal));

            
            migrationBuilder.AddForeignKey(
                name: "FK_Wallets_Services_ServiceId",
                table: "Wallets",
                column: "ServiceId",
                principalTable: "Services",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Wallets_Services_ServiceId",
                table: "Wallets");

            migrationBuilder.RenameColumn(
                name: "ServiceId",
                table: "Wallets",
                newName: "VendorId");

            migrationBuilder.RenameIndex(
                name: "IX_Wallets_ServiceId",
                table: "Wallets",
                newName: "IX_Wallets_VendorId");

            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultSuperDealerCommissionCap",
                table: "Services",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultDealerCommissionCap",
                table: "Services",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "DefaultAgentCommissionCap",
                table: "Services",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CustomerCommissionCap",
                table: "Services",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CommissionCap",
                table: "Services",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            migrationBuilder.AlterColumn<decimal>(
                name: "CommissionCap",
                table: "DealerCommission",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);

            
            migrationBuilder.AddForeignKey(
                name: "FK_Wallets_Vendors_VendorId",
                table: "Wallets",
                column: "VendorId",
                principalTable: "Vendors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
