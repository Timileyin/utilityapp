﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class addPatnershipReg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PaternshipRegistration",
                columns: table => new
                {
                    DateCreated = table.Column<DateTime>(nullable: false),
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IsActive = table.Column<bool>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    LastUpdateDate = table.Column<DateTime>(nullable: true),
                    CreatedBy = table.Column<string>(nullable: true),
                    DeletedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    RowVersion = table.Column<byte[]>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Firstname = table.Column<string>(nullable: true),
                    Surname = table.Column<string>(nullable: true),
                    Phonenumber = table.Column<string>(nullable: true),
                    CompanyName = table.Column<string>(nullable: true),
                    BVN = table.Column<string>(nullable: true),
                    NIN = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PaternshipRegistration", x => x.Id);
                });

           
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PaternshipRegistration");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "73853683-b60b-4989-9cff-fd2670d305b6", new DateTime(2021, 3, 27, 14, 25, 48, 274, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "466248ed-f299-4dad-9fd6-9faea457526e", new DateTime(2021, 3, 27, 14, 25, 48, 328, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "1dfdbe8a-2b46-4be6-939e-bc098e419810", new DateTime(2021, 3, 27, 14, 25, 48, 328, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "09368acf-fa74-47e2-9481-d535bcdb6b70", new DateTime(2021, 3, 27, 14, 25, 48, 328, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8e123f24-d275-4485-849b-8e981296f78f", new DateTime(2021, 3, 27, 14, 25, 48, 328, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "0521628f-f693-4ae9-b3d9-6f0802d4f503", new DateTime(2021, 3, 27, 14, 25, 48, 328, DateTimeKind.Local), "AQAAAAEAACcQAAAAEIMS4xL367lV3fdBQFenBmT+lMMue093O1tcgrnOlIRukk8lNVltFdhJEtR6cKUb0Q==", null, "fb1b46c1-01b3-4e49-8c4b-b80ca910ec53" });
        }
    }
}
