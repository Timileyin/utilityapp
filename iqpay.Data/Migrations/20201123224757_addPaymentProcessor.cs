﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class addPaymentProcessor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Processor",
                table: "BankTransfers",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Processor",
                table: "BankTransfers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "da183e2f-368e-49bf-9c78-e2d14e11f439", new DateTime(2020, 11, 21, 21, 45, 51, 658, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "b8364577-5781-4808-b148-c594d6b41dde", new DateTime(2020, 11, 21, 21, 45, 51, 679, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "1749a0a6-cf95-48f4-80fc-db64be8e58c8", new DateTime(2020, 11, 21, 21, 45, 51, 679, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "98811294-0f5d-4845-90f7-1bb091e6b7a7", new DateTime(2020, 11, 21, 21, 45, 51, 679, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e8bfad6e-c240-421d-9976-71ffdd0637b5", new DateTime(2020, 11, 21, 21, 45, 51, 679, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "cd08b127-b619-4697-8eba-b0a6178867eb", new DateTime(2020, 11, 21, 21, 45, 51, 679, DateTimeKind.Local), "AQAAAAEAACcQAAAAEP3r1sSMk64j8U+NqPuaW++P8PwlPHq1uxdpB5/z9l/xv+xH2ucD69LgYUyBG11S8A==", null, "66d80178-9816-425e-94a2-dcf1b24aa34a" });
        }
    }
}
