﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class AddSlotImplementation : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsSharedProduct",
                table: "Services",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastOrderDate",
                table: "Services",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "MaxOrders",
                table: "Services",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsSharedProduct",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "LastOrderDate",
                table: "Services");

            migrationBuilder.DropColumn(
                name: "MaxOrders",
                table: "Services");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8e348cc6-2f73-4259-8abd-e958ad4ba361", new DateTime(2022, 7, 12, 8, 48, 42, 453, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "be13ad83-5f04-4b24-831d-189ba4f12879", new DateTime(2022, 7, 12, 8, 48, 42, 455, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "7fac1a0c-6801-4a4d-a0d3-b08e53917dae", new DateTime(2022, 7, 12, 8, 48, 42, 455, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "5ee33ae2-d887-4002-875d-06f3c5bda6e6", new DateTime(2022, 7, 12, 8, 48, 42, 455, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "fe8b54a4-72c3-432c-822d-94fa85972879", new DateTime(2022, 7, 12, 8, 48, 42, 455, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "270e559d-4882-4006-bebe-b5127a05a5f3", new DateTime(2022, 7, 12, 8, 48, 42, 455, DateTimeKind.Local), "AQAAAAEAACcQAAAAELb3eRLq7yeb6bgIh15Su0UNSkU3bsVr3oVtB7sXCBjd1FzaSomFCwJt4LXerfdDLQ==", null, "0fe5ff4a-c1b1-4334-9cf9-c39d3111e360" });
        }
    }
}
