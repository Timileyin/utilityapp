﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class RoleTypesUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
          
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] {  "DateCreated", "Name", "NormalizedName" },
                values: new object[] { new DateTime(2020, 8, 24, 21, 38, 21, 232, DateTimeKind.Local), "SUPER DEALER", "SUPER DEALER" });

            
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "8efb7d71-a1ca-4095-b413-0cc248a2a163", new DateTime(2020, 8, 24, 9, 15, 38, 136, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "Name", "NormalizedName" },
                values: new object[] { "a197af94-5968-4f5d-8500-4ece67dfebdb", new DateTime(2020, 8, 24, 9, 15, 38, 152, DateTimeKind.Local), "AGENT", "AGENT" });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "878cb023-885e-48b6-bbdc-903fcc75d1db", new DateTime(2020, 8, 24, 9, 15, 38, 152, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "aa956259-7e8b-432d-a5a1-0706b8cb4074", new DateTime(2020, 8, 24, 9, 15, 38, 152, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "a9cba037-92fa-4777-a723-8295d07699f7", new DateTime(2020, 8, 24, 9, 15, 38, 153, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "e53ccfff-376a-4fdf-956b-99f360d5ec55", new DateTime(2020, 8, 24, 9, 15, 38, 153, DateTimeKind.Local), "AQAAAAEAACcQAAAAEI/0LiOnUKXMzzP9ocxZF7QqNY4zu8rvyswuPALiu0uT05+8e+wy3cbOtw9mbYYzrw==", null, "81a1b66f-fa21-4fbc-920d-348bc29daa61" });
        }
    }
}
