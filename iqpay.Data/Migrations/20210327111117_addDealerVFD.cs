﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class addDealerVFD : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "VFDAccountName",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VFDAccountNumber",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "VirtualAccountName",
                table: "AspNetUsers",
                nullable: true);

             }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "VFDAccountName",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "VFDAccountNumber",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "VirtualAccountName",
                table: "AspNetUsers");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "da1a3c60-33f1-43ab-a2e7-4a321a760564", new DateTime(2021, 3, 27, 3, 54, 27, 900, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "89db925e-98e3-4854-b2b9-5f109bf732d9", new DateTime(2021, 3, 27, 3, 54, 27, 925, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "9c4764e3-e171-4ba1-854e-dd7d24037642", new DateTime(2021, 3, 27, 3, 54, 27, 925, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "56a053f4-9094-4b4f-ab97-d34793e96253", new DateTime(2021, 3, 27, 3, 54, 27, 925, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "359487c5-1270-4ade-93de-056ec6802bcd", new DateTime(2021, 3, 27, 3, 54, 27, 925, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "ceefd7ba-c1e5-4585-a649-5a07952e3d6d", new DateTime(2021, 3, 27, 3, 54, 27, 926, DateTimeKind.Local), "AQAAAAEAACcQAAAAEOzDjmAlzlyiN6ciCDaT2FIw8n56Pvx04nX/zG55sIaukAfv219dOU1qlyV3x2NmAQ==", null, "01d7272c-857f-4c15-804d-d1eb2c1d2650" });
        }
    }
}
