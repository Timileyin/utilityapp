﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace iqpay.Data.Migrations
{
    public partial class VendorWallets : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "VendorId",
                table: "Wallets",
                nullable: true);

            
            migrationBuilder.CreateIndex(
                name: "IX_Wallets_VendorId",
                table: "Wallets",
                column: "VendorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Wallets_Vendors_VendorId",
                table: "Wallets",
                column: "VendorId",
                principalTable: "Vendors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Wallets_Vendors_VendorId",
                table: "Wallets");

            migrationBuilder.DropIndex(
                name: "IX_Wallets_VendorId",
                table: "Wallets");

            migrationBuilder.DropColumn(
                name: "VendorId",
                table: "Wallets");

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 2L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "911446ac-1b3b-41da-b9c6-b037f4a12d75", new DateTime(2020, 9, 11, 6, 20, 55, 233, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 3L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "e0cd3d55-1225-491a-9f94-97d2ce1d7280", new DateTime(2020, 9, 11, 6, 20, 55, 246, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 4L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "49ffdd4b-9677-42cd-b930-c1a8327ed7f1", new DateTime(2020, 9, 11, 6, 20, 55, 246, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 5L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "af95dd10-e1a6-45c6-adfe-6c50944730ad", new DateTime(2020, 9, 11, 6, 20, 55, 246, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: 6L,
                columns: new[] { "ConcurrencyStamp", "DateCreated" },
                values: new object[] { "5c21cee1-2d96-44ab-bb7f-c1611fd25d7a", new DateTime(2020, 9, 11, 6, 20, 55, 246, DateTimeKind.Local) });

            migrationBuilder.UpdateData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: 10L,
                columns: new[] { "ConcurrencyStamp", "DateCreated", "PasswordHash", "RowVersion", "SecurityStamp" },
                values: new object[] { "9f5b3d16-3c38-471a-a7cd-9a873b447869", new DateTime(2020, 9, 11, 6, 20, 55, 246, DateTimeKind.Local), "AQAAAAEAACcQAAAAEAP8y+VctOxCC6puUSaU8xso3h84xQ0vyT5z2dCeu5RtkQYV9+kUhROq/9xA/2Swag==", null, "74f0835e-53ec-45de-a220-88539b737853" });
        }
    }
}
