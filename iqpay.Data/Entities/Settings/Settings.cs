using System;
using System.Collections.Generic;
using System.ComponentModel;
using iqpay.Data.Entities.Payment;
using iqpay.Data.UserManagement.Model;

namespace iqpay.Data.Entities.Settings
{
    public class Settings:EntityBase
    {    
        public string HouseHoldPowerBonus { get; set; }
        public string HouseHoldBonusDates{get;set;}     
        public bool EnableHouseHoldBonus{get;set;}           
    }

}
