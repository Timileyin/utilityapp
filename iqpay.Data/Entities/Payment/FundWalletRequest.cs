﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using iqpay.Data.UserManagement.Model;

namespace iqpay.Data.Entities.Payment
{
    public class FundWalletRequest : EntityBase
    {
        public string TransactionRef { get; set; }
        public decimal Amount{ get; set; }
        public decimal AmmountCreditted{get;set;}
        public FundWalletRequestStatus Status { get; set; }
        public bool IsAuto { get; set; }
        public string TransactionDetails { get; set; }
        public string BankName { get; set; }
        public string KoraPayBank { get; set; }
        public string KoraPayAccountNumber { get; set; }
        public DateTime? TransactionDate { get; set; }
        public long WalletId { get; set; }
        [ForeignKey("WalletId")]
        public Wallet Wallet { get; set; }
        public long? CreditedById { get; set; }
        [ForeignKey("CreditedById")]
        public ApplicationUser CreditedBy { get; set; }
        public long? InitiatedById { get; set; }
        [ForeignKey("InitiatedById")]
        public ApplicationUser InitiatedBy { get; set; }
        public string IQBankAccount { get; set; }
        public string DepositorName { get; set; }
         
        public long? BaseWalletId { get; set; }
        public Wallet BaseWallet { get; set; }
        public bool IsIntraWallet { get; set; }
        public decimal? DebittedWalletBalance{get;set;}
        public decimal? CredittedWalletBalance{get;set;}
        


    }


    public enum FundWalletRequestStatus
    {
        [Description("Initiated")]
        Inititated,
        [Description("Creditted")]
        Credited,
        [Description("Failed")]
        Failed
    }
}
