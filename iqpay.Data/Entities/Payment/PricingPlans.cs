﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iqpay.Data.Entities.Payment
{
    public class PricingPlan:EntityBase
    {
        public string PlanName { get; set; }
        public int LowerBound { get; set; }
        public int UpperBound { get; set; }
        public decimal AmountPerAttendee { get; set; }
        public decimal Amount { get; set; }
        public string Description { get; set; }
    }
}
