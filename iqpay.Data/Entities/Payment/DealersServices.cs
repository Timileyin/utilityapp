using iqpay.Engine.Base.Entities;
using iqpay.Data.Entities.Services;
using System.ComponentModel.DataAnnotations.Schema;
using iqpay.Data.UserManagement.Model;

namespace iqpay.Data.Entities.Payment
{
    public class DealersServices:EntityBase
    {
        [ForeignKey("UserId")]
        public ApplicationUser User{get;set;}
        public long UserId{get;set;}
        [ForeignKey("ServiceId")]
        public Services.Services Service{get;set;}
        public long? ServiceId{get;set;}
        public long? VendorId{get;set;}
        [ForeignKey("VendorId")]
        public Vendor Vendor{get;set;}
    }
}