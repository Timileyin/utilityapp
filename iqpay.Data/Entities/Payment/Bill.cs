﻿using EmailEngine.Base.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using iqpay.Data.Entity.Interface;
using iqpay.Data.UserManagement.Model;
using iqpay.Engine.Base.Entities;

namespace iqpay.Data.Entities.Payment
{
    public class BillLog:VatBill<ApplicationUser, long, PaymentLog>, IEntity, IAudit
    {
        public decimal TechFee { get; set; }
    }

    public enum BillStatus
    {
        Initiated = 1,
        Paid,
        PaymentFailed
    }

    public enum BillType
    {
        [Description("Wallet Funding")]
        WalletFunding = 1,
        [Description("Wallet ")]
        PurchaseService
    }
}
