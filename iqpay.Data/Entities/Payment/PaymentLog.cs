﻿using EmailEngine.Base.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using iqpay.Data.Entity.Interface;
using iqpay.Data.UserManagement.Model;
using iqpay.Engine.Base.Entities;

namespace iqpay.Data.Entities.Payment
{
    public class PaymentLog: VatPaymentLog<ApplicationUser, long>, IEntity, IAudit
    {
        public new long BillId { get; set; }
        [ForeignKey("BillId")]
        public BillLog BillLog { get; set; }
    }

    public enum PaymentStatus
    {
        Initiated = 1,
        Paid,
        PaymentFailed
    }

    public enum PaymentMethod
    {
        PaystackCard =1,
        PaystackBank
    }
}
