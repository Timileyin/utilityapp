using System;
using System.ComponentModel.DataAnnotations.Schema;
using iqpay.Data.UserManagement.Model;
using System.Collections.Generic;
using System.Text;

namespace iqpay.Data.Entities.Payment
{
    public class SendMoneyLog:EntityBase
    {
        public string TxnRef{get;set;}    
        public decimal Amount{get;set;}
        public decimal? Fee{get;set;}
        public SendMoneyEnum Status{get;set;}
        public string AccountNumber{get;set;}
        public string AccountName{get;set;}
        public string Bank{get;set;}
        public long SentById{get;set;}
        [ForeignKey("SentById")]
        public ApplicationUser SentBy{get;set;}
        public DateTime DateCompleted{get;set;}
        public string TransferResponse{get;set;}
        public string Narration{get;set;}
        public decimal WalletBalance{get;set;}
    } 

    public enum SendMoneyEnum
    {
        Initiated = 1,
        Completed
    }

    
}