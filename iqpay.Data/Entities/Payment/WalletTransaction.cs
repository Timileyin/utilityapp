﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using iqpay.Data.UserManagement.Model;
namespace iqpay.Data.Entities.Payment
{
    public class WalletTransaction : EntityBase
    {
        public string TransactionRef { get; set; }
        public long WalletId { get; set; }
        [ForeignKey("WalletId")]
        public Wallet Wallet{get;set;}
        public long? FromWalletId { get; set; }
        [ForeignKey("FromWalletId")]
        public Wallet FromWallet{get;set;}
        public TransactionType Type { get; set; }
        public decimal Amount { get; set; }
        public decimal Profit{get;set;}
        public TransactionStatus Status{get;set;}

        public long UserId{get;set;}
        [ForeignKey("UserId")]
        public ApplicationUser User{get;set;}


    }

    public enum TransactionMode
    {
        Debit,
        Credit
    }

    public enum TransactionStatus
    {
        Initiated,
        Completed,
        Failed,
        Retry
    }

    public enum TransactionType
    {
        WalletFunding,
        ServicePurchase
    }
}
