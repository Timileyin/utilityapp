﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using iqpay.Data.Entities.Services;
using iqpay.Data.UserManagement.Model;

namespace iqpay.Data.Entities.Payment
{
    public class VendingLogs : EntityBase
    {
        public string CustomerNumber { get; set; }
        public long ServiceId { get; set; }
        [ForeignKey("ServiceId")]
        public Services.Services Service { get; set; }
        public decimal Amount { get; set; }
        public VendingStatusEnum Status { get; set; }
        public long UserId { get; set; }
        public ApplicationUser User { get; set; }
        public long? ProcessorId { get; set; }
        [ForeignKey("ProcessorId")]
        public Processor Processor { get; set; }
        public decimal AgentCommission { get; set; }
        public decimal DealerCommission{get;set;}
        public decimal SuperDealerCommission{get;set;}
        public string VendingCode { get; set; }
        public string AccountName { get; set; }
        public string AccountAddress{get;set;}
        public string AccountAddressLine2{get;set;}
        public string CollectionPoint{get;set;}
        public string ProcessorsPayLoad{get;set;}
        public decimal IQPAYProfit{get;set;}
        public decimal? ProcessorBalance{get;set;}
        public int? NumberOfPins{get;set;}
        public decimal? WalletBalance{get;set;}
        public DeliveryOption? DeliveryOption{get;set;}
        public decimal? DeliveryFee{get;set;}
        public string State{get;set;}
        public string LGA{get;set;}        
    }

    public enum DeliveryOption
    {
        CollectionPoint = 1,
        DeliveryCourier
    }

    public enum VendingStatusEnum
    {
        [Description("Initiated")]
        Initiated = 1,
        [Description("Completed")]
        Completed,
        [Description("In Progress")]
        InProgress,
        [Description("Processing Failed")]
        ProcessingFailed,
        [Description("Cancelled By User")]
        Cancelled,
        [Description("Transaction Reversed")]
        TransactionReversed,
        [Description("Order Placed")]
        OrderPlaced,
        [Description("Order Processed")]
        OrderProcessed,
        [Description("Order shipped")]
        OrderShipped,
        [Description("Order Received")]
        OrderReceived
    }

}
