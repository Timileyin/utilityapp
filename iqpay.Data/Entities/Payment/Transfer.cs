using System;
using System.Collections.Generic;
using System.Text;

namespace iqpay.Data.Entities.Payment
{
    public class BankTransfer:EntityBase
    {
        public string PaymentRef{get;set;}
        public string TransactionRef{get;set;}
        public decimal AmountPaid{get;set;}
        public string PaymentStatus{get;set;}
        public bool IsProcessed{get;set;}
        public string AccountNumber{get;set;}
        public string AccountName{get;set;}
        public string Bank{get;set;}
        public DateTime DatePaid{get;set;}
        public string MonnifyResponse{get;set;}
        public DateTime? DateProcessed{get;set;}
        public string CustomerEmail{get;set;}
        public string CustomerName{get;set;}
        public PaymentProcessor? Processor{get;set;}
        public string CreditAccount {get;set;}
        public string DealerCode{get;set;}
        public TransferType Type{get;set;}

    }

    public enum TransferType
    {
        Debit,
        Credit
    }

    public enum PaymentProcessor
    {
        Monnify = 1,
        Rubies,
        Wema,
        VFD
    }
}