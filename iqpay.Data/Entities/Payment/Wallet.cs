﻿using iqpay.Data.Entities;
using iqpay.Data.Entities.Services;
using iqpay.Data.UserManagement.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace iqpay.Data.Entities.Payment
{
    public class Wallet:EntityBase
    {
        public decimal Balance { get; set; }
        public long UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
        public WalletType Type { get; set; }
        public CommissionMode CommissionMode { get; set; }
        public string WalletName { get; set; }
        public string Description { get; set; }
        public bool IsEnabled { get; set; }
        public long? ServiceId { get; set; }
        [ForeignKey("ServiceId")]
        public Services.Services Service { get; set; }
        public long? VendorId{get;set;}
        [ForeignKey("VendorId")]
        public Vendor Vendor{get;set;}
    }

    public class WalletBalance
    {
        public long DealerId{get;set;}
        public string DealerCode{get;set;}
        public string DealerName{get;set;}
        public string WalletType{get;set;}
        public decimal TotalBalance{get;set;}
    }

    public enum WalletType
    {
        [Description("Transaction Base")]
        Transaction = 1,
        [Description("Commission")]
        Commission,
        [Description("Wema Wallet")]
        WemaInternalWallet
    }

    public enum CommissionMode
    {
        [Description("Base")]
        Base,
        [Description("Transaction based")]
        TransactionBased,
        [Description("Commission ahead")]
        CommissionAhead        
    }
}
