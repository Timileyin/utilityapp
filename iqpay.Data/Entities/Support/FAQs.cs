﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using iqpay.Data.UserManagement.Model;

namespace iqpay.Data.Entities.Support
{
    public class FAQs:EntityBase
    {
        public string Question { get; set; }
        public string Answer { get; set; }
        [ForeignKey("CreatedBy")]
        public ApplicationUser User { get; set; }
    }
}
