﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using iqpay.Data.UserManagement.Model;

namespace iqpay.Data.Entities.Support
{
    public class SupportTicket:EntityBase
    {
        public int? UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
        public string FullName { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public SupportTicketStatus Status { get; set; }
        public string Response { get; set; }
        public int? ResponsedBy { get; set; }
        [ForeignKey("ResponsedBy")]
        public ApplicationUser ResponseUser { get; set; }
        public string EmailAddress { get; set; }
        public string SupportImage { get; set; }
        public List<SupportTicketImage> SupportImages { get; set; }
    }

    public enum SupportTicketStatus
    {
        Opened = 1,
        AttendedTo,
        Closed
    }
}
