﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace iqpay.Data.Entities.Support
{
    public class SupportTicketImage:EntityBase
    {
        public int SupportTicketId { get; set; }
        [ForeignKey("SupportTicketId")]
        public SupportTicket SupportTicket { get; set; } 
        public string SupportImageLoc { get; set; }
    }
}
