﻿using System;
using System.Collections.Generic;
using iqpay.Data.Entity.Interface;
using System.ComponentModel.DataAnnotations;

namespace iqpay.Data.Entities
{
    public class EntityBase: IAudit
    {
        public EntityBase()
        {
            IsActive = true;
            DateCreated=DateTime.Now;
            
        }
        public long Id { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsTransient()
        {
            return EqualityComparer<long>.Default.Equals(Id, default(long));
        }

        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string DeletedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public byte[] RowVersion { get; set; }
    }
}