﻿using System;
using System.Collections.Generic;
using System.Text;

namespace iqpay.Data.Entities.Email
{
    public enum EmailTemplateType
    {
        AdminUserCreation = 1,
        ProfileCreation = 2
    }

    public enum EmailStatus
    {
        Fresh = 1,
        Sent,
        Failed
    }
}

