﻿using iqpay.Data.Entity.Interface;
using iqpay.Engine.Base.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace iqpay.Data.Entities.Email
{
    public class EmailTemplate: VatEmailTemplate, IEntity, IAudit
    {        
        public DateTime DateCreated { get; set; }
        public bool IsTransient()
        {
            return EqualityComparer<long>.Default.Equals(Id, default(int));
        }        
        public string SMSContent{get;set;}
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public string DeletedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public byte[] RowVersion { get; set; }

    }
}
