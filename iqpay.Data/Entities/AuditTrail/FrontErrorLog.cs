﻿using System;
using System.Collections.Generic;
using System.Text;
using iqpay.Data.UserManagement.Model;

namespace iqpay.Data.Entities.AuditTrail
{
    public class FrontErrorLog:EntityBase
    {
        public string Error { get; set; }
        public DeviceType DeviceType { get; set; }
        public string DeviceToken { get; set; }
    }
}
