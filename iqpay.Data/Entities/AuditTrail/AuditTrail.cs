﻿using iqpay.Data.Entity.Interface;
using iqpay.Engine.Base.Entities;
using System;
using System.Collections.Generic;
using System.Text;
namespace iqpay.Data.Entities.AuditTrail
{
    public class AuditTrail : VatActivityLog, IAudit
    {
        public string CreatedBy { get; set; }
        public string DeletedBy { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? LastUpdateDate { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
