﻿using System;
using System.ComponentModel;

namespace iqpay.Data.Entities.Services
{
    public enum ProcessorsEnum
    {
        [Description("Nomi")]
        NomiWorld = 1,
        [Description("CPCN")]
        Capricorn,
        [Description("GE")]
        GiftEdge,
        [Description("VATEKO")]
        VATEBRAEKO,
        [Description("VATIBADAN")]
        VATEBRAIBADAN,
        [Description("VATWAEC")]
        VATEBRAWAEC,
        [Description("BP")]
        BUYPOWER,
        [Description("SHAGO")]
        SHAGO,
        [Description("FETS")]
        FETS,
        [Description("GS")]
        GRAYSTONE,
        [Description("IQPAY")]
        IQIPAYSHIPPING

    }
}
