﻿using System;
using System.Collections.Generic;

namespace iqpay.Data.Entities.Services
{
    public class Processor:EntityBase
    {
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Balance { get; set; }
        public ServiceStatusEnum Status { get; set; }
        public List<ProcessorsServiceCodes> Services { get; set; }
    }
}
