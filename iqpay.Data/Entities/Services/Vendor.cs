﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using iqpay.Data.UserManagement.Model;
using iqpay.Data.Entities.Payment;

namespace iqpay.Data.Entities.Services
{
    public class Vendor:EntityBase
    {
        public string VendorName { get; set; }
        public VendorStatusEnum Status { get; set; }
        public decimal VendorWalletBalance { get; set; }
        public long UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
        public List<Services> Serivces { get; set; }
        public string Description { get; set; }
        public decimal Commission { get; set; }
        public string LogoSrc{get;set;}
        public bool IsCommissionAhead{get;set;}
        public decimal? CommissionCap{get;set;}
        public decimal CustomerCommission{get;set;}
        public decimal? CustomerCommissionCap{get;set;}
        public decimal SuperDealerCommission{get;set;}
        public decimal? SuperDealerCommissionCap{get;set;}
        public decimal DealerCommission{get;set;}
        public decimal? DealerCommissionCap{get;set;}
        public decimal AgentCommission{get;set;}
        public decimal? AgentCommissionCap{get;set;}
        public List<DealerCommission> DealerCommissions{get;set;}  
        public bool EnableHouseHoldBonus{get;set;}
        public string HouseHoldBonusDates{get;set;}
        public decimal HouseHoldBonusPercentage{get;set;}
        public decimal HouseHoldBonusCap{get;set;}
    }

    public enum VendorStatusEnum
    {
        [Description("Deactivated")]
        Deactivated,
        [Description("Activated")]
        Activated
    }
}
