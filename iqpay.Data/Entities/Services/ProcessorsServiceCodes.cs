﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace iqpay.Data.Entities.Services
{
    public class ProcessorsServiceCodes:EntityBase
    {        
        public long ServiceId { get; set; }
        [ForeignKey("ServiceId")]
        public Services Service { get; set; }
        public long ProcessorId { get; set; }
        [ForeignKey("ProcessorId")]
        public Processor Processor { get; set; }
        public string Code { get; set; }
    }
}
