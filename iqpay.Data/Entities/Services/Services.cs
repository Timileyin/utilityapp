﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using iqpay.Data.Entities.Payment;
using iqpay.Data.UserManagement.Model;

namespace iqpay.Data.Entities.Services
{
    public class Services:EntityBase
    {    
        public string ServiceName { get; set; }
        public long VendorId { get; set; }
        public Vendor Vendor { get; set; }
        public decimal? Price { get; set; }
        public ServiceStatusEnum Status { get; set; }
        public long UserId { get; set; }
        public ApplicationUser User { get; set; }
        public ServiceType Type { get; set; }
        public string Description { get; set; }
        public CommissionMode CommissionMode { get; set; }
        public bool isFixedPrice { get; set; }
        public bool RequiresVerification { get; set; }
        public decimal Commission{get;set;}
        public decimal? CommissionCap{get;set;}

        public decimal CustomerCommission{get;set;}
        public decimal? CustomerCommissionCap{get;set;}

        public decimal DefaultSuperDealerCommission{get;set;}
        public decimal? DefaultSuperDealerCommissionCap{get;set;}

        public decimal DefaultDealerCommission{get;set;}
        public decimal? DefaultDealerCommissionCap{get;set;}

        public decimal DefaultAgentCommission{get;set;}
        public decimal? DefaultAgentCommissionCap{get;set;}
        public List<ProcessorsServiceCodes> ProcessorCodes { get; set; }
        public List<DealerCommission> DealerCommissions{get;set;}      
        public bool IsMeasuredService{get;set;}
        public string UnitType{get;set;}  
        public DeliveryTypes? DeliveryType{get;set;}

        public decimal? FlatDeliveryFee {get;set;}
        public decimal? FlatDeliveryFeeCap{get;set;}
        public decimal? FlatDeliveryFeeBeyondCap{get;set;}

        public decimal? FlatCollectionFee {get;set;}
        public decimal? FlatCollectionFeeCap{get;set;}
        public decimal? FlatCollectionFeeBeyondCap{get;set;}

        public bool IsSharedProduct{get;set;}
        public int? MaxOrders{get;set;}
        public DateTime? LastOrderDate{get;set;}
        
    }

    public enum ServiceStatusEnum
    {
        [Description("Deactivated")]
        Deactivated,
        [Description("Activated")]
        Activated
    }

    public enum ServiceType
    {
        [Description("Electricty")]
        Electricity = 1,
        [Description("Airtime Topup")]
        AirTimeTopup,
        [Description("Educational")]
        Educational,
        [Description("Cable TV")]
        CableTV,
        [Description("Internet")]
        Internet,
        [Description("Lotto")]
        Lotto,
        [Description("E-Pins")]
        EPins,
        [Description("Food")]
        Food
    }

    public enum DeliveryTypes
    {
        CollectionPoint = 1,
        Delivery,
        Both
    }
}
