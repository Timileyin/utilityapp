﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace iqpay.Data.Entities.Notification
{
    public class PushNotification:EntityBase
    {
        public List<NotificationDeviceToken> RecipientsTokens { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public string Data { get; set; }
        public DateTime SendTime { get; set; }
        public bool IsInstant { get; set; }
        public bool IsCreatedBySystem { get; set; }
        public NotificationStatus Status { get; set; }
        public int? AnnouncementId { get; set; }
        [ForeignKey("AnnouncementId")]
        public Announcement Announcement { get; set; }
        public string FireBasePayLoad { get; set; }
        public DateTime? DateSent { get; set; }

    }

    public enum NotificationStatus
    {
        Initiated = 1,
        Sent,
        Failed
    }
}
