﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using iqpay.Data.UserManagement.Model;

namespace iqpay.Data.Entities.Notification
{
    public class NotificationDeviceToken:EntityBase
    {
        public int NotificationId { get; set; }
        [ForeignKey("NotificationId")]
        public PushNotification Notification { get; set; }
        [ForeignKey("DeviceTokenId")]
        public DeviceToken DeviceToken { get; set; }
        public int DeviceTokenId { get; set; }
    }
}
