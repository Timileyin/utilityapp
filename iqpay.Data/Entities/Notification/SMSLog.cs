using EmailEngine.Base.Entities;
using iqpay.Data.Entity.Interface;
using iqpay.Engine.Base.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace iqpay.Data.Entities.Notification
{
    public class SMSLog : EntityBase
    {
        public string Content{get;set;}
        public string Receiver{get;set;}
        public SMSStatusEnum Status{get;set;}    
        public DateTime DateToSend{get;set;} 
        public bool IsSendImmediately{get;set;}      
        public string MessageId{get;set;}       
    }

    public enum SMSStatusEnum
    {
        Fresh = 1,
        Sent,
        Failed
    }
}
