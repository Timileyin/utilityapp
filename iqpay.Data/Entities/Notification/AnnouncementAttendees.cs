﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace iqpay.Data.Entities.Notification
{
    public class AnnouncementAttendees:EntityBase
    {
        public int AnnouncementId { get; set; }
        [ForeignKey("AnnouncementId")]
        public Announcement Announcement { get; set; }    
    }
}
