﻿using System;
using System.Collections.Generic;

namespace iqpay.Data.Entities.Notification
{
    public class Announcement:EntityBase
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public string Data { get; set; }
        public DateTime SendTime { get; set; }
        public List<AnnouncementAttendees> Attendees { get; set; }
        public bool IsInstant { get; set; }
        public List<PushNotification> Notifications { get; set; }

    }
}
