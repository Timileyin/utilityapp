﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using iqpay.Data.UserManagement.Model;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using iqpay.Data.Entity.Interface;
using iqpay.Data.Entities;
using iqpay.Data.Entities.Email;
using iqpay.Data.Entities.AuditTrail;
using iqpay.Data.Extensions;
using iqpay.Data.Entities.Payment;
using iqpay.Data.Entities.Services;
using iqpay.Data.Entities.Notification;

namespace iqpay.Data
{
    public class IQPAYContext : IdentityDbContext<ApplicationUser,ApplicationRole,long,ApplicationUserClaim,ApplicationUserRole,ApplicationUserLogin, IdentityRoleClaim<long>,IdentityUserToken<long>>
    {
      
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<ApplicationRole> ApplicationRoles { get; set; }
        public DbSet<ApplicationUserRole> ApplicationUserRoles { get; set; }
        public DbSet<ApplicationUserClaim> ApplicationUserClaims { get; set; }
        public DbSet<ApplicationUserLogin> ApplicationUserLogins { get; set; }
        public DbSet<EmailLog> EmailLog { get; set; }
        public DbSet<EmailTemplate> EmailTemplates { get; set; }        
        public DbSet<Country> Countries { get; set; }
        public DbSet<DeviceToken> DeviceTokens { get; set; }
        public DbSet<AuditTrail> AuditTrails { get; set; }
        public DbSet<Wallet> Wallets { get; set; }
        public DbSet<Services> Services { get; set; }
        public DbSet<Vendor> Vendors { get; set; }
        public DbSet<FundWalletRequest> FundWallet { get; set; }
        public DbSet<ProcessorsServiceCodes> ProcessorServiceCode { get; set; }
        public DbSet<Processor> Processors { get; set; }
        public DbSet<VendingLogs> VendingLogs { get; set; }
        public DbSet<ThridPartyAPIKeys> ThirdPartyAPIKeys { get; set; }
        public DbSet<BillLog> BillLogs { get; set; }
        public DbSet<BankTransfer> BankTransfers{get;set;}
        public DbSet<WalletTransaction> Transfers{get;set;}
        public DbSet<SMSLog> SMSLogs{get;set;} 
        public DbSet<SendMoneyLog> SendMoneyLogs{get;set;}    
        public DbSet<DealersServices> DealersServices{get;set;}    
        public DbSet<PartnershipRegistration> PaternshipRegistration {get;set;}

        public IQPAYContext(DbContextOptions<IQPAYContext> options) :base(options)
        {
            Database.Migrate();
            //this.Confi
            //options.Configuration.ProxyCreationEnabled = false;

            //Configuration.LazyLoadingEnabled = false;
            // this.Database.Log = (e) => Debug.WriteLine(e);
        }

        public override int SaveChanges()
        {
            try
            {
                Audits();
                return base.SaveChanges();
            }
            catch (DbUpdateException filterContext)
            {
                if (typeof(DbUpdateException) == filterContext.GetType())
                {
                    Debug.WriteLine("Property: {0}", filterContext.Message);                    
                }
                throw;
            }
        }        


        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            try
            {
               Audits();
                return await base.SaveChangesAsync(cancellationToken);
            }
            catch (DbUpdateConcurrencyException filterContext)
            {
                  Debug.WriteLine("Concurrency Error: {0}",filterContext.Message);
                return await Task.FromResult(0);
            }            
        }

        protected override  void OnModelCreating(Microsoft.EntityFrameworkCore.ModelBuilder modelBuilder)
        {   
            modelBuilder.Entity<ApplicationUser>(entity =>
            {
                entity.Property(u => u.PhoneNumber).IsRequired();
                entity.HasIndex(u => u.PhoneNumber).IsUnique();                
            }); 
            
                     
            base.OnModelCreating(modelBuilder);
            modelBuilder.Seed();       

            
        }

        //add to manage Audit
        private void Audits()
        {

            var entities = ChangeTracker.Entries().Where(x => (x.Entity is IEntity || x.Entity is IAudit || x.Entity is EntityBase) && (x.State == EntityState.Added || x.State == EntityState.Modified));
            try
            {
                var userId = Environment.UserName;
                foreach (var entity in entities)
                {
                    if (entity.State == EntityState.Added)
                    {

                        if (entity.Entity is IAudit)
                        {
                            ((IAudit) entity.Entity).DateCreated = DateTime.Now;
                            ((IAudit) entity.Entity).CreatedBy = userId;
                            ((IAudit) entity.Entity).IsActive = true;
                            ((IAudit) entity.Entity).IsDeleted = false;
                            ((IAudit) entity.Entity).DateCreated = DateTime.Now;
                            ((IAudit) entity.Entity).IsActive = true;
                            ((IAudit) entity.Entity).IsDeleted = false;
                        }

                    }
                    else if (entity.State == EntityState.Modified)
                    {
                        if (entity.Entity is IAudit)
                        {
                            ((IAudit) entity.Entity).LastUpdateDate = DateTime.Now;
                            ((IAudit) entity.Entity).UpdatedBy = userId;
                            if (((IAudit) entity.Entity).IsDeleted)
                            {
                                ((IAudit) entity.Entity).DeletedBy = userId;

                            }
                        }
                    }


                }
            }
            catch (Exception e)
            {
                Debug.Write(e.Message);
            }
            
        }

        

        
    }
}
