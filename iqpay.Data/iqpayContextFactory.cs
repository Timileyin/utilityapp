﻿using iqpay.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace VATEEP.Data
{
    public class IQPAYContextFactory: IDesignTimeDbContextFactory<IQPAYContext>
    {
        public IQPAYContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<IQPAYContext>();
            //optionsBuilder.UseSqlServer("Server=FLEETTFSSRV001\\APPSDB; Database=NACC;User ID=development;Password=password;MultipleActiveResultSets=true;");
           optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=IQPAY;User ID=sa;Password=qwerty;MultipleActiveResultSets=true");
            
            return new IQPAYContext(optionsBuilder.Options);
        }
    }
} 