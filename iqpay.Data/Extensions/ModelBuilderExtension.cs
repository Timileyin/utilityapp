﻿using iqpay.Core.Utilities;
using iqpay.Data.UserManagement.Model;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using iqpay.Data.Entities.Payment;
using iqpay.Data.Entities.Services;
namespace iqpay.Data.Extensions
{
    public static class ModelBuilderExtension
    {
        
        
        public static async void Seed(this ModelBuilder modelBuilder)
        {

            //Seeding roles
            var sysadminRole = new ApplicationRole()
            {
                Id = 2,
                Name = RoleTypes.SYSADMIN.GetDescription(),
                NormalizedName = RoleTypes.SYSADMIN.GetDescription().ToUpper()

            };

            var agentRole = new ApplicationRole()
            {
                Id = 3,
                Name = RoleTypes.IQISUPERDEALER.GetDescription(),
                NormalizedName = RoleTypes.IQISUPERDEALER.GetDescription().ToUpper()
            };

            var superAgentRole = new ApplicationRole()
            {
                Id = 4,
                Name = RoleTypes.AGENT.GetDescription(),
                NormalizedName = RoleTypes.AGENT.GetDescription().ToUpper()
            };

            var dealer = new ApplicationRole()
            {
                Id = 5,
                Name = RoleTypes.DEALER.GetDescription(),
                NormalizedName = RoleTypes.DEALER.GetDescription().ToUpper()
            };

            var regularCustomer = new ApplicationRole()
            {
                Id = 6,
                Name = RoleTypes.REGULAR_CUSTOMER.GetDescription(),
                NormalizedName = RoleTypes.REGULAR_CUSTOMER.GetDescription().ToUpper()
            };


            //Seeding sysadmin
            var appUser = new ApplicationUser
            {
                Id = 10,
                FirstName = "System",
                LastName = "Admin",
                EmailConfirmed = true,
                UserName = "sysadmin@iqipay.com",
                NormalizedEmail = "sysadmin@iqipay.com".ToUpper(),
                NormalizedUserName = "sysadmin@iqipay.com".ToUpper(),
                SecurityStamp = Guid.NewGuid().ToString(),
                PhoneNumber = "08096279121",
                Email = "sysadmin@iqipay.com"                    
            };

            var passwordHasherHelper = new PasswordHasher<ApplicationUser>();
            var hash = passwordHasherHelper.HashPassword(appUser, "password");
            appUser.PasswordHash = hash;

            modelBuilder.Entity<ApplicationUser>().HasData(appUser);
                           
            modelBuilder.Entity<ApplicationRole>().HasData(agentRole, regularCustomer, dealer, superAgentRole, sysadminRole);
                        
            
            
        }
    }
}
