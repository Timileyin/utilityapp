﻿using Hangfire.Annotations;
using Hangfire.Dashboard;
namespace iqpay.Web.Filters
{
    public class MyAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull]DashboardContext context)
        {
            // In case you need an OWIN context, use the next line, `OwinContext` class
            // is the part of the `Microsoft.Owin` package.
            var httpContext = context.GetHttpContext();
            // Allow all authenticated users to see the Dashboard (potentially dangerous).
            return true;// owinContext.Authentication.User.Identity.IsAuthenticated;
        }
    }
}