using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System.Text;
using System;
using Microsoft.AspNetCore.Identity;
using iqpay.Data.UserManagement.Model;
using Microsoft.Extensions.Logging;

namespace iqpay.Web.Filters
{
    public class AuthenticationMiddleware   
    {
        private readonly RequestDelegate _next;
        private readonly UserManager<ApplicationUser> _usrMgr;
        private readonly ILogger<AuthenticationMiddleware> _logger;
        
        // Dependency Injection
        public AuthenticationMiddleware(RequestDelegate next, UserManager<ApplicationUser> usrMgr, ILogger<AuthenticationMiddleware> logger)
        {
            _next = next;
            _usrMgr = usrMgr;
            _logger = logger;
        }

        private static byte[] ConvertFromBase64String(string input) 
        {
            string working = input.Replace('-', '+').Replace('_', '/');
            while (working.Length % 3 != 0) {
                working += '=';
            }
            try {
                return Convert.FromBase64String(working);
            } catch(Exception) {
                try {
                    return Convert.FromBase64String(input.Replace('-', '+').Replace('_', '/'));
                } catch(Exception) {}
                try {
                    return Convert.FromBase64String(input.Replace('-', '+').Replace('_', '/') + "=");
                } catch(Exception) {}
                try {
                    return Convert.FromBase64String(input.Replace('-', '+').Replace('_', '/') + "==");
                } catch(Exception) {}
                
                return null;
            }
        }

        public async Task Invoke(HttpContext context)
        {
            //Reading the AuthHeader which is signed with JWT
            string authHeader = context.Request.Headers["Authorization"];
            

            if (authHeader != null)
            {   
                //Reading the JWT middle part 
                _logger.LogWarning($"this is the auth header =>{authHeader}");          
                int startPoint = authHeader.IndexOf(".") + 1;               
                int endPoint = authHeader.LastIndexOf(".");

                var tokenString = authHeader
                    .Substring(startPoint, endPoint - startPoint).Split(".");
                var token = tokenString[0].ToString();

                _logger.LogWarning($"this is the auth header =>{token}"); 

                var credentialString = Encoding.UTF8.GetString(ConvertFromBase64String(token));//Encoding.UTF8
                    //.GetString(Convert.FromBase64String(token));
                
                // Splitting the data from Jwt
                var credentials = credentialString.Split(new char[] { ':',',' });
                _logger.LogWarning($"this is the credentials =>{credentialString}");

                // Trim this Username and UserRole.
               // var userRule = credentials[5].Replace("\"", ""); 
                var userName = credentials[1].Replace("\"", "");

                //var username = context.User.Identity.Name;
                _logger.LogWarning($"The username {userName}");

                var user = await _usrMgr.FindByEmailAsync(userName);
                if(user != null)
                {
                    if(user.LockoutEnd.HasValue)
                    {
                        context.Response.StatusCode = 401;
                        return;    
                    }                    
                }                
            }

            await _next(context);
            //Pass to the next middleware
            
        } 
    }

}

