﻿using AutoMapper;
using EmailEngine.Base.Entities.Models;
using VATEEP.Repository.Users;
using iqpay.Data.Entities.Payment;
using iqpay.Web.Models;
using iqpay.Data.UserManagement.Model;
using iqpay.Data.Entities.Services;
using iqpay.Core.Utilities;
using System.Linq;
using System;
using iqpay.Repository.Rubies.Models;
using iqpay.Data.Entities.Settings;
using iqpay.Repository.Wema.Models;
using Microsoft.Extensions.Configuration;

namespace iqpay.Web.AutoMapperProfile
{
    public class MapperProfile:Profile
    {
        public MapperProfile()
        {
            CreateMap<SettingsModel, Settings>().ReverseMap();            
            CreateMap<PaymentLog, PaymentDetails>()
                .ForMember(x => x.BillRefenceNo, y => { y.MapFrom(x => x.BillLog.BillRefenceNo); })
                .ForMember(x => x.BillStatus, y => { y.MapFrom(x => x.BillLog.BillStatus); })
                .ForMember(x => x.FullName, y => { y.MapFrom(x => $"{x.BillLog.User.LastName} {x.BillLog.User.FirstName}"); })
                .ReverseMap();  

            CreateMap<PartnershipRegistration, PartnerRegisterViewModel>()
                .ForMember(x=>x.Status, y=>{y.MapFrom(x=>x.Status == 1 ? "Approved" :"Pending Approval");})
                .ForMember(x=>x.ApprovedBy, y=>{y.MapFrom(x=> x.User != null ? $"{x.User.FirstName}{x.User.LastName}" : "");})
            .ReverseMap();
            
            CreateMap<ApplicationUser, UserModel>()
                .ForMember(x => x.IsLockedOut , y => { y.MapFrom(x => x.LockoutEnabled && x.LockoutEnd.HasValue); })
                .ReverseMap();
            CreateMap<ApplicationUser, AdminViewModel>()
                .ForMember(x => x.Email, y => { y.MapFrom(x => x.Email); })
                .ForMember(x => x.FirstName, y => { y.MapFrom(x => x.FirstName); })
                .ForMember(x => x.LastName, y => { y.MapFrom(x => x.LastName); })
                .ForMember(x => x.Id, y => { y.MapFrom(x => x.Id); })
                .ForMember(x=>x.DateCreated, y=> { y.MapFrom(x => x.DateCreated); });

            CreateMap<ApplicationUser, WemaAccount>()
                .ForMember(x=>x.AccountNumber, y => {y.MapFrom(x=>x.VirtualAccountNumber);})
                .ForMember(x=>x.DealerCode, y=>{y.MapFrom(x=>x.UserName);});

            CreateMap<AdminUserModel, AdminViewModel>()
                .ForMember(x => x.Email, y => { y.MapFrom(x => x.Email); })
                .ForMember(x => x.FirstName, y => { y.MapFrom(x => x.FirstName); })
                .ForMember(x => x.LastName, y => { y.MapFrom(x => x.LastName); })
                .ForMember(x => x.Id, y => { y.MapFrom(x => x.Id); })
                .ForMember(x => x.Role, y => { y.MapFrom(x => x.Role); }).ReverseMap();

            CreateMap<Services, DealerCommissionModel>()
                .ForMember(x=>x.Service, y=>{y.MapFrom(x=>$"{x.ServiceName}({x.Vendor.VendorName})");})
                .ForMember(x=>x.CommissionMode, y=>{y.MapFrom(x=>x.CommissionMode.GetDescription());})
                .ForMember(x=>x.ServiceId, y=>{y.MapFrom(x=>x.Id);})
                .ForMember(x=>x.Commission, y=>{y.MapFrom(x=>x.DealerCommissions.FirstOrDefault().Commission);})
                .ForMember(x=>x.Cap, y=>{y.MapFrom(x=>x.DealerCommissions.FirstOrDefault().CommissionCap);});

            CreateMap<WalletTransaction, CommissionAheadReport>()
                .ForMember(x=>x.Service, y=>{y.MapFrom(x=> x.Wallet.Vendor == null ? $"{x.Wallet.Service.Vendor.VendorName} /{x.Wallet.Service.ServiceName}" : x.Wallet.Vendor.VendorName);})                
                .ForMember(x=>x.TransactionDate, y=>{y.MapFrom(x=>x.DateCreated.ToString("  dd MMM yyyy hh:mm tt"));})
                .ForMember(x=>x.DealerCode, y=>{y.MapFrom(x => x.User.UserName);})
                .ForMember(x=>x.DealerName, y=>{y.MapFrom(x=>x.User.Name);});

            CreateMap<Vendor, DealerCommissionModel>()
                .ForMember(x=>x.Vendor, y=>{y.MapFrom(x=>x.VendorName);})
                .ForMember(x=>x.VendorId, y=>{y.MapFrom(x=>x.Id);})
                .ForMember(x=>x.CommissionMode, y=>{y.MapFrom(x=>x.IsCommissionAhead ? "Commission Ahead" : "Commission As You Transact");})
                .ForMember(x=>x.Commission, y=>{y.MapFrom(x=>x.DealerCommissions.FirstOrDefault().Commission);})
                .ForMember(x=>x.Cap, y=>{y.MapFrom(x=>x.DealerCommissions.FirstOrDefault().CommissionCap);});

            CreateMap<Wallet, WalletModel>()
                .ForMember(x => x.Owner, y => { y.MapFrom(x => $"{x.User.LastName} {x.User.FirstName}"); })
                .ForMember(x => x.WalletType, y => { y.MapFrom(x => x.Type.ToString()); })
                .ForMember(x => x.WalletDescription, y => { y.MapFrom(x => x.Description); })
                .ForMember(x=>x.VendorLogo, y => {y.MapFrom(x=> x.VendorId == null ? x.Service.Vendor.LogoSrc : x.Vendor.LogoSrc);})
                .ForMember(x => x.Mode, y => { y.MapFrom(x => x.CommissionMode.GetDescription()); }).ReverseMap();
                
            CreateMap<ProcessorsServiceCodes, ProcessorsCode>()
                .ForMember(x => x.ProcessorCode, y => { y.MapFrom(x => x.Processor.Code); })
                .ForMember(x => x.ProcessorName, y => { y.MapFrom(x => x.Processor.Name); })
                .ForMember(x => x.ServiceId, y => { y.MapFrom(x => x.ServiceId); })
                .ForMember(x => x.ProcessorServiceCode, y => { y.MapFrom(x => x.Code); })
                .ForMember(x=>x.ProcessorId, y => { y.MapFrom(x => x.Processor.Id); })
                .ReverseMap();

            CreateMap<Processor, ProcessorModel>()
                .ForMember(x=>x.DateCreatedStr, y => { y.MapFrom(x => x.DateCreated.ToString()); })
                .ForMember(x=>x.StatusInt, y=> { y.MapFrom(x => (int)x.Status); } )
                .ReverseMap();

            CreateMap<ApplicationUser, RegisterViewModel>()
                .ForMember(x=>x.Firstname, y=>{y.MapFrom(x=>x.FirstName);})
                .ForMember(x=>x.Surname, y=>{y.MapFrom(x=>x.LastName);})
                .ReverseMap();

            CreateMap<ApplicationUser, AgentViewModel>()
                .ForMember(x => x.DateCreated, y => { y.MapFrom(x => x.DateCreated.ToString()); })
                .ForMember(x => x.IsLockedOut, y => { y.MapFrom(x => x.LockoutEnabled && x.LockoutEnd.HasValue); });

            CreateMap<Vendor, VendorsModel>()
                .ForMember(x => x.Status, y => { y.MapFrom(x => x.Status.GetDescription()); })
                .ForMember(x => x.Name, y => { y.MapFrom(x => x.VendorName.ToString()); })
                .ForMember(x => x.DateCreated, y => { y.MapFrom(x => x.DateCreated); })
                .ForMember(x => x.DateCreatedStr, y => { y.MapFrom(x => x.DateCreated.ToString("dd MMM yyyy hh:mm tt")); })
                .ForMember(x => x.Balance, y => { y.MapFrom(x => x.VendorWalletBalance); })
                .ForMember(x => x.StatusInt, y=> { y.MapFrom(x => (int)x.Status); } )
                .ForMember(x=>x.LogoSrc, y=>{y.MapFrom(x=>$"/CompanyLogo/{x.LogoSrc}");});               


            CreateMap<VendorsModel, Vendor>()
                //.ForMember(x => x.Status, y => { y.MapFrom(x => x.Status.GetDescription()); })
                .ForMember(x => x.VendorName, y => { y.MapFrom(x => x.Name.ToString()); })
                .ForMember(x => x.DateCreated, y => { y.MapFrom(x => x.DateCreated); })
                .ForMember(x => x.Description, y => { y.MapFrom(x => x.Description); })
                .ForMember(x => x.VendorWalletBalance, y=> { y.MapFrom(x => x.Balance); });
                

            //.ForMember(x=>x.)

            CreateMap<Services, ServicesModel>()
                .ForMember(x => x.Name, y => { y.MapFrom(x => x.ServiceName); })
                .ForMember(x => x.VendorName, y => { y.MapFrom(x => x.Vendor.VendorName); })
                .ForMember(x => x.Amount, y => { y.MapFrom(x => x.Price); })
                .ForMember(x => x.Status, y => y.MapFrom(x => x.Status.GetDescription()))
                .ForMember(x => x.ServiceType, y => { y.MapFrom(x => x.Type.GetDescription()); })
                .ForMember(x=>x.ServiceTypeInt, y => { y.MapFrom(x => (int)x.Type); })
                .ForMember(x=>x.StatusInt, y=> { y.MapFrom(x => x.Status); })
                .ForMember(x=>x.DeliveryTypeInt, y=> { y.MapFrom(x => (int?)x.DeliveryType); })
                .ForMember(x=>x.CommissionModeStr, y => { y.MapFrom(x => x.CommissionMode.GetDescription()); })
                .ForMember(x=>x.CommissionCap, y=>{ y.MapFrom(x=>x.CommissionCap); })
                .ForMember(x=>x.Commission, y=>{ y.MapFrom(x=>x.Commission); })
                .ForMember(x=>x.AgentCommission, y=>{ y.MapFrom(x=>x.DefaultAgentCommission);})
                .ForMember(x=>x.AgentCommissionCap, y=>{ y.MapFrom(x=>x.DefaultAgentCommissionCap);})
                .ForMember(x=>x.CustomerCommission, y=>{ y.MapFrom(x=>x.CustomerCommission);})
                .ForMember(x=>x.CustomerCommissionCap, y=>{ y.MapFrom(x=>x.CustomerCommissionCap);})
                .ForMember(x=>x.DealerCommission, y=>{ y.MapFrom(x=>x.DefaultDealerCommission);})
                .ForMember(x=>x.DealerCommissionCap, y=>{y.MapFrom(x=>x.DefaultDealerCommissionCap);})
                .ForMember(x=>x.SuperDealerCommission, y=>{y.MapFrom(x=>x.DefaultSuperDealerCommission);})
                .ForMember(x=>x.SuperDealerCommissionCap, y=>{y.MapFrom(x=>x.DefaultSuperDealerCommissionCap);})
                .ForMember(x => x.DateCreatedStr, y => { y.MapFrom(x => x.DateCreated.ToString("dd MMM yyyy hh:mm tt")); });
                
            //.ReverseMap();

            CreateMap<FundWalletRequest, FundingRequest>()
                .ForMember(x => x.AccountNumber, y => { y.MapFrom(x => x.IQBankAccount); })
                .ForMember(x=>x.WalletName, y=> { y.MapFrom(x => x.Wallet.WalletName); })
                .ForMember(x=>x.WalletUser, y => { y.MapFrom(x => x.Wallet.User.Name == null ? $"{x.Wallet.User.FirstName} {x.Wallet.User.LastName}" : x.Wallet.User.UserName); })
                .ForMember(x=>x.Description, y=> { y.MapFrom(x => x.TransactionDetails); })
                .ForMember(x=>x.Status, y=> { y.MapFrom(x => x.Status.GetDescription()); })
                .ForMember(x=>x.TransactionDate, y=>{ y.MapFrom(x=>x.TransactionDate.HasValue ? x.TransactionDate.Value.ToString("dd MMM yyyy hh:mm tt") : "N/A");})
                .ForMember(x=>x.DateCreated, y=>{ y.MapFrom(x=>x.DateCreated.ToString("dd MMM yyyy hh:mm tt")); })
                .ForMember(x=>x.AmountCreditted, y=>{y.MapFrom(x=>x.AmmountCreditted);})
                .ReverseMap();

            CreateMap<ApplicationUser, ReservedBankDetails>()
                .ForMember(x => x.AccountName, y =>{y.MapFrom(x=>x.ReservedAccountName);})
                .ForMember(x => x.AccountNumber, y => {y.MapFrom(x=>x.ReservedAccountNumber);})
                .ForMember(x=>x.RubiesAccountName, y=>{y.MapFrom(x=>x.RubiesAccountName);})
                .ForMember(x=>x.RubiesAccountNumber, y=>{y.MapFrom(x=>x.RubiesAccountNumber);})
                .ForMember(x=>x.BankName, y=>{y.MapFrom(x=>x.ReservedAccountBankName);})
                .ForMember(x=>x.VFDAccountName, y=> {y.MapFrom(x=>x.VFDAccountName);})
                .ForMember(x=>x.VFDAccountNumber, y=>{y.MapFrom(x=>x.VFDAccountNumber);})
                .ForMember(x=>x.WEMAAccountName, y=> {y.MapFrom(x=>x.VirtualAccountName);})
                .ForMember(x=>x.WEMAAccountNumber, y=> {y.MapFrom(x=>x.VirtualAccountNumber);});

            CreateMap<ServicesModel, Services>()
                .ForMember(x => x.VendorId, y => { y.MapFrom(x => x.VendorId); })
                .ForMember(x => x.Type, y => { y.MapFrom(x => ((ServiceType)x.ServiceTypeInt)); })
                .ForMember(x => x.ServiceName, y => { y.MapFrom(x => x.Name); })
                .ForMember(x => x.Price, y => { y.MapFrom(x => x.Amount); });

            CreateMap<Vendor, VendorServiceModel>()
                .ForMember(x=>x.Name, y => { y.MapFrom(x => x.VendorName); })
                .ForMember(x=>x.VendorLogo, y=> { y.MapFrom(x => x.LogoSrc.Contains("CompanyLogo") ? x.LogoSrc : $"CompanyLogo/{x.LogoSrc}");})
                .ReverseMap();

            CreateMap<BillLog, TransactionModel>()
                .ForMember(x=>x.Email, y=> { y.MapFrom(x => x.User.Email); })
                .ForMember(x=>x.Name, y => { y.MapFrom(x => x.User.Name == null ? $"{x.User.FirstName} {x.User.LastName}" : x.User.Name); })
                .ForMember(x=>x.PhoneNumber, y => { y.MapFrom(x => x.User.PhoneNumber); })
                .ForMember(x=>x.BillType, y=> { y.MapFrom(x => ((BillType)x.BillType).GetDescription()); })
                .ForMember(x=>x.DatePaid, y => { y.MapFrom(x => x.DatePaid == null ? DateTime.Now.ToString("dd MMM yyyy hh:mm tt") : x.DatePaid.Value.ToString("dd MMM yyyy hh:mm")); })
                .ForMember(x=>x.BillStatusStr, y => { y.MapFrom(x => ((BillStatus)x.BillStatus).GetDescription()); })
                .ReverseMap();

            CreateMap<SendMoneyLog, BankTransferModel>()
                .ForMember(x=>x.BankName, y=>{y.MapFrom(x=>x.Bank);})
                .ForMember(x=>x.Status, y=>{y.MapFrom(x=>x.Status.ToString());})
                .ForMember(x=>x.BankAccountNumber, y=>{y.MapFrom(x=>x.AccountNumber);})
                .ForMember(x=>x.TransferDate, y=>{y.MapFrom(x=>x.DateCreated);})
                .ForMember(x=>x.DealerCode, y=>{y.MapFrom(x=>x.SentBy.UserName);})
                .ReverseMap();
                
            CreateMap<BankDataModel, BankData>()
                .ForMember(x=>x.Bankcode, y=>{y.MapFrom(x=>x.BankCode); })
                .ForMember(x=>x.Bankname, y=>{y.MapFrom(x=>x.BankName); })
                .ReverseMap();

            CreateMap<VendingLogs, VendingReportModel>()
                .ForMember(x => x.Amount, y => { y.MapFrom(x => x.Amount); })
                .ForMember(x=>x.Status, y => { y.MapFrom(x => x.Status.GetDescription()); })
                .ForMember(x=>x.Service, y => { y.MapFrom(x => $"{x.Service.Vendor.VendorName}/{x.Service.ServiceName}"); })
                .ForMember(x=>x.AgentName, y => { y.MapFrom(x => x.User.AccountType != AccountType.REGULAR ? x.User.Name : ""); })
                .ForMember(x=>x.DealerCode, y => { y.MapFrom(x => x.User.AccountType != AccountType.REGULAR ? x.User.UserName : ""); })                
                .ForMember(x=>x.TransactionDate, y=>{y.MapFrom(x=>x.DateCreated.ToString("dd MMM yyyy hh:mm tt"));})
                .ForMember(x => x.Unit, y=> { y.MapFrom(x=>x.Service.UnitType);})
                .ForMember(x => x.PhoneNumber, y=> { y.MapFrom(x=>x.CustomerNumber);})
                .ReverseMap();

            CreateMap<VendingLogs, VendingModel>()
                .ForMember(x => x.Amount, y => { y.MapFrom(x => x.Amount); })
                .ForMember(x => x.Unit, y=> { y.MapFrom(x=>x.Service.UnitType);})
                .ForMember(x=>x.Status, y => { y.MapFrom(x => x.Status.GetDescription()); })
                .ForMember(x=>x.Service, y => { y.MapFrom(x => $"{x.Service.Vendor.VendorName}/{x.Service.ServiceName}"); })
                .ForMember(x=>x.AgentName, y => { y.MapFrom(x => x.User.AccountType != AccountType.REGULAR ? x.User.Name : ""); })
                .ForMember(x=>x.DealerCode, y => { y.MapFrom(x => x.User.AccountType != AccountType.REGULAR ? x.User.UserName : ""); })
                .ForMember(x=>x.DealerAddress , y => { y.MapFrom(x => x.User.AccountType != AccountType.REGULAR ? x.User.Address : ""); })
                .ForMember(x=>x.Type, y => { y.MapFrom(x => x.Service.Type.GetDescription()); })
                .ForMember(x=>x.VendorLogo, y=>{y.MapFrom(x=>$"/CompanyLogo/{x.Service.Vendor.LogoSrc}");})
                .ForMember(x=>x.StatusInt, y => {y.MapFrom(x=>(int)x.Status);})
                .ForMember(x=>x.TransactionDate, y=>{y.MapFrom(x=>x.DateCreated.ToString("dd MMM yyyy hh:mm tt"));})
                .ForMember(x=>x.IsSharedProduct, y=>{y.MapFrom(x=>x.Service.IsSharedProduct);})
                .ForMember(x=>x.DeliveryDate, y=>{y.MapFrom(x=>x.Service.LastOrderDate);})
                .ReverseMap();
        }
    }
}
