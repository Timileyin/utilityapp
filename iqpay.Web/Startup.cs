using iqpay.Data;
using iqpay.Data.Entities.AuditTrail;
using iqpay.Data.Entities.Email;
using iqpay.Data.Entities.Payment;
using iqpay.Data.UserManagement.Model;
using iqpay.Engine.Base.Repository.EmailRepository;
using iqpay.Engine.Repository.EmailRepository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Autofac.Extensions.DependencyInjection;
using iqpay.Repository.AutoFacModule;
using Autofac;
using ElmahCore.Mvc;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.OpenApi.Models;
using System.Reflection;
using System.IO;
using Hangfire;
using iqpay.Web.Filters;
using iqpay.Repository.EmailServiceRepository;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Hangfire.Storage;
using iqpay.Repository.SMSRepository;
using iqpay.Repository.AutoReversals;

namespace iqpay.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        readonly string MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

        public IConfiguration Configuration { get; }
        public IContainer ApplicationContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        [Obsolete]
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            services.AddHangfire(x => x.UseSqlServerStorage(Configuration.GetConnectionString("DefaultConnection")));
            services.AddHangfireServer();

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });

            

            services.AddCors(options =>
            {
                options.AddPolicy(MyAllowSpecificOrigins, b =>
                {
                    b.AllowAnyOrigin();
                    b.AllowAnyHeader();
                    b.AllowAnyMethod();
                    //b.AllowCredentials();
                });                  
            });

            services.AddDbContext<IQPAYContext>(options =>
               options.UseSqlServer(
                   Configuration.GetConnectionString("DefaultConnection")));
            services
                .AddIdentity<ApplicationUser, ApplicationRole>()
                .AddEntityFrameworkStores<IQPAYContext>()
                .AddUserManager<UserManager<ApplicationUser>>()
                .AddRoleManager<RoleManager<ApplicationRole>>()
                .AddSignInManager<SignInManager<ApplicationUser>>()
                .AddUserStore<ApplicationUserStore>()
                .AddDefaultTokenProviders();
            services.AddScoped<ApplicationUserStore>();

            //services.AddMvc().AddNewtonsoftJson();

            services.Configure<IdentityOptions>(options =>
            {

                options.Password.RequireUppercase = false;
                options.Password.RequiredUniqueChars = 0;
                options.Password.RequireLowercase = false;
                options.Password.RequireDigit = false;
                options.Password.RequireNonAlphanumeric = false;
                // options.Password.RequiredUniqueChars = 1;
                options.Password.RequiredLength = 4;
                options.SignIn.RequireConfirmedEmail = true;
                options.User.RequireUniqueEmail = true;            
            });

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
           .AddJwtBearer(cfg =>
           {

               cfg.RequireHttpsMetadata = false;
               cfg.SaveToken = true;
               cfg.TokenValidationParameters = new TokenValidationParameters
               {
                   ValidateIssuer = true,
                   ValidIssuer = Configuration["JwtIssuer"],
                   ValidateAudience = true,
                   ValidAudience = Configuration["JwtIssuer"],
                   ValidateIssuerSigningKey = true,
                   IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["JwtKey"])),
                    //RequireExpirationTime = false,
                    ValidateLifetime = false,
                   ClockSkew = TimeSpan.Zero,
                    // remove delay of token when expire,

                };
               cfg.Events = new JwtBearerEvents
               {
                   OnAuthenticationFailed = context =>
                   {
                        //context. = new ObjectResult(new ApiResponse("Autorization has failed", (HttpStatusCode)context.Response.StatusCode, null, true));
                        var logger = context.HttpContext.RequestServices.GetRequiredService<ILoggerFactory>().CreateLogger(nameof(JwtBearerEvents));
                       logger.LogError("Authentication failed.", context.Exception);
                       return Task.CompletedTask;
                   },


                   OnMessageReceived = context =>
                   {
                       return Task.CompletedTask;
                   },
                   OnChallenge = context =>
                   {
                       var logger = context.HttpContext.RequestServices.GetRequiredService<ILoggerFactory>().CreateLogger(nameof(JwtBearerEvents));
                       logger.LogError("OnChallenge error", context.Error, context.ErrorDescription);
                       return Task.CompletedTask;
                   },
               };

           });

            services.Configure<ApiBehaviorOptions>(options => {
                options.SuppressModelStateInvalidFilter = true;
            });


            services.AddAutoMapper();
            services.AddLogging();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "iqpay API", Version = "v1" });
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            services.AddElmah(options =>
            {
                options.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
            });

            services.AddTransient<IEmailManager<EmailLog, EmailTemplate>, EmailManager<EmailLog, EmailTemplate>>();
            services.AddTransient<IAuditTrailManager<AuditTrail>, AuditTrailManager<AuditTrail>>();
            services.AddTransient<IBilling<BillLog, PaymentLog, ApplicationUser, long>, Billing<BillLog, PaymentLog, ApplicationUser, long>>();
            services.AddScoped<INotificationService, NotificationService>();
                 

            var builder = new ContainerBuilder();
            builder.Populate(services);
            builder.RegisterModule<BillRepositoryModule<BillLog, PaymentLog, ApplicationUser, long, IQPAYContext>>();
            builder.RegisterModule<RepositoryModule<EmailLog, EmailTemplate, IQPAYContext>>();
            builder.RegisterModule<GenericRepositoryModule<AuditTrail, IQPAYContext>>();
            builder.RegisterModule<RepositoryModule>();

            ApplicationContainer = builder.Build();

            // Create the IServiceProvider based on the container.
            return new AutofacServiceProvider(ApplicationContainer);
        }

        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseDeveloperExceptionPage();
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseCors(MyAllowSpecificOrigins); 
            //app.UseMiddleware<AuthenticationMiddleware>();
            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new MyAuthorizationFilter() },
            });

            var recurringJobs = Hangfire.JobStorage.Current.GetConnection().GetRecurringJobs();

            foreach (var item in recurringJobs)
            {
                RecurringJob.RemoveIfExists(item.Id);
            }
            RecurringJob.AddOrUpdate<IEmailManager<EmailLog, EmailTemplate>>(salesReport => salesReport.SendBatchMailAsync(), Cron.Minutely());
            RecurringJob.AddOrUpdate<ISMSManager>(SmsService=>SmsService.SendBatchSMSAsync(), Cron.Minutely);
            RecurringJob.AddOrUpdate<IVaterbraReversal>(x=>x.ReversalTransaction(), Cron.Daily(17));
            // app.UseHangfireDashboard();
            
            app.UseAuthentication();
           // app.UseAuthorization();
            //app.UseHttpsRedirection();z
            app.UseStaticFiles();
            app.UseSpaStaticFiles();
            app.UseElmah();

            //Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                
                //if (env.IsDevelopment())
                //{
                //    c.SwaggerEndpoint("v1/swagger.json", "My API V1"); 
                //} else {
                    c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                //} 
            });

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
