﻿using iqpay.Data.UserManagement.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace iqpay.Web.Models
{
    public class ReservedBankDetails
    {
        public string AccountName{get;set;}
        public string AccountNumber{get;set;}
        public string RubiesAccountName{get;set;}
        public string RubiesBankName{get;set;}
        public string RubiesAccountNumber{get;set;}
        public string VFDAccountName{get;set;}
        public string VFDBankName{get;set;}
        public string VFDAccountNumber{get;set;}
        public string WEMAAccountName{get;set;}
        public string WEMABankName{get;set;}
        public string WEMAAccountNumber{get;set;}
        public string BankName{get;set;}
    }
    
    public class LoginViewModel
    {

        [Required(ErrorMessage = "Please provide the username")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Please provide the password")]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
        //[Required(ErrorMessage = "Please provide the device token")]
        public string DeviceToken { get; set; }
        //[Required(ErrorMessage = "Please provide the device category")]
        public DeviceType DeviceType { get; set; }
        //[Required(ErrorMessage = "Please provide device name")]
        public string DeviceName { get; set; }
    }

    public class ChangePasswordModel
    {
        [Required(ErrorMessage = "Please provide current password")]
        public string CurrentPassword{get;set;}
        [Required(ErrorMessage = "Please provide you new password")]
        [DataType(DataType.Password,ErrorMessage = "Please provide a valid password")]
        public string NewPassword { get; set; }
        [Compare("NewPassword",ErrorMessage = "New password and confirm password do not match")]
        public string ConfirmPassword { get; set; }
    }

    public class AuthModel
    {
        public long UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public string Role { get; set; }
        public DateTime ExpiryTime { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Permissions{get;set;}
    }

    public class VerifyEmailViewModel
    {
        public string UserId { get; set; }
        public string Code { get; set; }

        public VerifyEmailViewModel(string code, string userId)
        {
            UserId = userId;
            Code = code;
        }
    }

    public class ForgotPassswordRequest
    {
        [Required(ErrorMessage = "Please provide username")]
        public string Username { get; set; }
    }

    public class DealerUpdateViewModel
    {
        [Required(ErrorMessage = "Email address field is required")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please provide a valid Email address")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Phone number is required")]
        [Display(Name = "Phonenumber")]
        public string Phonenumber { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [Display(Name = "Address")]
        public string Address { get; set; }
    }


    public class DealerRegisterViewModel
    {
        [Required(ErrorMessage = "Email address field is required")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please provide a valid Email address")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [Display(Name = "Name")]
        public string Name { get; set; }        

        [Required(ErrorMessage = "Phone number is required")]
        [Display(Name = "Phonenumber")]
        public string Phonenumber { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Password field is required")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Confirm password not the same as password")]
        public string ConfirmPassword { get; set; }

        public bool UpdateVateepProfile { get; set; }
    }

    public class AgentViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string DateCreated { get; set; }
        public string Email { get; set; }
        public string Phonenumber { get; set; }
        public string Address { get; set; }
        public bool EmailVerified { get; set; }
        public bool IsLockedOut { get; set; }

    }

    public class AgentRegisterViewModel
    {
        [Required(ErrorMessage = "Email address field is required")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please provide a valid Email address")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Name is required")]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Phone number is required")]
        [Display(Name = "Phonenumber")]
        public string Phonenumber { get; set; }

        [Required(ErrorMessage = "Address is required")]
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Password field is required")]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Confirm password not the same as password")]
        public string ConfirmPassword { get; set; }

        public bool UpdateVateepProfile { get; set; }
    }

    public class SysAdminViewModel
    {        
        [Required(ErrorMessage = "Email address field is required")]
        [DataType(DataType.EmailAddress,ErrorMessage ="Please provide a valid Email address")]
        [Display(Name = "Email")]
        public string Email{ get; set; }

        [Required(ErrorMessage = "Name is required")]
        [Display(Name = "Name")]
        public string Name { get; set; }
        public string Surname { get; set; }

        [Required(ErrorMessage = "Phone number is required")]
        [Display(Name = "Phonenumber")]
        public string Phonenumber { get; set; }

        [Required(ErrorMessage = "Password field is required")]
        public string Password { get; set; }

        [Required]
        [Compare("Password",ErrorMessage = "Confirm password not the same as password")]        
        public string ConfirmPassword{ get; set; }

        public bool UpdateVateepProfile { get; set; }        
    }

    public class RegisterPartnerDealer
    {
        public long Id{get;set;}
        public string UserType{get;set;}
    }

     public class PartnerRegisterViewModel
    {        
        public long? Id{get;set;}

        public long? SuperDealerId{get;set;}

        [Required(ErrorMessage = "Email address field is required")]
        [DataType(DataType.EmailAddress,ErrorMessage ="Please provide a valid Email address")]
        [Display(Name = "Email")]
        public string Email{ get; set; }

        [Required(ErrorMessage = "First name is required")]
        [Display(Name = "Firstname")]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Surname is required")]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Phone number is required")]
        [Display(Name = "Phonenumber")]
        public string Phonenumber { get; set; }

        [Required(ErrorMessage = "BVN is required")]
        [RegularExpression("\\d{11}", ErrorMessage="Please supply a valid BVN")]
        [Display(Name = "BVN")]
        public string BVN { get; set; }

        [Display(Name = "NIN")]
        [RegularExpression("\\d{11}", ErrorMessage="Please supply a valid NIN")]
        public string NIN { get; set; }

        [Required(ErrorMessage = "Address(Line 1) is required")]
        [Display(Name = "Address")]
        public string AddressLine1 {get;set;}      

        [Display(Name = "Address (Line2)")]
        public string AddressLine2 {get;set;}

        public int LGA{get;set;}
        public int State{get;set;}
        public string PhotoIdLoc{get;set;}

        [Required(ErrorMessage="Company name is required")]
        public string CompanyName{get;set;}

        public string Status{get;set;}

        public string ApprovedBy{get;set;}

        public IFormFile PhotoID{get;set;}

        public bool UpdateVateepProfile { get; set; }     
    }

    public class PermissionsModel
    {
        [Required]
        public string Permissions{get;set;}
    }


    public class RegisterViewModel
    {        
        public long? Id{get;set;}

        [Required(ErrorMessage = "Email address field is required")]
        [DataType(DataType.EmailAddress,ErrorMessage ="Please provide a valid Email address")]
        [Display(Name = "Email")]
        public string Email{ get; set; }

        [Required(ErrorMessage = "First name is required")]
        [Display(Name = "Firstname")]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Surname is required")]
        [Display(Name = "Surname")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Phone number is required")]
        [Display(Name = "Phonenumber")]
        public string Phonenumber { get; set; }

        [Required(ErrorMessage = "Password field is required")]
        public string Password { get; set; }

        [Required]
        [Compare("Password",ErrorMessage = "Confirm password not the same as password")]        
        public string ConfirmPassword{ get; set; }

        [Required]
        public string Address {get;set;}

        public bool UpdateVateepProfile { get; set; }        
    }

    public class AdminViewModel
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Firstname is required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required")]
        public string LastName { get; set; }
        
        [Required(ErrorMessage = "Email address field is required")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please provide a valid Email address")]
        [Display(Name = "Email")]
        public string Email { get; set; }       
        public string Role { get; set; }
        public int RoleId { get; set; }
        public DateTime DateCreated { get; set; }
    }

    public class ConfirmEmail
    {
        [Required(ErrorMessage = "Please provide User's Id")]
        [Display(Name = "UserId")]
        public string UserId { get; set; }
        [Required(ErrorMessage = "Please supply verification code")]
        [Display(Name = "Code")]
        public string Code { get; set; }
    }

    public class PasswordResetModel
    {
        
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} and at max {1} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
        [Required]
        public string Code { get; set; }      
        [Required]
        public string UserId { get; set; }
    }

    public class UserModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Please provide your firstname")]
        public string FirstName { get; set; }       
        public string MiddleName { get; set; }
        [Required(ErrorMessage ="Please provide your surname")]
        public string LastName { get; set; }
        //[Required(ErrorMessage ="Please provide your gender")]
        public Gender Gender { get; set; }
        //[Required(ErrorMessage ="Please provide your date of birth")]
        //[DataType(DataType.Date, ErrorMessage = "Please provide a valid date of birth")]
        public DateTime DoB { get; set; }
        //[Required(ErrorMessage ="Please provide a title")]
        public ProfileTitle Title { get; set; }

        //[StringLength(160,ErrorMessage = "Bio can not be greater than 160 characters")]
        public string Bio { get; set; }
        //[Required(ErrorMessage = "Please provide your nationality")]
        public string Nationality { get; set; }        
        
        public string EmployerName { get; set; }
        [Required]
        public string Name{get; set;}        
        public string SuperDealer{get;set;}
        [Required]
        public string Address{get;set;}
        
        public string Occupation { get; set; }
        //[Required(ErrorMessage = "Please provide your Residential Address")]
        public string ResidentialAddress { get; set; }
        [Required(ErrorMessage = "Please provide your phone number")]
        [DataType(DataType.PhoneNumber,ErrorMessage = "Please provide a valid Phone number")]
        public string PhoneNumber { get; set; }
        //[Required(ErrorMessage = "Please provide the country code for your phone number")]        
        public string CountryCode { get; set; }
        public string ProfilePicLoc { get; set; }
        //[Required(ErrorMessage = "Please provide you event interests")]
        public string Interests { get; set; }
        public string FaceBook { get; set; }
        public string Twitter { get; set; }
        public string GooglePlus { get; set; }
        public string Instagram { get; set; }
        public string OrganizerDescription { get; set; }
        public string Email { get; set; }
        public string NumberOfEvents { get; set; }
        public bool IsLockedOut {get;set;}
    }

    public class APIKEY
    {
        public string SecretAPIKEY { get; set; }
        public int UserId { get; set; }
        public string DateCreated { get; set; }
    }

    public class AEAARequestBody
    {
        public string FormNo { get; set; }
        public string InvCode { get; set; }
        public int CampaignId { get; set; } 
    }
    
    public class AEAAResponse
    {
        public string D { get; set; }
    }
    public class AEAARegistrationModel
    {
        public string Title { get; set; }
        public string Surname { get; set; }
        public string Firstname { get; set; }
        public string Email { get; set; }
        public string Organisation { get; set; }
        public string Address { get; set; }
        public string Gender { get; set; }
        public string PhoneNo { get; set; }
        public System.DateTime RegDate { get; set; }
        public bool InvitationRequired { get; set; }
        public string FormCode { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public int CountryID { get; set; }
        public Nullable<int> TourActivityID { get; set; }
        public Nullable<int> CampaignID { get; set; }
        public Nullable<decimal> AmountToPay { get; set; }
        public Nullable<bool> HasPaid { get; set; }
        public string InvitationCode { get; set; }
        public Nullable<int> TourID { get; set; }
        public string DoneBy { get; set; }
        public string PassportLocation { get; set; }
        public string Country { get; set; }
        
    }


    public enum MaritalStatus
    {
        Single,
        Married,
        Divorced,
    }
}
