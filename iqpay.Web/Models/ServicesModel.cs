﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using iqpay.Data.Entities.Payment;
using iqpay.Data.Entities.Services;

namespace iqpay.Web.Models
{
    public class ServicesModel
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Please provide vendor name")]
        public string Name { get; set; }
        public string VendorName { get; set; }
        [Required(ErrorMessage = "Please provide vendor id")]
        public long VendorId { get; set; }
        [Required(ErrorMessage = "Please provide service amount")]
        public decimal Amount { get; set; }
        [Required(ErrorMessage = "Please provide service description")]
        public string Description { get; set; }
        public string Status { get; set; }
        [Required(ErrorMessage = "Please provide service type")]
        public string ServiceType { get; set; }
        public int ServiceTypeInt { get; set; }
        [Required(ErrorMessage = "Please choose a commission mode")]
        public CommissionMode CommissionMode { get; set; }
        public string CommissionModeStr { get; set; }
        public bool isFixedPrice { get; set; }
        public string DateCreatedStr { get; set; }
        public int StatusInt { get; set; }
        public bool RequiresVerification { get; set; }
        public List<ProcessorsCode> Codes { get; set; }
        public decimal Commission{get;set;}
        public decimal CommissionCap{get;set;}

        public decimal CustomerCommission{get;set;}
        public decimal CustomerCommissionCap{get;set;}

        public decimal SuperDealerCommission{get;set;}
        public decimal SuperDealerCommissionCap{get;set;}

        public decimal DealerCommission{get;set;}
        public decimal DealerCommissionCap{get;set;}

        public decimal AgentCommission{get;set;}
        public decimal AgentCommissionCap{get;set;}
        public bool IsMeasuredService{get;set;}
        public string UnitType{get;set;}
        public int? DeliveryTypeInt{get;set;}
        public decimal? FlatDeliveryFee {get;set;}
        public decimal? FlatDeliveryFeeCap{get;set;}
        public decimal? FlatDeliveryFeeBeyondCap{get;set;}

        public decimal? FlatCollectionFee {get;set;}
        public decimal? FlatCollectionFeeCap{get;set;}
        public decimal? FlatCollectionFeeBeyondCap{get;set;}

        public bool IsSharedProduct{get;set;}
        public int? MaxOrders{get;set;}
        public DateTime? LastOrderDate{get;set;} 
    }

    public class ProcessorsCode
    {        
        public long ProcessorId { get; set; }
        public string ProcessorName { get; set; }
        public string ProcessorCode { get; set; }        
        public string ProcessorServiceCode { get; set; }
        [Required]
        public long ServiceId { get; set; }
    }

    public class VendingReportModel
    {
        public long Id { get; set; }
        public decimal Amount { get; set; }
        public string CustomerNumber { get; set; }
        public string VendingCode { get; set; }
        public string TransactionDate{get;set;}
        public string Status { get; set; }
        public string Service { get; set; }
        public string AgentName { get; set; }
        public string DealerCode { get; set; }
        public decimal AgentCommission{get;set;}
        public decimal DealerCommission{get;set;}
        public decimal SuperDealerCommission{get;set;}
        public decimal IQPAYProfit{get;set;}
        public decimal? WalletBalance{get;set;}
        public string Unit{get;set;}
        public string PhoneNumber{get;set;}
    }


    public class VendingModel
    {
        public long Id { get; set; }
        [Required]
        public long ServiceId { get; set; }
        [Required]
        public decimal Amount { get; set; }
        [Required]       
        public string CustomerNumber { get; set; }
        public  int? NumberOfPins {get;set;}
        public string VendingCode { get; set; }
        public string TransactionDate{get;set;}
        public string Status { get; set; }
        public string Service { get; set; }
        public string DeliveryAddressLine1{get;set;}
        public string DeliveryAddressLine2{get;set;}
        public string CollectionPoint{get;set;}
        public string Unit{get;set;}
        public string AgentName { get; set; }        
        public string DealerCode { get; set; }
        public string DealerAddress{get;set;}
        public string VendorLogo { get; set; }
        public string AccountName { get; set; }
        public string AccountAddress{get;set;}
        public string Type { get; set; }
        public string Pin { get; set; }
        public string Quantity { get; set; }
        public decimal AgentCommission{get;set;}
        public decimal DealerCommission{get;set;}
        public decimal SuperDealerCommission{get;set;}
        public decimal IQPAYProfit{get;set;}
        public decimal ProcessorBalance{get;set;}
        public decimal? WalletBalance{get;set;}
        public List<EPin> Pins{get;set;}
        public string UnitType{get;set;}
        public DeliveryOption? DeliveryOption{get;set;}
        public int StatusInt{get;set;}
        public decimal? DeliveryFee{get;set;}
        public string State{get;set;}
        public string LGA {get;set;} 
        public bool? IsSharedProduct{get;set;}
        public DateTime? DeliveryDate{get;set;} 
    }

    public class EPin
    {
        public string SerialNumber{get;set;}
        public string Pin{get;set;}
    }

    public class DashboardModel
    {
        public long TransactionCount{get;set;}
        public decimal WalletBalance{get;set;}
        public decimal CommissionBalance{get;set;}
        public decimal TodayIncome{get;set;}
        public int SuperDealers{get;set;}
        public int Dealers{get;set;}
        public int Agents{get;set;}
    }

    public class ProcessorModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public decimal Balance { get; set; }
        public string Status { get; set; }
        public int StatusInt { get; set; }
        public string DateCreatedStr { get; set; }
    }


    public class VendorsModel
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Please provide the vendor name")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Please choose the vendor status")]
        public string Status { get; set; }
        [Required(ErrorMessage = "Please provide a description for the vendor")]
        public string Description { get; set; }
        public DateTime DateCreated { get; set; }
        public string DateCreatedStr { get; set; }
        public decimal Balance { get; set; }
        public int StatusInt { get; set; }
        public string LogoSrc{get;set;}
        public bool IsCommissionAhead{get;set;}

        public decimal Commission{get;set;}
        public decimal CommissionCap{get;set;}

        public decimal CustomerCommission{get;set;}
        public decimal CustomerCommissionCap{get;set;}

        public decimal SuperDealerCommission{get;set;}
        public decimal SuperDealerCommissionCap{get;set;}

        public decimal DealerCommission{get;set;}
        public decimal DealerCommissionCap{get;set;}

        public decimal AgentCommission{get;set;}
        public decimal AgentCommissionCap{get;set;}

        public bool EnableHouseHoldBonus{get;set;}
        public string HouseHoldBonusDates{get;set;}
        public decimal HouseHoldBonusPercentage{get;set;}
        public decimal HouseHoldBonusCap{get;set;}
    }

    public class DealerCommissionModel
    {
        public string Service{get;set;}
        
        public long? ServiceId{get;set;}
        public string Vendor{get;set;}
        public string CommissionMode{get;set;}

        public long? VendorId{get;set;}
        [Required]
        public decimal? Commission{get;set;}   
        public decimal? Cap{get;set;}    
        [Required]
        public long DealerId{get;set;}
    }

    public class VendorCommissionModel
    {
        [Required]
        public decimal Commission{get;set;}
        [Required]
        public decimal CommissionCap{get;set;}
        [Required]
        public decimal CustomerCommission{get;set;}
        [Required]
        public decimal CustomerCommissionCap{get;set;}
        [Required]
        public decimal SuperDealerCommission{get;set;}
        [Required]
        public decimal SuperDealerCommissionCap{get;set;}
        [Required]
        public decimal DealerCommission{get;set;}
        [Required]
        public decimal DealerCommissionCap{get;set;}
        [Required]
        public decimal AgentCommission{get;set;}
        [Required]
        public decimal AgentCommissionCap{get;set;}

    }

    public class ServiceCommissionModel
    {
        [Required]
        public decimal Commission{get;set;}
        [Required]
        public decimal CommissionCap{get;set;}
        [Required]
        public decimal CustomerCommission{get;set;}
        [Required]
        public decimal CustomerCommissionCap{get;set;}
        [Required]
        public decimal SuperDealerCommission{get;set;}
        [Required]
        public decimal SuperDealerCommissionCap{get;set;}
        [Required]
        public decimal DealerCommission{get;set;}
        [Required]
        public decimal DealerCommissionCap{get;set;}
        [Required]
        public decimal AgentCommission{get;set;}
        [Required]
        public decimal AgentCommissionCap{get;set;}

    }

    public class DealerCapModel
    {
        public string Service{get;set;}
        
        public long? ServiceId{get;set;}
        public string Vendor{get;set;}
        
        public long? VendorId{get;set;}
        
        public decimal Cap{get;set;}       
        [Required]
        public long DealerId{get;set;}
    }

    public class VendorServiceModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<ServicesModel> Serivces { get; set; }
        public string VendorLogo{get;set;}
    }

    public class DealerServiceModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public List<ServicesModel> Serivces { get; set; }
        public string VendorLogo{get;set;}
        public bool IsActive{get;set;}
    }


}
