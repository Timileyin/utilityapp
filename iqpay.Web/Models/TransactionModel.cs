﻿using System;
using System.ComponentModel.DataAnnotations;
using iqpay.Data.Entities.Payment;

namespace iqpay.Web.Models
{
    public class TransactionModel
    {
        public long Id { get; set; }
        public DateTime DateCreated { get; set; }
        public BillStatus BillStatus { get; set; }
        public string BillRefenceNo { get; set; }
        public string DatePaid { get; set; }
        public decimal Amount { get; set; } = 0.00M;
        public string Name { get; set; }
        public string Email { get; set; }
        public string PublicKey { get; set; }
        public decimal TechFee { get; set; }
        public decimal TotalAmount { get; set; }
        public string WalletName { get; set; }
        public string PhoneNumber { get; set; }
        public string BillType { get; set; }
        public string BillStatusStr { get; set; }

    }

    public class TransactionCompleted
    {
        public string Event { get; set; }
        public TransactionData Data { get; set; }
    }

    public class KoraPayTransactionResponse<T>
    {
        public T Data{ get; set; }
        public bool Status{ get; set; }
        public string Message { get; set; }
    }

    public class KoraPayTransactionInfo
    {
        public long Id { get; set; }
        public string Type { get; set; }
        public string Currentcy { get; set; }
        public string Amount { get; set; }
        public string Fee { get; set; }
        public string Reference { get; set; }
        public string Naration { get; set; }
        public string Status { get; set; }
        public string Meta { get; set; }
        public DateTime Created_at { get; set; }
        public UserModel Customer { get; set; }
    }

    public class TransactionData
    {
        public string Reference { get; set; }
        public string Currency { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public string Status { get; set; }
    }

    
    public class CompleteVendingModel
    {
        [Required]
        public string VendingCode { get; set; }
    }
}
