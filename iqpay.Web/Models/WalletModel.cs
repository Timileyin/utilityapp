﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using iqpay.Data.Entities.Payment;

namespace iqpay.Web.Models
{
    public class WalletModel
    {
        public  long Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string WalletType { get; set; }
        public decimal Balance { get; set; }
        public string Owner { get; set; }
        [Required]
        [StringLength(250, MinimumLength = 3)]
        public string WalletName { get; set; }
        [Required]
        public string WalletDescription { get; set; }
        [Required]
        [EnumDataType(typeof(CommissionMode), ErrorMessage = "Please choose a valid mode")]
        public CommissionMode CommissionMode { get; set; }
        public string VendorLogo{get;set;}
        public string Mode { get; set; }
        
    }

    public class CommissionAheadReport
    {
        public decimal Amount{get;set;}
        public decimal Profit{get;set;}
        public string Service{get;set;}
        public string TransactionDate{get;set;}   
        public string DealerCode{get;set;}
        public string DealerName{get;set;}
    }

    public class FundingRequest
    {
        public long Id { get; set; }
        [Required]
        public long WalletId { get; set; }
        [Required]
        public decimal Amount { get; set; }
        public decimal AmountCreditted{get;set;}
        public string BankName { get; set; }
        public string AccountNumber { get; set; }
        public string TransactionRef { get; set; }
        public bool IsAuto { get; set; }
        public string Description { get; set; }
        public long? BaseWalletId { get; set; }
        public bool IsIntraWallet { get; set; }
        public long TransactionId { get; set; }
        public string DateCreated { get; set; }
        public string TransactionDate { get; set; }
        public string DepositorName { get; set; }
        public string WalletName { get; set; }
        public string WalletUser { get; set; }
        public string Status { get; set; }
        public decimal? DebittedWalletBalance{get;set;}
        public decimal? CredittedWalletBalance{get;set;}
    }


}
