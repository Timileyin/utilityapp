﻿using System;
using System.ComponentModel;

namespace iqpay.Web.Models
{
    public enum ServiceEnum
    {
        //[ServiceAttribute("DSTV ACCESS","","", )]
        DSTV_ACCESS
    }

    
    public enum ProcessorEnums
    {
        [Description("CPCN")]
        CAPRICON,
        [Description("FETS")]
        FETS,
        [Description("VAT")]
        VATEBRA,

    }
}
