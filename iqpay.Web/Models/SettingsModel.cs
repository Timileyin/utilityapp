using System;

namespace iqpay.Web.Models
{
    public class SettingsModel
    {
         public string HouseHoldPowerBonus { get; set; }
        public string HouseHoldBonusDates{get;set;}     
        public bool EnableHouseHoldBonus{get;set;}  
        public DateTime LastUpdateDate{get;set;}
    }
}