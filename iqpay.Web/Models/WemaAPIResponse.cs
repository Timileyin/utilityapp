using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace iqpay.Web.Models
{
    public class WemaAPIResponse
    {
        public string Accountname;
        public string Status;
        public string Status_desc;
        public HttpStatusCode StatusCode;
        public bool Error;

        public WemaAPIResponse(string accountName, HttpStatusCode statusCode = HttpStatusCode.OK, string status = "", string statusDesc = "", bool error = false)
        {
            Accountname = accountName;
            Status = status;
            Status_desc = statusDesc;
            StatusCode = statusCode;
            Error = error;
        }        

        public WemaAPIResponse()
        {

        }
    }
}
