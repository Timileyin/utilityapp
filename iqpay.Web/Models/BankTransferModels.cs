using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace iqpay.Web.Models
{
    public class NameEnquiryModel
    {
        [Required]                
        public string BankCode{ get; set; }     
        [Required] 
        public string BankAccountNumber{ get; set; }
        public string AccountName{ get; set; }
    }  

    public class BankDataModel
    {
        public string BankCode{ get; set; }
        public string BankName{ get; set; }
    }

    public class BankTransferModel
    {
        public long Id{get;set;}
        public string Sender{ get;set; }
        [Required]
        public string BankAccountNumber{ get;set; }
        [Required]
        public decimal Amount{ get;set; }
        [Required]
        public string BankName{get;set;}
        public DateTime TransferDate{get;set;}
        [Required]
        public string AccountName{get;set;}
        [Required]
        public string BankCode{get;set;}
        [Required]
        public string Narration{get;set;}
        public string TxnRef{get;set;}
        public string Status{get;set;}
        public decimal WalletBalance{get;set;}
        public string DealerCode {get;set;}
        public string BVN{get;set;}
        public string ClientId{get;set;}
        public string ToSession{get;set;}
        public string SavingsId{get;set;}

    }
}   