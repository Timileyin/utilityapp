using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace iqpay.Web.Models
{
    public class ProcessorVending
    {
        public long ProcessorId{get;set;}
        public string ProcessorName{get;set;}
        public long TransactionCounts{get;set;}
        public decimal TransactionValue{get;set;}
        public decimal TotalProfits{get;set;}
        public long FailedCounts{get;set;}
        public long SuccessfulCount{get;set;}
        public long CancelledCounts{get;set;}
    }  

    public class ProcessorBalance
    {
        public long ProcessorId{get;set;}
        public string ProcessorName{get;set;}
        public decimal Balance{get;set;}
    }
}