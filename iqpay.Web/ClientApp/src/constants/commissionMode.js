﻿export const commissionMode = [
    { key: 1, value: "Transaction based" },
    { key: 2, value: "Commission ahead" }
]

export const deliveryTypes = [
    { key: 1, value: "Collection Point" },
    { key: 2, value: "Delivery" },
    { key: 3, value: "Both" }  
];

export const deliveryOptions = [
    { key: 1, value: "Collection Point" },
    { key: 2, value: "Delivery" }
];

export const fundingMethods = [
    {key : 2, value: "Manual Funding"}
]

export const status = [
    { key:0, value: "Deactivated"},
    {key: 1, value: "Activated"}
]

export const serviceTypes = [
    { key: 1, value: "Electricity", module:"electricity" ,icon: "fas fa-lightbulb" },
    { key: 2, value: "Airtime Topup", module:"mobileTopUp", icon: "fas fa-mobile-alt" },
    //{ key: 3, value: "Educational", module: "educational", icon: "fas fa-book" },
    { key: 4, value: "Cable TV", module: "cableTV", icon: "fas fa-tv" },
    { key: 5, value: "Internet", module: "internet", icon: "fas fa-globe"},    
    //{ key: 6, value: "Lotto", module: "lotto", icon: "fas fa-lotto"},
    { key: 7, value: "E-Pins", module: "ePins", icon: "fas fa-pin"},
    { key: 8, value: "Affordable Farms", module: "food", icon: "fas fa-food"}
]
    