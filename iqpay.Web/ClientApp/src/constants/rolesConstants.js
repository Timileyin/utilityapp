export const roles = {
    SUPERADMIN: "SYSTEM ADMIN",
    REGULAR_CUSTOMER: "REGULAR CUSTOMER",
    DEALER: "DEALER",
    AGENT: "AGENT",
    SUPERDEALER: "SUPER DEALER"
};

export const userTypes = [
    "DEALER",
    "AGENT",
    "SUPER DEALER"
];