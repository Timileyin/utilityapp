﻿export const alertConstants = {
    SUCCESS: 'ALERT_SUCCESS',
    ERROR: 'ALERT_ERROR',
    CLEAR: 'ALERT_CLEAR',

    DANGER_TOASTR: 'alert-danger',
    SUCCESS_TOASTR: 'alert-success',
    WARNING_TOASTR: 'alert-warning'
};