export const permissions = [
    "Home", "Vendors", "Wallets", "Funding Request", "Transfer Money","Partnership Requests", "Super Dealers", "Dealers","Agents","Household Users",
    "Virtual Accounts","Virtual Transactions","Transactions","Commission Ahead Report","Wallet Funding History","Master Wallets","Processor Vending",
    "Wallet Balances","Administrators","Wallet Balances By Vendors"
];