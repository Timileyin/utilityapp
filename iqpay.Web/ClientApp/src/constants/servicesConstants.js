﻿export const servicesConstant = {
    SAVE_SERVICES_REQUEST: "SAVE_SERVICES_REQUEST",
    SAVE_SERVICES_SAVE_SUCCESS: "SAVE_SERVICES_SAVE_SUCCESS",
    SAVE_SERVICES_SAVE_FAILURE: "SAVE_SERVICES_SAVE_FAILURE",
    FETCH_SERVICE_FAILED: "FETCH_SERVICE_FAILED",
    FETCH_SERVICE_SUCCESS: "FETCH_SERVICE_SUCCESS",
    FETCH_SERVICE_REQUEST: "FETCH_SERVICE_REQUEST",
    FETCH_SERVICE_INFO: "FETCH_SERVICE_INFO",
    FETCH_SERVICE_CODE: "FETCH_SERVICE_CODE",
    FETCH_SERVICE_CODE_FAILED: "FETCH_SERVICE_CODE_FAILED",
    SAVE_SERVICE_CODES: "SAVE_SERVICE_CODES",
    SAVE_SERVICE_CODE_SUCCESS: "SAVE_SERVICE_CODE_SUCCESS",
    SAVE_SERVICE_CODE_FAILURE: "SAVE_SERVICE_CODE_FAILURE",
    VENDING_SUCCESSFUL: "VENDING_SUCCESSFUL",
    VENDING_FAILED: "VENDING_FAILED",
    FETCH_VENDING_SUCCESS: "FETCH_VENDING_SUCCESS",
    FETCH_VENDING_FAILED: "FETCH_VENDING_FAILED",
    COMPLETE_VENDING_FAILED: "COMPLETE_VENDING_FAILED",
    COMPLETE_VENDING_SUCCESS: "COMPLETE_VENDING_SUCCESS",
    FETCH_SERVICE_COMMISSION_SUCCESS: "FETCH_SERVICE_COMMISSION_SUCCESS",
    FETCH_SERVICE_COMMISSION_FAILURE: "FETCH_SERVICE_COMMISSION_SUCCESS"
};