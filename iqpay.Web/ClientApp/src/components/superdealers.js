﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { superdealerActions } from '../actions/superdealerAction';
import { connect } from 'react-redux';
import CreateEditDealer from './create-edit-dealer';
import CommissionRate from './commission-rates';
import {dealersActions} from '../actions/dealersAction';
import { superdealerConstants } from '../constants/dealerConstants';
import VendorCheckList from './vendor-checklist'

import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";

import jQuery from 'jquery';
class SuperDealers extends React.Component {
    modalpopup = true;
    constructor(props) {
        super(props);

        this.state = {

            pageTitle: 'Manage Superdealers',
            superdealers: [],
            superdealerInfo: null,
            modalPopup: false,
            commissionRates:null,
            checkListItems: null,
            dealerId: null
        };


    }

    toggleActivate = (id, status) => {
        if (window.confirm("Are you sure you want to complete this task?")) {
            const { dispatch } = this.props;
            dispatch(superdealerActions.toggleActivate(id, status));
        }
    }

    saveDealer = (dealerInfo) => {
        const { dispatch } = this.props;
        dispatch(superdealerActions.saveDealer(dealerInfo));
        //this.setState({ vendorInfo: null });
    }

    updateCheckListItems(checkListItems){
        if(window.confirm("Are you sure you want to update this vendor checklist")){
            const {dispatch} = this.props;
            dispatch(dealersActions.updateCheckListItems(this.state.dealerId, checkListItems));
        }                
    }

    showServiceChecklist = (dealerId) => {
        const{dispatch} = this.props;
        this.setState({dealerId: dealerId});
        dispatch(dealersActions.fetchServiceChecklist(dealerId))
    }

    closeCheckListModal = () => {
        const{dispatch} = this.props;
        dispatch(dealersActions.resetserviceChecklist());
        this.setState({checkListItems: null});
        this.setState({checkListItems: null});
    }


    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if (this.props.superdealer.dealers !== undefined) {
                this.setState({ superdealers: this.props.superdealer.dealers });
            }

            if (this.props.superdealer.dealerInfo !== undefined) {
                this.setState({ superdealerInfo: this.props.superdealer.dealerInfo })
            }

            if(this.props.dealer.commissionRates !== undefined){
                this.setState({commissionRates: this.props.dealer.commissionRates});
            }
            if(this.props.dealer.dealerChecklist !== undefined)
            {
                this.setState({checkListItems: this.props.dealer.dealerChecklist});
            }

            jQuery(document).ready(function () 
            {
                jQuery('#tableData').DataTable({
                    dom: 'Bfrtip',
                    retrieve: true,
                    buttons: [
                        'csv', 'excel', 'pdf'
                    ]
                });
            });
        }
    }

    editDealer = (dealerId) => {
        const { dispatch } = this.props;
        dispatch(superdealerActions.fetchDealerDetails(dealerId));
    }

    addDealer = () => {
        this.setState({ superdealerInfo: {} });
    }

    commissionRate = (dealerId) =>{
        const {dispatch} = this.props;
        dispatch(dealersActions.fetchCommissionRate(dealerId));
    }

    closeModal = () => {        
        this.setState({ superdealerInfo: null });
    }

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document

        const { dispatch } = this.props;
        dispatch(superdealerActions.fetchDealers());

    }

    closecommissionModal = () => {
        const{dispatch} = this.props;
        dispatch(dealersActions.resetDealerCommissionRate());
        this.setState({ commissionRates: null});
    }

    updateSerivceCommissison = (serviceId, vendorId, dealerId, rate) => {
        if(window.confirm("Are you sure you want to proceed with these changes")){
            const{dispatch} = this.props;
            dispatch(dealersActions.updateDealerCommission(serviceId,vendorId, dealerId, rate)); 
        }
        
    }

    updateServiceCommissionCap = (serviceId, vendorId, dealerId, cap) => {
        if(window.confirm("Are you sure you want to proceed with these changes")){
            const{dispatch} = this.props;
            dispatch(dealersActions.updateDealerCommissionCap(serviceId,vendorId, dealerId, cap)); 
        }        
    }

    render() {
        var superdealers = this.state.superdealers;
        let dealersTable = [];
        if (superdealers.length > 0) {
            superdealers.map((v) => {
                dealersTable.push(
                    <tr>
                        <td className="align-middle">{v.name}</td>
                        <td className="align-middle">{v.phonenumber}</td>
                        <td className="align-middle">{v.email}</td>
                        <td className="align-middle">{v.dateCreated}</td>
                        <td>
                            <Button color="info" onClick={() => { this.editDealer(v.id) }} className="pull-right" size="sm"><i class="fas fa-edit"></i> Edit</Button>
                            <Button color="info" onClick={()=>{this.showServiceChecklist(v.id)}} className="pull-right" size="sm"><i class="fas fa-edit"></i> Service Checklist</Button>
                            <Button color="info" onClick={() => { this.commissionRate(v.id) }} className="pull-right" size="sm"><i class="fas fa-cogs"></i> View Commission Rates</Button>
                            &nbsp; <Button color={v.isLockedOut ? "info" : "danger"} onClick={() => { this.toggleActivate(v.id, v.isLockedOut) }} className="pull-right" size="sm"><i class={v.isLockedOut ? "fas fa-check" : "fas fa-lock"}></i> {v.isLockedOut ? "Activate" : "Deactivate"}</Button>
                        </td>

                    </tr>);
            });
        } else {
            dealersTable = <tr><td colspan='5'>Superdealers have not been setup</td></tr>;
        }

        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                        <Row>
                                            <Col>
                                                <Button color="" onClick={this.addDealer} className="pull-right" size="sm"><i class="fas fa-plus"></i> Create Superdealer</Button>
                                                <Table id="tableData" border striped hover size="md">
                                                    <thead className="thead-light"> 
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Phone number</th>
                                                            <th>Email</th>
                                                            <th>Date Created</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {dealersTable}
                                                    </tbody>
                                                </Table>
                                            </Col>
                                            <VendorCheckList checkListItems={this.state.checkListItems} onCancel={this.closeCheckListModal} onSave ={(checkListItems)=>this.updateCheckListItems(checkListItems)} />
                                            <CreateEditDealer dealerType="Superdealer" onSave={this.saveDealer} onCancel={this.closeModal} dealerInfo={this.state.superdealerInfo} />
                                            <CommissionRate updateServiceCommissionCap= {(serviceId, vendorId, dealerId, cap) => this.updateServiceCommissionCap(serviceId, vendorId, dealerId, cap)} updateServiceCommission={(serviceId, vendorId, dealerId, rate) => this.updateSerivceCommissison(serviceId, vendorId, dealerId, rate)} rates={this.state.commissionRates} onCancel={this.closecommissionModal}/>
                                        </Row>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, superdealer, dealer } = state;

    return {
        alert, superdealer, dealer
    };
}

const connectedDealersPage = connect(mapStateToProps)(SuperDealers);
export { connectedDealersPage as SuperDealers };