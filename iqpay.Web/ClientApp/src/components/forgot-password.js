import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../actions/userAction';
import { Link, Redirect } from 'react-router-dom';

class ForgotPassword extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: this.props.isVisible,
            username: ""
        };
    }

    handleCreateChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        if (name === 'username') {
            theState.username = value;
        }
        this.setState(theState);
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ isVisible: this.props.isVisible })
        }
    }

    sendPasswordResetLink = () => {
        const {dispatch} = this.props;
        var username = this.state.username;
        if (username != "") {
            dispatch(userActions.forgotPassword(username));
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document
    }

    render() {
        return (
            <div id="content">
                <section className="page-header page-header-text-light bg-secondary">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-md-8">
                                <h1>Login & Signup</h1>
                            </div>
                            <div className="col-md-4">
                                <ul className="breadcrumb justify-content-start justify-content-md-end mb-0">
                                    <li><Link to="/">Home</Link></li>
                                    <li className="active">Forgot Password</li>
                                </ul>
                            </div>
                        </div>
                    </div>
            </section>
            <div className="container">
                <div id="login-signup-page" className="bg-light shadow-md rounded mx-auto p-4">

                    <div className="form-group">
                        <label>Please provide your valid username/dealercode a mail would be sent to your email to reset your password</label>
                        <input type="email" className="form-control" name="username" onChange={(e)=>this.handleCreateChange(e) } id="loginMobile" required placeholder="Email Address" />
                    </div>                  
                    <button className="btn btn-primary btn-block" onClick={this.sendPasswordResetLink}>Send Password Reset Code</button>
                </div>
            </div>
        </div>
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn, signinUp, alert } = state;   
    return {
        loggingIn,
        signinUp,
        alert
    };
}

const connectedForgotPassword = connect(mapStateToProps)(ForgotPassword);
export { connectedForgotPassword as ForgotPassword }; 
