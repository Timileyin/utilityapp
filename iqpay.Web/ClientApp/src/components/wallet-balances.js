import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Badge } from 'reactstrap';
import { superdealerActions } from '../actions/superdealerAction';
import { connect } from 'react-redux';
import { userActions } from '../actions/userAction';

import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";
import numeral from 'numeral';
import moment from 'moment';

import $ from 'jquery';


class WalletBalances extends React.Component {
    modalpopup = true;
    constructor(props) {
        super(props);
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));
        this.state = {
            role: currentUser.role,
            pageTitle: 'Wallet Balances',
            walletBalances: [],
            totalBalance: 0.00            
        };
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if(this.props.authentication.walletBalances != undefined){
                this.setState({walletBalances: this.props.authentication.walletBalances });                                    
            }

            $(document).ready(function () 
            {
                $('#tableData').DataTable({
                    dom: 'Bfrtip',
                    "bSort" : false,
                    retrieve: true,
                    buttons: [
                        'csv', 'excel', 'pdf'
                    ]
                });
            });            
        }
    }

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document        
        const { dispatch } = this.props;
        dispatch(userActions.fetchWalletBalances());        
    }

    render() {
        var balances = this.state.walletBalances;
        let balancesTable = [];  
        let totalAmount = 0.00;      
        if (balances.length > 0) {            
            balances.map((v) => {
                totalAmount += v.totalBalance;
                balancesTable.push(
                    <tr key={v.dealerId}> 
                        <td className="align-middle">{v.dealerCode}</td>                      
                        <td className="align-middle">{v.dealerName}</td> 
                        <td className="align-middle">{v.walletType}</td> 
                        <td className="align-middle">{numeral(v.totalBalance.toFixed(2)).format("0,00.00")}</td>                     
                    </tr>                    
                );
                
            });
        } else {
            balancesTable = <tr><td colSpan='8'>No wallet found</td></tr>;
        }

        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                        <Row>
                                            <Col md={12}>
                                            <div className="alert alert-info">Please refresh this page to view most recent report as this is not a live feed. And balances could have changed. Last retrieval time <Badge color="info">{moment(new Date()).format("DD ddd MMM yyyy, hh:mm A")}</Badge></div>
                                            <span className="text-right">Total: {numeral(totalAmount).format("0,00.00")}</span> 
                                            <Table id="tableData" border striped hover size="md">
                                                    <thead className="thead-light"> 
                                                        <tr>
                                                            <th>Dealer code</th>
                                                            <th>Name </th> 
                                                            <th>Wallet Type</th>                                                            
                                                            <th>Total Wallet Balance</th>                                                           
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {balancesTable}
                                                    </tbody>
                                                </Table>
                                            </Col>                                           
                                        </Row>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, authentication } = state;

    return {
        alert, authentication
    };
}

const connectedWalletBalancesPage = connect(mapStateToProps)(WalletBalances);
export { connectedWalletBalancesPage as WalletBalances };