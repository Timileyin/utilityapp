﻿import React from 'react';
import $ from 'jquery';
import { connect } from 'react-redux';
import { servicesActions } from '../actions/servicesAction';
import { vendorService } from '../services/vendorService';

window.$ = window.jQuery = $;
require('easy-responsive-tabs');

class CableTV extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: this.props.isVisible,
            serviceTypeKey: this.props.serviceTypeKey,
            vendors: this.props.vendors,
            vendorServices: [],
            service: {},
            amount: null,
            customerNumber: null
        };
    }

    recharge = () => {
        if (window.confirm("Are you sure you want to continue?")) {
            const { dispatch } = this.props;
            dispatch(servicesActions.initiateVending(this.state.service.id, this.state.customerNumber, this.state.amount));
        }
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change

        if (this.props !== prevprops) {

            this.setState({ isVisible: this.props.isVisible, serviceTypeKey: this.props.serviceTypeKey, vendors: this.props.vendors });
        }


    }

    handleCreateChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        if (name === 'vendor') {
            theState.vendorServices = [];
            var vendor = theState.vendors.find(x => x.id == value);
            if (vendor != undefined) {
                var vendorServices = vendor.serivces.filter(x => x.serviceTypeInt == this.state.serviceTypeKey);
                theState.vendorServices = vendorServices;
            }

        }

        if (name === 'customerNumber') {
            theState.customerNumber = value
        }


        if (name === 'service') {
            theState.service = {};
            var selectedService = theState.vendorServices.find(x => x.id == value);
            if (selectedService != undefined) {
                if (selectedService.isFixedPrice) {
                    theState.amount = selectedService.amount.toFixed(2);
                    theState.service = selectedService
                }
            }

        }

        if (name === 'amount') {
            theState.amount = value;
        }

        this.setState(theState);
    }


    initiateVendService = () => {

        if (window.confirm("Are you sure you want to proceed?")) {
            const { dispatch } = this.props;
            console.log(this.state);
            dispatch(servicesActions.initiateVending(this.state.service.id, this.state.customerNumber, this.state.amount));
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document


    }
    render() {
        var vendorsOptions = [];
        this.state.vendors.map(s => {
            vendorsOptions.push(<option value={s.id}>{s.name}</option>);
        });

        var serviceOptions = [];
        this.state.vendorServices.map(s => {
            serviceOptions.push(<option value={s.id}>{s.name}</option>);
        });           

        return (

            <div style={!this.state.isVisible ? { display: 'none' } : null}>
                <h2 className="text-6 mb-4">Pay your CableTV Bill</h2>
                    <div className="form-group">
                        <label htmlFor="electricityOperator">Your CableTV Provider</label>
                        <select name="vendor" onChange={this.handleCreateChange} className="custom-select" id="cableTVOperator" required="">
                            <option value="">Select Your Operator </option>
                            {vendorsOptions}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="electricityyourState">Subscription Type</label>
                    <select onChange={this.handleCreateChange} name="service" className="custom-select" id="service" required="">
                            <option value="">Select Your Subscription Type</option>
                            {serviceOptions}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="serviceNumber">Your Customer Number/Account Number</label>
                    <input type="text" className="form-control" onChange={this.handleCreateChange} data-bv-field="number" name="customerNumber" id="customerNumber" required placeholder="Enter Service Number" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="electricityAmount">Amount</label>
                        <div className="input-group">
                        <div className="input-group-prepend"> <span className="input-group-text">&#8358;</span> </div>
                        <input className="form-control" onChange={this.handleCreateChange} readOnly={this.state.service.isFixedPrice} id="amount" value={this.state.amount} name="amount" placeholder="Enter Amount" required type="text" />
                        </div>
                    </div>
                    <button onClick={this.recharge} className="btn btn-primary btn-block">Continue</button>
                
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, serviceManagement } = state;

    return {
        alert, serviceManagement
    };
}

const connectedElectricityBillsPage = connect(mapStateToProps)(CableTV);
export { connectedElectricityBillsPage as CableTV };
