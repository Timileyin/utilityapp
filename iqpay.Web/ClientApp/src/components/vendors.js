﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col,Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { vendorActions } from '../actions/vendorAction';
import { connect } from 'react-redux';
import CreateEditVendor from './create-edit-vendor';
import { vendorConstants } from '../constants/vendorConstants';

import {alertActions} from '../actions/alertActions'; 

import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";
import ManageVendorCommission from './vendor-comm';

import jQuery from 'jquery';


class Vendors extends React.Component {
    modalpopup = true
    constructor(props) {
        super(props);

        this.state = {
           
            pageTitle: 'Manage Vendors',
            vendors: [],
            vendorInfo: null,
            vendorComm: null,
            modalPopup: false
        };        
    }

    saveVendor = (vendorInfo) => {
        const { dispatch } = this.props;
        dispatch(vendorActions.saveVendor(vendorInfo));
        //this.setState({ vendorInfo: null });
    }
    

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if (this.props.vendor.vendors !== undefined) {
                this.setState({ vendors: this.props.vendor.vendors });                
            }

            if (this.props.vendor.vendorInfo !== undefined) {
                this.setState({ vendorInfo: this.props.vendor.vendorInfo})
            }

            if(this.props.vendor.vendorComm !== undefined){
                this.setState({ vendorComm: this.props.vendor.vendorComm})
            }
            
            if(this.state.vendors.length > 0){
                jQuery(document).ready(function () 
                {
                    jQuery('#tableData').DataTable();
                });
            }
            
        }
    }

    editVendor = (vendorId) => {
        const { dispatch } = this.props;
        dispatch(vendorActions.fetchVendor(vendorId));
    }

    manageComm = (vendorId) =>{
        const{dispatch} = this.props;
        dispatch(vendorActions.fetchVendorComm(vendorId));
    }

    saveVendorComm = (vendorInfo) =>{
        const { dispatch } = this.props;
        dispatch(vendorActions.saveVendorComm(vendorInfo));
    }

    uploadVerndorLogo = (e) => {
        const {dispatch} = this.props;
        var file = e.target.files[0];
        console.log(file);
        var name = file.name;
        var nameSplits = name.split(".");
        var fileExt = nameSplits[nameSplits.length - 1]?.toLowerCase();
        if(fileExt == "jpg" || fileExt == "png" || fileExt == "jpeg"||fileExt == "gif"){
            var reader = new FileReader();
            reader.onload = function (ev) {
                jQuery('#logoImg').attr('src', ev.target.result);
            }
            reader.readAsDataURL(file);                
            dispatch(vendorActions.uploadLogoImage(this.state.vendorInfo.id, file));
        }else{
            dispatch(alertActions.error("Please upload a valid image","File upload failed"));
        }
    }

    addVendor = () => {
        this.setState({ vendorInfo: {} });
    }

    closeModal = () => {
        this.setState({ vendorInfo: null });
    }

    closeCommModal = () => {
        this.setState({vendorComm: null});
    }

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document

        const { dispatch } = this.props;
        dispatch(vendorActions.fetchVendors());

    }
    render() {
        var vendors = this.state.vendors;
        let vendorTable = [];
        if (vendors.length > 0) {
            vendors.map((v) => {
                vendorTable.push(
                    <tr>
                        <td className="align-middle">
                            <Link to={"vendors/"+v.id+"/services"}>
                                <img src={v.logoSrc} className="img-thumbnail d-inline-flex mr-1" height="40px" width="40px" />
                                <span class="text-1 d-inline-flex">{v.name}</span>
                            </Link>
                        </td>
                        <td className="align-middle">{v.dateCreatedStr}</td>
                        <td className="align-middle">{v.description}</td>
                        <td class="align-middle text-center">
                            {v.status == "Activated" ? <i class="fas fa-check-circle text-4 text-success" data-toggle="tooltip" data-original-title="Vendor is active"></i> : <i class="fas fa-times-circle text-4 text-danger" data-toggle="tooltip" data-original-title="Vendor is deactivated"></i>}
                        </td>
                        <td className="align-middle">
                            <Button color="info" onClick={() => { this.editVendor(v.id) }} className="pull-right" size="sm"><i class="fas fa-edit"></i> Edit</Button>
                            <Button color="info" hidden={!v.isCommissionAhead} onClick={() => { this.manageComm(v.id) }} className="pull-right" size="sm"><i class="fas fa-money-check"></i> Manage Comm.</Button>
                        </td>

                    </tr>);
            });
        } else {
            vendorTable = <tr><td colspan='5'>Vendors have not been setup</td></tr>;
        }

        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                    <h4 class="mb-3">Vendors<Button color="" onClick={this.addVendor} className="pull-right" size="sm"><i class="fas fa-plus"></i> Create Vendor</Button></h4>
                                        <Row>
                                            <Col>                                                
                                                <div className="table-responsive-md">
                                                    <Table id="tableData" className="table table-hover border" striped hover size="md">
                                                        <thead className="thead-light">
                                                            <tr>
                                                                <th>Name</th>
                                                                <th>Date Created</th>
                                                                <th>Description</th>
                                                                <th className="text-center">Status</th>
                                                                <th></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>

                                                            {vendorTable}
                                                            
                                                        </tbody>
                                                    </Table>
                                                </div>
                                            </Col>
                                            <ManageVendorCommission onSave={this.saveVendorComm}  onCancel={this.closeCommModal} vendorInfo={this.state.vendorComm} />
                                            <CreateEditVendor onSave={this.saveVendor} uploadVendorLogo={this.uploadVerndorLogo} onCancel={this.closeModal} vendorInfo={this.state.vendorInfo} />
                                        </Row>

                                        
                                    </div>
                                    
                                </div>
                            </div>

                        </div>
                        </div>
                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    console.log(state);
    const { alert, vendor } = state;

    return {
        alert, vendor
    };
}

const connectedVendorsPage = connect(mapStateToProps)(Vendors);
export { connectedVendorsPage as Vendors };
