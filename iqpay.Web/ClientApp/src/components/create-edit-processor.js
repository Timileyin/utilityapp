﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import { status } from '../constants/commissionMode';
export default class CreateEditProcessor extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            processorInfo: this.props.processorInfo
        };


    }

    handleCreateChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };

        if (name === 'name') {
            theState.processorInfo.name = value;
        }

        if (name === 'code') {
            theState.processorInfo.code = value;
        }        

        if (name === 'status') {
            theState.processorInfo.statusInt = value;
            theState.processorInfo.status = value;
        }
        this.setState(theState);
    }



    componentDidUpdate = (prevprops) => {

        if (prevprops !== this.props) {
            this.setState({ isOpen: this.props.isOpen, processorInfo: this.props.processorInfo })
        }
    }


    render() {
        var processorInfo;
        if (this.state.processorInfo == null) {
            processorInfo = {};
        } else {
            processorInfo = this.state.processorInfo;
        }

        return (

            <Modal isOpen={this.state.processorInfo != null} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    {processorInfo.id == undefined ? "Create Processor" : "Edit " + processorInfo.name}
                </ModalHeader>
                <ModalBody>
                    <Form>
                        <Row form>
                            <Col md={12}>
                                <FormGroup>
                                    <Label for="exampleEmail">Processor Name</Label>
                                    <Input value={processorInfo.name} type="text" onChange={this.handleCreateChange} name="name" bsSize="sm" id="name" placeholder="Processor name" />
                                </FormGroup>
                            </Col>
                            <Col md={12}>
                                <FormGroup>
                                    <Label for="exampleEmail">Processor Code</Label>
                                    <Input value={processorInfo.code} type="text" onChange={this.handleCreateChange} name="code" bsSize="sm" id="code" placeholder="Processor code" />
                                </FormGroup>
                            </Col>
                            <Col md={12}>
                                <FormGroup>
                                    <Label for="exampleEmail">Status</Label>
                                    <Input value={processorInfo.statusInt} onChange={this.handleCreateChange} name="status" id="status" type="select" bsSize="sm">
                                        
                                        {
                                            status.map((c) =>
                                                <option key={c.key} value={c.key}>{c.value}</option>
                                            )
                                        }
                                    </Input> </FormGroup>
                            </Col>

                           
                        </Row>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => { this.props.onSave(this.state.processorInfo) }}>Save</Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}