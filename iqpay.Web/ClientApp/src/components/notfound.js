import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';

export const NotFound = props => (

    <div className="col-lg-12">
        <PageTitle title="Page Not Found" />
        <div className="row justify-content-center align-items-center text-center">
            <div className="col-sm-12 mb-5">
                <div className="card shadow-sm border-0">
                    <div className="card-body">
                    <h1>Oops!</h1>
                    <h2>404 Not Found</h2>
                    <div>
                        Sorry, an error has occured, Requested page not found!
                    </div>
                <div class="error-actions">
                    <Link to="/" className="btn btn-primary btn-lg">
                        <span class="glyphicon glyphicon-home"></span>Take Me Home 
                    </Link>
                </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  );
  
  export default connect()(NotFound);