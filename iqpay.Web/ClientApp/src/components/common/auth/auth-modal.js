﻿import React from 'react';
import { Modal, Button, Nav, Tab, Row, Col } from 'react-bootstrap';
import './auth.css'
import  Login from './login';
import SignUp  from './signup'
import $ from 'jquery';

window.$ = window.jQuery = $;
require('easy-responsive-tabs');

export default class AuthModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: this.props.showModal,
        };
    }
    toggle() {
        this.setState({
            show: this.state.show
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ show: this.props.showModal })
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document


    }
    render() {
        return (

            <Modal show={this.state.show} onHide={() => this.toggle()} >

                <Modal.Body>
                        <Row>
                            <Col xs={12} md={12}>
                                <Tab.Container id="left-tabs-example" defaultActiveKey="login">
                            <Nav variant="tabs" defaultActiveKey="login">
                                <Nav.Item>
                                    <Nav.Link eventKey="login" >Login</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey="signup">Sign Up</Nav.Link>
                                </Nav.Item>
                            </Nav>

                            <Tab.Content className="pt-4">
                                <Tab.Pane eventKey="login">
                                    <Login />
                                </Tab.Pane>
                                <Tab.Pane eventKey="signup">
                                    <SignUp />
                                </Tab.Pane>
                            </Tab.Content>
                                </Tab.Container>
                            </Col>
                        </Row>
                </Modal.Body>
                
            </Modal>           
        );
    }
}
