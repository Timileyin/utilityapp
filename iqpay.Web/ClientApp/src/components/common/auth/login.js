﻿import React from 'react';

export default class Login extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: this.props.isVisible,
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ isVisible: this.props.isVisible })
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document


    }
    render() {
        return (

            <form id="loginForm" method="post">
                <div className="form-group">
                    <input type="text" className="form-control" id="loginMobile" required placeholder="Email Address" />
                </div>
                <div className="form-group">
                    <input type="password" className="form-control" id="loginPassword" required placeholder="Password" />
                </div>
                <div className="row mb-4">
                    <div className="col-sm">
                        <div className="form-check custom-control custom-checkbox">
                            <input id="remember-me" name="remember" className="custom-control-input" type="checkbox" />
                            <label className="custom-control-label" htmlFor="remember-me">Remember Me</label>
                        </div>
                    </div>
                    <div className="col-sm text-right"> <a className="justify-content-end" href="#">Forgot Password ?</a> </div>
                </div>
                <button className="btn btn-primary btn-block" type="submit">Login</button>
            </form>

        );
    }
}
