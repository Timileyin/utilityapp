﻿import React from 'react';

export default class SignUp extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: this.props.isVisible,
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ isVisible: this.props.isVisible })
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document


    }
    render() {
        return (

            <form id="signupForm" method="post">
                <div className="form-group">
                    <input type="text" className="form-control" data-bv-field="number" id="signupEmail" required placeholder="Email ID" />
                </div>
                <div className="form-group">
                    <input type="text" className="form-control" id="signupMobile" required placeholder="Mobile Number" />
                </div>
                <div className="form-group">
                    <input type="password" className="form-control" id="signuploginPassword" required placeholder="Password" />
                </div>
                <button className="btn btn-primary btn-block" type="submit">Signup</button>
            </form>

        );
    }
}
