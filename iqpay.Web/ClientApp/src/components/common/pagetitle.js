﻿import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { userActions } from '../../actions/userAction';
import ShowReservedAccount from './show-reserved-account';

class PageTitle extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            title: this.props.title,
            showReservedAccountModal: false,
            accountDetails: null
        };
    }
 
    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ title: this.props.title})
            if(this.props.authentication.reservedAccount != null){                
                this.setState({showReservedAccountModal: true, accountDetails: this.props.authentication.reservedAccount});
            }
        }
    }

    generateAccountNumber = () => {
        const { dispatch } = this.props;
        dispatch(userActions.generateAccountNumber());
    }

    showReservedAccountModal = () => {        
        const { dispatch } = this.props;
        dispatch(userActions.fetchReservedAccount());                
    }

    closeModal = () => {
        this.setState({showReservedAccountModal: false});
    }

    componentDidMount() {
        //This method is called when the component is first added to the document


    }
    render() {
        return (
            
                <section className="page-header page-header-text-light bg-secondary">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-md-8">
                            <h1>{this.state.title}</h1>
                            </div>
                        <div className="col-md-4 text-right">
                            <ul className="breadcrumb justify-content-start justify-content-md-end mb-0">
                                <li><Link to="/">Home</Link></li>
                                <li className="active">{this.state.title}</li>
                            </ul>
                            <a className="btn btn-info btn-sm" onClick={this.showReservedAccountModal}>View Bank Details</a>
                        </div>
                    </div>
                    </div>
                    <ShowReservedAccount onCancel={this.closeModal} accountDetails={this.state.accountDetails} generateAccountNumber={this.generateAccountNumber} isOpen={this.state.showReservedAccountModal} /> 
                </section>  
                               
        );
    }
}

function mapStateToProps(state) {
    const { alert, authentication } = state;
    console.log(state);
    return {
        alert, authentication
    };
}

const connectedLayout = connect(mapStateToProps)(PageTitle);
export { connectedLayout as PageTitle };
