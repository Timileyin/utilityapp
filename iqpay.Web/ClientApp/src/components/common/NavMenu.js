import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import './NavMenu.css';

export default class NavMenu extends React.Component {
  constructor (props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
        isOpen: false,
        loggedIn: this.props.showLoginSignUpButton,
        userdetails: JSON.parse(localStorage.getItem("currentUser"))
    };
  }

  logout = () => {
    localStorage.removeItem('currentUser');
    window.location.reload('/login');
  }

  toggle () {
    this.setState({
      isOpen: !this.state.isOpen
    });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ loggedIn: this.props.showLoginSignUpButton, userdetails: JSON.parse(localStorage.getItem("currentUser")) });                        
        }
    }


  render () {
    return (
            <header id="header">
                <div className="container">
                    <div className="header-row">
                        <div className="header-column justify-content-start">                            
                            <div className="logo">
                                <Link to="/" title="iqPay"><img src="images/logo.png" alt="Quickai" width="127" height="29" /></Link>
                            </div>
                        </div>
                        <div className="header-column justify-content-end">
                            
		                    <nav className="primary-menu navbar navbar-expand-lg nav-dark-dropdown">
                                <div id="header-nav" className="collapse navbar-collapse">
                                    <ul className="navbar-nav">
                                    <li> <Link to="/">Home</Link></li>
                                    {!this.state.loggedIn ?
                                        <li className="login-signup ml-lg-2"><Link className="pl-lg-4 pr-0" data-toggle="modal" data-target="#login-signup" to="/login" title="Login / Sign up">Login / Sign up <span className="d-none d-lg-inline-block"><i className="fas fa-user"></i></span></Link></li> :                                        
                                        <li className="login-signup ml-lg-2"><Link to='/dashboard' className="pl-lg-4 pr-0" href="#" title="Welcome">Welcome {this.state?.userdetails?.firstname} <span className="d-none d-lg-inline-block"><i className="fas fa-user"></i></span></Link> <a onClick={this.logout} className="pl-lg-4 pr-0" data-toggle="modal" data-target="#login-signup" href="#" title="Logout">Logout <span className="d-none d-lg-inline-block"><i className="fas fa-sign-out-alt"></i></span></a> </li>
                                    }</ul>
                                </div>
                            </nav>
                        </div>
                        
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#header-nav"> <span></span> <span></span> <span></span> </button>
                    </div>
                </div>
            </header>

        //<Navbar classNameName="navbar-expand-sm navbar-toggleable-sm border-bottom box-shadow mb-3" light >
        //  <Container>
        //    <NavbarBrand tag={Link} to="/">iqpay.Web</NavbarBrand>
        //    <NavbarToggler onClick={this.toggle} classNameName="mr-2" />
        //    <Collapse classNameName="d-sm-inline-flex flex-sm-row-reverse" isOpen={this.state.isOpen} navbar>
        //      <ul classNameName="navbar-nav flex-grow">
        //        <NavItem>
        //          <NavLink tag={Link} classNameName="text-dark" to="/">Home</NavLink>
        //        </NavItem>
        //        <NavItem>
        //          <NavLink tag={Link} classNameName="text-dark" to="/counter">Counter</NavLink>
        //        </NavItem>
        //        <NavItem>
        //          <NavLink tag={Link} classNameName="text-dark" to="/fetch-data">Fetch data</NavLink>
        //        </NavItem>
        //      </ul>
        //    </Collapse>
        //  </Container>
        //</Navbar>
    );
  }
}
