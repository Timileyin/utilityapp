﻿import React from 'react';
import $ from 'jquery';
import { serviceTypes } from '../../constants/commissionMode';

window.$ = window.jQuery = $;
require('easy-responsive-tabs');

export default class Sidebar extends React.Component {
    constructor(props) {
        super(props);
        
        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }

    onlistItemClicked = (componentClicked, serviceTypeKey) => {
        //alert(serviceTypeKey);

        this.props.currentComponentChanger(componentClicked, serviceTypeKey)
        
    }

    

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidMount() {
         //This method is called when the component is first added to the document
        

    }
    render() {
        return (
            <div className="col-md-3 my-0 my-md-4">
                <ul className="sample resp-tabs-list">
                    {serviceTypes.map(s => {                        
                        return <li key={s.key} onClick={() => this.onlistItemClicked(s.module, s.key)}><span><i className={s.icon}></i></span>{s.value}</li>
                    })}                                       
                </ul>
            </div>
        );
    }
}
