import React from 'react';
import DateRangePicker from '@wojtekmaj/react-daterange-picker';
import moment from 'moment'
export default class FilterModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            startDate: new Date(),
            endDate: new Date()
        };
    }

    cancel = () =>{
        this.setState({isOpen: false});
    }

    handleCreateChange = (date) => {
        
        var theState = { ...this.state };   
        console.log(date);     
        theState.startDate = date[0];
        theState.endDate = date[1];        
        this.setState(theState);
        this.props.onFilter(moment(theState.startDate).format("yyyy-MM-DD"), moment(theState.endDate).format("yyyy-MM-DD"));
    }

    render() {
        return (
            <DateRangePicker onChange={this.handleCreateChange} value = {[this.state.startDate, this.state.endDate]}/>                                                            
        )
    }
}