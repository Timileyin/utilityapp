﻿import React from 'react';
import { Collapse, Container, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink } from 'reactstrap';
import { Link } from 'react-router-dom';
import moment from 'moment';
import './NavMenu.css';

export default class Footer extends React.Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isOpen: false
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    render() {
        return (
            <footer id="footer">
                <section className="section bg-light shadow-md pt-4 pb-3">
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-8 col-md-3">
                                <div className="featured-box text-center">
                                    <div className="featured-box-icon"> <i className="fas fa-lock"></i> </div>
                                    <h4>100% Secure Payments</h4>
                                    <p>We do not need a card to complete utility transactions.</p>
                                </div>
                            </div>
                            <div className="col-sm-8 col-md-3">
                                <div className="featured-box text-center">
                                    <div className="featured-box-icon"> <i className="fas fa-thumbs-up"></i> </div>
                                    <h4>Trusted payments</h4>
                                    <p>100% Payment Protection.</p>
                                </div>
                            </div>
                            {/* <div className="col-sm-6 col-md-3">
                                <div className="featured-box text-center">
                                    <div className="featured-box-icon"> <i className="fas fa-bullhorn"></i> </div>
                                    <h4>Refer & Earn</h4>
                                    <p>Invite a friend to sign up and earn up to $100.</p>
                                </div>
                            </div> */}
                            <div className="col-sm-8 col-md-3">
                                <div className="featured-box text-center">
                                    <div className="featured-box-icon"> <i className="far fa-life-ring"></i> </div>
                                    <h4>24X7 Support</h4>
                                    <p>We're here to help. Have a query and need help ? <a href="#">Call +234 708 067 1100</a></p>
                                </div>
                            </div>
                            <div className="col-sm-8 col-md-3">
                                <div className="featured-box text-center">
                                    <div className="featured-box-icon"> <i className="far fa-life-ring"></i> </div>
                                    <h4>Become a partner</h4>
                                    <p>Want to partner with us?. <a href="#">Call +234 708 067 1100</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="container mt-4">
                    <div className="row">
                        <div className="col-md-12 d-flex align-items-md-center flex-column">
                            <p>Keep in touch</p>
                            <ul className="social-icons">
                                <li className="social-icons-facebook"><a data-toggle="tooltip" href="http://www.facebook.com/" target="_blank" title="Facebook"><i className="fab fa-facebook-f"></i></a></li>
                                <li className="social-icons-twitter"><a data-toggle="tooltip" href="http://www.twitter.com/" target="_blank" title="Twitter"><i className="fab fa-twitter"></i></a></li>
                                <li className="social-icons-instagram"><a data-toggle="tooltip" href="http://www.instagram.com/" target="_blank" title="Instagram"><i className="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div className="container">
                    <div className="footer-copyright">
                        <ul hidden={true} className="nav justify-content-center">
                            <li className="nav-item"> <a className="nav-link active" href="#">About Us</a> </li>
                            <li className="nav-item"> <a className="nav-link" href="#">Faq</a> </li>
                            <li className="nav-item"> <a className="nav-link" href="#">Contact</a> </li>
                            <li className="nav-item"> <a className="nav-link" href="#">Support</a> </li>
                            <li className="nav-item"> <a className="nav-link" href="#">Terms of Use</a> </li>
                            <li className="nav-item"> <a className="nav-link" href="#">Privacy Policy</a> </li>
                        </ul>
                        <p className="copyright-text">Copyright © {moment(new Date()).format("YYYY")} <a target="_blank" href="https://iqitech.com.ng">IQITECH</a>. All Rights Reserved.</p>
                    </div>
                </div>
            </footer> 
        );
    }
}
