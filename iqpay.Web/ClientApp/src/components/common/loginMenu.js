﻿import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import {roles} from '../../constants/rolesConstants';

export default class LoginMenu extends React.Component {
    constructor(props) {
        super(props);
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));
        this.state = {
            activeMenu: this.props.activeMenu,
            role: currentUser.role,
            permissions: currentUser.permissions?.split(",")
        };
    }

    toggle() {
        this.setState({
            activeMenu: !this.state.activeMenu
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ activeMenu: this.props.activeMenu})
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document
    }

    hasPermission = (permission) => {
        var permissions = this.state.permissions;
        if(permissions != null)
        {
            return permissions.indexOf(permission) > 0;
        }
        return true;
    }

    render() {
        return (
            <div className="container">
                    <div className="row">
                        <div className="col-lg-3">
                            <ul className="nav nav-pills alternate flex-lg-column">
                                <li className="nav-item">
                                    <NavLink className="nav-link" activeClassName="active" to="/dashboard"><i className="fas fa-tachometer-alt"></i>Dashboard</NavLink>
                                </li>                                                                
                                <li hidden = {this.state.role != roles.SUPERADMIN && !this.hasPermission("Vendors") } className="nav-item">
                                    <NavLink className="nav-link" activeClassName="active" to="/vendors"><i className="fas fa-users"></i> Vendors</NavLink>
                                </li>                                
                                <li className="nav-item" hidden={!this.hasPermission("Wallets")}>
                                    <NavLink className="nav-link" activeClassName="active" to="/wallets"><i className="fas fa-wallet"></i>Wallets</NavLink>
                                </li>
                                <li hidden = {this.state.role == roles.AGENT || this.state.role == roles.REGULAR_CUSTOMER || !this.hasPermission("Funding Request")} className="nav-item">
                                    <NavLink className="nav-link" activeClassName="active" to="/funding-request"><i className="fas fa-users"></i> Funding Request</NavLink>
                                </li>
                                <li className="nav-item" hidden={!this.hasPermission("Transfer Money")}>
                                    <NavLink className="nav-link" activeClassName="active" to="/money-transfer"><i className="fas fa-tachometer-alt"></i>Transfer Money</NavLink>
                                </li>
                                <li className="nav-item" hidden={!this.hasPermission("Profile")}>
                                <NavLink className="nav-link" activeClassName="active" to="/profile"><i className="fas fa-user"></i>Profile</NavLink>
                                </li>
                                <li hidden = {!this.hasPermission("Partnership Requests")}>
                                    <NavLink className="nav-link" activeClassName="active" to="/partnersrequest"><i className="fas fa-chart-line"></i>Partnership Requests</NavLink>
                                </li>  
                                <li hidden = {this.state.role != roles.SUPERADMIN}>
                                    <NavLink className="nav-link" activeClassName="active" to="/orders"><i className="fas fa-chart-line"></i>Placed IQPAY Orders</NavLink>
                                </li>  
                                <li hidden = {this.state.role == roles.REGULAR_CUSTOMER  || this.state.role == roles.DEALER || this.state.role == roles.SUPERDEALER || this.state.role == roles.AGENT || !this.hasPermission("Super Dealers")} className="nav-item">
                                <NavLink className="nav-link" to="/super-dealers"><i className="fas fa-user"></i>Super Dealers</NavLink>
                                </li>
                                <li hidden = {this.state.role == roles.REGULAR_CUSTOMER  || this.state.role == roles.DEALER || this.state.role == roles.AGENT || !this.hasPermission("Dealers") } className="nav-item">
                                <NavLink className="nav-link" activeClassName="active" to="/dealers"><i className="fas fa-user"></i>Dealers</NavLink>
                                </li>
                                <li hidden = {this.state.role == roles.REGULAR_CUSTOMER || this.state.role == roles.AGENT || !this.hasPermission("Transfer Money") } className="nav-item">
                                <NavLink className="nav-link" activeClassName="active" to="/agents"><i className="fas fa-user"></i>Agents</NavLink>
                                </li> 
                                <li hidden = {this.state.role != roles.SUPERADMIN || !this.hasPermission("Household Users")} className="nav-item">
                                <NavLink className="nav-link" activeClassName="active" to="/householdusers"><i className="fas fa-mobile"></i>Household Users</NavLink>
                                </li> 
                                <li hidden = {this.state.role != roles.SUPERADMIN || !this.hasPermission("Virtual Accounts")} className="nav-item">
                                <NavLink className="nav-link" activeClassName="active" to="/virtualaccounts"><i className="fas fa-safe"></i>Virtual Accounts</NavLink>
                                </li>
                                <li hidden = {this.state.role != roles.SUPERADMIN || !this.hasPermission("Administrators")} className="nav-item">
                                <NavLink className="nav-link" activeClassName="active" to="/admins"><i className="fas fa-safe"></i>Administrators</NavLink>
                                </li>                                                                
                            </ul>
                            <hr />
                            <p>Reports</p>                            
                            <ul className="nav nav-pills alternate flex-lg-column sticky-top">
                            <li className="nav-item">
                                    <NavLink className="nav-link" activeClassName="active" to="/transactions"><i className="fas fa-chart-pie"></i>Transaction</NavLink>
                                </li>
                                <li>
                                <NavLink className="nav-link" activeClassName="active" to="/commAheadReport"><i className="fas fa-chart-line"></i>Commission Ahead Report</NavLink>
                                </li>
                                <li>
                                    <NavLink className="nav-link" activeClassName="active" to="/walletfundinghistory"><i className="fas fa-chart-line"></i>Wallet Funding History</NavLink>
                                </li>
                                <li hidden = {this.state.role != roles.SUPERADMIN}>
                                    <NavLink className="nav-link" activeClassName="active" to="/masterwallets"><i className="fas fa-chart-line"></i>Master Wallets</NavLink>
                                </li>                                
                                <li hidden = {this.state.role != roles.SUPERADMIN}>
                                    <NavLink className="nav-link" activeClassName="active" to="/processor-vending-summary"><i className="fas fa-chart-line"></i>Processor Vending</NavLink>
                                </li>
                                <li hidden = {this.state.role != roles.SUPERADMIN}>
                                    <NavLink className="nav-link" activeClassName="active" to="/wallet-balances"><i className="fas fa-wallet"></i>Wallet Balances</NavLink>
                                </li>
                                <li hidden = {this.state.role != roles.SUPERADMIN} className="nav-item">
                                    <NavLink className="nav-link" activeClassName="active" to="/vendors-cumm-balance"><i className="fas fa-safe"></i>Wallet Balances By Vendors</NavLink>
                                </li> 
                            </ul>
                    </div>
                    {this.props.children}
                    </div>
                </div>           
        );
    }
}
