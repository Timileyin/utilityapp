import React from 'react';
import { Container } from 'reactstrap';
import NavMenu from './NavMenu';
import Footer from './footer'
import LoginModal from './auth/auth-modal';
import { ToastContainer } from "react-toastr";
import { connect } from 'react-redux';
import { alertConstants } from '../../constants/alertConstants';
import { alertActions } from '../../actions/alertActions';
import jQuery from 'jquery';
require('react-toastr/src/components/demo.css');
class Layout extends React.Component {

    constructor(props) {
        super(props);
        this.state = { isUnAuthorised: false, loggedIn: this.props.authentication.loggedIn}
        this.container = null;
    }

    componentDidMount = () => {
        //const{dispatch} = this.props;
       // dispatch(alertActions.clear());
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            var alert = this.props.alert;
            var loggedIn = this.props.authentication.loggedIn;
            if (alert.type) {
                switch (alert.type) {
                    case alertConstants.SUCCESS_TOASTR:
                        this.container.success(alert.message, alert.title)
                        break;

                    case alertConstants.DANGER_TOASTR:
                        this.container.error(alert.message, alert.title);
                        break;
                    default:
                        this.container.error(alert.message, alert.title);
                        break;
                }

                const{dispatch} = this.props;
                dispatch(alertActions.clear());
            }            
            this.setState({ isUnAuthorised: alert.isUnAuthorised, loggedIn: loggedIn});
        }
    }
    
    
    render() {
        return (
            <div id="main-wrapper">

                <ToastContainer
                    ref={ref => this.container = ref}
                    className="toast-top-right"
                />

                <NavMenu showLoginSignUpButton={this.state.loggedIn} />
                <div id="content">                
                    {this.props.children}                    
                </div>
                <LoginModal showModal={this.state.isUnAuthorised} />
                <Footer />
            </div>
        )
    }
}



function mapStateToProps(state) {
    const { alert, authentication } = state;
    console.log(state);
    return {
        alert, authentication
    };
}

const connectedLayout = connect(mapStateToProps)(Layout);
export { connectedLayout as Layout };