import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
export default class ShowReservedAccount extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            reservedAccountInfo: {},
        };
    }

    componentDidUpdate = (prevprops) => {

        if (prevprops !== this.props) {
            if (this.props.accountDetails != null) {
                this.setState({ isOpen: this.props.isOpen });
                console.log(this.props.accountDetails);
                if (this.props.accountDetails.wemaAccountNumber) {
                    this.setState({ reservedAccountInfo: this.props.accountDetails })
                }
            }
        }
    }

    render() {
        var accountDetails;
        if (this.state.reservedAccountInfo == undefined) {
            accountDetails = <tbody><tr>You account numbers have not been generated. Click the "Generate" button below to generate one now. </tr></tbody>
        } else {
            console.log(this.state.reservedAccountInfo);
            accountDetails = <tbody>
                <tr><th colSpan={2}>Your virtual account numbers are as follows: <ul><li>{this.state.reservedAccountInfo.vfdAccountNumber} VFD Microfinance Bank ({this.state.reservedAccountInfo.vfdAccountName}) (Charges: ₦50/transaction)</li><li>{this.state.reservedAccountInfo.wemaAccountNumber} WEMA ({this.state.reservedAccountInfo.wemaAccountName}) (Charges: ₦50/transaction)</li></ul> You can top up your wallet instantly by doing a transfer to any of these account numbers. Save it as a beneficiary on your banking app or transfer with USSD so you can send money to it at any time! Thank you </th></tr>
            </tbody>;
        }

        return (

            <Modal isOpen={this.state.isOpen} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    Reserved Account
                </ModalHeader>
                <ModalBody>
                    <Table striped hover size="md">
                            {accountDetails}
                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" hidden={this.state.reservedAccountInfo?.rubiesAccountNumber && this.state.reservedAccountInfo?.accountNumber} onClick={() => { this.props.generateAccountNumber(this.state.dealerInfo) }}>Generate </Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}