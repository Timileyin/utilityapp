import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { permissions } from '../constants/permissionConstants'; 
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import { status, commissionMode, serviceTypes } from '../constants/commissionMode';
export default class ManagePermission extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            checkListItems: permissions,
            userPermissions: this.props.userPermissions,                        
        };
    }

    arrayRemove = (arr, value) => { 
    
        return arr.filter((ele) => { 
            return ele != value; 
        });
    }

    handleChange = (e) => {
        
        const { name, value } = e.target;
        var theState = { ...this.state };
        //theState.checkListItems.forEach(v => {
        if(e.target.checked)            
        {
            theState.userPermissions.push(value);
        }else{
            theState.userPermissions = this.arrayRemove(theState.userPermissions, value);
        }
        //});
        this.setState(theState);
    }

    componentDidUpdate = (prevprops) => {

        if (prevprops !== this.props) {
                this.setState({ checkListItems: permissions, userPermissions: this.props.userPermissions })            
        }
    }

    render() {
        var checkListItems;
        var checkListHtml = [];
        if (this.state.checkListItems == null) {
            checkListItems = [];
        } else {
            checkListItems = this.state.checkListItems;
        }

        checkListItems.map(x=>{
            checkListHtml.push(<div key={x[0]}><Input value={x} key={x} checked={x.isActive} onChange={(e) => this.handleChange(e)} type="checkbox" />{' '} {x}</div>);
        })

        return (

            <Modal isOpen={this.state.userPermissions !== null} toggle={this.props.onCancel}>
                <ModalHeader toggle={this.toggle}>
                    Vendor's Checklist
                </ModalHeader>
                <ModalBody>
                    <Form>
                        {checkListHtml}
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => { this.props.onSave(this.state.userPermissions) }}>Save</Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}