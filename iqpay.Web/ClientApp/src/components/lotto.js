import React from 'react';
import $ from 'jquery';
import { connect } from 'react-redux';
import { servicesActions } from '../actions/servicesAction';
import { vendorService } from '../services/vendorService';

window.$ = window.jQuery = $;
require('easy-responsive-tabs');


class Lotto extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: this.props.isVisible,
            serviceTypeKey: this.props.serviceTypeKey,
            vendors: this.props.vendors,
            vendorServices: []
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    handleCreateChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        if (name === 'vendor') {
            var vendorServices = theState.vendors.find(x => x.id == value).serivces.filter(x => x.serviceTypeInt == this.state.serviceTypeKey);
            console.log(vendorServices);
            theState.vendorServices = vendorServices;
        }

        this.setState(theState);
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change

        if (this.props !== prevprops) {

            this.setState({ isVisible: this.props.isVisible, serviceTypeKey: this.props.serviceTypeKey, vendors: this.props.vendors });
        }

    }

    recharge = () => {
        if (window.confirm("Are you sure you want to continue?")) {

        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document


    }
    render() {

        var vendorsOptions = [];
        this.state.vendors.map(s => {
            vendorsOptions.push(<option value={s.id}>{s.name}</option>);
        });

        var serviceOptions = [];
        this.state.vendorServices.map(s => {
            serviceOptions.push(<option value={s.id}>{s.name}</option>);
        });

        return (

            <div style={!this.state.isVisible ? { display: 'none' } : null}>
                <h2 className="text-6 mb-4">Play Lotto</h2>
                <form id="recharge-bill" method="post">
                    <div className="mb-3">

                        <div className="form-group">
                            <label htmlFor="operator">Select Your Provider</label>
                            <select className="custom-select" name="vendor" onChange={this.handleCreateChange} id="vendor" required="">
                                <option value="">Select Your Network Provider </option>
                                {vendorsOptions}
                            </select>
                        </div>

                        <div className="form-group">
                            <label htmlFor="operator">Select Game Type</label>
                            <select className="custom-select" name="service" id="service" required="">
                                <option value="">Select Your Network Provider </option>
                                {serviceOptions}
                            </select>
                        </div>


                    </div>
                    <div className="form-group">
                        <label htmlFor="mobileNumber">Player's Phone Number</label>
                        <input type="text" className="form-control" data-bv-field="number" id="mobileNumber" required placeholder="Enter Mobile Number" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="amount">Amount</label>
                        <div className="input-group">
                            <div className="input-group-prepend"> <span className="input-group-text">&#8358;</span> </div>
                            <input className="form-control" id="amount" placeholder="Enter Amount" required type="text" />
                        </div>
                    </div>
                    <a className="btn btn-primary btn-block" onClick={this.recharge} href="recharge-order-summary.html">Continue to Recharge</a>
                </form>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, serviceManagement } = state;

    return {
        alert, serviceManagement
    };
}

const connectedLotto = connect(mapStateToProps)(Lotto);
export { connectedLotto as Lotto };
