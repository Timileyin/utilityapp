import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Row, Col, Button } from 'reactstrap';
import { connect } from 'react-redux';
import { virtualAccountsAction } from '../actions/virtualAccountsAction';
import {roles} from '../constants/rolesConstants';
import numeral from 'numeral';
import moment from 'moment';
import FilterModal from './common/filterModal';
import DataTable from 'react-data-table-component';
import {downloadCSV} from '../helpers/history';
import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";

import jQuery from 'jquery';

class VirtualTransactions extends React.Component {
    modalpopup = true;   
    currentUser = JSON.parse(localStorage.getItem("currentUser")); 
    columns = [
        
        {
            name: 'Dealer Code',
            selector: 'dealerCode',
            sortable: true,
        },
        {
            name: 'Date Generated',
            cell: row => moment(row.dateCreated).format("DD ddd MMM yyyy, hh:mm A"),
            sortable: true,
        },
        {
            name: 'Account Number',
            selector: 'accountNumber',
            sortable: true,
        },
        {
            name: '',
            button: true,
            cell: row => <a href={"/transactions/"+row.accountNumber} target="_blank" rel="noopener noreferrer">Deposits</a>,
        }      
    ]

    constructor(props) {
        super(props);
        
        this.state = {
            role: this.currentUser.role,
            pageTitle: 'Virtual Transactions',
            virtualTransactions: [],
            showFilterModal: false,
            dealerCode: this.props.match.params.dealerCode            
        };
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if(this.props.virtualAccounts.virtualTransactions != undefined){
                this.setState({logs: this.props.virtualAccounts.virtualTransactions });                                    
            }                           
        }
    }

    loadFilterModal = () =>{
        this.setState({showFilterModal: true})
    }

    onFilter = (startDate, endDate) => {
        const { dispatch } = this.props;
        //dispatch(transactionAction.filterVendingLogs(startDate, endDate));
    }

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document        
        const { dispatch } = this.props;
        if(this.state.dealerCode == undefined)
        {
            dispatch(virtualAccountsAction.fetchTransactions());
        }
        else
        {
            var dealerCode = this.state.dealerCode;
            dispatch(virtualAccountsAction.fetchAgentTransactions(dealerCode));
        }
                
    }

    render() {
        var accounts = this.state.logs;
        let accountTable = [];
        var transactionValue = 0;
        
        return (
            <div>

                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                        <Row>
                                            <Col>
                                            <span className="float-right"><i class="fa fa-filter"></i><FilterModal onFilter={(startDate, endDate) => this.onFilter(startDate,endDate)}/></span>
                                                <DataTable
                                                    title={ this.props.match.params.vendCode == undefined ? "Virtual Transactions" : "Virtual Transactions for "+this.state.dealerCode}
                                                    columns={this.columns}
                                                    data={this.state.logs}
                                                    actions={<Button className="btn btn-sm" onClick={()=>downloadCSV(this.state.logs)}>Export</Button> }
                                                    pagination
                                                    paginationPerPage={30}
                                                />
                                            </Col>                                           
                                        </Row>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </LoginMenu>                
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, virtualAccounts } = state;

    return {
        alert, virtualAccounts
    };
}

const connectedTransactionsPage = connect(mapStateToProps)(VirtualTransactions);
export { connectedTransactionsPage as VirtualTransactions};