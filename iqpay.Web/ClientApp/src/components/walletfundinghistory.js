import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Badge } from 'reactstrap';
import { fundingRequestActions } from '../actions/fundingRequestActions';
import ApproveFundingRequest from './approve-funding-request';
import { connect } from 'react-redux';
import DataTable from 'react-data-table-component'
import {downloadCSV} from '../helpers/history';
import FilterModal from './common/filterModal';
import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";

import $ from 'jquery';

class WalletFundingRequestHistory extends React.Component {
    modalpopup = true;
    columns = [
        {
            name: 'Wallet name',
            selector: 'walletUser',
            sortable: true,
        },
        {
            name: 'Date',
            selector: 'dateCreated',
            sortable: true,
        },
        {
            name: 'Transaction Date',
            selector: 'transactionDate',
            sortable: true,
        },
        {
            name: 'Amount Requested',
            selector: 'amount',
            sortable: true,
        },
        {
            name: 'Amount Creditted',
            selector: 'amountCreditted',
            sortable: true,
        }        
        ,
        {
            name: 'Creditted Wallet Balance',
            selector: 'credittedWalletBalance',
            sortable: true,
        },
        {
            name: 'Status',
            selector: 'status',
            sortable: true,
        }         
    ];

    constructor(props) {
        super(props);

        this.state = {

            pageTitle: 'Funding Request History',
            requests: [],
            requestInfo: null, 
            modalPopup: false,
            
        };
    }

    toggle = () => this.modalpopup = !this.modalpopup;

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            console.log(this.props);
            if (this.props.fundingRequest.requests !== undefined) {

                this.setState({ requests: this.props.fundingRequest.requests });
            }

            if (this.props.fundingRequest.request !== undefined) {
                var requestInfo = this.props.fundingRequest.request;
                this.setState({
                    requestInfo: requestInfo
                });
            }            
        }
    }

    cancel = () => {
        this.setState({
            requestInfo: null
        });
    }

    approveFundingRequest = (requestInfo) => {

        const { dispatch } = this.props;
        dispatch(fundingRequestActions.approveFundingRequest(requestInfo.id, requestInfo.amountCreditted));
    }

    loadRequestInfo = ( requestId) => {
        const { dispatch } = this.props;
        dispatch(fundingRequestActions.fetchFundingRequestInfo(requestId));        
    }

    onFilter = (startDate, endDate) => {
        const { dispatch } = this.props;
        dispatch(fundingRequestActions.filterFundingRequestHistory(startDate, endDate)); 
    }

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document

        const { dispatch } = this.props;
        dispatch(fundingRequestActions.fetchFundingRequestHistory());
    }

    render() {
        var requests = this.state.requests;
        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                        
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                    <h4 className="mb-3">Wallet Funding History</h4>
                                        <Row>
                                            <Col md={12}> 
                                                <span className="float-right"><i class="fa fa-filter"></i><FilterModal onFilter={(startDate, endDate) => this.onFilter(startDate,endDate)}/></span>
                                                <DataTable
                                                    title="Wallet Funding History"
                                                    columns={this.columns}
                                                    data={requests}
                                                    actions={<Button className="btn btn-sm" onClick={()=>downloadCSV(requests)}>Export</Button>}
                                                    pagination
                                                    paginationPerPage={30}
                                                />
                                            </Col>
                                        </Row>
                                        <ApproveFundingRequest requestInfo={this.state.requestInfo} onApprove={this.approveFundingRequest} onCancel={this.cancel} />

                                    </div>

                                </div>
                            </div>

                        </div>

                        
                    </div>

                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, fundingRequest } = state;

    return {
        alert, fundingRequest
    };
}

const walletFundingRequest = connect(mapStateToProps)(WalletFundingRequestHistory);
export { walletFundingRequest as WalletFundingRequestHistory };
