﻿import React from 'react';
import { Link } from 'react-router-dom';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { Modal, Button, Nav, Tab, Row, Col } from 'react-bootstrap';

class AuthPage extends React.Component {
    constructor(props) {
        super(props);

        this.props.dispatch(userActions.logout);

        this.state = {
            login: {
                username: '',
                password: ''
            },
            register: {
                email: '',
                password: '',
                confirmPassword: '',
                firstname: '',
                surname: '',
                phonenumber:''
            },
            loginSubmitted: false,
            signupSubmitted: false
        };
    }

    handleLoginChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        if (name === 'password') {
            theState.login.password = value;
        }

        if (name === 'username') {
            theState.login.username = value;
        }
        this.setState(theState);
    }

    handleSignupChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        if (name === 'password') {
            theState.register.password = value;
        }

        if (name === 'firstname') {
            theState.register.firstname = value
        }

        if (name === 'surname') {
            theState.register.surname = value;
        }

        if (name === 'confirmPassword') {
            theState.register.confirmPassword = value;
        }

        if (name === 'email') {
            theState.register.email = value;
        }

        if (name === 'phonenumber') {
            theState.register.phonenumber = value
        }
        this.setState(theState);
    }

    handleLoginSubmit = (e) => {
        e.preventDefault();
        this.setState({ loginSubmitted: true });
        const { username, password } = this.state.login;
        const { dispatch } = this.props;
        if (username && password) {
            dispatch(userActions.login(username, password));
        }
    }

    handleSignSubmit = (e) => {
        e.preventDefault();
        this.setState({ signupSubmitted: true });
        const { email, firstname, surname, confirmPassword, password, phonenumber } = this.state.register;
        const { dispatch } = this.props;
        if (email && firstname && surname && confirmPassword && password && phonenumber) {
            dispatch(userActions.signup(email, firstname, surname, confirmPassword, password, phonenumber));
        }
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ isVisible: this.props.isVisible })
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document

    }

    render() {
        const { loggingIn, signinUp, alert } = this.props;
        const { login, register, loginSubmitted, registerSubmitted} = this.state;

        return (
            <div id="content">
                <section className="page-header page-header-text-light bg-secondary">
                <div className="container">
                    <div className="row align-items-center">
                        <div className="col-md-8">
                            <h1>Login & Signup</h1>
                        </div>
                        <div className="col-md-4">
                            <ul className="breadcrumb justify-content-start justify-content-md-end mb-0">
                                <li><Link to="/">Home</Link></li>
                                <li className="active">Login/Signup</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
                <div className="container">
                   
                    <div id="login-signup-page" className="bg-light shadow-md rounded mx-auto p-4">
                        <Tab.Container id="left-tabs-example" defaultActiveKey="login">
                            <Nav variant="tabs" defaultActiveKey="login">
                                <Nav.Item>
                                    <Nav.Link eventKey="login" >Login</Nav.Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Nav.Link eventKey="signup">Sign Up</Nav.Link>
                                </Nav.Item>
                            </Nav>
                            <Tab.Content className="pt-4">
                                
                                <Tab.Pane eventKey="login">
                                    
                                    <form id="loginForm" onSubmit={this.handleLoginSubmit} method="post">
                                        <div className="form-group">
                                            <label htmlFor="loginMobile">Dealer code or Phone number</label>
                                            <input type="text" name="username" value={login.username} onChange={this.handleLoginChange} className="form-control" id="loginMobile" required placeholder="Dealer Code or Phone number" />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="loginPassword">Password</label>
                                            <input type="password" name="password" value={login.password} onChange={this.handleLoginChange} className="form-control" id="loginPassword" required placeholder="Password" />
                                        </div>
                                        <div className="row mb-4">
                                            <div className="col-sm">
                                                <div className="form-check custom-control custom-checkbox">
                                                    <input id="remember-me" name="remember" className="custom-control-input" type="checkbox" />
                                                    <label className="custom-control-label" htmlFor="remember-me">Remember Me</label>
                                                </div>
                                            </div>
                                            <div className="col-sm text-right"> <Link className="justify-content-end" to="forgot-password">Forgot Password ?</Link> </div>
                                        </div>
                                        <button className="btn btn-primary btn-block" type="submit">Login</button>
                                    </form>
                                </Tab.Pane>
                                <Tab.Pane eventKey="signup">
                                   
                                    <form id="signupForm" onSubmit={this.handleSignSubmit}>
                                        <div className="form-group">
                                            <label htmlFor="signupEmail">Email Address</label>
                                            <input type="email" name="email" value={register.email} onChange={this.handleSignupChange} className="form-control" data-bv-field="email" id="signupEmail" required placeholder="Email" />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="firstName">Firstname</label>
                                            <input type="text" name="firstname" value={register.firstname} className="form-control" onChange={this.handleSignupChange} data-bv-field="number" id="signupFirstname" required placeholder="Firstname" />
                                        </div>                                       
                                        <div className="form-group">
                                            <label htmlFor="lastName">Lastname</label>
                                            <input type="text" value={register.surname} className="form-control" onChange={this.handleSignupChange} data-bv-field="number" id="signupLastname" required placeholder="Lastname" name="surname" />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="signupMobile">Mobile Number</label>
                                            <input type="text" className="form-control" value={register.phonenumber} onChange={this.handleSignupChange} name="phonenumber" id="signupMobile" required placeholder="Mobile Number" />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="signuploginPassword">Password</label>
                                            <input type="password" onChange={this.handleSignupChange} value={register.password} className="form-control" id="signupPassword" required placeholder="Password" name="password" />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="signuploginPassword">Confirm Password</label>
                                            <input type="password" onChange={this.handleSignupChange} value={register.confirmPassword} className="form-control" id="signupConfirmPassword" required name="confirmPassword" placeholder="Confirm Password" />
                                        </div>
                                        <button className="btn btn-primary btn-block" type="submit">Signup</button>
                                    </form>
                                </Tab.Pane>
                            </Tab.Content>
                        </Tab.Container>
              </div>
                </div>
            </div>
        );
    }    
}

function mapStateToProps(state) {
    const { loggingIn, signinUp, alert } = state;
    //const { alert } = alert;
    return {
        loggingIn,
        signinUp,
        alert
    };
}

const connectedLoginPage = connect(mapStateToProps)(AuthPage);
export { connectedLoginPage as AuthPage }; 