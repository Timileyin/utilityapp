import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import CreateEditDealer from './create-edit-dealer';
import { dealersActions } from '../actions/dealersAction';
import CommissionRate from './commission-rates';
import VendorCheckList from './vendor-checklist'
import ManagePermission from './manage-permission';

import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";
import 'datatables.net-buttons-dt';
import 'datatables.net-buttons/js/buttons.html5.js';
import jQuery from 'jquery';
import { alertActions } from '../actions/alertActions';

class SysadminList extends React.Component {
    modalpopup = true;
    constructor(props) {
        super(props);

        this.state = {

            pageTitle: 'Manage Administrators',
            admins: [],
            adminInfo: null,
            modalPopup: false,
            adminId: null,
            userPermissions: null,
        };
    }

    savePermission = (permissions) => {
        const {dispatch} = this.props;
        dispatch(userActions.savePermission(this.state.adminId, permissions))               
        this.setState({userPermissions: null});
    }

    toggleActivate = (id, isLockedOut) => {
        if(window.confirm("Are you sure you want to perform this action?"))
        {
            const {dispatch} = this.props;        
            dispatch(dealersActions.toggleActivate(id,isLockedOut));
            window.location.reload();
        }        
    }
        
    saveDealer = (dealerInfo) => {
        const { dispatch } = this.props;
        dispatch(userActions.saveAdminInfo(dealerInfo));
        //this.setState({ vendorInfo: null });
    }   

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if (this.props.authentication.users !== undefined) {
                this.setState({ admins: this.props.authentication.users});
            }

            if (this.props.authentication.profileInformation !== undefined) {
                this.setState({ adminInfo: this.props.authentication.profileInformation  })
            }

            jQuery(document).ready(function () 
            {
                jQuery('#tableData').DataTable({
                    dom: 'Bfrtip',
                    retrieve: true,
                    buttons: [
                        'csv', 'excel', 'pdf'
                    ]
                });
            });
        }
    }

    editDealer = (adminId) => {
        const { dispatch } = this.props;
        dispatch(userActions.getAdminInfo(adminId));
    }

    addDealer = () => {
        this.setState({ adminInfo: {} });
    }    

    closeModal = () => {
        this.setState({ adminInfo: null });
    }

    closePermissionModal = () => {
        this.setState({userPermissions: null})
    }

    showPermissionModal = (id) => {
        this.setState({userPermissions: [], adminId: id});
    }

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document

        const { dispatch } = this.props;
        dispatch(userActions.fetchAdministrators());
    }

    render() {
        var admins = this.state.admins;
        let adminsTable = [];
        if (admins.length > 0) {
            admins.map((v) => {
                adminsTable.push(
                    <tr key={v.id}>
                        <td>{v.firstName+ ' ' + v.lastName}</td>
                        <td>{v.phoneNumber}</td>
                        <td>{v.email}</td>
                        <td>
                            <Button color="info" onClick={() => { this.editDealer(v.id) }} className="pull-right" size="sm"><i className="fas fa-edit"></i> Edit</Button>
                            &nbsp; <Button color={v.isLockedOut ? "info" : "danger"} onClick={() => { this.toggleActivate(v.id, v.isLockedOut) }} className="pull-right" size="sm"><i className={v.isLockedOut ? "fas fa-check" : "fas fa-lock"}></i> {v.isLockedOut ? "Activate" : "Deactivate"}</Button>
                            &nbsp; <Button color="info" onClick={() => { this.showPermissionModal(v.id) }} className="pull-right" size="sm"><i className="fa fa-cogs"></i> Permissions </Button>
                        </td>

                    </tr>);
            });
        } else {
            //dealersTable = <tr><td colspan='5'>Agents have not been setup</td></tr>;
        }

        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                        <Row>
                                            <Col>
                                                <Button color="" onClick={this.addDealer} className="pull-right" size="sm"><i className="fas fa-plus"></i> Create Administrator</Button>
                                                <Table id="tableData" striped hover size="md">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Phone number</th>
                                                            <th>Email</th>
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {adminsTable}
                                                    </tbody>
                                                </Table>
                                            </Col>
                                            <ManagePermission userPermissions={this.state.userPermissions} onSave={this.savePermission} onCancel={this.closePermissionModal}/>
                                            <CreateEditDealer dealerType="Administrator" onSave={this.saveDealer} onCancel={this.closeModal} dealerInfo={this.state.adminInfo} />
                                        </Row>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, authentication } = state;

    return {
        alert, authentication
    };
}

const connectedDealersPage = connect(mapStateToProps)(SysadminList);
export { connectedDealersPage as SysadminList };