﻿import React from 'react';
import {PageTitle} from './common/pagetitle';
import { connect } from 'react-redux';
import {roles} from '../constants/rolesConstants';
import { servicesActions } from '../actions/servicesAction';
import numeral from 'numeral';
import {deliveryOptions} from '../constants/commissionMode';
import {states} from '../constants/states';
import moment from 'moment';

class VendSlip extends React.Component {
    constructor(props) {
        super(props);
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));
        this.state = {
            isVisible: this.props.isVisible,
            pageTitle: 'Vending Slip',
            vendCode: this.props.match.params.vendCode,
            role: currentUser.role,
            vendDetails: {}
        };
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(servicesActions.fetchVendingDetails(this.state.vendCode));
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ pageTitle: 'Vending Slip' });

            if (this.props.serviceManagement.vendDetails !== undefined) {
                console.log(this.props.serviceManagement.vendDetails)
                this.setState({ vendDetails: this.props.serviceManagement.vendDetails });
            }
        }
    }

    print = () => {
        window.print();
    }

    render() {
        let extraElectricity = "";
        let extraCustomerDetails = "";
        let extraAgentInfo = "";        
        let epinsDetails = "";
        if(this.state.role != roles.REGULAR_CUSTOMER)
        {
            extraAgentInfo = <div>
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Dealer Name:</p>
                    <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.agentName}</p>
                </div>
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Dealer Code:</p>
                    <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.dealerCode}</p>
                </div>
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Dealer Address:</p>
                    <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.dealerAddress}</p>
                </div>
            </div>
        }
	
        if (this.state.vendDetails.type == "Electricty") {
            extraElectricity = <div>
                        <div className="row">
                            <p className="col-sm text-muted mb-0 mb-sm-3">Token:</p>
                    <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.pin == null ? "N/A" : this.state.vendDetails.pin}</p>
                        </div>
                        <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Quantity:</p>
                    <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.quantity == null ? "N/A" : this.state.vendDetails.quantity}</p>
                        </div>
                    </div>
        }

        

        if(this.state.vendDetails.type == "Food" )
        {
            var delOption = deliveryOptions.find(x=>x.key == this.state.vendDetails.deliveryOption)?.value ?? "Delivery"
            var state = states.find(x => x.state.id == this.state.vendDetails.state).state;
            var lga = state.locals.find(x=>x.id == this.state.vendDetails.lgs);

            var deliveryAddress = this.state.vendDetails.accountAddress;
            if(this.state.vendDetails.deliveryAddressLine2){
                deliveryAddress += ", "+this.state.vendDetails.deliveryAddressLine2;
            }
            deliveryAddress += ", " + lga?.name + ", " + state?.name

            var deliveryDate = "N/A";
            if(this.state.vendDetails.isSharedProduct){
                deliveryDate =  moment(this.state.vendDetails.deliveryDate).format("DD ddd MMM yyyy");
            }

            extraCustomerDetails = 
            <div>
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Service:</p>
                    <p className="col-sm text-sm-right font-weight-500">(Affordable Farms) {this.state.vendDetails.service}</p>
                </div>                                 
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Quantity:</p>
                    <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.numberOfPins}{this.state.vendDetails.unitType}</p>
                </div>  
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Customer Name:</p>
                    <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.accountName}</p>
                </div>          
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Delivery Address:</p>
                    <p className="col-sm text-sm-right font-weight-500">{deliveryAddress}</p>
                </div>
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Collection Point:</p>
                    <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.collectionPoint}</p>
                </div>

                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Delivery Date:</p>
                    <p className="col-sm text-sm-right font-weight-500">{deliveryDate}</p>
                </div>

                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Preferred Delivery Option:</p>
                    <p className="col-sm text-sm-right font-weight-500">{delOption}</p>
                </div>                   
            </div>                            
        }

        if(this.state.vendDetails.type == "Cable TV" || this.state.vendDetails.type == "Electricty" )
        {
            extraCustomerDetails = 
            <div>
                 <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Customer Name:</p>
                    <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.accountName}</p>
                </div>
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Customer Address:</p>
                    <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.accountAddress}</p>
                </div>
            </div>            
        }

        if(this.state.vendDetails.type == "E-Pins")
        {
            var pins = this.state.vendDetails.pins;
            let pinsView = [];
            if(pins.length > 0)
            {
                var i = 1;
                pins.map((x)=>{                    
                    pinsView.push(
                    <div>
                        <hr />
                        <div className="row">
                            <p className="col-sm text-muted mb-0 mb-sm-3">Serial Number({i}):</p>
                            <p className="col-sm text-sm-right font-weight-500">{x.serialNumber}</p>
                        </div>                            
                        <div className="row">
                            <p className="col-sm text-muted mb-0 mb-sm-3">Pin({i}):</p>
                            <p className="col-sm text-sm-right font-weight-500">{x.pin}</p>
                        </div>
                        <hr />
                    </div>)
                    {i++;}
                });
            }
            epinsDetails = 
            <div>
                {pinsView}
            </div>
        }

        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <div className="col-lg-11 mx-auto">
                    <div className="col-lg-12 text-center mt-5">
                        <h2 className="text-8">Vending Slip</h2>
                        <p className="lead">Transaction: {this.state.vendDetails.status}</p>
                    </div>
                    <div className="col-md-8 col-lg-6 col-xl-5 mx-auto">
                        <div className="bg-light shadow-sm rounded p-3 p-sm-4 mb-0 mb-sm-4">
                            <div className="row">                                
                                <p className="col-sm text-center text-muted mb-0 mb-sm-12">
                                    <img width="25%" src={this.state.vendDetails.vendorLogo} /><br />
                                    {this.state.vendDetails.transactionDate}
                                </p>
                            </div>
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Vending Code:</p>
                                <p className="col-sm text-sm-right font-weight-500">{this.state.vendCode}</p>
                            </div>
                            {extraAgentInfo}
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Customer Ref:</p>
                                <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.customerNumber}</p>
                            </div>                            
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Amount:</p>
                                <p className="col-sm text-sm-right font-weight-500">&#8358;{this.state.vendDetails.amount !== undefined? numeral(this.state.vendDetails.amount.toFixed(2)).format("0,00.00") : this.state.vendDetails.amount}</p>
                            </div>
                            <div className="row" hidden={this.state.vendDetails.type != "Food"}>
                                <p className="col-sm text-muted mb-0 mb-sm-3">Delivery Fee:</p>
                                <p className="col-sm text-sm-right font-weight-500">₦{numeral(this.state.vendDetails.deliveryFee ?? 0.00).format("0,00.00")}</p>
                            </div>
                            <div className="row" hidden={this.state.vendDetails.type != "Food"}> 
                                <p className="col-sm text-muted mb-0 mb-sm-3">Total:</p>
                                <p className="col-sm text-sm-right font-weight-500">₦{numeral(this.state.vendDetails.amount + this.state.vendDetails.deliveryFee).format("0,00.00")}</p>
                            </div>
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Vending Status:</p>
                                <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.status}</p>
                            </div>
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Service:</p>
                                <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.service}</p>
                            </div>
                            {extraCustomerDetails}
                            {extraElectricity}
                            {epinsDetails}
                            <div className="row">                                
                                <p className="col-sm text-center text-muted mb-0 mb-sm-12">
                                    <img src="images/logo.png"/>
                                </p>
                            </div>
                            
                            <p className="mt-4 mb-0">
                                 <button onClick={this.print} disabled={this.state.vendDetails == {}} className="btn btn-primary btn-block">Print</button>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, serviceManagement } = state;
    return {
        alert, serviceManagement
    };
}

const connectedVendSlip = connect(mapStateToProps)(VendSlip);
export { connectedVendSlip as VendSlip };
