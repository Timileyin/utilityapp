import React from 'react';
import $ from 'jquery';
import { connect } from 'react-redux';
import { servicesActions } from '../actions/servicesAction';
import { vendorService } from '../services/vendorService';
import { alertActions } from '../actions/alertActions';
import {states} from '../constants/states';

window.$ = window.jQuery = $;
require('easy-responsive-tabs');


class Food extends React.Component {
    
    constructor(props) {
        super(props);

        this.state = {
            isVisible: this.props.isVisible,
            serviceTypeKey: this.props.serviceTypeKey,
            vendors: this.props.vendors,
            vendorServices: [],
            service: {},
            amount: null,
            customerNumber: null,
            unitToPurchase: 1,
            totalAmount: 0.00.toFixed(2),
            deliveryAddress: ''
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    handleCreateChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        if (name === 'vendor') {
            var vendorServices = theState.vendors.find(x => x.id == value).serivces.filter(x => x.serviceTypeInt == this.state.serviceTypeKey);            
            theState.vendorServices = vendorServices;
        }

        if (name === 'service') {
            var selectedService = theState.vendorServices.find(x => x.id == value);

            if (selectedService.isFixedPrice) {
                theState.unitToPurchase = 1;
                theState.amountPerUnit = selectedService.amount.toFixed(2) + " / " + (selectedService.isMeasuredService ? selectedService.unitType : "");
                theState.amount = selectedService.amount.toFixed(2);
                theState.totalAmount = selectedService.amount.toFixed(2);
            }
            theState.service = selectedService
        }

        if(name === 'deliveryOption'){
            theState.deliveryOption = value;
            if(value == 2){
                theState.collectionPoint = "";
            }
        }

        if(name === 'unitToPurchase'){
            if(!isNaN(value)){
                theState.unitToPurchase = value;
                theState.totalAmount = (theState.amount * value).toFixed(2);                
            }else{
                alertActions.error("Please provide a valid number", "Error occured");
            }            
        }

        if(name === 'deliveryAddress1'){
            theState.deliveryAddress1 = value;
        }

        if(name === 'customerName'){
            console.log(value);
            theState.accountName = value;
        }

        if(name === 'deliveryAddress2'){
            theState.deliveryAddress2 = value;
        }

        if(name === 'collectionPoint'){
            theState.collectionPoint = value;
        }

        if(name === 'phoneNumber'){
            theState.customerNumber = value;
        }

        if (name === 'state') {
            theState.state = value;
            theState.lga = "";
        }

        if (name === 'lga') {
            theState.lga = value;            
        }
        
        this.setState(theState);
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (this.props !== prevprops) {
            this.setState({ isVisible: this.props.isVisible, serviceTypeKey: this.props.serviceTypeKey, vendors: this.props.vendors });
        }
    }

    initiateVendService = () => {
        if (window.confirm("Are you sure you want to continue?")) {
            const { dispatch } = this.props;
            console.log(this.state);
            dispatch(servicesActions.initiateVending(this.state.service.id, this.state.customerNumber, this.state.totalAmount,this.state.unitToPurchase, this.state.accountName, this.state.deliveryAddress1, this.state.deliveryAddress2, this.state.collectionPoint, this.state.deliveryOption, this.state.state, this.state.lga));
        }
    }

    recharge = () => {
        if (window.confirm("Are you sure you want to continue?")) {

        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document
    }

    render() {

        var vendorsOptions = [];
        this.state.vendors.map(s => {
            vendorsOptions.push(<option value={s.id}>{s.name}</option>);
        });

        var serviceOptions = [];
        this.state.vendorServices.map(s => {
            serviceOptions.push(<option value={s.id}>{s.name}</option>);
        });

        var lgas =  [];
        if(this.state.state) {
            lgas = states.find(x=>x.state.id == this.state.state).state.locals;  
        } 

        return (

            <div style={!this.state.isVisible ? { display: 'none' } : null}>
                <h2 className="text-6 mb-4">Foods</h2>
                <form id="recharge-bill" method="post">
                    <div className="mb-3">

                        <div className="form-group">
                            <label htmlFor="operator">Select Food Type</label>
                            <select className="custom-select" name="vendor" onChange={this.handleCreateChange} id="vendor" required="">
                                <option value="">Select Your Food Vendor</option>
                                {vendorsOptions}
                            </select>
                        </div>

                        <div className="form-group">
                            <label htmlFor="operator">Select Product Type</label>
                            <select className="custom-select" name="service" onChange={this.handleCreateChange} id="service" required="">
                                <option value="">Select Your Product Type</option>
                                {serviceOptions}
                            </select>
                        </div>


                    </div>
                    <div className="form-group">
                        <label htmlFor="mobileNumber">Price per unit</label>
                        <div className='input-group'>
                            <div className="input-group-prepend"> <span className="input-group-text">&#8358;</span> </div>
                            <input type="text" className="form-control" data-bv-field="number" name="pricePerunit" id="pricePerunit" value={this.state.amountPerUnit} disabled={this.state.service.isFixedPrice} required placeholder="Price per unit" />
                        </div>  
                    </div>

                    <div className="form-group" hidden={this.state.service.isSharedProduct}>
                        <label htmlFor="amount">Unit to Purchase</label>
                        <input type="text" value={this.state.unitToPurchase} className="form-control" onChange={this.handleCreateChange} data-bv-field="number" name="unitToPurchase" id="mobileNumber" required placeholder="Unit to purchase" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="amount">Customer's Name</label>
                        <input type="text" className="form-control" onChange={this.handleCreateChange} data-bv-field="number" name="customerName" id="mobileNumber" required placeholder="Customer name" />
                    </div>

                    <div className="form-group">
                        <label htmlFor="amount">Delivery Address Line 1</label>
                        <input type="text" className="form-control" name="deliveryAddress1" onChange={this.handleCreateChange} data-bv-field="number" id="deliveryAddress" required placeholder="Delivery Address"/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="amount">Delivery Address Line 2</label>
                        <input type="text" className="form-control" name="deliveryAddress2" onChange={this.handleCreateChange} data-bv-field="number" id="deliveryAddress" required placeholder="Delivery Address"/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="amount">State</label>
                        <select className="custom-select" name="state" onChange={this.handleCreateChange} id="service" required="">
                            <option value="">Select State</option>                            
                                {states.map(s=>(
                                    <option value={s.state.id}>{s.state.name}</option>
                                ))}
                        </select>
                    </div>

                    <div className="form-group">
                        <label htmlFor="amount">LGA</label>
                        <select className="custom-select" name="lga" onChange={this.handleCreateChange} id="service" required="">
                            <option value="">Select LGA</option>
                            {lgas.map(s=>(
                                    <option value={s.id}>{s.name}</option>
                                ))}
                        </select>
                    </div>

                    <div className="form-group" hidden={this.state.service.deliveryTypeInt == 2 || this.state.deliveryOption == 2}>
                        <label htmlFor="amount">Collection Point</label>
                        <select className="custom-select" name="collectionPoint" onChange={this.handleCreateChange} id="service" required="">
                            <option value="">Select Your Collection Point</option>
                            <option value="Agege">Agege</option>
                            <option value="Berger">Berger</option>
                            <option value="Oshodi">Oshodi</option>
                        </select>
                    </div>

                    <div className="form-group" hidden={this.state.service.deliveryTypeInt !== 3 }>
                        <label htmlFor="amount">Preferred Delivery Option</label>
                        <select className="custom-select" name="deliveryOption" onChange={this.handleCreateChange} id="service" required="">
                            <option value="">Select Your Product Type</option>
                            <option value="1">Collection Point</option>
                            <option value="2">Courier / Delivery</option>
                        </select>
                    </div>

                    <div className="form-group">
                        <label htmlFor="amount">Phone number</label>
                        <input type="text" className="form-control" data-bv-field="number" id="mobileNumber" onChange={this.handleCreateChange} name="phoneNumber" required placeholder="Phone number"/>
                    </div>

                    <div className="form-group">
                        <label htmlFor="amount">Total Amount</label>
                        <div className='input-group'>
                            <div className="input-group-prepend"> <span className="input-group-text">&#8358;</span></div>
                            <input type="text" className="form-control" data-bv-field="number" value={this.state.totalAmount} id="mobileNumber" required placeholder="Enter Mobile Number" />
                        </div>                        
                    </div>

                    <button className="btn btn-primary btn-block" type="button" onClick={this.initiateVendService}>Proceed</button>
                </form>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, serviceManagement } = state;

    return {
        alert, serviceManagement
    };
}

const connectedAirtimePins = connect(mapStateToProps)(Food);
export { connectedAirtimePins as Food };
