import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import {userTypes} from '../constants/rolesConstants';
import { Table, Button, Badge, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, Collapse, Card, CardBody, CardTitle } from 'reactstrap';
import { status } from '../constants/commissionMode';
import {states} from '../constants/states';
import jQuery from 'jquery';
export default class CreateEditPartner extends React.Component {

    constructor(props) {
        super(props);   
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));     
        this.state = {
            isOpen: this.props.isOpen,
            registrationInfo: this.props.registrationInfo,
            userType: null,
            role: currentUser.role
        };

        if(this.state.role == "SUPER DEALER")
        {
            userTypes = ["DEALER", "AGENT"]
        } 

        if(this.state.role == "DEALER")
        {
            userTypes = ["AGENT"]
        }

        if(this.state.role == "DEALER"){
            userTypes = []
        }
    }

    handleCreateChange = (e) => {
        const { name, value } = e.target;       
        var theState = { ...this.state };
        if(name == "userType")
        {
            theState.userType = value
        }
        this.setState(theState);
    }

    componentDidUpdate = (prevprops) => {
        if (prevprops !== this.props) {
            this.setState({ isOpen: this.props.isOpen, registrationInfo: this.props.registrationInfo,isCommissionAhead: this.props.registrationInfo?.isCommissionAhead })
        }
    }

    triggerLogoFileUploadChange = () => {
        jQuery("#logoFileUpload").trigger("click");
    }

    render() {
        var registrationInfo;
        if (this.state.registrationInfo == null) {
            registrationInfo = {};
        } else {
            registrationInfo = this.state.registrationInfo;
        }

        return (

            <Modal isOpen={this.state.registrationInfo != null} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    Confirm Partner Registration
                </ModalHeader>
                <ModalBody>
                    <Table>
                        <tbody>
                            <tr>
                                <th>Company Name</th>
                                <td>{registrationInfo.companyName}</td>
                            </tr>
                            <tr>
                                <th>Phone number</th>
                                <td>{registrationInfo.phonenumber}</td>
                            </tr>
                            <tr>                            
                                <th>Contact Person</th>
                                <td>{registrationInfo.firstname} {registrationInfo.surname}</td>
                            </tr>
                            <tr>                            
                                <th>Email Address</th>
                                <td>{registrationInfo.email} </td>
                            </tr>
                            <tr>                            
                                <th>Address</th>
                                <td>{registrationInfo.addressLine1+"/n"+registrationInfo.addressLine2+"\n"+states.find(x=>x.state.id == registrationInfo.state)?.state.locals.find(x=>x.id == registrationInfo.lga).name+", "+states.find(x=>x.state.id == registrationInfo.state)?.state.name} </td>
                            </tr> 
                            <tr>                           
                                <th>BVN</th>
                                <td>{registrationInfo.bvn}</td>
                            </tr>
                            <tr>                           
                                <th>Status</th>
                                <td>
                                    {registrationInfo.status == "Approved" ? <Badge color="success">{registrationInfo.status}</Badge> : <Badge color="danger">{registrationInfo.status}</Badge>}
                                </td>
                            </tr>
                            <tr>                           
                                <th>User Type</th>
                                <td>
                                    <Input onChange={this.handleCreateChange} name="userType" id="userType" type="select" bsSize="sm">
                                        <option>--Please choose one--</option>
                                        {
                                            userTypes.map((c) =>
                                                <option key={c} value={c}>{c}</option>
                                            )
                                        }
                                    </Input>
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => { this.props.onSave(registrationInfo.id, this.state.userType)}}>Save</Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}