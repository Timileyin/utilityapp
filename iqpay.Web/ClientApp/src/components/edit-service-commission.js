import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import { status, commissionMode, serviceTypes } from '../constants/commissionMode';
export default class EditServiceCommission extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            serviceInfo: this.props.serviceInfo,
            serviceName: this.props.serviceName,
            isFixedPrice: true//this.props.serviceInfo.isFixedPrice
        };

    }

    handleCreateChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };

        if (name === 'commission') {
            theState.serviceInfo.commission = value;
        }

        if (name === 'commissionCap') {
            theState.serviceInfo.commissionCap = value;
        }        

        if (name === 'customerCommissionCap') {
            theState.serviceInfo.customerCommissionCap = value;
        }

        if (name === 'customerCommission') {
            theState.serviceInfo.customerCommission = value;
        }

        if (name === 'agentCommission') {
            theState.serviceInfo.agentCommission = value;
        }

        if (name === 'agentCommissionCap') {
            theState.serviceInfo.agentCommissionCap = value;
        }

        if (name === 'superDealerCommission') {
            theState.serviceInfo.superDealerCommission = value;
        }

        if (name === 'superDealerCommissionCap') {
            theState.serviceInfo.superDealerCommissionCap = value;
        }

        if (name === 'dealerCommissionCap') {
            theState.serviceInfo.dealerCommissionCap = value;
        }

        if (name === 'dealerCommission') {
            theState.serviceInfo.dealerCommission = value;
        }

        this.setState(theState);
    }



    componentDidUpdate = (prevprops) => {

        if (prevprops !== this.props) {
            if (this.props.serviceInfo != null) {
                this.setState({ isOpen: this.props.isOpen, serviceInfo: this.props.serviceInfo, isFixedPrice: this.props.serviceInfo.isFixedPrice })
            } else {
                this.setState({ isOpen: this.props.isOpen, serviceInfo: this.props.serviceInfo });
            }
        }
    }

   


    render() {
        var serviceInfo;
        if (this.state.serviceInfo == null) {
            serviceInfo = {};
        } else {
            serviceInfo = this.state.serviceInfo;
        }

        return (

            <Modal isOpen={this.state.serviceInfo != null} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    Manage {this.state.serviceName} Commission Settings
                </ModalHeader>
                <ModalBody>
                    <Form>
                        <Row form>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">IQPAY Commission</Label>
                                    <Input value={serviceInfo.commission} type="text" onChange={this.handleCreateChange} name="commission" bsSize="sm" id="name" placeholder="Customer Commission" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">IQPAY Commission Cap</Label>
                                    <Input value={serviceInfo.commissionCap} type="text" onChange={this.handleCreateChange} name="commissionCap" bsSize="sm" id="name" placeholder="Customer Commission" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Household Users Comm.</Label>
                                    <Input value={serviceInfo.customerCommission} type="text" onChange={this.handleCreateChange} name="customerCommission" bsSize="sm" id="name" placeholder="Customer Commission" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Household Users Comm. Cap</Label>
                                    <Input  value={serviceInfo.customerCommissionCap} type="text" onChange={this.handleCreateChange} name="customerCommissionCap" bsSize="sm" id="price" placeholder="Price" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Super dealer commission</Label>
                                    <Input value={serviceInfo.superDealerCommission} type="text" onChange={this.handleCreateChange} name="superDealerCommission" bsSize="sm" id="name" placeholder="Service name" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Super dealer Commission Cap</Label>
                                    <Input  value={serviceInfo.superDealerCommissionCap} type="text" onChange={this.handleCreateChange} name="superDealerCommissionCap" bsSize="sm" id="price" placeholder="Price" />
                                </FormGroup>
                            </Col>  
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Dealer commission</Label>
                                    <Input value={serviceInfo.dealerCommission} type="text" onChange={this.handleCreateChange} name="dealerCommission" bsSize="sm" id="name" placeholder="Service name" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Dealer Commission Cap</Label>
                                    <Input  value={serviceInfo.dealerCommissionCap} type="text" onChange={this.handleCreateChange} name="dealerCommissionCap" bsSize="sm" id="price" placeholder="Price" />
                                </FormGroup>
                            </Col>   
                            
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Agent commission</Label>
                                    <Input  value={serviceInfo.agentCommission} type="text" onChange={this.handleCreateChange} name="agentCommission" bsSize="sm" id="price" placeholder="Price" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Agent commission Cap</Label>
                                    <Input value={serviceInfo.agentCommissionCap} type="text" onChange={this.handleCreateChange} name="agentCommissionCap" bsSize="sm" id="price" placeholder="Price" />
                                </FormGroup>
                            </Col>  
                        </Row>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => { this.props.onSave(this.state.serviceInfo.id, this.state.serviceInfo) }}>Save</Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}