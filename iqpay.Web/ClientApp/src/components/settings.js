import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import {roles} from '../constants/rolesConstants';
import { connect } from 'react-redux';
import { userActions } from '../actions/userAction';
import {Input, Card, Col, Button, Collapse, CardBody, Form, FormGroup, Label} from 'reactstrap';
import jQuery from 'jquery';
import { alertActions } from '../actions/alertActions';

class Settings extends React.Component {
    constructor(props) {
        super(props);
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));
        this.state = {
            isVisible: this.props.isVisible,
            pageTitle: 'Settings',
            role: currentUser.role,
            settings: {}
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if(this.props.authentication.profileInformation !== undefined){    
                if(this.props.authentication.profileInformation !== null){
                    this.setState({profileInfo: this.props.authentication.profileInformation});
                }                                            
            }            
        }
    }

    handleCreateChange = (e) => {

        const { name, value } = e.target;
        var theState = { ...this.state };

        if(name == 'firstName'){
            theState.profileInfo.firstName = value;
        }

        if(name == 'lastName'){
            theState.profileInfo.lastName = value;
        }

        if(name == 'phoneNumber'){
            theState.profileInfo.phoneNumber = value;
        }

        if(name == 'name'){
            theState.profileInfo.name = value;
        }

        if(name == 'houseHoldBonusEnable')
        {
            theState.settings.houseHoldBonusEnable = !theState.settings.houseHoldBonusEnable
        }


        if(name == 'address'){
            theState.profileInfo.address = value;
        }
        
        this.setState(theState);
    }

    updateUserInfo = (userInfo) => {
        const { dispatch } = this.props;
        dispatch(userActions.updateUserInfo(userInfo));
        //const{dispatch} = this.props;
        dispatch(alertActions.clear());
    }    

    triggerFileUpload = () =>{
        jQuery("#inputProfileUpload").trigger("click");        
    }    

    componentDidMount() {
        //This method is called when the component is first added to the document
        const { dispatch } = this.props;
        dispatch(userActions.getUserInfo());        
    }

    render() {
        var settings = this.state.settings;
        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="bg-light shadow-md rounded p-4"> 
                
                            <div className="row">      
                                <div className="col-lg-8">  
                                <div>
                                    <input type="checkbox" checked={settings.houseHoldBonusEnable} name="houseHoldBonusEnable" onChange={this.handleCreateChange}/> Enable Household Bonus                                                                       
                                    <Collapse isOpen={settings.houseHoldBonusEnable}>
                                    <Card>
                                        <CardBody>
                                        <Col md={12}>                                        
                                            <FormGroup>
                                                <Label for="examplePassword">Drop Bonus in commission wallet</Label>
                                                <Input type="number"  name="dropBonus" id="description" onChange={this.handleCreateChange} value={settings.description} bsSize="sm" placeholder="Percentage Bonus"></Input>
                                            </FormGroup>
                                            <FormGroup>
                                                <Label for="examplePassword">Use bonus as discount</Label>
                                                <Input type="number"  name="dropBonus" id="description" onChange={this.handleCreateChange} value={settings.description} bsSize="sm" placeholder="Percentage Bonus"></Input>
                                            </FormGroup>
                                        </Col>
                                        </CardBody>
                                    </Card>
                                    </Collapse>
                                </div>
                                </div>                                                                                                                                                                            
                            </div>
                            
                        </div>
                    </div>
                                
                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, authentication } = state;
    return {
        alert, authentication
    };
}

const connectedSettings = connect(mapStateToProps)(Settings);
export { connectedSettings as Settings };
