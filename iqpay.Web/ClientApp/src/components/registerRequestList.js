import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { superdealerActions } from '../actions/superdealerAction';
import { connect } from 'react-redux';
import CreateEditDealer from './create-edit-dealer';
import CommissionRate from './commission-rates';
import {dealersActions} from '../actions/dealersAction';
import {userActions} from '../actions/userAction';
import { superdealerConstants } from '../constants/dealerConstants';
import VendorCheckList from './vendor-checklist';
import numeral from 'numeral';
import DataTable from 'react-data-table-component';
import {downloadCSV} from '../helpers/history';
import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";
import {Badge} from 'reactstrap';
import CreateEditPartner from './create-edit-partner';
import {states} from '../constants/states';

import jQuery from 'jquery';
class RegisterRequestList extends React.Component {
    modalpopup = true;
    columns = [
        {
            name: 'BVN',
            button: true,
            cell: row => <a href="" onClick={(event) => this.requestDetails(event, row.id)}>{row.bvn}</a>,
        },
        {
            name: 'Firstname',
            selector: 'firstname',
            sortable: true,
        },
        {
            name: 'Lastname',
            selector: 'surname',
            sortable: true,
        },
        {
            name: 'Company Name',
            selector: 'companyName',
            sortable: true,
        },        
        {
            name: 'Email',
            selector: 'email',
            sortable: true,
        },
        {
            name: 'State',            
            sortable: true,
            cell: row => states.find(x=>x.state.id == row.state).state.name            
        },
        {
            name: 'LGA',
            cell: row => states.find(x=>x.state.id == row.state).state.locals.find(x=>x.id == row.lga).name,  
            sortable: true,
        },
        {
            name: 'Status',
            sortable: false,
            cell: row => row.status == "Approved" ? <Badge color="success">{row.status}</Badge> : <Badge color="danger">{row.status}</Badge>,            
        },
        {
            name: 'Address (Line1)',
            selector: 'addressLine1',
            sortable: true
        },{
            name: 'Address (Line2)',
            selector: 'addressLine2',
            sortable: true
        },                
        {
            name: 'NIN',
            selector: 'nin',
            sortable: false,
        },
        {
            name: 'Action',
            sortable: false,
            cell: row => <span><a href={'/bvnImages/'+row.bvn+'.jpg'}>View BVN Image</a><a href={'/photoIds/'+row.bvn+'.jpg'}>View Photo ID</a></span>,            
        },   
    ]
    constructor(props) {
        super(props);
        console.log(states);
        this.state = {

            pageTitle: 'Manage Partnership Request',
            requests: [],
            requestInfo: null,
            modalPopup: false,
            commissionRates:null,
            checkListItems: null,
            requestId: null
        };
    }


    registerDealer = (id, dealerType) => {
        const { dispatch } = this.props;
        dispatch(userActions.registerDealer(id, dealerType));
        dispatch(userActions.viewRequestInfo(id));
    }

    onCancel = () => {
        this.setState({requestInfo:null})        
    }

    requestDetails = (e,id) => {
        e.preventDefault();
        const { dispatch } = this.props;
        dispatch(userActions.viewRequestInfo(id));
    }

    toggleActivate = (id, status) => {
        if (window.confirm("Are you sure you want to complete this task?")) {
            const { dispatch } = this.props;
            dispatch(superdealerActions.toggleActivate(id, status));
        }
    }

    saveDealer = (dealerInfo) => {
        const { dispatch } = this.props;
        dispatch(superdealerActions.saveDealer(dealerInfo));
        //this.setState({ vendorInfo: null });
    }

    updateCheckListItems(checkListItems){
        if(window.confirm("Are you sure you want to update this vendor checklist")){
            const {dispatch} = this.props;
            dispatch(dealersActions.updateCheckListItems(this.state.dealerId, checkListItems));
        }                
    }

    showServiceChecklist = (dealerId) => {
        const{dispatch} = this.props;
        this.setState({dealerId: dealerId});
        dispatch(dealersActions.fetchServiceChecklist(dealerId))
    }

    closeCheckListModal = () => {
        const{dispatch} = this.props;
        dispatch(dealersActions.resetserviceChecklist());
        this.setState({checkListItems: null});
        this.setState({checkListItems: null});
    }


    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if (this.props.authentication.partnerRequests !== undefined) {
                this.setState({ requests: this.props.authentication.partnerRequests });
            }   
            
            if(this.props.authentication.requestInfo !== undefined)
            {
                this.setState({requestInfo: this.props.authentication.requestInfo})
            }             
        }
    }

    editDealer = (dealerId) => {
        const { dispatch } = this.props;
        dispatch(superdealerActions.fetchDealerDetails(dealerId));
    }

    addDealer = () => {
        this.setState({ superdealerInfo: {} });
    }

    commissionRate = (dealerId) =>{
        const {dispatch} = this.props;
        dispatch(dealersActions.fetchCommissionRate(dealerId));
    }

    closeModal = () => {        
        this.setState({ superdealerInfo: null });
    }

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document

        const { dispatch } = this.props;
        dispatch(userActions.fetchPartnershipRequests());
    }
    
    render() {
        var requests = this.state.requests;  
        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                        <Row>
                                            <Col>
                                                <DataTable
                                                    title="Parternship Request List"
                                                    columns={this.columns}
                                                    data={requests}
                                                    actions={<Button className="btn btn-sm" onClick={()=>downloadCSV(this.state.logs)}>Export</Button>}
                                                    pagination
                                                    paginationPerPage={30}
                                                />
                                            </Col>
                                        </Row>
                                        <CreateEditPartner onSave={(id, dealerType) => this.registerDealer(id, dealerType)} onCancel={()=>this.onCancel()}  registrationInfo={this.state.requestInfo}/>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, authentication } = state;

    return {
        alert, authentication
    };
}

const connectedDealersPage = connect(mapStateToProps)(RegisterRequestList);
export { connectedDealersPage as RegisterRequestList };