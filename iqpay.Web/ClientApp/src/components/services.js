﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { vendorActions } from '../actions/vendorAction';
import { connect } from 'react-redux';
import CreateEditService from './create-edit-service';
import { servicesActions } from '../actions/servicesAction';
import { commissionMode } from '../constants/commissionMode';
import { service } from '../services/service';
import ManageServiceCodes from './manage-service-codes';
import EditServiceCommission from './edit-service-commission'; 

class Services extends React.Component {
    modalpopup = true
    constructor(props) {
        super(props);

        this.state = {

            pageTitle: 'Manage Services',
            services: [],
            vendorId: this.props.match.params.vendorId,
            serviceInfo: null,
            serviceCodes: null,
            serviceCommission: null
        };
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change

        console.log(this.props.serviceManagement.serviceCodes);
        if (prevprops !== this.props) {
            if (this.props.serviceManagement.services !== undefined) {
                this.setState({ services: this.props.serviceManagement.services });
            }

            if (this.props.serviceManagement.serviceInfo !== undefined) {
                if (this.props.serviceManagement.serviceInfo != null) {
                    this.props.serviceManagement.serviceInfo.isFixedPrice = !this.props.serviceManagement.serviceInfo.isFixedPrice;

                    this.setState({ serviceInfo: this.props.serviceManagement.serviceInfo })
                }
                                
            }

            if(this.props.serviceManagement.serviceCommission !== undefined){
                if (this.props.serviceManagement.serviceCommission != null) {                    
                    this.setState({ serviceCommission: this.props.serviceManagement.serviceCommission })
                }
            }

            if (this.props.serviceManagement.serviceCodes !== undefined) {
                if (this.props.serviceManagement.serviceCodes != null) {
                    this.setState({ serviceCodes: this.props.serviceManagement.serviceCodes });
                }
            }

        }
    }

    manageServiceCommission = (serviceId) => {
        const {dispatch} = this.props;
        dispatch(servicesActions.fetchServiceCommission(serviceId));
    }

    fetchServiceCodes = (serviceId) => {
        const { dispatch } = this.props;
        dispatch(servicesActions.fetchServiceCodes(serviceId));
    }

    saveServiceCodes = (serviceId, serviceCodes) => {
        const { dispatch } = this.props;
        dispatch(servicesActions.saveServiceCodes(serviceId, serviceCodes));
    }


    editService = (serviceId) => {
        const { dispatch } = this.props;
        dispatch(servicesActions.fetchService(serviceId));
    }

    loadServiceCodes = (serviceId) => {
        const { dispatch } = this.props;
        dispatch(servicesActions.fetchServiceCodes(serviceId));
    }

    addService = () => {
        this.setState({ serviceInfo: { isFixedPrice: false, vendorId: this.state.vendorId } });
    }

    closeModal = () => {
        this.setState({ serviceInfo: null });
        this.setState({serviceCommission: null});
    }

    closeServiceCodeModal = () => {
        this.setState({ serviceCodes: null });
    }

    saveServiceCommission = (serviceId, service) => {
        const{dispatch} = this.props;
        dispatch(servicesActions.saveServiceCommission(serviceId, service));
    }

    saveService = (service) => {
        service.isFixedPrice = !service.isFixedPrice;
        const { dispatch } = this.props;
        dispatch(servicesActions.saveService(service));
    }

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document

        const { dispatch } = this.props;
        if (this.state.vendorId !== undefined) {
            dispatch(servicesActions.fetchServices(this.state.vendorId));
        }

    }
    render() {
        var services = this.state.services;
        let serviceTable = [];
        if (services.length > 0) {
            services.map((s) => {
                serviceTable.push(
                    <tr>
                        <td><a onClick={() => { this.loadServiceCodes(s.id)}}>{s.name}</a></td>
                        <td>{s.isFixedPrice ? s.amount : "N/A"}</td>
                        <td>{s.serviceType}</td>
                        <td>{s.vendorName}</td>
                        <td>{s.commissionModeStr}</td>
                        <td>{s.status}</td>
                        <td>
                            <Button color="info" onClick={() => { this.editService(s.id) }} className="pull-right" size="sm"><i class="fas fa-edit"></i> Edit</Button>

                            <Button color="info" onClick={() => { this.manageServiceCommission(s.id) }} className="pull-right" size="sm"><i class="fas fa-money-check"></i> Comm. Settings</Button>
                        </td>

                    </tr>);
            });
        } else {
            serviceTable = <tr><td colspan='7'>Services have not been setup</td></tr>;
        }

        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">

                                        <Row>
                                            <Col>
                                                <Button color="" onClick={this.addService} className="pull-right" size="sm"><i className="fas fa-plus"></i> Create Service</Button>
                                                <Table striped hover size="md">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Price</th>
                                                            <th>Type</th>
                                                            <th>Vendor</th>
                                                            <th>Mode</th>
                                                            <th>Status</th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        {serviceTable}

                                                    </tbody>
                                                </Table>
                                            </Col>
                                            <CreateEditService onSave={this.saveService} onCancel={this.closeModal} serviceInfo={this.state.serviceInfo} />

                                            <EditServiceCommission onSave={this.saveServiceCommission} onCancel={this.closeModal} serviceInfo={this.state.serviceCommission} />

                                            <ManageServiceCodes onSave={this.saveServiceCodes} onCancel={this.closeServiceCodeModal} serviceCodes={this.state.serviceCodes} />
                                        </Row>


                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    console.log(state);
    const { alert, serviceManagement } = state;
    
    return {
        alert, serviceManagement
    };
}

const connectedServicesPage = connect(mapStateToProps)(Services);
export { connectedServicesPage as Services };
