﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { processorActions, processorAction } from '../actions/processorAction';
import { connect } from 'react-redux';
import CreateEditProcessor from './create-edit-processor';
import { processorConstants } from '../constants/processorConstants';

class Processors extends React.Component {
    modalpopup = true
    constructor(props) {
        super(props);

        this.state = {

            pageTitle: 'Manage Processors',
            processors: [],
            processorInfo: null,
            modalPopup: false
        };


    }

    saveProcessor = (processorInfo) => {
        const { dispatch } = this.props;
        dispatch(processorAction.saveProcessor(processorInfo));
        //this.setState({ vendorInfo: null });
    }


    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        console.log(this.props.processors.processors);
        if (prevprops !== this.props) {
            if (this.props.processors.processors !== undefined) {
                this.setState({ processors: this.props.processors.processors });
            }

            console.log(this.props.processors.processorInfo);
            if (this.props.processors.processorInfo !== undefined) {
                this.setState({ processorInfo: this.props.processors.processorInfo})
            }

        }
    }

    editProcessor = (processorId) => {
        const { dispatch } = this.props;
        dispatch(processorAction.fetchProcessor(processorId));
    }

    addProcessor = () => {
        this.setState({ processorInfo: {} });
    }

    closeModal = () => {
        this.setState({ processorInfo: null });
    }

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document

        const { dispatch } = this.props;
        dispatch(processorAction.fetchProcessors());

    }
    render() {
        var processors = this.state.processors;
        let processorTable = [];
        if (processors.length > 0) {
            processors.map((v) => {
                processorTable.push(
                    <tr key={v.id}>
                        <td>{v.name}</td>                        
                        <td>{v.code}</td>
                        <td>{v.status}</td>
                        <td>{v.dateCreatedStr}</td>
                        <td>
                            <Button color="info" onClick={() => { this.editProcessor(v.id) }} className="pull-right" size="sm"><i class="fas fa-edit"></i> Edit</Button>
                        </td>

                    </tr>);
            });
        } else {
            processorTable = <tr><td colspan='5'>Processors have not been setup</td></tr>;
        }

        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">

                                        <Row>
                                            <Col>
                                                <Button color="" onClick={this.addProcessor} className="pull-right" size="sm"><i class="fas fa-plus"></i> Create Processor</Button>
                                                <Table striped hover size="md">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Code</th>
                                                            <th>Status</th>
                                                            <th>Date Created</th>                                                                                                                        
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                        {processorTable}

                                                    </tbody>
                                                </Table>
                                            </Col>
                                            <CreateEditProcessor onSave={this.saveProcessor} onCancel={this.closeModal} processorInfo={this.state.processorInfo} />
                                        </Row>


                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {
    console.log(state);
    const { alert, processors } = state;

    return {
        alert, processors
    };
}

const connectedVendorsPage = connect(mapStateToProps)(Processors);
export { connectedVendorsPage as Processors };
