import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../actions/userAction';
import queryString from 'query-string';
import { Link, Redirect } from 'react-router-dom';

class NewPassword extends React.Component {
    constructor(props) {
        super(props);
        const search = this.props.location.search;
        const params = queryString.parse(search);
        console.log(params.userId);
        this.state = {
            isVisible: this.props.isVisible,
            newPassword: "",
            confirmNewPassword: "",
            userId: params.userId,
            code: ""
        };

    }

    handleCreateChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        theState.userId = this.state.userId;
        if (name === 'newPassword') {
            theState.newPassword = value;
        }
        if (name === 'confirmNewPassword') {
            theState.confirmNewPassword = value;
        }
        if (name === 'code') {
            theState.code = value;
        }
        
        this.setState(theState);
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ isVisible: this.props.isVisible })
        }
    }



    resetPassword = (code, newPassword, confirmNewPassword) => {
        const {dispatch} = this.props;
        var code = this.state.code;
        var newPassword = this.state.newPassword;
        var confirmNewPassword = this.state.confirmNewPassword;   
        var userId = this.state.userId;   
        console.log(userId);  
        dispatch(userActions.resetPassword(userId, code, newPassword, confirmNewPassword));        
    }

    componentDidMount() {
        //This method is called when the component is first added to the document
    }

    render() {
        return (
            <div id="content">
                <section className="page-header page-header-text-light bg-secondary">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-md-8">
                                <h1>Login & Signup</h1>
                            </div>
                            <div className="col-md-4">
                                <ul className="breadcrumb justify-content-start justify-content-md-end mb-0">
                                    <li><Link to="/">Home</Link></li>
                                    <li className="active">New Password</li>
                                </ul>
                            </div>
                        </div>
                    </div>
            </section>
            <div className="container">
                <div id="login-signup-page" className="bg-light shadow-md rounded mx-auto p-4">
                <div className="form-group">
                        <label>Code</label>
                        <input type="text" className="form-control" name="code" onChange={(e)=>this.handleCreateChange(e) } id="loginMobile" required placeholder="Code" />
                    </div>                     
                    <div className="form-group">
                        <label>New password</label>
                        <input type="password" className="form-control" name="newPassword" onChange={(e)=>this.handleCreateChange(e) } id="loginMobile" required placeholder="New passowrd" />
                    </div>   
                    <div className="form-group">
                        <label>Confirm New password</label>
                        <input type="password" className="form-control" name="confirmNewPassword" onChange={(e)=>this.handleCreateChange(e) } id="loginMobile" required placeholder="Confirm password" />
                    </div>                  
                    <button className="btn btn-primary btn-block" onClick={this.resetPassword}>Change Password</button>
                </div>
            </div>
        </div>
        );
    }
}

function mapStateToProps(state) {
    const { loggingIn, signinUp, alert } = state;   
    return {
        loggingIn,
        signinUp,
        alert
    };
}

const connectedNewPassword = connect(mapStateToProps)(NewPassword);
export { connectedNewPassword as NewPassword }; 
