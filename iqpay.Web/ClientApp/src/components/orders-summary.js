import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import { connect } from 'react-redux';
import { history } from '../helpers/history';
import { config } from '../config';
import {roles} from '../constants/rolesConstants';
import { servicesActions } from '../actions/servicesAction';
import numeral from 'numeral';
import {deliveryOptions} from '../constants/commissionMode';
import { states } from '../constants/states';


class OrderSummary extends React.Component {

  constructor(props) {
      super(props);
      var currentUser = JSON.parse(localStorage.getItem("currentUser"));
      this.state = {
          isVisible: this.props.isVisible,
          pageTitle: 'Confirm Vending',
          vendCode: this.props.match.params.vendCode,
          role: currentUser.role,
          vendDetails: {},
          disableCompleteButton: false,
          newStatusInt: 0
      };
  }  

  componentDidMount() {
    const { dispatch } = this.props;
    dispatch(servicesActions.fetchVendingDetails(this.state.vendCode));
  }

  updateOrderStatus = () => {
    const { dispatch } = this.props;
    dispatch(servicesActions.processOrder(this.state.vendCode, this.state.newStatus));
    //dispatch(servicesActions.fetchVendingDetails(this.state.vendCode));
  }

  handleCreateChange = (e) => {
    const { name, value } = e.target;
    var theState = { ...this.state };
    if (name === 'newStatus') 
    {
        theState.newStatus = value
    }

    this.setState(theState);
  }

  componentDidUpdate = (prevprops) => {
    // This method is called when the route parameters change
    if (prevprops !== this.props) {
        this.setState({ pageTitle: 'Vending  Details' });
        if (this.props.serviceManagement.vendDetails !== undefined) {
            this.setState({ vendDetails: this.props.serviceManagement.vendDetails });
        }
    }
  }

  render() {
          var extraAgentInfo = "";
          if(this.state.role != roles.REGULAR_CUSTOMER)
          {
              extraAgentInfo = <div>
                  <div className="row">
                      <p className="col-sm text-muted mb-0 mb-sm-3">Dealer Name:</p>
                      <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.agentName}</p>
                  </div>
                  <div className="row">
                      <p className="col-sm text-muted mb-0 mb-sm-3">Dealer Code:</p>
                      <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.dealerCode}</p>
                  </div>
                  <div className="row">
                      <p className="col-sm text-muted mb-0 mb-sm-3">Dealer Address:</p>
                      <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.dealerAddress}</p>
                  </div>
              </div>
          }
          var delOption = deliveryOptions.find(x=>x.key == this.state.vendDetails.deliveryOption)?.value ?? "Delivery";
          var state = states.find(x => x.state.id == this.state.vendDetails.state).state;
          var lga = state.locals.find(x=>x.id == this.state.vendDetails.lga);

          var deliveryAddress = this.state.vendDetails.accountAddress;
          var deliveryAddress = this.state.vendDetails.accountAddress;
            if(this.state.vendDetails.deliveryAddressLine2){
                deliveryAddress += ", "+this.state.vendDetails.deliveryAddressLine2;
            }
            deliveryAddress += ", " + lga.name + ", " + state.name

          var extraCustomerDetails = 
            <div>
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Service:</p>
                    <p className="col-sm text-sm-right font-weight-500">(Affordable Farms) {this.state.vendDetails.service}</p>
                </div>                                 
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Quantity:</p>
                    <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.numberOfPins}{this.state.vendDetails.unitType}</p>
                </div>  
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Customer Name:</p>
                    <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.accountName}</p>
                </div>          
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Delivery Address:</p>
                    <p className="col-sm text-sm-right font-weight-500">{deliveryAddress}</p>
                </div>
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Collection Point:</p>
                    <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.collectionPoint}</p>
                </div>                
                <div className="row">
                    <p className="col-sm text-muted mb-0 mb-sm-3">Preferred Delivery Option:</p>
                    <p className="col-sm text-sm-right font-weight-500">{delOption}</p>
                </div>                   
            </div>;                            
    
      return (
        <div> 
          <div id="content">
              <div class="container">
                <div class="row my-5">
                  <div class="col-lg-11 mx-auto">
                      <div class="row widget-steps">
                          <div class={this.state.vendDetails.statusInt >= 7 ? "col-3 step complete" : "col-3 step disabled" }>
                            <div class="step-name">Order Placed</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="recharge-order.html" class="step-dot"></a>
                          </div>
                          
                          <div class={this.state.vendDetails.statusInt >= 8 ? "col-3 step complete" : this.state.vendDetails.status == 7 ? "col-3 step active" : "col-3 step disabled" }>
                            <div class="step-name">Order Processed</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="#" class="step-dot"></a>
                          </div>
                          
                          <div class={this.state.vendDetails.statusInt >= 9 ? "col-3 step complete" : this.state.vendDetails.status == 8 ? "col-3 step active" : "col-3 step disabled" }>
                            <div class="step-name">Order Shipped</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="#" class="step-dot"></a>
                          </div>
                          
                          
                          <div class={this.state.vendDetails.statusInt >= 10 ? "col-3 step complete" : this.state.vendDetails.status == 9 ? "col-3 step active" : "col-3 step disabled" }>
                            <div class="step-name">Order Completed</div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <a href="#" class="step-dot"></a>
                          </div>
                      </div>
                  </div>              
                  <div className="col-lg-11 mx-auto">
                    <div className="col-lg-12 text-center mt-5">
                      <h2 class="text-8">Order Summary</h2>
                    </div>
                    <div className="col-md-8 mx-auto">
                        <div className="bg-light shadow-sm rounded p-3 p-sm-4 mb-0 mb-sm-4">
                            <div className="row">                                
                                <p className="col-sm text-center text-muted mb-0 mb-sm-12">
                                    <img width="25%" src={this.state.vendDetails.vendorLogo} /><br />
                                    {this.state.vendDetails.transactionDate}
                                </p>
                            </div>
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Vending Code:</p>
                                <p className="col-sm text-sm-right font-weight-500">{this.state.vendCode}</p>
                            </div>
                            {extraAgentInfo}
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Customer Ref:</p>
                                <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.customerNumber}</p>
                            </div>
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Amount:</p>
                                <p className="col-sm text-sm-right font-weight-500">₦{numeral(this.state.vendDetails.amount).format("0,00.00")}</p>
                            </div>
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Delivery Fee:</p>
                                <p className="col-sm text-sm-right font-weight-500">₦{numeral(this.state.vendDetails.deliveryFee ?? 0.00).format("0,00.00")}</p>
                            </div>
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Total:</p>
                                <p className="col-sm text-sm-right font-weight-500">₦{numeral(this.state.vendDetails.amount + this.state.vendDetails.deliveryFee).format("0,00.00")}</p>
                            </div>
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Vending Status:</p>
                                <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.status}</p>
                            </div>                            
                            <div className="row" hidden={this.state.vendDetails.type == "Food"}>
                                <p className="col-sm text-muted mb-0 mb-sm-3">Service:</p>
                                <p className="col-sm text-sm-right font-weight-500">{this.state.vendDetails.service}</p>
                            </div>
                            {extraCustomerDetails}
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Change Order Status:</p>
                                <p className="col-sm text-sm-right font-weight-500">
                                    <select className="custom-select" name="newStatus" onChange={this.handleCreateChange} id="service" required="">
                                        <option value="">Select Order Status</option>
                                        <option value ="7">Order Placed</option>
                                        <option value ="8">Order Processed</option>
                                        <option value ="9">Order Shipped</option>
                                        <option value ="10">Order Received</option>
                                </select>
                                </p>
                            </div>
                            <div className="row">                                
                                <p className="col-sm text-center text-muted mb-0 mb-sm-12">
                                    <img src="images/logo.png"/>
                                </p>
                            </div>
                            <p className="mt-4 mb-0">
                                <button onClick={this.updateOrderStatus} disabled={this.state.vendDetails == {} || this.state.disableCompleteButton} className="btn btn-primary btn-block">Update Status</button>
                            </p>
                        </div>
                    </div>
                </div>                  
              </div>
            </div>
          </div>
        </div>
      );
  }

}

function mapStateToProps(state) {

  const { alert, serviceManagement } = state;

  return {
      alert, serviceManagement
  };
}

const connectedTransactionsPage = connect(mapStateToProps)(OrderSummary);
export { connectedTransactionsPage as OrderSummary };