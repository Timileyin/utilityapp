﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import { commissionMode, fundingMethods} from '../constants/commissionMode';
export default class FundBaseWallet extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            walletInfo: [],
            showFundPanel: false,
            fundingOption: 0,
            fundingInfo:[]

        };


    }


    componentDidUpdate = (prevprops) => {

        if (prevprops !== this.props) {
            this.setState({ isOpen: this.props.isOpen,fundingOption:0, showFundPanel: false, walletInfo: this.props.wallet })
        }
    }

    onFundingOptionChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        if (name == "fundingOption") {            
            theState.showFundPanel = true;
            theState.fundingOption = value;
            this.setState(theState);
        }
    }

    onFundingInfoChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        if (name == "accountNumber") {
            theState.fundingInfo.accountNumber = value;
        }

        if (name == "transactionRef")
        {
            theState.fundingInfo.transactionRef = value;
        }

        if (name == "transactionDate") {
            theState.fundingInfo.transactionDate = value;
        }

        if (name == "amount") {
            theState.fundingInfo.amount = value;
        }

        if (name == "description") {
            theState.fundingInfo.description = value;
        }

        if (name == "bankName") {
            theState.fundingInfo.bankName = value;
        }

        if (name == "depositorName") {
            theState.fundingInfo.depositorName = value;
        }

        this.setState(theState);

       
    }

   


    render() {
        var walletInfo = this.state.walletInfo;
        var fundingOption = [];
        var fundingInfo = this.state.fundingInfo;
        fundingInfo.walletId = walletInfo.id;
        fundingMethods.map(c => {
            fundingOption.push(
                <FormGroup key={c.key} check>
                    <Label check>
                        <Input onChange={this.onFundingOptionChange} type="radio" value={c.key} name="fundingOption" />
                        {c.value}
                  </Label>
                </FormGroup>
            )
        });
            
        return ( 
    
            <Modal isOpen={this.state.isOpen} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    Fund Wallet
                </ModalHeader>
                <ModalBody>
                        <Row>
                            <Col md={12}> 
                                <div className="col-sm-12 col-lg-12 mb-4">
                                    <div className="card shadow-sm border-0">
                                        <ul className="list-group list-group-flush">
                                            <span className="list-group-item list-group-item-action" href="#">Wallet Information</span>
                                        </ul>
                                        <div className="card-body">

                                            Wallet name: {walletInfo.walletName}<br />
                                            Balance: {walletInfo.balance}<br />
                                            Description: {walletInfo.walletDescription}<br />
                                            Date Created: {walletInfo.dateCreated}<br />
                                        
                                            {fundingOption} 
                                        </div>
                                    </div>

                                
                                </div>

                                <div className="col-sm-12 col-lg-12 mb-4" style={this.state.showFundPanel ? {} : { display: 'none' }}>
                                    <div className="card shadow-sm border-0">
                                        <ul className="list-group list-group-flush">
                                        <span className="list-group-item list-group-item-action" href="#">{this.state.fundingOption == 1 ? "" : "Enter you bank deposit details"}</span>
                                        </ul>
                                    <div className="card-body" hidden={this.state.fundingOption == 0 }>
                                        
                                        <Form style={this.state.fundingOption == 1 ? {} : {display: 'none'}}>
                                            <Row form>
                                                <FormGroup>
                                                    <Label for="exampleEmail">Amount</Label>
                                                    <Input value={fundingInfo.amount} type="text" onChange={this.onFundingInfoChange} name="amount" bsSize="sm" id="amount" placeholder="Amount deposited" />
                                                </FormGroup>
                                            </Row>
                                        </Form>

                                        <Form style={this.state.fundingOption == 2 ? {} : { display: 'none' }}>                                            
                                            <Row form>                                                                                                
                                                <Col md={6}>
                                                    <FormGroup>
                                                        <Label for="exampleEmail">Amount</Label>
                                                        <Input value={fundingInfo.amount} type="text" onChange={this.onFundingInfoChange} name="amount" bsSize="sm" id="amount" placeholder="Amount deposited" />
                                                    </FormGroup>
                                                </Col>                                                                                       
                                                <Col md={12}>
                                                    <FormGroup>
                                                        <Label for="exampleEmail">Description</Label>
                                                        <Input value={fundingInfo.description} type="textarea" onChange={this.onFundingInfoChange} name="description" bsSize="sm" id="description" placeholder="Transaction description" />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                        </Form>

                                        </div>
                                    </div>
                                </div>

                            </Col>

                        </Row>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" disabled={this.state.fundingOption < 1} onClick={() => { this.props.onContinueFunding(this.state.fundingOption, this.state.fundingInfo) }}>Continue</Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}