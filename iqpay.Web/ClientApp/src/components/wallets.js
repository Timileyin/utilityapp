﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import AddEditWallet from './add-edit-wallets';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import FundBaseWallet from './fundBaseWallet';
import FundNonBaseWallet from './fundNonBaseWallet';
import moment from 'moment';
import numeral from 'numeral';
import WemaSendMoneyModal from './wemaSendMoney';
import { alertActions } from '../actions/alertActions';
import { transactionAction} from '../actions/transactionAction';

class Wallets extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: this.props.isVisible,
            pageTitle: 'My Wallets',
            wallets: [],
            modalPopup: false,
            isOpen: false,
            walletInfo: null,
            baseWallet: [],
            fundWallet: [],
            sendMoneyData:null,
            bankList: []
        };
    }


    fetchWalletInfo = (walletId) => {
        const { dispatch } = this.props;
        dispatch(userActions.fetchWalletInfo(walletId))        
    }

    componentDidMount(prevprops) {
        const { dispatch } = this.props;
        dispatch(userActions.fetchWallets());
    }

    onFundWallet = (walletId, basewalletId, amountToFund) => {
        const { dispatch } = this.props;
        dispatch(userActions.fundIntraWallet(walletId, basewalletId, amountToFund));
        this.setState({ fundNonBaseWallet: false });
    }

    toggle = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    saveWallet = (walletInfo) => {
        const { dispatch } = this.props;
        dispatch(userActions.saveWallet(walletInfo));
    }

    closeModal = () => {
        this.setState({
            walletInfo: null
        });
    }

    moveFundsToBase = (wallet) =>{
        var baseWallet = this.state.wallets.find(w => w.mode !== 0 && w.walletType == "Transaction");
        const{ dispatch } = this.props;        
        dispatch(userActions.fundBaseWalletWithComm(wallet.id, baseWallet.id));
    }

    closeFundWallet = () => {
        this.setState({
            fundNonBaseWallet: false
        });
    }

    fund = (wallet) => {
        const { dispatch } = this.props;
        
        if (wallet.commissionMode == 0) {
            this.setState({
                fundWallet: wallet,
                fundBaseWallet: true
            })
        } else {
            dispatch(userActions.fetchWallets());
            var baseWallet = this.state.wallets.find(w => w.mode !== 0 && w.walletType == "Transaction");
            this.setState({
                baseWallet: baseWallet,
                fundNonBaseWallet: true,
                fundWallet: wallet
            });
        }
    }

    addNew = () => {
        this.setState({
            walletInfo: {}
        });
    }
    

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
                 
        if (prevprops !== this.props) {
            if (this.props.authentication.wallets !== undefined) {
                var wallets = this.props.authentication.wallets;

                if (wallets !== null && wallets.length > 0) {

                    this.setState({
                        wallets: wallets
                    });

                }
            }

            if(this.props.transaction.bankList != undefined)
            {
                this.setState({bankList: this.props.transaction.bankList});
            }

            if(this.props.transaction.verifyAccount != undefined)
            {
                var theState = { ...this.state };
                if(theState.sendMoneyData != null)
                {
                    if(this.props.transaction.verifyAccount == null)
                    {                        
                        theState.sendMoneyData.accountName = "";                        
                    }else{     
                        
                        theState.sendMoneyData.bankAccountNumber = this.props.transaction.verifyAccount.bankAccountNumber;
                        theState.sendMoneyData.accountName = this.props.transaction.verifyAccount.accountName;
                        theState.sendMoneyData.bankName = this.props.transaction.verifyAccount.bankName;    
                        theState.sendMoneyData.bvn = this.props.transaction.verifyAccount.bvn;   
                        theState.sendMoneyData.clientId = this.props.transaction.verifyAccount.clientId;
                        theState.sendMoneyData.toSession = this.props.transaction.verifyAccount.sessionId;             
                    }    
                }
                this.setState(theState);
            }

            if (this.props.authentication.walletInfo !== undefined) {
                var walletInfo = this.props.authentication.walletInfo;
                this.setState({
                    isOpen: true,
                    walletInfo: walletInfo
                });
            }
            
        }
        
    }

    closefundModal = () => {
        this.setState({
            fundBaseWallet: false
        });
    }

    closeFundNonBaseModal = () =>
    {
        this.setState({            
            fundNonBaseWallet: false
        });
    }

    verifyAccoutNumber = (accountNumber, bankCode) => {
        const {dispatch} = this.props;
        dispatch(transactionAction.verifyAccoutNumber(accountNumber, bankCode))
    }
    
    onContinueFunding = (fundingOption, fundingInfo) => {
        const { dispatch } = this.props;
        if (fundingOption == 1) {
            dispatch(userActions.initiateAutoFunding(fundingInfo));            
        } else {
            dispatch(userActions.maunalFund(fundingInfo));
            this.setState({
                fundWallet: [],
                fundBaseWallet: false
            });

        }
    } 

    transferFromWemaWallet = () => {
        if(this.state.bankList.length < 1)
        {
            const { dispatch } = this.props;
            dispatch(transactionAction.fetchBankList());
        }
        this.setState({sendMoneyData: {accountNumber: "", bankCode: "", bankName: ""}});
    }

    doTransfer = (sendMoneyData) => {
        const { dispatch } = this.props;
        dispatch(alertActions.success("Funds have been transferred successfully")); 
        this.setState({sendMoneyData: null});       
    }
    
    render() {

        var wallets = this.state.wallets;
        let walletTable = [];
        let walletCards = [];
        if (wallets.length > 0) {
            wallets.map((v) => {

                var overlayButtn = "";
                if(v.walletType == "Transaction")
                {
                    overlayButtn = <Button color="info" tooltip="Fund Wallet" onClick={() => this.fund(v)} className="pull-right" size="sm">Fund Wallet</Button>
                }else if(v.walletType == "WemaInternalWallet")
                {
                    overlayButtn = <Button onClick={() => this.transferFromWemaWallet()} color="info" tooltip="View Virtual Transactions" className="btn btn-info pull-right" size="sm">Move Funds</Button>
                }else
                {                    
                    overlayButtn = <Button color="info" tooltip="Fund Wallet" onClick={() => this.moveFundsToBase(v)} className="pull-right" size="sm">Move funds to Base Wallet</Button>
                }

                walletCards.push(
                    <div className="col-12 col-sm-6 col-lg-4 mb-4">
                        <div className="account-card account-card-primary text-white rounded p-3 mb-4 mb-lg-0">
                            <p className="text-4">{v.walletType == "Commission" ? v.mode : v.WalletName  }</p>
                            <p className="d-flex align-items-center"> 
                                <span className="account-card-expire text-uppercase d-inline-block opacity-6 mr-2">
                                    Date<br />
                                    Created<br />
                                </span> 
                                <span className="text-4 opacity-9">{moment(v.dateCreated).format('MMM YY')}</span> 
                                <span className="bg-light text-0 text-body font-weight-500 rounded-pill d-inline-block px-2 line-height-4 opacity-8 ml-auto">{v.walletName}</span> 
                            </p>
                            <p className="d-flex align-items-center m-0"> 
                                <span className="text-uppercase font-weight-500">{numeral(v.balance.toFixed(2)).format("0,00.00")}</span> 
                                <img className="ml-auto" src={v.vendorLogo == null ? "images/logo.png" : "companyLogo/"+v.vendorLogo} width="40px" alt="visa" title="" /> 
                            </p>
                            <div className="account-card-overlay rounded"> {overlayButtn} </div>
                        </div>
                    </div>
                );

                walletTable.push(
                    <tr key={v.id}>
                        <td>{v.walletType == "Commission" ? v.walletName : v.commissionMode == 0 ? v.walletName : v.mode+" ("+ v.walletName +")"  }</td>                       
                        <td>{v.balance}</td>
                        <td>{v.status}</td>
                        <td>
                            {v.walletType == "Transaction" ? <Button color="info" tooltip="Fund Wallet" onClick={() => this.fund(v)} className="pull-right" size="sm">Fund Wallet</Button> : <Button color="info" tooltip="Fund Wallet" onClick={() => this.moveFundsToBase(v)} className="pull-right" size="sm">Move funds to Base Wallet</Button>}
                        </td>

                    </tr>);
            });
        } else {
            walletCards = <div className="alert alert-danger">No wallet has been created</div>;
            walletTable = <tr><td colspan='5'>No wallet has been created</td></tr>;
        }

        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">                            
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">                                        
                                        <Row>
                                             {walletCards}                                                                                                                                   
                                        </Row>
                                        <AddEditWallet onSave={this.saveWallet} onCancel={this.closeModal} isOpen={this.state.isOpen} walletInfo={this.state.walletInfo} />
                                        <FundBaseWallet isOpen={this.state.fundBaseWallet} wallet={this.state.fundWallet} onCancel={this.closefundModal} onContinueFunding={this.onContinueFunding} />
                                        <FundNonBaseWallet isOpen={this.state.fundNonBaseWallet} cancel={this.closeFundNonBaseModal} onFundWallet={this.onFundWallet} baseWallet={this.state.baseWallet} fundWallet={this.state.fundWallet} />
                                    </div>
                                </div>
                            </div>
                                                   
                        </div>
                        <WemaSendMoneyModal sendMoneyData={this.state.sendMoneyData} doTransfer={(sendMoneyData)=>this.doTransfer(sendMoneyData)} verifyAccoutNumber={(accountNumber, bankCode)=>{this.verifyAccoutNumber(accountNumber, bankCode)}} bankList={this.state.bankList} />
                    </div>
                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {
    const { alert, authentication, transaction } = state;
    
    return {
        alert, authentication, transaction
    };
}

const connectedWalletsPage = connect(mapStateToProps)(Wallets);
export { connectedWalletsPage as Wallets };
