import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Badge } from 'reactstrap';
import { transactionAction } from '../actions/transactionAction';
import { connect } from 'react-redux';
import moment from 'moment';
import FilterModal from './common/filterModal';
import numeral from 'numeral';

class ProcessorTransactionReport extends React.Component {
    modalpopup = true
    constructor(props) {
        super(props);

        this.state = {
            pageTitle: 'Processor Vending Summary',
            processorTransactions: []            
        };
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if (this.props.transaction.processorTransactions !== undefined) {
                this.setState({ processorTransactions: this.props.transaction.processorTransactions });
            }            
        }
    }

    onFilter = (startDate, endDate) => 
    {
        const { dispatch } = this.props;
        dispatch(transactionAction.filterProcessorTransaction(startDate, endDate));
    }
    
    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document

        const { dispatch } = this.props;
        dispatch(transactionAction.fetchProcessorTransaction());

    }

    render() {
        console.log("wallets=>",this.state.processorTransactions)
        var processorTransactions = this.state.processorTransactions;
        let processorTransactionsTable = [];
        if (processorTransactions.length > 0) {
            console.log(processorTransactionsTable);
            var totalValue = 0;
            var totalCount = 0;
            var totalFailed = 0;
            var totalSuccessful = 0;
            processorTransactions.map((v) => {
                processorTransactionsTable.push(
                    <tr key={v.processorId}>
                        <td className="align-middle" ><Link to={"/processor-vending/"+v.processorId+"/"+v.processorName}>{v.processorName}</Link></td>
                        <td className="align-middle">{numeral(v.successfulCount).format("0,00")}</td> 
                        <td className="align-middle">{numeral(v.failedCounts).format("0,00")}</td> 
                        <td className="align-middle">{numeral(v.transactionCounts).format("0,00")}</td>                                                 
                        <td className="align-middle">{numeral(v.transactionValue.toFixed(2)).format("0,00.00")}</td>                        
                    </tr>
                );   
                totalFailed += v.failedCounts;
                totalSuccessful += v.successfulCount;
                totalValue += v.transactionValue;
                totalCount += v.transactionCounts; 
            });
            processorTransactionsTable.push(
                <tr>
                    <td><Badge color="info">Total</Badge></td><td>{totalSuccessful}</td><td>{totalFailed}</td><td>{numeral(totalCount).format("0,00")}</td><td>{numeral(totalValue.toFixed(2)).format("0,00.00")}</td>
                </tr>
            );
        } else {
            processorTransactionsTable = <tr><td colSpan='7'>No request have been made</td></tr>;
        }

        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                        
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                    <h4 class="mb-3">Processor Vending Summary</h4>
                                        <Row>
                                            <Col md={12}> 
                                                <div className="alert alert-info">Please refresh this page to view most recent report as this is not a live feed. And lances could have changed. Last retrieval time <Badge color="info">{moment(new Date()).format("DD ddd MMM yyyy, hh:mm A")}</Badge></div>
                                                <span className="float-right"><i class="fa fa-filter"></i><FilterModal onFilter={(startDate, endDate) => this.onFilter(startDate,endDate)}/></span>
                                                <Table className="table table-hover border" striped hover size="md">
                                                    <thead>
                                                        <tr>
                                                            <th>Processor Name</th>                                                            
                                                            <th>Successful</th>
                                                            <th>Failed</th>
                                                            <th>Total</th>
                                                            <th>Total Successful Value</th>                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {processorTransactionsTable}
                                                    </tbody>
                                                </Table>
                                            </Col>
                                        </Row>
                                    </div>

                                </div>
                            </div>

                        </div>

                        
                    </div>

                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, transaction } = state;

    return {
        alert, transaction
    };
}

const walletFundingRequest = connect(mapStateToProps)(ProcessorTransactionReport);
export { walletFundingRequest as ProcessorTransactionReport };
