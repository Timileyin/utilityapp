import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { connect } from 'react-redux';
import { superdealerConstants } from '../constants/dealerConstants';
import { transactionAction } from '../actions/transactionAction';
import {roles} from '../constants/rolesConstants';
import numeral from 'numeral';
import moment from 'moment';
import SendMoneyModal from './sendMoneyModal';
import DataTable from 'react-data-table-component';
import FilterModal from './common/filterModal';
import {downloadCSV} from '../helpers/history';

import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";

import $ from 'jquery';
import { userActions } from '../actions/userAction';
import { ThemeConsumer } from 'react-bootstrap/esm/ThemeProvider';



class TransferMoney extends React.Component {
    modalpopup = true;
    columns = [
        {
            name: 'Date',
            selector: 'transferDate',
            sortable: true,
            cell: row => moment(row.transferDate).format("DD MMM yyyy, hh:mm A")
        },
        {
            name: 'Ref',
            selector: 'txnRef',
            sortable: true,
        },
        {
            name: 'Dealer Code',
            selector: 'dealerCode',
            sortable: true,
        },
        {
            name: 'Account number',
            selector: 'bankAccountNumber',
            sortable: true,
        },
        {
            name: 'Bank name',
            selector: 'bankName',
            sortable: true,
        },
        {
        name: 'Amount',
        selector: 'amount',
        sortable: true,
        },
        {
            name: 'Wallet Balance',
            selector: 'walletBalance',
            sortable: true,
        },
        {
            name: 'Status',
            selector: 'status',
            sortable: true,
        },
        {
            name: 'Narration',
            selector: 'narration',
            sortable: true,
        },
    ]
    constructor(props) {
        super(props);
        this.state = {
            pageTitle: 'Transfer Money',
            walletBalance: 0.00,            
            logs: [],
            sendMoneyData: null,
            bankList: []            
        };
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {

            if(this.props.authentication.wallets != undefined)
            {                
                var baseWallet = this.props.authentication.wallets.find(w => w.mode !== 0 && w.walletType == "Transaction");
                this.setState({walletBalance: baseWallet.balance});
            }

            if(this.props.transaction.bankList != undefined)
            {
                this.setState({bankList: this.props.transaction.bankList});
            }

            if(this.props.transaction.verifyAccount != undefined)
            {
                var theState = { ...this.state };
                if(theState.sendMoneyData != null)
                {
                    if(this.props.transaction.verifyAccount == null)
                    {                        
                        theState.sendMoneyData.accountName = "";                        
                    }else{     
                        
                        theState.sendMoneyData.bankAccountNumber = this.props.transaction.verifyAccount.bankAccountNumber;
                        theState.sendMoneyData.accountName = this.props.transaction.verifyAccount.accountName;
                        theState.sendMoneyData.bankName = this.props.transaction.verifyAccount.bankName;    
                        theState.sendMoneyData.bvn = this.props.transaction.verifyAccount.bvn;   
                        theState.sendMoneyData.clientId = this.props.transaction.verifyAccount.clientId;
                        theState.sendMoneyData.toSession = this.props.transaction.verifyAccount.sessionId;             
                    }    
                }
                this.setState(theState);
            }

            if(this.props.transaction.moneyTransferLogs != undefined){
                this.setState({logs: this.props.transaction.moneyTransferLogs });                                    
            }                        
        }
    }

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document        
        const { dispatch } = this.props;   
        dispatch(userActions.fetchWallets());       
        dispatch(transactionAction.fetchMoneyTransferLogs());             
    }

    verifyAccoutNumber = (accountNumber, bankCode) => {
        const {dispatch} = this.props;
        dispatch(transactionAction.verifyAccoutNumber(accountNumber, bankCode))
    }

    doTransfer = (sendMoneyData) => {
        if(window.confirm("Are you sure you want to transfer "+ sendMoneyData.amount+ " to "+ sendMoneyData.accountName+ " "+ sendMoneyData.bankName)){
            this.setState({sendMoneyData: null});
            const {dispatch} = this.props;
            sendMoneyData.bankAccountNumber = sendMoneyData.accountNumber;
            dispatch(transactionAction.sendMoney(sendMoneyData));
        }
    }

    onCloseSendMoney = () =>{
        this.setState({sendMoneyData: null});   
    }

    onFilter = (startDate, endDate) => {
        const { dispatch } = this.props;        
        dispatch(transactionAction.filterMoneyTransferLogs(startDate, endDate));
    }

    openSendMoneyModal = () => {        
        if(this.state.bankList.length < 1)
        {
            const { dispatch } = this.props;
            dispatch(transactionAction.fetchBankList());
        }
        this.setState({sendMoneyData: {accountNumber: "", bankCode: "", bankName: ""}});
    }

    render() {
        var walletBalance = this.state.walletBalance;
        var transactions = this.state.logs;
        let transactionTable = [];
        var transactionValue = 0;
        if (transactions.length > 0) {
            transactions.map((v) => {
                if(v.status == "Completed")
                {
                    transactionValue += v.amount
                }  
            });
        } else {
            transactionTable = <tr><td colSpan='8'>No transaction has been made</td></tr>;
        }

        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                        <Row>
                                            <Col>
                                                <div className="text-right">Transaction Wallet Balance: &#8358;{numeral(walletBalance).format("0,00.00")}<br /><Button color="info" onClick={this.openSendMoneyModal} size="sm">Transfer Money</Button></div>
                                                <span className="float-right"><i class="fa fa-filter"></i><FilterModal onFilter={(startDate, endDate) => this.onFilter(startDate,endDate)}/></span>
                                                <DataTable
                                                        title="Transaction List"
                                                        columns={this.columns}
                                                        data={this.state.logs}
                                                        actions={<Button className="btn btn-sm" onClick={()=>downloadCSV(this.state.logs)}>Export</Button>}
                                                        pagination
                                                        paginationPerPage={30}
                                                />
                                            </Col>                                           
                                        </Row>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </LoginMenu>
                <SendMoneyModal sendMoneyData={this.state.sendMoneyData} doTransfer={(sendMoneyData)=>this.doTransfer(sendMoneyData)} verifyAccoutNumber={(accountNumber, bankCode)=>{this.verifyAccoutNumber(accountNumber, bankCode)}} onCancel={this.onCloseSendMoney} bankList={this.state.bankList}/>
            </div>
            
        );
    }
}

function mapStateToProps(state) {

    const { alert, authentication, transaction } = state;

    return {
        alert, authentication, transaction
    };
}

const connectedTransactionsPage = connect(mapStateToProps)(TransferMoney);
export { connectedTransactionsPage as TransferMoney };