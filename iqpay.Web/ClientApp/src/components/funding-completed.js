﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import { transactionAction } from '../actions/transactionAction';
import { connect } from 'react-redux';

class FundingCompleted extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: this.props.isVisible,
            pageTitle: 'Fund Wallet',
            pageDetails: {
                commission: 0.00,
                transaction: 0.00,
            },
            txnDetails: {},
            txnRef: this.props.match.params.txnref,
        };

        console.log(this.props.match.params);
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(transactionAction.fetchTransactionByRef(this.state.txnRef));
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            
            if (this.props.transaction.transactionInfo != undefined) {

                this.setState({ txnDetails: this.props.transaction.transactionInfo });
            }
        }
    }

    render() {

        return (

            <div class="row my-5">

               

                <div class="col-lg-12 text-center mt-5">
                    <p class="text-success text-16 line-height-07"><i class="fas fa-check-circle"></i></p>
                    <h2 class="text-8">Funding Successful</h2>

                    <p class="lead">We are processing the same and you will be notified via email.</p>
                </div>
                <div class="col-md-8 col-lg-6 col-xl-5 mx-auto">
                    <div class="bg-light shadow-sm rounded p-3 p-sm-4 mb-4">
                        <div class="row">
                            <div class="col-sm text-muted">Transaction Ref.</div>
                            <div class="col-sm text-sm-right font-weight-600">{this.state.txnDetails.billRefenceNo}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-sm text-muted">Date</div>
                            <div class="col-sm text-sm-right font-weight-600">{this.state.txnDetails.datePaid}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-sm text-muted">Transaction Status</div>
                            <div class="col-sm text-sm-right font-weight-600 text-success">{this.state.txnDetails.billStatusStr}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-sm text-muted">Customer Name</div>
                            <div class="col-sm text-sm-right font-weight-600">{this.state.txnDetails.name}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-sm text-muted">Mobile No</div>
                            <div class="col-sm text-sm-right font-weight-600">{this.state.txnDetails.phoneNumber}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-sm text-muted">Subject</div>
                            <div class="col-sm text-sm-right font-weight-600">{this.state.txnDetails.billType}</div>
                        </div>
                        <hr />
                        <div class="row">
                            <div class="col-sm text-muted">Payment Amount</div>
                            <div class="col-sm text-sm-right text-6 font-weight-500">{this.state.txnDetails.totalAmount}</div>
                        </div>
                        
                    </div>

                    <div class="text-center">
                        <p class="mt-4 mb-0"><a href="/wallets" class="btn btn-primary">Go back to wallets</a></p>
                    </div>
                </div>
            </div>);
    }
}

function mapStateToProps(state) {

    const { alert, transaction } = state;
    console.log(transaction);
    return {
        alert, transaction
    };
}

const connectedFundingCompleted = connect(mapStateToProps)(FundingCompleted);
export { connectedFundingCompleted as FundingCompleted };
