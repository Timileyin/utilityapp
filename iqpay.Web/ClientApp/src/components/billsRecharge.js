﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import Sidebar from './common/sidebar';
import { actionCreators } from '../store/WeatherForecasts';
import { MobileTopUp } from './mobile-topup';
import DTHRecharge from './dth';
import LandLineBills from './landlineBills';
import BroadBandBills from './broadband';
import { CableTV } from './cableTV';
import { ElectricityBills } from './electricityBills';
import {Internet} from './internet';
import MetroBills from './metro';
import { Lotto } from './lotto';
import {AirtimePins} from './airtimePins';
import {Food} from './food';

import $ from 'jquery';
import DataCardRecharge from './datacardrecharge';
import { serviceTypes } from '../constants/commissionMode';
import { servicesActions } from '../actions/servicesAction';
import { vendorActions } from '../actions/vendorAction';
import { servicesService } from '../services/servicesService';
import { service } from '../services/service';
import { EPins } from './e-pins';

window.$ = window.jQuery = $;
require('easy-responsive-tabs');


class BillsRecharge extends Component {

    constructor(props) {
        // Required step: always call the parent class' constructor
        super(props);

        // Set the state directly. Use props if necessary.
        this.state = {
            currentComponent: serviceTypes.find(c => c.key == 1).module,
            serviceTypeKey: 1,
            vendors: []
        }

        const { dispatch } = this.props;
        dispatch(vendorActions.fetchVendorsInType(1));
    }

    componentDidMount() {
        // This method is called when the component is first added to the document
        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            tabidentify: 'sample'
        });
       
    }

    changeComponent = (component, serviceTypeKey) => {
        
        this.setState({ currentComponent: component, serviceTypeKey: serviceTypeKey });

        const { dispatch } = this.props;
        dispatch(vendorActions.fetchVendorsInType(serviceTypeKey));
        
    }

    componentDidUpdate = (prevProps) =>  {
        // This method is called when the route parameters change

        if (this.props !== prevProps) {
            this.setState({ vendors: this.props.vendor.vendors });
        }
    }

    

    render() {
        return (
     <section className="container">
      <div className="row mt-4">
        <div className="col-md-12 col-lg-10">
          <div id="verticalTab">
        <div className="row no-gutters">
            <Sidebar currentComponentChanger={this.changeComponent}></Sidebar>
              <div className="col-md-9">
                <div className="resp-tabs-container bg-light shadow-md rounded h-100 p-3">
                    <MobileTopUp serviceTypeKey={this.state.serviceTypeKey} vendors={this.state.vendors} isVisible={this.state.currentComponent === 'mobileTopUp'} />
                    <DTHRecharge serviceTypeKey={this.state.serviceTypeKey} vendors={this.state.vendors} isVisible={this.state.currentComponent === 'dth'} />
                    <DataCardRecharge serviceTypeKey={this.state.serviceTypeKey} vendors={this.state.vendors} isVisible={this.state.currentComponent === 'datacard'} />
                    <LandLineBills serviceTypeKey={this.state.serviceTypeKey} vendors={this.state.vendors} isVisible={this.state.currentComponent === 'landlines'} />
                    <Internet serviceTypeKey={this.state.serviceTypeKey} vendors={this.state.vendors} isVisible={this.state.currentComponent === 'internet'} />
                    <CableTV  serviceTypeKey={this.state.serviceTypeKey} vendors={this.state.vendors} isVisible={this.state.currentComponent === 'cableTV'} />
                    <EPins serviceTypeKey={this.state.serviceTypeKey} vendors={this.state.vendors} isVisible={this.state.currentComponent === 'ePins'} /> 
                    <Lotto  serviceTypeKey={this.state.serviceTypeKey} vendors={this.state.vendors} isVisible={this.state.currentComponent === 'lotto'} />
                    <AirtimePins  serviceTypeKey={this.state.serviceTypeKey} vendors={this.state.vendors} isVisible={this.state.currentComponent === 'airtimePins'} />
                    <ElectricityBills serviceTypeKey={this.state.serviceTypeKey} vendors={this.state.vendors} isVisible={this.state.currentComponent === 'electricity'} />
                    <MetroBills serviceTypeKey={this.state.serviceTypeKey} vendors={this.state.vendors} isVisible={this.state.currentComponent === 'metro'} />
                    <Food serviceTypeKey={this.state.serviceTypeKey} vendors={this.state.vendors} isVisible={this.state.currentComponent === 'food'} />                  
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <div className="col-lg-2 mt-4 mt-lg-0">
          <div className="row">
            <div className="col-6 col-lg-12 text-center"> <a href="#"><img src="images/slider/small-banner-7.jpg" alt="" title="" className="img-fluid rounded shadow-md"/></a> </div>
            <div className="col-6 col-lg-12 mt-lg-3 text-center"> <a href=""><img src="images/slider/small-banner-8.jpg" alt="" title="" className="img-fluid rounded shadow-md"/></a> </div>
          </div>
        </div>
        
       </div>


    </section> 
         
    );
    }
}

function mapStateToProps(state) {
    const { alert, vendor } = state;

    return {
        alert, vendor
    };
}

const connectedBillsRechargePage = connect(mapStateToProps)(BillsRecharge);
export { connectedBillsRechargePage as BillsRecharge };


