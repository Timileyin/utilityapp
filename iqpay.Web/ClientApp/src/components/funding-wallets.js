﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import { transactionAction } from '../actions/transactionAction';
import { connect } from 'react-redux';
import { history } from '../helpers/history';
import { config } from '../config';

class FundWallet extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: this.props.isVisible,
            pageTitle: 'Fund Wallet',
            pageDetails: {
                commission: 0.00,
                transaction: 0.00,
            },
            txnRef: this.props.match.params.txnref,
            txnDetails: {}
        };
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(transactionAction.fetchTransactionByRef(this.state.txnRef));
    }


    makePayment = () => {
        console.log("txnRef=>",this.state.txnRef);
        var txnRef = this.state.txnRef;
        window.Korapay.initialize({
            key: this.state.txnDetails.publicKey,
            amount: this.state.txnDetails.totalAmount,
            reference: this.state.txnDetails.billRefenceNo,
            currency: "NGN",
            customer: {
                name: this.state.txnDetails.name,
                email: this.state.txnDetails.email,
                
            },
            onClose: function () {
                history.push("/payment-complete/" + txnRef);
            },

            onSuccess: function (data) {
                console.log(data);
                history.push("/payment-complete/" + txnRef);
            },

            onFailure: function (data) {
                history.push("/payment-complete/" + txnRef);
            },
            notification_url: config.PAYMENT_NOTIFICATION + this.state.txnDetails.billRefenceNo
        });
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ pageTitle: 'Fund Wallet' });
            
            if (this.props.transaction.transactionInfo != undefined) {
                
                this.setState({ txnDetails: this.props.transaction.transactionInfo });
            }
        }
    }


    render() {
        return (
            <div>
                <PageTitle title={this.state.pageTitle} />                
                    <div className="col-lg-11 mx-auto">
                    <div className="col-lg-12 text-center mt-5">
                        <h2 className="text-8">Wallet Funding Summary</h2>
                        <p className="lead">Confirm payment</p>
                    </div>
                    <div className="col-md-8 col-lg-6 col-xl-5 mx-auto">
                        <div className="bg-light shadow-sm rounded p-3 p-sm-4 mb-0 mb-sm-4">
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Transaction Ref.:</p>
                                <p className="col-sm text-sm-right font-weight-500">{this.state.txnDetails.billRefenceNo}</p>
                            </div>
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Mobile Number:</p>
                                <p className="col-sm text-sm-right font-weight-500">{this.state.txnDetails.phoneNumber}</p>
                            </div>
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Name:</p>
                                <p className="col-sm text-sm-right font-weight-500">{this.state.txnDetails.name}</p>
                            </div>
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Email Address:</p>
                                <p className="col-sm text-sm-right font-weight-500">{this.state.txnDetails.email}</p>
                            </div>
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Wallet name:</p>
                                <p className="col-sm text-sm-right font-weight-500">{this.state.txnDetails.walletName}</p>
                            </div>
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Amount:</p>
                                <p className="col-sm text-sm-right font-weight-500">N{this.state.txnDetails.amount?.toFixed(2)}</p>
                            </div>
                            <div className="row">
                                <p className="col-sm text-muted mb-0 mb-sm-3">Tech Fee:</p>
                                <p className="col-sm text-sm-right font-weight-500">N{this.state.txnDetails.techFee?.toFixed(2)}</p>
                            </div>
                            <div className="row">
                                <p className="col-12 text-muted mb-0">Payment Description:</p>
                                <p className="col-12 text-1">{this.state.txnDetails.billType}</p>
                            </div>


                            <div className="bg-light-4 rounded p-3">
                                <div className="row">
                                    <div className="col-sm text-3 font-weight-600">Payment Amount</div>
                                    <div className="col-sm text-sm-right text-5 font-weight-500">N{this.state.txnDetails.totalAmount?.toFixed(2)}</div>
                                </div>
                            </div>                           

                            <p className="mt-4 mb-0"><button onClick={this.makePayment} disabled={this.state.txnDetails == {}} className="btn btn-primary btn-block">Make Payment</button></p>
                            </div>
                        </div>
                    </div>
                </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, transaction } = state;
    console.log(transaction);
    return {
        alert, transaction
    };
}

const connectedFundWallet = connect(mapStateToProps)(FundWallet);
export { connectedFundWallet as FundWallet };
