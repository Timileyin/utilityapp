﻿import React from 'react';
import $ from 'jquery';

window.$ = window.jQuery = $;
require('easy-responsive-tabs');

export default class CableTVBills extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: this.props.isVisible,
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ isVisible: this.props.isVisible })
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document


    }
    render() {
        return (

            <div style={!this.state.isVisible ? { display: 'none' } : null}>
                <h2 className="text-6 mb-4">CableTv Recharge or Bill Payment</h2>
                <form id="cableTvRechargeBill" method="post">
                    <div className="form-group">
                        <label htmlFor="cableTvoperator">Your Operator</label>
                        <select className="custom-select" id="cableTvoperator" required="">
                            <option value="">Select Your Operator</option>
                            <option>1st Operator</option>
                            <option>2nd Operator</option>
                            <option>3rd Operator</option>
                            <option>4th Operator</option>
                            <option>5th Operator</option>
                            <option>6th Operator</option>
                            <option>7th Operator</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="accountNumber">Your Operator</label>
                        <input type="text" className="form-control" data-bv-field="number" id="accountNumber" required placeholder="Enter Account Number" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="cableTvamount">Amount</label>
                        <div className="input-group">
                            <div className="input-group-prepend"> <span className="input-group-text">$</span> </div>
                            <input className="form-control" id="cableTvamount" placeholder="Enter Amount" required type="text" />
                        </div>
                    </div>
                    <button className="btn btn-primary btn-block" type="submit">Continue</button>
                </form>
            </div>

        );
    }
}
