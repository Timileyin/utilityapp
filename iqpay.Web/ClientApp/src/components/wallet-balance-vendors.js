import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Badge } from 'reactstrap';
import { superdealerActions } from '../actions/superdealerAction';
import { connect } from 'react-redux';
import { userActions } from '../actions/userAction';
import DataTable from 'react-data-table-component';

import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";
import numeral from 'numeral';
import moment from 'moment';

import $ from 'jquery';
import { vendorActions } from '../actions/vendorAction';



class VendorsCummBalance extends React.Component {

    columns = [
        
        {
            name: 'Vendor Name',
            selector: 'vendorName',
            sortable: true,
        },
        {
            name: 'Wallet Count',
            selector: 'walletCount',
            sortable: true,
        },
        {
            name: 'Total Balance',
            cell: row => numeral(row.cummBalance).format("0,00.00"),
            sortable: true,
        }     
    ];

    modalpopup = true;
    constructor(props) {
        super(props);
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));
        this.state = {
            role: currentUser.role,
            pageTitle: 'Vendors Cummulative Wallet Balances',
            walletBalances: [],
            vendorsList: [],
            totalBalance: 0.00            
        };
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            
            if(this.props.vendor.vendorsWallet != undefined)
            {
                this.setState({walletBalances: this.props.vendor.vendorsWallet});
            }                                  
        }
    }    

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document        
        const { dispatch } = this.props;
        dispatch(vendorActions.fetchCummWallets());        
    }

    render() {
        var balances = this.state.walletBalances;
        let balancesTable = [];  
        let totalAmount = 0.00; 
        var vendors = this.state.vendorsList;     
        if (balances.length > 0) {            
            balances.map((v) => {
                totalAmount += v.totalBalance;                                
            });
        } else {
            balancesTable = <tr><td colSpan='8'>No wallet found</td></tr>;
        }

        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">                        
                        <div className="row justify-content-center align-items-center">                            
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                        <Row>
                                            <Col md={12}>
                                                <div className="alert alert-info">Please refresh this page to view most recent report as this is not a live feed. And balances could have changed. Last retrieval time <Badge color="info">{moment(new Date()).format("DD ddd MMM yyyy, hh:mm A")}</Badge></div>
                                                <span className="text-right">Total: {numeral(totalAmount).format("0,00.00")}</span> 
                                                <DataTable
                                                        title="Cummulative Wallet Balances"
                                                        columns={this.columns}
                                                        data={balances}
                                                        actions={<Button className="btn btn-sm" >Export</Button> }
                                                        pagination
                                                        paginationPerPage={30}
                                                />
                                            </Col>                                           
                                        </Row>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, authentication, vendor } = state;

    return {
        alert, authentication, vendor
    };
}

const connectedWalletBalancesPage = connect(mapStateToProps)(VendorsCummBalance);
export { connectedWalletBalancesPage as VendorsCummBalance };