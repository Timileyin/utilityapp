﻿import React from 'react';
import $ from 'jquery';

window.$ = window.jQuery = $;
require('easy-responsive-tabs');

export default class DTHRecharge extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: this.props.isVisible,
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ isVisible: this.props.isVisible })
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document


    }
    render() {
        return (

            <div style={!this.state.isVisible ? { display: 'none' } : null}>
                <h2 className="text-6 mb-4">DTH Recharge</h2>
                <form id="dthRechargeBill" method="post">
                    <div className="form-group">
                        <label htmlFor="DTHoperator">Your Operator</label>
                        <select className="custom-select" id="DTHoperator" required="">
                            <option value="">Select Your Operator</option>
                            <option>1st Operator</option>
                            <option>2nd Operator</option>
                            <option>3rd Operator</option>
                            <option>4th Operator</option>
                            <option>5th Operator</option>
                            <option>6th Operator</option>
                            <option>7th Operator</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="cardNumber">Your Card Number</label>
                        <input type="text" className="form-control" data-bv-field="number" id="cardNumber" required placeholder="Enter Your Card Number" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="DTHamount">Amount</label>
                        <div className="input-group">
                            <div className="input-group-prepend"> <span className="input-group-text">&#8358;</span> </div>
                            <a href="#" data-target="#view-plans" data-toggle="modal" className="view-plans-link">View Plans</a>
                            <input className="form-control" id="DTHamount" placeholder="Enter Amount" required type="text" />
                        </div>
                    </div>
                    <a className="btn btn-primary btn-block" href="recharge-order-summary.html">Continue to Recharge</a>
                </form>
            </div>

        );
    }
}
