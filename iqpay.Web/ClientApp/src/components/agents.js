﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { agentActions } from '../actions/agentsAction';
import { connect } from 'react-redux';
import CreateEditDealer from './create-edit-dealer';
import { agentsConstants } from '../constants/agentsConstants';
import { dealersActions } from '../actions/dealersAction';
import CommissionRate from './commission-rates';
import VendorCheckList from './vendor-checklist'

import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";
import 'datatables.net-buttons-dt';
import 'datatables.net-buttons/js/buttons.html5.js';
import jQuery from 'jquery';

class Agents extends React.Component {
    modalpopup = true;
    constructor(props) {
        super(props);

        this.state = {

            pageTitle: 'Manage Agents',
            agents: [],
            agentInfo: null,
            modalPopup: false,
            commissionRates: null,
            checkListItems: null,
            dealerId: null
        };


    }

    toggleActivate = (id, status) => {
        if (window.confirm("Are you sure you want to complete this task?")) {
            const { dispatch } = this.props;
            dispatch(agentActions.toggleActivate(id, status));
        }
    }

    updateSerivceCommissison = (serviceId, vendorId, dealerId, rate) => {
        if(window.confirm("Are you sure you want to proceed with these changes")){
            const{dispatch} = this.props;
            dispatch(dealersActions.updateDealerCommission(serviceId, vendorId, dealerId, rate)); 
        }
        
    }
    showServiceChecklist = (dealerId) => {
        const{dispatch} = this.props;
        this.setState({dealerId: dealerId});
        dispatch(dealersActions.fetchServiceChecklist(dealerId))
    }

    updateCheckListItems(checkListItems){
        if(window.confirm("Are you sure you want to update this vendor checklist")){
            const {dispatch} = this.props;
            dispatch(dealersActions.updateCheckListItems(this.state.dealerId, checkListItems));
        }                
    }

    closeCheckListModal = () => {
        const{dispatch} = this.props;
        dispatch(dealersActions.resetserviceChecklist());
        this.setState({checkListItems: null});
    }

    commissionRate = (dealerId) =>{
        const {dispatch} = this.props;
        dispatch(dealersActions.fetchCommissionRate(dealerId));
    }

    saveDealer = (dealerInfo) => {
        const { dispatch } = this.props;
        dispatch(agentActions.saveDealer(dealerInfo));
        //this.setState({ vendorInfo: null });
    }

    commissionRate = (dealerId) =>{
        const {dispatch} = this.props;
        dispatch(dealersActions.fetchCommissionRate(dealerId));
    }


    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if (this.props.agent.dealers !== undefined) {
                this.setState({ agents: this.props.agent.dealers });
            }

            if (this.props.agent.dealerInfo !== undefined) {
                this.setState({ agentInfo: this.props.agent.dealerInfo })
            }

            if(this.props.dealer.commissionRates !== undefined){
                this.setState({commissionRates: this.props.dealer.commissionRates});
            }
            if(this.props.dealer.dealerChecklist !== undefined)
            {
                this.setState({checkListItems: this.props.dealer.dealerChecklist});
            }

            jQuery(document).ready(function () 
            {
                jQuery('#tableData').DataTable({
                    dom: 'Bfrtip',
                    retrieve: true,
                    buttons: [
                        'csv', 'excel', 'pdf'
                    ]
                });
            });
        }
    }

    editDealer = (dealerId) => {
        const { dispatch } = this.props;
        dispatch(agentActions.fetchDealerDetails(dealerId));
    }

    addDealer = () => {
        this.setState({ agentInfo: {} });
    }

    closecommissionModal = () => {
        const{dispatch} = this.props;
        dispatch(dealersActions.resetDealerCommissionRate());
        this.setState({ commissionRates: null});
    }

    updateSerivceCommissison = (serviceId, vendorId, dealerId, rate) => {
        if(window.confirm("Are you sure you want to proceed with these changes")){
            const{dispatch} = this.props;
            dispatch(dealersActions.updateDealerCommission(serviceId, vendorId, dealerId, rate)); 
        }
        
    }

    closeModal = () => {
        this.setState({ agentInfo: null });
    }

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document

        const { dispatch } = this.props;
        dispatch(agentActions.fetchDealers());

    }
    render() {
        var agents = this.state.agents;
        let dealersTable = [];
        if (agents.length > 0) {
            agents.map((v) => {
                dealersTable.push(
                    <tr key={v.id}>
                        <td>{v.name}</td>
                        <td>{v.phonenumber}</td>
                        <td>{v.email}</td>
                        <td>{v.dateCreated}</td>
                        <td>
                            <Button color="info" onClick={() => { this.editDealer(v.id) }} className="pull-right" size="sm"><i className="fas fa-edit"></i> Edit</Button>
                            <Button color="info" onClick={()=>{this.showServiceChecklist(v.id)}} className="pull-right" size="sm"><i className="fas fa-edit"></i> Service Checklist</Button>
                            <Button color="info" onClick={() => { this.commissionRate(v.id) }} className="pull-right" size="sm"><i className="fas fa-cogs"></i> View Commission Rates</Button>
                            &nbsp; <Button color={v.isLockedOut ? "info" : "danger"} onClick={() => { this.toggleActivate(v.id, v.isLockedOut) }} className="pull-right" size="sm"><i className={v.isLockedOut ? "fas fa-check" : "fas fa-lock"}></i> {v.isLockedOut ? "Activate" : "Deactivate"}</Button>
                        </td>

                    </tr>);
            });
        } else {
            //dealersTable = <tr><td colspan='5'>Agents have not been setup</td></tr>;
        }

        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                        <Row>
                                            <Col>
                                                <Button color="" onClick={this.addDealer} className="pull-right" size="sm"><i className="fas fa-plus"></i> Create Agent</Button>
                                                <Table id="tableData" striped hover size="md">
                                                    <thead>
                                                        <tr>
                                                            <th>Name</th>
                                                            <th>Phone number</th>
                                                            <th>Email</th>
                                                            <th>Date Created</th>
                                                            <td></td>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {dealersTable}
                                                    </tbody>
                                                </Table>
                                            </Col>
                                            <VendorCheckList checkListItems={this.state.checkListItems} onCancel={this.closeCheckListModal} onSave ={(checkListItems)=>this.updateCheckListItems(checkListItems)} />
                                            <CreateEditDealer dealerType="Agent" onSave={this.saveDealer} onCancel={this.closeModal} dealerInfo={this.state.agentInfo} />
                                            <CommissionRate updateServiceCommissionCap= {(serviceId, vendorId, dealerId, cap) => this.updateServiceCommissionCap(serviceId, vendorId, dealerId, cap)} updateServiceCommission={(serviceId, vendorId, dealerId, rate) => this.updateSerivceCommissison(serviceId, vendorId, dealerId, rate)} rates={this.state.commissionRates} onCancel={this.closecommissionModal}/>
                                        </Row>

                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, agent, dealer } = state;

    return {
        alert, agent, dealer
    };
}

const connectedDealersPage = connect(mapStateToProps)(Agents);
export { connectedDealersPage as Agents };