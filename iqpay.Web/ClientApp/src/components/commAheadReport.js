import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import { superdealerActions } from '../actions/superdealerAction';
import { connect } from 'react-redux';
import CreateEditDealer from './create-edit-dealer';
import CommissionRate from './commission-rates';
import {dealersActions} from '../actions/dealersAction';
import { superdealerConstants } from '../constants/dealerConstants';
import { transactionAction } from '../actions/transactionAction';
import {roles} from '../constants/rolesConstants';
import numeral from 'numeral';
import DataTable from 'react-data-table-component';
import {downloadCSV} from '../helpers/history';
import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";
import FilterModal from "./common/filterModal";

import $ from 'jquery';



class CommissionAheadReports extends React.Component {
    modalpopup = true;
    columns = [
        {
            name: 'Dealer Code',
            selector: 'dealerCode',
            sortable: true,
        },
        {
            name: 'Dealer Name',
            selector: 'dealerName',
            sortable: true,
        },
        {
            name: 'Service',
            selector: 'service',
            sortable: true,
        },
        {
            name: 'Date',
            selector: 'transactionDate',
            sortable: true,
        },
        {
            name: 'Amount',
            selector: 'amount',
            sortable: true,
            cell: row => numeral(row.amount).format("0,00.00")
        },
        {
            name: 'Commission',
            selector: 'profit',
            sortable: true,
            cell: row => numeral(row.profit).format("0,00.00")
        }
    ]
    constructor(props) {
        super(props);
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));
        this.state = {
            role: currentUser.role,
            pageTitle: 'Commission Ahead Reports',
            logs: []            
        };
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if(this.props.transaction.commAheadReport != undefined){
                this.setState({logs: this.props.transaction.commAheadReport });                                    
            }                   
        }
    }

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document        
        const { dispatch } = this.props;
        dispatch(transactionAction.fetchCommAheadReport());        
    }

    onFilter = (startDate, endDate) => {
        const { dispatch } = this.props;
        dispatch(transactionAction.filterCommAheadReport(startDate, endDate));
    }

    render() {
        var transactions = this.state.logs;
        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                        <Row>
                                            <Col>
                                            <span className="float-right"><i class="fa fa-filter"></i><FilterModal onFilter={(startDate, endDate) => this.onFilter(startDate,endDate)}/></span>
                                            <DataTable
                                                    title="Commission Ahead List"
                                                    columns={this.columns}
                                                    data={transactions}
                                                    actions={<Button className="btn btn-sm" onClick={()=>downloadCSV(transactions)}>Export</Button>}
                                                    pagination
                                                    paginationPerPage={30}
                                                />                                            
                                            </Col>                                           
                                        </Row>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, transaction } = state;

    return {
        alert, transaction
    };
}

const connectedTransactionsPage = connect(mapStateToProps)(CommissionAheadReports);
export { connectedTransactionsPage as CommissionAheadReports };