import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Row, Col, Button } from 'reactstrap';
import { connect } from 'react-redux';
import { transactionAction } from '../actions/transactionAction';
import {roles} from '../constants/rolesConstants';
import numeral from 'numeral';
import FilterModal from './common/filterModal';
import DataTable from 'react-data-table-component';
import {downloadCSV} from '../helpers/history';
import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";
import { deliveryOptions } from '../constants/commissionMode';
import jQuery from 'jquery';



class PlacedOrders extends React.Component {
    modalpopup = true;   
    currentUser = JSON.parse(localStorage.getItem("currentUser")); 
    columns = [
        {
            name: 'Date',
            selector: 'transactionDate',
            sortable: true,
        },        
        {
            name: 'Product Ordered',
            selector: 'service',
            sortable: true,
        },
        {
            name: 'Quantity',            
            cell: row => row.numberOfPins+" "+row.unit
        },
        {
            name: 'Customer Name',
            selector: 'accountName',
            sortable: true,
        },
        {
            name: 'Phone number',
            selector: 'customerNumber',
            sortable: true,
        },        
        {
            name: 'Dealer/Agent',
            selector: 'agentName',
            sortable: true,
        },
        {
            name: 'Address Line 1',
            selector: 'accountAddress',
            sortable: true,
        },
        {
            name: 'Address Line 2',
            selector: 'accountAddressLine2',
            sortable: true,
        },
        {
            name: 'LGA',
            selector: 'lga',
            sortable: true,
        },
        {
            name: 'State',
            selector: 'state',
            sortable: true,
        },
        {
            name: 'Preferred Delivery Option',
            selector: 'state',
            cell: row => row.deliveryOption ? deliveryOptions.find(x=> x.key == row.deliveryOption).value : "N/A"
        },
        {
            name: 'Collection Point',
            selector: 'state',
            cell: row => row.collectionPoint ? row.collectionPoint : "N/A"
        },        
        {
            name: 'Amount',
            sortable: true,
            cell: row => numeral(row.amount).format("0,00.00")
        },
        {
            name: 'Delivery Fee',
            sortable: true,
            cell: row => numeral(row.deliveryFee).format("0,00.00")
        },
        {
            name: 'Status',
            selector: 'status',
            sortable: false,
        },               
        {
            name: '',
            button: true,
            cell: row => <a href={"/slip/"+row.vendingCode} target="_blank" rel="noopener noreferrer">Details</a>,
        },  
        {
            name: '',
            button: true,
            omit: this.currentUser.role == roles.AGENT,
            cell: row => <a href={"/order-summary/"+row.vendingCode} target="_blank" rel="noopener noreferrer">Process Order</a>
        }   
    ]

    constructor(props) {
        super(props);        
        this.state = {
            role: this.currentUser.role,
            pageTitle: 'Affordable Orders',
            logs: [],
            showFilterModal: false            
        };
    }

    processOrder = (e, vendingCode) => {
        e.preventDefault();
        alert(vendingCode);
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        console.log("props=>", this.props)
        if (prevprops !== this.props) {
            if(this.props.transaction.orders != undefined){
                this.setState({logs: this.props.transaction.orders });                                    
            }                           
        }
    }

    loadFilterModal = () =>{
        this.setState({showFilterModal: true})
    }

    onFilter = (startDate, endDate) => {
        const { dispatch } = this.props;
        dispatch(transactionAction.fetchFoodOrders(startDate, endDate));
    }

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document        
        const { dispatch } = this.props;
        dispatch(transactionAction.fetchFoodOrders(null, null));        
    }

    render() {
        var transactions = this.state.logs;
        let transactionTable = [];
        var transactionValue = 0;

        transactions.map(t=>{
            if(t.status == "Completed"){
                transactionValue += t.amount;
            }
        });
        
        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                        <Row>
                                            <Col>
                                            <span className="float-right"><i class="fa fa-filter"></i><FilterModal onFilter={(startDate, endDate) => this.onFilter(startDate,endDate)}/></span>
                                            <span className="text-right">Total Orders Costs: {numeral(transactionValue).format("0,00.00")}</span>
                                                <DataTable
                                                    title="All Orders List"
                                                    columns={this.columns}
                                                    data={this.state.logs}
                                                    actions={<Button className="btn btn-sm" onClick={()=>downloadCSV(this.state.logs)}>Export</Button>}
                                                    pagination
                                                    paginationPerPage={30}
                                                />
                                            </Col>                                           
                                        </Row>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </LoginMenu>                
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, transaction } = state;

    return {
        alert, transaction
    };
}

const connectedTransactionsPage = connect(mapStateToProps)(PlacedOrders);
export { connectedTransactionsPage as PlacedOrders };