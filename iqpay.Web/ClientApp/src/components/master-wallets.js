import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Badge } from 'reactstrap';
import { transactionAction } from '../actions/transactionAction';
import { connect } from 'react-redux';
import moment from 'moment';
import numeral from 'numeral';

class MasterWallets extends React.Component {
    modalpopup = true
    constructor(props) {
        super(props);

        this.state = {
            pageTitle: 'Master Wallet Balances',
            masterWallets: []            
        };
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            console.log(this.props);
            if (this.props.transaction.masterWallets !== undefined) {
                this.setState({ masterWallets: this.props.transaction.masterWallets });
            }            
        }
    }
    
    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document

        const { dispatch } = this.props;
        dispatch(transactionAction.fetchMasterWalletsBalances());

    }

    render() {
        console.log("wallets=>",this.state.masterWallets)
        var masterWallets = this.state.masterWallets;
        let masterWalletsTable = [];
        if (masterWallets.length > 0) {
            console.log(masterWallets);
            masterWallets.map((v) => {
                masterWalletsTable.push(
                    <tr key={v.processorId}>
                        <td className="align-middle" >{v.processorName}</td>
                        <td className="align-middle">{numeral(v.balance.toFixed(2)).format("0,00.00")}</td>                        
                    </tr>);
            });
        } else {
            masterWalletsTable = <tr><td colSpan='7'>No request have been made</td></tr>;
        }

        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                        
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                    <h4 class="mb-3">Master Wallets Balances</h4>
                                        <Row>
                                            <Col md={12}> 
                                                <div className="alert alert-info">Please refresh this page to view most recent balances as this is not a live feed. And balances could have changed. Last retrieval time <Badge color="info">{moment(new Date()).format("DD ddd MMM yyyy, hh:mm A")}</Badge></div>
                                                <Table className="table table-hover border" striped hover size="md">
                                                    <thead>
                                                        <tr>
                                                            <th>Processor Name</th>
                                                            <th>Wallet Balance</th>                                                            
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        {masterWalletsTable}
                                                    </tbody>
                                                </Table>
                                            </Col>
                                        </Row>
                                    </div>

                                </div>
                            </div>

                        </div>

                        
                    </div>

                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, transaction } = state;

    return {
        alert, transaction
    };
}

const walletFundingRequest = connect(mapStateToProps)(MasterWallets);
export { walletFundingRequest as MasterWallets };
