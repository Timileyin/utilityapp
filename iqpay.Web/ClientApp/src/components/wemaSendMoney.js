import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import { status } from '../constants/commissionMode';
import { transactionAction } from '../actions/transactionAction';
export default class WemaSendMoneyModal extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            sendMoneyData: this.props.sendMoneyData,
            bankList: this.props.bankList,
            dealerType: this.props.dealerType,
            disableSend: true
        };
    }

    doTransfer = () => {
        
        console.log(this.state.sendMoneyData);
    }

    handleCreateChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };

        if (name === 'bankName') {
            theState.sendMoneyData.bankCode = value;
            theState.sendMoneyData.bankName = this.state.bankList.find(x=>x.bankCode == value)?.bankName;
            if(theState.sendMoneyData.accountNumber.length == 10)
            {
                theState.sendMoneyData.bankAccountNumber = theState.sendMoneyData.accountNumber;
                theState.disableSend = true;
                this.props.verifyAccoutNumber(this.state.sendMoneyData.accountNumber, value);
            }
        }

        if (name === 'accountNumber') {
            theState.disableSend = true;
            theState.sendMoneyData.accountName  = "";
            theState.sendMoneyData.bankAccountNumber = value;
            theState.sendMoneyData.accountNumber = value;
            if(value.length == 10){
                this.props.verifyAccoutNumber(value, this.state.sendMoneyData.bankCode);
            }else{
                theState.disableSend = true;
            }            
        }

        if (name === 'amount') {
            theState.sendMoneyData.amount = value;
        }

        if (name === 'narration') {
            theState.sendMoneyData.narration = value;
        }        
       
        this.setState(theState);
    }

    componentDidUpdate = (prevprops) => {

        if (prevprops !== this.props) {
            this.setState({ isOpen: this.props.isOpen, sendMoneyData: this.props.sendMoneyData, bankList: this.props.bankList});
            if(this.props.sendMoneyData?.accountName)
            {
                this.setState({disableSend: !this.props.sendMoneyData?.accountName}); 
            }
        }
    }

    render() {
        var sendMoneyData;
        var bankList = this.state.bankList;
        let bankOptions = []
        var bankKey = 1
        if(bankList.length > 0)
        {
            bankList.map(b=>{
                bankOptions.push(<option key={bankKey} value={b.code}>{b.name}</option>)
                bankKey++;
            });
        }
        if (this.state.sendMoneyData == null) {
            sendMoneyData = {};
        } else {
            sendMoneyData = this.state.sendMoneyData;
        }

        return (

            <Modal isOpen={this.state.sendMoneyData != null} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    Send Money
                </ModalHeader>
                <ModalBody>
                    <Form>
                        <Row form>
                            
                            <Col md={12}>
                            <FormGroup>
                                    <Label for="examplePassword">Beneficiairy Bank Name</Label>
                                    <select name="bankName" onChange={this.handleCreateChange} className="custom-select" id="electricityOperator" required="">
                                        <option value="">Select Beneficiairy Bank</option>
                                        {bankOptions}
                                    </select>                                    
                                </FormGroup>

                                <FormGroup>
                                    <Label for="examplePassword">Beneficiary Account Number</Label>
                                    <Input type="text" name="accountNumber" id="accountNumber" onChange={this.handleCreateChange} bsSize="sm" placeholder="Enter beneficiary account number"></Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="examplePassword">Beneficiary Name</Label>
                                    <Input type="text" name="beneficiaryName" id="beneficiaryName" value={sendMoneyData.accountName} disabled={true} onChange={this.handleCreateChange} bsSize="sm" placeholder="Beneficiary name"></Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="examplePassword">Amount</Label>
                                    <Input type="text" name="amount" id="amount" disabled={this.state.disableSend} onChange={this.handleCreateChange} bsSize="sm" placeholder="Amount"></Input>
                                </FormGroup>
                                <FormGroup>
                                    <Label for="examplePassword">Narration</Label>
                                    <Input type="textarea" name="narration" id="narration" disabled={this.state.disableSend} onChange={this.handleCreateChange} bsSize="sm" placeholder=""></Input>
                                </FormGroup>
                            </Col>
                        </Row>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" disabled={this.state.disableSend} onClick={() => { this.props.doTransfer(this.state.sendMoneyData) }}>Complete</Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}