﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import { status, commissionMode, serviceTypes, deliveryTypes } from '../constants/commissionMode';

export default class CreateEditService extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            serviceInfo: this.props.serviceInfo,
            isFixedPrice: true,
            requiresVerification: true//this.props.serviceInfo.isFixedPrice
        };

    }

    handleCreateChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };

        if (name === 'name') {
            theState.serviceInfo.name = value;
        }

        if (name === 'description') {
            theState.serviceInfo.description = value;
        }

        if (name === 'price') {
            theState.serviceInfo.amount = value;
        }

        if (name === 'status') {
            theState.serviceInfo.statusInt = value;
            theState.serviceInfo.status = value;
        }

        if (name === 'serviceType') {
            theState.serviceInfo.serviceTypeInt = value;
            theState.serviceInfo.serviceType = value;
        }

        if (name === "commissionMode") {
            theState.serviceInfo.commissionMode = value
        }

        if (name === "fixedPrice") {
            theState.serviceInfo.isFixedPrice = !this.state.isFixedPrice;
            theState.isFixedPrice = !this.state.isFixedPrice;
        }

        if (name === "requiresVerification") {
            theState.serviceInfo.requiresVerification = !this.state.requiresVerification;
            theState.requiresVerification = !this.state.requiresVerification;
        }

        if(name === "deliveryType"){
            theState.serviceInfo.deliveryTypeInt = value;
            theState.serviceInfo.deliveryType = value;
        }

        if(name === "isMeasuredService"){
            theState.serviceInfo.isMeasuredService = !theState.serviceInfo.isMeasuredService;
            theState.isMeasuredService = !theState.isMeasuredService;
        }

        if(name === "unitType"){
            theState.serviceInfo.unitType = value;
            theState.unitType = value;
        }

        if(name === "flatDeliveryFee"){
            theState.serviceInfo.flatDeliveryFee = value;
            theState.flatDeliveryFee = value;
        }

        if(name === "flatDeliveryFeeCap"){
            theState.serviceInfo.flatDeliveryFeeCap = value;
            theState.flatDeliveryFeeCap = value;
        }

        if(name === "flatCollectionFee"){
            theState.serviceInfo.flatCollectionFee = value;
            theState.flatCollectionFee = value;
        }

        if(name === "flatCollectionFeeCap"){
            theState.serviceInfo.flatCollectionFeeCap = value;
            theState.flatCollectionFeeCap = value;
        }

        if(name === "isSharedProduct"){
            theState.serviceInfo.isSharedProduct = !theState.serviceInfo.isSharedProduct;   
            theState.isSharedProduct = !theState.isSharedProduct         
        }

        if(name === "lastOrderDate"){
            theState.serviceInfo.lastOrderDate = value;
            theState.lastOrderDate = value;
        }

        
        if(name === "maxOrders"){
            theState.serviceInfo.maxOrders = value;
            theState.maxOrders = value;
        }

        if(name === "flatDeliveryFeeBeyondCap"){
            theState.serviceInfo.flatDeliveryFeeBeyondCap = value;
            theState.flatDeliveryFeeBeyondCap = value;
        }

        if(name === "flatCollectionFeeBeyondCap"){
            theState.serviceInfo.flatCollectionFeeBeyondCap = value;
            theState.flatCollectionFeeBeyondCap = value;
        }

        this.setState(theState);
    }

    componentDidUpdate = (prevprops) => {
        if (prevprops !== this.props) {
            if (this.props.serviceInfo != null) {
                this.setState({ isOpen: this.props.isOpen, serviceInfo: this.props.serviceInfo, isFixedPrice: this.props.serviceInfo.isFixedPrice, requiresVerification: this.props.serviceInfo.requiresVerification })
            } else {
                this.setState({ isOpen: this.props.isOpen, serviceInfo: this.props.serviceInfo });
            }
        }
    }

    render() {
        var serviceInfo;
        if (this.state.serviceInfo == null) {
            serviceInfo = {};
        } else {
            serviceInfo = this.state.serviceInfo;
        }

        return (

            <Modal isOpen={this.state.serviceInfo != null} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    {serviceInfo.id == undefined ? "Create Service" : "Edit " + serviceInfo.name}
                </ModalHeader>
                <ModalBody>
                    <Form>
                        <Row form>
                            <Col md={12}>
                                <FormGroup>
                                    <Label for="exampleEmail">Service Name</Label>
                                    <Input value={serviceInfo.name} type="text" onChange={this.handleCreateChange} name="name" bsSize="sm" id="name" placeholder="Service name" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Price</Label>
                                    <Input disabled={this.state.isFixedPrice} value={serviceInfo.amount} type="text" onChange={this.handleCreateChange} name="price" bsSize="sm" id="price" placeholder="Price" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Service Type</Label>
                                    <Input value={serviceInfo.serviceTypeInt} onChange={this.handleCreateChange} name="serviceType" id="serviceType" type="select" bsSize="sm">
                                        <option>--Please choose one--</option>
                                        {
                                            serviceTypes.map((c) =>
                                                <option key={c.key} value={c.key}>{c.value}</option>
                                            )
                                        }
                                    </Input>
                                </FormGroup>
                            </Col>

                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Delivery Type</Label>
                                    <Input value={serviceInfo.deliveryTypeInt} onChange={this.handleCreateChange} name="deliveryType" id="serviceType" type="select" bsSize="sm">
                                        <option>--Please choose one--</option>
                                        {
                                            deliveryTypes.map((c) =>
                                                <option key={c.key} value={c.key}>{c.value}</option>
                                            )
                                        }
                                    </Input>
                                </FormGroup>
                            </Col>

                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Commission Mode</Label>
                                    <Input value={serviceInfo.commissionMode} onChange={this.handleCreateChange} name="commissionMode" id="serviceType" type="select" bsSize="sm">
                                        <option>--Please choose one--</option>
                                        {
                                            commissionMode.map(
                                                (c) => <option key={c.key} value={c.key}>{c.value}</option>
                                            )
                                        }
                                    </Input>
                                </FormGroup>
                            </Col>                            
                            <Col md={12}>
                                <FormGroup>
                                    <Label for="exampleEmail">Status</Label>
                                    <Input value={serviceInfo.statusInt} onChange={this.handleCreateChange} name="status" id="status" type="select" bsSize="sm">
                                        <option>--Please choose one--</option>
                                        {
                                            status.map((c) =>
                                                <option key={c.key} value={c.key}>{c.value}</option>
                                            )
                                        }
                                    </Input> </FormGroup>
                            </Col>
                            <Col md={12}>
                                <FormGroup check>
                                    <Label check>
                                        <Input value={serviceInfo.isMeasuredService} defaultChecked={serviceInfo.isMeasuredService} type="checkbox" onChange={this.handleCreateChange} name="isMeasuredService" bsSize="sm" id="price" placeholder="Price" />Please check this box if this product is a measured product
                                    </Label>
                                    <Input hidden={!serviceInfo.isMeasuredService} value={serviceInfo.unitType} type="text" onChange={this.handleCreateChange} name="unitType" bsSize="sm" id="price" placeholder="Unit" />
                                </FormGroup>
                            </Col>

                            <Col md={12}>
                                <FormGroup check>
                                    <Label check>
                                        <Input value={serviceInfo.isSharedProduct} defaultChecked={serviceInfo.isSharedProduct} type="checkbox" onChange={this.handleCreateChange} name="isSharedProduct" bsSize="sm" id="price" placeholder="Price" />Please check this box if this product is a shared product
                                    </Label>
                                    <Input hidden={!serviceInfo.isSharedProduct} value={serviceInfo.maxOrders} type="text" onChange={this.handleCreateChange} name="maxOrders" bsSize="sm" id="maxOrders" placeholder="Max no. orders" />
                                    <Input hidden={!serviceInfo.isSharedProduct} value={serviceInfo.lastOrderDate} type="text" onChange={this.handleCreateChange} name="lastOrderDate" bsSize="sm" id="lastOrderDate" placeholder="Order end date" />
                                </FormGroup>
                            </Col>

                            <Col md={12}>
                                <FormGroup>
                                    <Label for="examplePassword">Description</Label>
                                    <Input type="textarea" name="description" id="description" onChange={this.handleCreateChange} value={serviceInfo.description} bsSize="sm" placeholder="Description"></Input>
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Flat Delivery Fee:</Label>
                                    <Input value={serviceInfo.flatDeliveryFee} onChange={this.handleCreateChange} name="flatDeliveryFee" id="flatDeliveryFee" type="text" bsSize="sm"></Input>
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Flat Delivery Cap Weight:</Label>
                                    <Input value={serviceInfo.flatDeliveryFeeCap} onChange={this.handleCreateChange} name="flatDeliveryFeeCap" id="flatDeliveryFeeCap" type="text" bsSize="sm"></Input>
                                </FormGroup>
                            </Col>                              
                            <Col md={12}>
                                <FormGroup>
                                    <Label for="exampleEmail">Delivery Fee Beyond Cap(per kg):</Label>
                                    <Input value={serviceInfo.flatDeliveryFeeBeyondCap} onChange={this.handleCreateChange} name="flatDeliveryFeeBeyondCap" id="flatDeliveryFeeBeyondCap" type="text" bsSize="sm"></Input>
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Collection Fee:</Label>
                                    <Input value={serviceInfo.flatCollectionFee} onChange={this.handleCreateChange} name="flatCollectionFee" id="flatCollectionFee" type="text" bsSize="sm"></Input>
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Collection Cap Weight:</Label>
                                    <Input value={serviceInfo.flatCollectionFeeCap} onChange={this.handleCreateChange} name="flatCollectionFeeCap" id="flatCollectionFeeCap" type="text" bsSize="sm"></Input>
                                </FormGroup>
                            </Col>                              
                            <Col md={12}>
                                <FormGroup>
                                    <Label for="exampleEmail">Collection Fee Beyond Cap(per kg):</Label>
                                    <Input value={serviceInfo.flatCollectionFeeBeyondCap} onChange={this.handleCreateChange} name="flatCollectionFeeBeyondCap" id="flatDeliveryFeeBeyondCap" type="text" bsSize="sm"></Input>
                                </FormGroup>
                            </Col> 
                            <Col md={12}>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox" name="fixedPrice" checked={this.state.isFixedPrice} onChange={this.handleCreateChange} />Please check this box if the price is not fixed
                                    </Label>
                                    <Label check>
                                        <Input type="checkbox" name="requiresVerification" checked={this.state.requiresVerification} onChange={this.handleCreateChange} />Please check this box if service requires customer verification
                                    </Label>
                                </FormGroup>
                            </Col>                                 
                        </Row>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => { this.props.onSave(this.state.serviceInfo) }}>Save</Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}