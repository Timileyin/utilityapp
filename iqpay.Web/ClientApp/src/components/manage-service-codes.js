﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import { status, commissionMode, serviceTypes } from '../constants/commissionMode';
export default class ManageServiceCodes extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            serviceCodes: this.props.serviceCodes,            
        };

    }

    handleCreateChange = (serviceCodes,e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        var serviceCodes = theState.serviceCodes;
        serviceCodes.map((c) => {
            if (c.processorCode == name) {
                c.processorServiceCode = value;
            }
        });
        theState.serviceCodes = serviceCodes;
        this.setState(theState);
    }



    componentDidUpdate = (prevprops) => {

        if (prevprops !== this.props) {
            if (this.props.serviceCodes != null) {
                this.setState({ isOpen: this.props.isOpen, serviceCodes: this.props.serviceCodes})
            } else {
                this.setState({ isOpen: this.props.isOpen, serviceCodes: this.props.serviceCodes });
            }
        }
    }




    render() {
        var serviceCodes;
        if (this.state.serviceCodes == null) {
            serviceCodes = [];
        } else {
            serviceCodes = this.state.serviceCodes;
        }

        var codeForm = [];

        serviceCodes.map((c) => {
            codeForm.push(
                <Col md={6}>
                    <FormGroup>
                        <Label for="exampleEmail">{c.processorName}</Label>
                        <Input value={c.processorServiceCode} type="text" onChange={(e) => this.handleCreateChange(serviceCodes, e)} name={c.processorCode} bsSize="sm" id={c.processorCode} placeholder={c.processorName} />
                    </FormGroup>
                </Col>
            )
        });
        


        return (

            <Modal isOpen={this.state.serviceCodes != null} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    Update Processor Codes
                </ModalHeader>
                <ModalBody>
                    <Form>
                        <Row form>
                            {codeForm}
                        </Row>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => { this.props.onSave(this.state.serviceCodes[0].serviceId, this.state.serviceCodes) }}>Save</Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}