﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import {roles} from '../constants/rolesConstants';
import { connect } from 'react-redux';
import { userActions } from '../actions/userAction';
import numeral from 'numeral';

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));

        this.state = {
            isVisible: this.props.isVisible,
            pageTitle: 'My Dashboard',
            role: currentUser.role,
            dealerCode: currentUser.username,
            pageDetails: {                
                transactionCount: 0,
                walletBalance: 0.00,
                commissionBalance: 0.00,
                todayIncome: 0.00,
                dealers: 0,
                superDealers: 0,
                agents: 0
            }
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if(this.props.authentication.dashboard != null){
                this.setState({ pageDetails: this.props.authentication.dashboard })
            }
            
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document   
        const {dispatch} = this.props;
        dispatch(userActions.fetchDashboardData());      
    }

    render() {
        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    
                                         
                        <div className="col-lg-9"> 
                        {
                            this.state.role == "AGENT" || this.state.role == "SUPER DEALER" || this.state.role == "DEALER" ?
                            <div className="alert alert-info">
                                Your personified partnership request form: <a href={"https://iqpay.com.ng/register?a="+this.state.dealerCode}>{"https://iqpay.com.ng/register?a="+this.state.dealerCode}</a>
                            </div> : "" 
                        }                                                  
                            <div className="row">
                                <div className="col-sm-6 col-lg-3 mb-4" >
                                    <div className="card shadow-sm border-0">
                                        <div className="card-body">
                                            <span className="d-block text-16 text-center text-primary my-4"><i className="fas fa-life-ring"></i></span>
                                            <h5 className="card-title text-4 text-center">Transactions</h5>                                        
                                        </div>
                                        <ul className="list-group list-group-flush">
                                            <a className="list-group-item list-group-item-action" href="/transactions">{numeral(this.state.pageDetails.transactionCount).format("0,00")}</a>
                                        </ul>
                                    </div>
                                </div>

                                <div className="col-sm-6 col-lg-3 mb-4">
                                    <div className="card shadow-sm border-0">
                                        <div className="card-body">
                                            <span className="d-block text-16 text-center text-primary my-4"><i className="fas fa-life-ring"></i></span>
                                            <h5 className="card-title text-4 text-center">Wallet Balance</h5>                                        
                                        </div>
                                        <ul className="list-group list-group-flush">
                                            <a className="list-group-item list-group-item-action" href="/wallets">{numeral(this.state.pageDetails.walletBalance.toFixed(2)).format("0,00.00")}</a>
                                        </ul>
                                    </div>
                                </div>

                                <div className="col-sm-6 col-lg-3 mb-4">
                                    <div className="card shadow-sm border-0">
                                        <div className="card-body">
                                            <span className="d-block text-16 text-center text-primary my-4"><i className="fas fa-life-ring"></i></span>
                                            <h5 className="card-title text-4 text-center">Comm. Wallet Balance</h5>                                       
                                        </div>
                                        <ul className="list-group list-group-flush">
                                            <a className="list-group-item list-group-item-action" href="/wallets">{numeral(this.state.pageDetails.commissionBalance.toFixed(2)).format("0,00.00")}</a>
                                        </ul>
                                    </div>
                                </div>

                                <div hidden={this.state.role == roles.REGULAR_CUSTOMER} className="col-sm-6 col-lg-3 mb-4">
                                    <div className="card shadow-sm border-0">
                                        <div className="card-body">
                                            <span className="d-block text-16 text-center text-primary my-4"><i className="fas fa-life-ring"></i></span>
                                            <h5 className="card-title text-4 text-center">Today's Income</h5>                                        
                                        </div>
                                        <ul className="list-group list-group-flush">
                                            <a className="list-group-item list-group-item-action" href="/transactions">{numeral(this.state.pageDetails.todayIncome.toFixed(2)).format("0,00.00")}</a>
                                        </ul>
                                    </div>
                                </div>

                                <div hidden={this.state.role != roles.SUPERADMIN } className="col-sm-6 col-lg-3 mb-4">
                                    <div className="card shadow-sm border-0">
                                        <div className="card-body">
                                            <span className="d-block text-16 text-center text-primary my-4"><i className="fas fa-life-ring"></i></span>
                                            <h5 className="card-title text-4 text-center">Super Dealers</h5>                                        
                                        </div>
                                        <ul className="list-group list-group-flush">
                                            <a className="list-group-item list-group-item-action" href="/super-dealers">{this.state.pageDetails.superDealers}</a>
                                        </ul>
                                    </div>
                                </div>

                                <div hidden={this.state.role != roles.SUPERADMIN && this.state != roles.SUPERDEALER } className="col-sm-6 col-lg-3 mb-4">
                                    <div className="card shadow-sm border-0">
                                        <div className="card-body">
                                            <span className="d-block text-16 text-center text-primary my-4"><i className="fas fa-life-ring"></i></span>
                                            <h5 className="card-title text-4 text-center">Dealers</h5>                                        
                                        </div>
                                        <ul className="list-group list-group-flush">
                                            <a className="list-group-item list-group-item-action" href="/dealers">{this.state.pageDetails.dealers}</a>
                                        </ul>
                                    </div>
                                </div>

                                

                                <div className="col-sm-6 col-lg-3 mb-4" hidden={this.state.role == roles.AGENT || this.state.role == roles.REGULAR_CUSTOMER }>
                                    <div className="card shadow-sm border-0">
                                        <div className="card-body">
                                            <span className="d-block text-16 text-center text-primary my-4"><i className="fas fa-life-ring"></i></span>
                                            <h5 className="card-title text-4 text-center">Agents</h5>                                        
                                        </div>
                                        <ul className="list-group list-group-flush">
                                            <a className="list-group-item list-group-item-action" href="/agents">{this.state.pageDetails.agents}</a>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, authentication } = state;

    return {
        alert, authentication
    };
}

const connectedDashboardPage = connect(mapStateToProps)(Dashboard);
export { connectedDashboardPage as Dashboard };
