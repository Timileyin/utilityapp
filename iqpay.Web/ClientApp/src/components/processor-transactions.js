import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Badge } from 'reactstrap';
import { transactionAction } from '../actions/transactionAction';
import { connect } from 'react-redux';
import {roles} from '../constants/rolesConstants';
import moment from 'moment';
import numeral from 'numeral';
import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";
import DataTable from "react-data-table-component";
import {downloadCSV} from '../helpers/history';
import FilterModal from './common/filterModal';
import $ from 'jquery';


class ProcessorTransaction extends React.Component {
    modalpopup = true
    currentUser = JSON.parse(localStorage.getItem("currentUser"))
    role =  this.currentUser.role
    columns = [
        {
            name: 'Date',
            selector: 'transactionDate',
            sortable: true,
        },
        {
            name: 'Vending Code',
            selector: 'vendingCode',
            sortable: true,
        },
        {
            name: 'Dealer Code',
            selector: 'dealerCode',
            sortable: true,
        },
        {
            name: 'Dealers Name',
            selector: 'agentName',
            sortable: true,
        },
        {
            name: 'Service',
            selector: 'service',
            sortable: true,
        },
        {
            name: 'Wallet Balance',
            selector: 'walletBalance',
            sortable: true,
        },        
        {
            name: 'Amount',
            sortable: true,
            cell: row => numeral(row.amount).format("0,00.00")
        },
        {
            name: 'Status',
            selector: 'status',
            sortable: false,
        },
        {
            name: 'Agent Comm.',
            sortable: false,
            cell: row => numeral(row.agentCommission).format("0,00.00")
        },
        {
            name: 'Dealer Comm.',
            sortable: false,
            cell: row => numeral(row.dealerCommission).format("0,00.00"),
            omit: this.currentUser.role == roles.AGENT
        },
        {
            name: 'Super Dealer Comm.',
            sortable: false,
            omit: this.currentUser.role != roles.SUPERDEALER && this.currentUser.role != roles.SUPERADMIN,
            cell: row => numeral(row.superDealerCommission).format("0,00.00")
        },
        {
            name: 'IQPAY Profit',
            sortable: false,
            omit: this.currentUser.role != roles.SUPERADMIN,
            cell: row => numeral(row.iqpayProfit).format("0,00.00")
        },
        {
            name: '',
            button: true,
            cell: row => <a href={"/slip/"+row.vendingCode} target="_blank" rel="noopener noreferrer">Details</a>,
        }   
    ]
    constructor(props) {
        super(props);
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));
        this.state = {
            pageTitle: 'Processor Vending',
            processorReport: [],
            processorId: this.props.match.params.processorId,
            processorName: this.props.match.params.processorName,
            role: currentUser.role       
        };
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if (this.props.transaction.processorReport !== undefined) {
                this.setState({ processorReport: this.props.transaction.processorReport });                
            }            
        }
    }

    onFilter = (startDate, endDate) => 
    {
        const { dispatch } = this.props;
        dispatch(transactionAction.filterProcessorTransactionByProcessor(this.state.processorId, startDate, endDate));
    }
    
    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document
        const { dispatch } = this.props;
        dispatch(transactionAction.fetchProcessorTransactionByProcessor(this.state.processorId));
    }

    render() {

        var processorReport = this.state.processorReport;
        let processorReportTable = [];
        var totalValue = 0;
        if (processorReport.length > 0) {            
            processorReport.map((v) => {    
                if(v.status == 'Completed')
                {
                    totalValue += v.amount;
                }             
                
            });
            
        } else {
            processorReportTable = <tr><td colSpan='9'>No vending have been made</td></tr>;
        }

        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                        
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                        <h4 class="mb-3">Processor Vending {" - "+this.state.processorName} </h4>
                                        <Row>
                                            <Col md={12}> 
                                                <div className="alert alert-info">Please refresh this page to view most recent report as this is not a live feed. And balances could have changed. Last retrieval time <Badge color="info">{moment(new Date()).format("DD ddd MMM yyyy, hh:mm A")}</Badge></div>                                                
                                                <span className="text-right">Total: {numeral(totalValue.toFixed(2)).format("0,00.00")}</span>
                                                <span className="float-right"><i class="fa fa-filter"></i><FilterModal onFilter={(startDate, endDate) => this.onFilter(startDate,endDate)}/></span>
                                                <DataTable
                                                    title={"Processor Vending  - "+this.state.processorName}
                                                    columns={this.columns}
                                                    data={this.state.processorReport}
                                                    actions={<Button className="btn btn-sm" onClick={()=>downloadCSV(processorReport)}>Export</Button>}
                                                    pagination
                                                    paginationPerPage={30}
                                                />
                                            </Col>
                                        </Row>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>
                </LoginMenu>
            </div>
        );
    }
}

function mapStateToProps(state) {

    const { alert, transaction } = state;

    return {
        alert, transaction
    };
}

const walletFundingRequest = connect(mapStateToProps)(ProcessorTransaction);
export { walletFundingRequest as ProcessorTransaction };
