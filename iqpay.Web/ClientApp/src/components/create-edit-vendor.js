﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input, Collapse, Card, CardBody, CardTitle } from 'reactstrap';
import { status } from '../constants/commissionMode';
import jQuery from 'jquery';
export default class CreateEditVendor extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            vendorInfo: this.props.vendorInfo,
            isCommissionAhead: this.props.vendorInfo?.isCommissionAhead
        };


    }

    handleCreateChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };

        if (name === 'name') {
            theState.vendorInfo.name = value;
        }

        if (name === 'description') {
            theState.vendorInfo.description = value;
        }

        if (name === 'balance') {
            theState.vendorInfo.balance = value;
        }

        if (name === 'status') {
            theState.vendorInfo.statusInt = value;
            theState.vendorInfo.status = value;
        }

        if(name === 'isCommissionAhead'){
            theState.vendorInfo.isCommissionAhead = !theState.vendorInfo.isCommissionAhead;
            theState.isCommissionAhead = !theState.isCommissionAhead;
        }

        if(name === 'enableHouseHoldBonus')
        {
            theState.vendorInfo.enableHouseHoldBonus = !theState.vendorInfo.enableHouseHoldBonus;   
        }

        if(name === 'houseHoldBonusDates')
        {
            theState.vendorInfo.houseHoldBonusDates = value;   
        }

        if(name === 'houseHoldBonusPercentage')
        {
            theState.vendorInfo.houseHoldBonusPercentage = value;   
        }

        if(name === 'houseHoldBonusCap')
        {
            theState.vendorInfo.houseHoldBonusCap = value;   
        }



        this.setState(theState);
    }

    componentDidUpdate = (prevprops) => {

        if (prevprops !== this.props) {
            this.setState({ isOpen: this.props.isOpen, vendorInfo: this.props.vendorInfo,isCommissionAhead: this.props.vendorInfo?.isCommissionAhead })
        }
    }

    triggerLogoFileUploadChange = () => {
        jQuery("#logoFileUpload").trigger("click");
    }

    render() {
        var vendorInfo;
        if (this.state.vendorInfo == null) {
            vendorInfo = {};
        } else {
            vendorInfo = this.state.vendorInfo;
        }

        return (

            <Modal isOpen={this.state.vendorInfo != null} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    {vendorInfo.id == undefined ? "Create Vendor" : "Edit " + vendorInfo.name}
                </ModalHeader>
                <ModalBody>
                    <img src={vendorInfo.logoSrc} id="logoImg" width="50%" height="50%"/><br />
                    <Input type="file" onChange={(e) => this.props.uploadVendorLogo(e)} id="logoFileUpload" hidden={true} />
                    <Button onClick={this.triggerLogoFileUploadChange} className="btn btn-primary btn-sm">Upload Logo</Button>                    
                    <Form>
                        <Row form>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Vendor Name</Label>
                                    <Input value={vendorInfo.name} type="text" onChange={this.handleCreateChange} name="name" bsSize="sm" id="name" placeholder="Vendor name" />
                                </FormGroup>
                            </Col>                            
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Status</Label>
                                    <Input value={vendorInfo.statusInt} onChange={this.handleCreateChange} name="status" id="status" type="select" bsSize="sm">
                                        
                                        {
                                            status.map((c) =>
                                                <option key={c.key} value={c.key}>{c.value}</option>
                                            )
                                        }
                                    </Input> </FormGroup>
                            </Col>
                            <Col md={12}>                                
                                <input type="checkbox" checked={vendorInfo.enableHouseHoldBonus} name="enableHouseHoldBonus" onChange={this.handleCreateChange}/> Enable Household Bonus                                                                       
                                
                                <Collapse isOpen={vendorInfo.enableHouseHoldBonus} >
                                    <Card>                                    
                                        <CardBody>
                                        <CardTitle>                                    
                                        </CardTitle>
                                                <Col md={12}>
                                                    <FormGroup>
                                                        <Label for="examplePassword">Household Calendar Bonus Dates (Separate multiple dates with commas)</Label>
                                                        <Input type="text"  name="houseHoldBonusDates" id="houseHoldBonusDates" onChange={this.handleCreateChange} value={vendorInfo.houseHoldBonusDates} bsSize="sm" placeholder="Calendar Bonus Dates"></Input>
                                                    </FormGroup>
                                                </Col>
                                                <Col md={12}>
                                                    <FormGroup>
                                                        <Label for="examplePassword">Household Bonus Percentage</Label>
                                                        <Input type="number"  name="houseHoldBonusPercentage" id="houseHoldBonusPercentage" onChange={this.handleCreateChange} value={vendorInfo.houseHoldBonusPercentage} bsSize="sm" placeholder="Percentage Bonus"></Input>
                                                    </FormGroup>
                                                </Col>
                                                <Col md={12}>
                                                    <FormGroup>
                                                        <Label for="examplePassword">Household Bonus Cap</Label>
                                                        <Input type="number"  name="houseHoldBonusCap" id="houseHoldBonusCap" onChange={this.handleCreateChange} value={vendorInfo.houseHoldBonusCap} bsSize="sm" placeholder="Percentage Bonus"></Input>
                                                    </FormGroup>
                                                </Col>                                                                       
                                        </CardBody>
                                    </Card>  
                                </Collapse>                          
                            </Col>
                            <Col md={12}>
                                <FormGroup>
                                    <Label for="examplePassword">Description</Label>
                                    <Input type="textarea"  name="description" id="description" onChange={this.handleCreateChange} value={vendorInfo.description} bsSize="sm" placeholder="Description"></Input>
                                </FormGroup>
                            </Col>
                            <input type="checkbox" checked={this.state.isCommissionAhead} name="isCommissionAhead" onChange={this.handleCreateChange}/>Check this box if vendor has commission ahead services
                        </Row>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => { this.props.onSave(this.state.vendorInfo) }}>Save</Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}