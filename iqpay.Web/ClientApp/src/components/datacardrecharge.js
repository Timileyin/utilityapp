﻿import React from 'react';
import $ from 'jquery';

window.$ = window.jQuery = $;
require('easy-responsive-tabs');

export default class DataCardRecharge extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: this.props.isVisible,
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ isVisible: this.props.isVisible })
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document


    }
    render() {
        return (

            <div style={!this.state.isVisible ? { display: 'none' } : null}>
                <h2 className="text-6 mb-4">DataCard Recharge or Bill Payment</h2>
                <form id="datacardRechargeBill" method="post">
                    <div className="mb-3">
                        <div className="custom-control custom-radio custom-control-inline">

                            <label className="custom-control-label" htmlFor="datacardPrepaid">Prepaid</label>
                        </div>
                        <div className="custom-control custom-radio custom-control-inline">
                            
                            <label className="custom-control-label" htmlFor="datacardPostpaid">Postpaid</label>
                        </div>
                    </div>
                    <div className="form-group">
                        <label htmlFor="dataCardNumber">DataCard Number</label>
                        <input type="text" className="form-control" data-bv-field="number" id="dataCardNumber" required placeholder="Enter DataCard Number" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="dataCardOperator">Your Operator</label>
                        <select id="dataCardOperator" className="custom-select" required="">
                            <option value="">Select Your Operator</option>
                            <option>1st Operator</option>
                            <option>2nd Operator</option>
                            <option>3rd Operator</option>
                            <option>4th Operator</option>
                            <option>5th Operator</option>
                            <option>6th Operator</option>
                            <option>7th Operator</option>
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="dataCardamount">Amount</label>
                        <div className="input-group">
                            <div className="input-group-prepend"> <span className="input-group-text">$</span> </div>
                            <a href="#" data-target="#view-plans" data-toggle="modal" className="view-plans-link">View Plans</a>
                            <input className="form-control" id="dataCardamount" placeholder="Enter Amount" required type="text" />
                        </div>
                    </div>
                    <a className="btn btn-primary btn-block" href="recharge-order-summary.html">Continue to Recharge</a>
                </form>
            </div>

        );
    }
}
