import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import { status, commissionMode, serviceTypes } from '../constants/commissionMode';
export default class VendorCheckList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            checkListItems: this.props.checkListItems,            
        };

    }

    handleChange = (e) => {
        
        const { name, value } = e.target;
        var theState = { ...this.state };
        theState.checkListItems.forEach(v => {
            if(v.id == value)            
            {
                v.isActive =  !v.isActive
            }
        });
        this.setState(theState);
    }

    componentDidUpdate = (prevprops) => {

        if (prevprops !== this.props) {
                this.setState({ checkListItems: this.props.checkListItems })            
        }
    }

    render() {
        var checkListItems;
        var checkListHtml = [];
        if (this.state.checkListItems == null) {
            checkListItems = [];
        } else {
            checkListItems = this.state.checkListItems;
        }

        checkListItems.map(x=>{
            checkListHtml.push(<div key={x.id}><Input value={x.id} key={x.id} checked={x.isActive} onChange={(e) => this.handleChange(e)} type="checkbox" />{' '} {x.name}</div>);
        })

        return (

            <Modal isOpen={this.state.checkListItems != null} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    Vendor's Checklist
                </ModalHeader>
                <ModalBody>
                    <Form>
                        {checkListHtml}
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => { this.props.onSave(this.state.checkListItems) }}>Save</Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}