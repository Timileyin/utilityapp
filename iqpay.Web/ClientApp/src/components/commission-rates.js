import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import EasyEdit from 'react-easy-edit';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import { status } from '../constants/commissionMode';
export default class CommissionRate extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            rates: this.props.rates
        };


    }

    handleCreateChange = (e) => {

    }



    componentDidUpdate = (prevprops) => {
        if (prevprops !== this.props) {            
            this.setState({ isOpen: this.props.isOpen, rates: this.props.rates })
        }
    }

    cancel =() => {
        
    }
    
    render() {
        var rates  = this.state.rates;
        let ratesTable = [];
        if(rates?.length > 0){
            rates.map(r=>{
                ratesTable.push(<tr>
                    <td>{r.service == null ? r.vendor : r.service}</td>      
                    <td>{r.commissionMode}</td>          
                    <td>
                        <EasyEdit
                            type="text"
                            onSave={
                                (value)=> this.props.updateServiceCommission(r.serviceId, r.vendorId, r.dealerId, value)
                            }
                            onCancel={this.cancel}
                            value= {r.commission == null ? "set commission" : r.commission }
                            saveButtonLabel="Save"
                            cancelButtonLabel="Cancel"
                            attributes={{ name: "awesome-input", id: 1}}
                            instructions="Please give a commission below yours"
                        />
                  </td>
                  <td>
                        <EasyEdit
                            type="text"
                            onSave={
                                (value)=> this.props.updateServiceCommissionCap(r.serviceId, r.vendorId, r.dealerId, value)
                            }
                            onCancel={this.cancel}
                            value= {r.cap == null ? "set commission cap" : r.cap }
                            saveButtonLabel="Save"
                            cancelButtonLabel="Cancel"
                            attributes={{ name: "awesome-input", id: 1}}
                            instructions="Please give a commission below yours"
                        />
                  </td>
                  </tr>)
            })
        }else{
            ratesTable = <tr><td>Services have not been created</td></tr>
        }

        return (

            <Modal size="lg" isOpen={this.state.rates != null} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    Commission Rates
                </ModalHeader>
                <ModalBody>
                    <Table>
                    <thead>
                        <tr>
                            <th>Service</th>
                            <th>Commission Type</th>
                            <th>Commission (%)</th>
                            <th>Cap Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        {ratesTable}
                    </tbody>
                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}