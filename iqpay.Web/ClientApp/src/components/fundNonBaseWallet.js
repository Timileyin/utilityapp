﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import { commissionMode } from '../constants/commissionMode';
export default class FundNonBaseWallet extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            baseWallet: [],
            walletInfo: [],
            fundAmount: 0.00
        };
    }

    fundAmountChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };

        if (name === 'fundAmount') {
            theState.fundAmount = value;
        }

        this.setState(theState);
    }

    
    componentDidUpdate = (prevprops) => {

        if (prevprops !== this.props) {

            this.setState({ isOpen: this.props.isOpen, walletInfo: this.props.fundWallet, baseWallet: this.props.baseWallet })
        }
    }


    render() {
        var walletInfo = this.state.walletInfo;
        var baseWallet = this.state.baseWallet;
        

        return (
            
            <Modal isOpen={this.state.isOpen} toggle={this.props.cancel} >
                <ModalHeader toggle={this.toggle}>
                    Fund {walletInfo.walletName} with {baseWallet.walletName}
                </ModalHeader>
                <ModalBody>
                    <Form>
                        <Row form>
                            <Col md={12}>                               
                                <div className="col-sm-12 col-lg-12 mb-4">
                                    <div className="card shadow-sm border-0">
                                        <ul className="list-group list-group-flush">
                                            <span className="list-group-item list-group-item-action" href="#">Base Wallet Information</span>
                                        </ul>
                                        <div className="card-body">

                                            Wallet name: {baseWallet.walletName}<br />
                                            Balance: {baseWallet.balance}<br />
                                            Description: {baseWallet.walletDescription}<br />
                                            Date Created: {baseWallet.dateCreated}
                                        </div>
                                        
                                    </div>
                                </div>

                                <div className="col-sm-12 col-lg-12 mb-4">
                                    <div className="card shadow-sm border-0">
                                        <ul className="list-group list-group-flush">
                                            <span className="list-group-item list-group-item-action" href="#">Fund Wallet Information</span>
                                        </ul>
                                        <div className="card-body">

                                            Wallet name: {walletInfo.walletName}<br />
                                            Balance: {walletInfo.balance}<br />
                                            Description: {walletInfo.walletDescription}<br />
                                            Date Created: {walletInfo.dateCreated}<br />
                                            Fund Amount:<br /> <Input value={this.state.fundAmount} type="text" onChange={this.fundAmountChange} name="fundAmount" bsSize="sm" id="fundAmount" placeholder="Fund amount" />
                                        </div>

                                    </div>
                                </div>                            
                            </Col>
                        </Row>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" disabled={this.state.fundAmount < 1} onClick={() => { this.props.onFundWallet(walletInfo.id, baseWallet.id, this.state.fundAmount) }}>Fund Wallet</Button>{' '}
                    <Button color="secondary" onClick={this.props.cancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}