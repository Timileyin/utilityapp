﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import {roles} from '../constants/rolesConstants';
import { connect } from 'react-redux';
import { userActions } from '../actions/userAction';
import {Input} from 'reactstrap';
import jQuery from 'jquery';
import { alertActions } from '../actions/alertActions';

class Profile extends React.Component {
    constructor(props) {
        super(props);
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));
        this.state = {
            isVisible: this.props.isVisible,
            pageTitle: 'My Profile',
            role: currentUser.role,
            profileInfo: {}
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if(this.props.authentication.profileInformation !== undefined){    
                if(this.props.authentication.profileInformation !== null){
                    this.setState({profileInfo: this.props.authentication.profileInformation});
                }                                            
            }            
        }
    }

    handleCreateChange = (e) => {

        const { name, value } = e.target;
        var theState = { ...this.state };

        if(name == 'firstName'){
            theState.profileInfo.firstName = value;
        }

        if(name == 'lastName'){
            theState.profileInfo.lastName = value;
        }

        if(name == 'phoneNumber'){
            theState.profileInfo.phoneNumber = value;
        }

        if(name == 'name'){
            theState.profileInfo.name = value;
        }


        if(name == 'address'){
            theState.profileInfo.address = value;
        }
        
        this.setState(theState);
    }

    updateUserInfo = (userInfo) => {
        const { dispatch } = this.props;
        dispatch(userActions.updateUserInfo(userInfo));
        //const{dispatch} = this.props;
        dispatch(alertActions.clear());
    }

    triggerFileUpload = () =>{
        jQuery("#inputProfileUpload").trigger("click");        
    }

    uploadProfileImage = (e) => {
        const {dispatch} = this.props;
        var file = e.target.files[0];
        console.log(file);
        var name = file.name;
        var nameSplits = name.split(".");
        var fileExt = nameSplits[nameSplits.length - 1]?.toLowerCase();
        if(fileExt == "jpg" || fileExt == "png" || fileExt == "jpeg"||fileExt == "gif"){
            var reader = new FileReader();
            reader.onload = function (ev) {
                jQuery('#profileImg').attr('src', ev.target.result);
                jQuery("#profileImg").removeAttr("hidden");
            }
            reader.readAsDataURL(file);                
            dispatch(userActions.uploadProfileImage(file));
        }else{
            dispatch(alertActions.error("Please upload a valid image","File upload failed"));
        }
        
    }

    componentDidMount() {
        //This method is called when the component is first added to the document
        const { dispatch } = this.props;
        dispatch(userActions.getUserInfo());        
    }

    render() {
        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="bg-light shadow-md rounded p-4"> 
                
                            <div className="row">      
                                <div className="col-lg-8">                          
                                    <h4 className="mb-4">Update your Profile</h4>
                                                                         
                                        <div className="form-group">
                                            <label for="fullName">Firstname</label>
                                            <Input type="text" value={this.state.profileInfo.firstName} onChange={this.handleCreateChange}  className="form-control" name="firstName" id="fullName" required placeholder="Firstname" />
                                        </div>
                                        <div className="form-group">
                                            <label for="fullName">Lastname</label>
                                            <Input type="text" value={this.state.profileInfo.lastName} onChange={this.handleCreateChange}  className="form-control" name="lastName" required placeholder="Lastname" />
                                        </div>
                                        <div className="form-group">
                                            <label for="fullName">Mobile Number</label>
                                            <Input type="text" value={this.state.profileInfo.phoneNumber} onChange={this.handleCreateChange}  className="form-control" name="mobileNumber" id="fullName" required placeholder="Mobile Number" />
                                        </div>
                                        <div className="form-group">
                                            <label for="fullName">Email</label>
                                            <Input type="text" value={this.state.profileInfo.email} disabled="true" className="form-control" name="email" id="email" required placeholder="Email address" />
                                        </div>
                                        <div className="form-group" hidden={this.state.role == roles.REGULAR_CUSTOMER || roles.SUPERDEALER }>
                                            <label for="fullName">Super Dealer<small>(for dealers only)</small></label>
                                            <Input type="text" value={this.state.profileInfo.superDealer} disabled="true" onChange={this.handleCreateChange}  className="form-control" name="superDealer" id="fullName" required placeholder="Super Dealer" />
                                        </div>
                                        
                                        <div className="form-group" hidden={this.state.role == roles.REGULAR_CUSTOMER}>
                                            <label for="fullName">Company Name<small>(for dealers only)</small></label>
                                            <Input type="text" value={this.state.profileInfo.name} onChange={this.handleCreateChange}  className="form-control" name="name" id="fullName" required placeholder="Company Name" />
                                        </div>
                                        <div className="form-group">
                                            <label for="fullName">Address</label>
                                            <Input type="textarea" value={this.state.profileInfo.address} onChange={this.handleCreateChange}  className="form-control" name="address" id="fullName" required placeholder="Address" />
                                        </div>                  
                                        <button className="btn btn-primary" onClick={()=>this.updateUserInfo(this.state.profileInfo)} type="submit">Update Now</button>
                                    
                                </div>
                                <div hidden={this.state.role == roles.REGULAR_CUSTOMER || true} className="col-lg-4 mt-4 mt-lg-0 ">
                                    <div className="bg-light-2 p-3">
                                        <p className="mb-2">Company Logo.</p>
                                        <img id="profileImg" height="100%" width="100%"  src={this.state.profileInfo.profilePicLoc} hidden={this.state.profileInfo.profilePicLoc == null || this.state.profileInfo.profilePicLoc== ""}/>
                                        <Input type="file" id="inputProfileUpload" onChange={ (e) => this.uploadProfileImage(e)} hidden={true}/>
                                        <button className="btn btn-info" onClick={this.triggerFileUpload}>Upload Logo </button>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                                
                </LoginMenu>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, authentication } = state;
    return {
        alert, authentication
    };
}

const connectedProfile = connect(mapStateToProps)(Profile);
export { connectedProfile as Profile };
