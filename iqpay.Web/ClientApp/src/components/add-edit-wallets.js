﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import { commissionMode } from '../constants/commissionMode';
export default class AddEditWallet extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            walletInfo: this.props.walletInfo
        };        
    }

    

    handleCreateChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        if (name === 'walletName') {
            theState.walletInfo.walletName = value;
        }

        if (name === 'walletDescription') {
            theState.walletInfo.walletDescription = value;
        }

        if (name === 'commissionMode') {
            theState.walletInfo.commissionMode = value;
        }
        this.setState(theState);
    }



    componentDidUpdate = (prevprops) => {
        
        if (prevprops !== this.props) {
            this.setState({ isOpen: this.props.isOpen, walletInfo: this.props.walletInfo })
        }
    }


    render() {
        var walletInfo;
        if (this.state.walletInfo == null) {
            walletInfo = {};
        } else {
            walletInfo = this.state.walletInfo;
        }
        
        return (

            <Modal isOpen={this.state.walletInfo != null} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    {walletInfo.id == undefined ? "Create Wallet" : "Edit " + walletInfo.walletName}
                </ModalHeader>
                <ModalBody>
                    <Form>
                        <Row form>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Wallet Name</Label>
                                    <Input value={walletInfo.walletName} type="text" onChange={this.handleCreateChange} name="walletName" bsSize="sm" id="walletName" placeholder="Wallet name" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="examplePassword">Commission Mode</Label>
                                    <Input value={walletInfo.commissionMode} onChange={this.handleCreateChange} name="commissionMode" id="commissionMode" type="select" bsSize="sm">
                                        <option  value="">--Please choose one--</option>
                                        {
                                            commissionMode.map((c) => 
                                                <option key={c.key} value={c.key}>{c.value}</option>
                                            )
                                        }
                                    </Input>
                                   
                                </FormGroup>
                            </Col>
                            <Col md={12}>
                                <FormGroup>
                                    <Label for="examplePassword">Description</Label>
                                    <Input type="textarea" name="walletDescription" id="walletDescription" onChange={this.handleCreateChange} value={walletInfo.walletDescription} bsSize="sm" placeholder="Description"></Input>
                                </FormGroup>
                            </Col>
                           
                        </Row>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => { this.props.onSave(this.state.walletInfo) }}>Save</Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}