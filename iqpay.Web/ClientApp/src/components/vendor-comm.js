import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import { status } from '../constants/commissionMode';
import jQuery from 'jquery';

export default class ManageVendorCommission extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            vendorInfo: this.props.vendorInfo
        };
    }

    componentDidUpdate = (prevprops) => {

        if (prevprops !== this.props) {
            this.setState({ isOpen: this.props.isOpen, vendorInfo: this.props.vendorInfo })
        }
    }

    handleCreateChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };

        if (name === 'commission') {
            theState.vendorInfo.commission = value;
        }

        if (name === 'commissionCap') {
            theState.vendorInfo.commissionCap = value;
        }        

        if (name === 'customerCommissionCap') {
            theState.vendorInfo.customerCommissionCap = value;
        }

        if (name === 'customerCommission') {
            theState.vendorInfo.customerCommission = value;
        }

        if (name === 'agentCommission') {
            theState.vendorInfo.agentCommission = value;
        }

        if (name === 'agentCommissionCap') {
            theState.vendorInfo.agentCommissionCap = value;
        }

        if (name === 'superDealerCommission') {
            theState.vendorInfo.superDealerCommission = value;
        }

        if (name === 'superDealerCommissionCap') {
            theState.vendorInfo.superDealerCommissionCap = value;
        }

        if (name === 'dealerCommissionCap') {
            theState.vendorInfo.dealerCommissionCap = value;
        }

        if (name === 'dealerCommission') {
            theState.vendorInfo.dealerCommission = value;
        }

        this.setState(theState);
    }


    render() {

        var vendorInfo;
        if (this.state.vendorInfo == null) {
            vendorInfo = {};
        } else {
            vendorInfo = this.state.vendorInfo;
        }
        return(
            <Modal isOpen={this.state.vendorInfo != null} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    {vendorInfo.id == undefined ? "Create Vendor" : "Edit " + vendorInfo.name}
                </ModalHeader>
                <ModalBody>
                <Form>
                        <Row form>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">IQPAY Comm</Label>
                                    <Input value={vendorInfo.commission} type="text" onChange={this.handleCreateChange} name="commission" bsSize="sm" id="balance" placeholder="Vendor balance" />
                                </FormGroup>
                            </Col>

                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">IQPAY Comm Cap</Label>
                                    <Input value={vendorInfo.commissionCap} type="text" onChange={this.handleCreateChange} name="commissionCap" bsSize="sm" id="balance" placeholder="Vendor balance" />
                                </FormGroup>
                            </Col>

                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Customer Comm</Label>
                                    <Input value={vendorInfo.customerCommission} type="text" onChange={this.handleCreateChange} name="customerCommission" bsSize="sm" id="balance" placeholder="Vendor balance" />
                                </FormGroup>
                            </Col>

                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Customer Comm Cap</Label>
                                    <Input value={vendorInfo.customerCommissionCap} type="text" onChange={this.handleCreateChange} name="customerCommissionCap" bsSize="sm" id="balance" placeholder="Vendor balance" />
                                </FormGroup>
                            </Col>

                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Super dealer Commission</Label>
                                    <Input value={vendorInfo.superDealerCommission} type="text" onChange={this.handleCreateChange} name="superDealerCommission" bsSize="sm" id="balance" placeholder="Vendor balance" />
                                </FormGroup>
                            </Col>

                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Super dealer Commission Cap</Label>
                                    <Input value={vendorInfo.superDealerCommissionCap} type="text" onChange={this.handleCreateChange} name="superDealerCommissionCap" bsSize="sm" id="balance" placeholder="Vendor balance" />
                                </FormGroup>
                            </Col>
                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Dealer Commission</Label>
                                    <Input value={vendorInfo.dealerCommission} type="text" onChange={this.handleCreateChange} name="dealerCommission" bsSize="sm" id="balance" placeholder="Vendor balance" />
                                </FormGroup>
                            </Col>

                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Dealer Commission Cap</Label>
                                    <Input value={vendorInfo.dealerCommissionCap} type="text" onChange={this.handleCreateChange} name="dealerCommissionCap" bsSize="sm" id="balance" placeholder="Vendor balance" />
                                </FormGroup>
                            </Col>

                            <Col md={6}>
                                <FormGroup>
                                    <Label for="exampleEmail">Agent Commission</Label>
                                    <Input value={vendorInfo.agentCommission} type="text" onChange={this.handleCreateChange} name="agentCommission" bsSize="sm" id="balance" placeholder="Vendor balance" />
                                </FormGroup>
                            </Col>

                            <Col md={6}>
                            <FormGroup>
                                <Label for="exampleEmail">Agent Commission Cap</Label>
                                <Input value={vendorInfo.agentCommissionCap} type="text" onChange={this.handleCreateChange} name="agentCommissionCap" bsSize="sm" id="balance" placeholder="Vendor balance" />
                            </FormGroup>
                        </Col>
                        </Row>
                </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => { this.props.onSave(this.state.vendorInfo) }}>Save</Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        );
    }
}