import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { Table, Row, Col, Button } from 'reactstrap';
import { connect } from 'react-redux';
import { transactionAction } from '../actions/transactionAction';
import {roles} from '../constants/rolesConstants';
import numeral from 'numeral';
import FilterModal from './common/filterModal';
import DataTable from 'react-data-table-component';
import {downloadCSV} from '../helpers/history';
import 'jquery/dist/jquery.min.js';
import "datatables.net-dt/js/dataTables.dataTables";
import "datatables.net-dt/css/jquery.dataTables.min.css";

import jQuery from 'jquery';



class Transactions extends React.Component {
    modalpopup = true;   
    currentUser = JSON.parse(localStorage.getItem("currentUser")); 
    columns = [
        {
            name: 'Date',
            selector: 'transactionDate',
            sortable: true,
        },
        {
            name: 'Vending Code',
            selector: 'vendingCode',
            sortable: true,
        },
        {
            name: 'Service',
            selector: 'service',
            sortable: true,
        },
        {
            name: 'Wallet Balance',
            selector: 'walletBalance',
            sortable: true,
        },        
        {
            name: 'Amount',
            sortable: true,
            cell: row => numeral(row.amount).format("0,00.00")
        },
        {
            name: 'Status',
            selector: 'status',
            sortable: false,
        },
        {
            name: 'Agent Comm.',
            sortable: false,
            cell: row => numeral(row.agentCommission).format("0,00.00")
        },
        {
            name: 'Dealer Comm.',
            sortable: false,
            cell: row => numeral(row.dealerCommission).format("0,00.00"),
            omit: this.currentUser.role == roles.AGENT
        },
        {
            name: 'Super Dealer Comm.',
            sortable: false,
            omit: this.currentUser.role != roles.SUPERDEALER && this.currentUser.role != roles.SUPERADMIN,
            cell: row => numeral(row.superDealerCommission).format("0,00.00")
        },
        {
            name: 'IQPAY Profit',
            sortable: false,
            omit: this.currentUser.role != roles.SUPERADMIN,
            cell: row => numeral(row.iqpayProfit).format("0,00.00")
        },
        {
            name: '',
            button: true,
            cell: row => <a href={"/slip/"+row.vendingCode} target="_blank" rel="noopener noreferrer">Details</a>,
        }      
    ]

    constructor(props) {
        super(props);
        
        this.state = {
            role: this.currentUser.role,
            pageTitle: 'Transaction Logs',
            logs: [],
            showFilterModal: false            
        };
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if(this.props.transaction.vendingLogs != undefined){
                this.setState({logs: this.props.transaction.vendingLogs });                                    
            }                           
        }
    }

    loadFilterModal = () =>{
        this.setState({showFilterModal: true})
    }

    onFilter = (startDate, endDate) => {
        const { dispatch } = this.props;
        dispatch(transactionAction.filterVendingLogs(startDate, endDate));
    }

    componentDidMount(prevprops) {
        //This method is called when the component is first added to the document        
        const { dispatch } = this.props;
        dispatch(transactionAction.fetchVendingLogs());        
    }

    render() {
        var transactions = this.state.logs;
        let transactionTable = [];
        var transactionValue = 0;

        transactions.map(t=>{
            if(t.status == "Completed"){
                transactionValue += t.amount;
            }
        });
        
        return (
            <div>
                <PageTitle title={this.state.pageTitle} />
                <LoginMenu activeMenu={this.state.pageTitle}>
                    <div className="col-lg-9">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-sm-12 mb-5">
                                <div className="card shadow-sm border-0">
                                    <div className="card-body">
                                        <Row>
                                            <Col>
                                            <span className="float-right"><i class="fa fa-filter"></i><FilterModal onFilter={(startDate, endDate) => this.onFilter(startDate,endDate)}/></span>
                                            <span className="text-right">Total Completed Transaction: {numeral(transactionValue).format("0,00.00")}</span>
                                                <DataTable
                                                    title="Transaction List"
                                                    columns={this.columns}
                                                    data={this.state.logs}
                                                    actions={<Button className="btn btn-sm" onClick={()=>downloadCSV(this.state.logs)}>Export</Button>}
                                                    pagination
                                                    paginationPerPage={30}
                                                />
                                            </Col>                                           
                                        </Row>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </LoginMenu>                
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, transaction } = state;

    return {
        alert, transaction
    };
}

const connectedTransactionsPage = connect(mapStateToProps)(Transactions);
export { connectedTransactionsPage as Transactions };