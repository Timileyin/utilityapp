﻿import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import { commissionMode } from '../constants/commissionMode';
export default class ApproveFundingRequest extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            requestInfo: this.props.requestInfo,
            disableButton: false,
        };
    }

    componentDidUpdate = (prevprops) => {

        if (prevprops !== this.props) {
            this.setState({ isOpen: this.props.isOpen, requestInfo: this.props.requestInfo })
        }
    }

    changeCreditAmount = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        if(name=="amountCreditted"){
            theState.requestInfo.amountCreditted = value;
        }
        
        this.state = theState;

    }


    render() {
        var requestInfo;
        if (this.state.requestInfo == null) {
            requestInfo = {};
        } else {
            requestInfo = this.state.requestInfo;
        }

        return (

            <Modal isOpen={this.state.requestInfo != null} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    Approve Manual Wallet Funding Request
                </ModalHeader>
                <ModalBody>
                    <Table>
                        <tr><th>Wallet Name</th><td>{requestInfo.walletName}</td></tr>
                        <tr><th>Wallet User</th><td>{requestInfo.walletUser}</td></tr>
                        <tr><th>Transaction Date</th><td>{requestInfo.transactionDate}</td></tr>
                        <tr><th>Date Requested</th><td>{requestInfo.dateCreated}</td></tr>
                        <tr><th>Amount</th><td>{requestInfo.amount}</td></tr>
                        <tr><th>Status</th><td>{requestInfo.status}</td></tr>
                        <tr><th>Credit Amount</th> {requestInfo.status !== "Creditted" ? <td><input type="text" onChange={(e)=>{this.changeCreditAmount(e)}} name="amountCreditted"/></td> : <td>{requestInfo.amountCreditted}</td> } </tr>
                    </Table>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" hidden={requestInfo.status == "Creditted"} disabled={this.state.disableButton} onClick={() => { this.setState({disableButton: true}); this.props.onApprove(this.state.requestInfo) }}>Approve Fund Request</Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}