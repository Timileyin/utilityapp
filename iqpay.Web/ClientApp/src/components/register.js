import React from 'react';
import { Link } from 'react-router-dom';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import {states} from '../constants/states';
import { Modal, Button, Nav, Tab, Row, Col } from 'react-bootstrap';
import { alertActions } from '../actions/alertActions';
import queryString from 'query-string';

class RegisterRequest extends React.Component {
    constructor(props) {
        super(props);

        this.props.dispatch(userActions.logout);
        const search = this.props.location.search;
        const params = queryString.parse(search);
        this.state = {
            login: {
                username: '',
                password: ''
            },
            register: {
                email: '',
                password: '',
                confirmPassword: '',
                firstname: '',
                surname: '',
                phonenumber:''
            },
            loginSubmitted: false,
            signupSubmitted: false,  
            dealerCode: params.a,
            superDealer: {}               
        };
    }

    handleLoginChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        if (name === 'password') {
            theState.login.password = value;
        }

        if (name === 'username') {
            theState.login.username = value;
        }
        this.setState(theState);
    }

    handleSignupChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        if (name === 'password') {
            theState.register.password = value;
        }

        if (name === 'firstname') {
            theState.register.firstname = value
        }

        if (name === 'surname') {
            theState.register.surname = value;
        }

        if (name === 'confirmPassword') {
            theState.register.confirmPassword = value;
        }

        if (name === 'email') {
            theState.register.email = value;
        }

        if (name === 'phonenumber') {
            theState.register.phonenumber = value
        }

        if (name === 'companyName') {
            theState.register.companyName = value
        }

        if (name === 'address') {
            theState.register.address = value
        }

        if (name === 'address2') {
            theState.register.address2 = value
        }

        if (name === 'state') {
            theState.register.state = value
        }

        if (name === 'lga') {
            theState.register.lga = value
        }

        if(name === 'photoID'){
            const { dispatch } = this.props;
            var file = e.target.files[0];
            if(file.size > (1024 * 1024 * 1)){
                dispatch(alertActions.error("File upload failed", "Please photo Id should not be more than 1MB"));
            }else{
                theState.register.photoID = file;
            }            
        }

        if (name === 'lga') {
            theState.register.lga = value
        }

        if (name === 'bvn') {
            theState.register.bvn = value
        }

        if (name === 'nin') {
            theState.register.nin = value
        }
        this.setState(theState);
    }

    handleLoginSubmit = (e) => {
        e.preventDefault();
        this.setState({ loginSubmitted: true });
        const { username, password } = this.state.login;
        const { dispatch } = this.props;
        if (username && password) {
            dispatch(userActions.login(username, password));
        }
    }

    handleSignSubmit = (e) => {
        e.preventDefault();
        this.setState({ signupSubmitted: true });
        const { email, firstname, surname, phonenumber, companyName , nin , bvn, address, address2, state, lga, photoID } = this.state.register;
        const { dispatch } = this.props;
        dispatch(userActions.agentRegistration(email, firstname, surname, phonenumber, companyName, nin, bvn, address, address2, state, lga, photoID, this.state.superDealer.id));        
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            this.setState({ isVisible: this.props.isVisible })
            if(this.props.authentication.profileInformation != undefined){
                this.state.superDealer = this.props.authentication.profileInformation
            }
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document
        if(this.state.dealerCode){
            const { dispatch } = this.props;
            dispatch(userActions.fetchUserByDealerCode(this.state.dealerCode))
        }        

    }

    render() {
        const { loggingIn, signinUp, alert } = this.props;
        const { login, register, loginSubmitted, registerSubmitted} = this.state;
        var lgas =  [];
        if(register.state) {
            lgas = states.find(x=>x.state.id == register.state).state.locals;  
        } 
        
        var warning = this.state.dealerCode ? <div className="alert alert-info">You are currently requesting to register under {this.state.superDealer.name}. If this is wrong please contact your intending super dealer for a correct personalized registration URL. Thank you</div>:""

        return (
            <div id="content">
                <section className="page-header page-header-text-light bg-secondary">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-md-8">
                                <h1>Partnership Registration Form</h1>
                            </div>
                            <div className="col-md-4">
                                <ul className="breadcrumb justify-content-start justify-content-md-end mb-0">
                                    <li><Link to="/">Home</Link></li>
                                    <li className="active">Login/Signup</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="container">
                   
                    <div className="col-md-6 bg-light shadow-md rounded mx-auto p-4">
                    <h5>Please fill the form below on successfully submission a contact from us would reach out to you and put you through how to complete your agent registration.</h5>
                        <form id="signupForm" onSubmit={this.handleSignSubmit}>
                            {warning}
                            <div className="form-group">
                                <label htmlFor="signupEmail">Email Address<span style={{"color": "red"}}>*</span></label>
                                <input type="email" name="email" value={register.email} onChange={this.handleSignupChange} className="form-control" data-bv-field="email" id="signupEmail" required placeholder="Email" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="firstName">Firstname<span style={{"color": "red"}}>*</span></label>
                                <input type="text" name="firstname" value={register.firstname} className="form-control" onChange={this.handleSignupChange} data-bv-field="number" id="signupFirstname" required placeholder="Firstname" />
                            </div>                                       
                            <div className="form-group">
                                <label htmlFor="lastName">Lastname<span style={{"color": "red"}}>*</span></label>
                                <input type="text" value={register.surname} className="form-control" onChange={this.handleSignupChange} data-bv-field="number" id="signupLastname" required placeholder="Lastname" name="surname" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="signupMobile">Mobile Number<span style={{"color": "red"}}>*</span></label>
                                <input type="text" className="form-control" value={register.phonenumber} onChange={this.handleSignupChange} name="phonenumber" id="signupMobile" required placeholder="Mobile Number" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="signupMobile">Company Name<span style={{"color": "red"}}>*</span></label>
                                <input type="text" className="form-control" value={register.companyName} onChange={this.handleSignupChange} name="companyName" id="signupMobile" required placeholder="Company Name"/>
                            </div>
                            <div className="form-group">
                                <label htmlFor="signupMobile">BVN<span style={{"color": "red"}}>*</span></label>
                                <input type="text" className="form-control" value={register.bvn} onChange={this.handleSignupChange} name="bvn" id="signupMobile" required placeholder="Bank Verification Number" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="signupMobile">NIN</label>
                                <input type="text" className="form-control" value={register.nin} onChange={this.handleSignupChange} name="nin" id="signupMobile" placeholder="National Identification Number" />
                            </div>

                            <div className="form-group">
                                <label htmlFor="signupMobile">NIN/Voter’s Card/Driver’s Lincense<span style={{"color": "red"}}>*</span> (File must be jpg or png and must not exceed 1MB)</label>
                                <input type="file" className="form-control" onChange={this.handleSignupChange} name="photoID" id="signupphotoID" placeholder="Upload Photo iD" />
                            </div>
                            
                            <div className="form-group">
                                <label htmlFor="signupMobile">State<span style={{"color": "red"}}>*</span></label>
                                <select className="form-control" onChange={this.handleSignupChange} name="state" id="signupState">
                                <option>--Please choose one--</option>
                                    {states.map(s=>(
                                        <option value={s.state.id}>{s.state.name}</option>
                                    ))}
                                </select>                                
                            </div>

                            <div className="form-group">
                                <label htmlFor="signupMobile">Local Government<span style={{"color": "red"}}>*</span></label>
                                <select className="form-control" onChange={this.handleSignupChange} name="lga" id="signupLga">
                                <option>--Please choose one--</option>
                                    {lgas.map(s=>(
                                        <option value={s.id}>{s.name}</option>
                                    ))}
                                </select>                                
                            </div>  

                            <div className="form-group">
                                <label htmlFor="signupMobile">Address(Line 1)<span style={{"color": "red"}}>*</span></label>
                                <textarea className="form-control" value={register.address} onChange={this.handleSignupChange} name="address" id="signupAddress" placeholder="Address" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="signupMobile">Address (Line 2)</label>
                                <textarea className="form-control" value={register.address2} onChange={this.handleSignupChange} name="address2" id="signupAddress2" placeholder="Address (Line 2)" />
                            </div>                         
                            
                            <button className="btn btn-primary btn-block" type="submit">Submit</button>
                        </form>
                    </div>    
                </div>
            </div>
        );
    }    
}

function mapStateToProps(state) {
    const { loggingIn, signinUp, alert, authentication } = state;
    //const { alert } = alert;
    return {
        loggingIn,
        signinUp,
        alert,
        authentication
    };
}

const connectedLoginPage = connect(mapStateToProps)(RegisterRequest);
export { connectedLoginPage as RegisterRequest }; 