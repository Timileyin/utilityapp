﻿import React from 'react';
import $ from 'jquery';
import { connect } from 'react-redux';
import { servicesActions } from '../actions/servicesAction';
import { vendorService } from '../services/vendorService';

window.$ = window.jQuery = $;
require('easy-responsive-tabs');

class ElectricityBills extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isVisible: this.props.isVisible,
            serviceTypeKey: this.props.serviceTypeKey,
            vendors: this.props.vendors,
            vendorServices: [],
            service: {},
            amount: null,
            customerNumber: null
        };
    }
    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        
        if (this.props !== prevprops) {

            this.setState({ isVisible: this.props.isVisible, serviceTypeKey: this.props.serviceTypeKey, vendors: this.props.vendors });
        }
    }

    handleCreateChange = (e) => {
        
        const { name, value } = e.target;
        var theState = { ...this.state };
        if (name === 'vendor') {
            var vendorServices = theState.vendors.find(x => x.id == value).serivces.filter(x => x.serviceTypeInt == this.state.serviceTypeKey);            
            theState.vendorServices = vendorServices;
        }

        if (name === 'customerNumber') {
            theState.customerNumber = value
        }
     

        if (name === 'service') {
            var selectedService = theState.vendorServices.find(x => x.id == value);

            if (selectedService.isFixedPrice) {
                theState.amount = selectedService.amount.toFixed(2);
            }
            theState.service = selectedService
        }

        if (name === 'amount') {
            theState.amount = value;
        }

        this.setState(theState);
    }

    initiateVendService = () => {
        
        if (window.confirm("Are you sure you want to proceed?")) {
            const { dispatch } = this.props;
            console.log(this.state);
            dispatch(servicesActions.initiateVending(this.state.service.id, this.state.customerNumber, this.state.amount));
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document
        

    }
    render() {
        var vendorsOptions = [];
        this.state.vendors.map(s => {
            vendorsOptions.push(<option style={{backgroundImage:"url(images/logo.png)"}} value={s.id} key={s.id}>{s.name}</option>);
        });

        var serviceOptions = [];
        this.state.vendorServices.map(s => {
            serviceOptions.push(<option key={s.id} value={s.id}>{s.name}</option>);
        });

        return (

            <div style={!this.state.isVisible ? { display: 'none' } : null}>
                <h2 className="text-6 mb-4">Pay your Electricity Bill</h2>
                
                    <div className="form-group">
                        <label htmlFor="electricityOperator">Your Electricity Provider</label>
                        <select name="vendor" onChange={this.handleCreateChange} className="custom-select" id="electricityOperator" required="">
                            <option value="">Select Your Operator </option>
                            {vendorsOptions}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="electricityyourState">Your Meter Type</label>
                        <select className="custom-select" onChange={this.handleCreateChange} name="service" id="service" required="">
                            <option value="">Select Your meter type</option>
                            {serviceOptions}
                        </select>
                    </div>
                    <div className="form-group">
                        <label htmlFor="serviceNumber">Your Meter Number/Account Number</label>
                        <input type="text" className="form-control" name="customerNumber" onChange={this.handleCreateChange} data-bv-field="number" id="serviceNumber" required placeholder="Enter Service Number" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="electricityAmount">Amount</label>
                        <div className="input-group">
                            <div className="input-group-prepend"> <span className="input-group-text">&#8358;</span> </div>
                            <input className="form-control" name="amount" value={this.state.amount} onChange={this.handleCreateChange} disabled={this.state.service.isFixedPrice} id="electricityAmount" placeholder="Enter Amount" required type="text" />
                        </div>
                    </div>
                    <button onClick={this.initiateVendService} className="btn btn-primary btn-block" type="submit">Continue</button>
                
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, serviceManagement } = state;

    return {
        alert, serviceManagement
    };
}

const connectedElectricityBillsPage = connect(mapStateToProps)(ElectricityBills);
export { connectedElectricityBillsPage as ElectricityBills };
