import React from 'react';
import { Link } from 'react-router-dom';
import {PageTitle} from './common/pagetitle';
import LoginMenu from './common/loginMenu';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import { Table, Button, Row, Col, Modal, ModalHeader, ModalBody, ModalFooter, Form, FormGroup, Label, Input } from 'reactstrap';
import { status } from '../constants/commissionMode';
export default class CreateEditAdmin extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            isOpen: this.props.isOpen,
            dealerInfo: this.props.dealerInfo,
            dealerType: this.props.dealerType
        };
    }

    handleCreateChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };

        if (name === 'name') {
            theState.dealerInfo.name = value;
        }

        if (name === 'address') {
            theState.dealerInfo.address = value;
        }

        if (name === 'email') {
            theState.dealerInfo.email = value;
        }

        if (name === 'phoneNumber') {
            theState.dealerInfo.phoneNumber = value;
        }

        if (name === 'address') {
            theState.dealerInfo.address = value;
        }
       
        this.setState(theState);
    }



    componentDidUpdate = (prevprops) => {

        if (prevprops !== this.props) {
            this.setState({ isOpen: this.props.isOpen, dealerInfo: this.props.dealerInfo })
        }
    }


    render() {
        var dealerInfo;
        if (this.state.dealerInfo == null) {
            dealerInfo = {};
        } else {
            dealerInfo = this.state.dealerInfo;
        }

        return (

            <Modal isOpen={this.state.dealerInfo != null} toggle={this.props.onCancel} >
                <ModalHeader toggle={this.toggle}>
                    {dealerInfo.id == undefined ? "Create "+this.state.dealerType : "Edit " + dealerInfo.name}
                </ModalHeader>
                <ModalBody>
                    <Form>
                        <Row form>
                            <Col md={12}>
                                <FormGroup>
                                    <Label for="exampleEmail">Firstname</Label>
                                    <Input value={dealerInfo.name} type="text" onChange={this.handleCreateChange} name="name" bsSize="sm" id="name" placeholder="Dealer name" />
                                </FormGroup>
                            </Col>

                            <Col md={12}>
                                <FormGroup>
                                    <Label for="exampleEmail">Firstname</Label>
                                    <Input value={dealerInfo.name} type="text" onChange={this.handleCreateChange} name="name" bsSize="sm" id="name" placeholder="Dealer name" />
                                </FormGroup>
                            </Col>                           

                            <Col md={12}>
                                <FormGroup>
                                    <Label for="exampleEmail">Email</Label>
                                    <Input value={dealerInfo.email} type="text" onChange={this.handleCreateChange} name="email" bsSize="sm" id="email" placeholder="Dealer email" />
                                </FormGroup>
                            </Col>

                            <Col md={12}>
                                <FormGroup>
                                    <Label for="exampleEmail">Phone number</Label>
                                    <Input value={dealerInfo.phoneNumber} type="text" onChange={this.handleCreateChange} name="phoneNumber" bsSize="sm" id="phoneNumber" placeholder="Dealer Phone number" />
                                </FormGroup>
                            </Col>  

                            <Col md={12}>
                                <FormGroup>
                                    <Label for="examplePassword">Address</Label>
                                    <Input type="textarea" name="address" id="address" onChange={this.handleCreateChange} value={dealerInfo.address} bsSize="sm" placeholder="Dealer address"></Input>
                                </FormGroup>
                            </Col>
                        </Row>
                    </Form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={() => { this.props.onSave(this.state.dealerInfo) }}>Save</Button>{' '}
                    <Button color="secondary" onClick={this.props.onCancel}>Cancel</Button>
                </ModalFooter>
            </Modal>
        )
    }
}