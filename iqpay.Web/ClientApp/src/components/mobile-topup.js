﻿import React from 'react';
import $ from 'jquery';
import { service } from '../services/service';
import { servicesActions } from '../actions/servicesAction';
import { connect } from 'react-redux';

window.$ = window.jQuery = $;
require('easy-responsive-tabs');

class MobileTopUp extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            isVisible: this.props.isVisible,
            serviceTypeKey: this.props.serviceTypeKey,
            vendors: this.props.vendors,
            vendorServices: [],
            service: {},
            amount: null,
            customerNumber: null
        };
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    handleCreateChange = (e) => {
        const { name, value } = e.target;
        var theState = { ...this.state };
        if (name === 'vendor') {
            var vendorServices = theState.vendors.find(x => x.id == value).serivces.filter(x => x.serviceTypeInt == this.state.serviceTypeKey);
            console.log(vendorServices);
            theState.vendorServices = vendorServices;
        }

        if (name === 'customerNumber') {
            theState.customerNumber = value
        }

        if (name === 'amount') {
            theState.amount = value;
        }

        if (name === 'service') {
            console.log(theState.vendorServices);
            var selectedService = theState.vendorServices.find(x => x.id == value);


            theState.service = selectedService
        }

        console.log(theState);
        this.setState(theState);
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        
        if (this.props !== prevprops) {

            this.setState({ isVisible: this.props.isVisible, serviceTypeKey: this.props.serviceTypeKey, vendors: this.props.vendors });
        }
        
    }

    recharge = () => {
        if (window.confirm("Are you sure you want to continue?")){
            const { dispatch } = this.props;
            dispatch(servicesActions.initiateVending(this.state.service.id, this.state.customerNumber, this.state.amount));
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document
        
    }
    render() {

        var vendorsOptions = [];
        this.state.vendors.map(s => {
            vendorsOptions.push(<option value={s.id}>{s.name}</option>);
        });

        var serviceOptions = [];
        this.state.vendorServices.map(s => {
            serviceOptions.push(<option value={s.id}>{s.name}</option>);
        });

        return (

            <div style={!this.state.isVisible ? { display: 'none' } : null}>
                <h2 className="text-6 mb-4">Mobile Recharge or Topup </h2>
                <form id="recharge-bill" method="post">
                    <div className="mb-3">

                        <div className="form-group">
                            <label htmlFor="operator">Select Your Network Provider</label>
                            <select className="custom-select" name="vendor" onChange={this.handleCreateChange} id="vendor" required="">
                                <option value="">Select Your Network Provider </option>
                                {vendorsOptions}
                            </select>
                        </div>

                        <div className="form-group">
                            <label htmlFor="operator">Service Type</label>
                            <select className="custom-select" onChange={this.handleCreateChange} name="service" id="service" required="">
                                <option value="">Select Your Network Provider </option>
                                {serviceOptions}
                            </select>
                        </div>

                        
                    </div>
                    <div className="form-group">
                        <label htmlFor="mobileNumber">Mobile Number</label>
                        <input type="text" className="form-control" onChange={this.handleCreateChange} data-bv-field="number" name="customerNumber" id="customerNumber" required placeholder="Enter Mobile Number" />
                    </div>
                    
                    <div className="form-group">
                        <label htmlFor="amount">Topup Amount</label>
                        <div className="input-group">
                            <div className="input-group-prepend"> <span className="input-group-text">&#8358;</span> </div>
                            <input className="form-control" onChange={this.handleCreateChange} name="amount" id="amount" placeholder="Enter Amount" required type="text" />
                        </div>
                    </div>
                    <button onClick={this.recharge} className="btn btn-primary btn-block" type="button">Continue</button>
                </form>
            </div>

        );
    }
}

function mapStateToProps(state) {

    const { alert, serviceManagement } = state;

    return {
        alert, serviceManagement
    };
}

const connectedMobileTopUp = connect(mapStateToProps)(MobileTopUp);
export { connectedMobileTopUp as MobileTopUp };
