﻿import React from 'react';
import queryString from 'query-string';
import { userActions } from '../actions/userAction';
import { connect } from 'react-redux';
import {PageTitle} from './common/pagetitle';

class ConfirmEmail extends React.Component {
    constructor(props) {
        super(props);

        this.state = queryString.parse(this.props.location.search);
        this.state.message = "";
        this.state.pageTitle = "Email Confirmation";        
    }

    toggle() {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }

    componentDidUpdate = (prevprops) => {
        // This method is called when the route parameters change
        if (prevprops !== this.props) {
            if(this.props.alert.type != undefined){
                if(this.props.alert.type == 'alert-danger'){
                    this.state.message = "Email confirmation has failed";
                    this.state.status = false;
                }else{
                    this.state.message = "Email confirmation has been completed successfully";
                    this.state.status = true;
                }
            }
            this.setState({ isVisible: this.props.isVisible })
        }
    }

    componentDidMount() {
        //This method is called when the component is first added to the document
        const { dispatch } = this.props;
        dispatch(userActions.confirmEmail(this.state.code, this.state.userId));
    }

    render() {
        return (
            
            <div class="container">
                <PageTitle title={this.state.pageTitle} />
                <div class="col-md-6 mt-4 mt-md-10 mx-auto">
                    <div class="bg-light shadow-md rounded p-4">
                        <h2 class="text-6 text-center">Email Confirmation</h2>
                        
                        <p class="text-3 text-center">{this.state.message}
                        {this.state.status ? <span><br /><a href='#'>Login</a></span> : <span><br /><a href='#'>Resend Confirmation Link</a></span> }
                        </p>                                               
                    </div>
                </div>
            </div>
        )
    }


}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedConfirmPage = connect(mapStateToProps)(ConfirmEmail);
export { connectedConfirmPage as ConfirmEmail }; 