﻿import { fundingRequestConstants } from '../constants/fundingRequestConstants';
import { fundingRequestService } from '../services/fundingRequestService';
import { alertActions } from './alertActions';
import { history } from '../helpers/history';

export const fundingRequestActions = {
    fetchFundingRequest,
    fetchFundingRequestInfo,
    approveFundingRequest,
    fetchFundingRequestHistory,
    filterFundingRequestHistory,
    filterFundingRequest

};

function approveFundingRequest(requestId, amount) {
    return dispatch => {

        fundingRequestService.approveFundingRequest(requestId, amount).then(resp => {
            dispatch(fetchFundingRequest());
            dispatch(alertActions.success(resp.message, "Approval completed successfully"));

        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Error occured while approving request", error.StatusCode));
        });
    }

    
    function failure(error) { return { type: fundingRequestConstants.FETCH_FUND_REQUEST_INFO_FAILURE, error } }
}

function fetchFundingRequestHistory(){
    return dispatch => {
        dispatch(request);

        fundingRequestService.fetchFundingRequestHistory().then(resp => {
            dispatch(alertActions.clear());
            dispatch(success(resp));
            dispatch(alertActions.success(resp.message, "Requests found"))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Error occured while fetching request", error.StatusCode));
        });
    }

    function request() { return { type: fundingRequestConstants.FETCH_FUND_REQUEST } }
    function success(resp) { return { type: fundingRequestConstants.FETCH_FUND_REQUEST_SUCCESS, resp } }
    function failure(error) { return { type: fundingRequestConstants.FETCH_FUND_REQUEST_FAILURE, error } }
}

function filterFundingRequestHistory(startDate, endDate){
    return dispatch => {
        dispatch(request);

        fundingRequestService.filterFundingRequestHistory(startDate, endDate).then(resp => {
            dispatch(alertActions.clear());
            dispatch(success(resp));
            dispatch(alertActions.success(resp.message, "Requests found"))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Error occured while fetching request", error.StatusCode));
        });
    }

    function request() { return { type: fundingRequestConstants.FETCH_FUND_REQUEST } }
    function success(resp) { return { type: fundingRequestConstants.FETCH_FUND_REQUEST_SUCCESS, resp } }
    function failure(error) { return { type: fundingRequestConstants.FETCH_FUND_REQUEST_FAILURE, error } }
}

function fetchFundingRequestInfo(requestId) {
    return dispatch => {
        dispatch(request);

        fundingRequestService.fetchFundingRequestInfo(requestId).then(resp => {
            dispatch(success(resp.content));
            
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Error occured while fetching request", error.StatusCode));
        });
    }

    function request() { return { type: fundingRequestConstants.FETCH_FUND_REQUEST_INFO } }
    function success(fundingInfo) { return { type: fundingRequestConstants.FETCH_FUND_REQUEST_INFO_SUCCESS, fundingInfo } }
    function failure(error) { return { type: fundingRequestConstants.FETCH_FUND_REQUEST_INFO_FAILURE, error } }
}

function filterFundingRequest(startDate, endDate){
    return dispatch => {
        dispatch(request);

        fundingRequestService.fetchFundingRequest(startDate, endDate).then(resp => {
            dispatch(alertActions.clear());
            dispatch(success(resp));
            dispatch(alertActions.success(resp.message, "Requests found"))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Error occured while fetching request", error.StatusCode));
        });
    }

    function request() { return { type: fundingRequestConstants.FETCH_FUND_REQUEST } }
    function success(resp) { return { type: fundingRequestConstants.FETCH_FUND_REQUEST_SUCCESS, resp } }
    function failure(error) { return { type: fundingRequestConstants.FETCH_FUND_REQUEST_FAILURE, error } }
}


function fetchFundingRequest() {
    return dispatch => {
        dispatch(request);

        fundingRequestService.fetchFundingRequest().then(resp => {
            dispatch(alertActions.clear());
            dispatch(success(resp));
            dispatch(alertActions.success(resp.message, "Requests found"))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Error occured while fetching request", error.StatusCode));
        });
    }

    function request() { return { type: fundingRequestConstants.FETCH_FUND_REQUEST } }
    function success(resp) { return { type: fundingRequestConstants.FETCH_FUND_REQUEST_SUCCESS, resp } }
    function failure(error) { return { type: fundingRequestConstants.FETCH_FUND_REQUEST_FAILURE, error } }
}