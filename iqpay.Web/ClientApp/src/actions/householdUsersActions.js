import { householdUserService } from '../services/householdUserService.js';
import { alertActions } from './alertActions';
import { history } from '../helpers/history';
import { householdConstants } from '../constants/householdConstants';

export const householdActions = {
    saveDealer,
    fetchDealers,
    fetchDealerDetails,
    toggleActivate
}

function saveDealer(dealer) {
    return dispatch => {

        if (dealer.id == undefined) {
            dealer.password = "Crystal123@@";
            dealer.confirmPassword = "Crystal123@@";
            householdUserService.createDealer(dealer).then(resp => {

                dispatch(alertActions.success(resp.message, "User saved"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Save user failed", error.StatusCode));
            });
        } else {
            householdUserService.updateDealer(dealer).then(resp => {

                dispatch(alertActions.success(resp.message, "User saved"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Update user failed", error.StatusCode));
            });
        }

    }

    function request() { return { type: householdConstants.SAVE_DEALER_REQUEST } }
    function success(resp) { return { type: householdConstants.SAVE_DEALER_SUCCESS, resp } }
    function failure(error) { return { type: householdConstants.SAVE_DEALER_FAILURE, error } }
}

function toggleActivate(id, status) {

    return dispatch => {
        if (status) {
            householdUserService.activate(id).then(resp => {
                dispatch(alertActions.success(resp.message, "User has been activated"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "User activation failed", error.StatusCode));
            })
        }
        else {
            householdUserService.deactivate(id).then(resp => {
                dispatch(alertActions.success(resp.message, "User has been deactivated"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "User deactivation failed", error.StatusCode));
            })
        }
    }

    function success(resp) { return { type: householdConstants.SAVE_DEALER_SUCCESS, resp } }
    function failure(error) { return { type: householdConstants.SAVE_DEALER_FAILURE, error } }
}

function fetchDealers() {
    return dispatch => {
        householdUserService.fetchDealers().then(resp => {

            dispatch(success(resp))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetch users failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: householdConstants.FETCH_DEALERS_SUCCESS, resp } }
    function failure(error) { return { type: householdConstants.FETCH_DEALERS_FAILURE, error } }
}

function fetchDealerDetails(id) {
    return dispatch => {
        householdUserService.fetchDealerDetails(id).then(resp => {

            dispatch(success(resp))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetch dealers details failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: householdConstants.FETCH_DEALERS_INFO_SUCCESS, resp } }
    function failure(error) { return { type: householdConstants.FETCH_DEALERS_FAILURE, error } }
}

