﻿import { dealerService } from '../services/dealerService';
import { alertActions } from './alertActions';
import { history } from '../helpers/history';
import { dealerConstants } from '../constants/dealerConstants';

export const dealersActions = {
    saveDealer,
    fetchDealers,
    fetchDealerDetails,
    toggleActivate,
    fetchCommissionRate,
    updateDealerCommission,
    updateDealerCommissionCap,
    resetDealerCommissionRate,
    resetserviceChecklist,
    fetchServiceChecklist,
    updateCheckListItems
}

function updateCheckListItems(dealerId, checkListItems)
{
    var activeVendors = [];
    checkListItems.map(x=>{
        if(x.isActive){
            activeVendors.push(x.id);
        }        
    });

    return dispatch => {

        dealerService.updateCheckListItems(dealerId, activeVendors).then(resp=>{
            dispatch(success(resp));
            dispatch(alertActions.success(resp.message,"Checklist has been updated successfully"));
        }, error => {
            dispatch(alertActions.error(error.message, "Error occured while updating dealer checklist"));
        });
    }
    
    function success(resp) { return { type: dealerConstants.FETCH_DEALER_CHECKLIST, resp } }  
}

function fetchServiceChecklist(dealerId)
{
    return dispatch =>{
        dealerService.fetchServiceChecklist(dealerId).then(resp=>{
            dispatch(success(resp));
        },error =>{
            dispatch(failure())
            dispatch(alertActions.error(error.message, "Error occurred while updating dealer commission"));
        });        
    } 

    function success(resp) { return { type: dealerConstants.FETCH_DEALER_CHECKLIST, resp } }  
    function failure() {return {type: dealerConstants.FETCH_DEALER_CHECKLIST_FAILED}}  
}

function resetDealerCommissionRate(){
    return dispatch =>{
        dispatch(resetDealerCommission());
    }

    function resetDealerCommission() { return { type: dealerConstants.RESET_COMMISSION_RATE } }
}

function resetserviceChecklist(){
    return dispatch =>{
        dispatch(resetserviceChecklist());
    }

    function resetserviceChecklist() { return { type: dealerConstants.RESET_SERVICE_CHECKLIST } }
}

function updateDealerCommission(serviceId, vendorId, dealerId, rate){
    return dispatch =>{
        dealerService.updateCommission(serviceId,vendorId, dealerId, rate).then(resp=>{
            dispatch(alertActions.success(resp.message,"Dealer commission updated"))
        },error =>{
            dispatch(alertActions.error(error.message, "Error occurred while updating dealer commission"));
        });
    }
}

function updateDealerCommissionCap(serviceId, vendorId, dealerId, cap){
    return dispatch =>{
        dealerService.updateDealerCommissionCap(serviceId, vendorId, dealerId, cap).then(resp=>{
            dispatch(alertActions.success(resp.message,"Dealer commission cap updated"))
        },error =>{
            dispatch(alertActions.error(error.message, "Error occurred while updating dealer commission"));
        });
    }    
}

function saveDealer(dealer){
    return dispatch => {

        if (dealer.id == undefined) {
            dealer.password = "Crystal123@@";
            dealer.confirmPassword = "Crystal123@@";
            dealerService.createDealer(dealer).then(resp => {

                dispatch(alertActions.success(resp.message, "Dealer saved"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Save dealer failed", error.StatusCode));
            });
        } else {
            dealerService.updateDealer(dealer).then(resp => {

                dispatch(alertActions.success(resp.message, "Dealer saved"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Update dealer failed", error.StatusCode));
            });
        }
    }

    function request() { return { type: dealerConstants.SAVE_DEALER_REQUEST } }
    function success(resp) { return { type: dealerConstants.SAVE_DEALER_SUCCESS, resp } }
    function failure(error) { return { type: dealerConstants.SAVE_DEALER_FAILURE, error } }
}

function fetchCommissionRate(dealerId){
    return dispatch =>{
        dealerService.fetchCommissionRates(dealerId).then(resp =>{
            dispatch(success(resp));
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Error occurred while fetching rates", error.statusCode))
        });
    }

    function success(resp) { return { type: dealerConstants.FETCH_COMMISSION_RATES_SUCCESS, resp } }
    function failure(error) { return { type: dealerConstants.SAVE_DEALER_FAILURE, error } }
}

function toggleActivate(id, status) {
    
    return dispatch => {
        if (status) {
            dealerService.activate(id).then(resp => {
                dispatch(alertActions.success(resp.message, "Dealer has been activated"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Dealer activation failed", error.statusCode));
            })
        }
        else {
            dealerService.deactivate(id).then(resp => {
                dispatch(alertActions.success(resp.message, "Dealer has been deactivated"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Dealer deactivation failed", error.StatusCode));
            })
        }
    }

    function success(resp) { return { type: dealerConstants.SAVE_DEALER_SUCCESS, resp } }
    function failure(error) { return { type: dealerConstants.SAVE_DEALER_FAILURE, error } }
}

function fetchDealers(){
    return dispatch => {
        dealerService.fetchDealers().then(resp => {

            dispatch(success(resp))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetch dealers failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: dealerConstants.FETCH_DEALERS_SUCCESS, resp } }
    function failure(error) { return { type: dealerConstants.FETCH_DEALERS_FAILURE, error } }    
}

function fetchDealerDetails(id){
    return dispatch => {
        dealerService.fetchDealerDetails(id).then(resp => {

            dispatch(success(resp))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetch dealers details failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: dealerConstants.FETCH_DEALERS_INFO_SUCCESS, resp } }
    function failure(error) { return { type: dealerConstants.FETCH_DEALERS_FAILURE, error } }
}

