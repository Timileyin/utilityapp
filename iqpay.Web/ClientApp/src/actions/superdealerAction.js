﻿import { superdealerService } from '../services/superdealerService';
import { alertActions } from './alertActions';
import { history } from '../helpers/history';
import { superdealerConstants } from '../constants/superdealerConstants';

export const superdealerActions = {
    saveDealer,
    fetchDealers,
    fetchDealerDetails,
    toggleActivate
}

function saveDealer(dealer) {
    return dispatch => {

        if (dealer.id == undefined) {
            dealer.password = "Crystal123@@";
            dealer.confirmPassword = "Crystal123@@";
            superdealerService.createDealer(dealer).then(resp => {

                dispatch(alertActions.success(resp.message, "Dealer saved"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Save dealer failed", error.StatusCode));
            });
        } else {
            superdealerService.updateDealer(dealer).then(resp => {

                dispatch(alertActions.success(resp.message, "Dealer saved"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Update dealer failed", error.StatusCode));
            });
        }

    }

    function request() { return { type: superdealerConstants.SAVE_DEALER_REQUEST } }
    function success(resp) { return { type: superdealerConstants.SAVE_DEALER_SUCCESS, resp } }
    function failure(error) { return { type: superdealerConstants.SAVE_DEALER_FAILURE, error } }
}

function toggleActivate(id, status) {

    return dispatch => {
        if (status) {
            superdealerService.activate(id).then(resp => {
                dispatch(alertActions.success(resp.message, "Dealer has been activated"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Dealer activation failed", error.StatusCode));
            })
        }
        else {
            superdealerService.deactivate(id).then(resp => {
                dispatch(alertActions.success(resp.message, "Dealer has been deactivated"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Dealer deactivation failed", error.StatusCode));
            })
        }
    }

    function success(resp) { return { type: superdealerConstants.SAVE_DEALER_SUCCESS, resp } }
    function failure(error) { return { type: superdealerConstants.SAVE_DEALER_FAILURE, error } }
}

function fetchDealers() {
    return dispatch => {
        superdealerService.fetchDealers().then(resp => {

            dispatch(success(resp))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetch dealers failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: superdealerConstants.FETCH_DEALERS_SUCCESS, resp } }
    function failure(error) { return { type: superdealerConstants.FETCH_DEALERS_FAILURE, error } }
}

function fetchDealerDetails(id) {
    return dispatch => {
        superdealerService.fetchDealerDetails(id).then(resp => {

            dispatch(success(resp))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetch dealers details failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: superdealerConstants.FETCH_DEALERS_INFO_SUCCESS, resp } }
    function failure(error) { return { type: superdealerConstants.FETCH_DEALERS_FAILURE, error } }
}

