﻿import { userConstants } from '../constants/userConstants';
import { userService } from '../services/userService';
import { alertActions } from './alertActions';
import { history } from '../helpers/history';
import React from 'react';
import { Redirect } from 'react-router-dom';

export const userActions = {
    login,
    logout,
    getAll,
    signup,
    fetchWallets,
    fetchWalletInfo,
    saveWallet,
    fundIntraWallet,
    maunalFund,
    initiateAutoFunding,
    confirmEmail,
    moveFundsCommissionToBase,
    getUserInfo,
    updateUserInfo,
    uploadProfileImage,
    fetchDashboardData,
    resetPassword,
    forgotPassword,
    fundBaseWalletWithComm,
    fetchReservedAccount,
    generateAccountNumber,
    fetchWalletBalances,
    fetchAdministrators,
    saveAdminInfo,
    getAdminInfo,
    agentRegistration,
    fetchPartnershipRequests,
    viewRequestInfo,
    registerDealer,
    savePermission,fetchUserByDealerCode
};

function savePermission(id, permissions){
    return dispatch => {
        userService.savePermission(id, permissions)
            .then(resp => {
                dispatch(alertActions.success(resp.message, "Permission have been saves successfully"));
            }, error => {
                dispatch(alertActions.error(error.message, "Permissions failed to save"))
        });
    }

    function success() { return { type: userConstants.SAVE_PERMISSION_FAILED}}
}

function registerDealer(id, dealerType){
    return dispatch => {
        userService.registerPartnerDealer(id, dealerType)
                .then(resp => {
                    dispatch(success(resp.content))
                    dispatch(alertActions.success(resp.message, "User has been created successfully"));
                }, error => {
                    dispatch(alertActions.error(error.message, "Error occured while fetching administrators list", error.statusCode));
                });
    }

    function success(details) { return { type: userConstants.REGISTER_PARTNER_DEALER} }
    //function failure(error) { return { type: userConstants.REGISTER_DEALER } }
}

function fetchAdministrators()
{
    return dispatch => {
        userService.fetchAdministrators()
                .then(resp => {
                    dispatch(success(resp.content))
                }, error => {
                    dispatch(alertActions.error(error.message, "Error occured while fetching administrators list", error.statusCode));
                });
    }

    function success(details) { return { type: userConstants.FETCH_ADMINISTRATORS, details} }
    function failure(error) { return { type: userConstants.FETCH_ADMINISTRATORS_FAILED, error } }
}

function saveAdminInfo(admin){
    return dispatch => {
        admin.password = "Crystal123@@";
        admin.confirmPassword = "Crystal123@@";
        if (admin.id == undefined) {            
            userService.createAdmin(admin).then(resp => {

                dispatch(alertActions.success(resp.message, "Administrator saved"));
                dispatch(success(resp))
                dispatch(fetchAdministrators());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Save administrator failed", error.StatusCode));
            });
        } else {
            userService.updateAdmin(admin).then(resp => {

                dispatch(alertActions.success(resp.message, "Adinistrator saved"));
                dispatch(success(resp))
                dispatch(fetchAdministrators());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Update administrator failed", error.StatusCode));
            });
        }
    }

    function success(details) { return { type: userConstants.SAVE_ADMINISTRATOR, details} }
    function failure(error) { return { type: userConstants.SAVE_ADMINISTRATOR_FAILED, error } }
}

function generateAccountNumber(){
    return dispatch => {
        userService.generateAccountNumber()
            .then(resp => {
                dispatch(success(resp.content))
            }, error => {
                dispatch(alertActions.error(error.message, "Error occured while fetching reserved account", error.statusCode));
            });
    }

    function success(details) { return { type: userConstants.FETCH_RESERVED_ACCOUNT, details} }
    function failure(error) { return { type: userConstants.FETCH_RESERVED_ACCOUNT_FAILURE, error } }
}

function fetchWalletBalances()
{
    return dispatch => {
        userService.fetchWalletBalances()
            .then(resp => {
                dispatch(success(resp.content))
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Error occured while fetching balance report", error.statusCode));
        })
    }

    function success(balances) { return { type: userConstants.FETCH_WALLET_BALANCES, balances} }
    function failure(error) { return { type: userConstants.FETCH_WALLET_BALANCE_FAILURE, error } }
}

function fetchReservedAccount()
{
    return dispatch => {
        userService.fetchReservedAccount()
            .then(resp => {
                dispatch(success(resp.content))
            }, error => {
                dispatch(alertActions.error(error.message, "Error occured while fetching reserved account", error.statusCode));
            });
    }

    function success(details) { return { type: userConstants.FETCH_RESERVED_ACCOUNT, details} }
    function failure(error) { return { type: userConstants.FETCH_RESERVED_ACCOUNT_FAILURE, error } }
}

function forgotPassword(email)
{
    return dispatch => {                      
        userService.forgotPassword(email)
            .then(resp => {
                //dispatch(success(resp));
                dispatch(alertActions.success(resp.message, "A mail and SMS has been sent to you containing reset password instructions."));
            }, error => {
                //dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Reset password failed", error.statusCode));
            });
    }
}

function resetPassword(userId, code, newPassword, confirmNewPassword){
    return dispatch=>{
        userService.resetPassword(userId, code, newPassword, confirmNewPassword).then(resp=>{
            dispatch(alertActions.success(resp.message, "Password reset has been completed successfully"));
            history.push("/login");
        }, error=>{
            dispatch(alertActions.error(error.message, "Error occured while resetting password", error.message));
        });
    }
}

function viewRequestInfo(id)
{
    return dispatch => {
        userService.fetchRequestInfo(id)
            .then(
                resp=>{
                    dispatch(success(resp.content));
                }, error => {
                    dispatch(failure());
                    dispatch(alertActions.error(error.message, "Error occured while fetching partnership request details"));
                }
            )
    }

    function success(requestInfo) { return { type: userConstants.PARTNERSHIP_REQUEST_INFO_SUCCESS, requestInfo} }
    function failure(error) { return { type: userConstants.PARTNERSHIP_REQUEST_INFO_FAILURE, error } }
}

function fetchPartnershipRequests()
{
    return dispatch => {
        userService.fetchPartnershipRequests()
            .then(
                resp=>{
                    dispatch(success(resp.content));
                }, error => {
                    dispatch(failure());
                    dispatch(alertActions.error(error.message, "Error occured while fetching partnership request"));
                }
            )
    }

    function success(partnerRequests) { return { type: userConstants.PARTNERSHIP_REQUEST_SUCCESS, partnerRequests} }
    function failure(error) { return { type: userConstants.PARTNERSHIP_REQUEST_FAILURE, error } }
}

function agentRegistration(email, firstname, surname, phonenumber, companyName, nin, bvn, address, addressLine2, state, lga, photoID, superDealerId){
    return dispatch => {
        userService.agentRegistration(email, firstname, surname, phonenumber, companyName,bvn,nin,address, addressLine2, state, lga,photoID, superDealerId)
            .then(
                resp => {
                    
                    dispatch(alertActions.success(resp.message,"Registration request submitted"));
                    return <Redirect to="/login"/>;
                },
                error => {
                    dispatch(alertActions.error(error.message, "Registration request has failed", error.statusCode));
                }
            );
    };

    function request(email) { return { type: userConstants.SIGNUP_REQUEST, email } }
    function success(resp) { return { type: userConstants.SIGNUP_SUCCESS, resp} }
    function failure(error) { return { type: userConstants.SIGNUP_FAILURE, error } }
}

function signup(email, firstname, surname, confirmPassword, password, phonenumber) {
    return dispatch => {
        dispatch(request({ email }))

        userService.signup(email, firstname, surname, confirmPassword, password, phonenumber)
            .then(
                resp => {
                    dispatch(success(resp));
                    dispatch(alertActions.success(resp,"Registration Completed"));
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error.message, "Registration Failed", error.statusCode));
                }
            );
    };

    function request(email) { return { type: userConstants.SIGNUP_REQUEST, email } }
    function success(resp) { return { type: userConstants.SIGNUP_SUCCESS, resp} }
    function failure(error) { return { type: userConstants.SIGNUP_FAILURE, error } }
}

function uploadProfileImage(file){
    return dispatch => {
        userService.updateProfileImage(file).then(
            resp=> {
                dispatch(alertActions.success(resp.message, "Company Logo has been updated successfully"));
            },
            error =>{
                dispatch(alertActions.error(error.message, "Company logo upload failed", error.statusCode));
            }
        )
    }
}

function fetchDashboardData(){
    return dispatch => {
        userService.fetchDashboardData().then(
            resp => {
                dispatch(success(resp.content));
            }, error => {
                dispatch(alertActions.error(error.message, "Dashboard data successfully returned"));
            }
        )
    }
    function success(dashboard) { return { type: userConstants.FETCH_DASHBOARD_DATA, dashboard } }
}

function confirmEmail(code, userId)
{
    return dispatch => {
        userService.confirmEmail(code, userId).then(
            resp => {
                dispatch(success(resp));
                dispatch(alertActions.success(resp.message, "Email confirmed successfully"));
            },
            error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Email confirmation failed", error.statusCode));
            }
        );
    };

    function success(resp) { return { type: userConstants.SIGNUP_SUCCESS, resp } }
    function failure(error) { return { type: userConstants.SIGNUP_FAILURE, error } }
}

function maunalFund(fundingInfo) {
    return dispatch => {
        userService.manualFund(fundingInfo)
            .then(
                resp => {
                    dispatch(success(resp));
                    dispatch(alertActions.success(resp.message, "Wallet Funding Request Completed"));
                }
                , error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error.message, "Wallet Funding Failed", error.statusCode));
                }
            );
    };

    function success(resp) { return { type: userConstants.SIGNUP_SUCCESS, resp } }
    function failure(error) { return { type: userConstants.SIGNUP_FAILURE, error } }
}

function moveFundsCommissionToBase(commissionWalletId, baseWalletId){
    return dispatch =>{
        userService.moveFundsCommissionToBase(commissionWalletId, baseWalletId).then(
            resp=>{
                dispatch(success(resp));
                dispatch(alertActions.success(resp, "Funds have been transfered successfully"));
            },
            error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Wallet Funding Failed", error.statusCode));
            }
        )
    }

    function success(resp) { return { type: userConstants.SIGNUP_SUCCESS, resp } }
    function failure(error) { return { type: userConstants.SIGNUP_FAILURE, error } }
}


function initiateAutoFunding(fundingInfo) {
    return dispatch => {
        userService.initiateAutoFunding(fundingInfo)
            .then(
                resp => {
                    dispatch(success(resp));                    
                    dispatch(alertActions.success(resp.message, "Wallet Funding Request Completed"));
                    history.push("/funding-summary/" + resp.content.txnRef);
                }
                ,error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error.message, "Wallet Funding Failed", error.statusCode));
                }
            );
    };

    function success(resp) { return { type: userConstants.SIGNUP_SUCCESS, resp } }
    function failure(error) { return { type: userConstants.SIGNUP_FAILURE, error } }
}

function login(username, password) {
    return dispatch => {
        dispatch(request({ username }));

        userService.login(username, password)
            .then(
                resp => {
                    dispatch(alertActions.clear());
                    dispatch(success(resp.content));
                    history.push("/");
                    //dispatch(alertActions.success("Login Successful", "Login successful", 200));
                },
                error => {
                    dispatch(alertActions.clear());
                    dispatch(failure(error));
                    dispatch(alertActions.error(error.message, "Login failed", error.statusCode));
                }
            );
    };

    function request(user) { return { type: userConstants.LOGIN_REQUEST, user } }
    function success(user) { return { type: userConstants.LOGIN_SUCCESS, user } }
    function failure(error) { return { type: userConstants.LOGIN_FAILURE, error } }
    //function signupSuccess(user) { return { type: userConstants.S } }
}

function fetchWallets() {
    return dispatch => {
        userService.fetchWallets().then(
            resp => {
                
                dispatch(alertActions.clear());
                dispatch(success(resp.content));            
            },
            error => {
                dispatch(alertActions.clear());
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Failed to fetch wallets", error.statusCode));
            }
        );
    };

    function success(wallets) { return { type: userConstants.FETCH_WALLET_SUCCESS, wallets } }
    function failure(error) { return { type: userConstants.FETCH_WALLET_ERROR, error } }

}

function fundBaseWalletWithComm(walletId, baseWalletId){

    return dispatch => {
        userService.fundBaseWalletWithComm(walletId, baseWalletId).then(
            resp => {
                dispatch(alertActions.clear());
                dispatch(alertActions.success(resp.message, "Wallet Funding"))
                dispatch(userActions.fetchWallets());
            },
            error => {
                dispatch(alertActions.clear());
                dispatch(alertActions.error(error.message, "Wallet funding failed", error.statusCode));
            }
        )
    }
}

function fundIntraWallet(walletId, baseWalletId, amount) {

    return dispatch => {
        userService.fundIntraWallet(walletId, baseWalletId, amount).then(
            resp => {
                dispatch(alertActions.clear());
                dispatch(alertActions.success(resp.message, "Wallet Funding"))
                dispatch(userActions.fetchWallets());
            },
            error => {
                dispatch(alertActions.clear());
                dispatch(failure(error))
                dispatch(alertActions.error(error.message, "Wallet funding failed", error.statusCode));
            }
        )
    }

    function success(walletInfo) { return { type: userConstants.FETCH_WALLET_UPDATE_SUCCESS, walletInfo } }
    function failure(error) { return { type: userConstants.WALLET_UPDATE_FAILED, error } }

}

function saveWallet(walletInfo) {
    return dispatch => {
        if (walletInfo.id == undefined) {
            userService.createWallet(walletInfo).then(
                resp => {
                    dispatch(alertActions.clear());
                    dispatch(alertActions.success(resp.message, "Wallet created"));
                    dispatch(userActions.fetchWallets());
                },
                error => {
                    dispatch(alertActions.clear());
                    dispatch(failure(error));
                    dispatch(alertActions.error(error.message, "Create wallet failed", error.statusCode));
                }
            )
        } else {
            userService.updateWallet(walletInfo).then(
                resp => {
                    dispatch(alertActions.clear());
                    dispatch(alertActions.success(resp.message, "Wallet updated"))
                    dispatch(userActions.fetchWallets());
                },
                error => {
                    dispatch(failure(error));
                    dispatch(alertActions.error(error.message, "Update wallet failed", error.statusCode));
                }
            )
        }
    };

    function success(walletInfo) { return { type: userConstants.FETCH_WALLET_UPDATE_SUCCESS, walletInfo } }
    function failure(error) { return { type: userConstants.WALLET_UPDATE_FAILED, error } }
}

function fetchWalletInfo(walletId) {
    return dispatch => {
        userService.fetchWalletInfo(walletId).then(
            resp => {

                dispatch(alertActions.clear());
                dispatch(success(resp.content));
            },
            error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Failed to fetch wallet details", error.statusCode));
            }
        );
    };

    function success(walletInfo) { return { type: userConstants.FETCH_WALLET_INFO_SUCCESS, walletInfo } }
    function failure(error) { return { type: userConstants.FETCH_WALLET_ERROR, error } }
}

function logout() {
    return dispatch => {
        userService.logout().then(
            dispatch(() => {return { type: userConstants.LOGOUT }})
        )
        
    }
}

function updateUserInfo(userInfo){
    return dispatch => {
        userService.updateUserInfo(userInfo).then(
            resp => {
               // dispatch(success(resp.content));
                dispatch(alertActions.success(resp.message, "Profile update successful"));
            }, error=>{
               // dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Error occurred while fetching your profile information", error.statusCode));
            }
        );
    }

    //function success(user) { return { type: userConstants.UPDATE_USER_INFO_SUCCESS, user } }
    //function failure(error) { return { type: userConstants.UPDATE_USER_INFO_FAILURE, error } }
    
}

function getUserInfo()
{
    return dispatch => {
        userService.getUserInfo().then(
            resp => {
                dispatch(alertActions.clear());
                dispatch(success(resp.content));                
            },
            error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Error occurred while fetching your profile information", error.statusCode));
            }
        );
    };

    function success(user) { return { type: userConstants.GET_USER_INFO_SUCCESS, user } }
    function failure(error) { return { type: userConstants.GET_USER_INFO_FAILURE, error } }
}

function getAdminInfo(id)
{
    return dispatch => {
        userService.getAdminInfo(id).then(
            resp => {
                dispatch(alertActions.clear());
                dispatch(success(resp.content));                
            },
            error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Error occurred while fetching your profile information", error.statusCode));
            }
        );
    };

    function success(user) { return { type: userConstants.GET_USER_INFO_SUCCESS, user } }
    function failure(error) { return { type: userConstants.GET_USER_INFO_FAILURE, error } }
}

function fetchUserByDealerCode(dealerCode)
{
    return dispatch => {
        userService.fetchUserByDealerCode(dealerCode).then(
            resp => {
                dispatch(alertActions.clear());
                dispatch(success(resp.content));                
            },
            error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Error occurred while fetching your profile information", error.statusCode));
            }
        );
    };

    function success(user) { return { type: userConstants.GET_USER_INFO_SUCCESS, user } }
    function failure(error) { return { type: userConstants.GET_USER_INFO_FAILURE, error } }
}

function getAll() {
    return dispatch => {
        dispatch(request());

        userService.getAll()
            .then(
                users => dispatch(success(users)),
                error => {
                    dispatch(failure(error));
                   // dispatch(alertActions.error(error))
                }
            );
    };

    function request() { return { type: userConstants.GETALL_REQUEST } }
    function success(users) { return { type: userConstants.GETALL_SUCCESS, users } }
    function failure(error) { return { type: userConstants.GETALL_FAILURE, error } }
}