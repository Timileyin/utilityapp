﻿import { transactionConstant } from '../constants/transactionConstants';
import { transactionService } from '../services/transactionService';
import { alertActions } from './alertActions';
import { history } from '../helpers/history';
import { Alert } from 'reactstrap';

export const transactionAction = {
    fetchTransactionByRef,
    fetchVendingLogs,
    fetchCommAheadReport,
    fetchMasterWalletsBalances,
    fetchProcessorTransaction,
    fetchProcessorTransactionByProcessor,
    fetchMoneyTransferLogs,
    fetchBankList,
    verifyAccoutNumber,
    sendMoney,
    filterVendingLogs,
    filterCommAheadReport,
    filterProcessorTransactionByProcessor,
    filterMoneyTransferLogs,
    filterProcessorTransaction,
    fetchFoodOrders
}

function fetchFoodOrders(startDate, endDate){

    return dispatch => {
        transactionService.fetchOrders(startDate, endDate).then(
            resp => {                
                dispatch(success(resp));
               dispatch(alertActions.success(resp.message, "Fetch orders gotten successfully"));                              
            }, error => {
                dispatch(alertActions.error(error.message, "Fetch orders gotten successfully"));
                dispatch(failure());
            }
        )        
    };

    function failure() { return {type: transactionConstant.FETCH_ORDERS_FAILED}}
    function success(resp) {return {type: transactionConstant.FETCH_ORDERS, resp}}
}

function sendMoney(sendMoneyData)
{
    return dispatch => {
        transactionService.sendMoney(sendMoneyData).then(
            resp => {                
            //dispatch(success(resp));  
               dispatch(alertActions.success(resp.message, "Money transfer completed successfully")); 
               dispatch(fetchMoneyTransferLogs());              
            }, error => {
                dispatch(alertActions.error(error.message, "Send money has failed."))
            }
        )
        
       // function success(resp) {return {type: transactionConstant.SEND_MONEY_SUCCESSFUL, resp}}
    }
}

function verifyAccoutNumber(accountNumber, bankCode)
{
    return dispatch =>  {
        transactionService.verifyAccoutNumber(accountNumber, bankCode).then(
        resp => {
            dispatch(success(resp));
        }, error =>{
            if(error.StatusCode != 404){
                dispatch(alertActions.error(error.message, "Failed to fetch beneficiary details"));                
            }
            dispatch(failure())
        });

        function failure() { return {type: transactionConstant.VERIFY_ACCOUNT_NUMBER_FAILED}}
        function success(resp) {return {type: transactionConstant.VERIFY_ACCOUNT_NUMBER, resp}}
    }
}

function fetchBankList()
{
    return dispatch => {
        transactionService.fetchBankList().then(
            resp => {
                dispatch(success(resp));
        }, error => {
            if(error.StatusCode != 404)
            {
                dispatch(alertActions.error(error.message, "Failed to fetch bank list"));
            }
        });

        function success(resp) {return {type: transactionConstant.FETCH_BANK_LIST, resp}}
    }
}

function filterMoneyTransferLogs(startDate, endDate)
{
    return dispatch => {
        transactionService.fetchMoneyTransferLogs(startDate, endDate).then(resp => {
            dispatch(success(resp));
        }, error => {
            if(error.StatusCode != 404){
                dispatch(alertActions.error(error.message, "Failed to fetch bank transfer log"));
            };
        });

        function success(resp) { return { type: transactionConstant.FETCH_MONEY_TRANSFER, resp}}
    }
}

function fetchMoneyTransferLogs()
{
    return dispatch => {
        transactionService.fetchMoneyTransferLogs().then(resp => {
            dispatch(success(resp));
        }, error => {
            if(error.StatusCode != 404){
                dispatch(alertActions.error(error.message, "Failed to fetch bank transfer log"));
            };
        });

        function success(resp) { return { type: transactionConstant.FETCH_MONEY_TRANSFER, resp}}
    }
}

function filterProcessorTransactionByProcessor(processorId, startDate, endDate)
{
    return dispatch => {
        transactionService.fetchProcessorTransactionByProcessor(processorId, startDate, endDate).then(resp => {
            dispatch(success(resp))
        }, error => {
            if(error.StatusCode != 404){
                dispatch(alertActions.error(error.message, "Failed to fetch processor vending report", error.StatusCode));
            }
        });
        function success(resp) { return { type: transactionConstant.FETCH_PROCESSOR_VENDING_REPORT, resp } } 
    }
}

function fetchProcessorTransactionByProcessor(processorId)
{
    return dispatch => {
        transactionService.fetchProcessorTransactionByProcessor(processorId).then(resp => {
            dispatch(success(resp))
        }, error => {
            if(error.StatusCode != 404){
                dispatch(alertActions.error(error.message, "Failed to fetch processor vending report", error.StatusCode));
            }
        });
        function success(resp) { return { type: transactionConstant.FETCH_PROCESSOR_VENDING_REPORT, resp } } 
    }
}

function fetchProcessorTransaction()
{
    return dispatch => {
        transactionService.fetchProcessorTransaction().then(resp => {
            dispatch(success(resp))
        }, error => {
            if(error.StatusCode != 404){
                dispatch(alertActions.error(error.message, "Failed to fetch processor vending summary", error.StatusCode));
            }
        });
        function success(resp) { return { type: transactionConstant.FETCH_PROCESSOR_VENDING_SUMMARY, resp } } 
    }
}

function filterProcessorTransaction(startDate, endDate)
{
    return dispatch => {
        transactionService.fetchProcessorTransaction(startDate, endDate).then(resp => {
            dispatch(success(resp))
        }, error => {
            if(error.StatusCode != 404){
                dispatch(alertActions.error(error.message, "Failed to fetch processor vending summary", error.StatusCode));
            }
        });
        function success(resp) { return { type: transactionConstant.FETCH_PROCESSOR_VENDING_SUMMARY, resp } } 
    }
}

function fetchMasterWalletsBalances()
{
    return dispatch => 
    {
        transactionService.fetchMasterWalletsBalances().then(resp=>{
            dispatch(success(resp))
        }, error =>{
            if(error.StatusCode != 404)
            {
                dispatch(alertActions.error(error.message, "Failed to load master wallets", error.StatusCode));
            }
        });
        function success(resp) { return { type: transactionConstant.FETCH_MASTER_WALLETS, resp } }
    }
}

function filterCommAheadReport(startDate, endDate)
{
    return dispatch => {
        transactionService.fetchCommAheadReport(startDate, endDate).then(resp=>{
            dispatch(success(resp))
        }, error => {
            if(error.StatusCode != 404){
                dispatch(alertActions.error(error.message, "Fetch Commission Ahead Report", error.StatusCode));
            }            
        });
    }
 
    function success(resp) { return { type: transactionConstant.FETCH_COMMISSION_AHEAD_REPORT_SUCCESS, resp } }
}

function fetchCommAheadReport()
{
    return dispatch => {
        transactionService.fetchCommAheadReport().then(resp=>{
            dispatch(success(resp))
        }, error => {
            if(error.StatusCode != 404){
                dispatch(alertActions.error(error.message, "Fetch Commission Ahead Report", error.StatusCode));
            }            
        });
    }
 
    function success(resp) { return { type: transactionConstant.FETCH_COMMISSION_AHEAD_REPORT_SUCCESS, resp } }
}

function fetchVendingLogs(){
    return dispatch => {
        transactionService.fetchVendingLogs().then(resp=>{
            dispatch(success(resp))
        }, error => {
            if(error.StatusCode != 404){
                dispatch(alertActions.error(error.message, "Fetch vending logs", error.StatusCode));
            }
            
        });
    }
 
    function success(resp) { return { type: transactionConstant.FETCH_VENDING_LOGS, resp } }
}

function filterVendingLogs(startDate, endDate){
    return dispatch => {
        transactionService.fetchVendingLogs(startDate, endDate).then(resp=>{
            dispatch(success(resp))
        }, error => {
            if(error.StatusCode != 404){
                dispatch(alertActions.error(error.message, "Fetch vending logs", error.StatusCode));
            }
            
        });
    }
 
    function success(resp) { return { type: transactionConstant.FETCH_VENDING_LOGS, resp } }
}

function fetchTransactionByRef(txnRef) {

    return dispatch => {
        transactionService.fetchtransaction(txnRef).then(resp => {
            
            dispatch(success(resp))
        }, error => {
                history.push("/wallets");
            dispatch(alertActions.error(error.message, "Fetch transaction", error.StatusCode));
            
        });
    }

    function success(resp) { return { type: transactionConstant.FETCH_TRANSACTION_INFO_SUCCESS, resp } }
    //function failure(error) { return { type: transactionConstant.FETCH_TRANSACTION_INFO_FAILURE, error } }    
}