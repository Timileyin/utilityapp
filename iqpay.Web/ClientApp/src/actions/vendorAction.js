﻿import { vendorConstants } from '../constants/vendorConstants';
import { vendorService } from '../services/vendorService';
import { alertActions } from './alertActions';
import { history } from '../helpers/history';

export const vendorActions = {
    fetchVendors,
    saveVendor,
    fetchVendor,
    fetchVendorsInType,
    uploadLogoImage,
    fetchVendorComm,
    saveVendorComm,
    fetchCummWallets
    //createVendors,
    //deactivateVendor,
    //activateVendor
};

function fetchCummWallets(){
    return dispatch => {
        vendorService.fetchCummWallets().then(resp=>{
            dispatch(alertActions.clear());
            dispatch(alertActions.success(resp.message, "Vendors Cummulative wallet returned"));
            dispatch(success(resp))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Save Vendor Commission failed", error.StatusCode));
        });            
    }

    function success(resp) { return { type: vendorConstants.FETCH_CUMM_WALLETS, resp } }
    function failure(error) { return { type: vendorConstants.FETCH_CUMM_WALLETS_FAILED, error } }
}


function saveVendorComm(vendorInfo)
{
    return dispatch => {
        vendorService.updateVendorComm(vendorInfo).then(resp => {
            dispatch(alertActions.clear());
            dispatch(alertActions.success(resp.message, "Vendors Commission saved"));
            dispatch(success(resp))
            dispatch(fetchVendors());
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Save Vendor Commission failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: vendorConstants.SAVE_VENDORS_SUCCESS, resp } }
    function failure(error) { return { type: vendorConstants.SAVE_VENDORS_FAILURE, error } }
}

function fetchVendorComm(vendorId){
    return dispatch => {
        vendorService.fetchVendorDetails(vendorId).then(resp => {
            
            dispatch(success(resp))
        }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Fetch Vendor Commission failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: vendorConstants.FETCH_VENDORS_COMM_SUCCESS, resp } }
    function failure(error) { return { type: vendorConstants.FETCH_VENDORS_COMM_FAILED, error } }
}

function uploadLogoImage(vendorId, file)
{
    return dispatch => {
        vendorService.updateLogo(vendorId, file).then(
            resp=> {
                dispatch(alertActions.success(resp.message, "Vendor Logo has been updated successfully"));
                dispatch(fetchVendors());
            },
            error =>{
                dispatch(alertActions.error(error.message, "Vendor logo upload failed", error.statusCode));
            }
        )
    }
}

function fetchVendorsInType(serviceType) {
    return dispatch => {
        dispatch(request);

        vendorService.fetchVendorsInType(serviceType).then(resp => {
            dispatch(alertActions.clear());
            dispatch(success(resp));
            dispatch(alertActions.success(resp.message, "Vendors found"))
        }, error => {
            dispatch(alertActions.clear());
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Error occured while fetching vendors", error.StatusCode));
        })

    }

    function request() { return { type: vendorConstants.FETCH_VENDORS_REQUEST } }
    function success(resp) { return { type: vendorConstants.FETCH_VENDORS_SUCCESS, resp } }
    function failure(error) { return { type: vendorConstants.FETCH_VENDORS_FAILURE, error } }
}

function saveVendor(vendor) {
    return dispatch => {

        if (vendor.id == undefined) {
            vendorService.createVendor(vendor).then(resp => {
                dispatch(alertActions.clear());
                dispatch(alertActions.success(resp.message, "Vendors saved"));
                dispatch(success(resp))
                dispatch(fetchVendors());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Save Vendor failed", error.StatusCode));
            });
        } else {
            vendorService.updateVendor(vendor).then(resp => {
                dispatch(alertActions.clear());
                dispatch(alertActions.success(resp.message, "Vendors saved"));
                dispatch(success(resp))
                dispatch(fetchVendors());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Save Vendor failed", error.StatusCode));
            });
        }
        
    }

    function request() { return { type: vendorConstants.SAVE_VENDORS_REQUEST } }
    function success(resp) { return { type: vendorConstants.SAVE_VENDORS_SUCCESS, resp } }
    function failure(error) { return { type: vendorConstants.SAVE_VENDORS_FAILURE, error } }

}

function fetchVendor(id) {
    return dispatch => {
        vendorService.fetchVendorDetails(id).then(resp => {
            
            dispatch(success(resp))
        }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Fetch Vendor Information failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: vendorConstants.FETCH_VENDORS_INFO_SUCCESS, resp } }
    function failure(error) { return { type: vendorConstants.FETCH_VENDORS_INFO_FAILED, error } }
}


function fetchVendors() {
    return dispatch => {
        dispatch(request);

        vendorService.fetchVendors().then(resp => {
            dispatch(success(resp));
            dispatch(alertActions.clear());
            dispatch(alertActions.success(resp.message, "Vendors found"))
        }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Error occured while fetching vendors", error.StatusCode));
        })
        
    }

    function request() { return { type: vendorConstants.FETCH_VENDORS_REQUEST } }
    function success(resp) { return { type: vendorConstants.FETCH_VENDORS_SUCCESS, resp } }
    function failure(error) { return { type: vendorConstants.FETCH_VENDORS_FAILURE, error } }
}