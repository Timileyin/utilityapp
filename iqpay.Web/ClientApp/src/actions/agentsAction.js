﻿import { agentService } from '../services/agentService';
import { alertActions } from './alertActions';
import { history } from '../helpers/history';
import { agentsConstants } from '../constants/agentsConstants';

export const agentActions = {
    saveDealer,
    fetchDealers,
    fetchDealerDetails,
    toggleActivate
}

function saveDealer(dealer) {
    return dispatch => {

        if (dealer.id == undefined) {
            dealer.password = "Crystal123@@";
            dealer.confirmPassword = "Crystal123@@";
            agentService.createDealer(dealer).then(resp => {

                dispatch(alertActions.success(resp.message, "Dealer saved"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Save dealer failed", error.StatusCode));
            });
        } else {
            agentService.updateDealer(dealer).then(resp => {

                dispatch(alertActions.success(resp.message, "Dealer saved"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Update dealer failed", error.StatusCode));
            });
        }

    }

    function request() { return { type: agentsConstants.SAVE_DEALER_REQUEST } }
    function success(resp) { return { type: agentsConstants.SAVE_DEALER_SUCCESS, resp } }
    function failure(error) { return { type: agentsConstants.SAVE_DEALER_FAILURE, error } }
}

function toggleActivate(id, status) {

    return dispatch => {
        if (status) {
            agentService.activate(id).then(resp => {
                dispatch(alertActions.success(resp.message, "Dealer has been activated"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Dealer activation failed", error.StatusCode));
            })
        }
        else {
            agentService.deactivate(id).then(resp => {
                dispatch(alertActions.success(resp.message, "Dealer has been deactivated"));
                dispatch(success(resp))
                dispatch(fetchDealers());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Dealer deactivation failed", error.StatusCode));
            })
        }
    }

    function success(resp) { return { type: agentsConstants.SAVE_DEALER_SUCCESS, resp } }
    function failure(error) { return { type: agentsConstants.SAVE_DEALER_FAILURE, error } }
}

function fetchDealers() {
    return dispatch => {
        agentService.fetchDealers().then(resp => {

            dispatch(success(resp))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetch dealers failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: agentsConstants.FETCH_DEALERS_SUCCESS, resp } }
    function failure(error) { return { type: agentsConstants.FETCH_DEALERS_FAILURE, error } }
}

function fetchDealerDetails(id) {
    return dispatch => {
        agentService.fetchDealerDetails(id).then(resp => {

            dispatch(success(resp))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetch dealers details failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: agentsConstants.FETCH_DEALERS_INFO_SUCCESS, resp } }
    function failure(error) { return { type: agentsConstants.FETCH_DEALERS_FAILURE, error } }
}

