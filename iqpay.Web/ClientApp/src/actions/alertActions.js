﻿                                                                                                                                                                                                                                                                                                                                                                                                                   import { alertConstants } from '../constants/alertConstants'
export const alertActions = {
    success,
    error,
    clear
};

function success(message, title) {
    return { type: alertConstants.SUCCESS, message, title, isUnAuthorised: false };
}


function error(message, title, statusCode) {
    if (title !== "Login failed") {
        return { type: alertConstants.ERROR, message, title, isUnAuthorised: statusCode === 401 };
    }

    return { type: alertConstants.ERROR, message, title, isUnAuthorised: false};
}

function clear() {
    return { type: alertConstants.CLEAR };
}