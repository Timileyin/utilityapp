import { virtualAccountConstants } from '../constants/virtualAccountConstant';
import { virtualAccountService } from '../services/virtualAccountService';
import { alertActions } from './alertActions';

export const virtualAccountsAction = {
    fetchAccounts,
    fetchTransactions,
    fetchAgentTransactions
};

function fetchTransactions()
{
    return dispatch => {
        virtualAccountService.fetchVirtualTransactions().then(resp => {
            dispatch(alertActions.clear());
            dispatch(alertActions.success(resp.message, "Virtual transactions returned"));
            dispatch(success(resp));
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetching virtual transactions failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: virtualAccountConstants.FETCH_VIRTUALTRANSACTION_SUCCESS, resp } }
    function failure(error) { return { type: virtualAccountConstants.FETCH_VIRTUALTRANSACTION_FAILURE, error }}
}

function fetchAgentTransactions(dealerCode)
{
    return dispatch => {
        virtualAccountService.fetchAgentVirtualTransactions(dealerCode).then(resp => {
            dispatch(alertActions.clear());
            dispatch(alertActions.success(resp.message, "Virtual transactions returned"));
            dispatch(success(resp));
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetching virtual transactions failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: virtualAccountConstants.FETCH_VIRTUALTRANSACTION_SUCCESS, resp } }
    function failure(error) { return { type: virtualAccountConstants.FETCH_VIRTUALTRANSACTION_FAILURE, error }}
}

function fetchAccounts()
{
    return dispatch => {
        virtualAccountService.fetchVirtualAccounts().then(resp => {
            dispatch(alertActions.clear());
            dispatch(alertActions.success(resp.message, "Virtual accounts returned"));
            dispatch(success(resp));
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetching virtual accounts failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: virtualAccountConstants.FETCH_VIRTUALACCOUNT_SUCCESS, resp } }
    function failure(error) { return { type: virtualAccountConstants.FETCH_VIRTUALACCOUNT_FAILURE, error } }
}