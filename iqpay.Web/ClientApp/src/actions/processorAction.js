﻿import { processorConstants } from '../constants/processorConstants';
import { processorService } from '../services/processorServices';
import { alertActions } from './alertActions';
import { history } from '../helpers/history';

export const processorAction = {
    fetchProcessors,
    saveProcessor,
    fetchProcessor
    //createVendors,
    //deactivateVendor,
    //activateVendor
};



function saveProcessor(processor) {
    return dispatch => {

        if (processor.id == undefined) {
            processorService.createProcessor(processor).then(resp => {

                dispatch(alertActions.success(resp.message, "Vendors saved"));
                dispatch(success(resp))
                dispatch(fetchProcessors());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Save Vendor failed", error.StatusCode));
            });
        } else {
            processorService.updateProcessor(processor).then(resp => {

                dispatch(alertActions.success(resp.message, "Vendors saved"));
                dispatch(success(resp))
                dispatch(fetchProcessors());
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Save Vendor failed", error.StatusCode));
            });
        }

    }

    function request() { return { type: processorConstants.SAVE_PROCESSORS_REQUEST } }
    function success(resp) { return { type: processorConstants.SAVE_PROCESSORS_SAVE_SUCCESS, resp } }
    function failure(error) { return { type: processorConstants.SAVE_FAILURE_SAVE_FAILURE, error } }

}

function fetchProcessor(id) {
    return dispatch => {
        processorService.fetchProcessorDetails(id).then(resp => {

            dispatch(success(resp))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetch Processor Information failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: processorConstants.FETCH_PROCESSOR_INFO, resp } }
    function failure(error) { return { type: processorConstants.FETCH_PROCESSOR_INFO_FAILED, error } }
}


function fetchProcessors() {
    return dispatch => {
        dispatch(request);

        processorService.fetchProcessors().then(resp => {
            dispatch(success(resp));
            dispatch(alertActions.success(resp.message, "Processors found"))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Error occured while fetching processors", error.StatusCode));
        })

    }

    function request() { return { type: processorConstants.FETCH_PROCESSOR_REQUEST } }
    function success(resp) { return { type: processorConstants.FETCH_PROCESSORS_SUCCESS, resp } }
    function failure(error) { return { type: processorConstants.FETCH_PROCESSORS_FAILED, error } }
}  