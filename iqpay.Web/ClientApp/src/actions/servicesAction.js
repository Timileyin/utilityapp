﻿import { servicesConstant } from '../constants/servicesConstants';
import { servicesService } from '../services/servicesService';
import { alertActions } from './alertActions';
import { history } from '../helpers/history';

export const servicesActions = {
    fetchServices,
    saveService,
    fetchService,
    fetchServiceCommission,
    fetchServicesInServiceType,
    fetchServiceCodes,
    saveServiceCodes,
    vendService,
    initiateVending,
    fetchVendingDetails,
    completeVending,
    saveServiceCommission,
    processOrder    
};

function processOrder(vendCode, status) {
    return dispatch => {
        servicesService.processOrder(vendCode, status).then(resp => {
            dispatch(alertActions.clear());
            dispatch(fetchVendingDetails(vendCode));
            dispatch(alertActions.success("Order status updated successfully", resp.message)); 
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Order status update failed", error.StatusCode));
                
        });
    }

    function failure(error) { return { type: servicesConstant.SAVE_SERVICES_SAVE_FAILURE, error } }
}

function saveServiceCommission(serviceId, service){
    return dispatch => {
        servicesService.saveServiceCommission(serviceId, service).then(resp => {
            dispatch(alertActions.clear());
            dispatch(alertActions.success("Service commission updated successfully", resp.message)); 
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Save service commission failed", error.StatusCode));                
        });
    }

    function failure(error) { return { type: servicesConstant.SAVE_SERVICES_SAVE_FAILURE, error } }

}

function saveServiceCodes(serviceId, serviceCodes) {
    return dispatch => {
        servicesService.saveServiceCode(serviceId, serviceCodes).then(resp => {
            dispatch(alertActions.clear());
            dispatch(alertActions.success("Service code updated", resp.message));            
        }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Save service code failed", error.StatusCode));
                
        });
    }

    function success(resp) { return { type: servicesConstant.SAVE_SERVICE_CODE_SUCCESS, resp } }
    function failure(error) { return { type: servicesConstant.SAVE_SERVICE_CODE_FAILURE, error } }
}

function fetchServiceCommission(id){
    return dispatch => {
        servicesService.fetchServiceDetails(id).then(resp => {

            dispatch(success(resp))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetch Service Commission failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: servicesConstant.FETCH_SERVICE_COMMISSION_SUCCESS, resp } }
    function failure(error) { return { type: servicesConstant.FETCH_SERVICE_COMMISSION_FAILURE, error } }
}

function fetchVendingDetails(vendingCode) {
    return dispatch => {
        servicesService.fetchVendingDetails(vendingCode).then(resp => {
            dispatch(success(resp));
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetch vending details failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: servicesConstant.FETCH_VENDING_SUCCESS, resp } }
    function failure(error) { return { type: servicesConstant.FETCH_VENDING_FAILED, error } }
}

/*function initiateVending(serviceId, customerNumber, amount, numOfPins) {
    return dispatch => {
        servicesService.initiateVending(serviceId, customerNumber, amount, numOfPins ).then(resp => {
            dispatch(alertActions.clear());
            dispatch(alertActions.success("Please confirm the information on this page", "Initiate vending successful"));
            history.push("/confirm-vending/" + resp.content.vendingCode);
        }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Initiate vending failed", error.StatusCode));
        })
    }

    function success(resp) { return { type: servicesConstant.VENDING_SUCCESSFUL, resp } }
    function failure(error) { return { type: servicesConstant.VENDING_FAILED, error } }
}*/


function initiateVending(serviceId, customerNumber, amount, unitToPurchase = null, accountName = null, deliveryAddress1 = null, deliveryAddress2 = null, collectionPoint = null, deliveryOption = null, state = null, lga = null) {
    return dispatch => {
        servicesService.initiateVending(serviceId, customerNumber,  amount, unitToPurchase,accountName, deliveryAddress1, deliveryAddress2, collectionPoint, deliveryOption, state, lga).then(resp => {
            dispatch(alertActions.clear());
            dispatch(alertActions.success("Please confirm the information on this page", "Initiate vending successful"));
            history.push("/confirm-vending/" + resp.content.vendingCode);
        }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Initiate vending failed", error.StatusCode));
        })
    }

    function success(resp) { return { type: servicesConstant.VENDING_SUCCESSFUL, resp } }
    function failure(error) { return { type: servicesConstant.VENDING_FAILED, error } }
}

function fetchServiceCodes(serviceId) {
    return dispatch => {
        servicesService.fetchServiceCodes(serviceId).then(resp => {
            dispatch(alertActions.clear());
            dispatch(alertActions.success("Fetch Services in type", "Successful"));
            dispatch(success(resp))

        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetch Service Information failed", error.StatusCode));
        })
    }


    function success(resp) { return { type: servicesConstant.FETCH_SERVICE_CODE, resp } }
    function failure(error) { return { type: servicesConstant.FETCH_SERVICE_CODE_FAILED, error } }
}

function completeVending(vendCode) {
    return dispatch => {
        servicesService.completeVending(vendCode).then(resp => {

            dispatch(alertActions.clear());
            dispatch(alertActions.success("Print Transaction Slip", resp.message));
            history.push("/slip/" + vendCode);
            dispatch(success(resp));
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Error occured while processing transaction", error.StatusCode));
            history.push("/slip/" + vendCode);
        });
    }


    function success(resp) { return { type: servicesConstant.COMPLETE_VENDING_SUCCESS, resp } }
    function failure(error) { return { type: servicesConstant.COMPLETE_VENDING_FAILED, error } }
}

function vendService(serviceId, amount) {

    return dispatch => {
        servicesService.initiateVending(serviceId, amount).then(resp => {
            dispatch(alertActions.clear());
            dispatch(alertActions.success("Service vending completed", "Successful"));
            dispatch(success(resp))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Vending Failed", error.StatusCode));
        })
    }

    function success(resp) { return { type: servicesConstant.VENDING_SUCCESSFUL, resp } }
    function failure(error) { return { type: servicesConstant.VENDING_FAILED, error } }
}


function fetchServicesInServiceType(serviceTypeKey) {
    return dispatch => {
        servicesService.fetchServicesInServiceType(serviceTypeKey).then(resp => {
            
            dispatch(alertActions.clear());
            dispatch(alertActions.success("Fetch Services in type", "Successful"));
            dispatch(success(resp))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetch Service Information failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: servicesConstant.FETCH_SERVICE_SUCCESS, resp } }
    function failure(error) { return { type: servicesConstant.FETCH_SERVICE_FAILED, error } }
}

function saveService(service) {
    return dispatch => {

        if (service.id == undefined) {
            servicesService.createService(service).then(resp => {
                dispatch(alertActions.clear());
                dispatch(alertActions.success(resp.message, "Vendor service saved"));
                dispatch(success(resp))
                dispatch(fetchServices(service.vendorId));
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Save Vendor Service failed", error.StatusCode));
            });
        } else {
            servicesService.updateService(service).then(resp => {

                dispatch(alertActions.success(resp.message, "Vendor service saved"));
                dispatch(success(resp))
                dispatch(fetchServices(service.vendorId));
            }, error => {
                dispatch(failure(error));
                dispatch(alertActions.error(error.message, "Save service failed", error.StatusCode));
            });
        }

    }

    function request() { return { type: servicesConstant.SAVE_SERVICES_REQUEST } }
    function success(resp) { return { type: servicesConstant.SAVE_SERVICES_SAVE_SUCCESS, resp } }
    function failure(error) { return { type: servicesConstant.SAVE_SERVICES_SAVE_FAILURE, error } }

}

function fetchService(id) {
    return dispatch => {
        servicesService.fetchServiceDetails(id).then(resp => {

            dispatch(success(resp))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Fetch Service Information failed", error.StatusCode));
        });
    }

    function success(resp) { return { type: servicesConstant.FETCH_SERVICE_INFO, resp } }
    function failure(error) { return { type: servicesConstant.FETCH_SERVICE_FAILED, error } }
}


function fetchServices(vendorId) {
    return dispatch => {
        dispatch(request);

        servicesService.fetchServices(vendorId).then(resp => {
            dispatch(success(resp));
            dispatch(alertActions.clear());
            dispatch(alertActions.success(resp.message, "Services found"))
        }, error => {
            dispatch(failure(error));
            dispatch(alertActions.error(error.message, "Error occured while fetching services", error.StatusCode));
        })

    }

    function request() { return { type: servicesConstant.FETCH_SERVICE_REQUEST } }
    function success(resp) { return { type: servicesConstant.FETCH_SERVICE_SUCCESS, resp } }
    function failure(error) { return { type: servicesConstant.FETCH_SERVICE_FAILED, error } }
}