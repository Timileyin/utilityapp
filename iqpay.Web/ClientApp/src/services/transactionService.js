﻿import { service } from './service';

export const transactionService = {

    sendMoney: function(sendMoneyData){
        return service.makeAuthRequest("/vfd/transfer","POST", sendMoneyData).then(resp=>{
            return resp;
        });
    },
    fetchOrders: function(startDate, endDate){
        if(startDate != null || startDate != null){
            return service.makeAuthRequest("/transaction/orders?startDate="+startDate+"&endDate="+endDate, "GET").then(resp=>{
                return resp;
            });
        }else{
            return service.makeAuthRequest("/transaction/orders", "GET").then(resp=>{
                return resp;
            });
        }
        
    },
    fetchBankList: function()
    {
        return service.makeAuthRequest("/vfd/bankslist", "GET").then(resp => {
            return resp;
        });
    },
    verifyAccoutNumber: function(accoutNumber, bankCode)
    {
        return service.makeAuthRequest("/vfd/bankenquiry", "POST", {bank: bankCode, accountNo: accoutNumber}).then(resp => {
            return resp;
        });
    },
    fetchtransaction: function (txnRef) {
        return service.makeAuthRequest("/transaction/" + txnRef, "GET").then(resp => {
            return resp;
        });
    },
    fetchMoneyTransferLogs: function(startDate, endDate){
        if(!startDate || !endDate)
        {
            return service.makeAuthRequest("/banktransfer", "GET").then(resp => {
                return resp;
            });
        }else{
            return service.makeAuthRequest("/banktransfer?startDate="+startDate+"&endDate="+endDate, "GET").then(resp => {
                return resp;
            });
        }
        
    },
    fetchProcessorTransactionByProcessor: function(processorId, startDate, endDate)
    {
        if(!startDate || !endDate)
        {
            return service.makeAuthRequest("/report/processors-vending/"+processorId, "GET").then(resp => {
                return resp;
            });
        }else
        {
            return service.makeAuthRequest("/report/processors-vending/"+processorId+"?startDate="+startDate+"&endDate="+endDate, "GET").then(resp => {
                return resp;
            });
        }
        
    },

    fetchProcessorTransaction: function(startDate, endDate)
    {
        if(startDate == null || endDate == null)
        {
            return service.makeAuthRequest("/report/processors-vending-summary", "GET").then(resp => {
                return resp;
            });
        }else{
            return service.makeAuthRequest("/report/processors-vending-summary?startDate="+startDate+"&endDate="+endDate, "GET").then(resp => {
                return resp;
            });
        }
    },

    fetchCommAheadReport: function (startDate, endDate){
        if(startDate == null || endDate == null)
        {
            return service.makeAuthRequest("/report/commission-ahead-profits", "GET").then(resp => {
                return resp;
            });
        }else{
            return service.makeAuthRequest("/report/commission-ahead-profits?startDate="+startDate+"&endDate="+endDate, "GET").then(resp => {
                return resp;
            });
        }
        
    },

    fetchMasterWalletsBalances: function(){
        return service.makeAuthRequest("/report/processors-balances", "GET").then(resp=>{
            return resp;
        });
    },

    fetchVendingLogs: function(startDate = null, endDate = null)
    {
        if(!startDate && !endDate)
        {
            return service.makeAuthRequest("/vending/logs","GET").then(resp => {
                return resp;
            });
        }else{
            return service.makeAuthRequest("/vending/logs?startDate="+startDate+"&endDate="+endDate,"GET").then(resp => {
                return resp;
            });
        }
        
    }

}