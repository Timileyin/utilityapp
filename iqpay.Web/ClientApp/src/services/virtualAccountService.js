import { service } from './service';

export const virtualAccountService = {

    fetchVirtualAccounts: function () {
        return service.makeAuthRequest("/wema/accounts", "GET").then(resp => {
            return resp;
        });
    },

    fetchVirtualTransactions: function(){
        return service.makeAuthRequest("/wema/transactions/", "GET").then(resp =>{
            return resp;
        });
    },

    fetchAgentVirtualTransactions: function(dealerCode) {
        return service.makeAuthRequest("/wema/transactions/"+dealerCode, "GET").then(resp =>{
            return resp;
        });
    }
}