﻿import { service } from './service';

export const servicesService = {

    fetchServices: function (vendorId) {
        return service.makeRequest("/vendors/" + vendorId + "/services", "GET").then(resp => {
            return resp;
        });
    },

    processOrder: function (vendCode, status){
        return service.makeAuthRequest("/vending/" + vendCode + "/updateorders/"+ status, "PUT").then(resp => {
            return resp;
        });
    },

    fetchServiceDetails: function (serviceId) {
        return service.makeRequest("/services/" + serviceId, "GET").then(resp => {
            return resp;
        })
    },

    completeVending: function (vendCode) {
        return service.makeAuthRequest("/vending/complete", "POST", { "vendingCode": vendCode }).then(resp => {
            return resp;
        });
    },

    saveServiceCommission: function (serviceId, serviceComm) {
        return service.makeAuthRequest("/services/"+serviceId+"/commission", "PUT" ,  serviceComm).then(resp=>{
            return resp;
        })
    },
    
    initiateVending: function (serviceId, customerNumber, amount, numOfPins = null, accountName = null, deliveryAddress1 = null, deliveryAddress2 = null, collectionPoint = null, deliveryOption = null, state = null, lga = null) {        
        return service.makeAuthRequest("/vending/initiate", "POST", { "serviceId": serviceId, "accountName": accountName,  "customerNumber": customerNumber, "amount": amount, "numberOfPins": numOfPins, 'deliveryOption': deliveryOption, 'deliveryAddressLine1': deliveryAddress1, 'deliveryAddressLine2': deliveryAddress2, 'collectionPoint': collectionPoint, 'state': state, 'lga': lga}).then(resp => {
            return resp;
        })
    },

    fetchVendingDetails: function (vendingCode) {
        return service.makeAuthRequest("/vending/" + vendingCode, "GET").then(resp => {
            return resp;
        })
    },

    saveServiceCode: function (serviceId, serviceCodes) {
        return service.makeAuthRequest("/services/" + serviceId + "/processorCodes", "PUT", serviceCodes).then(resp => {
            return resp;
        })
    },

    fetchServiceCodes: function (serviceId) {
        return service.makeAuthRequest("/services/" + serviceId + "/processorCodes", "GET").then(resp => {
            return resp
        })
    },

    createService: function (_service) {
        return service.makeAuthRequest("/services", "POST", _service).then(resp => {
            return resp;
        });
    },

    fetchServicesInServiceType: function (typeKey) {
        return service.makeRequest("/service-types/"+typeKey+"/services", "GET").then(resp => {
            return resp.content;
        });
    },

    vendService: function (serviceId, amount) {
        return service.makeAuthRequest("/vending", "POST", { "serviceId": serviceId, "amount": amount }).then(resp => {
            return resp.content
        });
    },

    deactivateVendor: function (vendorId) {

    },

    activateVendor: function (vendorId) {

    },

    updateService: function (_service) {
        return service.makeAuthRequest("/services/" + _service.id, "PUT", _service).then(resp => {
            return resp;
        });
    }


}