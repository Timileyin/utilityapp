﻿import { service } from './service';

export const agentService = {

    fetchDealerDetails: function (dealerId) {
        return service.makeAuthRequest("/agents/" + dealerId, "GET").then(resp => {
            return resp;
        })
    },

    fetchDealers: function () {
        return service.makeAuthRequest("/agents", "GET").then(resp => {
            return resp;
        });
    },

    createDealer: function (dealer) {
        return service.makeAuthRequest("/users/create-agent", "POST", dealer).then(resp => {
            return resp;
        });
    },

    updateDealer: function (dealer) {
        return service.makeAuthRequest("/agents/" + dealer.id, "PUT", dealer).then(resp => {
            return resp;
        });
    },

    activate: function (id) {
        return service.makeAuthRequest("/agents/" + id + "/activate", "PUT").then(resp => {
            return resp;
        });
    },

    deactivate: function (id) {
        return service.makeAuthRequest("/agents/" + id + "/deactivate", "PUT").then(resp => {
            return resp;
        });
    }
}
