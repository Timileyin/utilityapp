﻿import { service } from './service';

export const dealerService = {

    fetchDealerDetails: function (dealerId) {
        return service.makeAuthRequest("/dealers/" + dealerId, "GET").then(resp => {
            return resp;
        })
    },
    
    fetchServiceChecklist: function (dealerId){
        return service.makeAuthRequest("/dealers/"+dealerId+"/services", "GET").then(resp => {
            return resp;
        })
    },

    updateCheckListItems: function (dealerId, vendorChecklist){
        return service.makeAuthRequest("/dealers/"+dealerId+"/services", "PUT", vendorChecklist).then(resp => {
            return resp;
        }) 
    },

    updateCommission: function (serviceId, vendorId, dealerId, amount){
        return service.makeAuthRequest("/dealers/"+dealerId+"/commissions", "PUT", {serviceId: serviceId, vendorId: vendorId, dealerId: dealerId, commission: amount }).then(resp => {
            return resp;
        })
    },

    updateDealerCommissionCap: function (serviceId, vendorId, dealerId, amount){
        return service.makeAuthRequest("/dealers/"+dealerId+"/commissionscap", "PUT", {serviceId: serviceId, vendorId: vendorId, dealerId: dealerId, cap: amount }).then(resp => {
            return resp;
        })
    },

    fetchCommissionRates: function (dealerId){
        return service.makeAuthRequest("/dealers/"+dealerId+"/commissions","GET").then(resp => {
            return resp;
        })
    },

    fetchDealers: function () {
        return service.makeAuthRequest("/dealers", "GET").then(resp => {
            return resp;
        });
    },

    createDealer: function (dealer) {
        return service.makeAuthRequest("/users/create-dealer", "POST", dealer).then(resp => {
            return resp;
        });
    },

    updateDealer: function (dealer) {
        return service.makeAuthRequest("/dealers/" + dealer.id, "PUT", dealer).then(resp => {
            return resp;
        });
    },

    activate: function (id) {
        return service.makeAuthRequest("/dealers/" + id + "/activate", "PUT").then(resp => {
            return resp;
        });
    },

    deactivate: function (id) {
        return service.makeAuthRequest("/dealers/" + id + "/deactivate", "PUT").then(resp => {
            return resp;
        });
    }
}
