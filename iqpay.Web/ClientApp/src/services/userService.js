﻿import { fundingRequestActions } from '../actions/fundingRequestActions';
import { service} from './service';


export const userService = {
    savePermission: function(id, permissions){
        permissions = permissions.toString();
        return service.makeAuthRequest("/users/"+id+"/permissions", "PUT", {permissions}).then(
            resp=>{
                return resp;
            }
        );
    },

    fetchUserByDealerCode: function(dealerCode){
        return service.makeRequest("/users?dealerCode="+dealerCode).then(resp => {
            return resp;
        });
    },

    createAdmin: function(adminInfo){
        return service.makeAuthRequest("/users/create-sysadmin", "POST", adminInfo).then(resp =>{
            return resp;
        });
    },
    registerPartnerDealer: function(id, dealerType)
    {
        return service.makeAuthRequest("/users/register-partner-dealer", "POST", {id, userType: dealerType}).then(resp=>{
            return resp;
        });
    },
    agentRegistration: function(email, firstname, surname, phonenumber, companyName, bVN, nIN, address, addressLine2, state, lga, photoId, superDealerId)
    {
        const formData = new FormData();
        formData.append("email", email);
        formData.append("firstname", firstname);
        formData.append("surname", surname);
        formData.append("phonenumber", phonenumber);
        formData.append("companyName", companyName);
        formData.append("bVN", bVN);
        formData.append("nIN", nIN);
        formData.append("addressLine1", address);
        formData.append("addressLine2", addressLine2);
        formData.append("state", state);
        formData.append("lga", lga);
        formData.append("superDealerId", superDealerId);
        formData.append("photoID", photoId);
        return service.uploadFileForm("/users/paternership-registration", "POST", formData).then(resp => {
            return resp;
        });
    },
    updateAdmin: function(adminInfo)
    {
        return service.makeAuthRequest("/users/update-sysadmin", "PUT", adminInfo).then(resp => {
            return resp;
        });
    },
    fetchPartnershipRequests: function(){
        return service.makeAuthRequest("/users/partnership-requests","GET").then(resp => {
            return resp;
        });
    },
    fetchRequestInfo: function(id){
        return service.makeAuthRequest("/users/partnership-requests/"+id,"GET").then(resp => {
            return resp;
        });
    },

    fetchAdministrators: function(){
        return service.makeAuthRequest("/users/administrators", "GET").then(resp => {
            return resp;
        });
    },

    generateAccountNumber: function() {
        return service.makeAuthRequest("/users/generate-reservedBankAccount", "POST").then(resp => {
            return resp;
        });
    },

    fetchWalletBalances: function(){
        return service.makeAuthRequest("/report/wallet-balances", "GET").then(resp=>{
            return resp;
        });
    },

    login: function (username, password) {

        return service.makeRequest("/users/authenticate", "POST", { username: username, password: password }).then(resp => {
            localStorage.setItem('currentUser', JSON.stringify(resp.content));
            return resp;
        });
    },

    fetchReservedAccount: function(){
        return service.makeAuthRequest("/users/reservedBankDetails", "GET").then(resp => {
            return resp;
        });
    },

    fetchDashboardData: function(){
        return service.makeAuthRequest("/users/dashboard", "GET").then(resp=>{
            return resp;
        });
    },

    updateProfileImage: function(file){
        return service.uploadFile("/users/company-logo", "PUT", file).then(resp => {
            return resp;
        });
    },

    confirmEmail: function (code, userId) {
        return service.makeRequest("/users/confirmEmail", "POST", { userId: userId, code: code }).then(resp => {
            return resp;
        });
    },    

    fundIntraWallet: function (walletId, baseWalletId, fundAmount) {
        return service.makeAuthRequest("/wallets/" + walletId + "/fund-with-baseWallet/" + baseWalletId, "PUT", { amount: fundAmount, walletId: walletId, baseWalletId: baseWalletId, isIntraWallet: true, isAuto: false }).then(resp => {
            return resp;
        });

    },

    fundBaseWalletWithComm: function(walletId, baseWallet){
        return service.makeAuthRequest("/wallets/"+walletId+"/move-to-base/"+baseWallet, "PUT",{}).then(resp=>{
            return resp;
        });
    },

    initiateAutoFunding(fundingInfo) {
        return service.makeAuthRequest("/wallets/" + fundingInfo.walletId + "/initiate-autofunding", "POST", { ...fundingInfo }).then(resp => {
            return resp;
        });
    },

    manualFund: function (fundingInfo) {
        return service.makeAuthRequest("/wallets/" + fundingInfo.walletId + "/manual-funding", "POST", { ...fundingInfo }).then(resp => {
            return resp;
        });
    },

    signup: function (email, firstname, surname, confirmPassword, password, phonenumber) {
        return service.makeRequest("/users/signup", "POST", { email, firstname, surname, confirmPassword, password, phonenumber }).then(req => {            
            return req.message;
        });
    },

    fetchWallets: function () {
        return service.makeAuthRequest("/users/wallets", "GET").then(req => {
            return req;
        });
    },

    fetchWalletInfo: function (walletId) {
        return service.makeAuthRequest("/wallets/" + walletId, "GET").then(req => {
            return req;
        });
    },

    createWallet: function (walletInfo) {
        console.log(walletInfo);
        return service.makeAuthRequest("/wallets", "POST", walletInfo).then(req => {
            return req;
        });
    },

    updateWallet: function(walletInfo) {
        return service.makeAuthRequest("/wallets/" + walletInfo.id, "PUT", walletInfo).then(req => {
            return req;
        });
    },

    getUserInfo: function(){
        return service.makeAuthRequest("/users/profile","GET").then(req=>{
            return req;
        });
    },

    getAdminInfo: function(id){
        return service.makeAuthRequest("/users/administrators/"+id,"GET").then(req=>{
            return req;
        });
    },

    updateUserInfo: function(userInfo){
        return service.makeAuthRequest("/users/profile","PUT",userInfo).then(resp=>{
            var currentUser = JSON.parse(localStorage.getItem('currentUser'));
            currentUser.firstname = resp.content.firstName;
            currentUser.lastname = resp.content.lastName;
            localStorage.setItem("currentUser", JSON.stringify(currentUser));
            return resp;
        });
    },

    resetPassword: function(userId, code, newPassword, confirmNewPassowrd){
        return service.makeRequest("/users/resetPassword", "POST", {"code": code, "password": newPassword, "confirmPassword": confirmNewPassowrd, "userId": userId}).then(resp=>{
            return resp;
        });
    },

    forgotPassword: function(username){
        return service.makeRequest("/users/forgotpassword", "POST", {"username": username}).then(req => {
            return req;
        });
    },

    logout: function(){
    // remove user from local storage to log user out
        localStorage.removeItem('user');
    }


}