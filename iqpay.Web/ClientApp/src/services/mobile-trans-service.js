import { service } from './service';
export const mobileTransactionService = {
    fetchtransaction: function (txnRef) {
        return service.makeAuthRequest("/transaction/" + txnRef, "GET").then(resp => {
            return resp;
        });
    },

    fetchVendingLogs: function(){
        return service.makeAuthRequest("/vending/logs","GET").then(resp => {
            return resp;
        });
    }

}