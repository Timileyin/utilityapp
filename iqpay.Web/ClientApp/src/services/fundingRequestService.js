﻿import { service } from './service';

export const fundingRequestService = {

    fetchFundingRequest: function (startDate, endDate) {
        if(!startDate || !endDate){
            return service.makeAuthRequest("/fundingRequest", "GET").then(resp => {
                return resp;
            });
        }else{
            return service.makeAuthRequest("/fundingRequest?startDate="+startDate+"&endDate="+endDate, "GET").then(resp => {
                return resp;
            });
        }
        
    },

    fetchFundingRequestInfo: function (requestId) {
        return service.makeAuthRequest("/fundingRequest/" + requestId, "GET").then(resp => {
            return resp;
        });
    }, 
    filterFundingRequestHistory: function(startDate, endDate)
    {
        return service.makeAuthRequest("/fundingRequest/history?startDate="+startDate+"&endDate="+endDate, "GET").then(resp => {
            return resp;
        });
    }, 

    fetchFundingRequestHistory: function(){
        return service.makeAuthRequest("/fundingRequest/history", "GET").then(resp => {
            return resp;
        });
    },

    filterFundingRequest: function (startDate, endDate, user) {
        return service.makeAuthRequest("/fundingRequest?startDate=" + startDate + "&endDate=" + endDate + "&user=" + user, "GET").then(
            resp => {
                return resp;
            });
    },

    approveFundingRequest: function (requestId, amount) {

        return service.makeAuthRequest("/fundingRequest/" + requestId + "/approve/"+amount, "PUT").then(resp => {
            return resp;
        });
    },

    fetchVendorDetails: function (vendorId) {

    },

    createVendor: function () {

    },

    deactivateVendor: function (vendorId) {

    },

    activateVendor: function (vendorId) {

    },

    updateVendor: function (vendorId) {

    }


}