﻿import { service } from './service';

export const processorService = {

    fetchProcessors: function () {
        return service.makeAuthRequest("/processors", "GET").then(resp => {
            return resp;
        });
    },

    fetchProcessorDetails: function (processorId) {
        return service.makeAuthRequest("/processors/" + processorId, "GET").then(resp => {
            return resp;
        })
    },

    createProcessor: function (processor) {
        return service.makeAuthRequest("/processors", "POST", processor).then(resp => {
            return resp;
        });
    },

    updateProcessor: function (processor) {
        return service.makeAuthRequest("/processors/" + processor.id, "PUT", processor).then(resp => {
            return resp;
        });
    }

}