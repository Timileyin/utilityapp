﻿import { config } from '../config';
import { history } from '../helpers/history';
export const service = {

    
    API_URL: config.API_URL,
    

    makeRequest: function(loc, method, body) {
        const requestOptions = {
            method: method,
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(body)
        };

        return fetch(this.API_URL + loc, requestOptions).then(this.handleResponse);


    },

    uploadFileForm: function(loc, method, fileForm)
    {
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));        
        const requestOptions = {
            method: method,
            headers: { 'Authorization': "bearer " + currentUser?.token },
            body: fileForm
        };

        return fetch(this.API_URL + loc, requestOptions).then(this.handleResponse);
    },

    uploadFile: function(loc, method, file) {
        
        var currentUser = JSON.parse(localStorage.getItem("currentUser"));
        const formData = new FormData()
        formData.append('file', file)
        const requestOptions = {
            method: method,
            headers: { 'Authorization': "bearer " + currentUser?.token },
            body: formData
        };

        return fetch(this.API_URL + loc, requestOptions).then(this.handleResponse);
    },

  
    makeAuthRequest: function(loc, method, body) {

        var currentUser = JSON.parse(localStorage.getItem("currentUser"));
        console.log(currentUser);
        
        const requestOptions = {
            method: method,            
            body: JSON.stringify(body)
        };
        if(currentUser != null){
            requestOptions.headers = { 'Content-Type': 'application/json', 'Authorization': "bearer " + currentUser.token };
        }
        return fetch(this.API_URL + loc, requestOptions).then(this.handleResponse);
    },

    handleResponse: function (response) {
        return response.text().then(text => {
            const data = text && JSON.parse(text);
            if (!response.ok) {
                if (response.status === 401) {
                    // auto logout if 401 response returned from api
                    localStorage.removeItem('currentUser');
                    //history.push("/login");
                }
                const error = data || response.statusText;
               // error.statusCode = data.statusCode;
                return Promise.reject(error);
            }

            return data;
        });


    }
}

