﻿import { service } from './service';

export const superdealerService = {

    fetchDealerDetails: function (dealerId) {
        return service.makeAuthRequest("/superdealers/" + dealerId, "GET").then(resp => {
            return resp;
        })
    },

    fetchDealers: function () {
        return service.makeAuthRequest("/superdealers", "GET").then(resp => {
            return resp;
        });
    },

    createDealer: function (dealer) {
        return service.makeAuthRequest("/users/create-superdealer", "POST", dealer).then(resp => {
            return resp;
        });
    },

    updateDealer: function (dealer) {
        return service.makeAuthRequest("/superdealers/" + dealer.id, "PUT", dealer).then(resp => {
            return resp;
        });
    },

    activate: function (id) {
        return service.makeAuthRequest("/superdealers/" + id + "/activate", "PUT").then(resp => {
            return resp;
        });
    },

    deactivate: function (id) {
        return service.makeAuthRequest("/superdealers/" + id + "/deactivate", "PUT").then(resp => {
            return resp;
        });
    }
}
