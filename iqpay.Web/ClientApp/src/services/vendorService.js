﻿import { service } from './service';
import { serviceTypes } from '../constants/commissionMode';

export const vendorService = {

    fetchVendors: function () {
        return service.makeRequest("/vendors", "GET").then(resp => {
            return resp;
        });
    },

    fetchCummWallets: function(){
        return service.makeAuthRequest("/report/cummwalletbalance","GET").then(resp => {
            return resp;
        });
    },

    updateVendorComm: function ( vendorComm) {
        return service.makeAuthRequest("/vendors/"+vendorComm.id+'/vend-comm', "PUT", vendorComm).then(resp=>{
            return resp;
        });
    },

    updateLogo: function(vendorId, file){
        return service.uploadFile("/vendors/"+vendorId+"/upload-logo", "PUT", file).then(resp=>{
            return resp;
        });
    },

    fetchVendorDetails: function (vendorId) {
        return service.makeRequest("/vendors/" + vendorId, "GET").then(resp => {
            return resp;
        });
    },

    createVendor: function (vendor) {
        return service.makeAuthRequest("/vendors", "POST", vendor).then(resp => {
            return resp;
        });
    },

    fetchVendorsInType: function (serviceType)
    {
        return service.makeAuthRequest("/service-types/" + serviceType + "/vendors").then(resp => {
            return resp;
        });
    },

    deactivateVendor: function (vendorId) {

    },

    activateVendor: function (vendorId) {

    },

    updateVendor: function (vendor) {
        return service.makeAuthRequest("/vendors/"+vendor.id, "PUT", vendor).then(resp => {
            return resp;
        });
    }


}