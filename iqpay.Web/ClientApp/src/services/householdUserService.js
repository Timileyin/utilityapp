import { service } from './service';

export const householdUserService = {

    fetchDealerDetails: function (dealerId) {
        return service.makeAuthRequest("/householduser/" + dealerId, "GET").then(resp => {
            return resp;
        })
    },

    fetchDealers: function () {
        return service.makeAuthRequest("/householduser", "GET").then(resp => {
            return resp;
        });
    },

    activate: function (id) {
        return service.makeAuthRequest("/agents/" + id + "/activate", "PUT").then(resp => {
            return resp;
        });
    },

    deactivate: function (id) {
        return service.makeAuthRequest("/agents/" + id + "/deactivate", "PUT").then(resp => {
            return resp;
        });
    }
}
