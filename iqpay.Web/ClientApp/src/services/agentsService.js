﻿import { service } from './service';
export const subDealerService = {

    fetchAgentDetails: function (subdealerId) {
        return service.makeRequest("/sdealers/" + subdealerId, "GET").then(resp => {
            return resp;
        })
    },

    fetchAgents: function () {
        return service.makeRequest("/sub-dealers", "GET").then(resp => {
            return resp;
        });
    },

    createAgent: function (dealer) {
        return service.makeAuthRequest("/sub-dealers", "POST", dealer).then(resp => {
            return resp;
        });
    },

    updateAgent: function (dealer) {
        return service.makeAuthRequest("/sub-dealers/" + dealer.id, "PUT", dealer).then(resp => {
            return resp;
        });
    }
}