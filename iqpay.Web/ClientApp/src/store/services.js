﻿import { servicesConstant } from '../constants/servicesConstants';
import { service } from '../services/service';
const initialState = { services: [], serviceInfo: null, serviceCommission: null ,serviceCodes: null, vendDetails: null  }

export function reducer(state = initialState, action) {

    switch (action.type) {
        case servicesConstant.SAVE_SERVICES_SAVE_SUCCESS:
            return {
                fetched: true,                
                serviceInfo: null,
                serviceCodes: null
            }

        case servicesConstant.FETCH_SERVICE_SUCCESS: {
            return {
                services: action.resp.content,
                serviceInfo: null,
                serviceCodes: null
            }
        }

        case servicesConstant.FETCH_SERVICE_INFO: {
            return {
                serviceInfo: action.resp.content,
                serviceCodes: null
            }
        }

        case servicesConstant.FETCH_SERVICE_CODE:
            return {
                serviceCodes: action.resp.content
            }

        case servicesConstant.SAVE_SERVICE_CODE_SUCCESS:
            return {
                
                serviceCodes: null,
                serviceInfo: null
            }

        case servicesConstant.VENDING_SUCCESSFUL:
            return {

            }

        case servicesConstant.VENDING_FAILED:
            return {

            }

        case servicesConstant.FETCH_VENDING_SUCCESS:
            return {
                vendDetails: action.resp.content
            }

        case servicesConstant.FETCH_SERVICE_CODE_FAILED:
            return {
                vendDetails: null
            }
        
        case servicesConstant.FETCH_SERVICE_FAILED:
            return {
                services: []
            }

        case servicesConstant.FETCH_SERVICE_COMMISSION_SUCCESS:
            return{
                serviceInfo: null,
                serviceCommission: action.resp.content,
                serviceCodes: null                
            }

        case servicesConstant.FETCH_SERVICE_COMMISSION_FAILURE:
            return{
                serviceInfo: null,
                serviceCommission: null,
                serviceCodes: null                
            }

        default:
            return state;
    }
}