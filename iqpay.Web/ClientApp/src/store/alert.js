﻿import { alertConstants } from '../constants/alertConstants';

export function reducer(state = {}, action) {
    switch (action.type) {
        case alertConstants.SUCCESS:
            return {
                type: 'alert-success',
                message: action.message,
                isUnAuthorised: action.isUnAuthorised,
                title: action.title
            };
        case alertConstants.ERROR:
            return {
                type: 'alert-danger',
                message: action.message,
                isUnAuthorised: action.isUnAuthorised,
                title: action.title
            };
        case alertConstants.CLEAR:
            return {};
        default:
            return state
    }
}