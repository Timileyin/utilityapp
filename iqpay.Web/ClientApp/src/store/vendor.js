﻿import { vendorConstants } from '../constants/vendorConstants';
const initialState = { vendors: [], vendorInfo: null, vendorsWallet: [] } 

export function reducer(state = initialState, action)
{
    switch (action.type) {
        case vendorConstants.FETCH_VENDORS_REQUEST:
            return {
                fetching: true,
                vendorInfo: null,
                vendors: []
            };

        case vendorConstants.FETCH_VENDORS_SUCCESS:
            return {
                fetched: true,
                vendors: action.resp.content,
                vendorInfo: null
            }

        case vendorConstants.FETCH_CUMM_WALLETS:{
            return {
                vendorsWallet: action.resp.content
            }
        }

        case vendorConstants.FETCH_VENDORS_ERROR:
            return {
                fetched: false,
            }
        case vendorConstants.FETCH_VENDORS_FAILURE:{
            return {
                vendors: []
            }
        }

        case vendorConstants.FETCH_VENDORS_COMM_SUCCESS:
            return{
                vendorComm: action.resp.content
            }

        case vendorConstants.FETCH_VENDORS_COMM_FAILED:
            return {
                vendorComm: null
            }
       

        case vendorConstants.FETCH_VENDORS_INFO_SUCCESS:
            return {
                vendorInfo: action.resp.content
            }

        case vendorConstants.FETCH_VENDORS_INFO_FAILED:
            return {
                vendorInfo: null
            }

        default:
            return state;
    }
}

