﻿import { transactionConstant } from '../constants/transactionConstants';
const initialState = { transactions: [], vendingLogs: [], orders:[], transactionInfo: null, moneyTransferLogs: [], bankList: [], verifyAccount: null}

export function reducer(state = initialState, action) {
    console.log(action);
    switch (action.type) {        
        case transactionConstant.FETCH_TRANSACTION_INFO_SUCCESS: {
            return {
                transactionInfo: action.resp.content
            }
        }

        case transactionConstant.FETCH_ORDERS: {
            return {
                orders: action.resp.content
            }
        }

        case transactionConstant.FETCH_ORDERS_FAILED: {
            return {
                orders: []
            }
        }

        case transactionConstant.FETCH_COMMISSION_AHEAD_REPORT_SUCCESS: {
            return {
                commAheadReport: action.resp.content
            }
        }

        case transactionConstant.VERIFY_ACCOUNT_NUMBER_FAILED: {
            return {
                verifyAccount: null
            }
        }       

        case transactionConstant.FETCH_MASTER_WALLETS: {
            return {
                masterWallets: action.resp.content
            }
        }

        case transactionConstant.FETCH_BANK_LIST: {
            return {
                bankList: action.resp.content,
                verifyAccount:null
            }
        }

        case transactionConstant.VERIFY_ACCOUNT_NUMBER:
        {
            return {
                verifyAccount: action.resp.content
            }
        }

        case transactionConstant.FETCH_MONEY_TRANSFER: {
            return {
                moneyTransferLogs: action.resp.content,
                verifyAccount: null 
            }
        }

        case transactionConstant.FETCH_PROCESSOR_VENDING_SUMMARY: {
            return {
                processorTransactions: action.resp.content
            }
        }

        case transactionConstant.FETCH_PROCESSOR_VENDING_REPORT:{
            return {
                processorReport: action.resp.content
            }
        }

        case transactionConstant.FETCH_VENDING_LOGS:{
            return {
                vendingLogs: action.resp.content
            }
        }

        default: {
            return state;
        }

    }
}