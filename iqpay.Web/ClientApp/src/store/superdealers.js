﻿import { superdealerConstants } from '../constants/superdealerConstants';
const initialState = { dealers: [], dealerInfo: null }

export function reducer(state = initialState, action) {
    switch (action.type) {
        case superdealerConstants.FETCH_DEALERS_REQUEST:
            return {
                fetching: true,
                dealerInfo: null
                //vendor: action.vendors
            };

        case superdealerConstants.FETCH_DEALERS_SUCCESS:
            return {
                fetched: true,
                dealers: action.resp.content,
                dealerInfo: null
            }

        case superdealerConstants.SAVE_DEALER_FAILURE:
            return {
                fetched: false,

            }

        case superdealerConstants.FETCH_DEALERS_INFO_SUCCESS:
            return {
                dealerInfo: action.resp.content
            }

        case superdealerConstants.FETCH_DEALERS_INFO_FAILED:
            return {
                dealerInfo: null
            }

        default:
            return state;
    }
}