﻿import { fundingRequestConstants } from '../constants/fundingRequestConstants';
const initialState = { requests: [], request: null } 

export const  reducer = (state = initialState, action) => {
    
    switch (action.type) {
        case fundingRequestConstants.FETCH_FUND_REQUEST_SUCCESS:
            console.log(action);
            return {
                requests: action.resp.content,
                request: null
            };

        case fundingRequestConstants.FETCH_FUND_REQUEST_FAILURE:
            return {

                request: null
            };

        case fundingRequestConstants.FETCH_FUND_REQUEST_INFO_SUCCESS:
            return {
                request: action.fundingInfo
            }

        case fundingRequestConstants.FETCH_FUND_REQUEST_INFO_FAILURE:
            return {
                request: null
            }



        default:
            return state            
    }
}