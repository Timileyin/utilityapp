import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import * as Counter from './Counter';
import * as WeatherForecasts from './WeatherForecasts';
import * as authentication from './authentication';
import * as vendor from './vendor';
import * as alert from './alert';
import * as fundingRequest from './fundingRequest';
import * as serviceManagement from './services';
import * as transaction from './transaction';
import * as processors from './processors';
import * as dealers from './dealers';
import * as superdealers from './superdealers';
import * as agents from './agents';
import * as householdusers from './householduser';
import * as virtualAccount from './virtualaccount';

export default function configureStore (history, initialState) {
  const reducers = {
    counter: Counter.reducer,
      weatherForecasts: WeatherForecasts.reducer,
      authentication: authentication.reducer,
      alert: alert.reducer,
      vendor: vendor.reducer,
      fundingRequest: fundingRequest.reducer,
      serviceManagement: serviceManagement.reducer,
      transaction: transaction.reducer,
      processors: processors.reducer,
      dealer: dealers.reducer,
      superdealer: superdealers.reducer,
      agent: agents.reducer,
      householdusers: householdusers.reducer,
      virtualAccounts: virtualAccount.reducer
  };

  const middleware = [
    thunk,
    routerMiddleware(history)
  ];

  // In development, use the browser's Redux dev tools extension if installed
  const enhancers = [];
  const isDevelopment = process.env.NODE_ENV === 'development';
  if (isDevelopment && typeof window !== 'undefined' && window.devToolsExtension) {
    enhancers.push(window.devToolsExtension());
  }

  const rootReducer = combineReducers({
    ...reducers,
    routing: routerReducer
  });

  return createStore(
    rootReducer,
    initialState,
    compose(applyMiddleware(...middleware), ...enhancers)
  );
}
