﻿import { processorConstants } from '../constants/processorConstants';
const initialState = { processors: [], processorInfo: null }

export function reducer(state = initialState, action) {
    switch (action.type) {
        case processorConstants.FETCH_PROCESSOR_REQUEST:
            return {
                fetching: true,
                processorInfo: null
                //vendor: action.vendors
            };

        case processorConstants.FETCH_PROCESSORS_SUCCESS:
            return {
                fetched: true,
                processors: action.resp.content,
                processorInfo: null
            }

        case processorConstants.FETCH_PROCESSORS_FAILED:
            return {
                fetched: false,

            }
        case processorConstants.FETCH_PROCESSOR_INFO:
            return {
                processorInfo: action.resp.content
            }

        case processorConstants.FETCH_PROCESSOR_INFO_FAILED:
            return {
                processorInfo: null
            }

        default:
            return state;
    }
}

