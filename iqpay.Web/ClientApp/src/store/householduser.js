import { householdConstants } from '../constants/householdConstants';
const initialState = { houseHoldUsers: [], houseHoldUserInfo: null }

export function reducer(state = initialState, action) {
    switch (action.type) {
        case householdConstants.FETCH_DEALERS_REQUEST:
            return {
                fetching: true,
                houseHoldUserInfo: null
                //vendor: action.vendors
            };

        case householdConstants.FETCH_DEALERS_SUCCESS:
            return {
                fetched: true,
                houseHoldUsers: action.resp.content,
                houseHoldUserInfo: null
            }

        case householdConstants.SAVE_DEALER_FAILURE:
            return {
                fetched: false,
            }

        case householdConstants.FETCH_DEALERS_INFO_SUCCESS:
            return {
                houseHoldUserInfo: action.resp.content
            }

        case householdConstants.FETCH_DEALERS_INFO_FAILED:
            return {
                houseHoldUserInfo: null
            }

        default:
            return state;
    }
}