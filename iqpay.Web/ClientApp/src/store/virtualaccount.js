import { virtualAccountConstants } from '../constants/virtualAccountConstant';
const initialState = { virtualaccounts: [], virtualTransactions: []} 

export function reducer(state = initialState, action)
{
    switch (action.type) {
        case virtualAccountConstants.FETCH_VIRTUALACCOUNT_SUCCESS:
            return {
                fetching: true,
                virtualaccounts: action.resp.content
            };

            case virtualAccountConstants.FETCH_VIRTUALACCOUNT_FAILURE:
                return {
                    fetching: true,
                    virtualaccounts: []
                };
        
        case virtualAccountConstants.FETCH_VIRTUALTRANSACTION_SUCCESS:
            return {
                fetching: true,
                virtualTransactions: action.resp.content
            };

            case virtualAccountConstants.FETCH_VIRTUALTRANSACTION_FAILURE:
                return {
                    fetching: true,
                    virtualTransactions: []
                };

        default:
            return{
                virtualaccounts: []
            }
    }
}