﻿import { agentsConstants } from '../constants/agentsConstants';
const initialState = { dealers: [], dealerInfo: null }

export function reducer(state = initialState, action) {
    switch (action.type) {
        case agentsConstants.FETCH_DEALERS_REQUEST:
            return {
                fetching: true,
                dealerInfo: null
                //vendor: action.vendors
            };

        case agentsConstants.FETCH_DEALERS_SUCCESS:
            return {
                fetched: true,
                dealers: action.resp.content,
                dealerInfo: null,
                commissionRates: null
            }

        case agentsConstants.SAVE_DEALER_FAILURE:
            return {
                fetched: false,

            }

        case agentsConstants.FETCH_DEALERS_INFO_SUCCESS:
            return {
                dealerInfo: action.resp.content
            }

        case agentsConstants.FETCH_DEALERS_INFO_FAILED:
            return {
                dealerInfo: null
            }

        default:
            return state;
    }
}