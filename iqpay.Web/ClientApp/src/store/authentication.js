﻿import { userConstants } from '../constants/userConstants';
import { actionCreators } from './WeatherForecasts';
let currentUser = JSON.parse(localStorage.getItem("currentUser"));
const initialState = currentUser ? { loggedIn: true, currentUser, dashboard: null, profileInformation: null, users: [], reservedAccount: null, partnerRequests: [], requestInfo:{} } : {};

export function reducer(state = initialState, action) {
    switch (action.type) {
        case userConstants.LOGIN_REQUEST:
            return {
                loggingIn: true,
                user: action.user,
                wallets: null,
                walletInfo: null,
                users: null,
                reservedAccount: null                
            };

        case userConstants.FETCH_ADMINISTRATORS:
            return {
                loggingIn: true,
                users: action.details,
                wallets: null,
                walletInfo: null,
                reservedAccount: null
            }
        
        case userConstants.FETCH_ADMINISTRATORS:
            return {
                loggingIn: true,
                users: null,
                reservedAccount: null
            }

        case userConstants.FETCH_RESERVED_ACCOUNT:
            return{
                loggingIn: true,   
                loggedIn: state.loggedIn,             
                reservedAccount: action.details
            }

        case userConstants.FETCH_WALLET_BALANCES:
            return{
                loggingIn: true,   
                loggedIn: state.loggedIn,
                walletBalances: action.balances,
                reservedAccount: null
            }

        case userConstants.FETCH_WALLET_BALANCES_FAILUE:
            return{
                loggingIn: true,   
                loggedIn: state.loggedIn,
                walletBalances: [],
                reservedAccount: null
            }
        
        case userConstants.FETCH_RESERVED_ACCOUNT_FAILURE:
            return{
                loggingIn: true,
                loggedIn: state.loggedIn,
                reservedAccount: null
            }
        
        case userConstants.LOGIN_SUCCESS:
            return {
                loggedIn: true,
                user: action.user,
                wallets: null,
                walletInfo: null,
                reservedAccount: null
            };
        case userConstants.LOGIN_FAILURE:
            return {};
        case userConstants.LOGOUT:
            return {
                loggedIn:false,
                user: null,
                reservedAccount: null
            };
        case userConstants.FETCH_WALLET_ERROR:
            return {
                wallets: null,
                walletInfo: null,
                loggedIn: state.loggedIn,
                user: action.user,
                reservedAccount: null
            }
        case userConstants.GET_USER_INFO_SUCCESS:
            return {
                profileInformation: action.user,
                loggedIn: state.loggedIn
            }

        case userConstants.GET_USER_INFO_FAILURE:
            return {
                profileInformation: null,
                loggedIn: state.loggedIn,
                user: action.user,
            }

        
        case userConstants.FETCH_WALLET_SUCCESS:
            
            return {
                wallets: action.wallets,
                walletInfo: null,
                loggedIn: state.loggedIn,
                reservedAccount: null
            }        
        
        case userConstants.FETCH_WALLET_INFO_SUCCESS:

            return {                
                walletInfo: action.walletInfo,
                loggedIn: state.loggedIn,
                reservedAccount: null
            }


        case userConstants.WALLET_UPDATE_SUCCESS:
            return {
                loggedIn: state.loggedIn,
                reservedAccount: null
            }

        case userConstants.FETCH_DASHBOARD_DATA:
            return{
                dashboard: action.dashboard,
                loggedIn: state.loggedIn,
                reservedAccount: null
            }

        case userConstants.WALLET_UPDATE_FAILED:
            return  {
                loggedIn: state.loggedIn,
                reservedAccount: null
            }

        case userConstants.PARTNERSHIP_REQUEST_SUCCESS:
            return {
                partnerRequests: action.partnerRequests
            }
        case userConstants.PARTNERSHIP_REQUEST_INFO_SUCCESS:
            return{
                requestInfo: action.requestInfo
            }
        default:
            return state
    }
}