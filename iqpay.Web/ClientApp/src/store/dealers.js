﻿import { dealerConstants } from '../constants/dealerConstants';
const initialState = { dealers: [], dealerInfo: null, commissionRates: null, dealerChecklist: null}

export function reducer(state = initialState, action) {
    switch (action.type) {
        case dealerConstants.FETCH_DEALERS_REQUEST:
            return {
                fetching: true,
                dealerInfo: null
                //vendor: action.vendors
            };

        case dealerConstants.FETCH_DEALERS_SUCCESS:
            return {
                fetched: true,
                dealers: action.resp.content,
                dealerInfo: null,
                commissionRates: null
            }
        
        case dealerConstants.RESET_SERVICE_CHECKLIST:
        {
            return {
                commissionRates: null,
                dealerChecklist: null
            }
        }

        case dealerConstants.RESET_COMMISSION_RATE:
            return {
                commissionRates: null,
                dealerChecklist: null
            }

        case dealerConstants.FETCH_DEALER_CHECKLIST:
            return {
                commissionRate: null,
                dealerChecklist: action.resp.content
            }

        case dealerConstants.FETCH_COMMISSION_RATES_SUCCESS:
            return {
                commissionRates: action.resp.content
            }

        case dealerConstants.SAVE_DEALER_FAILURE:
            return {
                fetched: false,
            }

        case dealerConstants.FETCH_DEALERS_INFO_SUCCESS:
            return {
                dealerInfo: action.resp.content,
                commissionRates: null
            }

        case dealerConstants.FETCH_DEALERS_INFO_FAILED:
            return {
                dealerInfo: null,
                commissionRates: null
            }

        default:
            return state;
    }
}