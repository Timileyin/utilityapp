import React from 'react';
import { Route, Switch } from 'react-router';
import { Layout } from './components/common/Layout';
import FetchData from './components/FetchData';
import { BillsRecharge } from './components/billsRecharge';
import { PrivateRoute } from './routes/privateRoute';
import { AuthPage } from './components/authpage';
import {Profile} from './components/profile';
import {Dashboard} from './components/dashboard';
import { Wallets } from './components/wallets';
import { FundWallet } from './components/funding-wallets';
import { Vendors } from './components/vendors';
import { Services } from './components/services';
import { FundingCompleted } from './components/funding-completed';
import { WalletFundingRequest } from './components/walletFundingRequest';
import { WalletFundingRequestHistory } from './components/walletfundinghistory';
import { Processors } from './components/processors';
import { ConfirmVending } from './components/confirmVending';
import { Dealers } from './components/dealers';
import { VendSlip } from './components/slip';
import { SuperDealers } from './components/superdealers';
import { Agents } from './components/agents';
import { ConfirmEmail } from './components/confirmemail';
import {Transactions } from './components/transactions';
import {ForgotPassword} from './components/forgot-password';
import {NewPassword} from './components/newPassword';
import {CommissionAheadReports} from './components/commAheadReport';
import {MasterWallets} from './components/master-wallets';
import {ProcessorTransactionReport} from './components/processor-transaction-report';
import {ProcessorTransaction} from './components/processor-transactions'
import {NotFound} from './components/notfound';
import { WalletBalances } from './components/wallet-balances';
import { TransferMoney } from './components/transfermoney';
import {Settings} from './components/settings';
import { HouseholdUsers } from './components/householdusers';
import {SysadminList} from './components/sysadmin-list';
import {VirtualAccounts} from './components/virtualaccount';
import {VirtualTransactions} from './components/virtualTransactions';
import {RegisterRequest} from './components/register';
import {RegisterRequestList} from './components/registerRequestList';
import {VendorsCummBalance} from './components/wallet-balance-vendors';
import {OrderSummary} from './components/orders-summary';
import {PlacedOrders} from './components/orders';

export default () => (
    <Layout>
      <Switch>
        <PrivateRoute exact path="/" component={BillsRecharge} />
        <PrivateRoute exact path="/profile" component={Profile} />
        <PrivateRoute exact path="/dashboard" component={Dashboard} />
        <PrivateRoute exact path="/transactions" component={Transactions} />
        <PrivateRoute exact path="/admins" component={SysadminList} />
        <PrivateRoute exact path="/wallets" component={Wallets} />
        <PrivateRoute exact path="/dealers" component={Dealers} />
        <PrivateRoute exact path="/super-dealers" component={SuperDealers} />
        <PrivateRoute exact path="/agents" component={Agents} />
        <PrivateRoute exact path="/vendors" component={Vendors} />
        <PrivateRoute exact path="/processors" component={Processors} />
        <PrivateRoute exact path="/funding-summary/:txnref" component={FundWallet} />
        <PrivateRoute exact path="/funding-request" component={WalletFundingRequest} />
        <PrivateRoute exact path="/walletfundinghistory" component={WalletFundingRequestHistory} />
        <PrivateRoute exact path="/masterwallets" component={MasterWallets}/>
        <PrivateRoute exact path="/vendors/:vendorId/services" component={Services} />
        <PrivateRoute exact path="/payment-complete/:txnref" component={FundingCompleted} />
        <PrivateRoute exact path="/confirm-vending/:vendCode" component={ConfirmVending} />
        <PrivateRoute exact path="/slip/:vendCode" component={VendSlip} />
        <PrivateRoute exact path="/processor-vending-summary" component={ProcessorTransactionReport}/>
        <PrivateRoute exact path="/processor-vending/:processorId/:processorName" component={ProcessorTransaction} />
        <PrivateRoute exact path="/wallet-balances" component={WalletBalances}/>
        <PrivateRoute exact path="/money-transfer" component={TransferMoney} />
        <PrivateRoute exact path="/commAheadReport" component={CommissionAheadReports} />   
        <PrivateRoute exact path="/settings" component={Settings} />   
        <PrivateRoute exact path="/householdusers" component={HouseholdUsers} />
        <PrivateRoute exact path='/virtualaccounts' component={VirtualAccounts} />
        <PrivateRoute exact path='/virtualtransactions' component={VirtualTransactions} /> 
        <PrivateRoute exact path='/virtualtransactions/:dealerCode' component={VirtualTransactions} />  
        <PrivateRoute exact path="/partnersrequest" component={RegisterRequestList} />   
        <PrivateRoute exact path="/vendors-cumm-balance" component={VendorsCummBalance} />
        <PrivateRoute path="/order-summary/:vendCode" component={OrderSummary}/>
        <PrivateRoute exact path="/orders" component={PlacedOrders} />
        <Route path="/confirmation" component={ConfirmEmail} />
        <Route path="/newPassword" component={NewPassword} />
        <Route path="/forgot-password" component={ForgotPassword} />        
        <Route path="/funding-mobile/:txnref" component={FundWallet} />
        <Route path='/login' component={AuthPage} />             
        <Route path='/fetch-data/:startDateIndex?' component={FetchData} />
        <Route path='/billsandrecharge' component={BillsRecharge} />
        <Route path='/emailConfirmation' component={ConfirmEmail} />   
        <Route path='/register' component={RegisterRequest} />                  
        <Route component={NotFound}/>
       
      </Switch>
  </Layout>
);
