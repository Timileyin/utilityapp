﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Data.Entities.AuditTrail;
using iqpay.Data.Entities.Email;
using iqpay.Engine.Base.Entities;
using iqpay.Engine.Base.Repository.EmailRepository;
using iqpay.Engine.Repository.EmailRepository;
using iqpay.Repository.Users;
using iqpay.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace iqpay.Web.Controllers
{


    [Route("api/[controller]")]
    [ApiController]
    public class SuperDealersController : BaseController
    {

        private ISuperDealerRepository _superDealerRepository;
        private IEmailManager<EmailLog, EmailTemplate> _emailManager;
        public SuperDealersController(ISuperDealerRepository superDealerRepository, IMapper mapper, ILogger<SuperDealersController> logger,
            IEmailManager<EmailLog, EmailTemplate> emailManager,
            IAuditTrailManager<AuditTrail> auditTrailManager
            ):base()
        {

            _superDealerRepository = superDealerRepository;
            _mapper = mapper;
            _logger = logger;
            _emailManager = emailManager;
            _auditTrailManager = auditTrailManager;
        }

        /// <summary>
        ///Fetch super dealers under system admin
        /// </summary>
        /// <returns>A payload of the list of super dealer</returns>
        [Authorize(Roles = "SYSTEM ADMIN")]
        [HttpGet]
        public async Task<IActionResult> FetchSuperDealers()
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var superdealers = await _superDealerRepository.FetchSuperDealer();
            if (superdealers.Count > 0)
            {

                return PrepareResponse(System.Net.HttpStatusCode.OK, $"{superdealers.Count} agents found", false, _mapper.Map<List<AgentViewModel>>(superdealers));
            }

            return PrepareResponse(System.Net.HttpStatusCode.NotFound, "No super dealer found", true);

        }

        /// <summary>
        ///Fetch subdealers details
        /// </summary>
        /// <param name="id">sub dealers id</param>
        /// <returns>A payload of the sub dealer's info</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> FetchAgent(int id)
        {

            var agent = await _superDealerRepository.FetchSuperDealerDetails(id);
            if (agent != null)
            {
                return PrepareResponse(HttpStatusCode.OK, "Superdealer found", false, agent);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No super dealer found for that id", false);
        }

        /// <summary>
        ///Update superdealers details
        /// </summary>
        /// <param name="id">sub dealers id</param>
        /// <returns>A payload showing if updating superdealers was succwessful</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateSuperDealer(int id, DealerUpdateViewModel agent)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var existingAgent = await _superDealerRepository.FetchSuperDealerDetails(id);
            if (existingAgent != null)
            {
                existingAgent.Address = agent.Address;
                existingAgent.Email = agent.Email;
                existingAgent.PhoneNumber = agent.Phonenumber;
                existingAgent.Name = agent.Name;
                await _superDealerRepository.UpdateSuperDealer(existingAgent);

                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    ActionTaken = AuditAction.Update,
                    DateCreated = DateTime.Now,
                    Description = $"IQISUPERDEALER {existingAgent.UserName} has been updated successfully",
                    Entity = "Super dealer",
                    UserId = Convert.ToInt64(userId)
                    
                });

                return PrepareResponse(HttpStatusCode.OK, "Agents details has been updated successfully", false, agent);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No agent found for that id", true);
        }


        /// <summary>
        ///Deactivate superdealers
        /// </summary>
        /// <param name="id">super dealers id</param>
        /// <returns>A payload showing if deactivating superdealers was succwessful</returns>
        [HttpPut("{id}/deactivate")]
        public async Task<IActionResult> DeactivateSuperDealer(int id)
        {
            
            var existingDealer = await _superDealerRepository.FetchSuperDealerDetails(id);
            if (existingDealer != null)
            {
                existingDealer.LockoutEnabled = true;
                existingDealer.LockoutEnd = DateTime.MaxValue;
                await _superDealerRepository.UpdateSuperDealer(existingDealer);
                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    ActionTaken = AuditAction.Update,
                    DateCreated = DateTime.Now,
                    Description = $"IQISUPERDEALER {existingDealer.UserName} has been deactivated successfully",
                    Entity = "Super dealer",
                    UserId = Convert.ToInt64(userId)

                });
                return PrepareResponse(HttpStatusCode.OK, "Super dealer has been deactivated successfully", false);
            }
            return PrepareResponse(HttpStatusCode.NotFound, "The specified super dealer does not exist");

        }

        /// <summary>
        ///Activate superdealers
        /// </summary>
        /// <param name="id">super dealers id</param>
        /// <returns>A payload showing if activating superdealers was successful</returns>
        [HttpPut("{id}/activate")]
        public async Task<IActionResult> ActivateDealer(int id)
        {
            var existingAgent = await _superDealerRepository.FetchSuperDealerDetails(id);
            if (existingAgent != null)
            {
                existingAgent.LockoutEnd = null;
                await _superDealerRepository.UpdateSuperDealer(existingAgent);

                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    ActionTaken = AuditAction.Update,
                    DateCreated = DateTime.Now,
                    Description = $"IQISUPERDEALER {existingAgent.UserName} has been activated successfully",
                    Entity = "Super dealer",
                    UserId = Convert.ToInt64(userId)

                });
                return PrepareResponse(HttpStatusCode.OK, "Super dealer has been activated successfully", false);
            }
            return PrepareResponse(HttpStatusCode.NotFound, "The specified super dealer does not exist");
        }

        /// <summary>
        ///Remove superdealers
        /// </summary>
        /// <param name="id">super dealers id</param>
        /// <returns>A payload showing if removing superdealers was succwessful</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> RemoveDealer(int id)
        {
            var existingDealer = await _superDealerRepository.FetchSuperDealerDetails(id);
            if (existingDealer != null)
            {
                await _superDealerRepository.UpdateSuperDealer(existingDealer);

                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    ActionTaken = AuditAction.Delete,
                    DateCreated = DateTime.Now,
                    Description = $"IQISUPERDEALER {existingDealer.UserName} has been removed successfully",
                    Entity = "Super dealer",
                    UserId = Convert.ToInt64(userId)

                });

                return PrepareResponse(HttpStatusCode.OK, "Super dealer has been removed successfully", false, null);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No agent found for that id", true);
        }
    }
}
