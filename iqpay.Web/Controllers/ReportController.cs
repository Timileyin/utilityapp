using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Repository.Processors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using iqpay.Repository.Processors.ProcessorImplementations;
using iqpay.Repository.ServicesRepository;
using iqpay.Web.Models;
using iqpay.Data.Entities.Services;
using System.Net;
using iqpay.Engine.Repository.EmailRepository;
using iqpay.Data.Entities.AuditTrail;
using iqpay.Engine.Base.Entities;
using System.IdentityModel.Tokens.Jwt;
using iqpay.Data.UserManagement.Model;
using iqpay.Core.Utilities;
using System.Security.Claims;
using iqpay.Repository.Wallet;
using iqpay.Core.Utilities;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860
namespace iqpay.Web.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : BaseController
    {
        private readonly IVendImplementation _vend;
        private readonly IWalletRepository _walletRepo;
        private readonly IProcessorRepository _processorRepo;     

        public ReportController(IVendImplementation vend, IWalletRepository walletRepo, IMapper mapper, IProcessorRepository processorRepo)
        {
            _walletRepo = walletRepo;
            _vend = vend;
            _mapper = mapper;
            _processorRepo = processorRepo;
        }

        [HttpGet("profits")]
        public IActionResult FetchProfit()
        {
            var userRole = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role).Value;
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            if(userRole.Contains(RoleTypes.SYSADMIN.GetDescription())){
                var transactions = _vend.FetchVendingLog();
                return PrepareResponse(HttpStatusCode.OK, $"{transactions.Count()} returned", false, _mapper.Map<List<VendingModel>>(transactions));
            }else{
                var transactions = _vend.FetchVendingLog(Convert.ToInt64(userId));
                return PrepareResponse(HttpStatusCode.OK, $"{transactions.Count()} returned", false, _mapper.Map<List<VendingModel>>(transactions));
            }
            
        }

        [HttpGet("cummwalletbalance")]
        public async Task<IActionResult> FetchCummWalletBalance()
        {
            var cummWalletBalance = await _walletRepo.FetchVendorCummWalletBalance();
            return PrepareResponse(HttpStatusCode.OK, $"{cummWalletBalance.Count()} returned", false, cummWalletBalance);
        }

        [HttpGet("commission-ahead-profits")]
        public async Task<IActionResult> FetchCommissionAheadProfits(DateTime? startDate, DateTime? endDate)
        {
            var userRole = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role).Value;
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            if(userRole.Contains(RoleTypes.SYSADMIN.GetDescription())){
                if(endDate.HasValue)
                {
                    endDate = endDate.Value.AddDays(1);
                }
                var profitReport = await _walletRepo.FetchWalletTransaction(null, startDate, endDate);                
                return PrepareResponse(HttpStatusCode.OK, $"Commission Ahead Profit Transactions", false, _mapper.Map<List<CommissionAheadReport>>(profitReport));
            }else{
                var profitReport = await _walletRepo.FetchWalletTransaction(Convert.ToInt64(userId), startDate, endDate);
                return PrepareResponse(HttpStatusCode.OK, $"Commission Ahead Profit Transactions", false, _mapper.Map<List<CommissionAheadReport>>(profitReport));
            }
        }

        [HttpGet("wallet-balances")]
        public async Task<IActionResult> FetchAgentsWalletBalances()
        {
            var walletBalances = await _walletRepo.FetchWalletBalances();
            return PrepareResponse(HttpStatusCode.OK, "Wallet balances fetched", false, walletBalances);
        } 

        [HttpGet("processors-balances")]
        public async Task<IActionResult> FetchProcessorsBalances()
        {
            var processors = Enum.GetValues(typeof(ProcessorsEnum));
            var processorBalance = new List<ProcessorBalance>();
            foreach(var processor in processors)
            {
                var processorCode = ((ProcessorsEnum)processor).GetDescription();
                var processorName = await _processorRepo.GetProcessorByCode(processorCode);
                if(processorName != null){
                    var balance = _vend.FetchProcessorBalance((ProcessorsEnum)processor, out string message); 
                    if(!string.IsNullOrEmpty(message))
                    {
                        processorName.Name += "*";
                    }               
                    processorBalance.Add(new ProcessorBalance{
                        ProcessorId = processorName.Id,
                        Balance = balance,
                        ProcessorName = processorName.Name
                    });
                }                
            }

            return PrepareResponse(HttpStatusCode.OK, "Processors Balances Found", false, processorBalance);
        }

        [HttpGet("processors-vending-summary")]
        public async Task<IActionResult> FetchVendingByProcessors(DateTime? startDate, DateTime? endDate)
        {
            var processors = Enum.GetValues(typeof(ProcessorsEnum));
            var processorTransaction = new List<ProcessorVending>();
            foreach(var processor in processors)
            {
                var processorCode = ((ProcessorsEnum)processor).GetDescription();
                var theProcessor = await _processorRepo.GetProcessorByCode(processorCode);
                if(theProcessor != null){
                    if(endDate.HasValue)
                    {
                        endDate = endDate.Value.AddDays(1);
                    }
                    var vendingLogs = await _vend.FetchVendingLogsProcessedBy(theProcessor.Id, startDate, endDate);
                    processorTransaction.Add(new ProcessorVending{
                        ProcessorId = theProcessor.Id,
                        ProcessorName = theProcessor.Name,
                        TransactionCounts = vendingLogs.Count,
                        FailedCounts = vendingLogs.Count(x=>x.Status == Data.Entities.Payment.VendingStatusEnum.ProcessingFailed),
                        CancelledCounts = vendingLogs.Count(x=>x.Status == Data.Entities.Payment.VendingStatusEnum.Cancelled),
                        SuccessfulCount = vendingLogs.Count(x=>x.Status == Data.Entities.Payment.VendingStatusEnum.Completed),
                        TotalProfits = vendingLogs.Sum(x=>x.IQPAYProfit),

                        TransactionValue = vendingLogs.Sum(x=>x.Amount),
                    });
                }
                
            }

            return PrepareResponse(HttpStatusCode.OK, "Processors Balances Found", false, processorTransaction);
        }

        [HttpGet("processors-vending/{processorId}")]
        public async Task<IActionResult> FetchVendingByProcessor(long processorId, DateTime? startDate, DateTime? endDate)
        {
            var theProcessor = await _processorRepo.GetProcessor(processorId);
            if(theProcessor == null){
                return PrepareResponse(HttpStatusCode.NotFound, "This is not a valid processor");
            }
            if(endDate.HasValue)
            {
                endDate = endDate.Value.AddDays(1);
            }
            var vendingLogs = await _vend.FetchVendingLogsProcessedBy(theProcessor.Id, startDate, endDate);
            return PrepareResponse(HttpStatusCode.OK, "Vending Logs returned", false, _mapper.Map<List<VendingReportModel>>(vendingLogs));
        }
    }
}