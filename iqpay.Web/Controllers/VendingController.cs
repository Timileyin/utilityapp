﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Core.Utilities;
using iqpay.Data.Entities.Services;
using iqpay.Repository.Processors.ProcessorImplementations;
using iqpay.Repository.ServicesRepository;
using iqpay.Repository.Wallet;
using iqpay.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.JsonWebTokens;
using iqpay.Data.UserManagement.Model;
using iqpay.Repository.Users;
using iqpay.Data.Entities.Payment;
using Microsoft.AspNetCore.Identity;
using iqpay.Repository.Processors;
using iqpay.Core.Utilities;
using Newtonsoft.Json;
using System.Linq;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace iqpay.Web.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class VendingController : BaseController
    {
        // Post: /<controller>/
        private readonly IServicesRepository _serviceRepository;
        private readonly IVendImplementation _vend;
        private readonly ICapriconRequests _capRequest;
        private readonly IWalletRepository _walletRepository;
        private readonly IDealerRepository _dealerRepo;
        private readonly IBuyPowerRequests _buyPowerRequest; 
        private readonly IProcessorRepository _processorRepo;
        private readonly IShagoRequests _shagoRequests;
        private readonly IVatebraRequests _vatRequests;
        private readonly IVatebraEKORequests _vatEKOREquests;
        private readonly IVatebraIbadanRequests _vatebraIbadanRequests;
        private readonly IGreyStoneRequests _greyStoneRequests;

        public VendingController(IGreyStoneRequests greyStoneRequests, IProcessorRepository processorRepo,IBuyPowerRequests buyPowerRequest, IShagoRequests shago, IVatebraIbadanRequests vatebraIbadanRequests, IWalletRepository walletRepository, IDealerRepository dealerRepo, IServicesRepository serviceRepository,IVatebraEKORequests vatEKOREquests, ICapriconRequests capRequest, IMapper mapper, IVendImplementation vend, UserManager<ApplicationUser> userManager, IVatebraRequests vatRequests)
        {
            _serviceRepository = serviceRepository;
            _mapper = mapper;
            _vend = vend;
            _capRequest = capRequest;
            _walletRepository = walletRepository;
            _dealerRepo = dealerRepo;
            _userManager = userManager;
            _buyPowerRequest = buyPowerRequest;
            _processorRepo = processorRepo;
            _shagoRequests = shago;            
            _vatRequests = vatRequests;
            _vatEKOREquests = vatEKOREquests;
            _vatebraIbadanRequests = vatebraIbadanRequests;
            _greyStoneRequests = greyStoneRequests;
        }

        [HttpGet("logs")]
        public IActionResult FetchTransactions(DateTime? startDate = null, DateTime? endDate = null)
        {
            var userRole = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role).Value;
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            if(userRole.Contains(RoleTypes.SYSADMIN.GetDescription())){   
                if(endDate.HasValue)
                {
                    endDate = endDate.Value.Date.AddDays(1);
                }             
                var transactions = _vend.FetchVendingLog(null, startDate, endDate);
                return PrepareResponse(HttpStatusCode.OK, $"{transactions.Count()} returned", false, _mapper.Map<List<VendingReportModel>>(transactions));
            }else{
                var transactions = _vend.FetchVendingLog(Convert.ToInt64(userId), startDate, endDate);
                return PrepareResponse(HttpStatusCode.OK, $"{transactions.Count()} returned", false, _mapper.Map<List<VendingReportModel>>(transactions));
            }
            
        }


        [HttpPut("{vendingCode}/updateorders/{status}")]
        public async Task<IActionResult> UpdateOrders(string vendingCode, int status)
        {
            var vendingDetails = _serviceRepository.FetchVending(vendingCode);
            if(vendingDetails != null)
            {
                vendingDetails.Status = (VendingStatusEnum)status;
                await _serviceRepository.UpdateVendDetails(vendingDetails);
                return PrepareResponse(HttpStatusCode.OK, "Order status udated successfully", false, null);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "Order not found", true);
        }

        [HttpGet("{vendingCode}")]
        public IActionResult FetchVendingDetails(string vendingCode)
        {
            var vendingDetails = _serviceRepository.FetchVending(vendingCode);
            if(vendingDetails != null)
            {
                if(vendingDetails.Service.Vendor.VendorName.Contains("IKEDC"))
                {
                    vendingDetails.Service.Vendor.VendorName = "IKEDC - IKEJA";
                }
                var vendingData = _mapper.Map<VendingModel>(vendingDetails);
                vendingData.Pins = new List<Models.EPin>();
                
                if(vendingDetails.Status == VendingStatusEnum.Completed && vendingDetails.Processor != null)
                {
                    if(!string.IsNullOrEmpty(vendingDetails.ProcessorsPayLoad))
                    {
                        if(vendingDetails.Processor.Code == ProcessorsEnum.Capricorn.GetDescription())
                        {
                            var capOutput = JsonConvert.DeserializeObject<CapriconResponse<TransactionSuccessful>>(vendingDetails.ProcessorsPayLoad);                
                            if(vendingDetails.Service.Type == ServiceType.Electricity)
                            {                                
                                vendingData.Pin = capOutput.Data.TokenCode;
                                vendingData.Quantity = capOutput.Data.AmountOfPower;
                            } 
                            if(vendingDetails.Service.Type == ServiceType.EPins)
                            {
                                foreach(var pin in capOutput.Data.Pins)
                                {
                                    vendingData.Pins.Add(new Models.EPin{
                                        Pin = pin.Pin,
                                        SerialNumber = pin.SerialNumber
                                    });
                                }                                
                            }                                                     
                        }
                        else if(vendingDetails.Processor.Code == ProcessorsEnum.BUYPOWER.GetDescription())
                        {
                            var bpOutput = JsonConvert.DeserializeObject<BuyPowerVendResponse>(vendingDetails.ProcessorsPayLoad);
                            vendingData.Pin = bpOutput.Data.Token;
                            vendingData.Quantity = bpOutput.Data.Units;
                        }else if(vendingDetails.Processor.Code == ProcessorsEnum.GRAYSTONE.GetDescription())
                        {
                            var greyOutput = JsonConvert.DeserializeObject<GreyStoneVendResponse>(vendingDetails.ProcessorsPayLoad);
                            vendingData.Pin = greyOutput.Value;
                            vendingData.Quantity = greyOutput.Units;
                        }
                        else if(vendingDetails.Processor.Code == ProcessorsEnum.SHAGO.GetDescription())
                        {
                            var shagOutput = JsonConvert.DeserializeObject<ShagoVendResponse>(vendingDetails.ProcessorsPayLoad);                            
                            vendingData.Pin = shagOutput.Token;
                            vendingData.Quantity = shagOutput.Unit;                            
                        }
                        else if(vendingDetails.Processor.Code == ProcessorsEnum.GiftEdge.GetDescription()){
        
                            if(vendingDetails.ProcessorsPayLoad.Contains("|Value") && vendingDetails.ProcessorsPayLoad.Contains("|CreditToken")){
                                var splitResponse = vendingDetails.ProcessorsPayLoad.Replace(". Thank you for using the service. Ikeja Electric.\"", "").Split('|');
                                var pinPortion = splitResponse[1];
                                var valuePortion = splitResponse[5];                                    
                                var pin = pinPortion.Split(":")[1];
                                var value = valuePortion.Split(":")[1];                                
                                vendingData.Pin = pin;
                                vendingData.Quantity = Convert.ToDecimal(value) == 0 ? "N/A" : value;
                            }                                                           
                        }else if(vendingDetails.Processor.Code == ProcessorsEnum.VATEBRAEKO.GetDescription())
                        {
                            var splitResponse = vendingDetails.ProcessorsPayLoad.Replace(". Thank you for using the service. EKEDC\"", "").Split('|');
                                var pinPortion = splitResponse[1];
                                var valuePortion = splitResponse[5];                                    
                                var pin = pinPortion.Split(":")[1];
                                var value = valuePortion.Split(":")[1];                                
                                vendingData.Pin = pin;
                                vendingData.Quantity = Convert.ToDecimal(value) == 0 ? "N/A" : value;
                        }else if(vendingDetails.Processor.Code == ProcessorsEnum.VATEBRAIBADAN.GetDescription())
                        {
                            var splitResponse = vendingDetails.ProcessorsPayLoad.Replace(". Thank you for using the service. IBEDC.\"", "").Split('|');
                                var pinPortion = splitResponse[1];
                                var valuePortion = splitResponse[5];                                    
                                var pin = pinPortion.Split(":")[1];
                                var value = valuePortion.Split(":")[1];                                
                                vendingData.Pin = pin;
                                vendingData.Quantity = Convert.ToDecimal(value) == 0 ? "N/A" : value;
                        }                        
                    }
                }      
                vendingData.UnitType = vendingDetails.Service.UnitType; 
                vendingData.DeliveryAddressLine2 = vendingDetails.AccountAddressLine2;                         
                return PrepareResponse(HttpStatusCode.OK, "Vending resource found", false, vendingData);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "The specified resource could not be found");
        }

        [Authorize(Roles = "SUPER DEALER,DEALER,AGENT,REGULAR,SYSTEM ADMIN")]
        [HttpPost("complete")]
        public async Task<IActionResult> CompleteVending(CompleteVendingModel model)
        {
            
            var vendingCode = model.VendingCode;
            var vendingDetails = _serviceRepository.FetchVending(vendingCode);
            var service = vendingDetails.Service;
            decimal vendorCommission; 
            var servicewallet = await _walletRepository.FetchServiceWallet(vendingDetails.UserId, service.Id);
            if(service.Vendor.IsCommissionAhead)
            {
                vendorCommission = (service.Vendor.Commission * vendingDetails.Amount)/100;
                servicewallet = await _walletRepository.FetchVendorWallet(vendingDetails.UserId, service.Vendor.Id);
            }
            vendorCommission = (service.Commission * vendingDetails.Amount)/100;
            var baseWallet = await _walletRepository.FetchBaseWallet(vendingDetails.UserId);
            #region Debit Wallet
                        
            if(service.CommissionMode == CommissionMode.CommissionAhead)
            {                
                if(servicewallet == null)
                {                    
                    vendingDetails.Status = VendingStatusEnum.Cancelled;
                    await _serviceRepository.UpdateVendDetails(vendingDetails);
                    return PrepareResponse(HttpStatusCode.NotFound, "A wallet to vend the service from can not be found. Please create a wallet for this service");
                }

                if(servicewallet.Balance < vendingDetails.Amount)
                {                    
                    vendingDetails.Status = VendingStatusEnum.Cancelled;
                    vendingDetails.WalletBalance = servicewallet.Balance;
                    await _serviceRepository.UpdateVendDetails(vendingDetails);
                    return PrepareResponse(HttpStatusCode.PaymentRequired, "You do not have sufficient funds to complete this transaction. Please fund you wallet to proceed");                    
                }
                //await _walletRepository.DebitWallet(servicewallet.Id, vendingDetails.Amount);
            }
            else
            {
                
                if(baseWallet == null)
                {
                    vendingDetails.Status = VendingStatusEnum.Cancelled;
                    await _serviceRepository.UpdateVendDetails(vendingDetails);
                    return PrepareResponse(HttpStatusCode.NotFound, "A wallet to vend the service from can not be found. Please create a wallet for this service");
                }

                if(baseWallet.Balance < vendingDetails.Amount)
                {
                    vendingDetails.Status = VendingStatusEnum.Cancelled;
                    vendingDetails.WalletBalance = baseWallet.Balance;
                    vendingDetails.ProcessorsPayLoad = "Vending Cancelled because dealer doesn't have sufficient balance";
                    await _serviceRepository.UpdateVendDetails(vendingDetails);
                    return PrepareResponse(HttpStatusCode.PaymentRequired, "You do not have sufficient funds to complete this transaction. Please fund you wallet to proceed");                    
                }
            }
            #endregion
            
            if(vendingDetails != null)
            {
                var completed = _vend.VendProduct(vendingDetails.VendingCode);
                var walletbalance = 0.00M;
                if (completed.IsSuccessful)
                {
                    #region Debit Wallet
                    if(service.CommissionMode == CommissionMode.CommissionAhead)
                    {                        
                        await _walletRepository.DebitWallet(servicewallet.Id, vendingDetails.Amount);
                        walletbalance =  servicewallet.Balance; 
                    }else{
                        await _walletRepository.DebitWallet(baseWallet.Id, vendingDetails.Amount);
                        walletbalance =  baseWallet.Balance; 
                    }
                    #endregion  
                    var dealerInfo =  await _userManager.FindByIdAsync(vendingDetails.UserId.ToString());    
                    var vendorDetails = service.Vendor;
                    bool todayIsBonusDay = false;
                    if(!string.IsNullOrWhiteSpace(vendorDetails.HouseHoldBonusDates))
                    {
                        todayIsBonusDay = vendorDetails.HouseHoldBonusDates.Split(',').Contains(DateTime.Now.Day.ToString()); 
                    }                                 
                    if(vendorDetails.EnableHouseHoldBonus && todayIsBonusDay)
                    {                        
                        if(dealerInfo.AccountType == AccountType.REGULAR)
                        {
                            var houseHoldBonus = vendorDetails.HouseHoldBonusPercentage * vendingDetails.Amount;
                            if(houseHoldBonus > vendorDetails.HouseHoldBonusCap)
                            {
                                houseHoldBonus = vendorDetails.HouseHoldBonusCap;
                            }

                            var commissionWallet = await _walletRepository.FetchCommissionWallet(vendingDetails.UserId);
                            if(commissionWallet != null)
                            {
                                _walletRepository.CreditWallet(commissionWallet.Id, houseHoldBonus);
                            }

                        }
                    }
                    #region Commission Sharing Between Users 
                    if(service.CommissionMode == CommissionMode.TransactionBased)
                    {
                        
                        if(dealerInfo != null)
                        {
                            if(dealerInfo.AccountType == AccountType.REGULAR){
                                var commissionPercentage = vendingDetails.Service.CustomerCommission;
                                var commission = (commissionPercentage * vendingDetails.Amount)/100;
                                if(commission >= vendingDetails.Service.CustomerCommissionCap && vendingDetails.Service.CustomerCommissionCap.HasValue){
                                    commission = vendingDetails.Service.CustomerCommissionCap.Value;
                                }

                                var commissionWallet = await _walletRepository.FetchCommissionWallet(vendingDetails.UserId);
                                _walletRepository.CreditWallet(commissionWallet.Id, commission); 
                            }
                            else if(dealerInfo.AccountType == AccountType.SUPER_DEALER)
                            {

                                var dealerCommissionInfo = await _dealerRepo.FetchServiceCommission(dealerInfo.Id, dealerInfo.AccountType, vendingDetails.ServiceId);
                                var commissionPercentage = dealerCommissionInfo.Commission;
                                var commission = (commissionPercentage * vendingDetails.Amount)/100;
                                if(commission >= dealerCommissionInfo.CommissionCap && dealerCommissionInfo.CommissionCap.HasValue){
                                    commission = dealerCommissionInfo.CommissionCap.Value;                                
                                }

                                var commissionWallet = await _walletRepository.FetchCommissionWallet(vendingDetails.UserId);
                                _walletRepository.CreditWallet(commissionWallet.Id, commission); 
                                vendingDetails.SuperDealerCommission = commission;
                            }
                            else if(dealerInfo.AccountType == AccountType.DEALER)
                            {
                                var superDealer = await _userManager.FindByIdAsync(dealerInfo.SuperDealerId.ToString());
                                var dealerCommission = await _dealerRepo.FetchServiceCommission(dealerInfo.Id, dealerInfo.AccountType, vendingDetails.ServiceId);
                                var superDealerCommission = await _dealerRepo.FetchServiceCommission(superDealer.Id, superDealer.AccountType, vendingDetails.ServiceId);

                                var dealerCommissionPercentage = dealerCommission.Commission;
                                var commission = (dealerCommission.Commission * vendingDetails.Amount) / 100;
                                if(commission >= dealerCommission.CommissionCap && dealerCommission.CommissionCap.HasValue )
                                {
                                    commission = dealerCommission.CommissionCap.Value;
                                }
                               
                                var dealerCommissionWallet = await _walletRepository.FetchCommissionWallet(vendingDetails.UserId);
                                _walletRepository.CreditWallet(dealerCommissionWallet.Id, commission);
                                 vendingDetails.DealerCommission = commission;

                                var superDealerCommissionPercentage = superDealerCommission.Commission - dealerCommissionPercentage;
                                var superDealerCommissionWorth = (superDealerCommissionPercentage * vendingDetails.Amount ) /100;                                                                

                                var superDealerCommissionWallet = await _walletRepository.FetchCommissionWallet(superDealer.Id);
                                _walletRepository.CreditWallet(superDealerCommissionWallet.Id, superDealerCommissionWorth);  
                                vendingDetails.SuperDealerCommission = superDealerCommissionWorth;                          
                            }
                            else if(dealerInfo.AccountType == AccountType.AGENT)
                            {

                                var agentInfo = dealerInfo;
                                var agentCommission = await _dealerRepo.FetchServiceCommission(agentInfo.Id, agentInfo.AccountType, vendingDetails.ServiceId);
                                
                                var agentCommissionPercentage = agentCommission.Commission;
                                var agentCommissionWorth = (agentCommission.Commission * vendingDetails.Amount)/100;
                                if(agentCommissionWorth >= agentCommission.CommissionCap && agentCommission.CommissionCap.HasValue)
                                {
                                    agentCommissionWorth = agentCommission.CommissionCap.Value;
                                } 
                                
                                var agentCommissionWallet = await _walletRepository.FetchCommissionWallet(vendingDetails.UserId);
                                _walletRepository.CreditWallet(agentCommissionWallet.Id, agentCommissionWorth);
                                vendingDetails.AgentCommission = agentCommissionWorth;

                                if(dealerInfo.SuperDealerId.HasValue)
                                {                                    
                                    dealerInfo = await _userManager.FindByIdAsync(agentInfo.SuperDealerId.ToString());   
                                    if(dealerInfo != null){
                                         var dealerCommission = await _dealerRepo.FetchServiceCommission(dealerInfo.Id, dealerInfo.AccountType, vendingDetails.ServiceId);                                    
                                        var dealerCommissionPercentage = dealerCommission.Commission - agentCommission.Commission;                                    
                                        var commission = (dealerCommissionPercentage * vendingDetails.Amount) / 100;                                                        
                                        var dealerCommissionWallet = await _walletRepository.FetchCommissionWallet(dealerInfo.Id);
                                        if(dealerCommissionWallet != null){
                                            _walletRepository.CreditWallet(dealerCommissionWallet.Id, commission);    
                                        }
                                        
                                        if(dealerInfo.AccountType == AccountType.DEALER){
                                            vendingDetails.DealerCommission = commission;
                                        }else{
                                            vendingDetails.SuperDealerCommission = commission;
                                        }

                                        if(dealerInfo.SuperDealerId.HasValue)
                                        {
                                            var superDealer = await _userManager.FindByIdAsync(dealerInfo.SuperDealerId.ToString());
                                            var superDealerCommission = await _dealerRepo.FetchServiceCommission(superDealer.Id, superDealer.AccountType, vendingDetails.ServiceId);                                        
                                            var superDealerCommissionPercentage = superDealerCommission.Commission - dealerCommission.Commission;
                                            var superDealerCommissionWorth = (superDealerCommissionPercentage * vendingDetails.Amount ) /100;                                        
                                            var superDealerCommissionWallet = await _walletRepository.FetchCommissionWallet(superDealer.Id);
                                            _walletRepository.CreditWallet(superDealerCommissionWallet.Id, superDealerCommissionWorth); 
                                            vendingDetails.SuperDealerCommission = superDealerCommissionWorth;
                                        }
                                    }                                                                                                     
                                }
                            }
                        }
                    }
                    vendingDetails.IQPAYProfit = vendorCommission - vendingDetails.AgentCommission - vendingDetails.DealerCommission - vendingDetails.SuperDealerCommission;
                    if(vendingDetails.Status != VendingStatusEnum.OrderPlaced){
                        vendingDetails.Status = VendingStatusEnum.Completed;
                    }                    
                    vendingDetails.ProcessorId = completed.ProcessorId;
                    vendingDetails.WalletBalance = walletbalance;
                    
                    var processor = await _processorRepo.GetProcessor(completed.ProcessorId.Value);
                    if(processor != null)
                    {
                        var processorEnums = Enum.GetValues(typeof(ProcessorsEnum)).OfType<ProcessorsEnum>().ToList();
                        var processorEnumValue = processorEnums.FirstOrDefault(x=>x.GetDescription() == processor.Code);                        
                        var processorBalance = _vend.FetchProcessorBalance(processorEnumValue, out string message);                        
                        vendingDetails.ProcessorBalance = processorBalance;                                                
                    }                    
                    await _serviceRepository.UpdateVendDetails(vendingDetails);
                    #endregion
                    return PrepareResponse(HttpStatusCode.OK, $"Vending has been completed successfully", false, completed);
                }

                vendingDetails.Status = VendingStatusEnum.ProcessingFailed;
                vendingDetails.WalletBalance = walletbalance;
                vendingDetails.ProcessorId = completed.ProcessorId;

                await _serviceRepository.UpdateVendDetails(vendingDetails);
                return PrepareResponse(HttpStatusCode.BadGateway, "Vending has failed. Please try again in 15 minutes");
            }

            vendingDetails.Status = VendingStatusEnum.Cancelled;            
            await _serviceRepository.UpdateVendDetails(vendingDetails);
            return PrepareResponse(HttpStatusCode.NotFound, "The specified resource could not be found");
        }

        [Authorize(Roles = "SUPER DEALER,DEALER,AGENT,REGULAR,SYSTEM ADMIN")]
        [HttpPost("initiate")]
        public async Task<IActionResult> Initiate(VendingModel vendingModel)
        {

            var service = await _serviceRepository.FetchService(vendingModel.ServiceId);
            if((string.IsNullOrWhiteSpace(vendingModel.DeliveryAddressLine1) || string.IsNullOrWhiteSpace(vendingModel.AccountName) || string.IsNullOrWhiteSpace(vendingModel.CustomerNumber)) && service.Type == ServiceType.Food)
            {
                return PrepareResponse(HttpStatusCode.BadRequest, "Please supply complete parameters");
            }

            if(service.Type == ServiceType.Food && service.DeliveryType == DeliveryTypes.CollectionPoint && vendingModel.CollectionPoint == null)
            {
                return PrepareResponse(HttpStatusCode.BadRequest, "Please select a collection point");
            }

            if(service.Type == ServiceType.Food && service.DeliveryType == DeliveryTypes.Both && vendingModel.DeliveryOption == null)
            {
                return PrepareResponse(HttpStatusCode.BadRequest, "Please select a preferred delivery option");
            }

            if(service.Type == ServiceType.Food && ((vendingModel.NumberOfPins ?? 0) == 0) && !service.IsSharedProduct )
            {
                return PrepareResponse(HttpStatusCode.BadRequest, "Please supply complete parameters");
            }

            if(service != null)
            {
                string accountName = string.Empty;
                string accountAddress = string.Empty;

                if(!string.IsNullOrWhiteSpace(vendingModel.DeliveryAddressLine1)){
                    accountAddress = vendingModel.DeliveryAddressLine1;
                }
                if(!string.IsNullOrWhiteSpace(vendingModel.AccountName)){
                    accountName = vendingModel.AccountName;
                }

                if(service.IsSharedProduct)
                {
                    var sharedProductOrders = await _serviceRepository.FetchOrderCount(vendingModel.ServiceId);
                    if(sharedProductOrders >= service.MaxOrders || DateTime.Now >= service.LastOrderDate.Value.AddDays(1)){
                        return PrepareResponse(HttpStatusCode.BadGateway, "This product is out of stock");
                    }
                }
                                
                if (service.RequiresVerification)
                {
                    if(service.Type == Data.Entities.Services.ServiceType.CableTV || service.Type == Data.Entities.Services.ServiceType.Electricity)
                    {
                        var code = service.Vendor.VendorName;
                        if (service.Type == ServiceType.Electricity)
                        {
                            code = service.ProcessorCodes.FirstOrDefault(x=>x.Processor.Code == ProcessorsEnum.BUYPOWER.GetDescription())?.Code;
                            if(code == null){
                                if(service.ServiceName.Contains("Postpaid") && service.Vendor.VendorName.ToUpper().Contains("IKEDC"))
                                {
                                    var user = await _vatRequests.FetchUser(vendingModel.CustomerNumber);
                                    if(user.Status)
                                    {
                                        var message = user.Message.Split("&&");
                                        accountName = message[0].Trim();
                                        accountAddress = message[1].Trim();
                                    }else
                                    {
                                        return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                    }
                                    
                                }
                                else if(service.ServiceName.Contains("Prepaid") && service.Vendor.VendorName.ToUpper().Contains("IKEDC"))
                                {
                                    var user = await _vatRequests.FetchUser(vendingModel.CustomerNumber);
                                    if(user.Status)
                                    {
                                        var message = user.Message.Split("&&");
                                        accountName = message[0].Trim();
                                        accountAddress = message[1].Trim();
                                    }else
                                    {
                                        return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                    }
                                }
                                else if(service.ServiceName.Contains("Postpaid") && service.Vendor.VendorName.ToUpper().Contains("EKEDC"))
                                {
                                    var user = await _greyStoneRequests.FetchUser("1", "2", vendingModel.CustomerNumber);                                    
                                    accountName = user.CustomerName;
                                    accountAddress = user.CustomerAddress;
                                }
                                else if(service.ServiceName.Contains("Prepaid") && service.Vendor.VendorName.ToUpper().Contains("EKEDC"))
                                {
                                    var user = await _greyStoneRequests.FetchUser("1", "1", vendingModel.CustomerNumber);                                    
                                    accountName = user.CustomerName;
                                    accountAddress = user.CustomerAddress;
                                }
                                else if(service.ServiceName.Contains("Prepaid") && service.Vendor.VendorName.ToUpper().Contains("IBEDC")){
                                    var user = await _vatebraIbadanRequests.FetchUser(vendingModel.CustomerNumber);
                                    if(user.Status)
                                    {
                                        var message = user.Message.Split("&&");
                                        accountName = message[0].Trim();
                                        accountAddress = message[1].Trim();
                                    }else
                                    {
                                        return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                    }
                                }
                                else if(service.ServiceName.Contains("Postpaid") && service.Vendor.VendorName.ToUpper().Contains("IBEDC")){
                                    var user = await _vatebraIbadanRequests.FetchUser(vendingModel.CustomerNumber);
                                    if(user.Status)
                                    {
                                        var message = user.Message.Split("&&");
                                        accountName = message[0].Trim();
                                        accountAddress = message[1].Trim();
                                    }else
                                    {
                                        return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                    }
                                }
                                else if(service.ServiceName.Contains("Prepaid") && service.Vendor.VendorName.ToUpper().Contains("JED")){
                                    code = "JOS-Prepaid";
                                    var codeArr = code.Split("-"); 
                                    var accountDetails = await _buyPowerRequest.VerifyAccount(vendingModel.CustomerNumber, codeArr[0], codeArr[1], false);;
                                    if (accountDetails == null)
                                    {
                                        return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                    }
                                    accountName = accountDetails.Name;
                                    accountAddress = accountDetails.Address;
                                }else if(service.ServiceName.Contains("Postpaid") && service.Vendor.VendorName.ToUpper().Contains("JED")){
                                    code = "JOS-Postpaid";
                                    var codeArr = code.Split("-"); 
                                    var accountDetails = await _buyPowerRequest.VerifyAccount(vendingModel.CustomerNumber, codeArr[0], codeArr[1], false);;
                                    if (accountDetails == null)
                                    {
                                        return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                    }
                                    accountName = accountDetails.Name;
                                    accountAddress = accountDetails.Address;
                                }else if(service.ServiceName.Contains("Postaid") && service.Vendor.VendorName.ToUpper().Contains("KAEDC")){
                                    code = "KADUNA-Prepaid";
                                    var codeArr = code.Split("-"); 
                                    var accountDetails = await _buyPowerRequest.VerifyAccount(vendingModel.CustomerNumber, codeArr[0], codeArr[1], false);;
                                    if (accountDetails == null)
                                    {
                                        return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                    }
                                    accountName = accountDetails.Name;
                                    accountAddress = accountDetails.Address;
                                }else if(service.ServiceName.Contains("Prepaid") && service.Vendor.VendorName.ToUpper().Contains("KAEDC")){
                                    code = "KADUNA-Postpaid";
                                    var codeArr = code.Split("-"); 
                                    var accountDetails = await _buyPowerRequest.VerifyAccount(vendingModel.CustomerNumber, codeArr[0], codeArr[1], false);;
                                    if (accountDetails == null)
                                    {
                                        return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                    }
                                    accountName = accountDetails.Name;
                                    accountAddress = accountDetails.Address;
                                }else if(service.ServiceName.Contains("Postpaid") && service.Vendor.VendorName.ToUpper().Contains("KEDCO")){
                                    code = "KANO-Postpaid";
                                    var codeArr = code.Split("-"); 
                                    var accountDetails = await _buyPowerRequest.VerifyAccount(vendingModel.CustomerNumber, codeArr[0], codeArr[1], false);;
                                    if (accountDetails == null)
                                    {
                                        return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                    }
                                    accountName = accountDetails.Name;
                                    accountAddress = accountDetails.Address;
                                }else if(service.ServiceName.Contains("Prepaid") && service.Vendor.VendorName.ToUpper().Contains("KEDCO")){
                                    code = "KANO-Prepaid";
                                    var codeArr = code.Split("-"); 
                                    var accountDetails = await _buyPowerRequest.VerifyAccount(vendingModel.CustomerNumber, codeArr[0], codeArr[1], false);;
                                    if (accountDetails == null)
                                    {
                                        return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                    }
                                    accountName = accountDetails.Name;
                                    accountAddress = accountDetails.Address;
                                }else if(service.ServiceName.Contains("Prepaid") && service.Vendor.VendorName.ToUpper().Contains("PHED")){
                                    code = "PHEDC-PREPAID";
                                    var accountDetails = await _shagoRequests.VerifyAccount(vendingModel.CustomerNumber, code);
                                    if(accountDetails == null)
                                    {
                                        return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                    }
                                    accountName = accountDetails.CustomerName;
                                    accountAddress = accountDetails.CustomerAddress;
                                    
                                }else if(service.ServiceName.Contains("Postpaid") && service.Vendor.VendorName.ToUpper().Contains("PHED")){
                                    code = "PHEDC-POSTPAID";
                                    var accountDetails = await _shagoRequests.VerifyAccount(vendingModel.CustomerNumber, code);
                                    if(accountDetails == null)
                                    {
                                        return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                    }
                                    accountName = accountDetails.CustomerName;
                                    accountAddress = accountDetails.CustomerAddress;
                                }else if(service.ServiceName.Contains("Postpaid") && service.Vendor.VendorName.ToUpper().Contains("AEDC")){
                                    code = "AEDC-POSTPAID";
                                    var accountDetails = await _shagoRequests.VerifyAccount(vendingModel.CustomerNumber, code);
                                    if(accountDetails == null)
                                    {
                                        return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                    }
                                    accountName = accountDetails.CustomerName;
                                    accountAddress = accountDetails.CustomerAddress;
                                }else if(service.ServiceName.Contains("Prepaid") && service.Vendor.VendorName.ToUpper().Contains("AEDC")){
                                    code = "AEDC-PREPAID";
                                    var accountDetails = await _shagoRequests.VerifyAccount(vendingModel.CustomerNumber, code);
                                    if(accountDetails == null)
                                    {
                                        return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                    }
                                    accountName = accountDetails.CustomerName;
                                    accountAddress = accountDetails.CustomerAddress;
                                }else if(service.ServiceName.Contains("Postpaid") && service.Vendor.VendorName.ToUpper().Contains("EEDC")){                                    
                                    var user = await _greyStoneRequests.FetchUser("3", "2", vendingModel.CustomerNumber);                                    
                                    accountName = user.CustomerName;
                                    accountAddress = user.CustomerAddress;
                                }else if(service.ServiceName.Contains("Prepaid") && service.Vendor.VendorName.ToUpper().Contains("EEDC")){
                                    var user = await _greyStoneRequests.FetchUser("3", "1", vendingModel.CustomerNumber);                                    
                                    accountName = user.CustomerName;
                                    accountAddress = user.CustomerAddress;
                                }
                            } else{
                                var codeArr = code.Split("-"); 
                                var accountDetails = await _buyPowerRequest.VerifyAccount(vendingModel.CustomerNumber, codeArr[0], codeArr[1], false);;
                                if (accountDetails == null)
                                {
                                    return PrepareResponse(HttpStatusCode.BadGateway, "Account number could not be verified");
                                }
                                accountName = accountDetails.Name;
                                accountAddress = accountDetails.Address;
                            }                                                       
                        }else{

                            code = service.Vendor.VendorName;
                            if(!string.IsNullOrEmpty(code))
                            {
                                 var VerifyAccount = await _capRequest.VerifyAccount(vendingModel.CustomerNumber, code);
                                 accountName = VerifyAccount.User.Name;
                                 accountAddress = VerifyAccount.User.Address;
                            }
                           
                        }                                                                                                                             
                    }                    
                }
                
                if(string.IsNullOrWhiteSpace(accountName)) accountName = "N/A";
                if(string.IsNullOrWhiteSpace(accountAddress)) accountAddress = "N/A";

                if(service.DeliveryType.HasValue){
                    var deliveryFee = 0.00M;
                    if(vendingModel.DeliveryOption == DeliveryOption.DeliveryCourier){
                        deliveryFee = (service.FlatDeliveryFee ?? 0.00M);                    
                        if(vendingModel.NumberOfPins > service.FlatDeliveryFeeCap)
                        {
                            deliveryFee = (decimal)(vendingModel.NumberOfPins * service.FlatDeliveryFeeBeyondCap.Value);
                        }

                    }else //if (vendingModel.DeliveryOption == DeliveryOption.CollectionPoint){
                    {
                        deliveryFee = (service.FlatCollectionFee ?? 0.00M);                    
                        if(vendingModel.NumberOfPins > service.FlatCollectionFeeCap)
                        {
                            deliveryFee = (decimal)(vendingModel.NumberOfPins * service.FlatCollectionFeeBeyondCap.Value);
                        }
                    }                    
                    vendingModel.DeliveryFee = deliveryFee;
                }
                
                if(((service.isFixedPrice && service.Price == vendingModel.Amount) && service.Price == vendingModel.Amount) || !service.isFixedPrice || (vendingModel.NumberOfPins.HasValue && vendingModel.NumberOfPins.Value * service.Price == vendingModel.Amount))
                {
                    var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                    var vendingData = _serviceRepository.InitiateVendService(vendingModel.ServiceId, vendingModel.Amount,vendingModel.CustomerNumber, Convert.ToInt64(userId), accountName, accountAddress,vendingModel.DeliveryAddressLine2, vendingModel.CollectionPoint, vendingModel.NumberOfPins, vendingModel.DeliveryOption, vendingModel.DeliveryFee, vendingModel.State, vendingModel.LGA);
                    var vendingResponse = _mapper.Map<VendingModel>(vendingData);    
                    vendingResponse.UnitType = service.UnitType;                
                    return PrepareResponse(HttpStatusCode.OK, "Vending has been initaited successfully", false, vendingResponse);
                }

                return PrepareResponse(HttpStatusCode.PreconditionFailed, "Please provide an exact amount for the service", true, null);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "This service you are vending doesn't exist", true, null);
        }       
    }
}
