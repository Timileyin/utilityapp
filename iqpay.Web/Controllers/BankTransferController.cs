using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Core.Utilities;
using iqpay.Data.Entities.AuditTrail;
using iqpay.Data.Entities.Payment;
using iqpay.Engine.Base.Entities;
using iqpay.Engine.Repository.EmailRepository;
using iqpay.Repository.Wallet;
using iqpay.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using iqpay.Repository.Monnify.Models;
using iqpay.Repository.Monnify;
using iqpay.Repository.Rubies;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Identity;
using iqpay.Data.UserManagement.Model;
using System.Globalization;
using iqpay.Repository.Rubies.Models;
using iqpay.Repository.TransactionRepository;
using System.Security.Claims;


namespace iqpay.Web.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BankTransferController : BaseController
    {
        private readonly IWalletRepository _walletRepository;
        private readonly IRubiesRequests _rubiesRequest;
        private readonly ISendMoneyRepo _sendMoneyRepo;
        private readonly decimal _fee;

        public BankTransferController(UserManager<ApplicationUser> userMgr, IConfiguration configuration, ISendMoneyRepo sendMoneyRepo, IWalletRepository walletRepository, IRubiesRequests rubiesReq, IMapper mapper)
        {
            _walletRepository = walletRepository;
            _rubiesRequest = rubiesReq;
            _userManager = userMgr;
            _mapper = mapper;
            _sendMoneyRepo = sendMoneyRepo;
            _configuration = configuration;
            _fee = Convert.ToDecimal(_configuration["Rubies:TransferFee"]);
        }

        [HttpGet("fetchBanks")]
        public async Task<IActionResult> FetchBanks()
        {   
            var rubiesResp = await _rubiesRequest.BankLookup();         
            var banklist = _mapper.Map<List<BankDataModel>>(rubiesResp.Banklist);
            return PrepareResponse(HttpStatusCode.OK, "Bank list returned", false, banklist);                              
        }

        private string GenerateTransactionRef()
        {
            var txnRef = Utilities.GenerateRandomString();
            while(_sendMoneyRepo.GetSendMoneyLog(txnRef) != null)
            {
                txnRef = Utilities.GenerateRandomString(); 
            }

            return txnRef;
        }

        [HttpPost("transfer")]
        public async Task<IActionResult> DoTransfer(BankTransferModel transferModel)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;  
            var user =  await _userManager.FindByIdAsync(userId);        
            var baseWallet = await _walletRepository.FetchBaseWallet(Convert.ToInt64(userId));
            if(baseWallet != null)
            {
                if(baseWallet.Balance >= (transferModel.Amount + _fee))
                {
                    var txnRef = GenerateTransactionRef();
                    var sendMoney = await _sendMoneyRepo.CreateLog(txnRef, transferModel.Amount, transferModel.BankAccountNumber, transferModel.AccountName, transferModel.BankName, transferModel.Narration, Convert.ToInt64(userId));
                    var transfer = await _rubiesRequest.FundTransfer(txnRef,transferModel.Amount, transferModel.Narration, transferModel.AccountName, transferModel.BankName, $"{user.UserName}/{user.Name}" , transferModel.BankAccountNumber, transferModel.BankCode);
                    if(transfer.Responsecode == "00")
                    {
                        var debitAmount = transferModel.Amount + _fee;
                        sendMoney.Fee = _fee;
                        sendMoney.Status = SendMoneyEnum.Completed;
                        sendMoney.TransferResponse = JsonConvert.SerializeObject(transfer);
                        sendMoney.DateCompleted = DateTime.Now;
                        sendMoney.Bank = transfer.Bankname;
                        sendMoney.Narration = transferModel.Narration;
                        sendMoney.WalletBalance = baseWallet.Balance - debitAmount;
                        await _sendMoneyRepo.UpdateLog(sendMoney);                                                  
                        await _walletRepository.DebitWallet(baseWallet.Id, debitAmount);       
                        return PrepareResponse(HttpStatusCode.OK, "Bank Transfer has been completed successfully", false);       
                    }
                    return PrepareResponse(HttpStatusCode.BadGateway, transfer.Responsemessage, true);
                }

            }
            return PrepareResponse(HttpStatusCode.PreconditionFailed, "You do not have sufficient funds to complete this transfer");            
        }        

        [HttpGet("")]
        public async Task<IActionResult> FetchTransfers(DateTime? startDate, DateTime? endDate)
        {
            var userRole = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role).Value;
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var logs = new List<SendMoneyLog>();
            if(endDate.HasValue)
            {
                endDate = endDate.Value.AddDays(1);                
            }
            if(userRole.Contains(RoleTypes.SYSADMIN.GetDescription()))
            {
                logs = await _sendMoneyRepo.FetchLogs(null, startDate, endDate);
            }
            else
            {
                logs = await _sendMoneyRepo.FetchLogs(Convert.ToInt64(userId),startDate, endDate);
            } 

            return PrepareResponse(HttpStatusCode.OK, "Bank transfer logs returned", false, _mapper.Map<List<BankTransferModel>>(logs));
        }

        [HttpGet("{txnref}")]
        public async Task<IActionResult> GetTransfer(string txnref)
        {
            var transferLog = _sendMoneyRepo.GetSendMoneyLog(txnref);
            if(transferLog != null)
            {
                return PrepareResponse(HttpStatusCode.OK, "Bank transfer log found", false, _mapper.Map<BankTransferModel>(transferLog));            
            }
            return PrepareResponse(HttpStatusCode.NotFound, "Bank transfer log not found");            
        }

        [HttpGet("bankenquiry")]
        public async Task<IActionResult> NameEnquiry(NameEnquiryModel model)
        {
            var nameEnquiry = await _rubiesRequest.NameEnquiry(model.BankAccountNumber, model.BankCode);
            return PrepareResponse(HttpStatusCode.OK, "Name enquiry successful", false,  new NameEnquiryModel{
                 BankAccountNumber = nameEnquiry.Accountnumber, AccountName = nameEnquiry.Accountname });  
        }
    
    }

}