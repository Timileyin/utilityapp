using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Core.Utilities;
using iqpay.Data.Entities.AuditTrail;
using iqpay.Data.Entities.Payment;
using iqpay.Engine.Base.Entities;
using iqpay.Engine.Repository.EmailRepository;
using iqpay.Repository.Wallet;
using iqpay.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using iqpay.Repository.Monnify.Models;
using iqpay.Repository.Monnify;
using iqpay.Repository.Rubies;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Identity;
using iqpay.Data.UserManagement.Model;
using System.Globalization;

namespace iqpay.Web.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class WalletsController : BaseController
    {
        private readonly IWalletRepository _walletRepository;
        private readonly IMonnifyRequests _monify;
        private readonly IRubiesRequests _rubiesRequest;
        public WalletsController(UserManager<ApplicationUser> userManager,IRubiesRequests rubiesRequest, ILogger<WalletsController> logger, IWalletRepository walletRepository, IMonnifyRequests monify ,IMapper mapper, IAuditTrailManager<AuditTrail> auditrailManager, IConfiguration config )
        {
            _walletRepository = walletRepository;
            _mapper = mapper;
            _auditTrailManager = auditrailManager;
            _configuration = config;
            _monify = monify;
            _logger = logger;
            _userManager = userManager;
            _rubiesRequest = rubiesRequest;
        }
        
        [HttpGet]
        public async Task<IActionResult> FetchWallets()
        {
            var wallets =  _mapper.Map<List<WalletModel>>(await _walletRepository.FetchWallets());
            if(wallets.Count > 0)
            {
                return PrepareResponse(HttpStatusCode.OK, $"{wallets.Count()} wallets returned", false, wallets);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "Wallets not found", true, null);
        }

        /// <summary>
        /// Create Wallet.
        /// </summary>
        /// <returns>Helps to create a wallet base on the mode of commission mode</returns>
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateWallet(WalletModel wallet)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var newWallet = _mapper.Map<Wallet>(wallet);
            newWallet.UserId = Convert.ToInt64(userId);
            newWallet.Type = WalletType.Transaction;
            var walletCreated = await _walletRepository.CreateWallet(newWallet);

            await _auditTrailManager.AddAuditTrail(new AuditTrail
            {
                UserId = Convert.ToInt64(userId),
                Description = "Wallet was created successfully",
                ActionTaken = AuditAction.Create,
                Entity = "Wallet",
            });

            if (walletCreated != null)
            {
                return PrepareResponse(HttpStatusCode.OK, "Wallets has been created successfully", false, _mapper.Map<WalletModel>(walletCreated));
            }

            return PrepareResponse(HttpStatusCode.InternalServerError, "Error occurred while creating wallet. Please try again");
        }


        


        /// <summary>
        /// Update Wallet.
        /// </summary>
        /// <returns>Helps to update a wallet base on the mode of commission mode</returns>
        [Authorize]
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateWallet(long id, WalletModel wallet)
        {
            var walletToUpdate = await _walletRepository.FetchWallet(id);
            if(walletToUpdate != null)
            {
                var newWallet = _mapper.Map<Wallet>(wallet);
                if (newWallet.Balance > 0.00M && newWallet.CommissionMode != walletToUpdate.CommissionMode)
                {
                    return PrepareResponse(HttpStatusCode.PreconditionFailed, "Please empty this wallet to change the commission mode.");
                }
                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                
                newWallet.UserId = Convert.ToInt64(userId);
                var walletUpdated = await _walletRepository.UpdateWallet(newWallet);
                if (walletUpdated)
                {
                    return PrepareResponse(HttpStatusCode.OK, "Wallets has been updated successfully", false);
                }

                return PrepareResponse(HttpStatusCode.InternalServerError, "Error occurred while creating wallet. Please try again");
            }
            else
            {
                return PrepareResponse(HttpStatusCode.NotFound, "The specified wallet could not be found");
            }
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> FetchWallet(long id)
        {
            var wallet = _mapper.Map<WalletModel>(await _walletRepository.FetchWallet(id));
            if (wallet != null)
            {
                return PrepareResponse(HttpStatusCode.OK, $"Wallet returned", false, wallet);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "Wallet not found", true, null);
        }

        [Authorize]
        [HttpPost("{walletId}/initiate-autofunding")]
        public async Task<IActionResult> InitiateAutoFunding(long walletId, FundingRequest request)
        {
            if (walletId == request.WalletId)
            {
                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                var wallet = await _walletRepository.FetchWallet(walletId);
                if (wallet != null)
                {
                    if(wallet.UserId.ToString() == userId)
                    {
                        var theRequest = _mapper.Map<FundWalletRequest>(request);
                        theRequest.InitiatedById = wallet.UserId;

                        var requested = await _walletRepository.InitiateAutoFundingRequest(theRequest);
                        if (!string.IsNullOrEmpty(requested))
                        {
                            //var encKey = _configuration.GetValue<string>("EncryptionKey");
                            return PrepareResponse(HttpStatusCode.OK, "Funding request has been initiated succeessfully. Please follow the instruction on the next page", false, new { txnRef = requested} );
                        }
                        return PrepareResponse(HttpStatusCode.InternalServerError, "You request could not be processed now. Please try agian later");
                    }
                }
            }
            return PrepareResponse(HttpStatusCode.Conflict, "Wallet Id mismatach");
        }

        [AllowAnonymous]
        [HttpPost("rubies-fund/{dealerCode}")]
        public async Task<IActionResult> RubiesFund(string dealerCode, RubiesPayLoad payLoad)
        {
            if(payLoad != null)
            {
                if(!string.IsNullOrWhiteSpace(payLoad.Paymentreference))
                {
                    var payLoadStr = JsonConvert.SerializeObject(payLoad);
                    var transfer = await _walletRepository.FetchRubiesTransfer(payLoad.Paymentreference);
                    if(transfer?.IsProcessed == true)
                    {
                        return PrepareResponse(HttpStatusCode.Conflict, "This transaction has been processed already");
                    }
                    var rubiesPaymentInfo = await _rubiesRequest.FetchTransaction(payLoad.Paymentreference);                    
                    if(rubiesPaymentInfo.Sessionid == payLoad.Sessionid && payLoad.Originatoraccountnumber == rubiesPaymentInfo.Originatoraccountnumber && payLoad.Originatorname == rubiesPaymentInfo.Originatorname)
                    {
                        var dealer = await _userManager.FindByNameAsync(dealerCode);
                        if(dealer != null){                        
                            var dealerWallet = await _walletRepository.FetchBaseWallet(dealer.Id);
                            if(dealerWallet != null)
                            {
                                var amountToFund =  payLoad.Amount - Convert.ToDecimal(_configuration["Rubies:Rate"]);
                                var creditted = _walletRepository.CreditWallet(dealerWallet.Id, amountToFund);
                                _logger.Log(LogLevel.Warning, $"Creditted => {dealerWallet.Id} credited: {creditted}");
                            }
                            await _walletRepository.LogTransfer(PaymentProcessor.Rubies, payLoad.Amount, payLoad.Paymentreference,DateTime.Now,"PAID", payLoad.Paymentreference, true, payLoad.Originatoraccountnumber, payLoad.Originatorname, payLoad.Bankcode, JsonConvert.SerializeObject(payLoad), payLoad.Craccountname, payLoad.Craccount, dealer.UserName, TransferType.Credit);
                            return PrepareResponse(HttpStatusCode.OK, "Transaction has been completed successfully", true);                        
                        }
                    }
                }   
            }

            return PrepareResponse(HttpStatusCode.BadRequest, "Transaction could not be completed");
        }

        [AllowAnonymous]
        [HttpPost("monnify-hook")]
        public async Task<IActionResult> ProcessMonnifyPayment(MonnifyPayLoad payLoad)
        {
            if(payLoad != null)
            {
                _logger.Log(LogLevel.Warning, JsonConvert.SerializeObject(payLoad));
                if(payLoad.PaymentStatus == "PAID")
                {
                    _logger.Log(LogLevel.Warning, "Started Processing Payment");
                    var secret = _configuration["Monnify:Secret"];
                    var paymentRef = payLoad.PaymentReference;
                    var transactionRef = payLoad.TransactionReference;
                    var amountPaid = payLoad.AmountPaid;
                    var paidOnString = payLoad.PaidOn.Replace("T", " ");
                    var culture = CultureInfo.GetCultureInfo("en-GB");
                    _logger.LogWarning(CultureInfo.CurrentCulture.Name);
                    var paidOnDate = DateTime.ParseExact(paidOnString, "dd/MM/yyyy hh:mm:ss tt",culture);
                    var paidOn = paidOnDate.ToString("dd/MM/yyyy hh:mm:ss tt").ToUpper();                    
                    _logger.LogWarning($"{paidOn}");
                    var generatedTransactionHash = Utilities.GenerateSHA512String($"{secret}|{paymentRef}|{amountPaid}|{paidOn}|{transactionRef}");                    
                    _logger.Log(LogLevel.Warning, generatedTransactionHash);
                    if(generatedTransactionHash.ToUpper() == payLoad.TransactionHash.ToUpper())
                    {
                        _logger.Log(LogLevel.Warning, "Transaction hash passed");
                        var transfer = await _walletRepository.FetchMonnifyTransfer(paymentRef);
                        if(transfer != null && transfer.IsProcessed)
                        {
                            return PrepareResponse(HttpStatusCode.Conflict, "This transaction has been processed already");
                        }
                        var transactionDetails = await _monify.FetchTransaction(payLoad.PaymentReference);
                        if(transactionDetails.PaymentStatus == "PAID"){
                            var customerEmail = payLoad.Customer.Email;                            
                            if(!string.IsNullOrWhiteSpace(customerEmail))
                            {
                                var user =  _userManager.Users.FirstOrDefault(x=>x.Email == customerEmail);
                                if(user != null){
                                    _logger.Log(LogLevel.Warning, $"{customerEmail} => {user.Id} Customer Email Available");
                                    var transactionWallet = await _walletRepository.FetchBaseWallet(user.Id);
                                    var amountToFund =  transactionDetails.Amount - Convert.ToDecimal(_configuration["Monnify:Rate"]);
                                    var creditted = _walletRepository.CreditWallet(transactionWallet.Id, amountToFund);
                                    _logger.Log(LogLevel.Warning, $"Creditted => {transactionWallet.Id} credited: {creditted}");
                                    await _walletRepository.LogTransfer(PaymentProcessor.Monnify, payLoad.AmountPaid, payLoad.PaymentReference, paidOnDate,payLoad.PaymentStatus, payLoad.TransactionReference, true, payLoad.AccountDetails.AccountNumber, payLoad.AccountDetails.AccountName, payLoad.AccountDetails.BankCode, JsonConvert.SerializeObject(payLoad), payLoad.Customer.Email, payLoad.Customer.Name, user.UserName, TransferType.Credit);
                                    return PrepareResponse(HttpStatusCode.OK, "Transaction has been completed successfully", true);
                                }
                            }
                        }                        
                        
                    }

                }                               
            }
            return  PrepareResponse(HttpStatusCode.BadRequest, "Transaction could not be completed");
        } 


        [HttpPost("{walletId}/manual-funding")]
        public async Task<IActionResult> ManualFunding(long walletId, FundingRequest request)
        {
            if (walletId == request.WalletId)
            {
                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                var wallet = await _walletRepository.FetchWallet(walletId);
                if(wallet != null)
                {
                    if(wallet.UserId.ToString() == userId)
                    {
                        var theRequest = _mapper.Map<FundWalletRequest>(request);
                        theRequest.InitiatedById = wallet.UserId;
                        var requested = await _walletRepository.InitiateFundingRequest(theRequest);
                        if (requested)
                        {
                            return PrepareResponse(HttpStatusCode.OK, "Funding request has been submitted succeessfully. You would be notified once it has been treated successfully", false);
                        }

                        return PrepareResponse(HttpStatusCode.InternalServerError, "You request could not be processed now. Please try agian later");
                    }
                    return PrepareResponse(HttpStatusCode.Unauthorized, "You are not authorized to fund this wallet");
                }
                return PrepareResponse(HttpStatusCode.NotFound, "The specified wallet not found");
            }

            return PrepareResponse(HttpStatusCode.Conflict, "Wallet Id mismatach");
        }

        [HttpPut("{walletId}/move-to-base/{baseWalletId}")]        
        public async Task<IActionResult> MoveCommissionWalletToBase(long walletId, long baseWalletId)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var userWallets = await _walletRepository.FetchWallets(Convert.ToInt64(userId));
            if(userWallets.Count(x=>x.Id == walletId || x.Id == baseWalletId) == 2)
            {
                var funded = _walletRepository.FundBaseWalletFromWallet(walletId, baseWalletId, out string message);
                if(funded)
                {
                    return PrepareResponse(HttpStatusCode.OK, "Funds have been moved to base wallet successfully", false);
                }

                return PrepareResponse(HttpStatusCode.BadGateway, $"Funds could not be moved successfully - {message}");
            }

            return PrepareResponse(HttpStatusCode.NotAcceptable, "You can not perform any action on one of these wallets");

        }


        [HttpPut("{walletId}/fund-with-baseWallet/{baseWalletId}")]
        public async Task<IActionResult> FundWallet(long walletId, long baseWalletId, FundingRequest request)
        {
            if (walletId == request.WalletId && baseWalletId == request.BaseWalletId)
            {
                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                var userWallets = await _walletRepository.FetchWallets(Convert.ToInt64(userId));
                if (userWallets.Count > 0)
                {
                    var wallets = userWallets.Where(x => x.Id == walletId || x.Id == request.BaseWalletId);                    
                    var fundWallet = await _walletRepository.FundWalletFromBaseWallet(walletId, baseWalletId, request.Amount,Convert.ToInt64(userId));
                    if (fundWallet)
                    {
                        return PrepareResponse(HttpStatusCode.OK, "Wallet funding has been completed successfully", false);
                    }
                    else
                    {
                        return PrepareResponse(HttpStatusCode.NotAcceptable, "Please ensure you have sufficient funds to complete this function");
                    }
                }
                else
                {
                    return PrepareResponse(HttpStatusCode.Forbidden, "You can not proceed as one or both of the wallets do not belong to you", true);
                }
            }
            else
            {
                return PrepareResponse(HttpStatusCode.ExpectationFailed, "Base wallet and wallet mismatch", true);
            }
        }
        
        
        [HttpPut("{id}/fund-request")]
        public async Task<IActionResult> CreditWallet(long id, FundingRequest request)
        {

            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var wallet = await _walletRepository.FetchWallet(id);
            if(wallet != null)
            {
                var initiated = _walletRepository.InitiateFundingRequest(new FundWalletRequest
                {
                    Amount = request.Amount,
                    InitiatedById = Convert.ToInt64(userId),
                    IsAuto = request.IsAuto,
                    Status = FundWalletRequestStatus.Inititated,
                    //TransactionRef = request.,
                    TransactionDetails = request.Description,
                    BankName = request.BankName                    
                });
            }

            return PrepareResponse(HttpStatusCode.NotFound, "The specified wallet cannot be found", true);
        }

    }
}