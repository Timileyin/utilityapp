﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Repository.Users;
using iqpay.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using iqpay.Data.UserManagement.Model;
using System.Security.Claims;
using iqpay.Core.Utilities;

namespace iqpay.Web.Controllers
{

    
    [Route("api/[controller]")]
    [ApiController]
    public class AgentsController : BaseController
    {

        private IAgentRepository _agentRepo;
        
        public AgentsController(IAgentRepository agentRepo, IMapper mapper)
        {
            _agentRepo = agentRepo;
            _mapper = mapper;
        }


        /// <summary>
        ///Fetch agents registered under a logged in dealer
        /// </summary>
        /// <returns>A payload of the list of dealers registered by a dealer</returns>
        [Authorize(Roles = "SUPER DEALER,SYSTEM ADMIN, DEALER")]
        [HttpGet]
        public async Task<IActionResult> FetchDealers()
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var role = User.Claims.FirstOrDefault(x=>x.Type == ClaimTypes.Role).Value;
            var agents = new List<ApplicationUser>();
            if(role.Contains(RoleTypes.SYSADMIN.GetDescription()))
            {
                 agents = await _agentRepo.FetchAgents(null);
            }
            else{
                  agents = await _agentRepo.FetchAgents(Convert.ToInt32(userId));
            }                    

            if (agents.Count > 0)
            {
                return PrepareResponse(System.Net.HttpStatusCode.OK, $"{agents.Count} agents found", false, _mapper.Map<List<AgentViewModel>>(agents));
            }

            return PrepareResponse(System.Net.HttpStatusCode.NotFound, "No agent found", true);

        }


        /// <summary>
        ///Fetch agent info registered under an agent
        /// </summary>
        /// <param name="id">Id of the agent</param>>
        /// <returns>A payload of the list of dealers registered by a dealer</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> FetchAgent(int id)
        {
            var agent = await _agentRepo.FetchAgentDetails(id);
            if (agent != null)
            {
                return PrepareResponse(HttpStatusCode.OK, "agent found", false, _mapper.Map<AgentViewModel>(agent));
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No agent found for that id", false);
        }


        /// <summary>
        ///Update agent information providing dealer information
        /// </summary>
        /// <param name="id">Id of the agent</param>>
        /// <returns>A response describing the status of the update</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateDealer(int id, AgentRegisterViewModel agent)
        {
            var existingAgent = await _agentRepo.FetchAgentDetails(id);
            if (existingAgent != null)
            {
                existingAgent.Address = agent.Address;
                existingAgent.Email = agent.Email;
                existingAgent.PhoneNumber = agent.Phonenumber;
                existingAgent.Name = agent.Name;
                await _agentRepo.UpdateAgent(existingAgent);
                return PrepareResponse(HttpStatusCode.OK, "Agents details has been updated successfully", false, agent);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No agent found for that id", true);
        }

        /// <summary>
        ///Deactivate a particular agent register under a dealer
        /// </summary>
        /// <param name="id">Id of the agent</param>>
        /// <returns>A response describing the status of the deactivation</returns>
        [HttpPut("{id}/deactivate")]
        public async Task<IActionResult> DeactivateDealer(int id)
        {
            var existingDealer = await _agentRepo.FetchAgentDetails(id);
            if (existingDealer != null)
            {
                existingDealer.LockoutEnabled = true;
                existingDealer.LockoutEnd = DateTime.MaxValue;
                await _agentRepo.UpdateAgent(existingDealer);

                return PrepareResponse(HttpStatusCode.OK, "Agent has been deactivated successfully", false);
            }
            return PrepareResponse(HttpStatusCode.NotFound, "The specified agent does not exist");

        }

        /// <summary>
        ///Deactivate a particular agent register under a dealer
        /// </summary>
        /// <param name="id">Id of the agent</param>>
        /// <returns>A response describing the status of the activation</returns>
        [HttpPut("{id}/activate")]
        public async Task<IActionResult> ActivateDealer(int id)
        {
            var existingAgent = await _agentRepo.FetchAgentDetails(id);
            if (existingAgent != null)
            {
                existingAgent.LockoutEnd = null;
                await _agentRepo.UpdateAgent(existingAgent);

                return PrepareResponse(HttpStatusCode.OK, "Agent has been activated successfully", false);
            }
            return PrepareResponse(HttpStatusCode.NotFound, "The specified agent does not exist");
        }

        /// <summary>
        ///Deactivate a particular agent register under a dealer
        /// </summary>
        /// <param name="id">Id of the agent</param>>
        /// <returns>A response describing the status of the dealer removal</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> RemoveDealer(int id)
        {
            var existingDealer = await _agentRepo.FetchAgentDetails(id);
            if (existingDealer != null)
            {
                await _agentRepo.RemoveAgent(existingDealer.Id);
                return PrepareResponse(HttpStatusCode.OK, "Agent has been removed successfully", false, null);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No agent found for that id", true);

        }
    }
}
