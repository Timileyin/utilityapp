﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Data.Entities.AuditTrail;
using iqpay.Data.Entities.Services;
using iqpay.Engine.Base.Entities;
using iqpay.Engine.Repository.EmailRepository;
using iqpay.Repository.ServicesRepository;
using iqpay.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace iqpay.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServicesController : BaseController
    {
        private readonly IServicesRepository _serviceRepository;
        private readonly IVendorRepository _vendorRepository;
        public ServicesController(IServicesRepository serviceRepo, IMapper mapper, IAuditTrailManager<AuditTrail> auditTrailManager, IVendorRepository vendorRepository)
        {
            _serviceRepository = serviceRepo;
            _mapper = mapper;
            _auditTrailManager = auditTrailManager;
            _vendorRepository = vendorRepository;
        }

        /// <summary>
        ///Fetch all services
        /// </summary>
        /// <returns>A payload that contains a list of services</returns>
        [HttpGet]
        public async Task<IActionResult> FetchServices()
        {
            var services = _mapper.Map<List<ServicesModel>>(await _serviceRepository.FetchServices());
            if(services.Count > 0)
            {
                return PrepareResponse(System.Net.HttpStatusCode.OK, $"{services.Count()} services found", false, services);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "Serivces not found");
        }

        /// <summary>
        ///Fetch a specified service
        /// </summary>
        /// <param name="id">The id of a specified service</params>
        /// <returns>A payload that contains details of the service</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> FetchService(long id)
        {
            var service = _mapper.Map<ServicesModel>(await _serviceRepository.FetchService(id));
            if(service != null)
            {
                return PrepareResponse(HttpStatusCode.OK, "Service found", false, service);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "Service not found");
        }

        /// <summary>
        ///Create a service
        /// </summary>
        /// <returns>A payload that contains the created service</returns>
        [HttpPost]
        [Authorize(Roles = "SYSTEM ADMIN")]
        public async Task<IActionResult> CreateService(ServicesModel service)
        {
            var newService = _mapper.Map<Services>(service);
            newService.UserId = Convert.ToInt64(User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value);
            await _serviceRepository.CreateService(newService);
            await _auditTrailManager.AddAuditTrail(new AuditTrail
            {
                UserId = newService.UserId,
                ActionTaken = Engine.Base.Entities.AuditAction.Create,
                DateCreated = DateTime.Now,
                Description = $"Service {service.Name} has been created successfully",
                Entity = "Service"
            });
            return PrepareResponse(HttpStatusCode.OK, "Service has been created  successfully", false, _mapper.Map<ServicesModel>(newService));
        }

        /// <summary>
        ///Update service commissions 
        /// </summary>
        /// <param name="id">The id of the service</param>
        /// <returns>A payload that shows the codes of the processors</returns>
        [Authorize]
        [HttpPut("{id}/commission")]
        public async Task<IActionResult> UpdateServiceCommissions(long id, ServiceCommissionModel model)
        {
            var service = await _serviceRepository.FetchService(id);
            if(service != null)
            {
                service.DefaultSuperDealerCommission = model.SuperDealerCommission;
                service.DefaultSuperDealerCommissionCap = model.SuperDealerCommissionCap;
                service.DefaultDealerCommissionCap = model.DealerCommissionCap;
                service.DefaultDealerCommission = model.DealerCommission;
                service.DefaultAgentCommission = model.AgentCommission;
                service.DefaultAgentCommissionCap = model.AgentCommissionCap;
                service.CustomerCommissionCap = model.CustomerCommissionCap;
                service.CustomerCommission = model.CustomerCommission;
                service.Commission = model.Commission;
                service.CommissionCap = model.CommissionCap;
                

                await _serviceRepository.UpdateService(service);
                return PrepareResponse(HttpStatusCode.OK, "Service has been updated successfully", false);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "The specified service not found", true);
        }

        /// <summary>
        ///Fetch a service processors codes 
        /// </summary>
        /// <param name="id">The id of the service</param>
        /// <returns>A payload that shows the codes of the processors</returns>
        [HttpGet("{id}/processorCodes")]
        public IActionResult FetchServiceProcessorCode(long id)
        {
            var serviceCodes =  _serviceRepository.FetchServiceProcessorCode(id);            
            return PrepareResponse(HttpStatusCode.OK, "Service code", false, _mapper.Map<List<ProcessorsCode>>(serviceCodes));
        }

        /// <summary>
        ///Update service processors codes 
        /// </summary>
        /// <param name="id">The id of the service</param>
        /// <returns>A payload that shows the codes of the processors</returns>
        [HttpPut("{id}/processorCodes")]
        public async Task<IActionResult> SaveServiceCodes(long id, List<ProcessorsCode> codes)
        {
            var serviceCodes = _mapper.Map<List<ProcessorsServiceCodes>>(codes);
            var updated = _serviceRepository.UpdateServiceCodes(id, serviceCodes, out string message);
            if (updated)
            {
                var userId = Convert.ToInt64(User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value);
                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    ActionTaken = AuditAction.Update,
                    Entity = "ProcessorCodes",
                    Description = $"Service Codes for service #{id} was updated",
                    UserId = userId
                });

                return PrepareResponse(HttpStatusCode.OK, "Service codes have been updated sucessfully", false);
            }

            return PrepareResponse(HttpStatusCode.OK, message);

        }


        /// <summary>
        ///Update a service
        /// </summary>
        /// <param name="id">The id of the service to be updated</param>
        /// <returns>A payload that shows the result of the update</returns>
        [HttpPut("{id}")]
        [Authorize(Roles = "SYSTEM ADMIN")]
        public async Task<IActionResult> UpdateService(long id, ServicesModel servicesModel)
        {
            var service = await _serviceRepository.FetchService(id);
            if(service != null)
            {
                service.Price = servicesModel.Amount;
                service.ServiceName = servicesModel.Name;
                service.isFixedPrice = servicesModel.isFixedPrice;
                service.CommissionMode = servicesModel.CommissionMode;
                //service.
                service.Status = (ServiceStatusEnum)servicesModel.StatusInt;
                service.Type = (ServiceType)servicesModel.ServiceTypeInt;
                service.Description = servicesModel.Description;
                service.RequiresVerification = servicesModel.RequiresVerification;
                service.IsMeasuredService = servicesModel.IsMeasuredService;
                service.UnitType = servicesModel.UnitType;
                service.DeliveryType = (DeliveryTypes?)servicesModel.DeliveryTypeInt;
                service.FlatDeliveryFee = servicesModel.FlatDeliveryFee;
                service.FlatDeliveryFeeBeyondCap = servicesModel.FlatDeliveryFeeBeyondCap;
                service.FlatDeliveryFeeCap = servicesModel.FlatDeliveryFeeCap;
                service.IsSharedProduct = servicesModel.IsSharedProduct;
                service.LastOrderDate = servicesModel.LastOrderDate;
                service.MaxOrders = servicesModel.MaxOrders;
                service.FlatCollectionFee = servicesModel.FlatCollectionFee;
                service.FlatCollectionFeeBeyondCap = servicesModel.FlatCollectionFeeBeyondCap;
                service.FlatCollectionFeeCap = servicesModel.FlatCollectionFeeCap;

                await _serviceRepository.UpdateService(service);
                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    UserId = service.UserId,
                    ActionTaken = Engine.Base.Entities.AuditAction.Update,
                    DateCreated = DateTime.Now,
                    Description = $"Service {servicesModel.Name} has been updated successfully",
                    Entity = "Service"
                });
                return PrepareResponse(HttpStatusCode.OK, "Service has been updated successfully", false, servicesModel);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "The specified service does not exist");
        }


        /// <summary>
        ///Fetch services in a service type
        /// </summary>
        /// <param name="serviceTypeId">The id of the service type</param>
        /// <returns>A payload that shows the list od services that belong to a service type</returns>
        [HttpGet("/api/service-types/{serviceTypeId}/services")]
        public async Task<IActionResult> FetchServicesInServiceType(int serviceTypeId)
        {
            var services = await _serviceRepository.FetchServicesInType(serviceTypeId);
            if(services.Count > 0)
            {

                return PrepareResponse(HttpStatusCode.OK, $"{services.Count} services returned ", false, _mapper.Map<List<ServicesModel>>(services));
            }

            return PrepareResponse(HttpStatusCode.NotFound, "The service type does not have services");

        }


        /// <summary>
        ///Fetch vendors in a service type
        /// </summary>
        /// <param name="serviceTypeId">The id of the service type</param>
        /// <returns>A payload that shows the list od services that belong to a service type</returns>
        [Authorize]
        [HttpGet("/api/service-types/{serviceTypeId}/vendors")]
        public async Task<IActionResult> FetchVendorsInServiceType(int serviceTypeId)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var vendors = await _vendorRepository.FetchVendorsInType(serviceTypeId, Convert.ToInt64(userId));
            if (vendors.Count > 0)
            {

                return PrepareResponse(HttpStatusCode.OK, $"{vendors.Count} vendors returned ", false, _mapper.Map<List<VendorServiceModel>>(vendors));
            }

            return PrepareResponse(HttpStatusCode.NotFound, "The service type does not have vendors");

        }


        /// <summary>
        ///Delete a service
        /// </summary>
        /// <param name="id">The id of the service to be deleted</param>
        /// <returns>A payload that shows the result of the deletion</returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "SYSTEM ADMIN")]
        public async Task<IActionResult> DeleteService(int serviceId)
        {
            var service = await _serviceRepository.FetchService(serviceId);
            if (service != null)
            {
                await _serviceRepository.RemoveService(serviceId);

                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    UserId = service.UserId,
                    ActionTaken = Engine.Base.Entities.AuditAction.Delete,
                    DateCreated = DateTime.Now,
                    Description = $"Service {service.ServiceName} has been deleted successfully",
                    Entity = "Service"
                });
                return PrepareResponse(HttpStatusCode.OK, "Service has been removed successfully", false);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "The specified service can not be found");
        }


    }
}
