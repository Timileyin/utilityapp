using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.Wallet;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Security.Claims; 
using Microsoft.AspNetCore.Identity;
using iqpay.Data.UserManagement.Model;
using System.Globalization;
using iqpay.Repository.VFD;
using iqpay.Repository.VFD.Models;
using Microsoft.AspNetCore.Identity;
using iqpay.Core.Utilities;
using iqpay.Web.Models;
using iqpay.Repository.TransactionRepository;

namespace iqpay.Web.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]    
    public class VFDController : BaseController
    {
        private readonly IVFDRequests _vfdRequest;
        private readonly IWalletRepository _walletRepository;
        private readonly ISendMoneyRepo _sendMoneyRepo;        
        public VFDController(ISendMoneyRepo sendMoneyRepo, IMapper mapper, IVFDRequests vfdRequest, UserManager<ApplicationUser> userManager, IWalletRepository walletRepo, IConfiguration config, ILogger<VFDController> logger)
        {
            _mapper = mapper;
            _vfdRequest = vfdRequest;
            _walletRepository = walletRepo;
            _userManager = userManager;
            _configuration = config;
            _logger = logger;
            _sendMoneyRepo = sendMoneyRepo;
        }        

        
        [AllowAnonymous]
        [HttpPost("fund")]
        public async Task<IActionResult> VFDFund(VFDHookModel model)
        {
            if(model.Reference != null)
            {
                if(!string.IsNullOrWhiteSpace(model.Reference))
                {
                    //var payLoadStr = JsonConvert.SerializeObject(payLoad);
                    var transfer = await _walletRepository.FetchVFDTransfer(model.Reference);
                    if(transfer?.IsProcessed == true)
                    {
                        return PrepareResponse(HttpStatusCode.Conflict, "This transaction has been processed already");
                    }
                    var vfdPaymentInfo = await _vfdRequest.FetchTransaction(model.Reference);                    
                    if(vfdPaymentInfo.TransactionStatus == "00")
                    {
                        var dealer = _userManager.Users.FirstOrDefault(x=>x.VFDAccountNumber == model.Account_number);
                        if(dealer != null)
                        {                        
                            var dealerWallet = await _walletRepository.FetchBaseWallet(dealer.Id);
                            if(dealerWallet != null)
                            {
                                var amountToFund =  vfdPaymentInfo.Amount - Convert.ToDecimal(_configuration["VFD:Rate"]);
                                var creditted = _walletRepository.CreditWallet(dealerWallet.Id, amountToFund);
                                _logger.Log(LogLevel.Information, $"Creditted => {dealerWallet.Id} credited: {creditted}");                                
                            }
                            await _walletRepository.LogTransfer(PaymentProcessor.VFD, vfdPaymentInfo.Amount, vfdPaymentInfo.TxnId,DateTime.Now,"PAID", vfdPaymentInfo.TxnId, true, model.Originator_account_number, model.Originator_account_name, vfdPaymentInfo.FromBank, JsonConvert.SerializeObject(vfdPaymentInfo), model.Account_number, dealer.VFDAccountName , dealer.UserName, TransferType.Credit);
                            return PrepareResponse(HttpStatusCode.OK, "Transaction has been completed successfully", false);                        
                        }
                    }
                }   
            }

            return PrepareResponse(HttpStatusCode.BadRequest, "Transaction could not be completed");
        }

        [HttpPost("bankenquiry")]
        public async Task<IActionResult> NameEnquiry(VFDNameEnquiryModel model)
        {
            var nameEnquiry = await _vfdRequest.NameEnquiry(model.AccountNo, model.Bank);
            var transfer_type = _configuration["VFD:VFDCode"] == model.Bank ? "intra" : "inter";
            return PrepareResponse(HttpStatusCode.OK, "Name enquiry successful", false,  new VFDNameEnquiryModel{
                 AccountNo = nameEnquiry.Account.Number, AccountName = nameEnquiry.Name, Bank = model.Bank, BankName=nameEnquiry.Bank, SessionId = nameEnquiry.Account.Id, Transfer_type = transfer_type, ClientId = nameEnquiry.ClientId, BVN = nameEnquiry.Bvn });  
        }



        [HttpGet("bankslist")]
        public async Task<IActionResult> FetchBanksList()
        {
            var bankList = await _vfdRequest.FetchBanks();
            return PrepareResponse(HttpStatusCode.OK, "Banks returned", false, bankList.Bank);
        }

        private string GenerateTransactionRef()
        {
            var txnRef = Utilities.GenerateRandomString();
            txnRef = _configuration["VFD:RefPrefix"]+"-"+txnRef;
            while(_sendMoneyRepo.GetSendMoneyLog(txnRef) != null)
            {
                txnRef = Utilities.GenerateRandomString(); 
                txnRef = _configuration["VFD:RefPrefix"]+"-"+txnRef;
            }

            return txnRef;
        }

        [HttpPost("transfer")]
        public async Task<IActionResult> CompleteTransfer(BankTransferModel model)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;  
            var user =  await _userManager.FindByIdAsync(userId);        
            var baseWallet = await _walletRepository.FetchBaseWallet(Convert.ToInt64(userId));
            if(baseWallet != null)
            { 
                var fee = Convert.ToDecimal(_configuration["VFD:TransferFee"]);
                if(baseWallet.Balance >= (model.Amount + fee)){

                    var amountToSend = model.Amount + fee;
                    var txnRef = GenerateTransactionRef();
                    var sendMoney = await _sendMoneyRepo.CreateLog(txnRef, model.Amount, model.BankAccountNumber, model.AccountName, model.BankName, model.Narration, Convert.ToInt64(userId));
                    var transfer = await _vfdRequest.CompleteOutwardTransfer(txnRef, amountToSend, model.BankAccountNumber, model.BankCode,model.BVN, model.AccountName, model.ClientId, model.ToSession, model.SavingsId, model.Narration);
                    
                    var debitAmount = model.Amount + fee;
                    sendMoney.Fee = fee;
                    sendMoney.Status = SendMoneyEnum.Completed;
                    sendMoney.TransferResponse = JsonConvert.SerializeObject(transfer);
                    sendMoney.DateCompleted = DateTime.Now;
                    sendMoney.Bank = model.BankName;
                    sendMoney.Narration = model.Narration;
                    sendMoney.WalletBalance = baseWallet.Balance - debitAmount;
                    await _sendMoneyRepo.UpdateLog(sendMoney);                                                  
                    await _walletRepository.DebitWallet(baseWallet.Id, debitAmount);       
                    return PrepareResponse(HttpStatusCode.OK, "Bank Transfer has been completed successfully", false);       
                
                }
                return PrepareResponse(HttpStatusCode.NotAcceptable, "Bank Transfer could not be completed due to insifficent funds");
            }
            return PrepareResponse(HttpStatusCode.NotFound, "Base wallet not found");
        }

    }
}