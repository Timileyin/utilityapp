﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Repository.Processors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using iqpay.Web.Models;
using iqpay.Data.Entities.Services;
using System.Net;
using iqpay.Engine.Repository.EmailRepository;
using iqpay.Data.Entities.AuditTrail;
using iqpay.Engine.Base.Entities;
using System.IdentityModel.Tokens.Jwt;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace iqpay.Web.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ProcessorsController : BaseController
    {

        private readonly IProcessorRepository _repository;
        public ProcessorsController(IProcessorRepository repository, IMapper mapper, IAuditTrailManager<AuditTrail> auditTrailManager)
        {
            _repository = repository;
            _mapper = mapper;
            _auditTrailManager = auditTrailManager;
        }

        // GET: /<controller>/
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var processors = _mapper.Map<List<ProcessorModel>>(await _repository.FetchProcessors());
            if (processors.Count == 0)
            {
                return PrepareResponse(HttpStatusCode.NotFound, "No processor found", true);
            }
            return PrepareResponse(HttpStatusCode.OK, "Processors found", false, processors);
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> FetchProcessor(long id)
        {
            var processor = _mapper.Map<ProcessorModel>(await _repository.GetProcessor(id));
            if (processor == null)
            {
                return PrepareResponse(HttpStatusCode.NotFound, "No processors found", false);
            }
            return PrepareResponse(HttpStatusCode.OK, "Processor found", false, processor);
        }

        [HttpPost]
        public async Task<IActionResult> CreateProcessor(ProcessorModel processor)
        {
            
            var createProcessor = _mapper.Map<Processor>(processor);
            await _repository.CreateProcessor(createProcessor);

            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            await _auditTrailManager.AddAuditTrail(new AuditTrail
            {
                ActionTaken = AuditAction.Create,
                UserId = Convert.ToInt64(userId),
                Description = "Processor has been created successfully",
                Entity = "Processor",
                UserName = User.Identity.Name
            });

            return PrepareResponse(HttpStatusCode.OK, "Processor Created", false, null);                       
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateProcessor(ProcessorModel processor)
        {
            var updateProcessor = _mapper.Map<Processor>(processor);
            await _repository.UpdateProcessor(updateProcessor);

            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            await _auditTrailManager.AddAuditTrail(new AuditTrail
            {
                ActionTaken = AuditAction.Update,
                UserId = Convert.ToInt64(userId),
                Description = "Processor has been updated successfully",
                Entity = "Processor",
                UserName = User.Identity.Name
            });

            return PrepareResponse(HttpStatusCode.OK, "Processo Updated successfully", false, null);
        }
    }
}
