using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Repository.Users;
using iqpay.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using iqpay.Data.UserManagement.Model;
using System.Security.Claims;
using iqpay.Core.Utilities;

namespace iqpay.Web.Controllers
{

    
    [Route("api/[controller]")]
    [ApiController]
    public class HouseHoldUserController : BaseController
    {

        private IHouseholdRepository _agentRepo;
        
        public HouseHoldUserController(IHouseholdRepository agentRepo, IMapper mapper)
        {
            _agentRepo = agentRepo;
            _mapper = mapper;
        }


        /// <summary>
        ///Fetch agents registered under a logged in dealer
        /// </summary>
        /// <returns>A payload of the list of dealers registered by a dealer</returns>
        [Authorize(Roles = "SYSTEM ADMIN")]
        [HttpGet]
        public IActionResult FetchDealers()
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var role = User.Claims.FirstOrDefault(x=>x.Type == ClaimTypes.Role).Value;
            var agents = new List<ApplicationUser>();
            if(role.Contains(RoleTypes.SYSADMIN.GetDescription()))
            {
                 agents = _agentRepo.FetchUsers();
            }                            

            if (agents.Count > 0)
            {
                return PrepareResponse(System.Net.HttpStatusCode.OK, $"{agents.Count} users found", false, _mapper.Map<List<AgentViewModel>>(agents));
            }

            return PrepareResponse(System.Net.HttpStatusCode.NotFound, "No users found", true);

        }


        /// <summary>
        ///Fetch agent info registered under an agent
        /// </summary>
        /// <param name="id">Id of the agent</param>>
        /// <returns>A payload of the list of dealers registered by a dealer</returns>
        [HttpGet("{id}")]
        public IActionResult FetchAgent(int id)
        {
            var agent = _agentRepo.FetchUsersDetails(id);
            if (agent != null)
            {
                return PrepareResponse(HttpStatusCode.OK, "User found", false, _mapper.Map<AgentViewModel>(agent));
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No user found for that id", false);
        }


        /// <summary>
        ///Update agent information providing dealer information
        /// </summary>
        /// <param name="id">Id of the agent</param>>
        /// <returns>A response describing the status of the update</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateDealer(int id, AgentRegisterViewModel agent)
        {
            var existingAgent = _agentRepo.FetchUsersDetails(id);
            if (existingAgent != null)
            {
                existingAgent.Address = agent.Address;
                existingAgent.Email = agent.Email;
                existingAgent.PhoneNumber = agent.Phonenumber;
                existingAgent.Name = agent.Name;
                await _agentRepo.UpdateUser(existingAgent);
                return PrepareResponse(HttpStatusCode.OK, "User details has been updated successfully", false, agent);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No user found for that id", true);
        }

        /// <summary>
        ///Deactivate a particular agent register under a dealer
        /// </summary>
        /// <param name="id">Id of the agent</param>>
        /// <returns>A response describing the status of the deactivation</returns>
        [HttpPut("{id}/deactivate")]
        public IActionResult DeactivateDealer(int id)
        {
            var existingDealer = _agentRepo.FetchUsersDetails(id);
            if (existingDealer != null)
            {
                existingDealer.LockoutEnabled = true;
                existingDealer.LockoutEnd = DateTime.MaxValue;
                _agentRepo.UpdateUser(existingDealer);

                return PrepareResponse(HttpStatusCode.OK, "User has been deactivated successfully", false);
            }
            return PrepareResponse(HttpStatusCode.NotFound, "The specified user does not exist");

        }

        /// <summary>
        ///Deactivate a particular agent register under a dealer
        /// </summary>
        /// <param name="id">Id of the agent</param>>
        /// <returns>A response describing the status of the activation</returns>
        [HttpPut("{id}/activate")]
        public IActionResult ActivateDealer(int id)
        {
            var existingAgent = _agentRepo.FetchUsersDetails(id);
            if (existingAgent != null)
            {
                existingAgent.LockoutEnd = null;
                _agentRepo.UpdateUser(existingAgent);

                return PrepareResponse(HttpStatusCode.OK, "User has been activated successfully", false);
            }
            return PrepareResponse(HttpStatusCode.NotFound, "The specified user does not exist");
        }

        /// <summary>
        ///Deactivate a particular agent register under a dealer
        /// </summary>
        /// <param name="id">Id of the agent</param>>
        /// <returns>A response describing the status of the dealer removal</returns>
        [HttpDelete("{id}")]
        public IActionResult RemoveDealer(int id)
        {
            var existingDealer =  _agentRepo.FetchUsersDetails(id);
            if (existingDealer != null)
            {
                _agentRepo.RemoveUser(existingDealer.Id);
                return PrepareResponse(HttpStatusCode.OK, "User has been removed successfully", false, null);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No user found for that id", true);

        }
    }
}
