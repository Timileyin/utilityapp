﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Data.Entities.AuditTrail;
using iqpay.Engine.Base.Entities;
using iqpay.Engine.Repository.EmailRepository;
using iqpay.Repository.Users;
using iqpay.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using iqpay.Data.Entities.Payment;
using iqpay.Data.UserManagement.Model;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using iqpay.Core.Utilities;
using iqpay.Repository.ServicesRepository;

namespace iqpay.Web.Controllers
{

    
    [Route("api/[controller]")]
    [ApiController]
    public class DealersController : BaseController
    {

        private IDealerRepository _dealerRepo;
        private IServicesRepository _serviceRepo;
        private IVendorRepository _vendorRepository;
        public DealersController(IVendorRepository vendorRepository, IServicesRepository serviceRepo, IDealerRepository dealerRepo, UserManager<ApplicationUser> userManager,  IMapper mapper, IAuditTrailManager<AuditTrail> auditTrailManager)
        {
            _userManager = userManager;
            _dealerRepo = dealerRepo;
            _mapper = mapper;
            _auditTrailManager = auditTrailManager;
            _serviceRepo = serviceRepo;
            _vendorRepository = vendorRepository;
        }


        /// <summary>
        ///Fetch agents registered under a logged in dealer
        /// </summary>
        /// <returns>A payload of the list of dealers registered by a dealer</returns>
        [Authorize(Roles = "SUPER DEALER,SYSTEM ADMIN")]
        [HttpGet]
        public async Task<IActionResult> FetchDealers()
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var role = User.Claims.FirstOrDefault(x=>x.Type == ClaimTypes.Role).Value;
            var dealers = new List<ApplicationUser>();
            if(role.Contains(RoleTypes.SYSADMIN.GetDescription()))
            {
                 dealers = await _dealerRepo.FetchDealers(null);
            }
            else{
                  dealers = await _dealerRepo.FetchDealers(Convert.ToInt32(userId));
            }
            if (dealers.Count > 0)
            {
                return PrepareResponse(System.Net.HttpStatusCode.OK, $"{dealers.Count} dealers found", false, _mapper.Map<List<AgentViewModel>>(dealers));
            }

            return PrepareResponse(System.Net.HttpStatusCode.NotFound, "No dealer found", true);
        }

        /// <summary>
        /// Update dealer's services checklist
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload of the list of services registered to a dealer</returns>

        [HttpPut]
        [Route("{id}/services")]
        public IActionResult UpdateServiceChecklist(long id, List<long> vendorIds)
        {
            _serviceRepo.UpdateDealerServiceChecklist(id, vendorIds);
            return PrepareResponse(HttpStatusCode.OK, "Dealer services checklist updated", false, null);
        }

        /// <summary>
        ///Fetch dealer's service checklist
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload of the list of services registered to a dealer</returns>
        [HttpGet]
        [Route("{id}/services")]
        public async Task<IActionResult> FetchDealerServices(long id)
        {
            var dealersServices = await _serviceRepo.FetchDealersServices(id);
            var vendorsList = await _vendorRepository.FetchActiveVendors();            
            var checklist = from v in vendorsList  
            join d in dealersServices on v.Id equals d.VendorId.Value into dv
            from c in dv.DefaultIfEmpty()
            select new DealerServiceModel{
                Id = v.Id,
                Name = v.VendorName,
                IsActive = c != null,
                VendorLogo = v.LogoSrc,
                Serivces = _mapper.Map<List<ServicesModel>>(v.Serivces) 
            };           

            return PrepareResponse(HttpStatusCode.OK, "Dealer Services returned", false, checklist);
        }


        /// <summary>
        ///Fetch dealer's information
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload of the list of dealers registered by a dealer</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> FetchDealer(int id)
        {
            var dealer = await _dealerRepo.FetchDealerDetails(id);
            if(dealer != null)
            {
                return PrepareResponse(HttpStatusCode.OK, "dealer found", false, _mapper.Map<AgentViewModel>(dealer));
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No dealer found for that id", false);
        }


        /// <summary>
        ///update dealer's information
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload showing the if update was successful or not</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateDealer(int id, DealerUpdateViewModel dealer)
        {
            var existingDealer = await _dealerRepo.FetchDealerDetails(id);
            if (dealer != null)
            {
                existingDealer.Address = dealer.Address;
                existingDealer.Email = dealer.Email;
                existingDealer.PhoneNumber = dealer.Phonenumber;
                existingDealer.Name = dealer.Name;                
                await _dealerRepo.UpdateDealer(existingDealer);

                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    ActionTaken = AuditAction.Update,
                    DateCreated = DateTime.Now,
                    Description = $"Dealer {existingDealer.UserName} has been updated successfully",
                    Entity = "Dealer",
                    UserId = Convert.ToInt64(userId)

                });
                return PrepareResponse(HttpStatusCode.OK, "dealer details has been updated successfully", false, dealer);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No dealer found for that id", true);
        }

        /// <summary>
        ///Fetch dealer's commission
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload showing if commission of services attached to a dealer</returns>
        [HttpGet("{id}/commissions")]
        public async Task<IActionResult> FetchCommission(long id)
        {
            var dealerCommission = await _dealerRepo.FetchCommission(id);
            var vendorCommission = await _dealerRepo.FetchVendorCommission(id);
            var user = await _userManager.FindByIdAsync(id.ToString());
            foreach(var vendor in vendorCommission)
            {
                vendor.DealerCommissions = vendor.DealerCommissions.Where(x=>x.UserId == id).ToList();
                if(vendor.DealerCommissions.Count == 0)
                {
                    if(user.AccountType == AccountType.AGENT){
                        var _dealerCommission = new DealerCommission{
                            Commission = vendor.AgentCommission,
                            CommissionCap = vendor.AgentCommissionCap
                        };
                        vendor.DealerCommissions.Add(_dealerCommission);
                    }

                    if(user.AccountType == AccountType.DEALER){
                        var _dealerCommission = new DealerCommission{
                            Commission = vendor.DealerCommission,
                            CommissionCap = vendor.DealerCommissionCap,
                        };
                        vendor.DealerCommissions.Add(_dealerCommission);
                    }

                    if(user.AccountType == AccountType.SUPER_DEALER){
                        var _dealerCommission = new DealerCommission{
                            Commission = vendor.SuperDealerCommission,
                            CommissionCap = vendor.SuperDealerCommissionCap,
                        };

                        vendor.DealerCommissions.Add(_dealerCommission);
                    }
                }
            }

            foreach(var service in dealerCommission)
            {
                service.DealerCommissions = service.DealerCommissions.Where(x=>x.UserId == id).ToList();
                if(service.DealerCommissions.Count == 0)
                {
                    if(user.AccountType == AccountType.AGENT){
                        var _dealerCommission = new DealerCommission{
                            Commission = service.DefaultAgentCommission,
                            CommissionCap = service.DefaultAgentCommissionCap
                        };
                        service.DealerCommissions.Add(_dealerCommission);
                    }

                    if(user.AccountType == AccountType.DEALER){
                        var _dealerCommission = new DealerCommission{
                            Commission = service.DefaultDealerCommission,
                            CommissionCap = service.DefaultDealerCommissionCap,
                        };
                        service.DealerCommissions.Add(_dealerCommission);
                    }

                    if(user.AccountType == AccountType.SUPER_DEALER){
                        var _dealerCommission = new DealerCommission{
                            Commission = service.DefaultSuperDealerCommission,
                            CommissionCap = service.DefaultSuperDealerCommissionCap,
                        };

                        service.DealerCommissions.Add(_dealerCommission);
                    }

                    
                }
            }

            var serviceCommissionRate = _mapper.Map<List<DealerCommissionModel>>(dealerCommission);
            var vendorCommissionRate = _mapper.Map<List<DealerCommissionModel>>(vendorCommission);

            serviceCommissionRate.AddRange(vendorCommissionRate);
            foreach(var rate in serviceCommissionRate){
                rate.DealerId = id;
            } 
            return PrepareResponse(HttpStatusCode.OK, "Commission returned successfully", false, serviceCommissionRate);
        }

        /// <summary>
        ///Update dealer's commission
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload showing if commission of services attached to a dealer</returns>
        [HttpPut("{id}/commissionscap")]
        public async Task<IActionResult> CommissionsCap(long id, DealerCapModel model)
        {
            if(id != model.DealerId)
            {
                return PrepareResponse(HttpStatusCode.BadRequest, "Dealer id mismatch", true);
            }

            
            var update = _dealerRepo.UpdateCommissionCap(model.ServiceId, model.VendorId, model.DealerId, model.Cap, out string message);
            if(update)
            {
                return PrepareResponse(HttpStatusCode.OK, "Dealer commission been updated successfully", false);
            }
            else
            {
                return PrepareResponse(HttpStatusCode.BadRequest, message);
            }
        }

        /// <summary>
        ///Update dealer's commission
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload showing if commission of services attached to a dealer</returns>
        [HttpPut("{id}/commissions")]
        public IActionResult UpdateCommission(long id, DealerCommissionModel model)
        {
            if(id != model.DealerId)
            {
                return PrepareResponse(HttpStatusCode.BadRequest, "Dealer id mismatch", true);
            }

            
            var update = _dealerRepo.UpdateCommission(model.ServiceId, model.VendorId, model.DealerId, model.Commission.Value, out string message);
            if(update)
            {
                return PrepareResponse(HttpStatusCode.OK, "Dealer commission been updated successfully", false);
            }
            else
            {
                return PrepareResponse(HttpStatusCode.BadRequest, message);
            }
            
        }


        /// <summary>
        ///Deactivte dealer
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload showing if deactivation was successful or not</returns>
        [HttpPut("{id}/deactivate")]
        public async Task<IActionResult> DeactivateDealer(long id)
        {
            var existingDealer = await _dealerRepo.FetchDealerDetails(id);
            if(existingDealer != null)
            {
                existingDealer.LockoutEnabled = true;
                existingDealer.LockoutEnd = DateTime.MaxValue;
                await _dealerRepo.UpdateDealer(existingDealer);
                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    ActionTaken = AuditAction.Update,
                    DateCreated = DateTime.Now,
                    Description = $"Dealer {existingDealer.UserName} has been deactivated successfully",
                    Entity = "Dealer",
                    UserId = Convert.ToInt64(userId)

                });
                return PrepareResponse(HttpStatusCode.OK, "Dealer has been deactivated successfully", false);
            }
            return PrepareResponse(HttpStatusCode.NotFound, "The specified dealer does not exist");

        }

        // <summary>
        ///Activate deactivated dealer
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload showing if activation was successful or no</returns>
        [HttpPut("{id}/activate")]
        public async Task<IActionResult> ActivateDealer(int id)
        {
            var existingDealer = await _dealerRepo.FetchDealerDetails(id);
            if (existingDealer != null)
            {
                existingDealer.LockoutEnd = null;
                await _dealerRepo.UpdateDealer(existingDealer);
                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    ActionTaken = AuditAction.Update,
                    DateCreated = DateTime.Now,
                    Description = $"Dealer {existingDealer.UserName} has been activated successfully",
                    Entity = "Dealer",
                    UserId = Convert.ToInt64(userId)

                });
                return PrepareResponse(HttpStatusCode.OK, "Dealer has been activated successfully", false);
            }
            return PrepareResponse(HttpStatusCode.NotFound, "The specified dealer does not exist");
        }


        /// <summary>
        ///Remove dealer
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload showing if removal was successful or not</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> RemoveDealer(int id)
        {
            var existingDealer = await _dealerRepo.FetchDealerDetails(id);
            if (existingDealer != null)
            {
                await _dealerRepo.RemoveDealer(existingDealer.Id);

                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    ActionTaken = AuditAction.Delete,
                    DateCreated = DateTime.Now,
                    Description = $"Dealer {existingDealer.UserName} has been removed successfully",
                    Entity = "Dealer",
                    UserId = Convert.ToInt64(userId)

                });
                return PrepareResponse(HttpStatusCode.OK, "dealer details has been removed successfully", false, null);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No dealer found for that id", true);
        }




    }
}