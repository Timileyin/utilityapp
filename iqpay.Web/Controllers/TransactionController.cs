﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using System.Collections.Generic;
using iqpay.Core.Utilities;
using iqpay.Data.Entities.Payment;
using iqpay.Repository.TransactionRepository;
using iqpay.Repository.Wallet;
using iqpay.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using iqpay.Repository.ServicesRepository;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using iqpay.Data.UserManagement.Model;
using iqpay.Web.Models;
using iqpay.Repository.Processors.ProcessorImplementations;

namespace iqpay.Web.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TransactionController : BaseController
    {
        private readonly ITransactionRepository _transactionRepository;
        private readonly IWalletRepository _walletRepository;
        private readonly IVendImplementation _vendImp;
        private readonly IAffordables _affordables;
        public TransactionController(IConfiguration config, ITransactionRepository transactionRepository, IAffordables affordables, IMapper mapper, IWalletRepository walletRepository, IVendImplementation vendImp)
        {
            _configuration = config;
            _transactionRepository = transactionRepository;
            _mapper = mapper;
            _walletRepository = walletRepository;
            _vendImp = vendImp;
            _affordables = affordables;
        }

        [HttpGet]
        public IActionResult FetchTransactions()
        {
            var userRole = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role).Value;
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            if(userRole.Contains(RoleTypes.SYSADMIN.GetDescription())){
                var transactions = _vendImp.FetchVendingLog();
                return PrepareResponse(HttpStatusCode.OK, $"{transactions.Count()} returned", false, transactions);
            }else{
                var transactions = _vendImp.FetchVendingLog(Convert.ToInt64(userId));
                return PrepareResponse(HttpStatusCode.OK, $"{transactions.Count()} returned", false, transactions);
            }
            
        }

        [HttpGet("orders")]
        public IActionResult FetchAffordableOrders(DateTime? startdate = null, DateTime? endDate = null)
        {
            var userRole = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role).Value;
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            
            if(userRole.Contains(RoleTypes.SYSADMIN.GetDescription())){
                var orders = _affordables.FetchOrders(startdate ?? DateTime.Today, endDate ?? DateTime.Now, null);
                return PrepareResponse(HttpStatusCode.OK, $"{orders.Count()} returned", false, _mapper.Map<List<VendingModel>>(orders));
            }else{
                var orders = _affordables.FetchOrders(startdate ?? DateTime.Today, endDate ?? DateTime.Now, Convert.ToInt64(userId));
                return PrepareResponse(HttpStatusCode.OK, $"{orders.Count()} returned", false,  _mapper.Map<List<VendingModel>>(orders));
            }            
        }

        [HttpGet("order")]
        public IActionResult FetchAffordableOrderByOrderNumber(string orderNumber)
        {
            var order = _affordables.FetchOrder(orderNumber);
            if(order == null){
                return PrepareResponse(HttpStatusCode.NotFound, "Order not found");
            }

            return PrepareResponse(HttpStatusCode.OK, $"Order Found", false, _mapper.Map<VendingModel>(order));            
        }


        [AllowAnonymous]
        [HttpPost("complete/{transactionRef}")]
        public async Task<IActionResult> CompleteTransaction(string transactionRef, TransactionCompleted payload)
        {
            //var encKey = _configuration.GetValue<string>("EncryptionKey");
            //var decTransactionRef = WebUtility.UrlDecode(Utilities.DecryptString(encKey, transactionRef));
            var transactionDetails = _transactionRepository.FetchTransaction(transactionRef);
            if (transactionDetails != null)
            {
                if (payload.Event == "charge.success")
                {
                    var httpClient = new HttpClient
                    {
                        BaseAddress = new Uri(_configuration.GetValue<string>("KoraPayMERCHANTURL"))
                    };

                    httpClient.DefaultRequestHeaders.Add("Authorization", "Bearer " + _configuration.GetValue<string>("KoraSecretKey"));

                    var transactionRequestURL = _configuration.GetValue<string>("KoraPayFetchTransaction");
                    var response = await httpClient.GetAsync($"{transactionRequestURL}{payload.Data.Reference}");
                    if (response.IsSuccessStatusCode)
                    {
                        var responseBody = await response.Content.ReadAsAsync<KoraPayTransactionResponse<KoraPayTransactionInfo>>();
                        if (responseBody.Data.Status == "success")
                        {
                            transactionDetails.BillStatus = (int)BillStatus.Paid;
                            transactionDetails.DatePaid = DateTime.Now;

                            _transactionRepository.UpdateTransation(transactionDetails);
                            var fundRequest = _walletRepository.FulfillAutoFundingRequest(transactionRef);
                            if (fundRequest)
                            {
                                return PrepareResponse(HttpStatusCode.OK, "Transaction has been completed successfully", false);
                            }
                            else
                            {
                                return PrepareResponse(HttpStatusCode.BadRequest, "Error occured while fulfilling trnasaction", true);
                            }
                            
                        }                        
                    }

                    transactionDetails.BillStatus = (int)BillStatus.PaymentFailed;
                    transactionDetails.DatePaid = DateTime.Now;
                    _transactionRepository.UpdateTransation(transactionDetails);
                    return PrepareResponse(HttpStatusCode.OK, "Transaction could not verfied successfully", true);
                }
                
                transactionDetails.BillStatus = (int)BillStatus.PaymentFailed;
                transactionDetails.DatePaid = DateTime.Now;
                _transactionRepository.UpdateTransation(transactionDetails);
                return PrepareResponse(HttpStatusCode.BadRequest, "Payment Failed", true);
            }
            else
            {
                return PrepareResponse(HttpStatusCode.NotFound, "Transaction Not Found", true);
            }
        }

        [HttpGet("{txnRef}")]
        public async Task<IActionResult> FetchTransactionByRef(string txnRef)
        {
            //var encKey = _configuration.GetValue<string>("EncryptionKey");
            //var decryptedTxnRef = WebUtility.UrlDecode(Utilities.DecryptString(encKey, txnRef));
            var transaction = _transactionRepository.FetchTransaction(txnRef);
            if(transaction != null)
            {
                var walletToFund = await _walletRepository.FetchWalletInTransaction(txnRef);
                var _transactionReturned = _mapper.Map<TransactionModel>(transaction);
                _transactionReturned.PublicKey = _configuration.GetValue<string>("koraPublicKey");
                _transactionReturned.TechFee = _configuration.GetValue<decimal>("TechFee");
                _transactionReturned.TotalAmount = _transactionReturned.Amount + _transactionReturned.TechFee;
                _transactionReturned.WalletName = walletToFund.WalletName;

                return PrepareResponse(HttpStatusCode.OK, "Initiated Transaction Found", false, _transactionReturned);
            }
            else
            {
                return PrepareResponse(HttpStatusCode.NotFound, "No transaction matched the reference");
            }
        }
    }
}
