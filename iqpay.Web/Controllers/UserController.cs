﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using iqpay.Engine.Base.Repository.EmailRepository;
using iqpay.Data.Entities.Email;
using iqpay.Engine.Base.Entities;
using iqpay.Engine.Repository.EmailRepository;
using iqpay.Data.Entities.AuditTrail;
using Microsoft.AspNetCore.Authorization;
using iqpay.Data.UserManagement.Model;
using iqpay.Web.Models;
using iqpay.Repository.Users;
using iqpay.Core.Utilities;
using iqpay.Repository.Wallet;
using iqpay.Repository.VFD;
using iqpay.Data.Entities.Payment;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using iqpay.Repository.SMSRepository;
using iqpay.Data.Entities.Notification;
using iqpay.Repository.Wema;
using iqpay.Repository.ServicesRepository;

namespace iqpay.Web.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UserController : BaseController
    {

        private readonly IEmailManager<EmailLog, EmailTemplate> _emailManager;
        private readonly IUserRepository _userRepository;
        private readonly IWalletRepository _walletRepository;
        private readonly IVendImplementation _vendImp;
        private readonly IVendorRepository _vendorRepo;
        private readonly IServicesRepository _serviceRepository;
        private readonly ISMSManager _smsManager;
        private readonly IWemaRepository _wemaRepo;
        private readonly IVFDRequests _vfdRequests;
        private readonly IPartnershipRegRepo _partnershipRepo;

        public UserController(
            UserManager<ApplicationUser> userManager, 
            RoleManager<ApplicationRole> roleManager,
            SignInManager<ApplicationUser> signInManager, 
            IConfiguration configuration, IVFDRequests vfdRequests,
            IMapper mapper, ILogger<UserController> logger,
            IEmailManager<EmailLog, EmailTemplate> emailManager,
            IAuditTrailManager<AuditTrail> auditTrailManager, ISMSManager smsManager,
            IUserRepository userRepository, IWalletRepository walletRepository,
            IPartnershipRegRepo partnershipRepo,
            IServicesRepository serviceRepository,IVendImplementation vendImp, IVendorRepository vendorRepo, IWemaRepository wemaRepo
            ) : base(
                userManager,
                signInManager,
                roleManager,
                configuration,
                mapper,
                logger,
                auditTrailManager
                )
        {
            _emailManager = emailManager;
            _userRepository = userRepository;
            _walletRepository = walletRepository;
            _serviceRepository = serviceRepository;
            _vendImp = vendImp;
            _vendorRepo = vendorRepo;
            _smsManager = smsManager;
            _wemaRepo = wemaRepo;
            _vfdRequests = vfdRequests;
            _partnershipRepo = partnershipRepo;
            
        }

        /// <summary>
        ///Create a public regular customer.
        /// </summary>
        /// <param name="Email">Email address of the public customer</param>
        /// <param name="Firstname">Firstname of the public customer</param>
        /// <param name="Surname">Surname of the public customer</param>
        /// <param name="Phonenumber">Phone number of the public customer</param>
        /// <param name="Password">Password of the public customer</param>
        /// <param name="ConfirmPassword">Password confirmation</param>
        /// <returns>A payload that describes the outcome of the registration</returns>
        [HttpPost("signup")]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            //var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var emailtemplate = _emailManager.GetEmailTemplate(VatEmailTemplateType.AccountCreation).FirstOrDefault(x => x.IsActive);
            if (emailtemplate != null)
            {

                var prefix = _configuration["WEMA:Prefix"];
                var fiCode = _configuration["WEMA:FICode"];
                var virtualAccount = _wemaRepo.GenerateNUBAN(prefix, fiCode);
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    PhoneNumber = model.Phonenumber,
                    FirstName = model.Firstname,
                    LastName = model.Surname,
                    AccountType = AccountType.REGULAR,
                    VirtualAccountNumber = virtualAccount
                    //SuperDealerId = Convert.ToInt64(userId)

                };

                var userCreated = await _userManager.CreateAsync(user, model.Password);
                if (userCreated.Succeeded)
                {                    
                    var verificationCode = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var verifyURL = _configuration["VERIFYURL"] + "?code=" + Uri.EscapeDataString(verificationCode) + "&userId="+user.Id;
                    var emailBody = emailtemplate.EmailBody.Replace("{VerificationURL}", verifyURL);
                    emailBody = emailBody.Replace("{USERNAME}", user.UserName).Replace("{EMAILASSETS}", Request.Host+"/email").Replace("{DealerCode}", user.UserName);                    
                    await _emailManager.LogEmail(new EmailLog
                    {
                        //BCC = "timileyinayegbayo@gmail.com",
                        //CC= "timileyinayegbayo@gmail.com",
                        CreatedBy = user.UserName,
                        MailBody = emailBody,
                        Receiver = user.Email,
                        Sender = emailtemplate.EmailSender,
                        Subject = emailtemplate.EmailSubject,
                        DateCreated = DateTime.Now,
                        DateToSend = DateTime.Now,
                        Status = VatEmailStatus.Fresh,
                        SendImmediately = true
                    });

                    await _auditTrailManager.AddAuditTrail(new AuditTrail {
                        ActionTaken = AuditAction.Create,
                        DateCreated = DateTime.Now,
                        Description = $"User {user.UserName} has been created successfully",
                        Entity = "User",
                        UserId = user.Id,
                        UserName = user.UserName,                        
                    });
                    await _userManager.AddToRoleAsync(user, RoleTypes.REGULAR_CUSTOMER.GetDescription());
                    await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Transaction,CommissionMode.Base);
                    await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Commission,CommissionMode.Base);

                    return PrepareResponse(HttpStatusCode.OK, "Account has been created successfully, a mail has been sent to your email address to validate your email address", false);
                }
                else
                {
                    return PrepareResponse(HttpStatusCode.BadRequest, userCreated.Errors.FirstOrDefault().Description, true, userCreated.Errors);
                }

            }
            return PrepareResponse(HttpStatusCode.PreconditionFailed, "Email Templates have not been setup", true, null);
        }

        [HttpPost("register-partner-dealer")]
        public async Task<IActionResult> RegisterPartnerDealer(RegisterPartnerDealer model)
        {
           var userId  = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var emailtemplate = _emailManager.GetEmailTemplate(VatEmailTemplateType.AccountCreation).FirstOrDefault(x => x.IsActive);
            if (emailtemplate != null)
            {

                var prefix = _configuration["WEMA:Prefix"];
                var fiCode = _configuration["WEMA:FICode"];

                var partnerrequest = await _partnershipRepo.GetPartnershipRegistration(model.Id);
                partnerrequest.ApprovedBy = Convert.ToInt64(userId);
                partnerrequest.Status = 2;
                var virtualAccount = _wemaRepo.GenerateNUBAN(prefix, fiCode);
                
                var vfdAccount = await _vfdRequests.ReserveAccount(partnerrequest.Firstname, partnerrequest.Surname, null, partnerrequest.AddressLine1, partnerrequest.Phonenumber, partnerrequest.BVN);
                var accountType = AccountType.DEALER;
                if(model.UserType == AccountType.DEALER.GetDescription())
                {
                    accountType = AccountType.DEALER;
                }
                else if(model.UserType == AccountType.SUPER_DEALER.GetDescription())
                {
                    accountType = AccountType.SUPER_DEALER;                    
                }
                else if(model.UserType == AccountType.AGENT.GetDescription())
                {
                    accountType = AccountType.AGENT;
                }
                var user = new ApplicationUser
                {
                    UserName = GenerateNextDealerCode(),//model.Email,
                    Email = partnerrequest.Email ,
                    PhoneNumber = partnerrequest.Phonenumber,
                    Name = partnerrequest.CompanyName,
                    Address = partnerrequest.AddressLine1,
                    AddressLine2 = partnerrequest.AddressLine2,
                    SuperDealerId = Convert.ToInt64(userId),
                    AccountType = accountType,
                    VirtualAccountNumber = virtualAccount,
                    VirtualAccountName = $"{partnerrequest.Firstname} {partnerrequest.Surname}",
                    VFDAccountNumber = vfdAccount.AccountNo,
                    VFDAccountName = $"{vfdAccount.Firstname} {vfdAccount.Lastname}",
                    BVN = partnerrequest.BVN,
                    NIN = partnerrequest.NIN
                };
                var password = "crystal123@@";
                var userCreated = await _userManager.CreateAsync(user, password);
                if (userCreated.Succeeded)
                {

                   // var verificationCode = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                   // var verifyURL = _configuration["VERIFYURL"] + "?code=" + Uri.EscapeDataString(verificationCode) + "&userId=" + user.Id;
                    
                    var passwordResetCode = await _userManager.GenerateChangePhoneNumberTokenAsync(user, user.PhoneNumber);                    
                    var verifyURL = _configuration["PASSWORDRESETURL"] + "?code=" + Uri.EscapeDataString(passwordResetCode)+"&userId="+user.Id;
                    
                    var emailBody = emailtemplate.EmailBody.Replace("{VerificationURL}", verifyURL);                   
                    emailBody = emailBody.Replace("{USERNAME}", user.UserName).Replace("{Name}",user.Name).Replace("{CODE}", passwordResetCode).Replace("{DealerCode}", user.UserName);
                    var smsBody = emailtemplate.SMSContent.Replace("{DealerCode}", user.UserName).Replace("{CODE}", passwordResetCode).Replace("{Name}", user.Name).Replace("{VerificationURL}", verifyURL);
                    await _smsManager.CreateSMSLog(new SMSLog{
                        Content = smsBody,
                        DateToSend = DateTime.Now,
                        Receiver = partnerrequest.Phonenumber,
                        IsSendImmediately = true,
                        Status = SMSStatusEnum.Fresh                        
                    });
                    
                    await _emailManager.LogEmail(new EmailLog
                    {
                        //BCC = "timileyinayegbayo@gmail.com",
                        //CC= "timileyinayegbayo@gmail.com",
                        CreatedBy = user.UserName,
                        MailBody = emailBody,
                        Receiver = user.Email,
                        Sender = emailtemplate.EmailSender,
                        Subject = emailtemplate.EmailSubject,
                        DateCreated = DateTime.Now,
                        DateToSend = DateTime.Now,
                        Status = VatEmailStatus.Fresh,
                        SendImmediately = true
                    });

                    await _auditTrailManager.AddAuditTrail(new AuditTrail
                    {
                        ActionTaken = AuditAction.Create,
                        DateCreated = DateTime.Now,
                        Description = $"Dealer {user.UserName} has been created successfully",
                        Entity = "User",
                        UserId = user.Id,
                        UserName = user.UserName,
                    });
                   
                    await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.Base);
                    await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Commission, CommissionMode.Base);

                    var commissionAheadServices = _serviceRepository.ServicesBasedOnCommissionMode(CommissionMode.CommissionAhead);
                    foreach (var service in commissionAheadServices)
                    {
                        if(!service.Vendor.IsCommissionAhead)
                        {                                
                            await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.CommissionAhead, service.Id, service.ServiceName);
                        }
                    }

                    var commissionAheadVendors = await _vendorRepo.FetchCommissionAheadVendors();
                    foreach(var vendor in commissionAheadVendors)
                    {
                        await _walletRepository.CreateVendorWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.CommissionAhead, vendor.Id, vendor.VendorName);
                    } 
                    
                    await _userManager.AddToRoleAsync(user, model.UserType);
                    return PrepareResponse(HttpStatusCode.OK, "Dealer has been created successfully, a mail has been sent to the contact email address", false);
                }
                else
                {
                    return PrepareResponse(HttpStatusCode.BadRequest, userCreated.Errors.FirstOrDefault().Description, true, userCreated.Errors);
                }

            }
            return PrepareResponse(HttpStatusCode.PreconditionFailed, "Email Templates have not been setup", true, null);
        }

        [HttpGet("partnership-requests")]
        public async Task<IActionResult> FetchPartnershipRequests()
        {
            var userId  = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var userRole = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role).Value;
            var _req = new List<PartnershipRegistration>();
            if(userRole.Contains(RoleTypes.SYSADMIN.GetDescription())){
                _req = await _partnershipRepo.FetchPartnershipRegistration(null);
            }else{
                _req = await _partnershipRepo.FetchPartnershipRegistration(Convert.ToInt64(userId));
            }
            var requests = _mapper.Map<List<PartnerRegisterViewModel>>(_req);
            
            return PrepareResponse(HttpStatusCode.OK, "Partnership requests returned", false, requests);
        }

        [HttpGet("partnership-requests/{id}")]
        public async Task<IActionResult> FetchPartnershipRequests(long id)
        {
            var requests = _mapper.Map<PartnerRegisterViewModel>(await _partnershipRepo.GetPartnershipRegistration(id));
            return PrepareResponse(HttpStatusCode.OK, "Partnership requests returned", false, requests);
        }

        [HttpPut("/{id}/permissions")]
        public async Task<IActionResult> UpdatePermissions(long id, PermissionsModel model)
        {
            var user = _userManager.Users.FirstOrDefault(x=>x.Id == id);
            if(user != null){
                user.Permissions = model.Permissions;
                await _userManager.UpdateAsync(user);
                return PrepareResponse(HttpStatusCode.OK, "User permissions have been saved successfully", false);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "Soecified admin could not be found");
        }

        [HttpPost("paternership-registration")]
        public async Task<IActionResult> PaternshipRegistration([FromForm]PartnerRegisterViewModel model)
        {
                if(model.PhotoID.Length > 1024*1024){
                    return PrepareResponse(HttpStatusCode.PreconditionFailed, "Please upload a photo ID not greater than 1MB");
                }
                var existingRec = await _partnershipRepo.GetPartnershipRegistrationByBVN(model.BVN, model.Email, model.NIN);
                if(!existingRec.Any())
                {
                    var verification = await _vfdRequests.VerifyBVN(model.BVN);                 
                    if(verification.LastName.ToUpper() == model.Surname.ToUpper() && verification.PhoneNo == model.Phonenumber)
                    {
                        var patRegModel = _mapper.Map<PartnershipRegistration>(model);
                        if(!string.IsNullOrWhiteSpace(verification.PixBase64))
                        {
                            var bvnImagePath = Path.Combine("wwwroot",_configuration.GetValue<string>("BVNImagePATH"),$"{model.BVN}.jpg");
                            var bytes = Convert.FromBase64String(verification.PixBase64);
                            using (var imageFile = new FileStream(bvnImagePath, FileMode.Create))
                            {
                                imageFile.Write(bytes ,0, bytes.Length);
                                imageFile.Flush();
                            }
                        }
                        var file = model.PhotoID;
                        var allowedExtensions = new string[] {"jpg","jpeg" ,"png", "gif"};
                        var extension = file.FileName.Split('.');
                        if(allowedExtensions.Contains(extension[1]))
                        {
                            var fileLoc = Path.Combine("wwwroot",_configuration.GetValue<string>("PhotoIDPATH"),$"{model.BVN}.{extension[1]}");
                            using (var fileStream = new FileStream(fileLoc, FileMode.Create)) {
                                await model.PhotoID.CopyToAsync(fileStream);
                            }
                            patRegModel.PhotoIdLoc = $"{model.BVN}.{extension[1]}";
                            _partnershipRepo.CreateNewPartnershipRequest(patRegModel);                    
                            return PrepareResponse(HttpStatusCode.OK, "Partnership Request has been submitted successfully. A representative would get to soon.", false);
                        }else{
                            return PrepareResponse(HttpStatusCode.Conflict, "File uploaded does not have required extensions");
                        }
                                            
                    }

                    return PrepareResponse(HttpStatusCode.NotAcceptable, "BVN Mismatch. Please provide a valid BVN");
                }else{
                    return PrepareResponse(HttpStatusCode.Conflict, "These details exists already. Please try again.");
                }
                
            
        }

        /// <summary>
        /// Checks if user has been deactivated
        /// </summary>
        /// <returns>A dealer status</returns>
        [Authorize]
        [HttpPost("isdeactivated")]
        public async Task<IActionResult> CheckDeactivated()
        {
            var username = User?.Identity?.Name;
            if(username != null)
            {
                var user = await _userManager.FindByNameAsync(username);
                if(user != null)
                {
                    if(user.LockoutEnd.HasValue) 
                    {
                        return PrepareResponse(HttpStatusCode.Unauthorized, "User has been deactivated");
                    } 
                }

            }
            return PrepareResponse(HttpStatusCode.OK, "User is still active", false);
        }


        /// <summary>
        /// Register a dealer.
        /// </summary>
        /// <param name="Email">The contact email of the agent</param>
        /// <param name="Name">The name of the agent</param>
        /// <param name="Phonenumber">The phone number of the agent</param>
        /// <param name="Address">The address of the agent</param>
        /// <param name="Password">The password of the agent</param>
        /// <param name="ConfirmPassword">Password confirmation</param>
        /// <returns>A string status</returns>
        [Authorize(Roles = "SUPER DEALER,SYSTEM ADMIN")]
        [HttpPost("create-dealer")]
        public async Task<IActionResult> DealerRegister(DealerRegisterViewModel model)
        {
            var userId  = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var emailtemplate = _emailManager.GetEmailTemplate(VatEmailTemplateType.AccountCreation).FirstOrDefault(x => x.IsActive);
            if (emailtemplate != null)
            {

                var prefix = _configuration["WEMA:Prefix"];
                var fiCode = _configuration["WEMA:FICode"];
                var virtualAccount = _wemaRepo.GenerateNUBAN(prefix, fiCode);
                var user = new ApplicationUser
                {
                    UserName = GenerateNextDealerCode(),//model.Email,
                    Email = model.Email,
                    PhoneNumber = model.Phonenumber,
                    Name = model.Name,
                    Address = model.Address,
                    SuperDealerId = Convert.ToInt64(userId),
                    AccountType = AccountType.DEALER,
                    VirtualAccountNumber = virtualAccount
                };

                var userCreated = await _userManager.CreateAsync(user, model.Password);
                if (userCreated.Succeeded)
                {

                   // var verificationCode = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                   // var verifyURL = _configuration["VERIFYURL"] + "?code=" + Uri.EscapeDataString(verificationCode) + "&userId=" + user.Id;
                    
                    var passwordResetCode = await _userManager.GenerateChangePhoneNumberTokenAsync(user, user.PhoneNumber);                    
                    var verifyURL = _configuration["PASSWORDRESETURL"] + "?code=" + Uri.EscapeDataString(passwordResetCode)+"&userId="+user.Id;
                    
                    var emailBody = emailtemplate.EmailBody.Replace("{VerificationURL}", verifyURL);                   
                    emailBody = emailBody.Replace("{USERNAME}", user.UserName).Replace("{Name}",user.Name).Replace("{CODE}", passwordResetCode).Replace("{DealerCode}", user.UserName);
                    var smsBody = emailtemplate.SMSContent.Replace("{DealerCode}", user.UserName).Replace("{CODE}", passwordResetCode).Replace("{Name}", user.Name).Replace("{VerificationURL}", verifyURL);
                    await _smsManager.CreateSMSLog(new SMSLog{
                        Content = smsBody,
                        DateToSend = DateTime.Now,
                        Receiver = model.Phonenumber,
                        IsSendImmediately = true,
                        Status = SMSStatusEnum.Fresh                        
                    });
                    
                    await _emailManager.LogEmail(new EmailLog
                    {
                        //BCC = "timileyinayegbayo@gmail.com",
                        //CC= "timileyinayegbayo@gmail.com",
                        CreatedBy = user.UserName,
                        MailBody = emailBody,
                        Receiver = user.Email,
                        Sender = emailtemplate.EmailSender,
                        Subject = emailtemplate.EmailSubject,
                        DateCreated = DateTime.Now,
                        DateToSend = DateTime.Now,
                        Status = VatEmailStatus.Fresh,
                        SendImmediately = true
                    });

                    await _auditTrailManager.AddAuditTrail(new AuditTrail
                    {
                        ActionTaken = AuditAction.Create,
                        DateCreated = DateTime.Now,
                        Description = $"Dealer {user.UserName} has been created successfully",
                        Entity = "User",
                        UserId = user.Id,
                        UserName = user.UserName,
                    });
                   
                    await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.Base);
                    await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Commission, CommissionMode.Base);

                    var commissionAheadServices = _serviceRepository.ServicesBasedOnCommissionMode(CommissionMode.CommissionAhead);
                    foreach (var service in commissionAheadServices)
                    {
                        if(!service.Vendor.IsCommissionAhead)
                        {                                
                            await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.CommissionAhead, service.Id, service.ServiceName);
                        }
                    }

                    var commissionAheadVendors = await _vendorRepo.FetchCommissionAheadVendors();
                    foreach(var vendor in commissionAheadVendors)
                    {
                        await _walletRepository.CreateVendorWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.CommissionAhead, vendor.Id, vendor.VendorName);
                    } 
                    
                    await _userManager.AddToRoleAsync(user, RoleTypes.DEALER.GetDescription());
                    return PrepareResponse(HttpStatusCode.OK, "Dealer has been created successfully, a mail has been sent to the contact email address", false);
                }
                else
                {
                    return PrepareResponse(HttpStatusCode.BadRequest, userCreated.Errors.FirstOrDefault().Description, true, userCreated.Errors);
                }

            }
            return PrepareResponse(HttpStatusCode.PreconditionFailed, "Email Templates have not been setup", true, null);
        }


        /// <summary>
        /// Fetch dealers.
        /// </summary>       
        /// <returns>A list of dealers registered under user</returns>
        [Authorize(Roles = "SUPER DEALER,DEALER")]
        [HttpGet("dealers")]
        public async Task<IActionResult> FetchDealers()
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var dealers = await _userRepository.FetchAgents(Convert.ToInt64(userId));
            return PrepareResponse(HttpStatusCode.OK, $"{dealers.Count} dealers found", false, _mapper.Map<List<AgentViewModel>>(dealers));
        }

        /// <summary>
        /// Fetch super dealers.
        /// </summary>       
        /// <returns>A list of dealers registered under user</returns>
        [Authorize(Roles = "SYSTEM ADMIN")]
        [HttpGet("super-dealers")]
        public async Task<IActionResult> FetchSuperDealers()
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var superDealers = await _userRepository.FetchSuperDealers();
            return PrepareResponse(HttpStatusCode.OK, $"{superDealers.Count} dealers found", false, _mapper.Map<List<AgentViewModel>>(superDealers));
        }

        /// <summary>
        /// Fetch agents.
        /// </summary>       
        /// <returns>A list of agents registered under user</returns>
        [Authorize(Roles = "SUPER DEALER,DEALER,SYSTEM ADMIN")]
        [HttpGet("agents")]
        public async Task<IActionResult> FetchAgents()
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var agents = await _userRepository.FetchAgents(Convert.ToInt64(userId));
            return PrepareResponse(HttpStatusCode.OK, $"{agents.Count} dealers found", false, _mapper.Map<List<AgentViewModel>>(agents));
        }

        /// <summary>
        /// Fetch user profile.
        /// </summary>
        /// <returns>Payload of data encapsulating logged in user profile</returns>
        [Authorize]
        [HttpGet("profile")]
        public async Task<IActionResult> FetchUserProfile()
        {
            var userId = User.Claims.FirstOrDefault(x=>x.Type == JwtRegisteredClaimNames.Jti).Value;
            var userInfo = await _userManager.FindByIdAsync(userId);
            var profileInfo = _mapper.Map<UserModel>(userInfo);
            if(userInfo != null)            
            {
                if(userInfo.SuperDealerId.HasValue)
                {
                    var superdealer = await _userManager.FindByIdAsync(userInfo.SuperDealerId.Value.ToString());
                    if(superdealer != null)
                    {
                        profileInfo.SuperDealer = superdealer.Name;
                    }
                    
                }
                profileInfo.ProfilePicLoc = $"/{_configuration.GetValue<string>("LOGOPATH")}/{userInfo.ProfileImageLoc}";
               
                return PrepareResponse(HttpStatusCode.OK, "User details found successfully", false, profileInfo);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "User profile not found", true);
        }


        /// <summary>
        /// upload user profile image or company logo.
        /// </summary>
        /// <returns>Payload stating if image upload was successful or not</returns>
        [Authorize]
        [HttpPut("company-logo")]
        public async Task<IActionResult> UpdateComopanyLogo(IFormFile file)
        {
            var userId = User.Claims.FirstOrDefault(x=>x.Type == JwtRegisteredClaimNames.Jti).Value;
            var user = await _userManager.FindByIdAsync(userId);
            var allowedExtensions = new string[] {"jpg","jpeg" ,"png", "gif"};
            if(file != null)
            {
                var fileParts = file.FileName.Split('.');
                var fileExtension = fileParts[fileParts.Length - 1];
                if(allowedExtensions.Contains(fileExtension.ToLower()))
                {
                    var logoFolderPath = _configuration.GetValue<string>("LOGOPATH"); 
                    var fileName = userId+"."+fileExtension;                   
                    var logoFilePath = Path.Combine("wwwroot",logoFolderPath,fileName);
                    using(var fs = new FileStream(logoFilePath,FileMode.Create))
                    {
                        file.CopyTo(fs);
                        user.ProfileImageLoc = fileName;

                        await _userManager.UpdateAsync(user);
                        return PrepareResponse(HttpStatusCode.OK, "Profile Image has been uploaded successfully", false);
                    }

                }
                return PrepareResponse(HttpStatusCode.UnsupportedMediaType, $"Please upload an image within the given extensions {string.Join(',',allowedExtensions)}", true);
            }
            return PrepareResponse(HttpStatusCode.ExpectationFailed, "Please upload a file", true);

        }




        /// <summary>
        /// Update user profile.
        /// </summary>
        /// <returns>Payload stating if profile update was successful or not</returns>
        [Authorize]
        [HttpPut("profile")]
        public async Task<IActionResult> UpdateUserProfile(UserModel model)
        {
            var userId = User.Claims.FirstOrDefault(x=>x.Type == JwtRegisteredClaimNames.Jti).Value;
            var userInfo = await _userManager.FindByIdAsync(userId);
            userInfo.Address = model.Address;
            userInfo.FirstName = model.FirstName;
            userInfo.LastName = model.LastName;
            userInfo.Name = model.Name;
            userInfo.PhoneNumber = model.PhoneNumber;

            var updated = await _userManager.UpdateAsync(userInfo);
            if(updated.Succeeded)
            {
                return PrepareResponse(HttpStatusCode.OK, "Profile has been updated successfully", false, _mapper.Map<UserModel>(userInfo));
            }else{
                var error = updated.Errors.FirstOrDefault().Description;
                return PrepareResponse(HttpStatusCode.BadGateway, "Profile Update failed", true);
            }
        }

        /// <summary>
        /// Fetch Wallets.
        /// </summary>
        /// <returns>A list of wallets that have been assigned to the user</returns>
        [Authorize]
        [HttpGet("wallets")]
        public async Task<IActionResult> FetchWallets()
        {
            var userId = User.Claims.FirstOrDefault(x=>x.Type == JwtRegisteredClaimNames.Jti).Value;

            var wallets = await _walletRepository.FetchWallets(Convert.ToInt64(userId));
            if(wallets.Count > 0)
            {
                return PrepareResponse(HttpStatusCode.OK, "Wallets Found", false, _mapper.Map<List<WalletModel>>(wallets));
            }
            else
            {
                return PrepareResponse(HttpStatusCode.NotFound, "No wallet found", true);
            }
            
        }

        [Authorize]
        [HttpGet("dashboard")]
        public async Task<IActionResult> FetchDashboardData()
        {
            var userId = User.Claims.FirstOrDefault(x=>x.Type == JwtRegisteredClaimNames.Jti).Value;
            var userRole = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role).Value;
            var longUserId = Convert.ToInt64(userId);
            var dashboardData = new DashboardModel();
            
            if(userRole.Contains(RoleTypes.SYSADMIN.GetDescription()))
            {
                var transactions = _vendImp.FetchVendingLog();
                dashboardData.TransactionCount = transactions.Count();
                dashboardData.Agents = _userManager.Users.Count(x=>x.IsActive && !x.IsDeleted && x.AccountType == AccountType.AGENT);
                dashboardData.SuperDealers = _userManager.Users.Count(x=>x.IsActive && !x.IsDeleted && x.AccountType == AccountType.SUPER_DEALER);
                dashboardData.Dealers = _userManager.Users.Count(x=>x.IsActive && !x.IsDeleted && x.AccountType == AccountType.DEALER);
                dashboardData.TodayIncome = transactions.Where(x=>x.DateCreated.Date == DateTime.Today ).Sum(x=>x.Amount);
                var wallets = await _walletRepository.FetchWallets(longUserId);
                var walletBalance = wallets.Where(x=>x.Type == WalletType.Transaction).Sum(x=>x.Balance);
                dashboardData.WalletBalance = walletBalance;
            }else{
                
                var transactions = _vendImp.FetchVendingLog(longUserId);
                var commWallet = await _walletRepository.FetchCommissionWallet(longUserId);
                var wallets = await _walletRepository.FetchWallets(longUserId);
                var walletBalance = wallets.Where(x=>x.Type == WalletType.Transaction).Sum(x=>x.Balance);
                dashboardData.TransactionCount = transactions.Count();
                dashboardData.Agents = _userManager.Users.Count(x=>x.IsActive && !x.IsDeleted && x.AccountType == AccountType.AGENT && x.SuperDealerId == longUserId);
                dashboardData.SuperDealers = _userManager.Users.Count(x=>x.IsActive && !x.IsDeleted && x.AccountType == AccountType.SUPER_DEALER && x.SuperDealerId == longUserId);
                dashboardData.Dealers = dashboardData.Agents = _userManager.Users.Count(x=>x.IsActive && !x.IsDeleted && x.AccountType == AccountType.DEALER && x.SuperDealerId == longUserId);
                dashboardData.TodayIncome = transactions.Where(x=>x.DateCreated.Date == DateTime.Today ).Sum(x=>x.Amount);
                dashboardData.CommissionBalance = commWallet.Balance;
                dashboardData.WalletBalance = walletBalance;
            }   

            return PrepareResponse(HttpStatusCode.OK,"Dashbord data returned", false, dashboardData );          
        }            

        /// <summary>
        /// Register a dealer.
        /// </summary>
        /// <param name="Email">The contact email of the agent</param>
        /// <param name="Name">The name of the agent</param>
        /// <param name="Phonenumber">The phone number of the agent</param>
        /// <param name="Address">The address of the agent</param>
        /// <param name="Password">The password of the agent</param>
        /// <param name="ConfirmPassword">Password confirmation</param>
        /// <returns>A string status</returns>
        [Authorize(Roles = "SYSTEM ADMIN")]
        [HttpPost("create-superdealer")]
        public async Task<IActionResult> SubDealerRegister(DealerRegisterViewModel model)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var emailtemplate = _emailManager.GetEmailTemplate(VatEmailTemplateType.AccountCreation).FirstOrDefault(x => x.IsActive);
            if (emailtemplate != null)
            {

                var prefix = _configuration["WEMA:Prefix"];
                var fiCode = _configuration["WEMA:FICode"];
                var virtualAccount = _wemaRepo.GenerateNUBAN(prefix, fiCode);
                var user = new ApplicationUser
                {
                    UserName = GenerateNextDealerCode(),//model.Email,
                    Email = model.Email,
                    PhoneNumber = model.Phonenumber,
                    Name = model.Name,
                    Address = model.Address,
                    AccountType = AccountType.SUPER_DEALER,
                    VirtualAccountNumber = virtualAccount
                };

                var userCreated = await _userManager.CreateAsync(user, model.Password);
                if (userCreated.Succeeded)
                {
                    //Send Confirmation Link
                    var passwordResetCode = await _userManager.GenerateChangePhoneNumberTokenAsync(user, user.PhoneNumber);                    
                    var verifyURL = _configuration["PASSWORDRESETURL"] + "?code=" + Uri.EscapeDataString(passwordResetCode)+"&userId="+user.Id;
                                    
                    var emailBody = emailtemplate.EmailBody.Replace("{VerificationURL}", verifyURL);
                    emailBody = emailBody.Replace("{USERNAME}", user.UserName).Replace("{Name}",user.Name).Replace("{CODE}", passwordResetCode).Replace("{DealerCode}", user.UserName);

                    var smsBody = emailtemplate.SMSContent.Replace("{DealerCode}", user.UserName).Replace("{CODE}", passwordResetCode).Replace("{Name}", user.Name).Replace("{VerificationURL}", verifyURL);
                    await _smsManager.CreateSMSLog(new SMSLog{
                        Content = smsBody,
                        DateToSend = DateTime.Now,
                        Receiver = model.Phonenumber,
                        IsSendImmediately = true,
                        Status = SMSStatusEnum.Fresh                        
                    });

                    await _emailManager.LogEmail(new EmailLog
                    {
                        //BCC = "timileyinayegbayo@gmail.com",
                        //CC= "timileyinayegbayo@gmail.com",
                        CreatedBy = user.UserName,
                        MailBody = emailBody,
                        Receiver = user.Email,
                        Sender = emailtemplate.EmailSender,
                        Subject = emailtemplate.EmailSubject,
                        DateCreated = DateTime.Now,
                        DateToSend = DateTime.Now,
                        Status = VatEmailStatus.Fresh,
                        SendImmediately = true
                    });

                    await _auditTrailManager.AddAuditTrail(new AuditTrail
                    {
                        ActionTaken = AuditAction.Create,
                        DateCreated = DateTime.Now,
                        Description = $"IQISUPERDEALER {user.UserName} has been created successfully",
                        Entity = "User",
                        UserId = user.Id,
                        UserName = user.UserName,
                    });


                    await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.Base);
                    await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Commission, CommissionMode.Base);

                    var commissionAheadServices = _serviceRepository.ServicesBasedOnCommissionMode(CommissionMode.CommissionAhead);
                    if (commissionAheadServices.Count() > 0)
                    {
                        var wallets = new List<Wallet>();
                        foreach (var service in commissionAheadServices)
                        {
                            if(!service.Vendor.IsCommissionAhead)
                            {                                
                                await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.CommissionAhead, service.Id, service.ServiceName);
                            }
                        }
                    }

                    var commissionAheadVendors = await _vendorRepo.FetchCommissionAheadVendors();
                    foreach(var vendor in commissionAheadVendors)
                    {
                        await _walletRepository.CreateVendorWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.CommissionAhead, vendor.Id, vendor.VendorName);
                    } 

                    await _userManager.AddToRoleAsync(user, RoleTypes.IQISUPERDEALER.GetDescription());
                    return PrepareResponse(HttpStatusCode.OK, "Dealer has been created successfully, a mail has been sent to the contact email address", false);
                }
                else
                {
                    return PrepareResponse(HttpStatusCode.BadRequest, userCreated.Errors.FirstOrDefault().Description, true, userCreated.Errors);
                }

            }
            return PrepareResponse(HttpStatusCode.PreconditionFailed, "Email Templates have not been setup", true, null);
        }

        [Authorize(Roles = "SYSTEM ADMIN")]
        [HttpGet("administrators")]
        public async Task<IActionResult> FetchAdministrators()
        {
            var role = RoleTypes.SYSADMIN.GetDescription();
           var admins =   await _userManager.GetUsersInRoleAsync(role);
           return PrepareResponse(HttpStatusCode.OK, "Administrators returned", true, _mapper.Map<List<UserModel>>(admins));
        }

        [Authorize(Roles = "SYSTEM ADMIN")]
        [HttpGet("administrators/{id}")]
        public async Task<IActionResult> FetchAdministrators(long id)
        {
            var role = RoleTypes.SYSADMIN.GetDescription();
           var admins =   await _userManager.GetUsersInRoleAsync(role);
           var admin = admins.FirstOrDefault(x=>x.Id == id);
           if(admin == null)
           {
               return PrepareResponse(HttpStatusCode.NotFound, "No administrator found");
           }
           return PrepareResponse(HttpStatusCode.OK, "Administrator returned", true, _mapper.Map<RegisterViewModel>(admin));
        }

        [Authorize(Roles = "SYSTEM ADMIN")]
        [HttpPut("update-sysadmin")]
        public async Task<IActionResult> UpdateSysAdmin(RegisterViewModel model)
        {
            var sysAdmin = await _userManager.FindByIdAsync(model.Id.ToString());
            if(sysAdmin != null)
            {
                sysAdmin.FirstName = model.Firstname;
                sysAdmin.LastName = model.Surname;
                sysAdmin.Email = model.Email;
                sysAdmin.Address = model.Address;
                sysAdmin.PhoneNumber = model.Phonenumber;

                var result = await _userManager.UpdateAsync(sysAdmin);
                if(result.Succeeded)
                {
                    return PrepareResponse(HttpStatusCode.OK, "Sysadmin data has been updated successfully", false);
                }

                return PrepareResponse(HttpStatusCode.BadRequest, result.Errors.FirstOrDefault().Description, true);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "Sysadmin not found", true);
        }
                
        [Authorize(Roles = "SYSTEM ADMIN")]
        [HttpPost("create-sysadmin")]
        public async Task<IActionResult> CreateSysAdmin(RegisterViewModel model)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var emailtemplate = _emailManager.GetEmailTemplate(VatEmailTemplateType.AccountCreation).FirstOrDefault(x => x.IsActive);
            if (emailtemplate != null)
            {
                
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    PhoneNumber = model.Phonenumber,
                    FirstName = model.Firstname,
                    LastName = model.Surname,
                    SuperDealerId = Convert.ToInt64(userId),
                    Address = model.Address
                };

                var userCreated = await _userManager.CreateAsync(user, model.Password);
                if (userCreated.Succeeded)
                {
                    //Send Confirmation Link
                    var passwordResetCode = await _userManager.GenerateChangePhoneNumberTokenAsync(user, user.PhoneNumber);                    
                    var verifyURL = _configuration["PASSWORDRESETURL"] + "?code=" + Uri.EscapeDataString(passwordResetCode)+"&userId="+user.Id;                                        
                    var emailBody = emailtemplate.EmailBody.Replace("{VerificationURL}", verifyURL);
                    emailBody = emailBody.Replace("{USERNAME}", user.UserName).Replace("{Name}",user.Name).Replace("{CODE}", passwordResetCode).Replace("{DealerCode}", user.UserName);

                    await _emailManager.LogEmail(new EmailLog
                    {
                        //BCC = "timileyinayegbayo@gmail.com",
                        //CC= "timileyinayegbayo@gmail.com",
                        CreatedBy = user.UserName,
                        MailBody = emailBody,
                        Receiver = user.Email,
                        Sender = emailtemplate.EmailSender,
                        Subject = emailtemplate.EmailSubject,
                        DateCreated = DateTime.Now,
                        DateToSend = DateTime.Now,
                        Status = VatEmailStatus.Fresh,
                        SendImmediately = true
                    });

                    await _auditTrailManager.AddAuditTrail(new AuditTrail
                    {
                        ActionTaken = AuditAction.Create,
                        DateCreated = DateTime.Now,
                        Description = $"System admin {user.UserName} has been created successfully",
                        Entity = "User",
                        UserId = user.Id,
                        UserName = user.UserName,
                    });

                    await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.Base);
                    await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Commission, CommissionMode.Base);
                    await _userManager.AddToRoleAsync(user, RoleTypes.SYSADMIN.GetDescription());
                    return PrepareResponse(HttpStatusCode.OK, "System admin has been created successfully, a mail has been sent to the address", false);
                }
                else
                {
                    return PrepareResponse(HttpStatusCode.BadRequest, userCreated.Errors.FirstOrDefault().Description, true, userCreated.Errors);
                }

            }
            return PrepareResponse(HttpStatusCode.PreconditionFailed, "Email Templates have not been setup", true, null);
        }

        private string GenerateNextDealerCode()
        {
            var firstDealerCode = "IQP00001";
            var dealersWithIQP = _userManager.Users.Where(x=>x.UserName.StartsWith("IQP")).Count();
            if(dealersWithIQP == 0)
            {
                return firstDealerCode;
            }
            else
            {
                var lastdealerWithIQP = _userManager.Users.Where(x=>x.UserName.StartsWith("IQP")).Last();
                var lastUsername = lastdealerWithIQP.UserName;
                var lastNumber = Convert.ToInt32(lastUsername.Substring(3));
                lastNumber += 1;
                var nextnumber = lastNumber.ToString().PadLeft(5,'0');
                var nextDealerCode = $"IQP{nextnumber}";
                while(_userManager.Users.Any(x=>x.UserName == nextDealerCode))
                {
                    lastNumber = Convert.ToInt32(nextnumber);
                    lastNumber += 1;
                    nextnumber = lastNumber.ToString().PadLeft(5,'0');
                    nextDealerCode = $"IQP{nextnumber}";
                }

                return nextDealerCode;
            }
        }

        [HttpGet]
        public async Task<IActionResult> FetchUserByDealerCode(string dealerCode)
        {
            var dealer = await _userManager.FindByNameAsync(dealerCode);
            if(dealer != null){
                return PrepareResponse(HttpStatusCode.OK, "Dealer found", false, _mapper.Map<AgentViewModel>(dealer));
            }
            return PrepareResponse(HttpStatusCode.NotFound, "Dealer not found");
        }


        [Authorize(Roles = "SYSTEM ADMIN")]
        [HttpPost("{superdealer}/generate3000")]
        public async Task<IActionResult> Generate3000Agents(long superdealer)
        {
            try{
            for(int i=0; i <=3000; i++)
            {
                var username = GenerateNextDealerCode();
                var user = new ApplicationUser
                {
                    UserName = username, //model.Email,
                    Email = $"{username}@iqipay.com",
                    PhoneNumber = "08096279121",
                    Name = username,
                    Address = "Address",
                    SuperDealerId = Convert.ToInt64(superdealer),
                    AccountType = AccountType.AGENT,                
                };
                var password = Convert.ToBase64String(System.Text.Encoding.Unicode.GetBytes(username));
                var userCreated = await _userManager.CreateAsync(user, password);
                if (userCreated.Succeeded)
                {
                    await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.Base);
                    await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Commission, CommissionMode.Base);

                    var commissionAheadServices = _serviceRepository.ServicesBasedOnCommissionMode(CommissionMode.CommissionAhead);
                    foreach (var service in commissionAheadServices)
                    {
                        if(!service.Vendor.IsCommissionAhead)
                        {                                
                            await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.CommissionAhead, service.Id, service.ServiceName);
                        }
                    }

                    var commissionAheadVendors = await _vendorRepo.FetchCommissionAheadVendors();
                    foreach(var vendor in commissionAheadVendors)
                    {
                        await _walletRepository.CreateVendorWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.CommissionAhead, vendor.Id, vendor.VendorName);
                    } 
                    await _userManager.AddToRoleAsync(user, RoleTypes.AGENT.GetDescription());                                                  
                    await _auditTrailManager.AddAuditTrail(new AuditTrail
                    {
                        ActionTaken = AuditAction.Create,
                        DateCreated = DateTime.Now,
                        Description = $"User {user.UserName} has been created successfully",
                        Entity = "User",
                        UserId = user.Id,
                        UserName = user.UserName,
                    });                    
                }else{
                    PrepareResponse(HttpStatusCode.BadRequest, userCreated.Errors.FirstOrDefault().Description, false);
                }
            }
            return PrepareResponse(HttpStatusCode.OK, "3000 dealers have been created successfully.", false);
            }catch(Exception ex){
                return PrepareResponse(HttpStatusCode.OK, ex.Message, true);
            }
        }

        /// <summary>
        /// Get details of reserved bank account.
        /// </summary>
        /// <returns>A string status</returns>
        [Authorize]
        [HttpGet("reservedBankDetails")]
        public async Task<IActionResult> FetchReservedBankDetails()
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var user = await _userManager.FindByIdAsync(userId);
            if(user != null)
            {
                var accountDetails = _mapper.Map<ReservedBankDetails>(user);
                return PrepareResponse(HttpStatusCode.OK, "Account details returned", false, accountDetails);
            }
            return PrepareResponse(HttpStatusCode.NotFound, "User not found");
        }


        /// <summary>
        /// Generate reserved bank account.
        /// </summary>
        /// <returns>A string status</returns>
        [Authorize]
        [HttpPost("generate-reservedBankAccount")]
        public async Task<IActionResult> GenerateReservedBankDetails()
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var user = await _userManager.FindByIdAsync(userId);
            if(user != null)
            {
                //try{
                    string dealerEmail = user.Email;
                    if(string.IsNullOrEmpty(user.VirtualAccountNumber))
                    {
                        var prefix = _configuration["WEMA:Prefix"];
                        var fiCode = _configuration["WEMA:FICode"];
                        var generatedAccount = _wemaRepo.GenerateNUBAN(prefix, fiCode);// ReserveAccount(user.UserName, "IQPAY", user.DateOfBirth, user.Address, user.PhoneNumber, user.Bvn); //_monnify.ReserveAccount(user.UserName, user.UserName, dealerEmail);
                        if(generatedAccount != null)
                        {
                            user.VirtualAccountNumber  = generatedAccount;
                            user.ReservedAccountBankName = "WEMA";
                            user.VirtualAccountName = $"IQPAY-{user.UserName}";                    
                        }
                    }
                    
                    if(string.IsNullOrEmpty(user.VFDAccountNumber))
                    {
                        var vfdGeneratedAccount = await _vfdRequests.ReserveAccount(user.UserName, "IQPAY", user.DateOfBirth, user.Address, user.PhoneNumber, user.BVN);
                        if(vfdGeneratedAccount != null)                    
                        {
                            user.VFDAccountNumber = vfdGeneratedAccount.AccountNo;
                            user.VFDAccountName = $"{vfdGeneratedAccount.Firstname} {vfdGeneratedAccount.Lastname}";
                        }
                    }

                    await _userManager.UpdateAsync(user);
                    var accountDetails = _mapper.Map<ReservedBankDetails>(user);
                    return PrepareResponse(HttpStatusCode.OK, "Reserved Account Number has been generated successfully",false, accountDetails); 

                //}//catch(Exception ex)
                //{
                   // return PrepareResponse(HttpStatusCode.InternalServerError,"Error occured while generating bank details. Please try again later");
                //}

            }else{

                return PrepareResponse(HttpStatusCode.NotFound, "User details not found");
            }
            
        }


        /// <summary>
        /// Register an agent.
        /// </summary>
        /// <param name="Email">The contact email of the agent</param>
        /// <param name="Name">The name of the agent</param>
        /// <param name="Phonenumber">The phone number of the agent</param>
        /// <param name="Address">The address of the agent</param>
        /// <param name="Password">The password of the agent</param>
        /// <param name="ConfirmPassword">Password confirmation</param>
        /// <returns>A string status</returns>
        [Authorize(Roles = "DEALER, SYSTEM ADMIN, SUPER DEALER")]
        [HttpPost("create-agent")]
        public async Task<IActionResult> AgentRegister(AgentRegisterViewModel model)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var emailtemplate = _emailManager.GetEmailTemplate(VatEmailTemplateType.AccountCreation).FirstOrDefault(x => x.IsActive);
            if (emailtemplate != null)
            {
                var prefix = _configuration["WEMA:Prefix"];
                var fiCode = _configuration["WEMA:FICode"];
                var virtualAccount = _wemaRepo.GenerateNUBAN(prefix, fiCode);
                var user = new ApplicationUser
                {
                    UserName = GenerateNextDealerCode(), //model.Email,
                    Email = model.Email,
                    PhoneNumber = model.Phonenumber,
                    Name = model.Name,
                    Address = model.Address,
                    SuperDealerId = Convert.ToInt64(userId),
                    AccountType = AccountType.AGENT,
                    VirtualAccountNumber = virtualAccount

                };

                var userCreated = await _userManager.CreateAsync(user, model.Password);
                if (userCreated.Succeeded)
                {

                   var passwordResetCode = await _userManager.GenerateChangePhoneNumberTokenAsync(user, user.PhoneNumber);                    
                    var verifyURL = _configuration["PASSWORDRESETURL"] + "?code=" + Uri.EscapeDataString(passwordResetCode)+"&userId="+user.Id;                                        
                    var emailBody = emailtemplate.EmailBody.Replace("{VerificationURL}", verifyURL);
                    emailBody = emailBody.Replace("{USERNAME}", user.UserName).Replace("{Name}",user.Name).Replace("{CODE}", passwordResetCode).Replace("{DealerCode}", user.UserName);
                    
                    var smsBody = emailtemplate.SMSContent.Replace("{DealerCode}", user.UserName).Replace("{CODE}", passwordResetCode).Replace("{Name}", user.Name).Replace("{VerificationURL}", verifyURL);
                    await _smsManager.CreateSMSLog(new SMSLog{
                        Content = smsBody,
                        DateToSend = DateTime.Now,
                        Receiver = model.Phonenumber,
                        IsSendImmediately = true,
                        Status = SMSStatusEnum.Fresh                        
                    });

                    await _emailManager.LogEmail(new EmailLog
                    {
                        //BCC = "timileyinayegbayo@gmail.com",
                        //CC= "timileyinayegbayo@gmail.com",
                        CreatedBy = user.UserName,
                        MailBody = emailBody,
                        Receiver = user.Email,
                        Sender = emailtemplate.EmailSender,
                        Subject = emailtemplate.EmailSubject,
                        DateCreated = DateTime.Now,
                        DateToSend = DateTime.Now,
                        Status = VatEmailStatus.Fresh,
                        SendImmediately = true
                    });

                    await _auditTrailManager.AddAuditTrail(new AuditTrail
                    {
                        ActionTaken = AuditAction.Create,
                        DateCreated = DateTime.Now,
                        Description = $"User {user.UserName} has been created successfully",
                        Entity = "User",
                        UserId = user.Id,
                        UserName = user.UserName,
                    });

                    await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.Base);
                    await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Commission, CommissionMode.Base);

                    var commissionAheadServices = _serviceRepository.ServicesBasedOnCommissionMode(CommissionMode.CommissionAhead);
                    foreach (var service in commissionAheadServices)
                    {
                        if(!service.Vendor.IsCommissionAhead)
                        {                                
                            await _walletRepository.CreateWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.CommissionAhead, service.Id, service.ServiceName);
                        }
                    }

                    var commissionAheadVendors = await _vendorRepo.FetchCommissionAheadVendors();
                    foreach(var vendor in commissionAheadVendors)
                    {
                        await _walletRepository.CreateVendorWallet(user.Id, 0.00M, WalletType.Transaction, CommissionMode.CommissionAhead, vendor.Id, vendor.VendorName);
                    } 

                    await _userManager.AddToRoleAsync(user, RoleTypes.AGENT.GetDescription());
                    return PrepareResponse(HttpStatusCode.OK, "Dealer has been created successfully, a mail has been sent to the contact email address", false);
                }
                else
                {
                    return PrepareResponse(HttpStatusCode.BadRequest, userCreated.Errors.FirstOrDefault().Description, true, userCreated.Errors);
                }

            }
            return PrepareResponse(HttpStatusCode.PreconditionFailed, "Email Templates have not been setup", true, null);
        }

        /// <summary>
        /// Retrieve the employee by their ID.
        /// </summary>
        /// <param name="id">The ID of the desired Employee</param>
        /// <returns>A string status</returns>
        [HttpPost("resendConfirmMail")]
        public async Task<IActionResult> ResendConfirmationMail(ForgotPassswordRequest forgot)
        {
            var emailtemplate = _emailManager.GetEmailTemplate(VatEmailTemplateType.ResendCOnfirmationLink).FirstOrDefault(x => x.IsActive);
            if (emailtemplate != null)
            {
                var user = _userManager.Users.FirstOrDefault(x => x.Email == forgot.Username || x.UserName == forgot.Username);
                if (user != null)
                {
                    if (user.EmailConfirmed)
                    {
                        return PrepareResponse(HttpStatusCode.Conflict, "User has already been confirmed");
                    }

                    var passwordResetCode = await _userManager.GenerateChangePhoneNumberTokenAsync(user, user.PhoneNumber);                    
                    var verifyURL = _configuration["PASSWORDRESETURL"] + "?code=" + Uri.EscapeDataString(passwordResetCode)+"&userId="+user.Id;                                        
                    var emailBody = emailtemplate.EmailBody.Replace("{VerificationURL}", verifyURL);
                    emailBody = emailBody.Replace("{USERNAME}", user.UserName).Replace("code", passwordResetCode);

                    await _emailManager.LogEmail(new EmailLog
                    {
                        //BCC = "timileyinayegbayo@gmail.com",
                        //CC= "timileyinayegbayo@gmail.com",
                        CreatedBy = user.UserName,
                        MailBody = emailBody,
                        Receiver = user.Email,
                        Sender = emailtemplate.EmailSender,
                        Subject = emailtemplate.EmailSubject,
                        DateCreated = DateTime.Now,
                        DateToSend = DateTime.Now,
                        Status = VatEmailStatus.Fresh,
                        SendImmediately = true
                    });

                    await _auditTrailManager.AddAuditTrail(new AuditTrail
                    {
                        ActionTaken = AuditAction.Create,
                        DateCreated = DateTime.Now,
                        Description = $"User {user.UserName} request for confirmation link",
                        Entity = "User",
                        UserId = user.Id,
                        UserName = user.UserName,
                    });
                    return PrepareResponse(HttpStatusCode.OK, "Confirmation link has been sent to your email address", false);

                }

                return PrepareResponse(HttpStatusCode.NotFound, "Specified user not found.");
            }
            return PrepareResponse(HttpStatusCode.PreconditionFailed, "EMail template has not been setup confirmation link reset.");
        }

        /// <summary>
        /// Retrieve the employee by their ID.
        /// </summary>
        /// <param name="id">The ID of the desired Employee</param>
        /// <returns>A string status</returns>
        [HttpPost("confirmEmail")]
        public async Task<IActionResult> ConfirmEmail([FromBody]ConfirmEmail confirmEmail)
        {           
            var user = await _userManager.FindByIdAsync(confirmEmail.UserId);
            if(user != null)
            {
                var confirmed = await _userManager.ConfirmEmailAsync(user, confirmEmail.Code);
                if (confirmed.Succeeded)
                {
                    return PrepareResponse(HttpStatusCode.OK, "Email confirmation has been completed successfully", false, null);
                }
                else
                {
                    var error = confirmed.Errors;
                    return PrepareResponse(HttpStatusCode.BadRequest, error.FirstOrDefault().Description, true, error);

                }
            }

            return PrepareResponse(HttpStatusCode.NotFound, "User not found", true, null);

        }

        /// <summary>
        /// Retrieve the employee by their ID.
        /// </summary>
        /// <param name="id">The ID of the desired Employee</param>
        /// <returns>A string status</returns>
        [HttpPost("resetPassword")]
        public async  Task<IActionResult> ResetPassword(PasswordResetModel passwordReset)
        {
            var user = await _userManager.FindByIdAsync(passwordReset.UserId);
            if(user != null)
            {
                var verfied = await _userManager.VerifyChangePhoneNumberTokenAsync(user, passwordReset.Code, user.PhoneNumber);
                if(verfied)
                {
                    var code = await _userManager.GeneratePasswordResetTokenAsync(user);                    
                    var resetPassword = await _userManager.ResetPasswordAsync(user, code, passwordReset.Password);
                    if (resetPassword.Succeeded)
                    {
                        if(!user.EmailConfirmed)
                        {
                            user.EmailConfirmed = true;
                            await _userManager.UpdateAsync(user);
                        }
                        return PrepareResponse(HttpStatusCode.OK, "Password reset has been completed successfully", false, null);
                    }
                    else
                    {
                        var errors = resetPassword.Errors;
                        return PrepareResponse(HttpStatusCode.BadGateway, errors.FirstOrDefault().Description, true);
                    }                    
                }

                return PrepareResponse(HttpStatusCode.BadGateway,"Code Verification failed. Please try again", true );
            }   

            return PrepareResponse(HttpStatusCode.NotFound, "User not found");
        }

        /// <summary>
        /// Helps users to reset password
        /// </summary>
        /// <returns>A string status</returns>
        [HttpPost("forgotPassword")]
        public async Task<IActionResult> ForgotPassword(ForgotPassswordRequest forgot)
        {
            var emailTemplate = _emailManager.GetEmailTemplate(VatEmailTemplateType.PasswordReset).FirstOrDefault(x=>x.IsActive);
            if (emailTemplate != null)
            {
                var user = await _userManager.FindByNameAsync(forgot.Username);
                if(user != null)
                {                    
                    var passwordResetCode = await _userManager.GenerateChangePhoneNumberTokenAsync(user, user.PhoneNumber);                    

                    var verifyURL = _configuration["PASSWORDRESETURL"] + "?code=" + Uri.EscapeDataString(passwordResetCode)+"&userId="+user.Id;
                    var emailBody = emailTemplate.EmailBody.Replace("{PASSWORDRESETURL}", verifyURL).Replace("{USERNAME}", user.UserName).Replace("{CODE}", passwordResetCode);
                    var smsBody = emailTemplate.SMSContent.Replace("{CODE}",passwordResetCode).Replace("{PASSWORDRESETURL}", verifyURL).Replace("{USERNAME}", user.UserName);
                    await _smsManager.CreateSMSLog(new SMSLog{
                        Content = smsBody,
                        IsSendImmediately = true,
                        Receiver = user.PhoneNumber,   
                        Status = SMSStatusEnum.Fresh,
                        DateToSend = DateTime.Now                    
                    });

                    await _emailManager.LogEmail(new EmailLog
                    {
                        //BCC = "timileyinayegbayo@gmail.com",
                        //CC= "timileyinayegbayo@gmail.com",
                        CreatedBy = user.UserName,
                        MailBody = emailBody,
                        Receiver = user.Email,
                        Sender = emailTemplate.EmailSender,
                        Subject = emailTemplate.EmailSubject,
                        DateCreated = DateTime.Now,
                        Status = VatEmailStatus.Fresh,
                        DateToSend = DateTime.Now.Date,
                        SendImmediately = true
                    });

                    return PrepareResponse(HttpStatusCode.OK, "Forgot password link has been sent to your email", false);
                }
                return PrepareResponse(HttpStatusCode.NotFound, "Username does not exist", true, null);
            }
            return PrepareResponse(HttpStatusCode.PreconditionFailed, "Email Templates have not been setup", true, null);
        }

        /// <summary>
        /// Retrieve the employee by their ID.
        /// </summary>
        /// <param name="id">The ID of the desired Employee</param>
        /// <returns>A string status</returns>
        [HttpPost("logout/{userId}")]
        [Authorize]
        public async Task<IActionResult> Logout(int userId, string token)
        {
            await _signInManager.SignOutAsync();
            await _userRepository.RemoveDeviceToken(userId, token);
            return PrepareResponse(HttpStatusCode.OK, "You have been logged out successfully");
        }

        

        /// <summary>
        /// Retrieve the employee by their ID.
        /// </summary>
        /// <param name="id">The ID of the desired Employee</param>
        /// <returns>A string status</returns>
        [HttpPost("authenticate")]
        //[ValidateAntiForgeryToken]
        public async Task<IActionResult> Authenticate(LoginViewModel loginModel)
        {
            if (ModelState.IsValid)
            {
                var user = _userManager.Users.FirstOrDefault(x=>x.UserName == loginModel.Username || x.Email == loginModel.Username);
                if(user != null)
                {
                    if (user.EmailConfirmed)
                    {
                        var signIn = await _signInManager.PasswordSignInAsync(user.UserName, loginModel.Password, loginModel.RememberMe, false);
                        if (signIn.Succeeded)
                        {                            
                            var userProfile = await GenerateJwtToken(user.Email, user);
                            Response.Cookies.Append("access_token", userProfile.Token, new CookieOptions {
                                Path = "/",
                                HttpOnly = false,
                                IsEssential = true,
                                Expires = DateTime.Now.AddHours(3)
                            });
                            await _userRepository.SaveDeviceToken(loginModel.DeviceType, loginModel.DeviceName, loginModel.DeviceToken, user.Id);
                            
                            return PrepareResponse(HttpStatusCode.OK, "Authentication Successful", false, userProfile);
                        }
                        if (signIn.IsLockedOut)
                        {
                            return PrepareResponse(HttpStatusCode.Unauthorized, "You have been deactivated. Please contact customer care", true);
                        }
                    }
                    else
                    {
                        return PrepareResponse(HttpStatusCode.Unauthorized, "Please verify your email");
                    }
                }                
                return PrepareResponse(HttpStatusCode.Unauthorized, "Invalid username or password", true);
            }

            return PrepareResponse(HttpStatusCode.NotAcceptable, "Incomplete Parameters", true);
        }

    }
}