using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Core.Utilities;
using iqpay.Data.Entities.AuditTrail;
using iqpay.Data.Entities.Payment;
using iqpay.Engine.Base.Entities;
using iqpay.Engine.Repository.EmailRepository;
using iqpay.Repository.Wallet;
using iqpay.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using iqpay.Repository.Monnify.Models;
using iqpay.Repository.Monnify;
using iqpay.Repository.Rubies;
using Newtonsoft.Json;
using System.Security.Claims; 
using Microsoft.AspNetCore.Identity;
using iqpay.Data.UserManagement.Model;
using System.Globalization;
using iqpay.Repository.Wema;
using iqpay.Repository.Wema.Models;

namespace iqpay.Web.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]    
    public class WemaController : BaseController
    {
        private readonly IWemaRepository _wemaRepo;
        private readonly IWalletRepository _walletRepository;
        public WemaController(IMapper mapper, IWemaRepository wemaRepo, UserManager<ApplicationUser> userManager, IWalletRepository walletRepo)
        {
            _mapper = mapper;
            _wemaRepo = wemaRepo;
            _walletRepository = walletRepo;
            _userManager = userManager;
        }

        [HttpPost("lookup")]
        public IActionResult LookUp(WemaLookUp model)
        {
            var agentDetails = _wemaRepo.FindUserByWema(model.Accountnumber);
            var status = "00";
            if(agentDetails != null)
            {
                var name = agentDetails.Name ?? agentDetails.LastName+" "+agentDetails.FirstName;                
                return PrepareWemaResponse(HttpStatusCode.OK, name, status, "Account number found", false);
            }
            status = "07";
            return PrepareWemaResponse(HttpStatusCode.NotFound, null, status);
        }

        [HttpGet("accounts")]
        public IActionResult FetchAccount()
        {
            var accounts = _wemaRepo.FetchWemaAccounts();
            if(accounts.Any())
            {
                return PrepareResponse(HttpStatusCode.OK, "Virtual accounts returned", false, _mapper.Map<List<WemaAccount>>(accounts));
            }

            return PrepareResponse(HttpStatusCode.NotFound, "Virtual accounts have not been generated");
        }

        [HttpGet("transactions")]
        public async Task<IActionResult> FetchTransactions()
        {
            var userRole = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role).Value;
            var userId =  User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value; 
            var transactions = new List<BankTransfer>();
            if(userRole.Contains(RoleTypes.SYSADMIN.GetDescription()))
            {
                transactions = await _walletRepository.FetchTransfers();
            }else{
                var dealer = await _userManager.FindByIdAsync(userId);
                transactions = await _walletRepository.FetchTransfers(dealer.UserName);
            } 
            
            if(transactions.Any())
            {
                return PrepareResponse(HttpStatusCode.OK, "Virtual account transactions returned", false, transactions);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "Virtual account transactions not found");
        }

        [HttpGet("transactions/{dealerCode}")]
        public async Task<IActionResult> FetchTransactions(string dealerCode)
        {
            var transactions = await _walletRepository.FetchTransfers(dealerCode);
            if(transactions.Any())
            {
                return PrepareResponse(HttpStatusCode.OK, "Virtual account transactions returned", false, transactions);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "Virtual account transactions not found");
        }

        [HttpPost("fund")]
         public async Task<IActionResult> WemaFund(WemaPayload payLoad)
        {
            if(payLoad != null)
            {
                if(!string.IsNullOrWhiteSpace(payLoad.Paymentreference))
                {
                    var payLoadStr = JsonConvert.SerializeObject(payLoad);
                    var transfer = await _walletRepository.FetchWemaTransfer(payLoad.Paymentreference);
                    if(transfer?.IsProcessed == true)
                    {
                        return PrepareResponse(HttpStatusCode.Conflict, "This transaction has been processed already");
                    }
                    var wemaPaymentInfo = await _wemaRepo.FetchTransaction(payLoad.Sessionid, payLoad.Craccount, DateTime.Now, payLoad.Amount);                    
                    if(wemaPaymentInfo.Sessionid == payLoad.Sessionid && payLoad.Originatoraccountnumber == wemaPaymentInfo.Originatoraccountnumber && payLoad.Originatorname == wemaPaymentInfo.Originatorname)
                    {
                        var dealer = _userManager.Users.FirstOrDefault(x=>x.VirtualAccountNumber == payLoad.Craccount);
                        if(dealer != null){                        
                            var dealerWallet = await _walletRepository.FetchBaseWallet(dealer.Id);
                            if(dealerWallet != null)
                            {
                                var amountToFund =  payLoad.Amount - Convert.ToDecimal(_configuration["Wema:Rate"]);
                                var creditted = _walletRepository.CreditWallet(dealerWallet.Id, amountToFund);
                                _logger.Log(LogLevel.Information, $"Creditted => {dealerWallet.Id} credited: {creditted}");

                                var wemaWallet = await _walletRepository.FetchWemaWallet();
                                if(wemaWallet != null)
                                {
                                    _walletRepository.CreditWallet(wemaWallet.Id, payLoad.Amount);
                                    _logger.Log(LogLevel.Information, $"Creditted Wema Wallet => {wemaWallet.Id} credited: {creditted}");
                                }
                            }
                            await _walletRepository.LogTransfer(PaymentProcessor.Wema, payLoad.Amount, payLoad.Paymentreference,DateTime.Now,"PAID", payLoad.Paymentreference, true, payLoad.Originatoraccountnumber, payLoad.Originatorname, payLoad.Bankcode, JsonConvert.SerializeObject(payLoad), payLoad.Craccountname, payLoad.Craccount,dealer.UserName, TransferType.Credit);
                            return PrepareResponse(HttpStatusCode.OK, "Transaction has been completed successfully", true);                        
                        }
                    }
                }   
            }

            return PrepareResponse(HttpStatusCode.BadRequest, "Transaction could not be completed");
        }



    }
}