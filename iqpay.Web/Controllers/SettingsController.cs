using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Repository.Wallet;
using iqpay.Web.Models;
using Microsoft.AspNetCore.Mvc;
using iqpay.Repository.SettingsRepository;
using iqpay.Data.Entities.Settings;

namespace iqpay.Web.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class SettingsController:BaseController
    {        
        private readonly ISettingsRepository _settingsRepository;    
        public SettingsController(ISettingsRepository settingsRepository)
        {
            _settingsRepository = settingsRepository;
        }

        [HttpGet("")]
        public IActionResult FetchSettings()
        {
            var settings = _mapper.Map<SettingsModel>(_settingsRepository.FetchSettings());            
            return PrepareResponse(HttpStatusCode.OK, "Settings Data returned", false, settings);
        }

        [HttpPut("")]
        public IActionResult UpdateSettings(SettingsModel settings)
        {
            var update = _mapper.Map<Settings>(settings);
            _settingsRepository.UpdateSettings(update);                             
            return PrepareResponse(HttpStatusCode.OK, "Settings has been updated successfully", false, settings);
        }        
    }
}