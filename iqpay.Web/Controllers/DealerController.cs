﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using iqpay.Data.Entities.AuditTrail;
using iqpay.Engine.Base.Entities;
using iqpay.Repository.Users;
using iqpay.Data.UserManagement.Model;
using iqpay.Web.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace iqpay.Web.Controllers
{

    
    [Route("api/[controller]")]
    [ApiController]
    public class DealerController : BaseController
    {

        private IDealerRepository _dealerRepo;

        public DealerController(IDealerRepository dealerRepo)
        {
            _dealerRepo = dealerRepo;
        }


        /// <summary>
        ///Fetch dealers registered under a logged in dealer
        /// </summary>
        /// <returns>A payload of the list of dealers registered by a dealer</returns>
        [Authorize(Roles = "SYSADMIN,SUPER DEALER")]
        [HttpGet]
        public async Task<IActionResult> FetchDealers()
        {
            
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var role = User.Claims.FirstOrDefault(x=>x.Type == ClaimTypes.Role).Value;
            var dealers = new List<ApplicationUser>();
            if(role == "SYSADMIN")
            {
                 dealers = await _dealerRepo.FetchDealers(null);
            }
            else{
                  dealers = await _dealerRepo.FetchDealers(Convert.ToInt32(userId));
            }           
            if (dealers.Count > 0)
            {
                return PrepareResponse(System.Net.HttpStatusCode.OK, $"{dealers.Count} dealers found", false, dealers);
            }

            return PrepareResponse(System.Net.HttpStatusCode.NotFound, "No dealer found", true);

        }


        /// <summary>
        ///Fetch dealer's information
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload of the list of dealers registered by a dealer</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> FetchDealer(long id)
        {
            var dealer = await _dealerRepo.FetchDealerDetails(id);
            if(dealer != null)
            {

                return PrepareResponse(HttpStatusCode.OK, "dealer found", false, dealer);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No dealer found for that id", false);
        }

        /// <summary>
        ///update dealer's information
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload showing the if update was successful or not</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateDealer(int id, DealerRegisterViewModel dealer)
        {
            var existingDealer = await _dealerRepo.FetchDealerDetails(id);
            if (dealer != null)
            {
                existingDealer.Address = dealer.Address;
                existingDealer.Email = dealer.Email;
                existingDealer.PhoneNumber = dealer.Phonenumber;
                existingDealer.Name = dealer.Name;                
                await _dealerRepo.UpdateDealer(existingDealer);

                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    ActionTaken = AuditAction.Update,
                    DateCreated = DateTime.Now,
                    Description = $"Dealer {existingDealer.UserName} has been updated successfully",
                    Entity = "Dealer",
                    UserId = Convert.ToInt64(userId)

                });
                return PrepareResponse(HttpStatusCode.OK, "dealer details has been updated successfully", false, dealer);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No dealer found for that id", true);
        }


        /// <summary>
        ///Deactivte dealer
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload showing if deactivation was successful or not</returns>
        [HttpPut("{id}/deactivate")]
        public async Task<IActionResult> DeactivateDealer(int id)
        {
            var existingDealer = await _dealerRepo.FetchDealerDetails(id);
            if(existingDealer != null)
            {
                existingDealer.LockoutEnabled = true;
                existingDealer.LockoutEnd = DateTime.MaxValue;
                await _dealerRepo.UpdateDealer(existingDealer);
                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    ActionTaken = AuditAction.Update,
                    DateCreated = DateTime.Now,
                    Description = $"Dealer {existingDealer.UserName} has been deactivated successfully",
                    Entity = "Dealer",
                    UserId = Convert.ToInt64(userId)

                });
                return PrepareResponse(HttpStatusCode.OK, "Dealer has been deactivated successfully", false);
            }
            return PrepareResponse(HttpStatusCode.NotFound, "The specified dealer does not exist");

        }

        // <summary>
        ///Activate deactivated dealer
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload showing if activation was successful or no</returns>
        [HttpPut("{id}/activate")]
        public async Task<IActionResult> ActivateDealer(int id)
        {
            var existingDealer = await _dealerRepo.FetchDealerDetails(id);
            if (existingDealer != null)
            {
                existingDealer.LockoutEnd = null;
                await _dealerRepo.UpdateDealer(existingDealer);
                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    ActionTaken = AuditAction.Update,
                    DateCreated = DateTime.Now,
                    Description = $"Dealer {existingDealer.UserName} has been activated successfully",
                    Entity = "Dealer",
                    UserId = Convert.ToInt64(userId)

                });
                return PrepareResponse(HttpStatusCode.OK, "Dealer has been activated successfully", false);
            }
            return PrepareResponse(HttpStatusCode.NotFound, "The specified dealer does not exist");
        }


        /// <summary>
        ///Remove dealer
        /// </summary>
        /// <param name="id">dealer's id</param>
        /// <returns>A payload showing if removal was successful or not</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> RemoveDealer(int id)
        {
            var existingDealer = await _dealerRepo.FetchDealerDetails(id);
            if (existingDealer != null)
            {
                await _dealerRepo.RemoveDealer(existingDealer.Id);

                var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
                await _auditTrailManager.AddAuditTrail(new AuditTrail
                {
                    ActionTaken = AuditAction.Delete,
                    DateCreated = DateTime.Now,
                    Description = $"Dealer {existingDealer.UserName} has been removed successfully",
                    Entity = "Dealer",
                    UserId = Convert.ToInt64(userId)

                });
                return PrepareResponse(HttpStatusCode.OK, "dealer details has been removed successfully", false, null);
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No dealer found for that id", true);
        }




    }
}