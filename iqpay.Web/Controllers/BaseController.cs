﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Microsoft.Extensions.Logging;
using iqpay.Data.UserManagement.Model;
using iqpay.Data.Entities.AuditTrail;
using iqpay.Engine.Repository.EmailRepository;
using iqpay.Web.Models;
using iqpay.Web.Filters;

namespace iqpay.Web.Controllers
{
    
    [ValidateModel]
    public class BaseController : ControllerBase
    {
        protected  UserManager<ApplicationUser> _userManager;
        protected readonly SignInManager<ApplicationUser> _signInManager;
        protected readonly RoleManager<ApplicationRole> _roleManager;
        protected  IConfiguration _configuration;
        protected  IMapper _mapper;
        protected  ILogger<BaseController> _logger;
        protected  IAuditTrailManager<AuditTrail> _auditTrailManager;
        protected readonly ApplicationUser _userInfo;

        public BaseController()
        {
            
        }

        public BaseController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, RoleManager<ApplicationRole> roleManager, IConfiguration configuration, IMapper mapper, ILogger<BaseController> logger, IAuditTrailManager<AuditTrail> auditTrailManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _configuration = configuration;
            _mapper = mapper;
            _logger = logger;
            _auditTrailManager = auditTrailManager;
            //_userInfo = User != null && User.Identity.IsAuthenticated ? .Result : null;

        }

      /**  public BaseController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            RoleManager<ApplicationRole> roleManager,
            IConfiguration configuration,
            IMapper mapper,
            ILogger<BaseController> logger,
            IAuditTrailManager<AuditTrail> auditTrailManager            

            )
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _configuration = configuration;
            _mapper = mapper;
            _logger = logger;
            _roleManager = roleManager;
            _auditTrailManager = auditTrailManager;
            _userInfo = User != null && User.Identity.IsAuthenticated ? _userManager.FindByNameAsync(User?.Identity?.Name).Result : null;
            

            
        }**/



        

        protected async Task<AuthModel> GenerateJwtToken(string email, ApplicationUser user)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);
            string roles = string.Empty;
            IList<string> role = await _userManager.GetRolesAsync(user);            
            
            if (role.Any())
            {
                roles = role.Join();
            }
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, email),
                new Claim(JwtRegisteredClaimNames.Jti, user.Id.ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.UserName),
                new Claim(ClaimTypes.Email, user.Email),
                new Claim(ClaimTypes.Name, user.UserName)
            }.Union(userClaims);

            foreach(var r in role)
            {
                claims = claims.Append(new Claim(ClaimTypes.Role, r));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtKey"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddMinutes(Convert.ToInt32(_configuration["JwtExpire"]));

            var token = new JwtSecurityToken(
                _configuration["JwtIssuer"],
                _configuration["JwtIssuer"],
                claims,
                expires: expires,
                signingCredentials: creds
            );

            var auth = new AuthModel
            {
                ExpiryTime = token.ValidTo,
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                Username = user.UserName,
                Role = roles,
                Email = user.Email,
                UserId = user.Id,
                Firstname = user.FirstName == null ? user.Name : user.FirstName,
                Lastname = user.LastName,
                Permissions = user.Permissions                
            };
            return auth;
        }
       
        protected IActionResult PrepareResponse(HttpStatusCode statusCode, string message = "", bool error = true, object content = null)
        {
            var obj = StatusCode((int)statusCode, new ApiResponse(message, statusCode, content, error));
            return obj;
        }

        protected IActionResult PrepareWemaResponse(HttpStatusCode statusCode, string accountName = null, string status = null, string statusDesc = null, bool error = true)
        {
            var obj = StatusCode((int)statusCode, new WemaAPIResponse(accountName, statusCode, status, statusDesc, error));
            return obj;
        }
    }
}