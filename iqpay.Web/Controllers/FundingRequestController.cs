﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Repository.Wallet;
using iqpay.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using iqpay.Data.Entities.Payment;
using iqpay.Data.UserManagement.Model;
using iqpay.Core.Utilities;

namespace iqpay.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FundingRequestController : BaseController
    {
        private IWalletRepository _repo;
        public FundingRequestController(IMapper mapper, IWalletRepository repo)
        {
            _mapper = mapper;
            _repo = repo;
        }

        [HttpGet]
        public IActionResult Index(DateTime? startDate, DateTime? endDate)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var userRole = User.Claims.FirstOrDefault(x=>x.Type == ClaimTypes.Role).Value;
            var requests = new List<FundWalletRequest>();
            if(endDate.HasValue)
            {
                endDate = endDate.Value.AddDays(1);
            }
            if(userRole.Contains(RoleTypes.SYSADMIN.GetDescription()))
            {
                requests = _repo.FetchFundWalletRequests(null, startDate, endDate);
            }else{
                requests = _repo.FetchFundWalletRequests(Convert.ToInt64(userId), startDate, endDate);
            }
                        
            var unAttendedRequest = requests.Where(x => x.Status == Data.Entities.Payment.FundWalletRequestStatus.Inititated);
            return PrepareResponse(HttpStatusCode.OK, $"{unAttendedRequest.Count()} unattended requests found", false, _mapper.Map<List<FundingRequest>>(requests));
        }

        [HttpGet("{id}")]
        public IActionResult Index(long id)
        {
            var request = _repo.GetFundWalletRequest(id);
            return PrepareResponse(HttpStatusCode.OK, "", false, _mapper.Map<FundingRequest>(request));
        }

        [HttpGet("history")]
        public IActionResult RequestedByUser(long initiatedBy, DateTime? startDate, DateTime? endDate)
        {
            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            if(endDate.HasValue)
            {
                endDate = endDate.Value.AddDays(1);
            }
            var requests = _repo.FundingRequestedByUser(Convert.ToInt64(userId), startDate, endDate);
            
            var unAttendedRequest = requests.Where(x => x.Status == Data.Entities.Payment.FundWalletRequestStatus.Inititated);
            return PrepareResponse(HttpStatusCode.OK, $"{unAttendedRequest.Count()} unattended requests found", false, _mapper.Map<List<FundingRequest>>(requests));
        }


        [HttpPut("{id}/approve/{amount}")]
        public IActionResult Approve(long id, decimal amount)
        {

            var userId = User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value;
            var request = _repo.GetFundWalletRequest(id);
            if(request != null)
            {
                if(request.Amount < amount)
                {
                    return PrepareResponse(HttpStatusCode.BadRequest, "You can not fund more than requested", true);
                }
                request.AmmountCreditted = amount;                
                var fulfilled = _repo.FulfillRequest(request, Convert.ToInt64(userId));
                if (fulfilled)
                {
                    return PrepareResponse(HttpStatusCode.OK, "Funding has been completed successfully", false);
                }
                else
                {
                    return PrepareResponse(HttpStatusCode.ExpectationFailed, "Funding could not be completed due to insufficient funds");
                }

            }

            return PrepareResponse(HttpStatusCode.NotFound, "The specified fund request could not be found");
        }


        //[HttpGet]
        //public IActionResult Index(DateTime startDate, DateTime endDate)
        //{
        //    var requests = _repo.FetchFundWalletRequests();
        //    var unAttendedRequest = requests.Where(x => x.Status == Data.Entities.Payment.FundWalletRequestStatus.Inititated);
        //    return PrepareResponse(HttpStatusCode.OK, $"{unAttendedRequest.Count()} unattended requests found", false, requests);
        //}
    }
}
