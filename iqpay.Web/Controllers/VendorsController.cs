﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using iqpay.Data.Entities.Services;
using iqpay.Repository.ServicesRepository;
using iqpay.Web.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Authorization;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace iqpay.Web.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class VendorsController : BaseController
    {
        private readonly IVendorRepository _repo;

        public VendorsController(IVendorRepository repo, IMapper mapper, IConfiguration configuration) : base()
        {
            _repo = repo;
            _mapper = mapper;
            _configuration = configuration;
        }

        ///<summary>
        ///Fetch vendors
        /// </summary>
        /// <returns>A payload showing a list of vendors</returns>
        [HttpGet]
        public async Task<IActionResult> FetchVendors()
        {

            var returnedVendors = await _repo.FetchVendors();
            var vendors = _mapper.Map<List<VendorsModel>>(returnedVendors);
            if (vendors.Count > 0)
            {
                return PrepareResponse(System.Net.HttpStatusCode.OK, $"{vendors.Count} vendors returned", false, vendors);
            }

            return PrepareResponse(System.Net.HttpStatusCode.NotFound, "No vendor found", true, null);
        }


        ///<summary>
        ///Create vendors
        /// </summary>
        /// <returns>A payload showing a list of vendors</returns>
        [HttpPost]
        [Authorize(Roles = "SYSTEM ADMIN")]
        public async Task<IActionResult> CreateVendors(VendorsModel model)
        {

            var vendor = _mapper.Map<Vendor>(model);
            vendor.UserId = Convert.ToInt64(User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value);
            await _repo.CreateVendor(vendor);

            return PrepareResponse(System.Net.HttpStatusCode.OK, "Vendor has been created successfully", false, null);

        }

        /// <summary>
        /// upload user profile image or company logo.
        /// </summary>
        /// <returns>Payload stating if image upload was successful or not</returns>
        [Authorize(Roles = "SYSTEM ADMIN")]
        [HttpPut("{id}/upload-logo")]
        public async Task<IActionResult> UploadVendorLogo(long id, IFormFile file)
        {
            var vendor = await _repo.FetchVendorInfo(id);
            if(vendor == null){
                return PrepareResponse (HttpStatusCode.NotFound, "The specified vendor can not be found");
            }
            
            var allowedExtensions = new string[] {"jpg","jpeg" ,"png", "gif"};
            if(file != null)
            {
                var fileParts = file.FileName.Split('.');
                var fileExtension = fileParts[fileParts.Length - 1];
                if(allowedExtensions.Contains(fileExtension.ToLower()))
                {
                    var logoFolderPath = _configuration.GetValue<string>("LOGOPATH"); 
                    var fileName = vendor.Id+"."+fileExtension;                   
                    var logoFilePath = Path.Combine("wwwroot",logoFolderPath,fileName);
                    using(var fs = new FileStream(logoFilePath,FileMode.Create))
                    {
                        file.CopyTo(fs);
                        vendor.LogoSrc = fileName;

                        await _repo.UpdateVendor(id, vendor);
                        return PrepareResponse(HttpStatusCode.OK, "Vendor logo has been uploaded successfully", false);
                    }

                }
                return PrepareResponse(HttpStatusCode.UnsupportedMediaType, $"Please upload an image within the given extensions {string.Join(',',allowedExtensions)}", true);
            }
            return PrepareResponse(HttpStatusCode.ExpectationFailed, "Please upload a file", true);

        }

        


        ///<summary>
        ///Update vendor
        /// </summary>
        /// <returns>A payload if vendor update was successful</returns>
        [HttpPut("{id}")]
        [Authorize(Roles = "SYSTEM ADMIN")]
        public async Task<IActionResult> UpdateVendors(long id, VendorsModel model)
        {

            var vendor = _mapper.Map<Vendor>(model);
            vendor.LogoSrc = null;
            //vendor.UserId = Convert.ToInt64(User.Claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti).Value);
            await _repo.UpdateVendor(id, vendor);
            
            return PrepareResponse(System.Net.HttpStatusCode.OK, "Vendor has been updated successfully", false, null);

        }

        ///<summary>
        ///Update vendor commission rates
        /// </summary>
        /// <returns>A payload if vendor update was successful</returns>
        [HttpPut("{id}/vend-comm")]
        [Authorize(Roles="SYSTEM ADMIN")]
        public async Task<IActionResult> UpdateVendorCommission(long id, VendorCommissionModel model)
        {
            var vendor = await _repo.FetchVendorInfo(id);
            if(vendor != null)
            {
                vendor.SuperDealerCommission = model.SuperDealerCommission;
                vendor.SuperDealerCommissionCap = model.SuperDealerCommissionCap;
                vendor.DealerCommissionCap = model.DealerCommissionCap;
                vendor.DealerCommission = model.DealerCommission;
                vendor.AgentCommission = model.AgentCommission;
                vendor.AgentCommissionCap = model.AgentCommissionCap;
                vendor.CustomerCommissionCap = model.CustomerCommissionCap;
                vendor.CustomerCommission = model.CustomerCommission;
                vendor.Commission = model.Commission;
                vendor.CommissionCap = model.CommissionCap;
                vendor.LogoSrc = null;
                await _repo.UpdateVendor(vendor.Id, vendor);
                return PrepareResponse(HttpStatusCode.OK, "Service has been updated successfully", false);            
            }

            return PrepareResponse(HttpStatusCode.NotFound, "Specified vendor not found", true);
        }


        ///<summary>
        ///Fetch vendors services
        /// </summary>
        /// <returns>A payload showing a list services of a vendor</returns>
        [HttpGet("{vendorId}/services")]
        public async Task<IActionResult> FetchVendorServices(int vendorId)
        {
            var services = await _repo.FetchVendorServices(vendorId);
            if(services.Count > 0)
            {
                return PrepareResponse(HttpStatusCode.OK, "Service returned", false, _mapper.Map<List<ServicesModel>>(services));
            }

            return PrepareResponse(HttpStatusCode.NotFound, "No service found for this vendor", true);
        }


        ///<summary>
        ///Fetch vendors
        /// </summary>
        /// <returns>A payload showing a list of vendors</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> FetchVendors(int id)
        {
            var vendor = await _repo.FetchVendorInfo(id);
            if (vendor != null)
            {
                return PrepareResponse(System.Net.HttpStatusCode.OK, "Vendors returned", false, _mapper.Map<VendorsModel>(vendor));
            }

            return PrepareResponse(System.Net.HttpStatusCode.NotFound, "Vendor not found", true, null);
        }





    }
}
